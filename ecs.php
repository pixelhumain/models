<?php
declare(strict_types=1);
// ecs.php
// use PhpCsFixer\Fixer\Import\NoUnusedImportsFixer;
// use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;
use Symplify\EasyCodingStandard\Config\ECSConfig;

// NoUnusedImportsFixer supprimer les use qui ne sont pas utilisés
// SetList::PSR_12 regle PSR 12
return static function (ECSConfig $ecsConfig): void {
    //$ecsConfig->rule(NoUnusedImportsFixer::class);

    $ecsConfig->paths([__DIR__ . '/src', __DIR__ . '/tests']);
    
    $ecsConfig->sets([SetList::PSR_12, SetList::ARRAY, SetList::SPACES]);
    
};