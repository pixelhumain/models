<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests\Services;

use PHPUnit\Framework\TestCase;
use PixelHumain\Models\Services\Interfaces\Mailer\ExtendedMailerInterface;
use PixelHumain\Models\Services\Interfaces\Mailer\MessageInterface;
use PixelHumain\Models\Services\Interfaces\Mailer\TransportInterface;
use PixelHumain\Models\Services\MailerService;

class MailerServiceTest extends TestCase
{
    private $mailerMock;

    private MailerService $mailerService;

    protected function setUp(): void
    {
        $this->mailerMock = $this->createMock(ExtendedMailerInterface::class);
        $this->mailerService = new MailerService($this->mailerMock);
    }

    public function testSetTransport()
    {
        $transport = $this->createMock(TransportInterface::class);
        $this->mailerMock->expects($this->once())
            ->method('setTransport')
            ->with($this->equalTo($transport));

        $this->mailerService->setTransport($transport);
    }

    public function testSend()
    {
        $message = $this->createMock(MessageInterface::class);
        $this->mailerMock->expects($this->once())
            ->method('send')
            ->with($this->equalTo($message))
            ->willReturn(true);

        $result = $this->mailerService->send($message);
        $this->assertTrue($result);
    }

    public function testCompose()
    {
        $view = 'emailTemplate';
        $params = [
            'param1' => 'value1',
            'param2' => 'value2',
        ];
        $expectedMessage = $this->createMock(MessageInterface::class);

        $this->mailerMock->expects($this->once())
            ->method('compose')
            ->with($this->equalTo($view), $this->equalTo($params))
            ->willReturn($expectedMessage);

        $result = $this->mailerService->compose($view, $params);
        $this->assertSame($expectedMessage, $result);
    }

    public function testSendMultiple()
    {
        $messages = [$this->createMock(MessageInterface::class), $this->createMock(MessageInterface::class)];
        $this->mailerMock->expects($this->once())
            ->method('sendMultiple')
            ->with($this->equalTo($messages))
            ->willReturn(2);

        $result = $this->mailerService->sendMultiple($messages);
        $this->assertEquals(2, $result);
    }
}
