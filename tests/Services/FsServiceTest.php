<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests\Services;

use League\Flysystem\DirectoryAttributes;
use League\Flysystem\FileAttributes;
use League\Flysystem\FilesystemOperator;

use PHPUnit\Framework\TestCase;
use PixelHumain\Models\Services\Flysystem\FilesystemExtends;
use PixelHumain\Models\Services\Flysystem\Local\LocalFilesystemAdapterExtends;

use PixelHumain\Models\Services\FsService;

// use League\Flysystem\Filesystem;
// use League\Flysystem\Local\LocalFilesystemAdapter;

class FsServiceTest extends TestCase
{
    protected FsService $fsService;

    protected FilesystemOperator $fsMock;

    protected string $rootPath;

    protected function setUp(): void
    {
        parent::setUp();

        // $this->fsMock = $this->createMock(FilesystemOperatorInterface::class);
        // $this->fsService = new FsService($this->fsMock);

        $this->rootPath = __DIR__ . '/root/';
        $adapter = new LocalFilesystemAdapterExtends($this->rootPath);
        $this->fsMock = new FilesystemExtends($adapter);

        $this->fsService = new FsService($this->fsMock);
    }

    public function testFileExists()
    {
        $location = 'file.txt';

        // $this->fsMock->expects($this->once())
        //     ->method('fileExists')
        //     ->with($location)
        //     ->willReturn(true);

        $result = $this->fsService->fileExists($location);

        $this->assertTrue($result);
    }

    public function testDirectoryExists()
    {
        $location = 'directory';
        // $this->fsMock->expects($this->once())
        //     ->method('directoryExists')
        //     ->with($location)
        //     ->willReturn(true);

        $result = $this->fsService->directoryExists($location);
        $this->assertTrue($result);
    }

    public function testHasDirectory()
    {
        $location = 'directory';
        // $this->fsMock->expects($this->once())
        //     ->method('has')
        //     ->with($location)
        //     ->willReturn(true);

        $result = $this->fsService->has($location);
        $this->assertTrue($result);
    }

    public function testHasFile()
    {
        $location = 'file.txt';
        // $this->fsMock->expects($this->once())
        //     ->method('has')
        //     ->with($location)
        //     ->willReturn(true);

        $result = $this->fsService->has($location);
        $this->assertTrue($result);
    }

    public function testRead()
    {
        $location = 'file.txt';
        $contents = 'Lorem ipsum dolor sit amet';

        // $this->fsMock->expects($this->once())
        //     ->method('read')
        //     ->with($location)
        //     ->willReturn($contents);

        $result = $this->fsService->read($location);
        $this->assertEquals($contents, $result);
    }

    public function testReadStream()
    {
        $location = 'file.txt';
        $expectedStreamContent = 'Lorem ipsum dolor sit amet';

        // $stream = fopen('php://memory', 'r');
        // $this->fsMock->expects($this->once())
        //     ->method('readStream')
        //     ->with($location)
        //     ->willReturn($stream);

        $stream = $this->fsService->readStream($location);

        // Lire le contenu du flux
        $content = stream_get_contents($stream);
        rewind($stream); // Rembobiner le flux si nécessaire

        $this->assertSame($expectedStreamContent, $content);
    }

    public function testListContents()
    {
        $location = 'directory';
        $deep = true;

        $expected = [
            new DirectoryAttributes(
                'directory/sub',
                'public',
                1_703_064_858
            ),
            new FileAttributes(
                'directory/sub/test.txt',
                0,
                'public',
                1_703_064_770
            ),
        ];

        // $listing = $this->createMock(DirectoryListing::class);
        // $this->fsMock->expects($this->once())
        //     ->method('listContents')
        //     ->with($location, $deep)
        //     ->willReturn($listing);

        $directoryListing = $this->fsService->listContents($location, $deep);
        $resultAsArray = iterator_to_array($directoryListing);

        // Assurez-vous que vous avez le bon nombre d'éléments
        $this->assertCount(count($expected), $resultAsArray);

        foreach ($resultAsArray as $index => $item) {
            // Vérifiez le type de chaque élément (DirectoryAttributes ou FileAttributes)
            $this->assertInstanceOf(get_class($expected[$index]), $item);

            // Vérifiez d'autres propriétés, si nécessaire, sauf les timestamps
            $this->assertEquals($expected[$index]['path'], $item['path']);
            $this->assertEquals($expected[$index]['visibility'], $item['visibility']);
        }
    }

    public function testLastModified()
    {
        $path = 'file.txt';

        if (file_exists($this->rootPath . $path)) {
            unlink($this->rootPath . $path);
            // create a new file with "Lorem ipsum dolor sit amet" as content
            file_put_contents($this->rootPath . $path, 'Lorem ipsum dolor sit amet');
        }

        $timestamp = 1_703_060_105;
        // $this->fsMock->expects($this->once())
        //     ->method('lastModified')
        //     ->with($path)
        //     ->willReturn($timestamp);

        $result = $this->fsService->lastModified($path);

        // Obtenez le timestamp actuel
        $now = time();

        // Vérifiez si le timestamp est dans une plage raisonnable, par exemple +/- 5 secondes
        $this->assertGreaterThanOrEqual($now - 20, $result);
        $this->assertLessThanOrEqual($now + 20, $result);
    }

    public function testFileSize()
    {
        $path = 'file.txt';
        $size = 26;
        // $this->fsMock->expects($this->once())
        //     ->method('fileSize')
        //     ->with($path)
        //     ->willReturn($size);

        $result = $this->fsService->fileSize($path);
        $this->assertEquals($size, $result);
    }

    public function testMimeType()
    {
        $path = 'file.txt';
        $mimeType = 'text/plain';
        // $this->fsMock->expects($this->once())
        //     ->method('mimeType')
        //     ->with($path)
        //     ->willReturn($mimeType);

        $result = $this->fsService->mimeType($path);
        $this->assertEquals($mimeType, $result);
    }

    public function testVisibility()
    {
        $path = 'file.txt';
        $visibility = 'public';
        // $this->fsMock->expects($this->once())
        //     ->method('visibility')
        //     ->with($path)
        //     ->willReturn($visibility);

        $result = $this->fsService->visibility($path);
        $this->assertEquals($visibility, $result);
    }

    public function testWrite()
    {
        $location = 'fileWrite.txt';
        $contents = 'Lorem ipsum dolor sit amet';

        // $config = ['option' => 'value'];
        // $this->fsMock->expects($this->once())
        //     ->method('write')
        //     ->with($location, $contents, $config);

        $this->fsService->write($location, $contents);

        // Vérifiez si le fichier a été écrit correctement
        $this->assertFileExists($this->rootPath . $location);
        $this->assertEquals($contents, file_get_contents($this->rootPath . $location));

        // Nettoyage: Supprimez le fichier après le test
        unlink($this->rootPath . $location);
    }

    public function testWriteStream()
    {
        $location = 'fileWrite.txt';
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, 'Test content');
        rewind($stream);
        // $config = ['option' => 'value'];
        // $this->fsMock->expects($this->once())
        //     ->method('writeStream')
        //     ->with($location, $stream, $config);

        $this->fsService->writeStream($location, $stream);

        // Vérifiez si le fichier a été écrit correctement
        $this->assertFileExists($this->rootPath . $location);
        $this->assertNotEquals(0, filesize($this->rootPath . $location));

        // Nettoyage: Supprimez le fichier et fermez le flux après le test
        fclose($stream);
        unlink($this->rootPath . $location);
    }

    public function testSetVisibility()
    {
        $path = 'directory/sub/test.txt';
        $newVisibility = 'private';

        // Obtenez la visibilité actuelle pour la restaurer plus tard
        $originalVisibility = $this->fsService->visibility($path);

        // $this->fsMock->expects($this->once())
        //     ->method('setVisibility')
        //     ->with($path, $visibility);

        // Modifiez la visibilité
        $this->fsService->setVisibility($path, $newVisibility);

        // Vérifiez si la visibilité a été modifiée correctement
        $updatedVisibility = $this->fsService->visibility($path);
        $this->assertEquals($newVisibility, $updatedVisibility);

        // Restaurez la visibilité originale
        $this->fsService->setVisibility($path, $originalVisibility);
    }

    public function testDelete()
    {
        $location = 'testAutre.txt';

        // Assurez-vous que le fichier existe avant de l'exécuter
        if (! file_exists($this->rootPath . $location)) {
            file_put_contents($this->rootPath . $location, '');
        }

        // $this->fsMock->expects($this->once())
        //     ->method('delete')
        //     ->with($location);

        $this->fsService->delete($location);

        // Vérifiez que le fichier a été supprimé
        $this->assertFileDoesNotExist($this->rootPath . $location);

        // Nettoyage: Recréez le fichier si nécessaire pour d'autres tests
        file_put_contents($this->rootPath . $location, '');
    }

    public function testDeleteDirectory()
    {
        $location = 'directoryTest';
        // Assurez-vous que le répertoire existe avant de l'exécuter
        if (! is_dir($this->rootPath . $location)) {
            mkdir($this->rootPath . $location);
        }
        // $this->fsMock->expects($this->once())
        //     ->method('deleteDirectory')
        //     ->with($location);

        $this->fsService->deleteDirectory($location);

        // Vérifiez que le répertoire a été supprimé
        $this->assertDirectoryDoesNotExist($this->rootPath . $location);
    }

    public function testCreateDirectory()
    {
        $location = 'directoryTest';
        // Supprimez le répertoire s'il existe déjà
        if (is_dir($this->rootPath . $location)) {
            rmdir($this->rootPath . $location);
        }
        // $config = ['option' => 'value'];
        // $this->fsMock->expects($this->once())
        //     ->method('createDirectory')
        //     ->with($location, $config);

        $this->fsService->createDirectory($location);

        // Vérifiez que le répertoire a été créé
        $this->assertDirectoryExists($this->rootPath . $location);

        // Nettoyage: Supprimez le répertoire après le test
        rmdir($this->rootPath . $location);
    }

    public function testMove()
    {
        $source = 'fileMove.txt';
        $destination = 'moveRep/newfile.txt';

        // Assurez-vous que le fichier source existe
        if (! file_exists($this->rootPath . $source)) {
            file_put_contents($this->rootPath . $source, 'Test content');
        }

        // Vérifiez si le répertoire de destination existe, sinon créez-le
        $destinationDir = dirname($this->rootPath . $destination);
        if (! is_dir($destinationDir)) {
            mkdir($destinationDir, 0777, true); // true pour la création récursive
        }

        // Assurez-vous que la destination n'existe pas
        if (file_exists($this->rootPath . $destination)) {
            unlink($this->rootPath . $destination);
        }

        // $this->fsMock->expects($this->once())
        //     ->method('move')
        //     ->with($source, $destination, $config);

        $this->fsService->move($source, $destination);

        // Vérifiez que le fichier source n'existe plus
        $this->assertFileDoesNotExist($this->rootPath . $source);

        // Vérifiez que le fichier existe à la destination
        $this->assertFileExists($this->rootPath . $destination);

        // Nettoyage: Supprimez le fichier déplacé et restaurez l'état initial si nécessaire
        unlink($this->rootPath . $destination);
        if (! file_exists($this->rootPath . $source)) {
            rmdir($destinationDir);
        }
    }

    public function testCopy()
    {
        $source = 'fileMove.txt';
        $destination = 'moveRep/newfile.txt';

        if (! file_exists($this->rootPath . $source)) {
            file_put_contents($this->rootPath . $source, 'Test content');
        }

        // Assurez-vous que le fichier de destination n'existe pas
        if (file_exists($this->rootPath . $destination)) {
            unlink($this->rootPath . $destination);
        }

        // Créez le répertoire de destination si nécessaire
        $destinationDir = dirname($this->rootPath . $destination);
        if (! is_dir($destinationDir)) {
            mkdir($destinationDir, 0777, true);
        }

        // $config = ['option' => 'value'];
        // $this->fsMock->expects($this->once())
        //     ->method('copy')
        //     ->with($source, $destination, $config);

        $this->fsService->copy($source, $destination);

        // Vérifiez que le fichier source existe toujours
        $this->assertFileExists($this->rootPath . $source);

        // Vérifiez que le fichier a été copié à la destination
        $this->assertFileExists($this->rootPath . $destination);

        // Nettoyage: Supprimez le fichier de destination après le test
        unlink($this->rootPath . $destination);
        // Supprimez le répertoire de destination si nécessaire
        if (is_dir($destinationDir) && (is_countable(scandir($destinationDir)) ? count(scandir($destinationDir)) : 0) === 2) { // 2 pour '.' et '..'
            rmdir($destinationDir);
        }
    }
}
