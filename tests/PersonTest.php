<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests;

use DateTime;
use DateTimeZone;
use Exception;
use InvalidArgumentException;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use PHPUnit\Framework\TestCase;
use PixelHumain\Models\Event;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\RoleInterface;
use PixelHumain\Models\Organization;
use PixelHumain\Models\Params;
use PixelHumain\Models\Person;
use PixelHumain\Models\Project;
use TypeError;

class PersonTest extends TestCase
{
    use TestSetupTrait;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpCommonMocks();
    }

    public function testRoleSetterGetter()
    {
        // Créer une instance de Person
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        $this->assertSame($this->roleMock, $person->getModelRole());
        $this->assertInstanceOf(RoleInterface::class, $person->getModelRole());
    }

    public function testGetRoleThrowsExceptionWhenNotSet()
    {
        $this->expectException(Exception::class);

        // Créer une instance de Person
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);
        $person->getModelRole();
    }

    public function testInvalidPropertyThrowsException()
    {
        $this->expectException(TypeError::class);
        new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,
            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'language' => 123,
            'params' => 123,
            'moduleId' => 'test',
        ]);
    }

    public function testLogguedAndValidReturnsTrueWhenLoggedInAndValid()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
            'userRole' => 'admin',
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn([
                "_id" => $this->dbMock->MongoId($mockData['userId']),
            ]);

        // Mock role canUserLogin method
        $this->roleMock->method('canUserLogin')
            ->with($this->equalTo([
                "_id" => $mockData['userId'],
            ]), $this->equalTo(false))
            ->willReturn([
                "result" => true,
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,
            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Call the logguedAndValid method
        $result = $person->logguedAndValid();

        // Assert that the result is true
        $this->assertTrue($result);
    }

    public function testLogguedAndValidReturnsFalseWhenNotLoggedIn()
    {
        $mockData = [];

        $this->sessionData = $mockData;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the logguedAndValid method
        $result = $person->logguedAndValid();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testLogguedAndValidReturnsFalseWhenUserNotFound()
    {
        // Mock session data
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
            'userRole' => 'admin',
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn(null);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the logguedAndValid method
        $result = $person->logguedAndValid();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testLogguedAndValidReturnsFalseWhenValidityCheckFails()
    {
        // Mock session data
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
            'userRole' => 'admin',
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn([
                "_id" => $this->dbMock->MongoId($mockData['userId']),
            ]);

        // Mock role canUserLogin method
        $this->roleMock->method('canUserLogin')
            ->with($this->equalTo([
                "_id" => $mockData['userId'],
            ]), $this->equalTo(false))
            ->willReturn([
                "result" => false,
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Call the logguedAndValid method
        $result = $person->logguedAndValid();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testLogguedAndAuthorizedReturnsTrueWhenLoggedInAndAuthorized()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
            'isRegisterProcess' => true,
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn([
                "_id" => $this->dbMock->MongoId($mockData['userId']),
            ]);

        // Mock role canUserLogin method
        $this->roleMock->method('canUserLogin')
            ->with($this->equalTo([
                "_id" => $mockData['userId'],
            ]), $this->equalTo(true))
            ->willReturn([
                "result" => true,
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Call the logguedAndAuthorized method
        $result = $person->logguedAndAuthorized();

        // Assert that the result is true
        $this->assertTrue($result);
    }

    public function testLogguedAndAuthorizedReturnsFalseWhenNotLoggedIn()
    {
        $this->sessionData = [];

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the logguedAndAuthorized method
        $result = $person->logguedAndAuthorized();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testLogguedAndAuthorizedReturnsFalseWhenUserNotFound()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn(null);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the logguedAndAuthorized method
        $result = $person->logguedAndAuthorized();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testLogguedAndAuthorizedReturnsFalseWhenNotAuthorized()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
            'isRegisterProcess' => false,
        ];

        $this->sessionData = $mockData;

        // Mock phdb findOneById method
        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($mockData['userId']))
            ->willReturn([
                "_id" => $this->dbMock->MongoId($mockData['userId']),
            ]);

        // Mock role canUserLogin method
        $this->roleMock->method('canUserLogin')
            ->with($this->equalTo([
                "_id" => $mockData['userId'],
            ]), $this->equalTo(false))
            ->willReturn([
                "result" => false,
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Call the logguedAndAuthorized method
        $result = $person->logguedAndAuthorized();

        // Assert that the result is false
        $this->assertFalse($result);
    }

    public function testSaveUserSessionData()
    {
        $mockData = [];

        $this->sessionData = $mockData;

        $account = [
            "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
            "email" => "test@example.com",
            "name" => "John Doe",
            "username" => "johndoe",
            "slug" => "john-doe",
            "cp" => "12345",
            "address" => [
                "postalCode" => "12345",
                "codeInsee" => "67890",
                "addressCountry" => "US",
            ],
            "roles" => [
                "sourceAdmin" => false,
            ],
            "preferences" => [
                "preference1" => "value1",
                "preference2" => "value2",
            ],
            "lastLoginDate" => time(),
        ];

        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $account["_id"]))
            ->willReturn([
                "_id" => $this->dbMock->MongoId((string) $account["_id"]),
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Mock role isSourceAdmin method
        $this->roleMock->method('isSourceAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));
        // Mock role isSourceAdmin method
        $this->roleMock->method('isUserSuperAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelPreference($this->preferenceMock);

        $person->setModelRole($this->roleMock);

        $person->setModelDocument($this->documentMock);

        $person->saveUserSessionData($account);

        $this->assertSame((string) $account["_id"], $this->sessionData["userId"]);
        $this->assertSame("test@example.com", $this->sessionData["userEmail"]);
        $this->assertSame("John Doe", $this->sessionData["user"]["name"]);
        $this->assertSame("johndoe", $this->sessionData["user"]["username"]);
        $this->assertSame("john-doe", $this->sessionData["user"]["slug"]);
        $this->assertSame("12345", $this->sessionData["user"]["postalCode"]);
        $this->assertSame("67890", $this->sessionData["user"]["codeInsee"]);
        $this->assertSame("US", $this->sessionData["user"]["addressCountry"]);
        $this->assertSame([
            "sourceAdmin" => false,
        ], $this->sessionData["user"]["roles"]);
        $this->assertSame([
            "preference1" => "value1",
            "preference2" => "value2",
        ], $this->sessionData["user"]["preferences"]);
        $this->assertSame(time(), $this->sessionData["user"]["lastLoginDate"]);
        $this->assertSame("http://example.com/profile.jpg", $this->sessionData["user"]["profilImageUrl"]);
        $this->assertSame("http://example.com/profile-medium.jpg", $this->sessionData["user"]["profilMediumImageUrl"]);
        $this->assertSame("http://example.com/profile-thumb.jpg", $this->sessionData["user"]["profilThumbImageUrl"]);
        $this->assertSame("http://example.com/profile-marker.jpg", $this->sessionData["user"]["profilMarkerImageUrl"]);
        $this->assertFalse($this->sessionData["isRegisterProcess"]);
        $this->assertFalse($this->sessionData["userIsAdmin"]);
        $this->assertFalse($this->sessionData["userIsAdminPublic"]);
        $this->assertSame("communecter", $this->sessionData["logguedIntoApp"]);
    }

    public function testSaveUserSessionDataGetByIdNull()
    {
        $mockData = [];

        $this->sessionData = $mockData;

        $account = [
            "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
            "email" => "test@example.com",
            "name" => "John Doe",
            "username" => "johndoe",
            "slug" => "john-doe",
            "cp" => "12345",
            "address" => [
                "postalCode" => "12345",
                "codeInsee" => "67890",
                "addressCountry" => "US",
            ],
            "roles" => [
                "sourceAdmin" => false,
            ],
            "preferences" => [
                "preference1" => "value1",
                "preference2" => "value2",
            ],
            "lastLoginDate" => time(),
        ];

        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $account["_id"]))
            ->willReturn(null);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Mock role isSourceAdmin method
        $this->roleMock->method('isSourceAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));
        // Mock role isSourceAdmin method
        $this->roleMock->method('isUserSuperAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));

        $person->setModelRole($this->roleMock);

        $person->setModelDocument($this->documentMock);

        $person->saveUserSessionData($account);

        // var_dump($mockData);
        $this->assertSame((string) $account["_id"], $this->sessionData["userId"]);
        $this->assertSame("test@example.com", $this->sessionData["userEmail"]);
        $this->assertSame("John Doe", $this->sessionData["user"]["name"]);
        $this->assertSame("johndoe", $this->sessionData["user"]["username"]);
        $this->assertSame("john-doe", $this->sessionData["user"]["slug"]);
        $this->assertSame("12345", $this->sessionData["user"]["postalCode"]);
        $this->assertSame("67890", $this->sessionData["user"]["codeInsee"]);
        $this->assertSame("US", $this->sessionData["user"]["addressCountry"]);
        $this->assertSame([
            "sourceAdmin" => false,
        ], $this->sessionData["user"]["roles"]);
        $this->assertSame([
            "preference1" => "value1",
            "preference2" => "value2",
        ], $this->sessionData["user"]["preferences"]);
        $this->assertSame(time(), $this->sessionData["user"]["lastLoginDate"]);
        $this->assertNull($this->sessionData["user"]["profilImageUrl"]);
        $this->assertNull($this->sessionData["user"]["profilMediumImageUrl"]);
        $this->assertNull($this->sessionData["user"]["profilThumbImageUrl"]);
        $this->assertNull($this->sessionData["user"]["profilMarkerImageUrl"]);
        $this->assertFalse($this->sessionData["isRegisterProcess"]);
        $this->assertFalse($this->sessionData["userIsAdmin"]);
        $this->assertFalse($this->sessionData["userIsAdminPublic"]);
        $this->assertSame("communecter", $this->sessionData["logguedIntoApp"]);
    }

    public function testSaveUserSessionDataGetByIdNoImages()
    {
        $mockData = [];

        $this->sessionData = $mockData;

        $account = [
            "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
            "email" => "test@example.com",
            "name" => "John Doe",
            "username" => "johndoe",
            "slug" => "john-doe",
            "cp" => "12345",
            "address" => [
                "postalCode" => "12345",
                "codeInsee" => "67890",
                "addressCountry" => "US",
            ],
            "roles" => [
                "sourceAdmin" => false,
            ],
            "preferences" => [
                "preference1" => "value1",
                "preference2" => "value2",
            ],
            "lastLoginDate" => time(),
        ];

        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $account["_id"]))
            ->willReturn([
                "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Mock role isSourceAdmin method
        $this->roleMock->method('isSourceAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));
        // Mock role isSourceAdmin method
        $this->roleMock->method('isUserSuperAdmin')
            ->with($this->equalTo($account["roles"]))
            ->will($this->returnCallback(
                fn () => false
            ));

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn([
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ]);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelRole($this->roleMock);

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $person->saveUserSessionData($account);

        // var_dump($mockData);
        $this->assertSame((string) $account["_id"], $this->sessionData["userId"]);
        $this->assertSame("test@example.com", $this->sessionData["userEmail"]);
        $this->assertSame("John Doe", $this->sessionData["user"]["name"]);
        $this->assertSame("johndoe", $this->sessionData["user"]["username"]);
        $this->assertSame("john-doe", $this->sessionData["user"]["slug"]);
        $this->assertSame("12345", $this->sessionData["user"]["postalCode"]);
        $this->assertSame("67890", $this->sessionData["user"]["codeInsee"]);
        $this->assertSame("US", $this->sessionData["user"]["addressCountry"]);
        $this->assertSame([
            "sourceAdmin" => false,
        ], $this->sessionData["user"]["roles"]);
        $this->assertSame([
            "preference1" => "value1",
            "preference2" => "value2",
        ], $this->sessionData["user"]["preferences"]);
        $this->assertIsInt($this->sessionData["user"]["lastLoginDate"]);
        $this->assertSame("http://example.com/profile.jpg", $this->sessionData["user"]["profilImageUrl"]);
        $this->assertSame("http://example.com/profile-medium.jpg", $this->sessionData["user"]["profilMediumImageUrl"]);
        $this->assertSame("http://example.com/profile-thumb.jpg", $this->sessionData["user"]["profilThumbImageUrl"]);
        $this->assertSame("http://example.com/profile-marker.jpg", $this->sessionData["user"]["profilMarkerImageUrl"]);
        $this->assertFalse($this->sessionData["isRegisterProcess"]);
        $this->assertFalse($this->sessionData["userIsAdmin"]);
        $this->assertFalse($this->sessionData["userIsAdminPublic"]);
        $this->assertSame("communecter", $this->sessionData["logguedIntoApp"]);
    }

    public function testGetByIdReturnsPersonDocumentWhenPersonExists()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $personId = $this->dbMock->MongoId('55ed9107e41d75a41a558524');

        $personData = [
            "_id" => $personId,
            "name" => "John Doe",
            // ... other person data
        ];

        date_default_timezone_set('UTC');
        $birthDate = '2000-01-01 00:00:00';
        $timestamp = strtotime($birthDate) * 1000; // Convertit la date en timestamp et le multiplie par 1000 pour obtenir des millisecondes
        $datetime = new DateTime('@' . $timestamp / 1000);
        $datetime->setTimezone(new DateTimeZone('UTC'));

        $personData["birthDate"] = new MongoDateWrapper($datetime->getTimestamp());

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn($personData);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getById((string) $personId);

        $valeurRetourDefault = [
            "address" => [
                "codeInsee" => null,
                "postalCode" => null,
                "addressLocality" => null,
                "streetAddress" => null,
                "addressCountry" => null,
            ],
            'typeSig' => 'people',
        ];

        $personData["birthDate"] = $birthDate;

        $retourTest = array_merge($personData, $imageTest, $valeurRetourDefault);

        $this->assertEquals($retourTest, $result);
    }

    public function testGetByIdReturnsNullWhenPersonDoesNotExist()
    {
        $personId = $this->dbMock->MongoId('55ed9107e41d75a41a558524');

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn(null);

        $valeurRetourDefault = [
            'name' => 'Unknown (deleted)',
            'slug' => 'unknown',
            'type' => 'citoyens',
            'typeSig' => 'citoyens',
        ];

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $result = $person->getById((string) $personId);

        $this->assertEquals($valeurRetourDefault, $result);
    }

    public function testGetByArrayIdReturnsArrayOfPersons()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $arrayId = [$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523')];
        $fields = ['name', 'email'];
        $clearAttribute = true;
        $simpleUser = false;

        $personData = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
            ],
        ];

        $this->dbMock->expects($this->once())
            ->method('find')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                '_id' => [
                    '$in' => $arrayId,
                ],
            ]), $this->equalTo($fields))
            ->willReturn($personData);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getByArrayId($arrayId, $fields, $clearAttribute, $simpleUser);

        $expectedResult = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetByArrayIdReturnsArrayOfPersonsWhenNoFields()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $arrayId = [$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523')];
        $fields = [];
        $clearAttribute = true;
        $simpleUser = false;

        $personData = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
            ],
        ];

        $this->dbMock->expects($this->once())
            ->method('find')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                '_id' => [
                    '$in' => $arrayId,
                ],
            ]), $this->equalTo($fields))
            ->willReturn($personData);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getByArrayId($arrayId, $fields, $clearAttribute, $simpleUser);

        $expectedResult = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetByArrayIdReturnsArrayOfPersonsWhenNoClearAttribute()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $arrayId = [$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523')];
        $fields = ['name', 'email'];
        $clearAttribute = false;
        $simpleUser = false;

        $personData = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
            ],
        ];

        $this->dbMock->expects($this->once())
            ->method('find')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                '_id' => [
                    '$in' => $arrayId,
                ],
            ]), $this->equalTo($fields))
            ->willReturn($personData);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $person->setModelDocument($this->documentMock);

        $result = $person->getByArrayId($arrayId, $fields, $clearAttribute, $simpleUser);

        $expectedResult = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => null,
                    "postalCode" => null,
                    "addressLocality" => null,
                    "streetAddress" => null,
                    "addressCountry" => null,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetByArrayIdReturnsArrayOfPersonsWhenSimpleUser()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $arrayId = [$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523')];
        $fields = ['name', 'email'];
        $clearAttribute = false;
        $simpleUser = true;

        $personData = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
            ],
        ];

        $this->dbMock->expects($this->once())
            ->method('find')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                '_id' => [
                    '$in' => $arrayId,
                ],
            ]), $this->equalTo($fields))
            ->willReturn($personData);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getByArrayId($arrayId, $fields, $clearAttribute, $simpleUser);

        $expectedResult = [
            '55ed9107e41d75a41a558524' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                'name' => 'John Doe',
                'email' => 'john@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => "",
                    "postalCode" => "",
                    "addressLocality" => "",
                    "streetAddress" => "",
                    "addressCountry" => "",
                ],
                "id" => "55ed9107e41d75a41a558524",
                "geo" => [],
            ],
            '55ed9107e41d75a41a558523' => [
                '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                'name' => 'Jane Smith',
                'email' => 'jane@example.com',
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
                'typeSig' => 'people',
                "address" => [
                    "codeInsee" => "",
                    "postalCode" => "",
                    "addressLocality" => "",
                    "streetAddress" => "",
                    "addressCountry" => "",
                ],
                "id" => "55ed9107e41d75a41a558523",
                "geo" => [],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetMinimalUserByIdReturnsMinimalUserInfoWhenPersonIsNull()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');

        $personResult = [
            "_id" => $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "roles" => ["admin"],
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilThumbImageUrl" => "http://example.com/profile_thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile_marker.jpg",
        ];
        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $id), $this->equalTo([
                "id" => 1,
                "name" => 1,
                "username" => 1,
                "email" => 1,
                "roles" => 1,
                "tags" => 1,
                "profilImageUrl" => 1,
                "profilThumbImageUrl" => 1,
                "profilMarkerImageUrl" => 1,
            ]))
            ->willReturn($personResult);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getMinimalUserById((string) $id);

        $expectedResult = [
            "_id" => $id,
            "id" => (string) $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            'typeSig' => 'people',
            "address" => [
                "addressLocality" => "Unknown",
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetMinimalUserByIdReturnsMinimalUserInfoWhenPersonIsNotNull()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');
        $personDefault = [
            "_id" => $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "roles" => ["admin"],
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getMinimalUserById((string) $id, $personDefault);

        $expectedResult = [
            "_id" => $id,
            "id" => (string) $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            'typeSig' => 'people',
            "address" => [
                "addressLocality" => "Unknown",
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetMinimalUserByIdReturnsEmptyArrayWhenPersonIsNullAndNotFoundInDatabase()
    {
        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');
        $personResult = null;

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $id), $this->equalTo([
                "id" => 1,
                "name" => 1,
                "username" => 1,
                "email" => 1,
                "roles" => 1,
                "tags" => 1,
                "profilImageUrl" => 1,
                "profilThumbImageUrl" => 1,
                "profilMarkerImageUrl" => 1,
            ]))
            ->willReturn($personResult);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);
        $result = $person->getMinimalUserById((string) $id);

        $this->assertEquals([], $result);
    }

    public function testGetSimpleUserByIdReturnSimpleUserInfoWhenPersonIsNull()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');
        $personResult = [
            "_id" => $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        // Mock the phdb findOneById method
        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $id), $this->equalTo([
                "id" => 1,
                "name" => 1,
                "username" => 1,
                "email" => 1,
                "shortDescription" => 1,
                "description" => 1,
                "address" => 1,
                "geo" => 1,
                "roles" => 1,
                "tags" => 1,
                "links" => 1,
                "pending" => 1,
                "profilImageUrl" => 1,
                "profilThumbImageUrl" => 1,
                "profilMarkerImageUrl" => 1,
                "profilMediumImageUrl" => 1,
                "numberOfInvit" => 1,
                "updated" => 1,
                "addresses" => 1,
                "slug" => 1,
            ]))
            ->willReturn($personResult);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getSimpleUserById((string) $id);

        $expectedResult = [
            "_id" => $id,
            "id" => (string) $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            'typeSig' => 'people',
            "address" => [],
            "geo" => [],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetSimpleUserByIdReturnSimpleUserInfoWhenPersonIsNotNull()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');
        $personData = [
            "_id" => $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $result = $person->getSimpleUserById((string) $id, $personData);

        $expectedResult = [
            "_id" => $id,
            "id" => (string) $id,
            "name" => "John Doe",
            "username" => "johndoe",
            "email" => "johndoe@example.com",
            "tags" => ["tag1", "tag2"],
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            'typeSig' => 'people',
            "address" => [],
            "geo" => [],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetSimpleUserByIdReturnsEmptyArrayWhenPersonIsNullAndNotFoundInDatabase()
    {
        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $id), $this->equalTo([
                "id" => 1,
                "name" => 1,
                "username" => 1,
                "email" => 1,
                "shortDescription" => 1,
                "description" => 1,
                "address" => 1,
                "geo" => 1,
                "roles" => 1,
                "tags" => 1,
                "links" => 1,
                "pending" => 1,
                "profilImageUrl" => 1,
                "profilThumbImageUrl" => 1,
                "profilMarkerImageUrl" => 1,
                "profilMediumImageUrl" => 1,
                "numberOfInvit" => 1,
                "updated" => 1,
                "addresses" => 1,
                "slug" => 1,
            ]))
            ->willReturn(null);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $result = $person->getSimpleUserById((string) $id);

        $this->assertEquals([], $result);
    }

    public function testGetWhereReturnsArrayOfPersons()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = $this->dbMock->MongoId('55ed9107e41d75a41a558524');

        $params = [
            'name' => 'John Doe',
        ];
        // Mock phdb findAndSort method
        $this->dbMock->method('findAndSort')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($params), $this->equalTo(["created"]))
            ->willReturn([
                (string) $id => [
                    '_id' => $id,
                    'name' => 'John Doe',
                    'created' => '2022-01-01',
                    'pwd' => '123456',
                ],
            ]);

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelPreference($this->preferenceMock);

        // Call the getWhere method
        $result = $person->getWhere($params);

        // Assert that the result is an array
        $this->assertIsArray($result);

        // Assert that the result contains the expected persons
        $this->assertEquals([
            (string) $id => [
                '_id' => $id,
                'name' => 'John Doe',
                'created' => '2022-01-01',
            ],
        ], $result);
    }

    // public function testGetOrganizationsById()
    // {

    //     // Create a new Person instance
    //     $person = new Person([
    //         'db' => $this->dbMock,
    //         'session' => $this->sessionMock,
    //
    //         'arrayHelper' => $this->arrayHelperMock,
    //         'cookieHelper' => $this->cookieHelperMock,
    //         'params' => $this->paramsMock,
    //         'language' => $this->languageMock,
    //         'moduleId' => $this->moduleId,
    //         'classNames' => [
    //             'organizationClass' => Organization::class,
    //         ]
    //     ]);

    //     $imageTest = [
    //         "profilImageUrl" => "http://example.com/profile.jpg",
    //         "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
    //         "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
    //         "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg"
    //     ];

    //     $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

    //     $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
    //         function (array $element, string $elementType, string $userId) {
    //             return $element;
    //         }
    //     ));

    //     $person->setDocument($this->documentMock);

    //     $person->setPreference($this->preferenceMock);


    //     // Set up the test data
    //     $id = '123';
    //     $personData = [
    //     'links' => [
    //         'memberOf' => [
    //             '55ed9107e41d75a41a558524' => ['name' => 'Organization 1'],
    //             '55ed9107e41d75a41a558523' => ['name' => 'Organization 2'],
    //         ],
    //     ],
    // ];
    //     $organization1 = ['_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'), 'name' => 'Organization 1'];
    //     $organization2 = ['_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'), 'name' => 'Organization 2'];

    //     $this->dbMock->expects($this->once())
    //     ->method('findOneById')
    //     ->willReturn($personData);


    //     // Mock the phdb findOne method
    //     $this->dbMock->expects($this->exactly(2))
    //     ->method('findOne')
    //     ->withConsecutive(
    //         [$this->equalTo('organizations'), $this->equalTo(['_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524')])],
    //         [$this->equalTo('organizations'), $this->equalTo(['_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523')])]
    //     )
    //     ->willReturnOnConsecutiveCalls($organization1, $organization2);

    //     // Call the getOrganizationsById method
    //     $result = $person->getOrganizationsById($id);

    //     // Assert that the result is an array
    //     $this->assertIsArray($result);

    //     // Assert that the result contains the expected organizations
    //     $this->assertEquals([$organization1, $organization2], $result);
    // }

    public function testGetOrganizationsById()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
            'classNames' => [
                'organizationClass' => Organization::class,
            ],
        ]);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        // Set up the test data
        $id = '55ed9107e41d75a41a558524';
        $personData = [
            '_id' => $this->dbMock->MongoId($id),
            'links' => [
                'memberOf' => [
                    '55ed9107e41d75a41a558524' => [
                        'name' => 'Organization 1',
                    ],
                    '55ed9107e41d75a41a558523' => [
                        'name' => 'Organization 2',
                    ],
                ],
            ],
        ];
        $organization1 = [
            '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
            'name' => 'Organization 1',
        ];
        $organization2 = [
            '_id' => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
            'name' => 'Organization 2',
        ];

        $organizationData = [
            '55ed9107e41d75a41a558524' => $organization1,
            '55ed9107e41d75a41a558523' => $organization2,
        ];

        $argumentOne = [$this->equalTo('citoyens'), $this->equalTo($this->dbMock->MongoId('55ed9107e41d75a41a558524'))];
        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with(...$argumentOne)
            ->willReturn($personData);

        $argument = [
            $this->equalTo('organizations'),
            $this->equalTo([
                "_id" => [
                    '$in' => [$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523')],
                ],
            ]),
        ];

        $this->dbMock->expects($this->once())
            ->method('find')
            ->with(...$argument)
            ->willReturn($organizationData);

        // Call the getOrganizationsById method
        $result = $person->getOrganizationsById($id);

        // Assert that the result is an array
        $this->assertIsArray($result);

        // Assert that the result contains the expected organizations
        $this->assertEquals([$organization1, $organization2], $result);
    }

    public function testGetPersonLinksByPersonId()
    {
        $mockData = [
            'userId' => '55ed9107e41d75a41a558524',
        ];

        $this->sessionData = $mockData;

        $id = '55ed9107e41d75a41a558524';

        // Mock the necessary dependencies
        $this->elementMock->expects($this->once())
            ->method('getElementSimpleById')
            ->with($this->equalTo($id), $this->equalTo(PersonInterface::COLLECTION), $this->isNull(), $this->equalTo(["links"]))
            ->willReturn([
                "_id" => $this->dbMock->MongoId($id),
                "links" => [
                    "memberOf" => [
                        "55ed9107e41d75a41a558524" => [
                            "type" => "organizations",
                        ],
                        "55ed9107e41d75a41a558523" => [
                            "type" => "organizations",
                        ],
                    ],
                    "projects" => [
                        "55ed9107e41d75a41a558522" => [
                            "type" => "projects",
                        ],
                        "55ed9107e41d75a41a558521" => [
                            "type" => "projects",
                        ],
                    ],
                    "events" => [
                        "55ed9107e41d75a41a558520" => [
                            "type" => "events",
                        ],
                        "55ed9107e41d75a41a558519" => [
                            "type" => "events",
                        ],
                    ],
                    "friends" => [
                        "55ed9107e41d75a41a558518" => [
                            "type" => "citoyens",
                        ],
                        "55ed9107e41d75a41a558519" => [
                            "type" => "citoyens",
                        ],
                    ],
                    "follows" => [
                        "55ed9107e41d75a41a558517" => [
                            "type" => "organizations",
                        ],
                        "55ed9107e41d75a41a558516" => [
                            "type" => "organizations",
                        ],
                    ],
                    "invalid_key" => [
                        "invalid_id" => [
                            "type" => "invalid_type",
                        ],
                    ],
                ],
            ]);

        $this->elementMock->expects($this->exactly(4))
            ->method('getElementSimpleByIds')
            // ->withConsecutive(
            //     [$this->equalTo([$this->dbMock->MongoId('55ed9107e41d75a41a558518'), $this->dbMock->MongoId('55ed9107e41d75a41a558519')]), $this->equalTo('citoyens'), $this->isNull(), $this->equalTo([
            //         "name", "collection", "slug", "username", "shortDescription", "address", "type", "profilThumbImageUrl",
            //         "profilImageUrl", "profilMediumImageUrl", "preferences", "hasRC", "endDate", "startDate", "email"
            //     ])],
            //     [$this->equalTo([$this->dbMock->MongoId('55ed9107e41d75a41a558524'), $this->dbMock->MongoId('55ed9107e41d75a41a558523'), $this->dbMock->MongoId('55ed9107e41d75a41a558517'), $this->dbMock->MongoId('55ed9107e41d75a41a558516')]), $this->equalTo('organizations'), $this->isNull(), $this->equalTo([
            //         "name", "collection", "slug", "username", "shortDescription", "address", "type", "profilThumbImageUrl",
            //         "profilImageUrl", "profilMediumImageUrl", "preferences", "hasRC", "endDate", "startDate", "email"
            //     ])],
            //     [$this->equalTo([$this->dbMock->MongoId('55ed9107e41d75a41a558522'), $this->dbMock->MongoId('55ed9107e41d75a41a558521')]), $this->equalTo('projects'), $this->isNull(), $this->equalTo([
            //         "name", "collection", "slug", "username", "shortDescription", "address", "type", "profilThumbImageUrl",
            //         "profilImageUrl", "profilMediumImageUrl", "preferences", "hasRC", "endDate", "startDate", "email"
            //     ])],
            //     [$this->equalTo([$this->dbMock->MongoId('55ed9107e41d75a41a558520'), $this->dbMock->MongoId('55ed9107e41d75a41a558519')]), $this->equalTo('events'), $this->isNull(), $this->equalTo([
            //         "name", "collection", "slug", "username", "shortDescription", "address", "type", "profilThumbImageUrl",
            //         "profilImageUrl", "profilMediumImageUrl", "preferences", "hasRC", "endDate", "startDate", "email"
            //     ])]
            // )
            ->willReturnOnConsecutiveCalls(
                [
                    "55ed9107e41d75a41a558518" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558518'),
                        "name" => "Citoyen 1",
                    ],
                    "55ed9107e41d75a41a558519" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558519'),
                        "name" => "Citoyen 2",
                    ],
                ],
                [
                    "55ed9107e41d75a41a558524" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                        "name" => "Organization 1",
                    ],
                    "55ed9107e41d75a41a558523" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                        "name" => "Organization 2",
                    ],
                    "55ed9107e41d75a41a558517" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558517'),
                        "name" => "Organization 3",
                    ],
                    "55ed9107e41d75a41a558516" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558516'),
                        "name" => "Organization 4",
                    ],
                ],
                [
                    "55ed9107e41d75a41a558522" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558522'),
                        "name" => "Project 1",
                    ],
                    "55ed9107e41d75a41a558521" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558521'),
                        "name" => "Project 2",
                    ],
                ],
                [
                    "55ed9107e41d75a41a558520" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558520'),
                        "name" => "Event 1",
                    ],
                    "55ed9107e41d75a41a558519" => [
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558519'),
                        "name" => "Event 2",
                    ],
                ]
            );

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
            'classNames' => [
                'organizationClass' => Organization::class,
                'projectClass' => Project::class,
                'eventClass' => Event::class,
            ],
        ]);

        $person->setModelElement($this->elementMock);
        $person->setModelCostum($this->costumMock);

        // Call the method under test
        $result = $person->getPersonLinksByPersonId($id);

        // Assert the expected result
        $expectedResult = [
            "citoyens" => [
                "55ed9107e41d75a41a558518" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558518'),
                    "name" => "Citoyen 1",
                    "type" => "citoyens",
                ],
                "55ed9107e41d75a41a558519" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558519'),
                    "name" => "Citoyen 2",
                    "type" => "citoyens",
                ],
            ],
            "organizations" => [
                "55ed9107e41d75a41a558524" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558524'),
                    "name" => "Organization 1",
                ],
                "55ed9107e41d75a41a558523" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                    "name" => "Organization 2",
                ],
            ],
            "projects" => [
                "55ed9107e41d75a41a558522" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558522'),
                    "name" => "Project 1",
                ],
                "55ed9107e41d75a41a558521" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558521'),
                    "name" => "Project 2",
                ],
            ],
            "events" => [
                "55ed9107e41d75a41a558520" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558520'),
                    "name" => "Event 1",
                ],
                "55ed9107e41d75a41a558519" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558519'),
                    "name" => "Event 2",
                ],
            ],
            "follows" => [
                "55ed9107e41d75a41a558517" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558517'),
                    "name" => "Organization 3",
                    "type" => "organizations",
                    "isFollowed" => true,
                ],
                "55ed9107e41d75a41a558516" => [
                    "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558516'),
                    "name" => "Organization 4",
                    "type" => "organizations",
                    "isFollowed" => true,
                ],
            ],
        ];

        $this->assertEquals($expectedResult, $result);
    }

    private function isStringLength($expectedLength)
    {
        return fn ($string) => is_string($string) && strlen($string) === $expectedLength;
    }

    private function isRecentMongoDate($timeMarginInSeconds = 10)
    {
        $currentTime = time();
        return function ($mongoDate) use ($currentTime, $timeMarginInSeconds) {
            if ($mongoDate instanceof UTCDateTime) {
                $mongoTimestamp = $mongoDate->toDateTime()->getTimestamp();
                return ($mongoTimestamp >= $currentTime - $timeMarginInSeconds) &&
                       ($mongoTimestamp <= $currentTime + $timeMarginInSeconds);
            }
            return false;
        };
    }

    public function testCreateAndInviteErrorParam()
    {
        // Prepare the test data
        $param = [
            // Add your test data here
        ];

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelCostum($this->costumMock);

        // Perform the method call
        $result = $person->createAndInvite($param);

        $expectedResult = [
            'result' => false,
            'msg' => 'Problem inviting the new person: name, email, and invitedBy are required',
        ];

        $this->assertEquals($expectedResult, $result, 'param array empty');

        // Prepare the test data
        $param = [
            'name' => 'John Doe',
        ];

        // Perform the method call
        $result = $person->createAndInvite($param);

        $expectedResult = [
            'result' => false,
            'msg' => 'Problem inviting the new person: name, email, and invitedBy are required',
        ];

        $this->assertEquals($expectedResult, $result, 'invitedBy and email is not set in param array');

        // Prepare the test data
        $param = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'invitedBy' => '55ed9107e41d75a41a558524',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($param['invitedBy']), $this->equalTo(["_id", "name"]))
            ->willReturn(null);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);

        // Perform the method call
        $result = $person->createAndInvite($param);

        $expectedResult = [
            'result' => false,
            'msg' => 'the person who invited cannot be found',
        ];

        $this->assertEquals($expectedResult, $result, 'the person who invited cannot be found');
    }

    public function testCreateAndInviteInsertReturnNoId()
    {
        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Prepare the test data
        $param = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'invitedBy' => '55ed9107e41d75a41a558524',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($param['invitedBy']), $this->equalTo(["_id", "name"]))
            ->willReturn([
                '_id' => '55ed9107e41d75a41a558524',
            ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelCostum($this->costumMock);

        // Perform the method call
        $result = $person->createAndInvite($param);

        $expectedResult = [
            'result' => false,
            'msg' => 'Problem inserting the new person',
        ];

        $this->assertEquals($expectedResult, $result, 'Problem inserting the new person');
    }

    public function testCreateAndInvite()
    {
        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Prepare the test data
        $param = [
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'invitedBy' => '55ed9107e41d75a41a558524',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo($param['invitedBy']), $this->equalTo(["_id", "name"]))
            ->willReturn([
                '_id' => '55ed9107e41d75a41a558524',
            ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelCostum($this->costumMock);

        $userData = [
            'username' => $this->isStringLength(32),
            'pending' => true,
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'roles' => [
                'tobeactivated' => true,
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => 'citoyens',
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => $this->isStringLength(32),
        ];
        $expectedData = $userData;
        $argumentInsert = [$this->equalTo('citoyens'), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['username'] = 'yxrdzn1cvqhlou8m69kp573ibs4g0fwe';
        $returnInsert['slug'] = 'yxrdzn1cvqhlou8m69kp573ibs4g0fwe';
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        // Perform the method call
        $result = $person->createAndInvite($param);

        $expectedResult = [
            'result' => true,
            'msg' => 'You are now communnected',
            'person' => $returnInsert,
            'id' => '55ed9107e41d75a41a558523',
        ];

        $this->assertEquals($expectedResult, $result, 'create and invite ok');
    }

    public function testInsertPersonArrayEmptyError()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : name is missing");

        $personData = [

        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertPersonArrayEmailEmptyError()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : email is missing");

        $personData = [
            'name' => 'John Doe',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertPersonArrayUsernameEmptyError()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : username is missing");

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertPersonArrayPwdEmptyError()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : pwd is missing");

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertPersonInsertError()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person");

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
            'pwd' => 'password123',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelCostum($this->costumMock);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertModeNormal()
    {
        // todo : faire le test si address est présent

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
            'pwd' => 'password123',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // email is valide
        $this->dataValidatorMock->method('email')
            ->with($this->equalTo($personData['email']))
            ->willReturn("");

        $this->dbMock->expects($this->exactly(2))
            ->method('findOne')
        // ->withConsecutive(
        //     // unique email verifie getPersonByEmail()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(["email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim($personData['email'])) . '$/i')])],
        //     // isUniqueUsername()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(array("username" => $personData['username'])), $this->equalTo(array("_id","username"))]
        // )
            ->willReturnOnConsecutiveCalls(null, null);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelAuthorisation($this->authorisationMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelCostum($this->costumMock);

        $userData = [
            'username' => 'johndoe',
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'tobeactivated' => true,
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => PersonInterface::COLLECTION,
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => 'johndoe',
        ];

        $expectedData = $userData;
        $argumentInsert = [$this->equalTo(PersonInterface::COLLECTION), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        // authorisation to update field
        $this->authorisationMock->method('canEditItem')
            ->with($this->equalTo('55ed9107e41d75a41a558523'), $this->equalTo(PersonInterface::COLLECTION), $this->equalTo('55ed9107e41d75a41a558523'))
            ->willReturn(true);

        $expectedResult = [
            'result' => true,
            'msg' => 'You are now communnected',
            'person' => $returnInsert,
            'id' => '55ed9107e41d75a41a558523',
        ];

        // Perform the method call
        $result = $person->insert($personData, $mode, $inviteCode, $forced);

        $this->assertEquals($expectedResult, $result, 'insert ok');
    }

    public function testInsertModeNormalEmailExist()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : a person with this email already exists in the plateform");

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
            'pwd' => 'password123',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);

        // email is valide
        $this->dataValidatorMock->method('email')
            ->with($this->equalTo($personData['email']))
            ->willReturn("");

        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->willReturn([
                '_id' => '55ed9107e41d75a41a558523',
            ]);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertModeNormalUsernameExist()
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Problem inserting the new person : a person with this username already exists in the plateform");

        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
            'pwd' => 'password123',
        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);

        // email is valide
        $this->dataValidatorMock->method('email')
            ->with($this->equalTo($personData['email']))
            ->willReturn("");

        $this->dbMock->expects($this->exactly(2))
            ->method('findOne')
        // ->withConsecutive(
        //     // unique email verifie getPersonByEmail()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(["email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim($personData['email'])) . '$/i')])],
        //     // isUniqueUsername()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(array("username" => $personData['username'])), $this->equalTo(array("_id","username"))]
        // )
            ->willReturnOnConsecutiveCalls(null, [
                '_id' => '55ed9107e41d75a41a558523',
            ]);

        $result = $person->insert($personData, $mode, $inviteCode, $forced);
    }

    public function testInsertModeNormalWithAddress()
    {
        // todo : faire la version Person::REGISTER_MODE_SERVICE
        $personData = [
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'username' => 'johndoe',
            'pwd' => 'password123',
            'address' => [
                "addressCountry" => "RE",
                "postalCode" => "97434",
            ],

        ];

        $mode = PersonInterface::REGISTER_MODE_NORMAL;
        $inviteCode = null;
        $forced = null;

        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // email is valide
        $this->dataValidatorMock->method('email')
            ->with($this->equalTo($personData['email']))
            ->willReturn("");

        $this->dbMock->expects($this->exactly(2))
            ->method('findOne')
        // ->withConsecutive(
        //     // unique email verifie getPersonByEmail()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(["email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim($personData['email'])) . '$/i')])],
        //     // isUniqueUsername()
        //     [$this->equalTo(Person::COLLECTION), $this->equalTo(array("username" => $personData['username'])), $this->equalTo(array("_id","username"))]
        // )
            ->willReturnOnConsecutiveCalls(null, null);

        $this->importMock->method('getAndCheckAddressForEntity')
            ->with($this->equalTo($personData['address']))
            ->willReturn([
                "result" => true,
                "address" => [
                    "addressCountry" => "RE",
                    "postalCode" => "97434",
                    "addressLocality" => "ST GILLES LES BAINS",
                    "streetAddress" => "4 chemin des casques",
                    "@type" => "PostalAddress",
                    "codeInsee" => "97415",
                    "level1" => "58be4af494ef47df1d0ddbcc",
                    "level1Name" => "Réunion",
                    "level3" => "58be4af494ef47df1d0ddbcc",
                    "level3Name" => "Réunion",
                    "level4" => "58be4af494ef47df1d0ddbcc",
                    "level4Name" => "Réunion",
                    "localityId" => "54c0965cf6b95c141800a518",
                ],
                "geo" => [
                    "latitude" => -21.0538749,
                    "longitude" => 55.2286827,
                    "@type" => "GeoCoordinates",
                ],
                "geoPosition" => [
                    "type" => "Point",
                    "float" => true,
                    "coordinates" => [floatval(55.2286827), floatval(-21.0538749)],
                ],
                "saveCities" => null,
            ]);

        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelAuthorisation($this->authorisationMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelImport($this->importMock);
        $person->setModelCostum($this->costumMock);

        $userData = [
            'username' => 'johndoe',
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'tobeactivated' => true,
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => PersonInterface::COLLECTION,
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => 'johndoe',
            "address" => [
                "addressCountry" => "RE",
                "postalCode" => "97434",
                "addressLocality" => "ST GILLES LES BAINS",
                "streetAddress" => "4 chemin des casques",
                "@type" => "PostalAddress",
                "codeInsee" => "97415",
                "level1" => "58be4af494ef47df1d0ddbcc",
                "level1Name" => "Réunion",
                "level3" => "58be4af494ef47df1d0ddbcc",
                "level3Name" => "Réunion",
                "level4" => "58be4af494ef47df1d0ddbcc",
                "level4Name" => "Réunion",
                "localityId" => "54c0965cf6b95c141800a518",
            ],
            "geo" => [
                "latitude" => -21.0538749,
                "longitude" => 55.2286827,
                "@type" => "GeoCoordinates",
            ],
            "geoPosition" => [
                "type" => "Point",
                "float" => true,
                "coordinates" => [floatval(55.2286827), floatval(-21.0538749)],
            ],
        ];

        $expectedData = $userData;
        $argumentInsert = [$this->equalTo(PersonInterface::COLLECTION), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        // authorisation to update field
        $this->authorisationMock->method('canEditItem')
            ->with($this->equalTo('55ed9107e41d75a41a558523'), $this->equalTo(PersonInterface::COLLECTION), $this->equalTo('55ed9107e41d75a41a558523'))
            ->willReturn(true);

        $expectedResult = [
            'result' => true,
            'msg' => 'You are now communnected',
            'person' => $returnInsert,
            'id' => '55ed9107e41d75a41a558523',
        ];

        // Perform the method call
        $result = $person->insert($personData, $mode, $inviteCode, $forced);

        $this->assertEquals($expectedResult, $result, 'insert ok');
    }

    public function testGetPersonByEmail(): void
    {
        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 1: Empty email
        $email = '';
        $result = $person->getPersonByEmail($email);
        $this->assertNull($result);

        // Test case 2: Email not found in the database
        $email = 'nonexistent@example.com';
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'email' => $this->dbMock->MongoRegex('/^' . preg_quote(trim($email)) . '$/i'),
            ])
            ->willReturn(null);
        $result = $person->getPersonByEmail($email);
        $this->assertNull($result);
    }

    public function testGetPersonByEmailFound(): void
    {
        // Create a new Person instance
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 3: Email found in the database
        $email = 'existing@example.com';
        $personData = [
            'name' => 'John Doe',
            'email' => $email,
        ];
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'email' => $this->dbMock->MongoRegex('/^' . preg_quote(trim($email)) . '$/i'),
            ])
            ->willReturn($personData);
        $result = $person->getPersonByEmail($email);
        $this->assertEquals($personData, $result);
    }

    public function testIsUniqueUsername(): void
    {
        $username = 'john_doe';
        // Create a new Person instance and set the mock PhdbDiInterface
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'username' => $username,
            ])
            ->willReturn(null);

        // Call the isUniqueUsername method and assert the result
        $result = $person->isUniqueUsername($username);
        $this->assertTrue($result);
    }

    public function testIsUniqueUsernameNotUnique(): void
    {
        $username = 'john_doe';
        // Create a new Person instance and set the mock PhdbDiInterface
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'username' => $username,
            ])
            ->willReturn([
                '_id' => '55ed9107e41d75a41a558523',
            ]);

        // Call the isUniqueUsername method and assert the result
        $result = $person->isUniqueUsername($username);
        $this->assertFalse($result);
    }

    public function testUpdatePersonFieldEmailisEmptyError(): void
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("L'email is missing");
        $personFieldName = 'email';
        $personFieldValue = '';
        $personId = '55ed9107e41d75a41a558524';
        $userId = '55ed9107e41d75a41a558524';

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // canEditItem
        $this->authorisationMock->expects($this->once())
            ->method('canEditItem')
            ->with($userId, PersonInterface::COLLECTION, $personId)
            ->willReturn(true);

        $person->setModelAuthorisation($this->authorisationMock);

        // getCollectionFieldNameAndValidate
        $this->dataValidatorMock->expects($this->once())
            ->method('getCollectionFieldNameAndValidate')
            ->with($person->dataBinding, $personFieldName, $personFieldValue)
            ->willReturn($personFieldName);

        $person->setModelDataValidator($this->dataValidatorMock);

        // Call the method under test
        $result = $person->updatePersonField($personId, $personFieldName, $personFieldValue, $userId);
    }

    public function testUpdatePersonField(): void
    {
        // todo : faire le test du field address
        // todo : faire le test du field tags
        // todo : tester au retour session en fonction des fields

        $personFieldName = 'name';
        $personFieldValue = 'john doe';
        $personId = '55ed9107e41d75a41a558524';
        $userId = '55ed9107e41d75a41a558524';

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // canEditItem
        $this->authorisationMock->expects($this->once())
            ->method('canEditItem')
            ->with($userId, PersonInterface::COLLECTION, $personId)
            ->willReturn(true);

        $person->setModelAuthorisation($this->authorisationMock);

        // getCollectionFieldNameAndValidate
        $this->dataValidatorMock->expects($this->once())
            ->method('getCollectionFieldNameAndValidate')
            ->with($person->dataBinding, $personFieldName, $personFieldValue)
            ->willReturn($personFieldName);

        $person->setModelDataValidator($this->dataValidatorMock);

        // Set up the expectations for the phdb mock
        $this->dbMock->expects($this->once())
            ->method('update')
            ->with(
                PersonInterface::COLLECTION,
                [
                    '_id' => $this->dbMock->MongoId($personId),
                ],
                [
                    '$set' => [
                        $personFieldName => $personFieldValue,
                    ],
                ]
            );

        // Call the method under test
        $result = $person->updatePersonField($personId, $personFieldName, $personFieldValue, $userId);

        // Assert the result
        $this->assertTrue($result['result']);
        $this->assertNull($result['user']);
        $this->assertEquals($personFieldName, $result['personFieldName']);
        $this->assertEquals('The person has been updated', $result['msg']);
    }

    public function testUpdatePersonFieldBirthDateIsEmptyError(): void
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Error updating the Person : birthDate is not well formated !");

        $personFieldName = 'birthDate';
        $personFieldValue = '';
        $personId = '55ed9107e41d75a41a558524';
        $userId = '55ed9107e41d75a41a558524';

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // canEditItem
        $this->authorisationMock->expects($this->once())
            ->method('canEditItem')
            ->with($userId, PersonInterface::COLLECTION, $personId)
            ->willReturn(true);

        $person->setModelAuthorisation($this->authorisationMock);

        // getCollectionFieldNameAndValidate
        $this->dataValidatorMock->expects($this->once())
            ->method('getCollectionFieldNameAndValidate')
            ->with($person->dataBinding, $personFieldName, $personFieldValue)
            ->willReturn($personFieldName);

        $person->setModelDataValidator($this->dataValidatorMock);

        // Call the method under test
        $result = $person->updatePersonField($personId, $personFieldName, $personFieldValue, $userId);
    }

    public function testLoginWithValidCredentials(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);
        $person->setModelCO2Stat($this->CO2StatMock);

        // canEditItem
        $this->authorisationMock->expects($this->once())
            ->method('canEditItem')
            ->with($personId, PersonInterface::COLLECTION, $personId)
            ->willReturn(true);

        $person->setModelAuthorisation($this->authorisationMock);

        // getCollectionFieldNameAndValidate
        $this->dataValidatorMock->expects($this->once())
            ->method('getCollectionFieldNameAndValidate')
            ->with($person->dataBinding, "lastLoginDate", time())
            ->willReturn("lastLoginDate");

        $person->setModelDataValidator($this->dataValidatorMock);

        // Mock the phdb->findOne method to return a valid account
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(
                PersonInterface::COLLECTION,
                [
                    '$or' => [
                        [
                            'email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i'),
                        ],
                        [
                            'username' => $emailOrUsername,
                        ],
                    ],
                ]
            )
            ->willReturn([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
            ]);

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        // Assert the expected result
        $expectedResult = [
            'result' => true,
            'id' => $personId,
            'isCommunected' => false,
            'account' => [
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
            ],
            'msg' => 'Vous êtes maintenant identifié : bienvenue sur communecter.',
        ];
        $this->assertEquals($expectedResult, $result);
    }

    // ...

    public function testLoginWithEmailNotFound(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $pwd = 'password';

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Mock the phdb->findOne method to return null (account not found)
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->willReturn(null);

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        // Assert the expected result
        $expectedResult = [
            'result' => false,
            'msg' => 'emailNotFound',
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testLoginWithEmailOrPwdEmpty(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $pwd = '';

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        // Assert the expected result
        $expectedResult = [
            'result' => false,
            'msg' => 'Cette requête ne peut aboutir. Merci de bien vouloir réessayer en complétant les champs nécessaires',
        ];
        $this->assertEquals($expectedResult, $result);
    }

    public function testLoginWithValidCredentialsPending(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);
        $canUserLoginResult = [
            "result" => false,
            "pendingUserId" => (string) $personId,
            "pendingUserEmail" => $emailOrUsername,
            "msg" => "accountPending",
        ];

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Mock the phdb->findOne method to return a valid account
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(
                PersonInterface::COLLECTION,
                [
                    '$or' => [
                        [
                            'email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i'),
                        ],
                        [
                            'username' => $emailOrUsername,
                        ],
                    ],
                ]
            )
            ->willReturn([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
                'pending' => true,
            ]);

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn($canUserLoginResult);

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        $this->assertEquals($canUserLoginResult, $result);
    }

    public function testLoginWithValidCredentialsRoleTobeactivated(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);
        $canUserLoginResult = [
            "result" => false,
            "id" => $personId,
            "msg" => "notValidatedEmail",
        ];

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Mock the phdb->findOne method to return a valid account
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(
                PersonInterface::COLLECTION,
                [
                    '$or' => [
                        [
                            'email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i'),
                        ],
                        [
                            'username' => $emailOrUsername,
                        ],
                    ],
                ]
            )
            ->willReturn([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
                'roles' => [
                    'tobeactivated' => true,
                ],
            ]);

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn($canUserLoginResult);

        // $this->roleMock checkUserRoles : $roles["tobeactivated"]

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        $this->assertEquals($canUserLoginResult, $result);
    }

    public function testLoginWithValidCredentialsIsBanned(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelRole($this->roleMock);

        // Mock the phdb->findOne method to return a valid account
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(
                PersonInterface::COLLECTION,
                [
                    '$or' => [
                        [
                            'email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i'),
                        ],
                        [
                            'username' => $emailOrUsername,
                        ],
                    ],
                ]
            )
            ->willReturn([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
                'roles' => [
                    'isBanned' => true,
                ],
            ]);

        $canUserLoginResult = [
            "result" => false,
            "id" => $personId,
            "msg" => "Your account has been certified as fraudulent towards the policies of respect",
        ];

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn($canUserLoginResult);

        // $this->roleMock checkUserRoles : $roles["isBanned"]

        // Call the login method
        $result = $person->login($emailOrUsername, $pwd);

        $this->assertEquals($canUserLoginResult, $result);
    }

    public function testLoginAuthTokenError(): void
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 1: Empty email and password
        $result = $person->loginAuthToken(null, null);
        $this->assertFalse($result['result']);
        $this->assertEquals("Cette requête ne peut aboutir. Merci de bien vouloir réessayer en complétant les champs nécessaires", $result['msg']);

        // Test case 2: Email not found
        $this->dbMock->method('findOne')->willReturn(null);
        $result = $person->loginAuthToken("nonexistent@example.com", "password");
        $this->assertFalse($result['result']);
        $this->assertEquals("emailNotFound", $result['msg']);
    }

    public function testLoginAuthToken(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);
        $tokenName = "comobi";

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 3: Valid login
        $this->dbMock->expects($this->exactly(2))
            ->method('findOne')
        // ->withConsecutive(
        //     [
        //         $this->equalTo(Person::COLLECTION),
        //         $this->equalTo([
        //             '$or' => [
        //                 ['email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i')],
        //                 ['username' => $emailOrUsername],
        //             ],
        //         ])
        //     ],
        //     [
        //         $this->equalTo(Person::COLLECTION),
        //         $this->equalTo([
        //             "_id" => $this->dbMock->MongoId($personId),
        //             'loginTokens.name' => $tokenName,
        //             'loginTokens.type' => 'personalAccessToken',
        //         ])
        //     ]
        // )
            ->willReturnOnConsecutiveCalls([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
            ], null);

        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $person->setModelRole($this->roleMock);

        $result = $person->loginAuthToken($emailOrUsername, $pwd);

        $this->assertTrue($result['result']);
        $this->assertEquals($personId, $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
        $this->assertIsString($result['token']);
    }

    public function testLoginAuthTokenTokenExist(): void
    {
        // Set up the test data
        $emailOrUsername = 'test@example.com';
        $personId = '55ed9107e41d75a41a558524';
        $pwd = 'password';
        $pwdHash = hash('sha256', $personId . $pwd);
        $tokenName = "comobi";

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 3: Valid login
        $this->dbMock->expects($this->exactly(2))
            ->method('findOne')
        // ->withConsecutive(
        //     [
        //         $this->equalTo(Person::COLLECTION),
        //         $this->equalTo([
        //             '$or' => [
        //                 ['email' => $this->dbMock->MongoRegex('/^' . $emailOrUsername . '$/i')],
        //                 ['username' => $emailOrUsername],
        //             ],
        //         ])
        //     ],
        //     [
        //         $this->equalTo(Person::COLLECTION),
        //         $this->equalTo([
        //             "_id" => $this->dbMock->MongoId($personId),
        //             'loginTokens.name' => $tokenName,
        //             'loginTokens.type' => 'personalAccessToken',
        //         ])
        //     ]
        // )
            ->willReturnOnConsecutiveCalls([
                '_id' => $this->dbMock->MongoId($personId),
                'email' => $emailOrUsername,
                'pwd' => $pwdHash,
            ], [
                "_id" => $this->dbMock->MongoId($personId),
            ]);

        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $person->setModelRole($this->roleMock);

        $result = $person->loginAuthToken($emailOrUsername, $pwd);

        $this->assertTrue($result['result']);
        $this->assertEquals($personId, $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
    }

    public function testServiceOauthWithNoServiceDecrypt()
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $serviceDecrypt = null;

        $expectedResult = [
            "result" => false,
            "msg" => "no serviceDecrypt",
        ];

        $actualResult = $person->serviceOauth($serviceDecrypt);

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testServiceOauthWithNoServiceDecryptServiceName()
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $serviceDecrypt = [
            // Provide service data without serviceName here
        ];

        $expectedResult = [
            "result" => false,
            "msg" => "no serviceDecrypt",
        ];

        $actualResult = $person->serviceOauth($serviceDecrypt);

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testServiceOauthWithInvalidServiceData()
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $serviceDecrypt = [
            "serviceName" => "ademe",
        ];

        $expectedResult = [
            "result" => false,
            "msg" => "Invalid service data",
        ];

        $actualResult = $person->serviceOauth($serviceDecrypt);

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testServiceOauthWithValidServiceData()
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelCostum($this->costumMock);

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $serviceDecrypt = [
            "serviceName" => "ademe",
            "serviceData" => [
                "id" => "123",
                "email" => "johndoe@example.com",
                "username" => "johndoe",
                "name" => "John Doe",
            ],
        ];

        $userData = [
            'username' => "johndoe",
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => 'citoyens',
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => "johndoe",
            'services' => [
                'ademe' => [
                    'id' => '123',
                    'username' => 'johndoe',
                    'email' => 'johndoe@example.com',
                ],
            ],
        ];
        $expectedData = $userData;
        $argumentInsert = [$this->equalTo('citoyens'), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        $this->dbMock->expects($this->exactly(5))
            ->method('findOne')
            ->withConsecutive(
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => "johndoe@example.com",
                    ])],
                // unique email verifie getPersonByEmail()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim('johndoe@example.com')) . '$/i'),
                    ])],
                // isUniqueUsername()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "username" => "johndoe",
                    ])],
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null, [
                "_id" => "55ed9107e41d75a41a558523",
            ]);

        $result = $person->serviceOauth($serviceDecrypt);

        $this->assertTrue($result['result']);
        $this->assertEquals('55ed9107e41d75a41a558523', $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
    }

    public function testServiceOauthWithValidServiceDataToken()
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelCostum($this->costumMock);

        // Mock the role->canUserLogin method to return a successful result
        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $serviceDecrypt = [
            "serviceName" => "ademe",
            "serviceData" => [
                "id" => "123",
                "email" => "johndoe@example.com",
                "username" => "johndoe",
                "name" => "John Doe",
            ],
        ];

        $userData = [
            'username' => "johndoe",
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => 'citoyens',
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => "johndoe",
            'services' => [
                'ademe' => [
                    'id' => '123',
                    'username' => 'johndoe',
                    'email' => 'johndoe@example.com',
                ],
            ],
        ];
        $expectedData = $userData;
        $argumentInsert = [$this->equalTo('citoyens'), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        $this->dbMock->expects($this->exactly(6))
            ->method('findOne')
            ->withConsecutive(
                // est ce que le service existe deja pour un user
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // si le service n'existe pas pour un user est ce que l'email existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => "johndoe@example.com",
                    ])],
                // unique email verifie getPersonByEmail()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim('johndoe@example.com')) . '$/i'),
                    ])],
                // isUniqueUsername()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "username" => "johndoe",
                    ])],
                // est ce que l'user à bine été créer
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // est ce que le token existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                        'loginTokens.name' => 'comobi',
                        'loginTokens.type' => 'personalAccessToken',
                    ])],
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null, [
                "_id" => "55ed9107e41d75a41a558523",
            ], null);
        // 1 - le service n'existe pas
        // 2 - l'email n'existe pas
        // 3 - test des valeur pour l'insert : email ne doit pas être deja pris
        // 4 - test des valeur pour l'insert : username ne doit pas être deja pris
        // 5 - on test que l'user avec le service à bien été créer par l'insert
        // 6 - vu qu'il y a un tokenName on test si il y a deja un token avec ce nom

        // todo: faire les autres versions

        $result = $person->serviceOauth($serviceDecrypt, "comobi");

        $this->assertTrue($result['result']);
        $this->assertEquals('55ed9107e41d75a41a558523', $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
        $this->assertIsString($result['token']);
    }

    public function testLoginAuthServiceTokenError(): void
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Test case 1: Service is empty
        $service = "";
        $tokenName = null;
        $expectedResult = [
            "result" => false,
            "msg" => "service is empty",
        ];
        $actualResult = $person->loginAuthServiceToken($service, $tokenName);
        $this->assertEquals($expectedResult, $actualResult);

        // Test case 2: no service decrypt
        $service = "test";
        $tokenName = "comobi";
        // Add your expected result here
        $expectedResult = [
            "result" => false,
            "msg" => "serviceDecrypt is empty",
        ];
        $actualResult = $person->loginAuthServiceToken($service, $tokenName);
        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testLoginAuthServiceTokenServiceDataEmptyError(): void
    {
        $this->params = [
            "passphrase" => "test",
        ];
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $service = [
            'userId' => '55ed9107e41d75a41a558523',
            'serviceName' => 'ademe',
            'serviceData' => [],
        ];
        $token = $person->addToken('55ed9107e41d75a41a558523', 'comobi');
        if (isset($token) && ! empty($token)) {
            $service["token"] = $token;
        }
        // Test case 1: Service is empty
        $serviceEncrypt = $person->cryptoJsAesEncrypt("test", $service);
        $tokenName = "comobi";
        $expectedResult = [
            "result" => false,
            "msg" => "Invalid service data",
        ];
        $actualResult = $person->loginAuthServiceToken($serviceEncrypt, $tokenName);
        $this->assertEquals($expectedResult, $actualResult);
    }

    public function testLoginAuthServiceToken(): void
    {
        $this->params = [
            "passphrase" => "test",
        ];
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelCostum($this->costumMock);

        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $serviceDecrypt = [
            "serviceName" => "ademe",
            "serviceData" => [
                "id" => "123",
                "email" => "johndoe@example.com",
                "username" => "johndoe",
                "name" => "John Doe",
            ],
        ];

        $userData = [
            'username' => "johndoe",
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => 'citoyens',
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => "johndoe",
            'services' => [
                'ademe' => [
                    'id' => '123',
                    'username' => 'johndoe',
                    'email' => 'johndoe@example.com',
                ],
            ],
        ];
        $expectedData = $userData;
        $argumentInsert = [$this->equalTo('citoyens'), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        $this->dbMock->expects($this->exactly(6))
            ->method('findOne')
            ->withConsecutive(
                // est ce que le service existe deja pour un user
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // si le service n'existe pas pour un user est ce que l'email existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => "johndoe@example.com",
                    ])],
                // unique email verifie getPersonByEmail()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim('johndoe@example.com')) . '$/i'),
                    ])],
                // isUniqueUsername()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "username" => "johndoe",
                    ])],
                // est ce que l'user à bine été créer
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // est ce que le token existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                        'loginTokens.name' => 'comobi',
                        'loginTokens.type' => 'personalAccessToken',
                    ])],
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null, [
                "_id" => "55ed9107e41d75a41a558523",
            ], null);
        // 1 - le service n'existe pas
        // 2 - l'email n'existe pas
        // 3 - test des valeur pour l'insert : email ne doit pas être deja pris
        // 4 - test des valeur pour l'insert : username ne doit pas être deja pris
        // 5 - on test que l'user avec le service à bien été créer par l'insert
        // 6 - vu qu'il y a un tokenName on test si il y a deja un token avec ce nom

        // todo: faire les autres versions

        $service = [
            'userId' => '55ed9107e41d75a41a558523',
        ];
        $service = array_merge($service, $serviceDecrypt);

        $serviceEncrypt = $person->cryptoJsAesEncrypt("test", $service);
        $tokenName = "comobi";

        $result = $person->loginAuthServiceToken($serviceEncrypt, $tokenName);
        $this->assertTrue($result['result']);
        $this->assertEquals('55ed9107e41d75a41a558523', $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
        $this->assertIsString($result['token']);
    }

    public function testLoginAuthServiceTokenWithParamsInstance(): void
    {
        $this->params = new Params([
            "passphrase" => "test",
        ]);
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDataValidator($this->dataValidatorMock);
        $person->setModelRole($this->roleMock);
        $person->setModelPreference($this->preferenceMock);
        $person->setModelSlug($this->slugMock);
        $person->setModelCostum($this->costumMock);

        $this->roleMock->expects($this->once())
            ->method('canUserLogin')
            ->willReturn([
                "result" => true,
                "msg" => "Everything is ok : user can login !",
            ]);

        $serviceDecrypt = [
            "serviceName" => "ademe",
            "serviceData" => [
                "id" => "123",
                "email" => "johndoe@example.com",
                "username" => "johndoe",
                "name" => "John Doe",
            ],
        ];

        $userData = [
            'username' => "johndoe",
            'name' => 'John Doe',
            'email' => 'johndoe@example.com',
            'roles' => [
                'standalonePageAccess' => true,
            ],
            'created' => $this->isRecentMongoDate(),
            'collection' => 'citoyens',
            'preferences' => [
                'isOpenData' => false,
                'sendMail' => false,
                'publicFields' => ['locality', 'directory'],
                'privateFields' => ['birthDate', 'email', 'phone'],
            ],
            'seePreferences' => true,
            'slug' => "johndoe",
            'services' => [
                'ademe' => [
                    'id' => '123',
                    'username' => 'johndoe',
                    'email' => 'johndoe@example.com',
                ],
            ],
        ];
        $expectedData = $userData;
        $argumentInsert = [$this->equalTo('citoyens'), $this->callback(function ($arg) use ($expectedData) {
            foreach ($expectedData as $key => $value) {
                if (is_callable($value)) {
                    if (! $value($arg[$key])) {
                        return false;
                    }
                } elseif ($arg[$key] !== $value) {
                    return false;
                }
            }
            return true;
        })];
        $returnInsert = $expectedData;
        $returnInsert['created'] = $this->dbMock->MongoDate(time());
        $returnInsert['_id'] = $this->dbMock->MongoId('55ed9107e41d75a41a558523');

        $this->dbMock->expects($this->once())
            ->method('insert')
            ->with(...$argumentInsert)
            ->willReturn($returnInsert);

        $this->dbMock->expects($this->exactly(6))
            ->method('findOne')
            ->withConsecutive(
                // est ce que le service existe deja pour un user
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // si le service n'existe pas pour un user est ce que l'email existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => "johndoe@example.com",
                    ])],
                // unique email verifie getPersonByEmail()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "email" => $this->dbMock->MongoRegex('/^' . preg_quote(trim('johndoe@example.com')) . '$/i'),
                    ])],
                // isUniqueUsername()
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "username" => "johndoe",
                    ])],
                // est ce que l'user à bine été créer
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "services.ademe.id" => "123",
                    ])],
                // est ce que le token existe deja
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                        "_id" => $this->dbMock->MongoId('55ed9107e41d75a41a558523'),
                        'loginTokens.name' => 'comobi',
                        'loginTokens.type' => 'personalAccessToken',
                    ])],
            )
            ->willReturnOnConsecutiveCalls(null, null, null, null, [
                "_id" => "55ed9107e41d75a41a558523",
            ], null);
        // 1 - le service n'existe pas
        // 2 - l'email n'existe pas
        // 3 - test des valeur pour l'insert : email ne doit pas être deja pris
        // 4 - test des valeur pour l'insert : username ne doit pas être deja pris
        // 5 - on test que l'user avec le service à bien été créer par l'insert
        // 6 - vu qu'il y a un tokenName on test si il y a deja un token avec ce nom

        // todo: faire les autres versions

        $service = [
            'userId' => '55ed9107e41d75a41a558523',
        ];
        $service = array_merge($service, $serviceDecrypt);

        $serviceEncrypt = $person->cryptoJsAesEncrypt("test", $service);
        $tokenName = "comobi";

        $result = $person->loginAuthServiceToken($serviceEncrypt, $tokenName);
        $this->assertTrue($result['result']);
        $this->assertEquals('55ed9107e41d75a41a558523', $result['id']);
        $this->assertFalse($result['isCommunected']);
        $this->assertArrayHasKey("account", $result);
        $this->assertEquals("Vous êtes maintenant identifié : bienvenue sur communecter.", $result['msg']);
        $this->assertIsString($result['token']);
    }

    public function testChangePassword(): void
    {
        $personId = '55ed9107e41d75a41a558524';
        $oldPassword = 'oldPassword';
        $newPassword = 'newPassword';

        $oldPwdHash = hash('sha256', $personId . $oldPassword);
        $newPwdHash = hash('sha256', $personId . $newPassword);
        $account = [
            "_id" => $this->dbMock->MongoId($personId),
            "email" => "test@example.com",
            "name" => "John Doe",
            "username" => "johndoe",
            "slug" => "john-doe",
            "cp" => "12345",
            "address" => [
                "postalCode" => "12345",
                "codeInsee" => "67890",
                "addressCountry" => "US",
            ],
            "roles" => [
                "sourceAdmin" => false,
            ],
            "preferences" => [
                "preference1" => "value1",
                "preference2" => "value2",
            ],
            "lastLoginDate" => time(),
            "pwd" => $oldPwdHash,
        ];

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $person->setModelDocument($this->documentMock);
        $person->setModelAuthorisation($this->authorisationMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $this->authorisationMock->method('canEditItem')
            ->with($this->equalTo($personId), $this->equalTo(PersonInterface::COLLECTION), $this->equalTo($personId))
            ->willReturn(true);

        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $account["_id"]))
            ->willReturn($account);

        $this->dbMock->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $account["_id"]))
            ->willReturn([
                "profilImageUrl" => "http://example.com/profile.jpg",
                "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
                "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
                "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
            ]);

        $this->dataValidatorMock->expects($this->once())
            ->method('getCollectionFieldNameAndValidate')
            ->with($person->dataBinding, "pwd", $newPwdHash)
            ->willReturn("pwd");

        // Call the changePassword method
        $result = $person->changePassword($oldPassword, $newPassword, (string) $account["_id"]);

        // Assert the result
        $this->assertTrue($result['result']);
        $this->assertEquals('Your password has been changed with success!', $result['msg']);
    }

    public function testValidateEmailAccount(): void
    {
        $personId = '55ed9107e41d75a41a558524';
        $email = "test@example.com";
        $validationKey = hash('sha256', $personId . $email);

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        $this->dbMock->expects($this->exactly(2))
            ->method('findOneById')
            ->withConsecutive(
                [
                    $this->equalTo(PersonInterface::COLLECTION), $this->equalTo($personId), $this->equalTo([
                        "email" => 1,
                    ])],
                [$this->equalTo(PersonInterface::COLLECTION), $this->equalTo($personId), $this->equalTo(["name", "email", "source", "mailToResend"])]
            )
            ->willReturnOnConsecutiveCalls(
                [
                    "email" => $email,
                ],
                [
                    "name" => "John Doe",
                    "email" => $email,
                    "source" => "source",
                ]
            );

        // Call the method under test
        $result = $person->validateEmailAccount($personId, $validationKey);

        // Assert the expected result
        $this->assertTrue($result['result']);
        $this->assertEquals('The account and email is now validated !', $result['msg']);
    }

    public function testUpdateMinimalDataNoUserError(): void
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("The person id is unkown : contact your admin");

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Set up the expectations
        $personId = '55ed9107e41d75a41a558524';
        $personData = [
            'name' => 'John Doe',
        ];

        // Call the method to be tested
        $result = $person->updateMinimalData($personId, $personData);
    }

    public function testUpdateMinimalDataNotPendingError(): void
    {
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Impossible to update an account not pending !");

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Set up the expectations
        $personId = '55ed9107e41d75a41a558524';
        $personData = [
            '_id' => $this->dbMock->MongoId($personId),
            'name' => 'John Doe',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn($personData);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        // Call the method to be tested
        $result = $person->updateMinimalData($personId, $personData);
    }

    public function testUpdateMinimalDataNoPasswordError(): void
    {
        $updatedPersonData = null;
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("Password is required for update.");

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Set up the expectations
        $personId = '55ed9107e41d75a41a558524';
        $personData = [
            '_id' => $this->dbMock->MongoId($personId),
            'name' => 'John Doe',
            'pending' => true,
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn($personData);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        // Call the method to be tested
        $result = $person->updateMinimalData($personId, $personData);

        // Assert the result
        $this->assertEquals($updatedPersonData, $result);
    }

    public function testUpdateMinimalDataPasswordNoValidError(): void
    {
        $updatedPersonData = null;
        $this->expectException(CTKException::class);
        $this->expectExceptionMessage("The password not valid");

        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Set up the expectations
        $personId = '55ed9107e41d75a41a558524';
        $personData = [
            '_id' => $this->dbMock->MongoId($personId),
            'name' => 'John Doe',
            'pending' => true,
            'pwd' => '1234567',
            'email' => 'johndoe@exemple.com',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn($personData);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        // Call the method to be tested
        $result = $person->updateMinimalData($personId, $personData);

        // Assert the result
        $this->assertEquals($updatedPersonData, $result);
    }

    public function testUpdateMinimalData(): void
    {
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Set up the expectations
        $personId = '55ed9107e41d75a41a558524';
        $personData = [
            '_id' => $this->dbMock->MongoId($personId),
            'name' => 'John Doe',
            'username' => 'johndoe',
            'email' => 'johndoe@example.com',
            'pending' => true,
            'pwd' => '12345678',
        ];

        $this->dbMock->expects($this->once())
            ->method('findOneById')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo((string) $personId))
            ->willReturn($personData);

        $imageTest = [
            "profilImageUrl" => "http://example.com/profile.jpg",
            "profilMediumImageUrl" => "http://example.com/profile-medium.jpg",
            "profilThumbImageUrl" => "http://example.com/profile-thumb.jpg",
            "profilMarkerImageUrl" => "http://example.com/profile-marker.jpg",
        ];

        $this->documentMock->method('retrieveAllImagesUrl')->willReturn($imageTest);

        $this->preferenceMock->method('clearByPreference')->will($this->returnCallback(
            fn (array $element, string $elementType, string $userId) => $element
        ));

        $person->setModelDocument($this->documentMock);

        $person->setModelPreference($this->preferenceMock);

        $person->setModelSlug($this->slugMock);
        $person->setModelDataValidator($this->dataValidatorMock);

        $personToUpdate = [
            "two_steps_register" => true,
            "name" => "John Doe",
            "email" => "johndoe@example.com",
            "username" => "johndoe",
            "pwd" => "5e6c30cce31aeb8c84b1c84357a2f061447c5b5d30e1e94e2aec3c6f39b285c1",
            "slug" => "johndoe",
            "preferences.sendMail" => [
                "source" => ["communecter"],
            ],
        ];
        $this->dbMock->expects($this->once())
            ->method('update')
            ->with($this->equalTo(PersonInterface::COLLECTION), $this->equalTo([
                "_id" => $this->dbMock->MongoId($personId),
            ]), $this->equalTo([
                '$set' => $personToUpdate,
                '$unset' => [
                    "pending" => "",
                    "roles.tobeactivated" => "",
                ],
            ]));

        // Call the method to be tested
        $result = $person->updateMinimalData($personId, $personData);

        // Assert the result
        $this->assertTrue($result['result']);
        $this->assertEquals('The pending user has been updated and is now complete', $result['msg']);
    }

    public function testIsUniqueEmail()
    {
        $email = 'test@example.com';

        // Mock the PhdbDiInterface
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'email' => $email,
            ])
            ->willReturn(null);

        // Create an instance of the Person class
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the isUniqueEmail method and assert the result
        $result = $person->isUniqueEmail($email);
        $this->assertTrue($result);
    }

    public function testIsFirstCitizen(): void
    {
        $insee = '123456789';

        // Mock the PhdbDiInterface
        $this->dbMock->expects($this->once())
            ->method('findOne')
            ->with(PersonInterface::COLLECTION, [
                'address.codeInsee' => $insee,
            ])
            ->willReturn(null);

        // Create an instance of the Person class
        $person = new Person([
            'db' => $this->dbMock,
            'session' => $this->sessionBuilderMock,

            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ]);

        // Call the method to test
        $result = $person->isFirstCitizen($insee);

        // Assert the result
        $this->assertTrue($result);
    }
}
