<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests;

use MongoDB\BSON\ObjectId;
use PixelHumain\Models\Interfaces\ArrayHelperInterface;
use PixelHumain\Models\Interfaces\AuthorisationInterface;
use PixelHumain\Models\Interfaces\CO2StatInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\CostumInterface;
use PixelHumain\Models\Interfaces\DataValidatorInterface;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\ElementInterface;
use PixelHumain\Models\Interfaces\I18NInterface;
use PixelHumain\Models\Interfaces\ImportInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PhdbDiInterface;
use PixelHumain\Models\Interfaces\PreferenceInterface;
use PixelHumain\Models\Interfaces\RoleInterface;
use PixelHumain\Models\Interfaces\SessionInterface;
use PixelHumain\Models\Interfaces\SlugInterface;
use PixelHumain\Models\Person;
use PixelHumain\Models\Services\Interfaces\DbServiceInterface;
use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;

use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;

trait TestSetupTrait
{
    protected $dbMock;

    protected $sessionMock;

    protected $sessionBuilderMock;

    protected $arrayHelperMock;

    protected $cookieHelperMock;

    protected $dataValidatorMock;

    protected $authorisationMock;

    protected $importMock;

    protected $params;

    protected string $languageDefault;

    protected $languageMock;

    protected string $moduleId;

    protected $roleMock;

    protected $documentMock;

    protected $preferenceMock;

    protected $elementMock;

    protected $slugMock;

    protected $CO2StatMock;

    protected $costumMock;

    protected array $sessionData = [];

    protected function createParamsMock()
    {
        $this->params ??= [];
        $this->paramsMock = $this->createMock(ParamsServiceInterface::class);
        $this->paramsMock->method('get')->will($this->returnCallback(
            fn ($key) => $this->params[$key] ?? null
        ));
        $this->paramsMock->method('set')->will($this->returnCallback(
            function ($key, $value) {
                $this->params[$key] = $value ?? null;
            }
        ));
    }

    protected function createDbMock()
    {
        $this->dbMock = $this->createMock(DbServiceInterface::class);

        $this->dbMock->method('MongoId')->will($this->returnCallback(
            fn ($id) => new ObjectId($id)
        ));

        $this->dbMock->method('MongoDate')->will($this->returnCallback(
            function ($value) {
                date_default_timezone_set('UTC');
                $date = new MongoDateWrapper($value);
                return $date->toBSONType();
            }
        ));

        $this->dbMock->method('MongoRegex')->will($this->returnCallback(
            function ($value) {
                $regex = new MongoRegexWrapper($value);
                return $regex->toBSONType();
            }
        ));
    }

    protected function createSessionMock()
    {
        $this->sessionMock = $this->createMock(SessionServiceInterface::class);
        $this->sessionBuilderMock = $this->getMockBuilder(SessionTest::class)->getMock();

        $this->sessionBuilderMock->method('get')
            ->will($this->returnCallback(
                fn ($key) => $this->sessionData[$key] ?? null
            ));

        $this->sessionBuilderMock->method('set')->will($this->returnCallback(
            function ($key, $value) {
                $this->sessionData[$key] = $value ?? null;
            }
        ));

        $this->sessionBuilderMock->method('unset')->will($this->returnCallback(
            function ($key) {
                unset($this->sessionData[$key]);
            }
        ));

        $this->sessionBuilderMock->method('isset')
            ->will($this->returnCallback(
                fn ($key) => array_key_exists($key, $this->sessionData)
            ));

        $this->sessionBuilderMock->method('getUserId')
            ->will($this->returnCallback(
                fn () => $this->sessionData["userId"] ?? null
            ));

        $this->sessionBuilderMock->method('getUserUsername')
            ->will($this->returnCallback(
                fn () => $this->sessionData["user"]["username"] ?? null
            ));

        $this->sessionBuilderMock->method('offsetExists')
            ->will($this->returnCallback(
                fn ($key) => array_key_exists($key, $this->sessionData)
            ));

        $this->sessionBuilderMock->method('offsetGet')
            ->will($this->returnCallback(
                fn ($key) => $this->sessionData[$key]
            ));

        $this->sessionBuilderMock->method('offsetSet')->will($this->returnCallback(
            function ($key, $value) {
                $this->sessionData[$key] = $value ?? null;
            }
        ));

        $this->sessionBuilderMock->method('offsetUnset')->will($this->returnCallback(
            function ($key) {
                unset($this->sessionData[$key]);
            }
        ));
    }

    protected function createI18nMock()
    {
        $this->languageMock = $this->createMock(LanguageServiceInterface::class);
        $this->languageMock->method('get')->will($this->returnCallback(
            fn () => $this->languageDefault
        ));
        $this->languageMock->method('t')->will($this->returnCallback(
            fn ($category, $message, $params = [], $source = null, $language = null) => $message
        ));
    }

    protected function createArrayHelperMock()
    {
        $this->arrayHelperMock = $this->createMock(ArrayHelperInterface::class);
    }

    protected function createCookieHelperMock()
    {
        $this->cookieHelperMock = $this->createMock(CookieHelperInterface::class);
    }

    protected function createDataValidatorMock()
    {
        $this->dataValidatorMock = $this->createMock(DataValidatorInterface::class);

        $this->dataValidatorMock->method('clearUserInput')->will($this->returnCallback(
            fn ($person) => $person
        ));
    }

    protected function createDocumentMock()
    {
        $this->documentMock = $this->createMock(DocumentInterface::class);
    }

    protected function createRoleMock()
    {
        $this->roleMock = $this->createMock(RoleInterface::class);

        $this->roleMock->method('getDefaultRoles')->will($this->returnCallback(
            function () {
                $roles = [];
                if (! isset($this->params['personNotValidated']) || $this->params['personNotValidated'] !== true) {
                    $roles["tobeactivated"] = true;
                }

                // By default no one is beta tester in a betaTest mode
                if (isset($this->params['betaTest'])) {
                    $roles["betaTester"] = false;
                }

                // Can access standalone Pages : true
                $roles["standalonePageAccess"] = true;

                return $roles;
            }
        ));
    }

    protected function createPreferenceMock()
    {
        $this->preferenceMock = $this->createMock(PreferenceInterface::class);

        $this->preferenceMock->method('initPreferences')->will($this->returnCallback(
            function ($type) {
                $preferences = [];
                $preferences["isOpenData"] = true;
                $preferences["sendMail"] = false;
                if ($type == PersonInterface::COLLECTION) {
                    $preferences["isOpenData"] = false;
                    $preferences["publicFields"] = ["locality", "directory"];
                    $preferences["privateFields"] = ["birthDate", "email", "phone"];
                } else {
                    $preferences["isOpenEdition"] = true;
                }
                return $preferences;
            }
        ));
    }

    protected function createElementMock()
    {
        $this->elementMock = $this->createMock(ElementInterface::class);
    }

    protected function createSlugMock()
    {
        $this->slugMock = $this->createMock(SlugInterface::class);

        $this->slugMock->method('checkAndCreateSlug')->will($this->returnCallback(
            fn ($str) => $str
        ));

        $this->slugMock->method('save')->will($this->returnCallback(
            fn ($type, $id, $slug) => true
        ));
    }

    protected function createAuthorisationMock()
    {
        $this->authorisationMock = $this->createMock(AuthorisationInterface::class);
    }

    protected function createImportMock()
    {
        $this->importMock = $this->createMock(ImportInterface::class);
    }

    protected function createCO2StatMock()
    {
        $this->CO2StatMock = $this->createMock(CO2StatInterface::class);
    }

    protected function createCostumMock()
    {
        $this->costumMock = $this->createMock(CostumInterface::class);
    }

    protected function setUpCommonMocks()
    {
        $this->languageDefault = 'en';
        $this->moduleId = 'communecter';

        $this->createParamsMock();
        $this->createDbMock();
        $this->createSessionMock();
        $this->createI18nMock();
        $this->createArrayHelperMock();
        $this->createCookieHelperMock();
        $this->createDataValidatorMock();
        $this->createDocumentMock();
        $this->createRoleMock();
        $this->createPreferenceMock();
        $this->createElementMock();
        $this->createSlugMock();
        $this->createAuthorisationMock();
        $this->createImportMock();
        $this->createCO2StatMock();
        $this->createCostumMock();
    }
}
