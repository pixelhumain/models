<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests;

use PHPUnit\Framework\TestCase;
use PixelHumain\Models\Factory\PersonFactory;
use PixelHumain\Models\Person;
use PixelHumain\Models\Services\Interfaces\DbServiceInterface;
use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;
use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;
use Psr\Container\ContainerInterface;

class PersonFactoryTest extends TestCase
{
    use TestSetupTrait;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpCommonMocks();
    }

    public function testCreatePerson()
    {
        // Create a mock for the ContainerInterface
        $container = $this->createMock(ContainerInterface::class);
        $container->method('get')->will($this->returnCallback(
            fn ($nameClass) => $this->createMock($nameClass)
        ));

        $config = [
            'db' => $this->dbMock,
            'session' => $this->sessionMock,
            'arrayHelper' => $this->arrayHelperMock,
            'cookieHelper' => $this->cookieHelperMock,
            'params' => $this->paramsMock,
            'language' => $this->languageMock,
            'moduleId' => $this->moduleId,
        ];

        // Call the createPerson method and assert the returned value
        $person = PersonFactory::create($container, $config);
        $this->assertInstanceOf(Person::class, $person);
    }
}
