<?php

namespace PixelHumain\Models\Tests;

use ArrayAccess;
use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;

/**
 * @template TKey of array-key
 * @template TValue
 * @implements ArrayAccess<TKey, TValue>
 */
abstract class SessionTest implements SessionServiceInterface, ArrayAccess
{
    private array $session = [];

    /**
     * Get a value from the session by key.
     *
     * @param string $key The key of the value to retrieve from the session.
     * @param mixed $default The default value to return if the key is not found in the session.
     * @return mixed The value from the session if found, otherwise the default value.
     */
    public function get(string $key, $default = null)
    {
        return $this->session[$key] ?? $default;
    }

    /**
     * Set a value in the session by key.
     *
     * @param string $key The key of the value to set in the session.
     * @param mixed $value The value to set in the session.
     * @return void
     */
    public function set(string $key, $value): void
    {
        $this->session[$key] = $value;
    }

    /**
     * Check if a session variable is set.
     *
     * @param string $key The name of the session variable.
     * @return bool Returns true if the session variable is set, false otherwise.
     */
    public function isset(string $key): bool
    {
        return isset($this->session[$key]);
    }

    /**
     * Unsets a session variable by key.
     *
     * @param string $key The key of the session variable to unset.
     * @return void
     */
    public function unset(string $key): void
    {
        unset($this->session[$key]);
    }

    /**
     * Get the user ID from the session.
     *
     * @return string|null The user ID from the session, or null if not found.
     */
    public function getUserId(): ?string
    {
        return $this->get('userId') ?? null;
    }

    /**
     * Returns the username associated with the current session.
     *
     * @return string|null The username, or null if no session is active.
     */
    public function getUserUsername(): ?string
    {
        return $this->get('user')('username') ?? null;
    }

    /**
     * Retrieves the session value from an array.
     *
     * @param array $data The array containing the session value.
     * @return array The session value extracted from the array.
     */
    public function getFromArray(array $data): array
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = $this->get((string) $key);
        }
        return $result;
    }

    /**
     * Set session value from an array.
     *
     * @param array $data The data array.
     * @return void
     */
    public function setFromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            $this->set((string) $key, $value);
        }
    }

    /**
     * Unsets session values from an array.
     *
     * @param array $data The array containing the session values.
     * @return void
     */
    public function unsetFromArray(array $data): void
    {
        foreach ($data as $key) {
            $this->unset((string) $key);
        }
    }

    public function __get($name)
    {
        return $this->session[$name];
    }

    public function __set($name, $value)
    {
        $this->session[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->session[$name]);
    }

    public function __unset($name)
    {
        unset($this->session[$name]);
    }

    /**
     * Check if a session offset exists.
     *
     * @param mixed $offset The offset to check.
     * @return bool True if the offset exists, false otherwise.
     */
    public function offsetExists($offset): bool
    {
        return isset($this->session[(string) $offset]);
    }

    /**
      * Retrieves the value at the specified offset.
      *
      * @param mixed $offset The offset to retrieve the value from.
      * @return mixed The value at the specified offset.
      * @psalm-return mixed
      * @psalm-suppress MethodSignatureMustProvideReturnType
      */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->session[(string) $offset];
    }

    /**
     * Sets the value at the specified offset.
     *
     * @param mixed $offset The offset to set.
     * @param mixed $value The value to set.
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        $this->session[(string) $offset] = $value;
    }

    /**
     * Removes the value at the specified offset.
     *
     * @param mixed $offset The offset to unset.
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->session[(string) $offset]);
    }
}
