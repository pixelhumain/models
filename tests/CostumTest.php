<?php

declare(strict_types=1);
namespace PixelHumain\Models\Tests;

use PHPUnit\Framework\TestCase;
use PixelHumain\Models\Costum;
use Psr\Container\ContainerInterface;

class CostumTest extends TestCase
{
    use TestSetupTrait;

    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpCommonMocks();
    }

    public function testSameFunction()
    {
        // $container = $this->createMock(ContainerInterface::class);
        // $container->method('get')->will($this->returnCallback(
        //     fn ($nameClass) => $this->createMock($nameClass)
        // ));
        // $container->method('has')->will($this->returnCallback(
        //     fn ($nameClass) => true
        // ));

        // // Créer une instance de Costum
        // $costum = new Costum([
        //     'phdb' => $this->dbMock,
        //     'session' => $this->sessionMock,
        //     'i18n' => $this->i18nMock,
        //     'arrayHelper' => $this->arrayHelperMock,
        //     'cookieHelper' => $this->cookieHelperMock,
        //     'params' => $this->params,
        //     'language' => $this->language,
        //     'moduleId' => $this->moduleId,
        //     'container' => $container,
        // ]);

        // $params = [
        //     'param1' => 'value1',
        // ];
        // $retour = $costum->sameFunction('testFon', $params);
        // print_r($retour);

        $this->assertEquals('yes', 'yes');
    }
}
