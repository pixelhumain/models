<?php

declare(strict_types=1);

// use Utils\Rector\Rector\ParamsArrayToMethodCallRector;
use Utils\Rector\Rector\DynamicReplaceStaticCallRector;
use Rector\Php71\Rector\FuncCall\CountOnNullRector;
use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Renaming\Rector\Name\RenameClassRector;
use Rector\Php73\Rector\FuncCall\JsonThrowOnErrorRector;
use Rector\Renaming\Rector\ClassConstFetch\RenameClassConstFetchRector;
use Rector\Renaming\ValueObject\RenameClassAndConstFetch;
use Rector\Renaming\ValueObject\MethodCallRename;


return static function (RectorConfig $rectorConfig): void {

  
    $rectorConfig->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ]);

    $rectorConfig->skip([
        JsonThrowOnErrorRector::class
    ]);

    $rectorConfig->importNames();

    // register a single rule
    $rectorConfig->rule(CountOnNullRector::class);

    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
    $rectorConfig->sets([
            LevelSetList::UP_TO_PHP_74
        ]);
    

    $directory = 'src/Interfaces';
    $files = scandir($directory);
    $rules = [];

    foreach ($files as $file) {
        // Ignorer les fichiers non-PHP
        if (substr($file, -4) !== '.php') {
            continue;
        }

        $interfaceName = basename($file, '.php');
        $className = str_replace('Interface', '', $interfaceName); // Enlever 'Interface' du nom

        $fullInterfaceName = 'PixelHumain\\Models\\Interfaces\\' . $interfaceName;
        $fullClassName = 'PixelHumain\\Models\\' . $className;

        $reflection = new ReflectionClass($fullInterfaceName);
        $constants = $reflection->getConstants();

        foreach ($constants as $constName => $constValue) {
            $rules[] = new RenameClassAndConstFetch($fullClassName, $constName, $fullInterfaceName, $constName);
        }
    }

    $rectorConfig->ruleWithConfiguration(RenameClassConstFetchRector::class, $rules);


    $rectorConfig->rule(DynamicReplaceStaticCallRector::class);

    // /**
    //  * Renommer les methodes traits
    //  */
    // $directory = 'src/Traits/Interfaces';
    // $files = scandir($directory);
    // $rulesTraits = [];

    // foreach ($files as $file) {
    //     // Ignorer les fichiers non-PHP
    //     if (substr($file, -4) !== '.php') {
    //         continue;
    //     }

    //     $fileName = basename($file, '.php');
    //     $className = str_replace('TraitInterface', '', $fileName); // Enlever 'Trait' du nom

    //     $traitName = 'PixelHumain\Models\Traits\\'. $className . 'Trait';
    //     $traitNameInterface = 'PixelHumain\Models\Traits\Interfaces\\'. $fileName;

    //     $rulesTraits[] = new MethodCallRename($traitName, 'get'.$className, 'getModel'.$className);
    //     $rulesTraits[] = new MethodCallRename($traitName, 'set'.$className, 'setModel'.$className);
    //     $rulesTraits[] = new MethodCallRename($traitNameInterface, 'get'.$className, 'getModel'.$className);
    //     $rulesTraits[] = new MethodCallRename($traitNameInterface, 'set'.$className, 'setModel'.$className);
        
    // }

    // $rectorConfig->ruleWithConfiguration(Rector\Renaming\Rector\MethodCall\RenameMethodRector::class, $rulesTraits);

};
