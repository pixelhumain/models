

# pixelhumain/models


## phpdoc

### Installation de phpdoc

```bash
alias phpdoc="docker run --rm -v $(pwd):/data phpdoc/phpdoc:3"
```

### Générer la documentation

```bash
phpdoc
```

## install local

```bash
composer config repositories.local path ../../pixelhumain-models
composer require pixelhumain/models:dev-main --dev
composer update pixelhumain/models
```

## rector

[https://github.com/rectorphp/rector](https://github.com/rectorphp/rector)

Rector est un outil qui permet de mettre à jour le code source d'un projet PHP. Il permet de mettre à jour le code source d'un projet PHP en appliquant des règles de mise à jour de code.

config dans le fichier `rector.php`

Dans la config, on peut définir les règles à appliquer, les fichiers à ignorer, les fichiers à traiter, les imports à faire, les sets de règles à appliquer, etc. Dans notre cas, on va utiliser le set de règles `LevelSetList::UP_TO_PHP_74` qui va appliquer les règles de mise à jour de code jusqu'à la version 7.4 de PHP. 

On peut aussi définir des règles spécifiques à appliquer, par exemple :
* `CountOnNullRector::class` qui va remplacer `count(null)` par `0`. 
* `InlineConstructorDefaultToPropertyRector::class` qui va remplacer le constructeur par défaut par une propriété. 

On peut aussi ignorer des règles, par exemple : 
* `JsonThrowOnErrorRector::class` qui va remplacer `json_decode($json, true)` par `json_decode($json, true, 512, JSON_THROW_ON_ERROR)`.

`importNames` permet d'importer les classes utilisées dans le code.

Pour lancer la mise à jour du code, on utilise la commande composer : `composer rector` ou `composer rector-fix` pour corriger les erreurs automatiquement.

```php
<?php

declare(strict_types=1);

use Rector\Php71\Rector\FuncCall\CountOnNullRector;
use Rector\CodeQuality\Rector\Class_\InlineConstructorDefaultToPropertyRector;
use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Renaming\Rector\Name\RenameClassRector;
use Rector\Php73\Rector\FuncCall\JsonThrowOnErrorRector;

return static function (RectorConfig $rectorConfig): void {

  
    $rectorConfig->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ]);

    $rectorConfig->skip([
        JsonThrowOnErrorRector::class
    ]);

    $rectorConfig->importNames();

    // register a single rule
    $rectorConfig->rule(CountOnNullRector::class);

    $rectorConfig->rule(InlineConstructorDefaultToPropertyRector::class);

    // define sets of rules
    $rectorConfig->sets([
            LevelSetList::UP_TO_PHP_74
        ]);
};
```

## ecs

[https://github.com/symplify/coding-standard](https://github.com/symplify/coding-standard)

EasyCodingStandard est un outil qui permet de vérifier le code source d'un projet PHP. Il permet de vérifier le code source d'un projet PHP en appliquant des règles de code style.

config dans le fichier `ecs.php`

Dans la config, on peut définir les règles à appliquer, les fichiers à ignorer, les fichiers à traiter, etc. Dans notre cas, on va utiliser le set de règles `LevelSetList::PSR_12` qui va appliquer les règles de code style PSR-12 et `LevelSetList::ARRAY` qui va appliquer les règles de code style array et `LevelSetList::SPACES` qui va appliquer les règles de code style spaces.

Pour lancer la vérification du code, on utilise la commande composer : `composer ecs-check` ou `composer ecs-fix` pour corriger les erreurs automatiquement.

```php
<?php
declare(strict_types=1);
// ecs.php
// use PhpCsFixer\Fixer\Import\NoUnusedImportsFixer;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;
use Symplify\EasyCodingStandard\Config\ECSConfig;

// NoUnusedImportsFixer supprimer les use qui ne sont pas utilisés
return static function (ECSConfig $ecsConfig): void {
    //$ecsConfig->rule(NoUnusedImportsFixer::class);

    $ecsConfig->paths([__DIR__ . '/src', __DIR__ . '/tests']);
    
    $ecsConfig->sets([SetList::PSR_12, SetList::ARRAY, SetList::SPACES]);
    
};
```

## psalm

[https://psalm.dev/](https://psalm.dev/)

Psalm est un outil d'analyse statique de code pour PHP. Il permet de vérifier le code source d'un projet PHP en appliquant des règles de code style.

config dans le fichier `psalm.xml` ou `psalm.xml.dist`

`errorLevel="1"` permet de définir le niveau d'erreur. 1 = erreur, 2 = warning, 3 = info.
`findUnusedBaselineEntry="true"` permet de trouver les entrées inutilisées dans le fichier `psalm-baseline.xml`.
`findUnusedCode="false"` permet de trouver le code inutilisé.
`issueHandlers` permet de définir les règles à appliquer, par exemple `MixedAssignment` qui va supprimer les erreurs de type `MixedAssignment`.

Pour lancer la vérification du code, on utilise la commande composer : `composer psalm`

```xml
<?xml version="1.0"?>
<psalm
    errorLevel="1"
    findUnusedBaselineEntry="true"
    findUnusedCode="false"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="https://getpsalm.org/schema/config"
    xsi:schemaLocation="https://getpsalm.org/schema/config vendor/vimeo/psalm/config.xsd"
>
    <projectFiles>
        <directory name="src" />
        <ignoreFiles>
            <directory name="vendor" />
        </ignoreFiles>
    </projectFiles>
    <issueHandlers>
        <MixedAssignment errorLevel="suppress" />
    </issueHandlers>
</psalm>
```

## phpunit

[https://phpunit.de/](https://phpunit.de/)

PHPUnit est un framework de test unitaire pour le langage de programmation PHP. Il permet de vérifier le code source d'un projet PHP en appliquant des tests unitaires.

config dans le fichier `phpunit.xml.dist`

Pour lancer la vérification du code, on utilise la commande composer : `composer test`

```xml
<?xml version="1.0" encoding="UTF-8"?>

<phpunit colors="true"
         verbose="true"
         failOnRisky="true"
         failOnWarning="true"
         convertErrorsToExceptions="true"
         convertNoticesToExceptions="true"
         convertWarningsToExceptions="true"
         stopOnFailure="false"
         executionOrder="random"
         resolveDependencies="true">
    <php>
        <ini name="error_reporting" value="-1"/>
    </php>

    <testsuites>
        <testsuite name="Pixelhumain Models common tests">
            <directory suffix=".php">./tests</directory>
            <exclude>./tests/TestEnvironments</exclude>
        </testsuite>
    </testsuites>
    <coverage>
        <include>
            <directory>./src</directory>
            <directory>./config</directory>
        </include>
    </coverage>
</phpunit>
```


## infection

config dans le fichier `infection.json`

## git hook pre-commit

1. **Créer un Script de Pre-commit :**
   Créez un script de pre-commit dans le dossier `.git/hooks` de votre projet. Ce script exécutera les commandes définies dans les scripts de votre `package.json`.

2. **Ajouter les Commandes dans le Script :**
   Éditez le script `pre-commit` pour inclure les commandes que vous souhaitez exécuter. Par exemple :

   ```bash
   #!/bin/sh

   # Exécuter Rector
   echo "Running Rector..."
   ./vendor/bin/rector process src tests --dry-run
   if [ $? != 0 ]; then
     echo "Rector found issues. Commit cancelled."
     exit 1
   fi

   # Exécuter ECS Check
   echo "Running ECS Check..."
   ./vendor/bin/ecs check src tests
   if [ $? != 0 ]; then
     echo "ECS found issues. Commit cancelled."
     exit 1
   fi

   # Exécuter PHPUnit Tests
   echo "Running PHPUnit tests..."
   ./vendor/bin/phpunit --testdox --no-interaction
   if [ $? != 0 ]; then
     echo "PHPUnit tests failed. Commit cancelled."
     exit 1
   fi

   exit 0
   ```

   Assurez-vous que ce script est exécutable :
   ```bash
   chmod +x .git/hooks/pre-commit
   ```

3. **Tester le Hook :**
   Tentez de faire un commit pour tester si le hook fonctionne correctement. Le script devrait exécuter Rector, ECS et PHPUnit, et si l'un de ces outils échoue, le commit sera bloqué.

4. **Gestion des Dépendances :**
   Assurez-vous que toutes les dépendances nécessaires (comme Rector, ECS, PHPUnit) sont installées dans votre projet. Habituellement, elles sont installées via Composer.

En utilisant ce hook `pre-commit`, vous pouvez garantir que votre code passe les vérifications de Rector, ECS, et les tests PHPUnit avant chaque commit, ce qui contribue à améliorer la qualité et la fiabilité du code dans votre projet.

## gitlab ci

code_quality : permet de lancer les outils de code quality (rector, ecs)
test : permet de lancer les tests unitaires

config dans le fichier `.gitlab-ci.yml`

```yaml
stages:
  - code_quality
  - test

rector:
  stage: code_quality
  image: php:7.4
  script:
    - apt-get update && apt-get install -y unzip
    - pecl install mongodb
    - docker-php-ext-enable mongodb
    - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    - composer install --no-progress --prefer-dist --optimize-autoloader
    - vendor/bin/rector process --dry-run

ecs:
  stage: code_quality
  image: php:7.4
  script:
    - apt-get update && apt-get install -y unzip
    - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    - composer install --no-progress --ignore-platform-req=ext-mongodb --prefer-dist --optimize-autoloader
    - vendor/bin/ecs check src tests

phpunit_test:
  stage: test
  image: php:7.4
  script:
    - apt-get update && apt-get install -y libssl-dev unzip
    - pecl install mongodb
    - docker-php-ext-enable mongodb
    - curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    - composer install --no-progress --prefer-dist --optimize-autoloader
    - ./vendor/bin/phpunit --configuration phpunit.xml.dist
```

