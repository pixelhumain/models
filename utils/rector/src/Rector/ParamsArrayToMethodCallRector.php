<?php

declare(strict_types=1);

namespace Utils\Rector\Rector;

use PhpParser\Node;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\PropertyFetch;
use Rector\Core\Rector\AbstractRector;
use Symplify\RuleDocGenerator\ValueObject\CodeSample\CodeSample;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;
use Rector\Core\Contract\Rector\ConfigurableRectorInterface;

class ParamsArrayToMethodCallRector extends AbstractRector implements ConfigurableRectorInterface
{
    /**
     * @var string
     */
    private $methodName = 'paramsGet'; // Valeur par défaut
    private $targetDirectory;

    public function getNodeTypes(): array
    {
        return [ArrayDimFetch::class];
    }

    public function refactor(Node $node): ?Node
    {
        if ($this->targetDirectory && strpos($this->file->getFilePath(), $this->targetDirectory) === false) {
            // Le fichier n'est pas dans le répertoire cible
            return null;
        }
                
        // Assurez-vous que c'est un accès à un tableau sur $this->params
        if (!$node instanceof ArrayDimFetch) {
            return null;
        }

        if (!$node->var instanceof PropertyFetch) {
            return null;
        }
        if (!$this->isName($node->var->var, 'this')) {
            return null;
        }
        if (!$this->isName($node->var->name, 'params')) {
            return null;
        }

        $scope = $node->getAttribute(\Rector\NodeTypeResolver\Node\AttributeKey::SCOPE);
        if ($scope instanceof \PHPStan\Analyser\Scope) {
            $classReflection = $scope->getClassReflection();
            if ($classReflection && $classReflection->isSubclassOf('PixelHumain\Models\BaseModel')) {
                // La classe actuelle étend BaseModel
                // Créez un appel de méthode paramsGet avec la clé comme argument
                $methodCall = $this->nodeFactory->createMethodCall('this', $this->methodName, [$node->dim]);
                return $methodCall;
            }
        }
                
        return null;
    }

    public function configure(array $configuration): void
    {
        $this->methodName = $configuration['methodName'] ?? $this->methodName;
        $this->targetDirectory = $configuration['targetDirectory'] ?? null;
    }

    public function getRuleDefinition(): RuleDefinition
    {
        return new RuleDefinition(
            'Convert $this->params[...] to $this->params->get(...)',
            [
                new CodeSample(
                    '$this->params[\'key\'];',
                    '$this->params->get(\'key\');'
                ),
            ]
        );
    }
}
