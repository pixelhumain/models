<?php

declare(strict_types=1);

namespace Utils\Rector\Rector;

use PhpParser\Node;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Expr\MethodCall;
use Rector\Core\Rector\AbstractRector;
use Symplify\RuleDocGenerator\ValueObject\CodeSample\CodeSample;
use Symplify\RuleDocGenerator\ValueObject\RuleDefinition;
use Rector\Core\Contract\Rector\ConfigurableRectorInterface;
use PhpParser\Node\Stmt\Class_;

class DynamicReplaceStaticCallRector extends AbstractRector
{
    private $namespacePrefix = 'PixelHumain\\Models\\';

    private array $ignoreClasses = [
        'Yii', 'CO2', 'Rest', 'ArrayHelper'
    ];

    public function getNodeTypes(): array
    {
        return [StaticCall::class];
    }

    public function refactor(Node $node): ?Node
    {
        if (!$node instanceof StaticCall) {
            return null;
        }

    $directory = 'src';
    $files = scandir($directory);
    $nameShortModeles = [];

    foreach ($files as $file) {
        // Ignorer les fichiers non-PHP
        if (substr($file, -4) !== '.php') {
            continue;
        }
        $modeleName = basename($file, '.php');
        $nameShortModeles[] = $modeleName;
    }

        // Obtenir le nom complet de la classe
        $fullClassName = $this->getName($node->class);

        if (!$fullClassName || strpos($fullClassName, $this->namespacePrefix) !== 0) {
            return null;
        }

        // Extraire le nom court de la classe
        $shortClassName = substr($fullClassName, strlen($this->namespacePrefix));

        if (in_array($shortClassName, $nameShortModeles) === false) {
            return null;
        }

        // trouver le nom de la classe ou on fait le changement
        $scope = $node->getAttribute(\Rector\NodeTypeResolver\Node\AttributeKey::SCOPE);
        if ($scope instanceof \PHPStan\Analyser\Scope) {
            $scopeClass = $scope->getClassReflection();
            if ($scopeClass) {
                $className = $scopeClass->getName();
                // si la class est la même que celle du static call, on fait le changement pour this
                if($className === $fullClassName || $this->isName($node->class, 'self')){
                    return new MethodCall(new Variable('this'), $node->name, $node->args);
                }
            }
        }

        if($this->isName($node->class, 'self')){
            return new MethodCall(new Variable('this'), $node->name, $node->args);
        }

        // Construire le nom de la méthode d'instance
        $instanceMethodName = 'getModel' . $shortClassName;

        // Créer un nouvel appel de méthode d'instance
        $instanceMethodCall = new MethodCall(new Variable('this'), $instanceMethodName);

        // Créer un nouvel appel de méthode en utilisant le nom de la méthode originale
        return new MethodCall($instanceMethodCall, $node->name, $node->args);
    }

    public function getRuleDefinition(): RuleDefinition
    {
        return new RuleDefinition(
            'Dynamically replaces static calls with instance calls based on configuration',
            [
                new CodeSample(
                    'Classified::getById',
                    '$this->getModelClassified()->getById;'
                ),
            ]
        );
    }
}
