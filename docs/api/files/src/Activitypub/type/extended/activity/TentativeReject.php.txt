<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

class TentativeReject extends Reject
{
    /**
     * @var string
     */
    protected $type = 'TentativeReject';
}
