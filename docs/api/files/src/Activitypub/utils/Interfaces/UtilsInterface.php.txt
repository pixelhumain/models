<?php

namespace PixelHumain\Models\Activitypub\utils\Interfaces;

use Exception;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;

interface UtilsInterface
{
    public const PUBLIC_INBOX = "https://www.w3.org/ns/activitystreams#Public";

    public const URL_REGEX = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|(([^\s()<>]+|(([^\s()<>]+)))*))+(?:(([^\s()<>]+|(([^\s()<>]+)))*)|[^\s`!()[]{};:'\".,<>?«»“”‘’]))/";

    /**
     * Creates a URL using the provided endpoint.
     *
     * @param string $endpoint The endpoint to append to the URL.
     * @return string The complete URL.
     */
    public function createUrl(string $endpoint): string;

    /**
     * Decodes a JSON string into an associative array.
     *
     * @param string $value The JSON string to decode.
     * @return mixed The decoded JSON as an associative array.
     * @throws Exception If the JSON decoding fails.
     */
    public function decodeJson(string $value);

    /**
     * Retrieves the PEM string based on the given type.
     *
     * @param string $type The type of PEM string to retrieve.
     * @return string|null The PEM string if found, null otherwise.
     */
    public function getPem(string $type): ?string;

    /**
     * Returns the current UTC date in RFC7231 format.
     *
     * @return string The current UTC date.
     */
    public function getCurrentUTCDate(): string;

    /**
     * Retrieves all the IDs from an array of objects that implement the AbstractObjectInterface.
     *
     * @param array $objects An array of objects.
     * @return array An array containing all the IDs.
     */
    public function retrieveAllIdsFromObjects(array $objects): array;

    /**
     * Checks if a text contains a URL.
     *
     * @param string $str The text to check.
     * @return bool Returns true if the text contains a URL, false otherwise.
     */
    public function isTextHasUrl(string $str): bool;

    /**
     * Retrieves the URL of the website's favicon.
     *
     * @param string $url The URL of the website.
     * @return string The URL of the website's favicon.
     */
    public function getWebsiteFavicon(string $url): string;

    /**
     * Retrieves the base URL of the website.
     *
     * @param string $url The URL of the website.
     * @return string The base URL of the website.
     */
    public function getWebsiteBaseUrl(string $url): string;

    /**
     * Retrieves the domain of the website.
     *
     * @param string $url The URL of the website.
     * @return string The domain of the website.
     */
    public function getWebsiteDomain(string $url): string;

    /**
     * Wraps URLs in a text with HTML link tags.
     *
     * @param string $str The text containing URLs.
     * @return string The text with URLs wrapped in HTML link tags.
     */
    public function warpUrlIntoLinkTag(string $str): string;

    /**
     * Converts a string date to a MongoDB date.
     *
     * @param string $myDate The string date to convert.
     * @param string $label The label for the date.
     * @return mixed The converted MongoDB date.
     */
    public function stringDateToMongoDate(string $myDate, string $label);

    /**
     * Truncates a text to a specified number of characters and adds an optional ellipsis at the end.
     *
     * @param string $text The text to truncate.
     * @param int $maxchar The maximum number of characters.
     * @param string $end The optional ellipsis to add at the end. Default is '...'.
     * @return string The truncated text.
     */
    public function substrwords(string $text, int $maxchar, string $end = '...'): string;

    /**
     * Returns the MIME type of an image based on its file path.
     *
     * @param string $image_path The path to the image file.
     * @return string The MIME type of the image.
     */
    public function get_image_mime_type(string $image_path): string;

    /**
     * Generates a UUID (Universally Unique Identifier).
     *
     * @return string The generated UUID.
     */
    public function genUuid(): string;

    /**
     * Converts an object to an event data array.
     *
     * @param object $object The object to convert.
     * @param string|null $uuid The UUID of the object.
     * @return array The event data array.
     */
    public function parseToEvent($object, ?string $uuid);

    // TODO : $uuid n'est pas utilisé
    /**
     * Parses the given object into a project data array.
     *
     * @param object $object The object to parse.
     * @param string|null $uuid The UUID of the project.
     * @return array The parsed project data.
     */
    public function parseToProject($object, ?string $uuid);

    /**
     * Returns an array of CC recipients based on the given parameters.
     *
     * @param string $elementType The type of the element.
     * @param array|string $linkType The type of the link.
     * @param AbstractActorInterface $subject The subject of the activity.
     * @param AbstractActorInterface $invitor The invitor of the activity.
     * @return array The array of CC recipients.
     */
    public function getCCRecipients(string $elementType, $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): array;

    /**
     * Converts an ActivityPub object to an event.
     *
     * @param string $objectUUID The UUID of the object to convert.
     * @return array The converted event.
     */
    public function activitypubObjectToEvent(string $objectUUID): array;

    /**
     * Removes duplicate values from an array.
     *
     * @param array $array The array to deduplicate.
     * @return array The deduplicated array.
     */
    public function deduplicateArray(array $array): array;

    /**
     * Adds or removes a value from an array.
     *
     * @param array $my_array The array to modify.
     * @param array $value The value to add or remove.
     * @return array The modified array.
     */
    public function addOrRemoveIntoArray(array $my_array, array $value): array;

    /**
     * Adds the given value to the array if it is not already present.
     *
     * @param array $my_array The array to check and modify.
     * @param array $value The value to add to the array.
     * @return array The modified array.
     */
    public function addIfNotInArray(array $my_array, array $value): array;

    /**
     * Removes the elements from the given array if they exist in the value array.
     *
     * @param array $my_array The array from which elements will be removed.
     * @param array $value The array containing elements to be removed.
     * @return array The modified array after removing the elements.
     */
    public function removeIfExistInArray(array $my_array, array $value): array;

    /**
     * Slugify a tag.
     *
     * @param string $text The tag text to slugify.
     * @param int|null $length The maximum length of the slug (optional).
     * @return string The slugified tag.
     */
    public function slugifyTag(string $text, ?int $length = null): string;

    /**
     * Converts a coLocation array to an ActivityPub location array.
     *
     * @param array $location The coLocation array to convert.
     * @param string|null $uuid The UUID of the location.
     * @return array The converted ActivityPub location array.
     */
    public function coLocationToActivitypubLocation(array $location, ?string $uuid): array;

    /**
     * Converts a coTag with a specified type to an ActivityPub tag.
     *
     * @param array|null $tags The coTags to convert.
     * @param string $type The type of the coTags.
     * @return array The converted ActivityPub tags.
     */
    public function coTagWithTypeToActivitypubTag(?array $tags = null, string $type): array;

    /**
     * Converts a coTag to an Activitypub tag for editing.
     *
     * @param array|null $exist The existing tags.
     * @param array $tags The new tags.
     * @return array The converted Activitypub tags.
     */
    public function coTagToActivitypubTagEdit(?array $exist, array $tags): array;

    /**
     * Combines the existing tags with a new tag for an ActivityPub edit operation.
     *
     * @param array|null $exist The existing tags.
     * @param string $type The new tag to be added.
     * @return array The combined tags.
     */
    public function coTypeCombineToActivitypubTagEdit(?array $exist, string $type): array;

    /**
     * Converts a coTag with a given type to an ActivityPub tag for editing.
     *
     * @param array|null $exist The existing tags.
     * @param array $tags The new tags.
     * @param string $type The type of the tag.
     * @return array The converted ActivityPub tag.
     */
    public function coTagWithTypeToActivitypubTagEdit(?array $exist, array $tags, string $type): array;

    /**
     * Converts a social network array to an ActivityPub link array.
     *
     * @param array $socialNetwork The social network array to convert.
     * @return array The converted ActivityPub link array.
     */
    public function coSocialNetworkToActivitypubLink(array $socialNetwork): array;

    /**
     * Converts a coAttachment to an Activitypub attachment.
     *
     * @param array $data The coAttachment data.
     * @param array|null $exist The existing attachment data (optional).
     * @return array The converted Activitypub attachment.
     */
    public function coAttachmentToActivitypubAttachment(array $data, ?array $exist = null): array;

    /**
     * Converts a CoImage array to an ActivityPub Image array.
     *
     * @param array $res The CoImage array to convert.
     * @return array|null The converted ActivityPub Image array, or null if the conversion fails.
     */
    public function coImageToActivityPubImage(array $res): ?array;

    /**
     * Converts the coImageBanner to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param array $el The element array.
     * @return array The converted ActivityPub image.
     */
    public function coImageBannerToActivityPubImage(?array $objectArray, array $el): array;

    /**
     * Converts a coImageProfil to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param array $el The element array.
     * @return array The converted ActivityPub image.
     */
    public function coImageProfilToActivityPubImage(?array $objectArray, array $el): array;

    /**
     * Converts a thumbnail to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param string $el The element.
     * @return array The converted image array.
     */
    public function coThumbToActivityPubImage(?array $objectArray, string $el): array;

    /**
     * Merge two arrays by domain.
     *
     * @param array $exist The existing array.
     * @param array $newData The new data array.
     * @return array The merged array.
     */
    public function mergeArrayByDomain(array $exist, array $newData): array;

    /**
     * Check if the given instance is a Mobilizon instance.
     *
     * @param string $instance The instance to check.
     * @return bool Returns true if the instance is a Mobilizon instance, false otherwise.
     */
    public function isMobilizonInstance(string $instance): bool;

    /**
     * This method takes an array of job parameters and returns an array of modified job parameters.
     *
     * @param array $job The job parameters to be modified.
     * @return array The modified job parameters.
     */
    public function jobToRunBeforePost(array $job): array;

    /**
     * Federates an element.
     *
     * @param array $toSave The element to federate.
     * @return array|null The federated element or null if it couldn't be federated.
     */
    public function federateElement(array $toSave): ?array;

    /**
     * Returns an array of actor follows.
     *
     * @return array The list of actor follows.
     */
    public function actorFollowsList(): array;

    /**
     * Check if ActivityPub is enabled.
     *
     * @return bool Returns true if ActivityPub is enabled, false otherwise.
     */
    public function isActivitypubEnabled(): bool;

    /**
     * Check if the given actor is a local actor.
     *
     * @param AbstractActorInterface|null $actor The actor to check. Defaults to null.
     * @return bool Returns true if the actor is local, false otherwise.
     */
    public function isLocalActor(?AbstractActorInterface $actor = null): bool;

    /**
     * Returns a link to the address.
     *
     * @param string $url The URL of the address.
     * @return string|null The link to the address, or null if the URL is empty.
     */
    public static function getLinkToAdress(string $url): ?string;

    /**
     * Returns the link to the username based on the given URL.
     *
     * @param string $url The URL to extract the username from.
     * @return string|null The link to the username, or null if the URL is invalid.
     */
    public static function getLinkToUsername(string $url): ?string;

    /**
     * Check if the given array of links contains a specific connection type.
     *
     * @param array $links The array of links to check.
     * @param string $connectType The connection type to search for.
     * @return bool Returns true if the array contains the connection type, false otherwise.
     */
    public function checkIsContainLinks(array $links, string $connectType): bool;

    /**
     * Check if a link exists in an array of links based on the connection type and link type.
     *
     * @param array $links The array of links to check.
     * @param string $connectType The connection type to match.
     * @param string $linkType The link type to match.
     * @return bool Returns true if a link matching the connection type and link type is found, false otherwise.
     */
    public function checkByLinkType(array $links, string $connectType, string $linkType): bool;

    /**
     * Retrieves information about the links.
     *
     * @param array $links The array of links.
     * @param string $connectType The type of connection.
     * @return mixed The information about the links.
     */
    public function getLinksInfo(array $links, string $connectType);

    /**
     * Returns a list of admins for a given parent entity.
     *
     * @param string $parentId The ID of the parent entity.
     * @param string $parentType The type of the parent entity.
     * @return array|bool The list of admins.
     */
    public function listAdmins(string $parentId, string $parentType);

    /**
     * Check if the given parent is an admin.
     *
     * @param string $parentId The ID of the parent.
     * @param string $parentType The type of the parent.
     * @return bool Returns true if the parent is an admin, false otherwise.
     */
    public function checkIfIsAdmin(string $parentId, string $parentType): bool;

    /**
     * Builds a URL from an array of parts.
     *
     * @param array $parts The array of parts to build the URL from.
     * @return string The built URL.
     */
    public function buildUrlFromParts(array $parts): string;

    /**
     * Check if the co-user is a follower of a person.
     *
     * @param mixed $person The person to check if the co-user is a follower of.
     * @return bool Returns true if the co-user is a follower, false otherwise.
     */
    public function checkIfCoUserIsFollower($person): bool;

    /**
     * Check if the given element is a local share in the Fediverse.
     *
     * @param array $element The element to check.
     * @return bool Returns true if the element is a local share, false otherwise.
     */
    public function isFediverseShareLocal(array $element): bool;

    /**
     * Converts an ActivityPub object to an Element.
     *
     * @param mixed $objectId The ID of the ActivityPub object.
     * @return array The converted Element.
     */
    public function activitypubToElement($objectId): array;

    /**
     * Returns the collection of the given object.
     *
     * @param AbstractObjectInterface $object The object for which to retrieve the collection.
     * @return string The collection of the object.
     */
    public function getCollection(AbstractObjectInterface $object): string;

    /**
     * Retrieves the collection type based on the provided URL.
     *
     * @param string $url The URL of the collection.
     * @return array The collection type.
     */
    public function getCollectionTypeByUrl(string $url): array;

    /**
     * Filters the results for the ActivityPub flux.
     *
     * @param array $results The results to filter.
     * @return array The filtered results.
     */
    public function filterResultsForActivityPubFlux(array $results): array;

    /**
     * Filters the search admin data for the ActivityPub flux.
     *
     * @param string $type The type of the data.
     * @param string $id The ID of the data.
     * @param array $post The post data.
     * @param array $results The search results.
     * @return array The filtered search admin data.
     */
    public function filterSearchAdminDataForActivityPubFlux(string $type, string $id, array $post, array $results): array;

    /**
     * Display the first 30 words of a given text.
     *
     * @param string $text The text to display.
     * @return string The first 30 words of the text.
     */
    public function displayFirst30Words(string $text): string;
}
