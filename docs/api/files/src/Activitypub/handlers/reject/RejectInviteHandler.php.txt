<?php

namespace PixelHumain\Models\Activitypub\handlers\reject;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;

class RejectInviteHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivitypubActorTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, UtilsTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivitypubActorTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use UtilsTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    public function __construct(array $config = [], ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        if ($this->getModelUtils()->isMobilizonInstance($this->activity->get('attributedTo'))) {
            $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
            $target = $this->getModeltypeAp()->createFromAnyValue($object->get("object"));
            $this->getModelActivitypubActivity()->save($this->activity, [$target], $this->payload);
            $this->getModelActivitypubLink()->deleteLink(
                "followers",
                $this->actor->get("preferredUsername"),
                $target,
                $this->payload
            );
            //federate activity
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $target->get("inbox"),
                $this->activity->toArray(),
                true
            );
        } else {
            $targets = [];
            $targetIds = [];
            $targetInboxes = [];
            if (is_array($this->activity->get("cc"))) {
                $targetIds = $this->activity->get("cc");
            }
            foreach ($targetIds as $id) {
                if (isset($id)) {
                    $x = $this->getModeltypeAp()->createFromAnyValue($id);
                    $targetInboxes[] = $x->get('inbox');
                }
            }
            foreach ($targetIds as $id) {
                if ($id !== UtilsInterface::PUBLIC_INBOX) {
                    $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
                }
            }
            $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
            $target = $this->getModeltypeAp()->createFromAnyValue($object->get("target"));
            $actor = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("actor"));
            $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);
            $this->getModelActivitypubLink()->deleteProjectLink("contributors", $target, $actor);
            foreach ($targetInboxes as $inbox) {
                $this->getModelRequest()->post(
                    $this->actor->get("id"),
                    $inbox,
                    $this->activity->toArray(),
                    true
                );
            }
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->actor->get("id"));
        $acceptActivity = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $actor = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("actor"));
        $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("target"));
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, []);
        $this->getModelActivitypubLink()->deleteProjectLink("contributors", $target, $actor);
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
