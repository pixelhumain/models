<?php

namespace PixelHumain\Models\Activitypub\notification\Traits\Interfaces;

use PixelHumain\Models\Activitypub\notification\Interfaces\ActivityNotificationInterface;

interface ActivityNotificationTraitInterface
{
    /**
     * Set the activityNotification.
     *
     * @param ActivityNotificationInterface $activityNotification The activityNotification object to set.
     * @return void
     */
    public function setModelActivityNotification(ActivityNotificationInterface $activityNotification): void;

    /**
     * Gets the activityNotification.
     *
     * @return ActivityNotificationInterface The activityNotification.
     */
    public function getModelActivityNotification(): ActivityNotificationInterface;
}
