<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ConfigInterface;

interface ConfigTraitInterface
{
    /**
     * Set the config.
     *
     * @param ConfigInterface $config The config object to set.
     * @return void
     */
    public function setModelConfig(ConfigInterface $config): void;

    /**
     * Gets the config.
     *
     * @return ConfigInterface The config.
     */
    public function getModelConfig(): ConfigInterface;
}
