<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CO2StatInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;

class CO2Stat extends BaseModel implements CO2StatInterface, CostumTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use ParamsTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateParamsProperty();
    }

    /**
     * Increases the number of loads for a given hash.
     *
     * @param string $hash The hash value.
     * @return array The stats.
     */
    public function incNbLoad(string $hash): array
    {
        $week = $this->getTodayWeek();
        $stat = $this->getOrCreateWeekStat($week);
        $today = date("D");

        if (isset($stat["hash"][$hash][$today])) {
            $stat["hash"][$hash][$today]["nbLoad"] += 1;
            $this->updateStat($week, $stat);

            // Re-fetch the updated stats from the database
            $stat = $this->getOrCreateWeekStat($week);
        }

        return $stat;
    }

    /**
     * Retrieves or creates a weekly statistic based on the given week.
     *
     * @param string $week The week for which to retrieve or create the statistic.
     * @return array The statistic.
     */
    private function getOrCreateWeekStat(string $week): array
    {
        $query = $this->buildQuery($week);
        $stat = $this->db->findOne(CO2StatInterface::COLLECTION, $query);

        if ($stat !== null) {
            $stat = $this->initWeek($week);
        }

        return $stat ?: [];
    }

    /**
     * Builds a query for retrieving CO2 statistics for a given week.
     *
     * @param string $week The week for which to retrieve the statistics.
     * @return array The built query.
     */
    private function buildQuery(string $week): array
    {
        $query = [
            "week" => $week,
            "domainName" => $this->params->get("CO2DomainName"),
        ];
        $costumSlug = $this->getCostumSlug();
        if ($costumSlug) {
            $query = [
                '$and' => [
                    $query, [
                        "sourceKey" => $costumSlug,
                    ]],
            ];
        }
        return $query;
    }

    /**
     * Returns the custom slug for the CO2Stat model.
     *
     * @return string|null The custom slug or null if not set.
     */
    private function getCostumSlug(): ?string
    {
        $costum = $this->getModelCostum()->getCostum();
        return is_array($costum) && ! empty($costum["slug"]) ? (string) $costum["slug"] : null;
    }

    /**
     * Updates the CO2 stat for a specific week.
     *
     * @param string $week The week for which the CO2 stat is being updated.
     * @param array $stat The CO2 stat data to be updated.
     * @return void
     */
    private function updateStat(string $week, array $stat): void
    {
        $query = $this->buildQuery($week);
        unset($stat["_id"]);
        $this->db->update(CO2StatInterface::COLLECTION, $query, [
            '$set' => $stat,
        ]);
    }

    /**
     * Retrieves the statistics by hash.
     *
     * @param string|null $week The week to filter the statistics (optional).
     * @param string|null $hash The hash to filter the statistics (optional).
     * @return array The array containing the statistics.
     */
    public function getStatsByHash(?string $week = null, ?string $hash = null): array
    {
        $week ??= $this->getTodayWeek();
        $CO2DomainName = $this->params->get("CO2DomainName");
        $costumSlug = $this->getCostumSlug();

        $query = $this->buildQueryForStats($week, $CO2DomainName, $costumSlug);
        $stat = $this->db->findOne(CO2StatInterface::COLLECTION, $query);

        if ($stat == false) {
            $stat = $this->initWeek($week);
        }

        // extrait les données du hash demandé (s'il y en a un, sinon return les stats de tous les hash)
        $statByHash = ($hash != null && isset($stat["hash"][$hash])) ? $stat["hash"][$hash] : $stat;

        unset($statByHash["hash"]["co2-web"]);
        unset($statByHash["hash"]["co2-websearch"]);
        unset($statByHash["hash"]["co2-referencement"]);

        $statByHash["week"] = substr($week, 0, 2) . " - " . substr($week, 2, 4);
        $statByHash["numweek"] = intval(substr($week, 0, 2));
        $statByHash["year"] = substr($week, 2, 4);

        return $statByHash;
    }

    /**
     * Builds the query for retrieving statistics.
     *
     * @param int $week The week number.
     * @param string $domainName The domain name.
     * @param string|null $costumSlug The custom slug (optional).
     * @return array The built query.
     */
    private function buildQueryForStats($week, $domainName, ?string $costumSlug = null): array
    {
        $query = [
            "week" => $week,
            "domainName" => $domainName,
        ];
        if (! empty($costumSlug)) {
            $query = [
                '$and' => [
                    $query, [
                        "sourceKey" => $costumSlug,
                    ]],
            ];
        }
        return $query;
    }

    /**
     * Retrieves all custom statistics.
     *
     * @return array An array containing the custom statistics.
     */
    public function getStatsCustomAll(): array
    {
        $costumSlug = $this->getCostumSlug();

        $results = [
            "co2-login" => 1,
            "co2-web" => 1,
            "co2-websearch" => 1,
            "co2-referencement" => 0,
            "co2-page" => 0,
            "co2-dashboard" => 0,
            "co2-search" => 1,
            "co2-dda" => 1,
            "co2-live" => 0,
            "co2-info" => 0,
            "co2-annonces" => 0,
            "co2-agenda" => 0,
            "co2-welcome" => 1,
            "co2-admin" => 0,
        ];

        $stat = $this->db->find(CO2StatInterface::COLLECTION, [
            "sourceKey" => $costumSlug,
        ]);
        foreach ($stat as $y => $v) {
            foreach ($v["hash"] as $key => $days) {
                foreach ($days as $ok => $kk) {
                    if (isset($results[$key])) {
                        $results[$key] = ($results[$key] + $kk["nbLoad"]);
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Initializes the CO2Stat object for a specific week.
     *
     * @param string $week The week for which to initialize the CO2Stat object.
     * @return array The initialized CO2Stat object.
     */
    private function initWeek(string $week): ?array
    {
        $CO2DomainName = $this->params->get("CO2DomainName");
        $costumSlug = $this->getCostumSlug();

        $newWeekStat = [
            "week" => $week,
            "domainName" => $CO2DomainName,
            "hash" => $this->initializeHashDomains(),
        ];

        if ($costumSlug) {
            $newWeekStat["sourceKey"] = $costumSlug;
        }

        $this->db->insert(CO2StatInterface::COLLECTION, $newWeekStat);

        $query = $this->buildQueryForWeek($week, $costumSlug);
        return $this->db->findOne(CO2StatInterface::COLLECTION, $query);
    }

    /**
     * Initializes the hash domains.
     *
     * @return array The initialized hash domains.
     */
    private function initializeHashDomains(): array
    {
        $hash = [];
        foreach (CO2StatInterface::HASH_DOMAINS as $domain) {
            $hash[$domain] = $this->initializeWeekDays();
        }
        return $hash;
    }

    /**
     * Initializes the week days.
     *
     * @return array The array of week days.
     */
    private function initializeWeekDays(): array
    {
        return array_fill_keys(["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"], [
            "nbLoad" => 0,
        ]);
    }

    /**
     * Builds a query for a specific week.
     *
     * @param string $week The week to build the query for.
     * @param string|null $costumSlug The custom slug to filter the query by (optional).
     * @return array The built query.
     */
    private function buildQueryForWeek(string $week, ?string $costumSlug): array
    {
        $query = [
            "week" => $week,
        ];
        if ($costumSlug) {
            $query = [
                '$and' => [
                    $query, [
                        "sourceKey" => $costumSlug,
                    ]],
            ];
        }
        return $query;
    }

    /**
     * Returns the current week in string format.
     *
     * @return string The current week.
     */
    private function getTodayWeek(): string
    {
        $w = date("W");
        $y = date("Y");
        $week = $w . $y;
        return $week;
    }
}
