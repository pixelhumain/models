<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionsInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;


use PixelHumain\Models\Traits\ActionRoomTrait;
use PixelHumain\Models\Traits\ActionTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\Interfaces\ActionRoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;

class Actions extends BaseModel implements ActionsInterface, AuthorisationTraitInterface, ActionTraitInterface, LinkTraitInterface, CommentTraitInterface, ActionRoomTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AuthorisationTrait;
    use ActionTrait;
    use LinkTrait;
    use CommentTrait;
    use ActionRoomTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data if found, null otherwise.
     */
    public function getById(string $id): ?array
    {
        return $this->db->findOne(ActionsInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Check if a user can administrate a specific item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the item.
     * @return bool Returns true if the user can administrate the item, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool
    {
        $actionRoom = $this->getById($id);

        if (empty($actionRoom)) {
            return false;
        }

        $parentId = ! empty($actionRoom["parentId"]) ? (string) $actionRoom["parentId"] : null;
        $parentType = ! empty($actionRoom["parentType"]) ? (string) $actionRoom["parentType"] : null;

        $isAdmin = false;
        if ($parentType == FormInterface::ANSWER_COLLECTION && $userId == $actionRoom["creator"]) {
            $isAdmin = true;
        } elseif ($parentId && ($parentType == OrganizationInterface::COLLECTION || $parentType == ProjectInterface::COLLECTION || $parentType == EventInterface::COLLECTION)) {
            $isAdmin = $this->getModelAuthorisation()->canDeleteElement($parentId, $parentType, $userId);
        }
        return $isAdmin;
    }

    // TODO : $_POST
    /**
     * Closes an action.
     *
     * @param array $params The parameters for closing the action.
     * @return array The updated array after closing the action.
     */
    public function closeAction(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(ActionsInterface::COLLECTION, [
                "_id" => $this->db->MongoId((string) $params["id"]),
            ])) {
                if ($this->session->get("userEmail") == $action["email"]) {
                    //then remove the parent survey
                    $status = (! empty($action["status"]) && $action["status"] == ActionsInterface::ACTION_CLOSED) ? ActionsInterface::ACTION_INPROGRESS : ActionsInterface::ACTION_CLOSED;
                    $this->db->update(
                        ActionsInterface::COLLECTION,
                        [
                            "_id" => $action["_id"],
                        ],
                        [
                            '$set' => [
                                "status" => $status,
                            ],
                        ]
                    );
                    $this->getModelAction()->updateParent($_POST['id'], ActionsInterface::COLLECTION);
                    $res["result"] = true;
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    // TODO : $_POST['id']
    /**
     * Assigns the current user to the specified parameters.
     *
     * @param array $params The parameters to assign the current user to.
     * @return array The updated parameters.
     */
    public function assignMe(array $params): array
    {
        $res = [
            "result" => false,
        ];
        $userId = $this->session->getUserId() ?: null;

        if ($userId) {
            $id = ! empty($params["id"]) ? (string) $params["id"] : null;
            if (empty($id)) {
                $res["msg"] = "SurveydoesntExist";
                return $res;
            }
            $action = $this->db->findOne(ActionsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            if (! empty($action) && ! empty($action["parentType"]) && ! empty($action["parentId"])) {
                if ($this->getModelAuthorisation()->canParticipate($userId, (string) $action["parentType"], (string) $action["parentId"])) {
                    $res = $this->getModelLink()->connect($id, ActionsInterface::COLLECTION, $userId, PersonInterface::COLLECTION, $this->session->getUserId(), "contributors", true);
                    $this->getModelAction()->updateParent($_POST['id'], ActionsInterface::COLLECTION);
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    // TODO : $_POST['id']
    /**
     * Assigns the given parameters to the action.
     *
     * @param array $params The parameters to assign.
     * @return array The assigned parameters.
     */
    public function assign(array $params): array
    {
        $res = [
            "result" => false,
        ];
        $userId = $this->session->getUserId() ?: null;
        $id = ! empty($params["id"]) ? (string) $params["id"] : null;
        if (empty($id)) {
            $res["msg"] = "SurveydoesntExist";
            return $res;
        }
        if ($userId) {
            $action = $this->db->findOne(ActionsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            if ($action && ! empty($action["parentType"]) && ! empty($action["parentId"])) {
                if ($this->getModelAuthorisation()->canParticipate($userId, (string) $action["parentType"], (string) $action["parentId"])) {
                    $res = $this->getModelLink()->connect($id, ActionsInterface::COLLECTION, $params["idLink"], $params["typeLink"], $userId, $params["verbLink"]);
                    $this->getModelAction()->updateParent($_POST['id'], ActionsInterface::COLLECTION);
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    /**
     * Deletes an action.
     *
     * @param string $id The ID of the action to delete.
     * @param string $userId The ID of the user performing the action.
     * @return array The result of the delete operation.
     */
    public function deleteAction(string $id, string $userId): array
    {
        $resComment = [];
        $res = [
            "result" => false,
            "msg" => "Something went wrong : contact your admin !",
        ];
        ;

        $action = $this->getById($id);
        if (empty($action)) {
            return [
                "result" => false,
                "msg" => "The action does not exist",
            ];
        }

        if (! $this->canAdministrate($userId, $id)) {
            return [
                "result" => false,
                "msg" => "You must be admin of the parent of this action if you want delete it",
            ];
        }

        //Remove all comments linked
        if (isset($action["comment"])) {
            $resComment = $this->getModelComment()->deleteAllContextComments($id, ActionsInterface::COLLECTION, $userId);
        }

        if (isset($resComment["result"]) && ! @$resComment["result"]) {
            return $resComment;
        }

        //Remove the entry (survey)
        if ($this->db->remove(ActionsInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ])) {
            $res = [
                "result" => true,
                "msg" => "The action has been deleted with success",
            ];
        }

        return $res;
    }

    /**
     * Deletes all actions of the room.
     *
     * @param string $actionRoomId The ID of the action room.
     * @param string $userId The ID of the user.
     * @return array The result of the deletion operation.
     */
    public function deleteAllActionsOfTheRoom(string $actionRoomId, string $userId): array
    {
        $res = [];
        $canDelete = $this->getModelActionRoom()->canAdministrate($userId, $actionRoomId);
        if ($canDelete) {
            $where = [
                "room" => $actionRoomId,
            ];
            $actions = $this->db->find(ActionsInterface::COLLECTION, $where);
            foreach ($actions as $id => $action) {
                $res = $this->deleteAction($id, $userId);
            }
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "You are not allowed to delete this action room"),
            ];
        }

        if ($res["result"]) {
            $res = [
                "result" => true,
                "msg" => $this->language->t("common", "The actions of this action room have been deleted with success"),
            ];
        }

        return $res;
    }
}
