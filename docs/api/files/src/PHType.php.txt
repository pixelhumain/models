<?php

namespace PixelHumain\Models;

class PHType
{
    /*
    convert to Json Ld
    const TYPE_CITOYEN         = "persons";
    const TYPE_GROUPS          = "organizations";
    const TYPE_ASSOCIATION     = "NGO";
    const TYPE_ENTREPRISE      = "LocalBusiness";
    const TYPE_COLLECTIVITE    = "GovernmentOrganization";

     */
    public const TYPE_CITOYEN = "citoyens";

    public const TYPE_PERSON = "person";

    public const TYPE_ORGANIZATIONS = "organizations";

    public const TYPE_GROUPS = "groups";

    public const TYPE_ASSOCIATION = "association";

    public const TYPE_ENTREPRISE = "entreprise";

    public const TYPE_COLLECTIVITE = "collectivite";

    public const TYPE_EVENTS = "events";

    public const TYPE_PROJECTS = "projects";

    public const TYPE_JOBS = "jobPosting";

    public const TYPE_DISCUSSION = "discussion";

    public const TYPE_APPLICATIONS = "applications";

    public const TYPE_SURVEYS = "surveys";

    public const TYPE_ACTIVITYSTREAM = "activityStream";

    public const TYPE_MICROFORMATS = "microformats";

    public const TYPE_LISTS = "lists";

    public const TYPE_MESSAGES = "messages";

    public const TYPE_JOBTYPES = "jobTypes";

    public const TYPE_NEWS = "news";

    public const TYPE_LAYOUT = "layout";

    /* Standard connection types, the user can then create his own groupings*/
    public const CONNECT_TYPE_FRIEND = "friend";

    public const CONNECT_TYPE_WORK = "work";

    public const CONNECT_TYPE_CONTACT = "contact";

    public const COLLECTION_GROUPS = "groups";
}
