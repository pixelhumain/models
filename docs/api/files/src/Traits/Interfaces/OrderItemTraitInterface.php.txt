<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\OrderItemInterface;

interface OrderItemTraitInterface
{
    /**
     * Set the orderItem.
     *
     * @param OrderItemInterface $orderItem The orderItem object to set.
     * @return void
     */
    public function setModelOrderItem(OrderItemInterface $orderItem): void;

    /**
     * Gets the orderItem.
     *
     * @return OrderItemInterface The orderItem.
     */
    public function getModelOrderItem(): OrderItemInterface;
}
