<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActionRoomInterface;

interface ActionRoomTraitInterface
{
    /**
     * Set the actionRoom.
     *
     * @param ActionRoomInterface $actionRoom The actionRoom object to set.
     * @return void
     */
    public function setModelActionRoom(ActionRoomInterface $actionRoom): void;

    /**
     * Gets the actionRoom.
     *
     * @return ActionRoomInterface The actionRoom.
     */
    public function getModelActionRoom(): ActionRoomInterface;
}
