<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActionsInterface;

interface ActionsTraitInterface
{
    /**
     * Set the actions.
     *
     * @param ActionsInterface $actions The actions object to set.
     * @return void
     */
    public function setModelActions(ActionsInterface $actions): void;

    /**
     * Gets the actions.
     *
     * @return ActionsInterface The actions.
     */
    public function getModelActions(): ActionsInterface;
}
