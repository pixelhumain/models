<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\EventInterface;

trait EventTrait
{
    protected ?EventInterface $event = null;

    /**
     * Set the event.
     *
     * @param EventInterface $event The event object to set.
     * @return void
     */
    public function setModelEvent(EventInterface $event): void
    {
        $this->event = $event;
    }

    /**
     * Gets the event.
     *
     * @return EventInterface The event.
     */
    public function getModelEvent(): EventInterface
    {
        if ($this->event === null) {
            throw new Exception(EventInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->event;
    }
}
