<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CleanTagsInterface;

trait CleanTagsTrait
{
    protected ?CleanTagsInterface $cleanTags = null;

    /**
     * Set the cleanTags.
     *
     * @param CleanTagsInterface $cleanTags The cleanTags object to set.
     * @return void
     */
    public function setModelCleanTags(CleanTagsInterface $cleanTags): void
    {
        $this->cleanTags = $cleanTags;
    }

    /**
     * Gets the cleanTags.
     *
     * @return CleanTagsInterface The cleanTags.
     */
    public function getModelCleanTags(): CleanTagsInterface
    {
        if ($this->cleanTags === null) {
            throw new Exception(CleanTagsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->cleanTags;
    }
}
