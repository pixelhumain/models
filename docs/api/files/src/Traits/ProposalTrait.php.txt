<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ProposalInterface;

trait ProposalTrait
{
    protected ?ProposalInterface $proposal = null;

    /**
     * Set the proposal.
     *
     * @param ProposalInterface $proposal The proposal object to set.
     * @return void
     */
    public function setModelProposal(ProposalInterface $proposal): void
    {
        $this->proposal = $proposal;
    }

    /**
     * Gets the proposal.
     *
     * @return ProposalInterface The proposal.
     */
    public function getModelProposal(): ProposalInterface
    {
        if ($this->proposal === null) {
            throw new Exception(ProposalInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->proposal;
    }
}
