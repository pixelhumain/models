<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CO2StatInterface;

trait CO2StatTrait
{
    protected ?CO2StatInterface $cO2Stat = null;

    /**
     * Set the cO2Stat.
     *
     * @param CO2StatInterface $cO2Stat The cO2Stat object to set.
     * @return void
     */
    public function setModelCO2Stat(CO2StatInterface $cO2Stat): void
    {
        $this->cO2Stat = $cO2Stat;
    }

    /**
     * Gets the cO2Stat.
     *
     * @return CO2StatInterface The cO2Stat.
     */
    public function getModelCO2Stat(): CO2StatInterface
    {
        if ($this->cO2Stat === null) {
            throw new Exception(CO2StatInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->cO2Stat;
    }
}
