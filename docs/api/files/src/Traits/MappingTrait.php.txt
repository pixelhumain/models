<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MappingInterface;

trait MappingTrait
{
    protected ?MappingInterface $mapping = null;

    /**
     * Set the mapping.
     *
     * @param MappingInterface $mapping The mapping object to set.
     * @return void
     */
    public function setModelMapping(MappingInterface $mapping): void
    {
        $this->mapping = $mapping;
    }

    /**
     * Gets the mapping.
     *
     * @return MappingInterface The mapping.
     */
    public function getModelMapping(): MappingInterface
    {
        if ($this->mapping === null) {
            throw new Exception(MappingInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->mapping;
    }
}
