<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SmartDataInterface;

trait SmartDataTrait
{
    protected ?SmartDataInterface $smartData = null;

    /**
     * Set the smartData.
     *
     * @param SmartDataInterface $smartData The smartData object to set.
     * @return void
     */
    public function setModelSmartData(SmartDataInterface $smartData): void
    {
        $this->smartData = $smartData;
    }

    /**
     * Gets the smartData.
     *
     * @return SmartDataInterface The smartData.
     */
    public function getModelSmartData(): SmartDataInterface
    {
        if ($this->smartData === null) {
            throw new Exception(SmartDataInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->smartData;
    }
}
