<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ClassifiedInterface;

trait ClassifiedTrait
{
    protected ?ClassifiedInterface $classified = null;

    /**
     * Set the classified.
     *
     * @param ClassifiedInterface $classified The classified object to set.
     * @return void
     */
    public function setModelClassified(ClassifiedInterface $classified): void
    {
        $this->classified = $classified;
    }

    /**
     * Gets the classified.
     *
     * @return ClassifiedInterface The classified.
     */
    public function getModelClassified(): ClassifiedInterface
    {
        if ($this->classified === null) {
            throw new Exception(ClassifiedInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->classified;
    }
}
