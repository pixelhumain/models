<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\RocketChatInterface;

trait RocketChatTrait
{
    protected ?RocketChatInterface $rocketChat = null;

    /**
     * Set the rocketChat.
     *
     * @param RocketChatInterface $rocketChat The rocketChat object to set.
     * @return void
     */
    public function setRocketChat(RocketChatInterface $rocketChat): void
    {
        $this->rocketChat = $rocketChat;
    }

    /**
     * Gets the rocketChat.
     *
     * @return RocketChatInterface The rocketChat.
     */
    public function getRocketChat(): RocketChatInterface
    {
        if ($this->rocketChat === null) {
            throw new Exception(RocketChatInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->rocketChat;
    }
}
