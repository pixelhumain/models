<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ZoneInterface;

trait ZoneTrait
{
    protected ?ZoneInterface $zone = null;

    /**
     * Set the zone.
     *
     * @param ZoneInterface $zone The zone object to set.
     * @return void
     */
    public function setModelZone(ZoneInterface $zone): void
    {
        $this->zone = $zone;
    }

    /**
     * Gets the zone.
     *
     * @return ZoneInterface The zone.
     */
    public function getModelZone(): ZoneInterface
    {
        if ($this->zone === null) {
            throw new Exception(ZoneInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->zone;
    }
}
