<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateDatagouvToPhInterface;

trait TranslateDatagouvToPhTrait
{
    protected ?TranslateDatagouvToPhInterface $translateDatagouvToPh = null;

    /**
     * Set the translateDatagouvToPh.
     *
     * @param TranslateDatagouvToPhInterface $translateDatagouvToPh The translateDatagouvToPh object to set.
     * @return void
     */
    public function setTranslateDatagouvToPh(TranslateDatagouvToPhInterface $translateDatagouvToPh): void
    {
        $this->translateDatagouvToPh = $translateDatagouvToPh;
    }

    /**
     * Gets the translateDatagouvToPh.
     *
     * @return TranslateDatagouvToPhInterface The translateDatagouvToPh.
     */
    public function getTranslateDatagouvToPh(): TranslateDatagouvToPhInterface
    {
        if ($this->translateDatagouvToPh === null) {
            throw new Exception(TranslateDatagouvToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateDatagouvToPh;
    }
}
