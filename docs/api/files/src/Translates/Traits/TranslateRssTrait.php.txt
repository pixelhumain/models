<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateRssInterface;

trait TranslateRssTrait
{
    protected ?TranslateRssInterface $translateRss = null;

    /**
     * Set the translateRss.
     *
     * @param TranslateRssInterface $translateRss The translateRss object to set.
     * @return void
     */
    public function setTranslateRss(TranslateRssInterface $translateRss): void
    {
        $this->translateRss = $translateRss;
    }

    /**
     * Gets the translateRss.
     *
     * @return TranslateRssInterface The translateRss.
     */
    public function getTranslateRss(): TranslateRssInterface
    {
        if ($this->translateRss === null) {
            throw new Exception(TranslateRssInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateRss;
    }
}
