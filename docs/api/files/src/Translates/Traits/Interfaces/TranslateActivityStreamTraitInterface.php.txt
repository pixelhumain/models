<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateActivityStreamInterface;

interface TranslateActivityStreamTraitInterface
{
    /**
     * Set the translateActivityStream.
     *
     * @param TranslateActivityStreamInterface $translateActivityStream The translateActivityStream object to set.
     * @return void
     */
    public function setTranslateActivityStream(TranslateActivityStreamInterface $translateActivityStream): void;

    /**
     * Gets the translateActivityStream.
     *
     * @return TranslateActivityStreamInterface The translateActivityStream.
     */
    public function getTranslateActivityStream(): TranslateActivityStreamInterface;
}
