<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateWikiToPhInterface;

interface TranslateWikiToPhTraitInterface
{
    /**
     * Set the translateWikiToPh.
     *
     * @param TranslateWikiToPhInterface $translateWikiToPh The translateWikiToPh object to set.
     * @return void
     */
    public function setTranslateWikiToPh(TranslateWikiToPhInterface $translateWikiToPh): void;

    /**
     * Gets the translateWikiToPh.
     *
     * @return TranslateWikiToPhInterface The translateWikiToPh.
     */
    public function getTranslateWikiToPh(): TranslateWikiToPhInterface;
}
