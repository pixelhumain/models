<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateCommunecterInterface;

trait TranslateCommunecterTrait
{
    protected ?TranslateCommunecterInterface $translateCommunecter = null;

    /**
     * Set the translateCommunecter.
     *
     * @param TranslateCommunecterInterface $translateCommunecter The translateCommunecter object to set.
     * @return void
     */
    public function setTranslateCommunecter(TranslateCommunecterInterface $translateCommunecter): void
    {
        $this->translateCommunecter = $translateCommunecter;
    }

    /**
     * Gets the translateCommunecter.
     *
     * @return TranslateCommunecterInterface The translateCommunecter.
     */
    public function getTranslateCommunecter(): TranslateCommunecterInterface
    {
        if ($this->translateCommunecter === null) {
            throw new Exception(TranslateCommunecterInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateCommunecter;
    }
}
