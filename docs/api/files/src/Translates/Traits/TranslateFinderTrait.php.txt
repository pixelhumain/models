<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateFinderInterface;

trait TranslateFinderTrait
{
    protected ?TranslateFinderInterface $translateFinder = null;

    /**
     * Set the translateFinder.
     *
     * @param TranslateFinderInterface $translateFinder The translateFinder object to set.
     * @return void
     */
    public function setTranslateFinder(TranslateFinderInterface $translateFinder): void
    {
        $this->translateFinder = $translateFinder;
    }

    /**
     * Gets the translateFinder.
     *
     * @return TranslateFinderInterface The translateFinder.
     */
    public function getTranslateFinder(): TranslateFinderInterface
    {
        if ($this->translateFinder === null) {
            throw new Exception(TranslateFinderInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateFinder;
    }
}
