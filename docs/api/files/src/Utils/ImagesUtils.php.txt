<?php

namespace PixelHumain\Models\Utils;

use Exception;
use PixelHumain\Models\Services\Interfaces\FsServiceInterface;
use PixelHumain\Models\Utils\Interfaces\ImagesUtilsInterface;

/**
 * Images manipulation
 */
class ImagesUtils implements ImagesUtilsInterface
{
    /**
     * @var resource $srcImage The source image path.
     */
    private $srcImage;

    public int $source_width;

    public int $source_height;

    public string $source_type;

    /**
     * @var resource $destImage The source image path.
     */
    private $destImage;

    private string $dest_type;

    private ?int $dest_width = null;

    private ?int $dest_height = null;

    private FsServiceInterface $fs;

    /**
     * Constructor for the ImagesUtils class.
     *
     * @param FsServiceInterface $fs The file system service interface.
     * @param string $srcImage The source image path.
     */
    public function __construct(FsServiceInterface $fs, string $srcImage)
    {
        $this->fs = $fs;

        $contents = $this->fs->read($srcImage);
        $mime = $this->fs->mimeType($srcImage);
        $im = imagecreatefromstring($contents);

        if (! $im) {
            throw new Exception("Error loading image");
        }

        $this->source_width = imagesx($im);
        $this->source_height = imagesy($im);
        $this->source_type = $mime;

        switch ($this->source_type) {
            case 'image/gif':
                $this->srcImage = $im;
                break;
            case 'image/jpeg':
                $this->srcImage = $im;
                break;
            case 'image/png':
                $this->srcImage = $im;
                break;
        }
        //By default the destination type is the same than the source type
        $this->dest_type = $this->source_type;
    }

    /**
     * Destructor for the ImagesUtils class.
     */
    public function __destruct()
    {
        /**
         * @psalm-suppress RedundantConditionGivenDocblockType
         */
        if (is_resource($this->srcImage)) {
            imagedestroy($this->srcImage);
        }
        /**
         * @psalm-suppress RedundantConditionGivenDocblockType
         */
        if (is_resource($this->destImage)) {
            imagedestroy($this->destImage);
        }
    }

    /**
     * Display method.
     *
     * @return void
     */
    public function display(): void
    {
        header('Content-type: image/png');
        imagepng($this->destImage);
    }

    /**
     * Saves an image to the specified destination path with the given quality.
     *
     * @param string $destImagePath The destination path where the image will be saved.
     * @param string $quality The quality of the saved image (default is 100).
     * @return void
     */
    public function save(string $destImagePath, string $quality = "100"): void
    {
        //Save Image
        ob_start();
        $quality = intval($quality);
        switch ($this->dest_type) {
            case 'image/gif':
                imagegif($this->destImage, null);
                break;
            case 'image/jpeg':
                imagejpeg($this->destImage, null, $quality);
                break;
            case 'image/png':
                $q = 9 / 100;
                $quality *= $q;
                imagepng($this->destImage, null, intval($quality));
                break;
        }
        $final_image = ob_get_contents();
        ob_end_clean();
        $this->fs->write($destImagePath, $final_image, [
            'visibility' => 'public',
        ]);
    }

    /**
     * Saves an image as a PNG file.
     *
     * @param string $destImagePath The path where the PNG file will be saved.
     * @param string $quality The quality of the PNG file (optional, default is "100").
     * @return void
     */
    public function savePng(string $destImagePath, string $quality = "100"): void
    {
        ob_start();
        $quality = intval($quality);
        $q = 9 / 100;
        $quality *= $q;
        imagepng($this->destImage, null, intval($quality));
        $final_image = ob_get_contents();
        ob_end_clean();
        $this->fs->write($destImagePath, $final_image, [
            'visibility' => 'public',
        ]);
    }

    /**
     * Resize the image to the specified dimensions.
     *
     * @param int $newwidth The new width of the image.
     * @param int $newheight The new height of the image.
     * @return ImagesUtilsInterface The resized image.
     */
    public function resizeImage(int $newwidth, int $newheight): ImagesUtilsInterface
    {
        $source_aspect_ratio = $this->source_width / $this->source_height;
        $desired_aspect_ratio = $newwidth / $newheight;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            // Triggered when source image is wider
            $temp_height = $newheight;
            $temp_width = (int) ($newheight * $source_aspect_ratio);
        } else {
            // Triggered otherwise (i.e. source image is similar or taller)
            $temp_width = $newwidth;
            $temp_height = (int) ($newwidth / $source_aspect_ratio);
        }

        /*
         * Resize the image into a temporary GD image
         */
        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagealphablending($temp_gdim, false);
        imagesavealpha($temp_gdim, true);

        imagecopyresampled(
            $temp_gdim,
            $this->srcImage,
            0,
            0,
            0,
            0,
            $temp_width,
            $temp_height,
            $this->source_width,
            $this->source_height
        );

        /*
         * Copy cropped region from temporary image into the desired GD image
         */
        $x0 = ($temp_width - $newwidth) / 2;
        $y0 = ($temp_height - $newheight) / 2;
        $desired_gdim = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($desired_gdim, false);
        imagesavealpha($desired_gdim, true);
        imagecopyresampled(
            $desired_gdim,
            $temp_gdim,
            0,
            0,
            $x0,
            $y0,
            $newwidth,
            $newheight,
            $temp_width,
            $temp_height
        );

        $this->destImage = $desired_gdim;
        imagedestroy($temp_gdim);
        //imagedestroy($desired_gdim);

        return $this;
    }

    /**
     * Resizes and crops an image.
     *
     * @param int $newwidth The new width of the image.
     * @param int $newheight The new height of the image.
     * @param array $crop The crop parameters for the image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizeAndCropImage(int $newwidth = 1300, int $newheight = 400, array $crop): ImagesUtilsInterface
    {
        $temp_gdim = imagecreatetruecolor($this->source_width, $this->source_height);
        imagecopyresampled(
            $temp_gdim,
            $this->srcImage,
            0,
            0,
            intval($crop["cropX"]),
            intval($crop["cropY"]),
            (intval($crop["cropW"]) + intval($crop["cropX"])),
            (intval($crop["cropH"]) + intval($crop["cropY"])),
            $this->source_width,
            $this->source_height
        );
        $this->destImage = $temp_gdim;
        imagedestroy($this->srcImage);
        return $this;
    }

    /**
     * Crop the image to the specified dimensions.
     *
     * @param int $new_width The width of the cropped image.
     * @param int $new_height The height of the cropped image.
     * @param int $x The x-coordinate of the starting point for cropping. Default is 0.
     * @param int $y The y-coordinate of the starting point for cropping. Default is 0.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function imagecropping(int $new_width, int $new_height, int $x = 0, int $y = 0): ImagesUtilsInterface
    {
        $im = imagecreatetruecolor($new_width, $new_height);
        imagealphablending($im, false);
        imagesavealpha($im, true);

        imagecopyresampled($im, $this->srcImage, 0, 0, $x, $y, $new_width, $new_height, $this->source_width, $this->source_height);

        $this->destImage = $im;
        imagedestroy($this->srcImage);
        return $this;
    }

    /**
     * Resize an image proportionally.
     *
     * @param int $newwidth The desired width of the resized image.
     * @param int $newheight The desired height of the resized image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizePropertionalyImage(int $newwidth, int $newheight): ImagesUtilsInterface
    {
        $width = $this->source_width;
        $height = $this->source_height;

        # taller
        if ($height > $newheight) {
            $width = ($newheight / $height) * $width;
            $height = $newheight;
            $resized = true;
        }

        # wider
        if ($width > $newwidth) {
            $height = ($newwidth / $width) * $height;
            $width = $newwidth;
            $resized = true;
        }

        $temp_width = $width;
        $temp_height = $height;

        $temp_gdim = imagecreatetruecolor(intval($temp_width), intval($temp_height));
        imagealphablending($temp_gdim, false);
        imagesavealpha($temp_gdim, true);
        imagecopyresampled(
            $temp_gdim,
            $this->srcImage,
            0,
            0,
            0,
            0,
            intval($temp_width),
            intval($temp_height),
            $this->source_width,
            $this->source_height
        );
        $this->destImage = $temp_gdim;
        imagedestroy($this->srcImage);
        return $this;
    }

    /**
     * Creates a circle image with the specified dimensions.
     *
     * @param int $newwidth The width of the new image.
     * @param int $newheight The height of the new image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function createCircleImage(int $newwidth, int $newheight): ImagesUtilsInterface
    {
        //There will be transparency around the circle so dest Type is png
        $this->dest_type = 'image/png';

        $this->resizeImage($newwidth, $newheight);
        $square = imagesx($this->destImage) < imagesy($this->destImage) ? imagesx($this->destImage) : imagesy($this->destImage);
        $width = $square;
        $height = $square;
        $this->circleCrop($width, $height);
        return $this;
    }

    /**
     * Crop the image into a circle shape.
     *
     * @param int $newwidth The width of the cropped image.
     * @param int $newheight The height of the cropped image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    private function circleCrop(int $newwidth, int $newheight): ImagesUtilsInterface
    {
        $mask = imagecreatetruecolor($newwidth, $newheight);
        imagealphablending($mask, false);

        $maskTransparent = imagecolorallocate($mask, 255, 0, 255);
        imagecolortransparent($mask, $maskTransparent);
        imagefilledellipse($mask, intval($newwidth / 2), intval($newheight / 2), $newwidth, $newheight, $maskTransparent);

        imagecopymerge($this->destImage, $mask, 0, 0, 0, 0, $newwidth, $newheight, 100);
        $dstTransparent = imagecolorallocate($this->destImage, 255, 0, 255);
        imagefill($this->destImage, 0, 0, $dstTransparent);
        imagefill($this->destImage, $newwidth - 1, 0, $dstTransparent);
        imagefill($this->destImage, 0, $newheight - 1, $dstTransparent);
        imagefill($this->destImage, $newwidth - 1, $newheight - 1, $dstTransparent);
        imagecolortransparent($this->destImage, $dstTransparent);
        return $this;
    }

    /**
     * Crée un marqueur à partir d'une image.
     *
     * @param string $srcEmptyMarker Le chemin de l'image du marqueur vide.
     * @return ImagesUtilsInterface Une instance de l'interface ImagesUtilsInterface.
     */
    public function createMarkerFromImage(string $srcEmptyMarker): ImagesUtilsInterface
    {
        //There will be transparency around the circle so dest Type is png
        $this->dest_type = 'image/png';

        //Create a circle image
        $this->createCircleImage(40, 40);

        $source = $this->destImage;

        $destination = imagecreatefrompng($srcEmptyMarker);
        imagealphablending($destination, false);
        imagesavealpha($destination, true);

        // On charge d'abord les images
        // Les fonctions imagesx et imagesy renvoient la largeur et la hauteur d'une image
        $largeur_source = imagesx($source);
        $hauteur_source = imagesy($source);
        $largeur_destination = imagesx($destination);
        $hauteur_destination = imagesy($destination);

        // On veut placer le logo au centre du marker
        $destination_x = 6;
        $destination_y = 6;

        // On met le logo (source) dans l'image de destination (le marker)
        imagecopymerge($destination, $source, $destination_x, $destination_y, 0, 0, $largeur_source, $hauteur_source, 100);

        $this->destImage = $destination;

        return $this;
    }

    /**
     * Transforms transparency in the image to white.
     *
     * @return void
     */
    public function transformTransparencyToWhite(): void
    {
        $this->destImage = imagecreatetruecolor($this->source_width, $this->source_height);
        $white = imagecolorallocate($this->destImage, 255, 255, 255);
        imagefilledrectangle($this->destImage, 0, 0, $this->source_width, $this->source_height, $white);
        imagecopy($this->destImage, $this->srcImage, 0, 0, 0, 0, $this->source_width, $this->source_height);
    }

    /**
     * Clones an image resource.
     *
     * @param resource $img The image resource to clone.
     * @return resource The cloned image resource.
     */
    private function _clone_img_resource($img)
    {
        //Get width from image.
        $w = imagesx($img);
        //Get height from image.
        $h = imagesy($img);
        //Get the transparent color from a 256 palette image.
        $trans = imagecolortransparent($img);

        //If this is a true color image...
        if (imageistruecolor($img)) {
            $clone = imagecreatetruecolor($w, $h);
            imagealphablending($clone, false);
            imagesavealpha($clone, true);
        }
        //If this is a 256 color palette image...
        else {
            $clone = imagecreate($w, $h);

            //If the image has transparency...
            if ($trans >= 0) {
                $rgb = imagecolorsforindex($img, $trans);

                imagesavealpha($clone, true);
                $trans_index = imagecolorallocatealpha($clone, intval($rgb['red']), intval($rgb['green']), intval($rgb['blue']), intval($rgb['alpha']));
                imagefill($clone, 0, 0, $trans_index);
            }
        }

        //Create the Clone!!
        imagecopy($clone, $img, 0, 0, 0, 0, $w, $h);

        return $clone;
    }

    /**
     * Resizes the image into a badge.
     *
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizeIntoBadge(): ImagesUtilsInterface
    {
        $square_dimensions = 400;
        // Badges need transparency activated
        $this->dest_type = 'image/png';

        // Step one: Rezise with proportion the src_file *** I found this in many places.
        $src_img = $this->srcImage;

        imagesavealpha($src_img, true);

        $old_x = imageSX($src_img);
        $old_y = imageSY($src_img);

        $ratio1 = $old_x / $square_dimensions;
        $ratio2 = $old_y / $square_dimensions;

        if ($ratio1 > $ratio2) {
            $thumb_w = $square_dimensions;
            $thumb_h = $old_y / $ratio1;
        } else {
            $thumb_h = $square_dimensions;
            $thumb_w = $old_x / $ratio2;
        }
        $smaller_image_with_proportions = ImageCreateTrueColor(intval($thumb_w), intval($thumb_h));
        imagealphablending($smaller_image_with_proportions, false);
        imagesavealpha($smaller_image_with_proportions, true);
        imagecopyresampled($smaller_image_with_proportions, $src_img, 0, 0, 0, 0, intval($thumb_w), intval($thumb_h), $old_x, $old_y);

        $final_image = imagecreatetruecolor($square_dimensions, $square_dimensions);
        imagealphablending($final_image, false);
        imagesavealpha($final_image, true);
        imagefill($final_image, 0, 0, 0xffffffff);

        if ($thumb_w > $thumb_h) {
            $dst_x = 0;
            $dst_y = ($square_dimensions - $thumb_h) / 2;
        } elseif ($thumb_h > $thumb_w) {
            $dst_x = ($square_dimensions - $thumb_w) / 2;
            $dst_y = 0;
        } else {
            $dst_x = 0;
            $dst_y = 0;
        }

        $src_x = 0;
        $src_y = 0;

        $src_w = $thumb_w;
        $src_h = $thumb_h;

        $pct = 100;

        imagecopy($final_image, $smaller_image_with_proportions, intval($dst_x), intval($dst_y), $src_x, $src_y, intval($src_w), intval($src_h));
        $this->destImage = $final_image;
        return $this;
    }
}
