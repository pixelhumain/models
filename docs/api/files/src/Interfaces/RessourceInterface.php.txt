<?php

namespace PixelHumain\Models\Interfaces;

interface RessourceInterface
{
    public const COLLECTION = "ressources";

    public const CONTROLLER = "ressources";

    public const MODULE = "ressources";

    public const TYPE_NEED = "needs";

    public const TYPE_OFFER = "offers";

    public const ICON = "fa-cubes";
}
