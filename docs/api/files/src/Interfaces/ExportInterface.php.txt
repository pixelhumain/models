<?php

namespace PixelHumain\Models\Interfaces;

interface ExportInterface
{
    /**
     * Converts an array of data to a CSV string and outputs it to the file system.
     *
     * @param array $data The data to be converted.
     * @param string $separ The separator character for the CSV fields. Default is ";".
     * @param string $separText The separator character for text values within the CSV fields. Default is " ".
     * @param bool $notPath Whether to exclude file paths from the CSV. Default is false.
     * @param bool $niv1 Whether to include the first level of the array in the CSV. Default is true.
     * @param mixed $type The type of data to be exported. Default is null.
     * @param array $order The order in which the data should be exported. Default is an empty array.
     * @return void
     */
    public function toCSV(array $data, string $separ = ';', string $separText = ' ', bool $notPath = false, bool $niv1 = true, $type = null, array $order = []): void;

    /**
     * Retrieves the member of a specific type for a given ID.
     *
     * @param string $id The ID of the member.
     * @param string $type The type of the member.
     * @return array The array of member data.
     */
    public function getMemberOf(string $id, string $type): array;

    /**
     * Reformats the data fields mongo to be exported.
     *
     * @param array $data The data to be reformatted.
     * @return array The reformatted data.
     */
    public function getMongoParam(array $data): array;

    /**
     * Converts an array of data to a CSV string.
     *
     * @param array $data The data to be converted.
     * @param string $separ The separator character for the CSV fields. Default is ";".
     * @param string $separText The separator character for text values within the CSV fields. Default is " ".
     * @param bool $notPath Indicates whether to exclude file paths from the CSV. Default is false.
     * @param bool $niv1 Indicates whether to include the first level of the array in the CSV. Default is true.
     * @param mixed $type The type of data to be exported. Default is null.
     * @param array $order The order in which the data should be exported. Default is an empty array.
     * @return mixed The CSV.
     */
    public function toCSV2(array $data, string $separ = ';', string $separText = ' ', bool $notPath = false, bool $niv1 = true, $type = null, array $order = []);
}
