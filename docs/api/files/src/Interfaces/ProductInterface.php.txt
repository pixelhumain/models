<?php

namespace PixelHumain\Models\Interfaces;

interface ProductInterface
{
    public const COLLECTION = "products";

    public const CONTROLLER = "product";
}
