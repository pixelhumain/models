<?php

namespace PixelHumain\Models;

use DateTime;
use PixelHumain\Models\Interfaces\OrganizationInterface;

use PixelHumain\Models\Interfaces\TranslateInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Translates\Traits\Interfaces\TranslateFtlTraitInterface;
use PixelHumain\Models\Translates\Traits\Interfaces\TranslateRssTraitInterface;
use PixelHumain\Models\Translates\Traits\TranslateFtlTrait;
use PixelHumain\Models\Translates\Traits\TranslateRssTrait;
use PixelHumain\Models\Translates\TranslateFtl;
use PixelHumain\Models\Translates\TranslateGeojson;
use PixelHumain\Models\Translates\TranslateKml;

// TODO : Yii::get('urlManager')->createUrl
// TODO : date_default_timezone_set

class Translate implements TranslateInterface, TranslateRssTraitInterface, TranslateFtlTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;
    use DbTrait;
    use SessionTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use TranslateRssTrait;
    use TranslateFtlTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
        $this->validateSessionProperty();
    }

    /**
     * Converts the given data using the provided bind map.
     *
     * @param array $data The data to be converted.
     * @param array $bindMap The bind map used for conversion.
     * @return array The converted data.
     */
    public function convert(array $data, array $bindMap): array
    {
        $newData = [];
        foreach ($data as $keyID => $valueData) {
            if (! empty($valueData) && is_array($valueData)) {
                $newData[$keyID] = $this->bindData($valueData, $bindMap);
            }
        }
        return $newData;
    }

    /**
     * Converts a GeoJSON array using a bind map.
     *
     * @param array $data The GeoJSON array to convert.
     * @param array $bindMap The bind map to use for conversion.
     * @return array The converted GeoJSON array.
     */
    public function convert_geojson(array $data, array $bindMap): array
    {
        $newData = [];
        foreach ($data as $keyID => $valueData) {
            if (! empty($valueData) && is_array($valueData)) {
                $newData[] = $this->bindData($valueData, $bindMap);
            }
        }
        return $newData;
    }

    /**
     * Converts the answer data using the provided bind map.
     *
     * @param array $data The answer data to be converted.
     * @param array $bindMap The bind map used for conversion.
     * @return array The converted answer data.
     */
    public function convert_answer(array $data, array $bindMap): array
    {
        $newAnswer = [];
        $form = ! empty($data["form"]) && is_array($data["form"]) ? $data["form"] : [];
        $subforms = ! empty($form["subForms"]) && is_array($form["subForms"]) ? $form["subForms"] : [];
        $forms = ! empty($data["forms"]) && is_array($data["forms"]) ? $data["forms"] : [];

        if (! empty($data["answers"]) && is_array($data["answers"])) {
            foreach ($data["answers"] as $ansId => $answers) {
                if (is_array($answers)) {
                    $newData = [
                        "@type" => "Answer",
                        "id" => $ansId,
                        "formId" => isset($form["_id"]) ? (string) $form["_id"] : "",
                        "formName" => isset($form["name"]) ? (string) $form["name"] : "",
                    ];

                    if (! empty($answers["links"]) && is_array($answers["links"])) {
                        $orgLinks = $answers["links"][OrganizationInterface::COLLECTION] ?? [];
                        if (is_array($orgLinks)) {
                            $linksData = $this->db->findByIds(OrganizationInterface::COLLECTION, array_keys($orgLinks));
                            $newData["linked_organization"] = $this->convert($linksData, TranslateFtl::$dataBinding_allOrganization);
                        }
                    }

                    if (! empty($answers["answers"]) && is_array($answers["answers"])) {
                        foreach ($answers["answers"] as $step => $fields) {
                            if (is_array($fields)) {
                                $indexStep = strval(array_search($step, $subforms, true) + 1);
                                $stepName = $step; // Valeur par défaut
                                if (isset($form[$step]) && is_array($form[$step])) {
                                    $stepName = $form[$step]["name"] ?? $form[$step]["step"] ?? $step;
                                }
                                $newData[(string) $stepName] = [
                                    "stepNumber" => "step" . $indexStep,
                                    "stepId" => $step,
                                ];

                                foreach ($fields as $field => $value) {
                                    if (empty($forms[$step]["inputs"]) && ! is_array($forms[$step]["inputs"])) {
                                        continue;
                                    }
                                    $formK = str_ireplace(Form::$cplx, "", (string) $field);
                                    $label = ! empty($forms[$step]["inputs"][$formK]["label"]) ? (string) $forms[$step]["inputs"][$formK]["label"] : "no label";
                                    $type = ! empty($forms[$step]["inputs"][$formK]["type"]) ? (string) $forms[$step]["inputs"][$formK]["type"] : "no type";

                                    $val = (is_string($value) || strpos($type, "multiRadio") !== false) ? ($value["value"] ?? "") : $value;

                                    $ans = [
                                        "label" => $label,
                                        "type" => $type,
                                        "value" => $val,
                                        "fullValue" => $value,
                                        "fieldId" => $field,
                                    ];
                                    $newData[$stepName][$label] = $this->bindData($ans, $bindMap);
                                }
                            }
                        }
                    }
                    $newAnswer[$ansId] = $newData;
                }
            }
        }

        return $newAnswer;
    }

    /**
     * Binds data to the given bind map.
     *
     * @param array $data The data to bind.
     * @param array $bindMap The bind map.
     * @return array The bound data.
     */
    private function bindData(array $data, array $bindMap): array
    {
        $newData = [];
        foreach ($bindMap as $key => $bindPath) {
            if (is_array($bindPath) && isset($bindPath["valueOf"])) {
                if (is_array($bindPath["valueOf"])) {
                    if (isset($bindPath["object"])) {
                        $currentValue = (strpos($bindPath["object"], ".") > 0) ? $this->getValueByPath($bindPath["object"], $data) : (! empty($data[$bindPath["object"]]) ? $data[$bindPath["object"]] : "");

                        if (! empty($currentValue)) {
                            $newData[$key] = [];
                            foreach ($currentValue as $dataKey => $dataValue) {
                                $refData = $dataValue;

                                if (isset($bindPath["collection"])) {
                                    if (isset($bindPath["refId"])) {
                                        $dataKey = $bindPath["refId"];
                                    }
                                    $refData = $this->db->findOne($bindPath["collection"], [
                                        "_id" => $this->db->MongoId($dataKey),
                                    ]);
                                }
                                $valByPath = $this->bindData($refData, $bindPath["valueOf"]);
                                if (! empty($valByPath)) {
                                    array_push($newData[$key], $valByPath);
                                }
                            }
                        }
                    } elseif (isset($bindPath["parentKey"]) && isset($data[$bindPath["parentKey"]])) {
                        if ($bindPath["parentKey"] == "scope") {
                            foreach ($data[$bindPath["parentKey"]] as $k => $v) {
                                $firstScope = $v;
                                break;
                            }
                            $valByPath = $this->bindData($firstScope, $bindPath["valueOf"]);
                        } else {
                            $valByPath = $this->bindData($data[$bindPath["parentKey"]], $bindPath["valueOf"]);
                        }
                        if (! empty($valByPath)) {
                            $newData[$key] = $valByPath;
                        }
                    } else {
                        $valByPath = $this->checkAndGetArray($this->bindData($data, $bindPath["valueOf"]));

                        if (! empty($valByPath)) {
                            $newData[$key] = $valByPath;
                        }
                    }
                } elseif (strpos($bindPath["valueOf"], ".") > 0) {
                    $valByPath = $this->getValueByPath($bindPath["valueOf"], $data);
                    if (! empty($valByPath)) {
                        $newData[$key] = $valByPath;
                    }
                } elseif (isset($data[$bindPath["valueOf"]])) {
                    $valByPath = $data[$bindPath["valueOf"]];
                    if (! empty($valByPath)) {
                        $newData[$key] = $valByPath;
                    }
                }
            } elseif (is_array($bindPath)) {
                $valByPath = $this->bindData($data, $bindPath);

                if (! empty($valByPath)) {
                    $newData[$key] = $valByPath;
                }
            } else {
                $newData[$key] = $bindPath;
            }

            if (isset($newData[$key]) && (isset($bindPath["type"]) || isset($bindPath["prefix"]) || isset($bindPath["suffix"]))) {
                $newData[$key] = $this->formatValueByType($newData[$key], $bindPath);
            }
        }

        $t = array_keys($newData);
        $sarray = true;
        foreach ($t as $key => $v) {
            if (! is_numeric($v)) {
                $sarray = false;
            }
        }
        if ($sarray == true) {
            $newData = array_values($newData);
        }

        return $newData;
    }

    /**
     * Retrieves the value from an array based on a given path.
     *
     * @param string $path The path to the desired value.
     * @param array $currentValue The array to search in.
     * @return mixed|null The value found at the specified path, or null if not found.
     */
    private function getValueByPath(string $path, array $currentValue)
    {
        $path = explode(".", $path);

        foreach ($path as $pathKey) {
            if (! empty($currentValue[$pathKey])) {
                // TODO : MongoId pas bon
                if (is_object($currentValue[$pathKey]) && get_class($currentValue[$pathKey]) == "MongoId") {
                    $currentValue = (string) $currentValue[$pathKey];
                    break;
                } else {
                    $currentValue = $currentValue[$pathKey];
                }
            } else {
                $currentValue = "";
            }
        }
        return $currentValue;
    }

    /**
     * Formats a value based on its type.
     *
     * @param mixed $val The value to be formatted.
     * @param array $bindPath The array representing the path to the value.
     * @return mixed The formatted value.
     */
    private function formatValueByType($val, array $bindPath)
    {
        $prefix = $bindPath["prefix"] ?? "";
        $suffix = $bindPath["suffix"] ?? "";
        $outsite = $bindPath["outsite"] ?? null;

        if (isset($bindPath["type"]) && $bindPath["type"] == "url") {
            $val = $prefix . $val . $suffix;
            if (empty($outsite)) {
                $server = ((isset($_SERVER['HTTPS']) and (! empty($_SERVER['HTTPS'])) and strtolower($_SERVER['HTTPS']) != 'off') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];

                $custom = $this->session->get("custom");
                if (! empty($custom) && ! empty($custom["url"])) {
                    $val = $server . Yii::get('urlManager')->createUrl($custom["url"] . $val);
                } else {
                    $val = $server . Yii::get('urlManager')->createUrl($val);
                }
            }
            if (isset($bindPath["toArray"]) && $bindPath["toArray"] == true) {
                $val = [$val];
            }
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "urlOsm") {
            $val = $prefix . $val["latitude"] . "/" . $val["longitude"] . $suffix;
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "array") {
            $val = [$val];
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "date") {
            $val = date('D, d M Y H:i:s O', $val->sec);
        } elseif (isset($bindPath["type"]) && ($bindPath["type"] == "title")) {
            $val = $this->getTranslateRss()->specFormatByType($val, $bindPath);
        } elseif (isset($bindPath["type"]) && ($bindPath["type"] == "description")) {
            $val = $this->getTranslateRss()->specFormatByType($val, $bindPath);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "coor") {
            $val = (new TranslateKml())->getKmlCoor($val, $bindPath);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "description_kml") {
            $val = (new TranslateKml())->specFormatByType($val, $bindPath);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "Point") {
            $val = (new TranslateGeojson())->getGeojsonCoor($val, $bindPath);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "properties") {
            $val = (new TranslateGeojson())->getGeoJsonProperties($val, $bindPath);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "image_rss") {
            $val = $this->getTranslateRss()->getRssImage($val, $bindPath);
        } elseif (isset($bindPath["prefix"]) || isset($bindPath["suffix"])) {
            $val = $prefix . $val . $suffix;
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "openingHours") {
            $val = $this->getTranslateFtl()->openingHours($val);
        } elseif (isset($bindPath["type"]) && $bindPath["type"] == "typePlace") {
            $val = $this->getTranslateFtl()->typePlace($val);
        }

        return $val;
    }

    /**
     * Check and get an array.
     *
     * @param array|null $array The array to check.
     * @return array|null The checked array.
     */
    private function checkAndGetArray(?array $array): ?array
    {
        $val = $array;
        if ((is_countable($array) ? count($array) : 0) == 0 || ((is_countable($array) ? count($array) : 0) == 1 && ! empty($array["@type"]))) {
            $val = null;
        }

        return $val;
    }

    // TODO : revoir le fonctionnement de cette fonction avec les dates et les types
    public function pastTime($date, $type, $timezone = null)
    {
        if ($type == "timestamp") {
            $date2 = $date; // depuis cette date
            if (@$date->sec) {
                $date2 = $date->sec;
            }
        } elseif ($type == "date") {
            $date2 = strtotime($date); // depuis cette date
        } elseif ($type == "datefr") {
            $date2 = DateTime::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');

            $date2 = strtotime($date2); // depuis cette date
        } else {
            return "Non reconnu";
        }

        $Ecart = time() - $date2;
        $lblEcart = "";
        $tradAgo = true;
        if (time() < $date2) {
            $tradAgo = false;
            $lblEcart = $this->language->t("common", "in") . " ";
            $Ecart = $date2 - time();
        }

        if (isset($timezone) && $timezone != "") {
            if (date_default_timezone_get() != $timezone) {
                date_default_timezone_set($timezone);
            }
        } else {
            date_default_timezone_set("UTC");
        }

        $Annees = date('Y', $Ecart) - 1970;
        $Mois = date('m', $Ecart) - 1;
        $Jours = date('d', $Ecart) - 1;
        $Heures = date('H', $Ecart);
        $Minutes = date('i', $Ecart);
        $Secondes = date('s', $Ecart);

        if ($Annees > 0) {
            $res = $lblEcart . $Annees . " " . $this->language->t("translate", "year" . ($Annees > 1 ? "s" : "")) . " " . $this->language->t("common", "and") . " " . $Jours . " " . $this->language->t("translate", "day" . ($Jours > 1 ? "s" : "")); // on indique les jours avec les année pour être un peu plus précis
        } elseif ($Mois > 0) {
            $res = $lblEcart . $Mois . " " . $this->language->t("translate", "month" . ($Mois > 1 ? "s" : ""));
            if ($Jours > 0) {
                $res .= " " . $this->language->t("common", "and") . " " . $Jours . " " . $this->language->t("translate", "day" . ($Jours > 1 ? "s" : ""));
            } // on indique les jours aussi
        } elseif ($Jours > 0) {
            $res = $lblEcart . $Jours . " " . $this->language->t("translate", "day" . ($Jours > 1 ? "s" : ""));
        } elseif ($Heures > 0) {
            if ($Heures < 10) {
                $Heures = substr($Heures, 1, 1);
            }
            $res = $lblEcart . $Heures . " " . $this->language->t("translate", "hour" . ($Heures > 1 ? "s" : ""));
            if ($Minutes > 0) {
                if ($Minutes < 10) {
                    $Minutes = substr($Minutes, 1, 1);
                }
                $res .= " " . $this->language->t("common", "and") . " " . $Minutes . " " . $this->language->t("translate", "minute" . ($Minutes > 1 ? "s" : ""));
            }
        } elseif ($Minutes > 0) {
            if ($Minutes < 10) {
                $Minutes = substr($Minutes, 1, 1);
            }
            $res = $lblEcart . $Minutes . " " . $this->language->t("translate", "minute" . ($Minutes > 1 ? "s" : ""));
        } elseif ($Secondes > 0) {
            if ($Secondes < 10) {
                $Secondes = substr($Secondes, 1, 1);
            }
            $res = $lblEcart . $Secondes . " " . $this->language->t("translate", "second" . ($Secondes > 1 ? "s" : ""));
        } else {
            if ($tradAgo == true) {
                $res = $this->language->t("translate", "Right now");
            } else {
                $res = $this->language->t("translate", "In a few second");
            }
        }
        if ($tradAgo) {
            $res = $this->language->t("translate", "{time} ago", [
                "{time}" => $res,
            ]);
        }
        return $res;
    }

    /**
     * Calculates the difference in days between a given date and the current date.
     *
     * @param string $date The date to calculate the difference from.
     * @param string $type The type of difference to calculate (e.g., "days", "months", "years").
     * @param string|null $timezone The timezone to use for the calculation (optional).
     * @return int|float|string The difference in days.
     */
    public function dayDifference($date, string $type, ?string $timezone = null)
    {
        if ($type == "timestamp") {
            $date2 = $date; // depuis cette date
        } elseif ($type == "date" && is_string($date)) {
            $date2 = strtotime($date); // depuis cette date
        } elseif ($type == "datefr" && is_string($date)) { //echo $date;exit;
            $date2 = DateTime::createFromFormat('d/m/Y H:i', $date)->format('Y-m-d H:i');

            $date2 = strtotime($date2); // depuis cette date
        } else {
            return "Non reconnu";
        }

        $Ecart = time() - $date2;
        $lblEcart = "";
        $tradAgo = true;
        if (time() < $date2) {
            $tradAgo = false;
            $lblEcart = $this->language->t("common", "in") . " ";
            $Ecart = $date2 - time();
        }

        if (isset($timezone) && $timezone != "") {
            if (date_default_timezone_get() != $timezone) {
                date_default_timezone_set($timezone);
            }
        } else {
            date_default_timezone_set("UTC");
        }

        $Annees = date('Y', $Ecart) - 1970;
        $Mois = date('m', $Ecart) - 1;
        $Jours = date('d', $Ecart) - 1;

        return $Jours + ($Mois * 30) + ($Annees * 356); //approximatif 30jours/mois
    }

    /**
     * Converts a string to a clickable link.
     *
     * @param string $str The input string to convert.
     * @return string The converted string with clickable links.
     */
    public function strToClickable(string $str): string
    {
        $url = '@(http(s)?)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $string = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $str);
        return $string;
    }
}
