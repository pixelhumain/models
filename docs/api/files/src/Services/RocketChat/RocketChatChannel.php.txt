<?php

namespace PixelHumain\Models\Services\RocketChat;

use GuzzleHttp\Exception\GuzzleException;
use PixelHumain\Models\Services\RocketChat\Interfaces\RoomInterface;

class Channel implements RoomInterface
{
    public ?string $id;

    public string $name;

    public array $members = [];

    protected User $user;

    /**
     * Constructor for the RocketChatChannel class.
     * @param array $config
     * @param string|object $name The name of the channel.
     * @param array $members An array of members in the channel.
     * @param mixed $admin The admin of the channel.
     */
    public function __construct(User $user, $name, array $members = [])
    {
        $this->user = $user;
        $this->name = is_string($name) ? $name : (string) $name->name;
        $this->id = is_object($name) && $name->_id ? (string) $name->_id : null;
        foreach ($members as $member) {
            $this->members[] = is_string($member) ? new User($this->user->config, $member) : $member;
        }
    }

    /**
     * Creates a new RocketChat channel.
     *
     * @return mixed The response from the API.
     */
    public function create()
    {
        /**
         * @var User $member
         */
        $members_id = array_map(fn ($member) => (string) $member->username, $this->members);
        try {
            $body = $this->user->sendRequest('POST', 'channels.create', [
                'json' => [
                    'name' => $this->name,
                    'members' => $members_id,
                ],
            ]);
            if (is_object($body) && $body->success && is_object($body->channel) && isset($body->channel->_id)) {
                $this->id = $body->channel->_id;
            }
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Retrieves information about the RocketChat channel.
     *
     * @return mixed The channel information.
     */
    public function info()
    {
        try {
            $body = $this->user->sendRequest('GET', 'channels.info', [
                'query' => [
                    'roomName' => $this->name,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Posts a message to the RocketChat channel.
     *
     * @param string|array $text The text of the message.
     * @return bool Whether the message was posted or not.
     */
    public function postMessage($text): bool
    {
        try {
            $message = is_string($text) ? [
                'text' => $text,
            ] : $text;

            if (! isset($message['attachments'])) {
                $message['attachments'] = [];
            }

            $body = $this->user->sendRequest('POST', 'chat.postMessage', [
                'json' => array_merge([
                    'channel' => '#' . $this->name,
                ], $message),
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Closes the RocketChat channel.
     *
     * @return bool Whether the channel was closed or not.
     */
    public function close(): bool
    {
        try {
            $body = $this->user->sendRequest('POST', 'channels.close', [
                'json' => [
                    'roomId' => $this->id,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Deletes the RocketChat channel.
     *
     * @return bool Whether the channel was deleted or not.
     */
    public function delete(): bool
    {
        try {
            $body = $this->user->sendRequest('POST', 'channels.delete', [
                'json' => [
                    'roomId' => $this->id,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Kicks a user from the RocketChat channel.
     *
     * @param string|object $user The username of the user to be kicked.
     * @return bool Whether the user was kicked or not.
     */
    public function kick($user): bool
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'channels.kick', [
                'json' => [
                    'roomId' => $this->id,
                    'userId' => $userId,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Invite a user to the RocketChat channel.
     *
     * @param string|object $user The username of the user to invite.
     * @return mixed|null Whether the user was invited or not.
     */
    public function invite($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'channels.invite', [
                'json' => [
                    'roomName' => $this->name,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Adds an owner to the RocketChat channel.
     *
     * @param string|object $user The username of the user to be added as an owner.
     * @return mixed|null Whether the user was added as an owner or not.
     */
    public function addOwner($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'channels.addOwner', [
                'json' => [
                    'roomName' => $this->name,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Removes the owner role from a user in the RocketChat channel.
     *
     * @param string|object  $user The username of the user to remove the owner role from.
     * @return mixed|null Whether the user was removed as an owner or not.
     */
    public function removeOwner($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'channels.removeOwner', [
                'json' => [
                    'roomId' => $this->id,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Set the type of the RocketChat channel.
     *
     * @param string $name The name of the channel.
     * @param string $type The type of the channel.
     * @return mixed|null Whether the type was set or not.
     */
    public function setType(string $name, string $type)
    {
        try {
            $body = $this->user->sendRequest('POST', 'channels.setType', [
                'json' => [
                    'roomName' => $name,
                    'type' => $type,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Renames the RocketChat channel.
     *
     * @param string $newName The new name for the channel.
     * @return mixed|null Whether the channel was renamed or not.
     */
    public function rename(string $newName)
    {
        try {
            $body = $this->user->sendRequest('POST', 'channels.rename', [
                'json' => [
                    'roomId' => $this->id,
                    'name' => $newName,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Set the custom fields for the RocketChat channel.
     *
     * @param array $customFields The custom fields to set.
     * @return mixed|null Whether the custom fields were set or not.
     */
    public function setCustomFields(array $customFields)
    {
        try {
            $body = $this->user->sendRequest('POST', 'channels.setCustomFields', [
                'json' => [
                    'roomId' => $this->id,
                    'customFields' => $customFields,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }
}
