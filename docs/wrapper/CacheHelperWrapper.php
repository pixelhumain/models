<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\CacheHelperInterface;
use yii\caching\TagDependency;
use PixelHumain\Models\Costum;

/**
 * Class CacheHelperWrapper
 *
 * This class is a wrapper for the CacheHelperInterface.
 * It provides additional functionality for caching operations.
 */
class CacheHelperWrapper implements CacheHelperInterface
{
    protected $cacheComponent;
    protected $requestComponent = null;

    /**
     * Constructor for the CacheHelperWrapper class.
     *
     * @param mixed $cacheComponent The cache component to be used.
     */
    public function __construct($cacheComponent, $requestComponent = null)
    {
        $this->cacheComponent = $cacheComponent;
        $this->requestComponent = $requestComponent;
    }
    
    /**
     * Check if the cache component is initialized.
     *
     * @return bool Returns true if the cache component is initialized, false otherwise.
     */
    protected function isInitialized()
    {
        if ($this->cacheComponent) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieves a value from the cache.
     *
     * @param string $id The cache ID.
     * @return mixed The cached value, or false if the cache is not initialized.
     */
    public function get($id)
    {
        if ($this->isInitialized()) {
            $value = $this->cacheComponent->get($id);
            return $value;
        } else {
            return false;
        }
    }

    /**
     * Sets a value in the cache.
     *
     * @param string $id The cache key.
     * @param mixed $value The value to be stored in the cache.
     * @param int $expire The number of seconds until the cache entry expires. Default is 0, which means the cache entry never expires.
     * @return bool Returns true if the value is set successfully or if the cache component is not initialized, otherwise returns false.
     */
    public function set($id, $value, $expire = 0): bool
    {
        if ($this->isInitialized()) {
            return $expire ? $this->cacheComponent->set($id, $value, $expire) : $this->cacheComponent->set($id, $value);
        } else {
            return false;
        }
    }

    /**
     * Sets the value of a cache item with specified tags.
     *
     * @param string $id The unique identifier for the cache item.
     * @param mixed $value The value to be stored in the cache item.
     * @param array $tags The tags associated with the cache item.
     * @return bool Returns true if the value is set successfully or if the cache component is not initialized, otherwise returns false.
     */
    public function setTags($id, $value, $tags): bool
    {
        if ($this->isInitialized()) {
            return $this->cacheComponent->set($id, $value, 0, new TagDependency($tags));
        } else {
            return false;
        }
    }

    /**
     * Invalidates cache tags.
     *
     * @param array|string $tags The cache tags to invalidate.
     * @return void
     */
    public function invalidateTags($tags)
    {
        if ($this->isInitialized()) {
            TagDependency::invalidate($this->cacheComponent, $tags);
        }
    }

    /**
     * Deletes a cache entry by its ID.
     *
     * @param string $id The ID of the cache entry to delete.
     * @return bool Returns true if the cache entry is successfully deleted or if the cache component is not initialized, otherwise returns false.
     */
    public function delete($id): bool
    {
        if ($this->isInitialized()) {
            return $this->cacheComponent->delete($id);
        } else {
            return false;
        }
    }

    /**
     * Flushes the cache.
     *
     * @return bool Returns true if the cache is flushed successfully, or false otherwise.
     */
    public function flush(): bool
    {
        if ($this->isInitialized()) {
            return $this->cacheComponent->flush();
        } else {
            return true;
        }
    }
    

    /**
     * Retrieves custom data based on the provided parameters.
     *
     * @param string|null $id The ID of the custom data.
     * @param string|null $type The type of the custom data.
     * @param string|null $slug The slug of the custom data.
     * @return mixed The custom data retrieved from the cache.
     */
    public function getCostum(?string $id = null, ?string $type = null, ?string $slug = null)
    {

      
        if (@$_GET["slug"] && !@$_POST["costumSlug"]) {
            $slug = $_GET["slug"];
        } elseif (@$_POST["costumSlug"]) {
            $slug = $_POST["costumSlug"];
        } elseif (!empty($slug)) {
            // ok slug is set
        } elseif ((isset($type) || isset($_POST["costumType"])) && (isset($id) || isset($_POST["costumId"]))) {
            $type = ($type || @$_POST["costumType"]);
            $id = ($id || $_POST["costumId"]);
        } else {
            $id = (!empty($id)) ? $id : @$_GET["id"];
        }

        if (isset($slug)) {
            $id = $slug;
        }

        if (!empty($id)) {
        } elseif (@$_GET["host"]) {
            $id = Costum::getSlugByHost($_GET["host"]);
        } elseif (@$_POST["costumSlug"]) {
            $id = $_POST["costumSlug"];
        }

        $cacheCostum = $this->get($id);

        if (@$_POST["costumEditMode"]) {
            $cacheCostum["editMode"] = $_POST["costumEditMode"];
        }

        
        return $cacheCostum;
    }
}
