<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\I18NInterface;
use yii\i18n\I18N;

/**
 * This class is a wrapper for the I18N component in the PixelHumain framework.
 * It implements the I18NInterface and provides additional functionality for handling internationalization.
 */
class I18nWrapper extends I18N implements I18NInterface
{
    public function translate($category, $message, $params = [], $language = null)
    {
        // Votre logique personnalisée pour traiter les paramètres
        if (is_array($params) && !empty($params)) {
            $arrayCopie = [];
            foreach ($params as $key => $value) {
                if (is_string($key) && substr($key, 0, 1) == '{') {
                    $key = trim($key, '{}');
                }
                $arrayCopie[$key] = $value;
            }
            $params = $arrayCopie;
        }

        // Appeler la méthode translate du parent
        return parent::translate($category, $message, $params, $language);
    }
}

// class I18nWrapper implements I18NInterface {
//     public function translate($category, $message, $params, $language) {
//         return $message;
//     }
// }
