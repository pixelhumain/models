<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\SessionInterface;
use yii\web\Session;

/**
 * This class represents a session wrapper that extends the Yii web session class and implements the SessionInterface.
 */
class SessionWrapper extends Session implements SessionInterface
{
}

// class SessionWrapper implements SessionInterface {
//     protected $sessionArray = [];
//     public function __get($key) {
//         return $this->sessionArray[$key];
//     }
//     public function __set($key, $value) {
//         return $this->sessionArray[$key] = $value;
//     }
//     public function __isset($key) {
//         return ($this->sessionArray[$key] ?? null) ? true : false;
//     }
//     public function __unset($key) {
//         unset($this->sessionArray[$key]);
//     }
//     public function remove($key) {
//         unset($this->sessionArray[$key]);
//     }
//     public function removeAll() {
//         $this->sessionArray = [];
//     }
// }
