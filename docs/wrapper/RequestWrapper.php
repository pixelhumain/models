<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\RequestInterface;
use yii\web\Request;

/**
 * This class represents a response wrapper that extends the Yii web response class and implements the RequestInterface.
 */
class RequestWrapper extends Request implements RequestInterface
{
}
