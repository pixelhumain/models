<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\ArrayHelperInterface;

/**
 * Class ArrayHelperWrapper
 *
 * This class is an implementation of the ArrayHelperInterface.
 * It provides helper methods for working with arrays.
 */
class ArrayHelperWrapper implements ArrayHelperInterface
{
    /**
     * Get the value from an associative array using dot notation path.
     *
     * @param array $assocArray The associative array to search in.
     * @param string $path The dot notation path to the desired value.
     * @return mixed|null The value found at the specified path, or null if not found.
     */
    public function getValueByDotPath($assocArray, $path)
    {
        $split = explode(".", $path);
        foreach ($split as $key) {
            if (isset($assocArray[$key])) {
                $assocArray = $assocArray[$key];
            } else {
                $assocArray = "";
                break;
            }
        }
        return $assocArray;
    }
}
