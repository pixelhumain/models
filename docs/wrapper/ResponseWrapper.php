<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Interfaces\ResponseInterface;
use yii\web\Response;

/**
 * This class represents a response wrapper that extends the Yii web response class and implements the ResponseInterface.
 */
class ResponseWrapper extends Response implements ResponseInterface
{
}
