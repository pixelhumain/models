<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\PixelHumain\components\PhdbDi;
use PixelHumain\Models\Interfaces\PhdbDiInterface;

/**
 * Class PhdbWrapper
 *
 * This class is a wrapper for the PhdbDi class.
 * It implements the PhdbDiInterface.
 */
class PhdbWrapper extends PhdbDi implements PhdbDiInterface
{
}

// class PhdbWrapper implements PhdbDiInterface {
//    public function find($collection, $where = [], $fields = null) {
//        return [];
//    }
//    ...
// }
