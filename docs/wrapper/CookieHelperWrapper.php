<?php

namespace PixelHumain\Models\Wrapper;

use PixelHumain\Models\Factory\Interfaces\CookieFactoryInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\RequestInterface;
use PixelHumain\Models\Interfaces\ResponseInterface;

/**
 * Class CacheHelperWrapper
 *
 * This class is a wrapper for the CacheHelperInterface.
 * It provides additional functionality for caching operations.
 */
class CookieHelperWrapper implements CookieHelperInterface
{
    protected $responseComponent;
    protected $requestComponent;
    protected $cookieFactory;

    /**
     * Constructor for the CacheHelperWrapper class.
     *
     * @param mixed $cacheComponent The cache component to be used.
     */
    public function __construct(RequestInterface $requestComponent, ResponseInterface $responseComponent, CookieFactoryInterface $cookieFactory)
    {
        $this->requestComponent = $requestComponent;
        $this->responseComponent = $responseComponent;
        $this->cookieFactory = $cookieFactory;
    }

    public function hasCookie($name)
    {
        $cookies = $this->requestComponent->getCookies();
        return $cookies->has($name);
    }
  
    public function getCookie($name)
    {
        return $this->requestComponent->getCookies()->get($name)->value;
    }
  
    public function setCookie($name, $value)
    {
        $cookies = $this->responseComponent->getCookies();

        $cookie = $this->cookieFactory->createCookie([
            'name' => $name,
            'value' => $value,
            'expire' => time() + 60 * 60 * 24 * 180,
        ]);

        $cookies->add($cookie);
    }
  
    public function removeCookie($name)
    {
        $cookies = $this->responseComponent->getCookies();
        $cookies->remove($name);
    }
}
