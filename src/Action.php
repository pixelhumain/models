<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CommentInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProductInterface;
use PixelHumain\Models\Interfaces\ServiceInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;

use PixelHumain\Models\Traits\BaseModel\DbTrait;

use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\GamificationTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\GamificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProposalTraitInterface;
use PixelHumain\Models\Traits\NewsTrait;
use PixelHumain\Models\Traits\NotificationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProposalTrait;

// TODO : CO2::getThemeParams()


/**
 * Represents an action model.
 * This class extends the BaseModel class.
 */
class Action extends BaseModel implements ActionInterface, PersonTraitInterface, ProposalTraitInterface, NotificationTraitInterface, GamificationTraitInterface, NewsTraitInterface, ElementTraitInterface, DataValidatorTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use ProposalTrait;
    use NotificationTrait;
    use GamificationTrait;
    use NewsTrait;
    use ElementTrait;
    use DataValidatorTrait;

    // TODO : moduleId comment je le gére ?
    protected ?string $moduleId = null;

    // TODO : verifier les $dataBinding les types et visibilité static ou pas
    // TODO : trouver peut être une autre organisation pour databiding avce un objet ?
    public static array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "description" => [
            "name" => "description",
            "rules" => ["required"],
        ],
        "tags" => [
            "name" => "tags",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "startDate" => [
            "name" => "startDate",
        ],
        "endDate" => [
            "name" => "endDate",
        ],
        "actors" => [
            "name" => "actors",
        ],
        // Open / Closed
        "status" => [
            "name" => "status",
            "rules" => ["required"],
        ],
        "idUserAuthor" => [
            "name" => "idUserAuthor",
            "rules" => ["required"],
        ],
        "idParentRoom" => [
            "name" => "idParentRoom",
            "rules" => ["required"],
        ],
        "answerId" => [
            "name" => "answerId",
        ],
        "parentId" => [
            "name" => "parentId",
            "rules" => ["required"],
        ],
        "parentType" => [
            "name" => "parentType",
            "rules" => ["required"],
        ],
        "parentIdSurvey" => [
            "name" => "parentIdSurvey",
            "rules" => ["required"],
        ],
        "parentTypeSurvey" => [
            "name" => "parentTypeSurvey",
            "rules" => ["required"],
        ],
        "role" => [
            "name" => "role",
        ],
        "idParentResolution" => [
            "name" => "idParentResolution",
        ],
        "email" => [
            "name" => "status",
        ],
        "links" => [
            "name" => "links",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "measures" => [
            "name" => "measures",
        ],
        "source" => [
            "name" => "source",
        ],
        "max" => [
            "name" => "max",
        ],
        "min" => [
            "name" => "min",
        ],
        "credits" => [
            "name" => "credits",
        ],
        "group" => [
            "name" => "group",
        ],
        "finishedBy" => [
            "name" => "finishedBy",
        ],
        "validated" => [
            "name" => "validated",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Returns the data binding for the Action model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array
    {
        return self::$dataBinding;
    }

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @param array|null $fields The fields to retrieve. If null, all fields will be retrieved.
     * @return array|null The action object if found, null otherwise.
     */
    public function getById(string $id, ?array $fields = null): ?array
    {
        return $this->db->findOne(ActionInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ], $fields);
    }

    /**
     * Retrieves the simple specification of an action by its ID.
     *
     * @param string $id The ID of the action.
     * @param array|null $where Additional condition to filter the results (optional).
     * @param array|null $fields Specific fields to retrieve (optional).
     * @return array|null The simple specification of the action.
     */
    public function getSimpleSpecById(string $id, ?array $where = null, ?array $fields = null): ?array
    {
        if (empty($fields)) {
            $fields = ["_id", "name"];
        }
        $where["_id"] = $this->db->MongoId($id);
        $action = $this->db->findOne(ActionInterface::COLLECTION, $where, $fields);
        return $action;
    }

    /**
     * Adds an action to the collection.
     *
     * @param string $userId The user ID.
     * @param string $id The ID of the action.
     * @param string $collection The name of the collection.
     * @param string $action The action to be added.
     * @param bool $unset (optional) Whether to unset the action.
     * @param bool $multiple (optional) Whether to allow multiple actions.
     * @param array|null $details (optional) Additional details for the action.
     * @param string|null $path (optional) The path for the action.
     * @return array The updated collection.
     */
    public function addAction(string $userId, string $id, string $collection, string $action, bool $unset = false, bool $multiple = false, ?array $details = null, ?string $path = null): array
    {
        $user = $this->getModelPerson()->getById($userId);
        $element = $this->findElementById($id, $collection);

        if (! $this->isValidAction($action, $element, $user, $userId)) {
            return $this->actionNotValidResponse();
        }

        return $this->updateDataBasedOnAction($user, $element, $action, $collection, $userId, $unset, $multiple, $details, $path);
    }

    /**
     * Find an element by its ID in a collection.
     *
     * @param string $id The ID of the element to find.
     * @param string $collection The name of the collection to search in.
     *
     * @return array|null The found element as an array, or null if not found.
     */
    private function findElementById(string $id, string $collection): ?array
    {
        return $this->db->findOne($collection, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Check if the action is valid.
     *
     * @param string $action The action to check.
     * @param array|null $element The element related to the action.
     * @param array|null $user The user performing the action.
     * @param string $userId The ID of the user performing the action.
     * @return bool Returns true if the action is valid, false otherwise.
     */
    private function isValidAction(string $action, ?array $element, ?array $user, string $userId): bool
    {
        $possibleActions = [ActionInterface::ACTION_ROOMS, ActionInterface::ACTION_ROOMS_TYPE_SURVEY, ActionInterface::ACTION_MODERATE, ActionInterface::ACTION_VOTE_UP, ActionInterface::ACTION_VOTE, ActionInterface::ACTION_VOTE_ABSTAIN, ActionInterface::ACTION_VOTE_UNCLEAR, ActionInterface::ACTION_VOTE_MOREINFO, ActionInterface::ACTION_VOTE_DOWN, ActionInterface::ACTION_FUND, ActionInterface::ACTION_PURCHASE, ActionInterface::ACTION_COMMENT, ActionInterface::ACTION_REPORT_ABUSE, ActionInterface::ACTION_FOLLOW];

        if (! in_array($action, $possibleActions) || empty($user) || empty($element)) {
            return false;
        }

        return true;
    }

    /**
     * Returns a response indicating that the action is not valid.
     *
     * @return array The response indicating that the action is not valid.
     */
    private function actionNotValidResponse(): array
    {
        return [
            "result" => false,
            "msg" => "Action not valid.",
        ];
    }

    /**
     * Updates the data based on the action.
     *
     * @param array       $user       The user data.
     * @param array       $element    The element data.
     * @param string      $action     The action being performed.
     * @param string      $collection The collection being updated.
     * @param string      $userId     The user ID.
     * @param bool        $unset      Whether to unset the data.
     * @param bool        $multiple   Whether to update multiple elements.
     * @param array|null  $details    Additional details for the update.
     * @param string|null $path       The path to the data being updated.
     *
     * @return array The updated data.
     */
    private function updateDataBasedOnAction(array $user, array $element, string $action, string $collection, string $userId, bool $unset, bool $multiple, ?array $details, ?string $path): array
    {
        //check user hasn't allready done the action or if it's allowed
        if (! $this->canPerformAction($element, $user, $userId, $action, $unset, $multiple)) {
            //Add or remove
            $res = [
                "result" => true,
                "userAllreadyDidAction" => true,
                "msg" => $this->language->t("common", "You have already made this action"),
            ];
        }

        // Configuration de base pour la mise à jour
        $inc = $unset ? -1 : 1;
        $dbMethod = $unset ? '$unset' : ($multiple ? '$addToSet' : '$set');
        $params = [
            $dbMethod => [],
            '$inc' => [],
            '$set' => [
                "updated" => time(),
                "modified" => $this->db->MongoDate(time()),
            ],
        ];
        $createNotification = true; // Initialisation de la variable de notification

        // Traitement pour le cas de suppression
        if ($unset) {
            $params['$unset'] = [
                $action . "." . $this->session->getUserId() => 1,
            ];
            $params['$inc'] = [
                $action . "Count" => $inc,
            ];
        } else {
            // Gestion spécifique pour l'action "vote"
            if ($action == "vote" && ! empty($element[$action][$userId]) && isset($element[$action][$userId]["status"]) && isset($details["status"]) && $element[$action][$userId]["status"] == $details["status"]) {
                return $this->updateVoteAction($element["_id"], $element[$action][$userId]["status"], $element[$action . "Count"], $userId, $collection);
            }

            // Traitement pour le cas d'ajout
            $details = (! empty($details) && is_array($details)) ? array_merge($details, [
                'date' => $this->db->MongoDate(time()),
            ]) : [
                'date' => $this->db->MongoDate(time()),
            ];
            $actionKey = $action . "." . (@$path ? $path . "." : "") . (string) $user["_id"];
            $params[$dbMethod] = [
                $actionKey => $details,
            ];
            $params['$inc'] = [
                $action . "Count" => $inc,
            ];

            // Gestion spécifique pour l'action "vote"
            if ($action == "vote") {
                $params['$inc'] = [
                    $action . "Count." . $details["status"] => $inc,
                ];
                if (isset($element[$action][$userId])) {
                    $createNotification = false;
                    if ($element[$action][$userId]["status"] > 1) {
                        $params['$inc'][$action . "Count." . $element[$action][$userId]["status"]] = -1;
                    } else {
                        $params['$unset'] = [
                            $action . "Count." . $element[$action][$userId]["status"] => 1,
                        ];
                    }
                }
            }
        }

        // Mise à jour de la base de données
        $this->db->update(
            $collection,
            [
                "_id" => $this->db->MongoId((string) $element["_id"]),
            ],
            $params
        );

        // Création de la notification si nécessaire
        if ($action === "vote" && $createNotification) {
            $this->createNotificationVote($collection, $element);
        }

        $this->updateParent((string) $element["_id"], $collection);

        //We update the points of the user
        $this->processGamification($user, $action, $userId);

        //Moderate automatic
        if ($action == "reportAbuse") {
            $this->reportAbuse($collection, $element);
        }

        return $this->createSuccessResponse($action, $userId, $element, $collection, $inc);
    }

    /**
     * Crée une réponse de succès.
     *
     * @param string $action L'action effectuée.
     * @param string $userId L'identifiant de l'utilisateur.
     * @param array $element L'élément créé.
     * @param string $collection La collection dans laquelle l'élément a été créé.
     * @param int $inc Le nombre d'éléments créés.
     * @return array La réponse de succès.
     */
    private function createSuccessResponse(string $action, string $userId, array $element, string $collection, int $inc): array
    {
        $msg = "OK !"; //WDTF ?
        if ($action == ActionInterface::ACTION_REPORT_ABUSE) {
            $msg = $this->language->t("common", "Thank you ! We are dealing it as quickly as possible. If there is more than 5 report, the news will be hidden");
        }
        if ($action == ActionInterface::ACTION_VOTE) {
            $msg = $this->language->t("common", "Reaction succesfully saved");
        }
        $res = [
            "result" => true,
            "userActionSaved" => true,
            "user" => $this->db->findOne(PersonInterface::COLLECTION, [
                "_id" => $this->db->MongoId($userId),
            ], ["actions"]),
            "element" => $this->db->findOne($collection, [
                "_id" => $this->db->MongoId((string) $element["_id"]),
            ], [$action]),
            "inc" => $inc,
            "msg" => $msg,
        ];
        return $res;
    }

    /**
     * Determines if a user can perform a specific action on an element.
     *
     * @param array $element The element on which the action is performed.
     * @param array $user The user performing the action.
     * @param string $userId The ID of the user performing the action.
     * @param string $action The action to be performed.
     * @param bool $unset Whether to unset the action after performing it.
     * @param bool $multiple Whether multiple actions can be performed.
     * @return bool Returns true if the user can perform the action, false otherwise.
     */
    private function canPerformAction(array $element, array $user, string $userId, string $action, bool $unset, bool $multiple): bool
    {
        if ($unset || ! isset($element[$action])
                    || (! $multiple && ! empty($element[$action]) && is_array($element[$action]) && ! in_array((string) $user["_id"], $element[$action])
                    || $multiple)) {
        } else {
            return false;
        }

        if ($unset && ($action == "voteUp" || $action == "voteDown") && (! isset($element[$action][$userId]))) {
            return false;
        }

        if (! $unset && ($action == "voteUp" || $action == "voteDown" || $action == "reportAbuse") && (isset($element[$action][$userId]))) {
            return false;
        }

        return true;
    }

    /**
     * Update the vote action for a specific element.
     *
     * @param string $elementId The ID of the element.
     * @param string $status The status of the vote action.
     * @param array $voteCount The vote count for the element.
     * @param string $userId The ID of the user.
     * @param string $collection The collection name.
     * @return array The updated vote action.
     */
    private function updateVoteAction(string $elementId, string $status, array $voteCount, string $userId, string $collection): array
    {
        $action = "vote";

        // Décrémenter le total du compte de vote pour le statut actuel
        if (isset($voteCount[$status])) {
            $voteCount[$status]--;
            if ($voteCount[$status] < 1) {
                unset($voteCount[$status]);
            }
        }

        // Calculer le total des votes
        $totalVoteCount = array_sum($voteCount);

        // Définir l'action de mise à jour de la collection
        $updateAction = [];
        if ($totalVoteCount > 0) {
            $updateAction = [
                '$set' => [
                    $action . "Count" => $voteCount,
                ],
                '$unset' => [
                    $action . "." . $this->session->getUserId() => "",
                ],
            ];
        } else {
            $updateAction = [
                '$unset' => [
                    $action . "Count" => "",
                    $action => "",
                ],
            ];
        }

        // Mettre à jour la base de données
        $this->db->update($collection, [
            "_id" => $this->db->MongoId($elementId),
        ], $updateAction);

        // Préparer la réponse
        $res = [
            "result" => true,
            "userActionSaved" => true,
            "user" => $this->db->findOne(PersonInterface::COLLECTION, [
                "_id" => $this->db->MongoId($userId),
            ], ["actions"]),
            "element" => $this->db->findOne($collection, [
                "_id" => $this->db->MongoId($elementId),
            ], [$action]),
            "inc" => -1,
            "msg" => "",
            "removedVoteStatus" => $status,
        ];
        return $res;
    }

    /**
     * Process the gamification for a specific user action.
     *
     * @param array $user The user information.
     * @param string $action The action performed by the user.
     * @param string $userId The ID of the user.
     * @return void
     */
    private function processGamification(array $user, string $action, string $userId): void
    {
        //We update the points of the user
        if (isset($user['gamification']) && isset($user['gamification']['actions']) && isset($user['gamification']['actions'][$action])) {
            $this->getModelGamification()->incrementUser($userId, $action);
        } else {
            //var_dump("fgh");exit;
            $this->getModelGamification()->updateUser($userId);
        }
    }

    /**
     * Creates a notification for a vote.
     *
     * @param string $collection The collection name.
     * @param array $element The element data.
     * @return void
     */
    private function createNotificationVote(string $collection, array $element): void
    {
        if ($collection != SurveyInterface::COLLECTION && FormInterface::ANSWER_COLLECTION != $collection) {
            $verb = ActStrInterface::VERB_REACT;

            $objectNotif = null;

            if ($collection == CommentInterface::COLLECTION) {
                $target = [
                    "type" => $element["contextType"],
                    "id" => $element["contextId"],
                ];
                $objectNotif = [
                    "type" => $collection,
                    "id" => (string) $element["_id"],
                ];
            } else {
                $target = [
                    "type" => $collection,
                    "id" => (string) $element["_id"],
                ];
                if (isset($element["targetIsAuthor"])) {
                    $target["targetIsAuthor"] = true;
                }
            }

            $this->getModelNotification()->constructNotification($verb, [
                "id" => $this->session->getUserId(),
                "name" => $this->session->getUserName(),
            ], $target, $objectNotif, $collection);
        }
    }

    /**
     * Report abuse for a specific element in a collection.
     *
     * @param string $collection The name of the collection.
     * @param array $element The element to report abuse on.
     * @return void
     */
    private function reportAbuse(string $collection, array $element): void
    {
        $action = "reportAbuse";

        if ($collection == NewsInterface::COLLECTION) {
            $params = CO2::getThemeParams();
            $abuseMax = $params["nbReportCoModeration"];
            $thisNews = $this->getModelNews()->getById($element["_id"]);
            if ($thisNews["reportAbuseCount"] >= $abuseMax) {
                $this->getModelProposal()->createModeration(NewsInterface::COLLECTION, (string) $element["_id"]);
            }
        } elseif ($collection == CommentInterface::COLLECTION) {
            $element = ($element["_id"]) ? $this->db->findOne($collection, [
                "_id" => $this->db->MongoId($element["_id"]),
            ]) : null;
            if (isset($element[$action . "Count"]) && $element[$action . "Count"] >= 3) {
                $this->db->update(
                    $collection,
                    [
                        "_id" => $this->db->MongoId($element["_id"]),
                    ],
                    [
                        '$set' => [
                            "isAnAbuse" => true,
                            "status" => "declaredAbused",
                        ],
                    ]
                );
            }
        }
    }

    /**
     * Get a list of actions.
     *
     * @param string $type The type of the action.
     * @param string $id The ID of the action.
     * @param string $actionType The type of the action.
     * @param int $indexStep The index step.
     * @return array|null The list of actions or null if not found.
     */
    public function getList(string $type, string $id, string $actionType, int $indexStep = 0): ?array
    {
        $object = $this->db->findOne(
            $type,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [$actionType . "Count", $actionType]
        );
        if (! empty($object) && ! empty($object[$actionType])) {
            foreach ($object[$actionType] as $key => $value) {
                $type = (@$value["type"]) ? $value["type"] : PersonInterface::COLLECTION;
                $target = $this->getModelElement()->getElementSimpleById($key, $type, null, ["slug", "name", "profilThumbImageUrl"]);
                $object[$actionType][$key] = array_merge($object[$actionType][$key], $target);
            }
        }
        return $object;
    }

    /**
     * Adds an action history entry.
     *
     * @param string|null $userId The ID of the user performing the action.
     * @param mixed|null $id The ID of the action.
     * @param string|null $collection The name of the collection.
     * @param string|null $action The action performed.
     * @return void
     */
    public function addActionHistory(?string $userId = null, $id = null, string $collection = null, $action = null): void
    {
        $currentAction = [
            "who" => $userId,
            "self" => $action,
            "collection" => $collection,
            "objectId" => $id,
            "created" => time(),
        ];
        $this->db->insert(ActivityStreamInterface::COLLECTION, $currentAction);
    }

    /**
     * Updates the parent of the action.
     *
     * @param string|null $id The ID of the action.
     * @param string|null $collection The collection of the action.
     * @return void
     */
    public function updateParent(?string $id = null, ?string $collection = null): void
    {
        $updatableParentTypes = [
            ActionRoomInterface::TYPE_ACTIONS => [
                "parentCollection" => ActionRoomInterface::COLLECTION,
                "parentField" => "room",
            ],
            SurveyInterface::COLLECTION => [
                "parentCollection" => ActionRoomInterface::COLLECTION,
                "parentField" => "survey",
            ],
        ];
        if ($obj = @$updatableParentTypes[$collection]) {
            $element = ($id) ? $this->db->findOne($collection, [
                "_id" => $this->db->MongoId($id),
            ]) : null;
            if (isset($element) && $parentId = @$element[$obj["parentField"]]) {
                $this->db->update(
                    $obj["parentCollection"],
                    [
                        "_id" => $this->db->MongoId($parentId),
                    ],
                    [
                        '$set' => [
                            "updated" => time(),
                        ],
                    ]
                );
            }
        }
    }

    /**
     * Check if a user is following a specific action type.
     *
     * @param mixed $value The value to check.
     * @param string $actionType The action type to check against.
     * @return bool Returns true if the user is following the action type, false otherwise.
     */
    public function isUserFollowing($value, string $actionType): bool
    {
        //return ( isset($value[ $actionType ]) && is_array($value[ $actionType ]) && in_array($this->session->getUserId(), $value[ $actionType ]) );
        $userId = $this->session->getUserId();
        return (isset($value[$actionType]) &&
                 is_array($value[$actionType]) &&
                (isset($value[$actionType][$userId]) || in_array($this->session->getUserId(), $value[$actionType]))
        );
    }

    /**
     * Vote for links and infos.
     *
     * @param bool $logguedAndValid Indicates if the user is logged in and valid.
     * @param array $value The value to be voted on.
     * @return array The updated array after voting.
     */
    public function voteLinksAndInfos(bool $logguedAndValid, array $value): array
    {
        $res = [
            "links" => "",
            "totalVote" => 0,
            "avoter" => "mesvotes",
            "hasVoted" => true,
        ];
        //has loged user voted on this entry
        //vote UPS
        $voteUpActive = ($logguedAndValid && $this->isUserFollowing($value, ActionInterface::ACTION_VOTE_UP)) ? "active" : "";
        $voteUpCount = $value[ActionInterface::ACTION_VOTE_UP . "Count"] ?? 0;
        $hrefUp = ($logguedAndValid && empty($voteUpActive)) ? "javascript:addaction('" . $value["_id"] . "','" . ActionInterface::ACTION_VOTE_UP . "')" : "";
        $classUp = $voteUpActive . " " . ActionInterface::ACTION_VOTE_UP . " " . $value["_id"] . ActionInterface::ACTION_VOTE_UP;
        $iconUp = ' fa-thumbs-up ';

        //vote ABSTAIN
        $voteAbstainActive = ($logguedAndValid && $this->isUserFollowing($value, ActionInterface::ACTION_VOTE_ABSTAIN)) ? "active" : "";
        $voteAbstainCount = $value[ActionInterface::ACTION_VOTE_ABSTAIN . "Count"] ?? 0;
        $hrefAbstain = ($logguedAndValid && empty($voteAbstainActive)) ? "javascript:addaction('" . (string) $value["_id"] . "','" . ActionInterface::ACTION_VOTE_ABSTAIN . "')" : "";
        $classAbstain = $voteAbstainActive . " " . ActionInterface::ACTION_VOTE_ABSTAIN . " " . $value["_id"] . ActionInterface::ACTION_VOTE_ABSTAIN;
        $iconAbstain = ' fa-circle';

        //vote UNCLEAR
        $voteUnclearActive = ($logguedAndValid && $this->isUserFollowing($value, ActionInterface::ACTION_VOTE_UNCLEAR)) ? "active" : "";
        $voteUnclearCount = $value[ActionInterface::ACTION_VOTE_UNCLEAR . "Count"] ?? 0;
        $hrefUnclear = ($logguedAndValid && empty($voteUnclearCount)) ? "javascript:addaction('" . $value["_id"] . "','" . ActionInterface::ACTION_VOTE_UNCLEAR . "')" : "";
        $classUnclear = $voteUnclearActive . " " . ActionInterface::ACTION_VOTE_UNCLEAR . " " . $value["_id"] . ActionInterface::ACTION_VOTE_UNCLEAR;
        $iconUnclear = " fa-pencil";

        //vote MORE INFO
        $voteMoreInfoActive = ($logguedAndValid && $this->isUserFollowing($value, ActionInterface::ACTION_VOTE_MOREINFO)) ? "active" : "";
        $voteMoreInfoCount = $value[ActionInterface::ACTION_VOTE_MOREINFO . "Count"] ?? 0;
        $hrefMoreInfo = ($logguedAndValid && empty($voteMoreInfoCount)) ? "javascript:addaction('" . $value["_id"] . "','" . ActionInterface::ACTION_VOTE_MOREINFO . "')" : "";
        $classMoreInfo = $voteMoreInfoActive . " " . ActionInterface::ACTION_VOTE_MOREINFO . " " . $value["_id"] . ActionInterface::ACTION_VOTE_MOREINFO;
        $iconMoreInfo = " fa-question-circle";

        //vote DOWN
        $voteDownActive = ($logguedAndValid && $this->isUserFollowing($value, ActionInterface::ACTION_VOTE_DOWN)) ? "active" : "";
        $voteDownCount = $value[ActionInterface::ACTION_VOTE_DOWN . "Count"] ?? 0;
        $hrefDown = ($logguedAndValid && empty($voteDownActive)) ? "javascript:addaction('" . (string) $value["_id"] . "','" . ActionInterface::ACTION_VOTE_DOWN . "')" : "";
        $classDown = $voteDownActive . " " . ActionInterface::ACTION_VOTE_DOWN . " " . $value["_id"] . ActionInterface::ACTION_VOTE_DOWN;
        $iconDown = " fa-thumbs-down";

        //votes cannot be changed, link become spans
        if (! empty($voteUpActive) || ! empty($voteAbstainActive) || ! empty($voteDownActive) || ! empty($voteUnclearActive) || ! empty($voteMoreInfoActive)) {
            $linkVoteUp = ($logguedAndValid && ! empty($voteUpActive)) ?
                            "<span class='" . $classUp . " ' ><i class='fa fa-caret-bottom'></i> " .
                                $this->language->t("survey", "Voted") .
                                " <span class='btnvote color-btnvote-green'><i class='fa $iconUp' ></i> Pour</span></span>" : "";
            $linkVoteAbstain = ($logguedAndValid && ! empty($voteAbstainActive)) ?
                            "<span class='" . $classAbstain . " '><i class='fa fa-caret-bottom'></i> " .
                                $this->language->t("survey", "Voted") .
                                " <span class='btnvote color-btnvote-white'><i class='fa $iconAbstain'></i> Blanc</span></span>" : "";
            $linkVoteUnclear = ($logguedAndValid && ! empty($voteUnclearActive)) ?
                            "<span class='" . $classUnclear . " '><i class='fa fa-caret-bottom'></i> " .
                                $this->language->t("survey", "Voted") .
                                " <span class='btnvote color-btnvote-blue'><i class='fa  $iconUnclear'></i> Incompris</span></span>" : "";
            $linkVoteMoreInfo = ($logguedAndValid && ! empty($voteMoreInfoActive)) ?
                            "<span class='" . $classMoreInfo . " '><i class='fa fa-caret-bottom'></i> " .
                                $this->language->t("survey", "Voted") .
                                " <span class='btnvote color-btnvote-purple'><i class='fa  $iconMoreInfo'></i> Incomplet</span></span>" : "";
            $linkVoteDown = ($logguedAndValid && ! empty($voteDownActive)) ?
                            "<span class='" . $classDown . " '><i class='fa fa-caret-bottom'></i> " .
                                $this->language->t("survey", "Voted") .
                                " <span class='btnvote color-btnvote-red'><i class='fa $iconDown'></i> Contre</span></span>" : "";
        } else {
            $res["avoter"] = "avoter";
            $res["hasVoted"] = false;

            $linkVoteUp = ($logguedAndValid) ? "<a class='btn " . $classUp . " voteIcon' data-vote='" . ActionInterface::ACTION_VOTE_UP . "' href=\" " . $hrefUp . " \" title='Voter Pour'><i class='fa $iconUp' ></i></a>" : "";
            $linkVoteAbstain = ($logguedAndValid) ? "<a class='btn " . $classAbstain . " voteIcon'  data-vote='" . ActionInterface::ACTION_VOTE_ABSTAIN . "' href=\"" . $hrefAbstain . "\" title='Voter Blanc'><i class='fa $iconAbstain'></i></a>" : "";
            $linkVoteUnclear = ($logguedAndValid) ? "<a class='btn " . $classUnclear . " voteIcon' data-vote='" . ActionInterface::ACTION_VOTE_UNCLEAR . "' href=\"" . $hrefUnclear . "\" title='Voter Pas Clair, Pas fini, Amender'><i class='fa $iconUnclear'></i></a>" : "";
            $linkVoteMoreInfo = ($logguedAndValid) ? "<a class='btn " . $classMoreInfo . " voteIcon' data-vote='" . ActionInterface::ACTION_VOTE_MOREINFO . "' href=\"" . $hrefMoreInfo . "\" title=\"Voter Pour Plus d'informations\"><i class='fa $iconMoreInfo'></i></a>" : "";
            $linkVoteDown = ($logguedAndValid) ? "<a class='btn " . $classDown . " voteIcon' data-vote='" . ActionInterface::ACTION_VOTE_DOWN . "' href=\"" . $hrefDown . "\" title='Voter Contre'><i class='fa $iconDown'></i></a>" : "";
        }

        //default Values are hasn't voted
        $res["totalVote"] = $voteUpCount + $voteAbstainCount + $voteDownCount + $voteUnclearCount + $voteMoreInfoCount;
        $res["ordre"] = $voteUpCount + $voteDownCount;
        $res["links"] = ($value["type"] == SurveyInterface::TYPE_ENTRY) ? "<span class='text-bold active btnvote color-btnvote-red'><i class='fa fa-clock-o'></i> " . $this->language->t("survey", "You did not vote", null, $this->moduleId) . "</span>" : "";

        if ((
            $value["type"] == SurveyInterface::TYPE_ENTRY
                && (! isset($value["dateEnd"]) || $value["dateEnd"] > time())
        ) || ($res["hasVoted"])
        ) {
            $res["links"] = "<div class='leftlinks'>" . $linkVoteUp . " " . $linkVoteUnclear . " " . $linkVoteAbstain . " " . $linkVoteMoreInfo . " " . $linkVoteDown . "</div>";
        } else {
            $res["avoter"] = "closed";
        }

        return $res;
    }

    // TODO : check fonction data
    /**
     * Format the parameters before saving.
     *
     * @param array $params The parameters to be formatted.
     * @return array The formatted parameters.
     */
    public function formatBeforeSaving(array $params): array
    {
        if (@$params["startDate"] && ! is_a($params["startDate"], 'MongoDate')) {
            $startDate = $this->getModelDataValidator()->getDateTimeFromString($params['startDate'], "start date");
            $params["startDate"] = $this->db->MongoDate($startDate->getTimestamp());
        }
        if (@$params["endDate"] && ! is_a($params["endDate"], 'MongoDate')) {
            $endDate = $this->getModelDataValidator()->getDateTimeFromString($params['endDate'], "end date");
            $params["endDate"] = $this->db->MongoDate($endDate->getTimestamp());
        }
        return [
            'result' => true,
            "params" => $params,
        ];
    }
}
