<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ClassifiedInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;

use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

//Yii::get('module')->getModule( self::MODULE )->assetsUrl
//CO2::getModuleContextList

class Classified extends BaseModel implements ClassifiedInterface, DocumentTraitInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DocumentTrait;
    use ElementTrait;

    //TODO Translate
    public array $classifiedTypes = [
        "health" => "Santé",
        "work" => "Travail",
        "retail" => "Immobilier",
    ];

    public array $classifiedSubTypes = [
        "health" => [
            "subType" => ["remplacement", "emploi", "cession", "stagiaire", "assistanat", "collaborateur", "mission humanitaire"],
        ],
        "work" => [
            "subType" => ["cherche travail", "proposition de travail", "bénévolat", "stagiaire"],
        ],
        "retail" => [
            "subType" => ["cherche collocation", "cherche location", "offre de vends", "cherche achat", "offre de location"],
        ],
    ];

    //From Post/Form name to database field name
    public array $dataBinding = [
        "section" => [
            "name" => "section",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "category" => [
            "name" => "category",
        ],
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
        ],
        "geoPosition" => [
            "name" => "geoPosition",
        ],
        "description" => [
            "name" => "description",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "price" => [
            "name" => "price",
        ],
        "devise" => [
            "name" => "devise",
        ],
        "public" => [
            "name" => "public",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "contactInfo" => [
            "name" => "contactInfo",
            "rules" => ["required"],
        ],
        "source" => [
            "name" => "source",
            "rules" => ["source"],
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    // Todo : Yii::get('module')->getModule( self::MODULE )->assetsUrl  et CO2::getModuleContextList ne devrait pas être dans un modele
    /**
     * Get the configuration for the Classified class.
     *
     * @param string|null $context The context for the configuration. Default is null.
     * @return array The configuration array.
     */
    public function getConfig(?string $context = null): array
    {
        return [
            "collection" => ClassifiedInterface::COLLECTION,
            "controller" => ClassifiedInterface::CONTROLLER,
            "module" => ClassifiedInterface::MODULE,
            "init" => Yii::get('module')->getModule(ClassifiedInterface::MODULE)->assetsUrl . "/js/init.js",
            "form" => Yii::get('module')->getModule(ClassifiedInterface::MODULE)->assetsUrl . "/js/" . $context . "/dynForm.js",
            "categories" => CO2::getModuleContextList(ClassifiedInterface::MODULE, "categories", $context),
            "deviseTheme" => [
                "all" => "All",
                "€" => "€",
                "Ğ1" => "Ğ1",
                "£" => "£",
                "$" => "$",
                "CFP" => "CFP",
            ],
            "eco" => "€dzfzfezfezfz",
            "deviseDefault" => "All",
            "lbhp" => true,
        ];
    }

    /**
     * Get a classified by its ID.
     *
     * @param string $id The ID of the classified.
     * @return array The classified data.
     */
    public function getById(string $id): ?array
    {
        $classified = $this->db->findOneById(ClassifiedInterface::COLLECTION, $id);
        if (empty($classified)) {
            return null;
        }
        $classified["images"] = $this->listImages($id);
        $classified["files"] = $this->listFiles($id);
        return $classified;
    }

    /**
     * Counts the number of classifieds by type and section.
     *
     * @param string $type The type of classifieds.
     * @param mixed $section The section of classifieds (optional).
     * @return int The number of classifieds.
     */
    public function countBy(string $type, $section = null): int
    {
        $where = [
            "type" => $type,
            "state" => [
                '$nin' => ["uncomplete", "deleted"],
            ],
            "status" => [
                '$nin' => ["uncomplete", "deleted", "deletePending"],
            ],
            '$or' => [[
                'preferences.private' => [
                    '$exists' => false,
                ],
            ], [
                'preferences.private' => false,
            ]],
        ];

        if (! empty($section)) {
            $where["section"] = $section;
        }
        return $this->db->count(ClassifiedInterface::COLLECTION, $where);
    }

    /**
     * Retrieves a classified by its creator.
     *
     * @param string $id The ID of the classified.
     * @return array The classified data.
     */
    public function getClassifiedByCreator(string $id): array
    {
        $allClassified = $this->db->findAndSort(ClassifiedInterface::COLLECTION, [
            "creator" => $id,
        ], [
            "updated" => -1,
        ]);
        foreach ($allClassified as $key => $value) {
            if (@$value["creator"]) {
                $parent = $this->getModelElement()->getElementById(@$value["creator"], "citoyens");
                $aParent = [
                    "name" => @$parent["name"],
                    "profilThumbImageUrl" => @$parent["profilThumbImageUrl"],
                ];
            } else {
                $aParent = [];
            }

            $allClassified[$key]["parent"] = $aParent;
            $allClassified[$key]["category"] = @$allClassified[$key]["type"];
            $allClassified[$key]["type"] = "classified";
            $allClassified[$key]["typeSig"] = ClassifiedInterface::COLLECTION;
        }
        return $allClassified;
    }
}
