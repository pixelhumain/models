<?php

namespace PixelHumain\Models\Services;

use Exception;

use InvalidArgumentException;

use PixelHumain\Models\Interfaces\ParamsInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;

class ParamsService implements ParamsServiceInterface
{
    /**
     * @var mixed $params The parameters for the model.
     */
    private $params;

    /**
     * ParamsService constructor.
     *
     * @param array|ParamsInterface $params The parameters for the service.
     */
    public function __construct($params)
    {
        if ($params instanceof ParamsInterface || is_array($params)) {
            $this->params = $params;
        } else {
            throw new InvalidArgumentException("params must be an instance of Params or an array");
        }
    }

    /**
     * Get a value from the params by key.
     *
     * @param string $key The key of the value to retrieve from the params.
     * @param mixed $default The default value to return if the key is not found in the params.
     * @return mixed The value from the params if found, otherwise the default value.
     */
    public function get(string $key, $default = null)
    {
        if ($this->params instanceof ParamsInterface) {
            return $this->params[$key] ?? $default;
        } elseif (is_array($this->params)) {
            return $this->params[$key] ?? $default;
        } else {
            throw new Exception("params must be an instance of Params or an array");
        }
    }

    /**
     * Set a value in the params by key.
     *
     * @param string $key The key of the value to set in the params.
     * @param mixed $value The value to set in the params.
     * @return void
     */
    public function set(string $key, $value): void
    {
        if ($this->params instanceof ParamsInterface) {
            $this->params[$key] = $value;
        } elseif (is_array($this->params)) {
            $this->params[$key] = $value;
        } else {
            throw new Exception("params must be an instance of Params or an array");
        }
    }
}
