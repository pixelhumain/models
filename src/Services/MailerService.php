<?php

namespace PixelHumain\Models\Services;

use PixelHumain\Models\Services\Interfaces\Mailer\ExtendedMailerInterface;

// use yii\symfonymailer\Mailer;
// use Symfony\Component\Mailer\Transport\TransportInterface;

use PixelHumain\Models\Services\Interfaces\Mailer\MessageInterface;
use PixelHumain\Models\Services\Interfaces\Mailer\TransportInterface;
use PixelHumain\Models\Services\Interfaces\MailerServiceInterface;

class MailerService implements MailerServiceInterface
{
    private ExtendedMailerInterface $mailer;

    public function __construct(ExtendedMailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Définit le transport pour l'envoi des e-mails.
     * @param array|TransportInterface $transport Le transport à utiliser.
     */
    public function setTransport($transport): void
    {
        $this->mailer->setTransport($transport);
    }

    /**
     * Sends a message using the MailerService.
     *
     * @param MessageInterface $message The message to be sent.
     * @return bool Returns true if the message was sent successfully, false otherwise.
     */
    public function send($message)
    {
        return $this->mailer->send($message);
    }

    /**
     * Compose a new email message.
     *
     * @param string|null|array $view The view template for the email.
     * @param array $params The parameters to be passed to the view template.
     * @return MessageInterface The composed email message.
     */
    public function compose($view = null, $params = [])
    {
        return $this->mailer->compose($view, $params);
    }

    public function sendMultiple($messages): int
    {
        return $this->mailer->sendMultiple($messages);
    }
}
