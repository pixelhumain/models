<?php

namespace PixelHumain\Models\Services;

use PixelHumain\Models\Interfaces\I18NInterface;

use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;

class LanguageService implements LanguageServiceInterface
{
    private I18NInterface $i18n;

    private ?string $currentLanguage = null;

    public function __construct(I18NInterface $i18n, ?string $language = null)
    {
        $this->i18n = $i18n;
        $this->currentLanguage = $language;
    }

    public function get(): ?string
    {
        return $this->currentLanguage;
    }

    public function set(?string $language): void
    {
        $this->currentLanguage = $language;
    }

    /**
     * Translates a message to the specified language.
     *
     * @param string $category the message category.
     * @param string $message the message to be translated.
     * @param array|null $params the parameters to be inserted into the translated message.
     * @param string|null $source the source language of the message. If null, the application language will be used.
     * @param string|null $language the target language to translate the message to. If null, the application language will be used.
     * @return string the translated message.
     */
    public function t($category, $message, $params = [], $source = null, $language = null): string
    {
        $language ??= $this->get();
        return $this->i18n->translate($category, $message, $params, $language);
    }
}
