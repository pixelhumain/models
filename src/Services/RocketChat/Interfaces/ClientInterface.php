<?php

namespace PixelHumain\Models\Services\RocketChat\Interfaces;

interface ClientInterface
{
    /**
     * Sends a request to the RocketChat API.
     *
     * @param string $method The HTTP method to use for the request.
     * @param string $uri The URI of the API endpoint.
     * @param array $options The options for the request.
     * @return object|null The response from the API.
     */
    public function sendRequest(string $method, string $uri, array $options = []): ?object;

    /**
     * Update the authentication headers with the provided auth token and user ID.
     *
     * @param string $authToken The authentication token.
     * @param string $userId The user ID.
     * @return void
     */
    public function updateAuthHeaders(string $authToken, string $userId): void;

    /**
     * Check if the user is authenticated as an admin.
     *
     * @return bool Returns true if the user is authenticated as an admin, false otherwise.
     */
    public function isAdminAuth(): bool;

    /**
     * Check if the user is authenticated as an admin.
     *
     * @return bool Returns true if the user is authenticated as an admin, false otherwise.
     */
    public function isUserAuth(): bool;

    /**
     * Check if the RocketChat client is authenticated.
     *
     * @return bool Returns true if the client is authenticated, false otherwise.
     */
    public function isAuth(): bool;
}
