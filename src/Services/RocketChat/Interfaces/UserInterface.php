<?php

namespace PixelHumain\Models\Services\RocketChat\Interfaces;

interface UserInterface
{
    /**
     * Log in the user to RocketChat.
     *
     * @param bool $save_auth Whether to save the authentication or not.
     * @return mixed The response from the API.
     */
    public function login(bool $save_auth = true);

    /**
     * Retrieves information about the RocketChat user.
     *
     * @return mixed The user information.
     */
    public function info();

    /**
     * Creates a new RocketChat user.
     *
     * @return mixed The user information.
     */
    public function create();

    /**
     * Deletes the RocketChat user.
     *
     * @return bool The response from the API.
     */
    public function delete(): bool;

    /**
     * Posts a message to the RocketChat user.
     *
     * @param string|array $text The message text.
     * @return bool The response from the API.
     */
    public function postMessage($text): bool;

    /**
     * List all the joined users in RocketChat.
     *
     * @return array|string The list of joined users.
     */
    public function listJoined();
}
