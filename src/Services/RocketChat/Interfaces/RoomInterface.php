<?php

namespace PixelHumain\Models\Services\RocketChat\Interfaces;

interface RoomInterface
{
    /**
     * Creates a new RocketChat channel.
     *
     * @return mixed The response from the API.
     */
    public function create();

    /**
     * Retrieves information about the RocketChat channel.
     *
     * @return mixed The channel information.
     */
    public function info();

    /**
     * Posts a message to the RocketChat channel.
     *
     * @param string|array $text The text of the message.
     * @return bool Whether the message was posted or not.
     */
    public function postMessage($text): bool;

    /**
     * Closes the RocketChat channel.
     *
     * @return bool Whether the channel was closed or not.
     */
    public function close(): bool;

    /**
     * Deletes the RocketChat channel.
     *
     * @return bool Whether the channel was deleted or not.
     */
    public function delete(): bool;

    /**
     * Kicks a user from the RocketChat channel.
     *
     * @param string|object $user The username of the user to be kicked.
     * @return bool Whether the user was kicked or not.
     */
    public function kick($user): bool;

    /**
     * Invite a user to the RocketChat channel.
     *
     * @param string|object $user The username of the user to invite.
     * @return mixed|null Whether the user was invited or not.
     */
    public function invite($user);

    /**
     * Adds an owner to the RocketChat channel.
     *
     * @param string|object $user The username of the user to be added as an owner.
     * @return mixed|null Whether the user was added as an owner or not.
     */
    public function addOwner($user);

    /**
     * Removes the owner role from a user in the RocketChat channel.
     *
     * @param string|object  $user The username of the user to remove the owner role from.
     * @return mixed|null Whether the user was removed as an owner or not.
     */
    public function removeOwner($user);

    /**
     * Set the type of the RocketChat channel.
     *
     * @param string $name The name of the channel.
     * @param string $type The type of the channel.
     * @return mixed|null Whether the type was set or not.
     */
    public function setType(string $name, string $type);

    /**
     * Renames the RocketChat channel.
     *
     * @param string $newName The new name for the channel.
     * @return mixed|null Whether the channel was renamed or not.
     */
    public function rename(string $newName);

    /**
     * Set the custom fields for the RocketChat channel.
     *
     * @param array $customFields The custom fields to set.
     * @return mixed|null Whether the custom fields were set or not.
     */
    public function setCustomFields(array $customFields);
}
