<?php

namespace PixelHumain\Models\Services\RocketChat;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;
use GuzzleHttp\Exception\GuzzleException;

use PixelHumain\Models\Services\RocketChat\Interfaces\ClientInterface;

abstract class Client implements ClientInterface
{
    private string $api;

    protected ?GuzzleClientInterface $guzzle = null;

    private string $rocketchatURL;

    private string $rocketchatAPiURI;

    private ?string $loginToken;

    private ?string $rocketUserId;

    private string $adminLoginToken;

    private string $adminRocketUserId;

    /**
     * @var mixed $admin The admin property.
     */
    private $admin;

    /**
     * Constructor for the RocketChatClient class.
     *
     * @param array $config The configuration for the RocketChatClient.
     * @param mixed $admin The admin parameter (optional).
     */
    public function __construct(array $config)
    {
        [
            'rocketchatURL' => $rocketchatURL,
            'rocketchatAPiURI' => $rocketchatAPiURI,
            'loginToken' => $loginToken,
            'rocketUserId' => $rocketUserId,
            'adminLoginToken' => $adminLoginToken,
            'adminRocketUserId' => $adminRocketUserId,
            'admin' => $admin
        ] = $config;

        $this->api = (string) $rocketchatURL . (string) $rocketchatAPiURI;
        $this->rocketchatURL = (string) $rocketchatURL;
        $this->rocketchatAPiURI = (string) $rocketchatAPiURI;
        $this->loginToken = $loginToken ?? null;
        $this->rocketUserId = $rocketUserId ?? null;
        $this->adminLoginToken = (string) $adminLoginToken;
        $this->adminRocketUserId = (string) $adminRocketUserId;
        $this->admin = $admin ?? null;
    }

    /**
     * Sends a request to the RocketChat API.
     *
     * @param string $method The HTTP method to use for the request.
     * @param string $uri The URI of the API endpoint.
     * @param array $options The options for the request.
     * @return object|null The response from the API.
     */
    public function sendRequest(string $method, string $uri, array $options = []): ?object
    {
        try {
            if (! $this->isAuth()) {
                // Log or handle the exception as needed
                return null;
            }

            if ($this->admin === true) {
                if ($this->isAdminAuth()) {
                    $this->updateAuthHeaders($this->adminLoginToken, $this->adminRocketUserId);
                } else {
                    // Log or handle the exception as needed
                    return null;
                }
            } elseif ($this->admin === 'noHeader') {
                // do nothing
                $this->guzzle = new GuzzleClient([
                    'base_uri' => $this->api,
                ]);
            } else {
                if ($this->isUserAuth()) {
                    $this->updateAuthHeaders((string) $this->loginToken, (string) $this->rocketUserId);
                } else {
                    // Log or handle the exception as needed
                    return null;
                }
            }
            if ($this->guzzle !== null) {
                $response = $this->guzzle->request($method, $uri, $options);
            } else {
                // Log or handle the exception as needed
                return null;
            }
            return (object) json_decode((string) $response->getBody());
        } catch (GuzzleException $e) {
            // Log or handle the exception as needed
            return null;
        }
    }

    /**
     * Check if the user is authenticated as an admin.
     *
     * @return bool Returns true if the user is authenticated as an admin, false otherwise.
     */
    public function isAdminAuth(): bool
    {
        return ! empty($this->adminLoginToken) && ! empty($this->adminRocketUserId);
    }

    /**
     * Check if the user is authenticated as an admin.
     *
     * @return bool Returns true if the user is authenticated as an admin, false otherwise.
     */
    public function isUserAuth(): bool
    {
        return ! empty($this->loginToken) && ! empty($this->rocketUserId);
    }

    /**
     * Check if the RocketChat client is authenticated.
     *
     * @return bool Returns true if the client is authenticated, false otherwise.
     */
    public function isAuth(): bool
    {
        return $this->isAdminAuth() || $this->isUserAuth();
    }

    /**
     * Update the authentication headers with the provided auth token and user ID.
     *
     * @param string $authToken The authentication token.
     * @param string $userId The user ID.
     * @return void
     */
    public function updateAuthHeaders(string $authToken, string $userId): void
    {
        $this->guzzle = new GuzzleClient([
            'base_uri' => $this->api,
            'headers' => [
                'X-Auth-Token' => $authToken,
                'X-User-Id' => $userId,
            ],
        ]);
    }

    // /**
    //  * Returns the version of the RocketChat client.
    //  *
    //  * @return string The version of the RocketChat client.
    //  */
    // public function version(): string
    // {
    //     $response = $this->sendRequest('GET', 'info');
    //     return is_object($response) && is_object($response->info) && isset($response->info->version) ? (string) $response->info->version : 'Unknown';
    // }

    // /**
    //  * Retrieves information about the authenticated user.
    //  *
    //  * @return mixed The user information.
    //  */
    // public function me() {
    //     $response = $this->sendRequest('GET', 'me');
    //     return $response && $response->status !== 'error' ? $response : false;
    // }

    // /**
    //  * Retrieves a list of users from the RocketChat server.
    //  *
    //  * @return mixed The list of users.
    //  */
    // public function list_users() {
    //     $response = $this->sendRequest('GET', 'users.list');
    //     return $response && $response->success ? $response->users : false;
    // }

    // /**
    //  * Retrieves a list of groups from the RocketChat server.
    //  *
    //  * @return mixed An array containing the groups.
    //  */
    // public function list_groups() {
    //     $response = $this->sendRequest('GET', 'groups.list');
    //     if ($response && $response->success) {
    //         return array_map(function ($group) {
    //             return new Group($this->rocketchatURL, $this->rocketchatAPiURI, (object) $group, [], $this->loginToken, $this->rocketUserId, $this->admin);
    //         }, (array) $response->groups);
    //     }
    //     return false;
    // }

    // /**
    //  * Retrieves a list of channels from the RocketChat server.
    //  *
    //  * @return mixed An array containing the list of channels.
    //  */
    // public function list_channels() {
    //     $response = $this->sendRequest('GET', 'channels.list');
    //     if ($response && $response->success) {
    //         return array_map(function ($channel) {
    //             return new Channel($this->rocketchatURL, $this->rocketchatAPiURI, (object) $channel, [], $this->loginToken, $this->rocketUserId, $this->admin);
    //         }, (array) $response->channels);
    //     }
    //     return false;
    // }
}
