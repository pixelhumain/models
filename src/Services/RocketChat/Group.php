<?php

namespace PixelHumain\Models\Services\RocketChat;

use GuzzleHttp\Exception\GuzzleException;
use PixelHumain\Models\Services\RocketChat\Interfaces\RoomInterface;

class Group implements RoomInterface
{
    public ?string $id;

    public string $name;

    public array $members = [];

    protected User $user;

    /**
     * Constructor for the RocketChatGroup class.
     *
     * @param User $user The user of the RocketChat group.
     * @param string|object $name The name of the RocketChat group.
     * @param array $members An array of members in the RocketChat group.
     */
    public function __construct(User $user, $name, array $members = [])
    {
        $this->user = $user;
        $this->name = is_string($name) ? $name : (string) $name->name;
        $this->id = is_object($name) && $name->_id ? (string) $name->_id : null;
        foreach ($members as $member) {
            $this->members[] = is_string($member) ? new User($this->user->config, $member) : $member;
        }
    }

    /**
     * Creates a new RocketChat group.
     *
     * @return mixed
     */
    public function create()
    {
        $members_id = array_map(fn ($member) => (string) $member->username, $this->members);
        try {
            $body = $this->user->sendRequest('POST', 'groups.create', [
                'json' => [
                    'name' => $this->name,
                    'members' => $members_id,
                ],
            ]);

            if (is_object($body) && $body->success && is_object($body->group) && isset($body->group->_id)) {
                $this->id = $body->group->_id;
            }
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Retrieves information about the RocketChatGroup.
     *
     * @return mixed The group information.
     */
    public function info()
    {
        try {
            $body = $this->user->sendRequest('GET', 'groups.info', [
                'query' => [
                    'roomName' => $this->name,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Posts a message to the RocketChat group.
     *
     * @param string|array $text The text of the message.
     * @return bool Whether the message was posted or not.
     */
    public function postMessage($text): bool
    {
        try {
            $message = is_string($text) ? [
                'text' => $text,
            ] : $text;

            if (! isset($message['attachments'])) {
                $message['attachments'] = [];
            }

            $body = $this->user->sendRequest('POST', 'chat.postMessage', [
                'json' => array_merge([
                    'channel' => '#' . $this->name,
                ], $message),
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Closes the RocketChat group.
     *
     * @return bool Whether the group was closed or not.
     */
    public function close(): bool
    {
        try {
            $body = $this->user->sendRequest('POST', 'groups.close', [
                'json' => [
                    'roomId' => $this->id,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Deletes the RocketChat group.
     *
     * @return bool Whether the group was deleted or not.
     */
    public function delete(): bool
    {
        try {
            $body = $this->user->sendRequest('POST', 'groups.delete', [
                'json' => [
                    'roomId' => $this->id,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Kicks a user from the RocketChat group.
     *
     * @param string|object $user The username of the user to be kicked.
     * @return bool Whether the user was kicked or not.
     */
    public function kick($user): bool
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'groups.kick', [
                'json' => [
                    'roomId' => $this->id,
                    'userId' => $userId,
                ],
            ]);
            return $body && $body->success;
        } catch (GuzzleException $e) {
            return false; // Or handle the exception as required
        }
    }

    /**
     * Invite a user to the RocketChat group.
     *
     * @param string|object $user The username of the user to invite.
     * @return mixed|null Whether the user was invited or not.
     */
    public function invite($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'groups.invite', [
                'json' => [
                    'roomName' => $this->name,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Adds an owner to the RocketChat group.
     *
     * @param string|object $user The username of the owner to be added.
     * @return mixed|null Whether the owner was added or not.
     */
    public function addOwner($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'groups.addOwner', [
                'json' => [
                    'roomName' => $this->name,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Removes the owner role from a user in the RocketChat group.
     *
     * @param string|object $user The username of the user to remove the owner role from.
     * @return mixed|null Whether the owner role was removed or not.
     */
    public function removeOwner($user)
    {
        $userId = is_string($user) ? $user : $user->id;
        try {
            $body = $this->user->sendRequest('POST', 'groups.removeOwner', [
                'json' => [
                    'roomId' => $this->id,
                    'username' => $userId,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Set the type of the RocketChat group.
     *
     * @param string $name The name of the group.
     * @param string $type The type of the group.
     * @return mixed|null Whether the type was set or not.
     */
    public function setType(string $name, string $type)
    {
        try {
            $body = $this->user->sendRequest('POST', 'groups.setType', [
                'json' => [
                    'roomName' => $name,
                    'type' => $type,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Renames the RocketChat group.
     *
     * @param string $newName The new name for the group.
     * @return mixed|null Whether the group was renamed or not.
     */
    public function rename(string $newName)
    {
        try {
            $body = $this->user->sendRequest('POST', 'groups.rename', [
                'json' => [
                    'roomId' => $this->id,
                    'name' => $newName,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }

    /**
     * Set the custom fields for the RocketChatGroup.
     *
     * @param array $customFields An array of custom fields.
     * @return mixed|null Whether the custom fields were set or not.
     */
    public function setCustomFields(array $customFields)
    {
        try {
            $body = $this->user->sendRequest('POST', 'groups.setCustomFields', [
                'json' => [
                    'roomId' => $this->id,
                    'customFields' => $customFields,
                ],
            ]);
            return $body;
        } catch (GuzzleException $e) {
            return null; // Or handle the exception as required
        }
    }
}
