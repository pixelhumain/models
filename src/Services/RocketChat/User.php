<?php

namespace PixelHumain\Models\Services\RocketChat;

use GuzzleHttp\Exception\GuzzleException;
use PixelHumain\Models\Services\RocketChat\Interfaces\UserInterface;

// TODO : gérer les exeptions et les erreurs mieux que ça

class User extends Client implements UserInterface
{
    public ?string $username;

    private ?string $password;

    public ?string $id = null;

    public ?string $authToken = null;

    public ?string $status = null;

    public ?string $nickname;

    public ?string $email;

    public ?string $serviceEncrypt = null;

    public array $config;

    /**
     * Constructor for the RocketChatUser class.
     *
     * @param array $config The configuration settings.
     * @param string $username The username of the RocketChat user.
     * @param string|null $password The password of the RocketChat user (optional).
     * @param array $fields Additional fields for the RocketChat user (optional).
     * @param string|null $serviceEncrypt The encryption service to use (optional).
     */
    public function __construct(array $config, ?string $username = null, ?string $password = null, array $fields = [], ?string $serviceEncrypt = null)
    {
        $this->config = $config;
        parent::__construct($this->config);

        $this->username = $username;
        $this->password = $password;
        $this->serviceEncrypt = $serviceEncrypt;
        $this->nickname = $fields['nickname'] ?? null;
        $this->email = $fields['email'] ?? null;
    }

    /**
     * Log in the user to RocketChat.
     *
     * @param bool $save_auth Whether to save the authentication or not.
     * @return mixed The response from the API.
     */
    public function login(bool $save_auth = true)
    {
        if (empty($this->username) && (empty($this->password) || empty($this->serviceEncrypt))) {
            return 'Error: username or password or serviceEncrypt is empty';
        }

        $loginArray = isset($this->password) ?
                      [
                          'user' => $this->username,
                          'password' => $this->password,
                      ] :
                    [
                        'user' => $this->username,
                        'service' => $this->serviceEncrypt !== null ? json_decode($this->serviceEncrypt, true) : null,
                    ];

        try {
            $body = $this->sendRequest('POST', 'login', [
                'json' => $loginArray,
            ]);
            if (is_object($body) && isset($body->status) && $body->status == 'success' &&
            is_object($body->data) && isset($body->data->userId, $body->data->authToken)) {
                $this->id = (string) $body->data->userId;
                $this->authToken = (string) $body->data->authToken;
                $this->status = $body->status;
                if ($save_auth) {
                    $this->updateAuthHeaders($this->authToken, $this->id);
                }
                return $body;
            }
        } catch (GuzzleException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Retrieves information about the RocketChat user.
     *
     * @return mixed The user information.
     */
    public function info()
    {
        if (empty($this->username)) {
            return 'Error: username is empty';
        }

        try {
            $body = $this->sendRequest('GET', 'users.info', [
                'query' => [
                    'username' => $this->username,
                ],
            ]);
            if (is_object($body) && $body->success && is_object($body->user)) {
                $this->id = $body->user->_id;
                $this->nickname = $body->user->name;
                $this->email = is_array($body->user->emails) && is_object($body->user->emails[0]) && isset($body->user->emails[0]->address) ? $body->user->emails[0]->address : null;
            }
            return $body;
        } catch (GuzzleException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Creates a new RocketChat user.
     *
     * @return mixed The user information.
     */
    public function create()
    {
        if (empty($this->username) || empty($this->password) || empty($this->email)) {
            return 'Error: username or password or email is empty';
        }
        try {
            $body = $this->sendRequest('POST', 'users.create', [
                'json' => [
                    'name' => $this->nickname,
                    'email' => $this->email,
                    'username' => $this->username,
                    'password' => $this->password,
                ],
            ]);
            if (is_object($body) && $body->success && is_object($body->user)) {
                $this->id = $body->user->_id;
                return $body->user;
            }
        } catch (GuzzleException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }

    /**
     * Deletes the RocketChat user.
     *
     * @return bool The response from the API.
     */
    public function delete(): bool
    {
        if (empty($this->id)) {
            return false;
        }
        try {
            $body = $this->sendRequest('POST', 'users.delete', [
                'json' => [
                    'userId' => $this->id,
                ],
            ]);
            return is_object($body) && $body->success ? (bool) $body->success : false;
        } catch (GuzzleException $e) {
            return false;
        }
    }

    /**
     * Posts a message to the RocketChat user.
     *
     * @param string|array $text The message text.
     * @return bool The response from the API.
     */
    public function postMessage($text): bool
    {
        if (empty($this->username)) {
            return false;
        }

        try {
            $body = $this->sendRequest('POST', 'chat.postMessage', [
                'json' => [
                    'channel' => '@' . $this->username,
                    'text' => $text,
                    'attachments' => [],
                ],
            ]);
            return is_object($body) && $body->success ? (bool) $body->success : false;
        } catch (GuzzleException $e) {
            return false;
        }
    }

    /**
     * List all the joined users in RocketChat.
     *
     * @return array|string The list of joined users.
     */
    public function listJoined()
    {
        try {
            $channelsBody = $this->sendRequest('GET', 'channels.list.joined');
            $groupsBody = $this->sendRequest('GET', 'groups.list');

            $list = [];
            if (is_object($channelsBody) && $channelsBody->success) {
                foreach ($channelsBody->channels as $channel) {
                    if (is_object($channel)) {
                        $list[] = (string) $channel->name;
                    }
                }
            }

            if (is_object($groupsBody) && $groupsBody->success) {
                foreach ($groupsBody->groups as $group) {
                    if (is_object($group)) {
                        $list[] = (string) $group->name;
                    }
                }
            }

            return $list;
        } catch (GuzzleException $e) {
            return 'Error: ' . $e->getMessage();
        }
    }
}
