<?php

namespace PixelHumain\Models\Services\Yii2Symfonymailer;

use PixelHumain\Models\Services\Interfaces\Mailer\ExtendedMailerInterface;
use PixelHumain\Models\Services\Interfaces\Mailer\TransportInterface;
use yii\symfonymailer\Mailer;

class ExtendsMailer extends Mailer implements ExtendedMailerInterface
{
    private ?array $_defaultTransportConfig = null;

    private bool $_isDefaultConfigSaved = false;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        if (isset($config['transport'])) {
            $this->_defaultTransportConfig = $config['transport'];
            $this->_isDefaultConfigSaved = true;
        }
    }

    /**
     * Set the transport for the mailer.
     *
     * @param TransportInterface|array $transport The transport to set.
     * @return void
     */
    public function setTransport($transport): void
    {
        if (! $this->_isDefaultConfigSaved) {
            $this->_defaultTransportConfig = $transport;
            $this->_isDefaultConfigSaved = true;
        }
        parent::setTransport($transport);
    }

    protected function sendMessage($message): bool
    {
        $result = parent::sendMessage($message);
        // Réinitialisation de la configuration de transport à l'état par défaut
        if ($this->_isDefaultConfigSaved) {
            $this->setTransport($this->_defaultTransportConfig);
        }
        return $result;
    }
}
