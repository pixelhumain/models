<?php

namespace PixelHumain\Models\Services;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Services\Interfaces\RocketChatServiceInterface;

use PixelHumain\Models\Services\RocketChat\Channel;
use PixelHumain\Models\Services\RocketChat\Group;
use PixelHumain\Models\Services\RocketChat\User;

/**
 * A class for interacting with the RocketChat API.
 *
 * $rocketChat = RocketChatService([
 *   'rocketchatURL' => $rocketchatURL,
 *   'rocketchatAPiURI' => $rocketchatAPiURI,
 *   'loginToken' => $loginToken ?? null,
 *   'rocketUserId' => $rocketUserId ?? null,
 *   'adminLoginToken' => $adminLoginToken,
 *   'adminRocketUserId' => $adminRocketUserId,
 *   'admin' => $admin ?? null
 *   ]);
 *
 * $rocketChat->getToken($email, $pwd);
 * $rocketChat->getTokenService($email, $serviceEncrypt);
 * $rocketChat->createGroup($name, $type, $username, $inviteOnly, $owner);
 * $rocketChat->rename($name, $new, $type);
 * $rocketChat->setCustomFields($name, $customFields, $type);
 * $rocketChat->post($to, $msg, $url, $type, $alias, $avatar, $lien);
 * $rocketChat->listUserChannels($userEmail);
 * $rocketChat->inviteGroup($name, $username);
 */


class RocketChatService implements RocketChatServiceInterface
{
    private array $config;

    public function __construct(array $config)
    {
        if (! isset($config['rocketchatURL']) || ! isset($config['rocketchatAPiURI'])) {
            throw new InvalidArgumentException("Rocket Chat URL is not set in the configuration.");
        }
        if (! isset($config['adminLoginToken']) || ! isset($config['adminRocketUserId'])) {
            throw new InvalidArgumentException("Rocket admin conf is not set in the configuration.");
        }

        $this->config = $config;
    }

    /**
     * Retrieves the RocketChat token for the given email and password.
     *
     * @param string $email The user's email.
     * @param string $pwd The user's password.
     * @return array The RocketChat token.
     */
    public function getToken(string $email, string $pwd): array
    {
        $this->setAdmin(false);
        return $this->attemptLogin(new User($this->config, $email, $pwd));
    }

    /**
     * Retrieves the token service for the given email and service encryption.
     *
     * @param string $email The email address.
     * @param string $serviceEncrypt The service encryption.
     * @return array The token service.
     */
    public function getTokenService(string $email, string $serviceEncrypt): array
    {
        $this->setAdmin(false);
        return $this->attemptLogin(new User($this->config, $email, null, [], $serviceEncrypt));
    }

    /**
     * Set the admin status to true.
     *
     * @return void
     */
    private function setAdmin(bool $admin): void
    {
        $this->config["admin"] = $admin;
    }

    private function attemptLogin(User $user): array
    {
        try {
            $log = $user->login();

            if (is_object($log) && $log->status == "success") {
                return [
                    "loginToken" => $user->authToken,
                    "rocketUserId" => $user->id,
                    "msg" => "user logged in",
                ];
            } else {
                return [
                    "msg" => "Mail or password don't exist or match, can't log into RC: " . (is_object($log) && isset($log->message) ? (string) $log->message : 'Unknown error'),
                    "error" => "unauthorised",
                ];
            }
        } catch (Exception $e) {
            return [
                "msg" => $e->getMessage(),
                "error" => "noHost",
            ];
        }
    }

    /**
     * Creates a new group in RocketChat.
     *
     * @param string $name The name of the group.
     * @param string|null $type The type of the group (optional).
     * @param string $username The username of the group creator.
     * @param mixed|null $inviteOnly Determines if the group is invite-only (optional).
     * @param bool $owner Determines if the group creator is the owner (default: false).
     * @return object The response from the API.
     */
    public function createGroup(string $name, ?string $type = null, string $username, $inviteOnly = null, bool $owner = false): object
    {
        $this->setAdmin(true);
        $user = new User($this->config);

        $res = (object) [
            "name" => $name,
            "type" => ($type == "channel") ? "channel" : "group",
            "username" => $username,
        ];

        try {
            $channel = ($type == "channel") ? new Channel($user, $name, []) : new Group($user, $name, []);

            if (! $inviteOnly) {
                $res->create = $channel->create();
                if (is_object($res->create) && ! ($res->create->success ?? false)) {
                    $res->info = $channel->info();

                    if (is_object($res->info) && ! ($res->info->success ?? false)) {
                        $alternateType = ($type == "channel") ? new Group($user, $name, []) : new Channel($user, $name, []);
                        $res->info2 = $alternateType->info();

                        if (is_object($res->info2) && $res->info2->success ?? false) {
                            $switchType = ($type == "channel") ? "c" : "p";
                            $res->settype = $alternateType->setType($name, $switchType);
                        }
                    }
                }
            }

            if ($username) {
                $channelToUse = is_object($res->settype) && ($res->settype->channel ?? false) ? new Group($user, $name, []) : new Channel($user, $name, []);
                $res->invite = $channelToUse->invite($username);
                // if (is_object($res->invite) && is_object($res->invite->channel) && ($res->invite->channel->username ?? false)) {
                //     $channel->members = $res->invite->channel->username;
                // }
                // if (is_object($res->invite) && is_object($res->invite->group) && ($res->invite->group->username ?? false)) {
                //     $channel->members = $res->invite->group->username;
                // }

                if ($owner) {
                    $res->owner = $channelToUse->addOwner($username);
                }
            }
        } catch (Exception $e) {
            $res->msg = $e->getMessage();
        }

        return $res;
    }

    /**
     * Renames a RocketChat service.
     *
     * @param string $name The current name of the service.
     * @param string $new The new name for the service.
     * @param bool $type The type of the service.
     * @return object The response from the API.
     */
    public function rename(string $name, string $new, bool $type = false): object
    {
        $this->setAdmin(true);
        $user = new User($this->config);

        $res = (object) [
            "name" => $name,
            "type" => ($type === true) ? "group" : "channel",
        ];

        try {
            $channel = ($type === true) ? new Group($user, $name, []) : new Channel($user, $name, []);

            $res->info = $channel->info();

            $res->rename = $channel->rename($new);
        } catch (Exception $e) {
            $res->msg = $e->getMessage();
        }
        return $res;
    }

    /**
     * Sets the custom fields for a RocketChat service.
     *
     * @param string $name The name of the custom field.
     * @param array $customFields An array containing the custom fields.
     * @param bool $type The type of the custom field. Default is false.
     * @return object The updated RocketChatService object.
     */
    public function setCustomFields(string $name, array $customFields, bool $type = false): object
    {
        $this->setAdmin(true);
        $user = new User($this->config);

        $res = (object) [
            "name" => $name,
            "type" => ($type === true) ? "group" : "channel",
        ];

        try {
            $channel = ($type === true) ? new Group($user, $name, []) : new Channel($user, $name, []);

            $res->info = $channel->info();

            $res->setCustomFields = $channel->setCustomFields($customFields);
        } catch (Exception $e) {
            $res->msg = $e->getMessage();
        }
        return $res;
    }

    /**
     * Sends a post request to RocketChat.
     *
     * @param string $to The recipient of the message.
     * @param string $msg The content of the message.
     * @param string $url The URL to be included in the message.
     * @param bool $type The type of the message.
     * @param string $alias The alias to be displayed in the message.
     * @param string $avatar The URL of the avatar to be displayed in the message.
     * @param string $lien The link to be displayed in the message.
     * @return array The response from the API.
     */
    public function post(string $to, string $msg, string $url = "", bool $type = false, string $alias = "oceco", string $avatar = "https://oce.co.tools/avatar.png", string $lien = "Lien oceco")
    {
        $this->setAdmin(true);
        $user = new User($this->config);

        $res = [];

        try {
            $channel = $type ? new Channel($user, $to, []) : new Group($user, $to, []);

            $res = [
                "name" => $to,
                "type" => $type ? "channel" : "group",
                "alias" => $alias,
                "avatar" => $avatar,
                "text" => $msg,
            ];
            if ($url != "" && $lien != "") {
                $res["attachments"] = [[
                    "color" => '#E33551',
                    "button_alignment" => 'horizontal',
                    "actions" => [
                        [
                            "type" => 'button',
                            "text" => $lien,
                            "url" => $url,
                        ],
                    ],
                ]];
            }
            $res['info'] = $channel->info();
            $res['post'] = $channel->postMessage($res);
        } catch (Exception $e) {
            $res['msg'] = $e->getMessage();
        }
        return $res;
    }

    /**
     * Lists the channels that a user is a member of.
     *
     * @param string $userEmail The email address of the user.
     * @return mixed An array of channel names.
     */
    public function listUserChannels(string $userEmail)
    {
        $this->setAdmin(false);
        $logged = new User($this->config, $userEmail);
        return $logged->listJoined();
    }

    /**
     * Invite a user to a group in RocketChat.
     *
     * @param string $name The name of the group.
     * @param string $username The username of the user to be invited.
     * @return object The response from the API.
     */
    public function inviteGroup(string $name, string $username): object
    {
        $this->setAdmin(true);
        $user = new User($this->config);

        $res = (object) [
            "name" => $name,
            "type" => "group",
        ];

        try {
            $channel = new Group($user, $name, []);

            $res->info = $channel->info();

            $res->invite = $channel->invite($username);
        } catch (Exception $e) {
            $res->msg = $e->getMessage();
        }
        return $res;
    }
}
