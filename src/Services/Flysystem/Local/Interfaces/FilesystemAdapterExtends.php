<?php

namespace PixelHumain\Models\Services\Flysystem\Local\Interfaces;

use League\Flysystem\FilesystemAdapter;

interface FilesystemAdapterExtends extends FilesystemAdapter
{
    public function has(string $location): bool;

    public function directoryExists(string $location): bool;
}
