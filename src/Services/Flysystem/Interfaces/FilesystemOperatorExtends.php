<?php

namespace PixelHumain\Models\Services\Flysystem\Interfaces;

use League\Flysystem\FilesystemOperator;

interface FilesystemOperatorExtends extends FilesystemOperator
{
    public function has(string $location): bool;

    public function directoryExists(string $location): bool;
}
