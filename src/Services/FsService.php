<?php

namespace PixelHumain\Models\Services;

use League\Flysystem\DirectoryListing;

// use League\Flysystem\FilesystemOperator;

use PixelHumain\Models\Services\Flysystem\Interfaces\FilesystemOperatorExtends;
use PixelHumain\Models\Services\Interfaces\FsServiceInterface;

/**
 * This class is a wrapper around the Flysystem FilesystemOperatorInterface
 *
 * exemple of use:
 * $adapter = new \League\Flysystem\Local\LocalFilesystemAdapter($rootPath);
 * $fs = new \League\Flysystem\Filesystem($adapter);
 */

class FsService implements FsServiceInterface
{
    protected FilesystemOperatorExtends $fs;

    public function __construct(FilesystemOperatorExtends $fs)
    {
        $this->fs = $fs;
    }

    public function fileExists(string $location): bool
    {
        return $this->fs->fileExists($location);
    }

    public function read(string $location): string
    {
        return $this->fs->read($location);
    }

    public function has(string $location): bool
    {
        return $this->fs->has($location);
    }

    public function directoryExists(string $location): bool
    {
        return $this->fs->directoryExists($location);
    }

    /**
     * @return resource
     */
    public function readStream(string $location)
    {
        return $this->fs->readStream($location);
    }

    public function listContents(string $location, bool $deep = self::LIST_SHALLOW): DirectoryListing
    {
        return $this->fs->listContents($location, $deep);
    }

    public function lastModified(string $path): int
    {
        return $this->fs->lastModified($path);
    }

    public function fileSize(string $path): int
    {
        return $this->fs->fileSize($path);
    }

    public function mimeType(string $path): string
    {
        return $this->fs->mimeType($path);
    }

    public function visibility(string $path): string
    {
        return $this->fs->visibility($path);
    }

    public function write(string $location, string $contents, array $config = []): void
    {
        $this->fs->write($location, $contents, $config);
    }

    /**
     * @param mixed $contents
     *
     */
    public function writeStream(string $location, $contents, array $config = []): void
    {
        $this->fs->writeStream($location, $contents, $config);
    }

    public function setVisibility(string $path, string $visibility): void
    {
        $this->fs->setVisibility($path, $visibility);
    }

    public function delete(string $location): void
    {
        $this->fs->delete($location);
    }

    public function deleteDirectory(string $location): void
    {
        $this->fs->deleteDirectory($location);
    }

    public function createDirectory(string $location, array $config = []): void
    {
        $this->fs->createDirectory($location, $config);
    }

    public function move(string $source, string $destination, array $config = []): void
    {
        $this->fs->move($source, $destination, $config);
    }

    public function copy(string $source, string $destination, array $config = []): void
    {
        $this->fs->copy($source, $destination, $config);
    }
}
