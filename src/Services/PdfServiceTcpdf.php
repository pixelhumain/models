<?php

namespace PixelHumain\Models\Services;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Services\Interfaces\FsServiceInterface;
use PixelHumain\Models\Services\Interfaces\PdfServiceInterface;
use TCPDF;

class PdfServiceTcpdf implements PdfServiceInterface
{
    // TODO : TCPDF
    private TCPDF $pdf;

    // TODO : FS interface n'est pas fait
    private FsServiceInterface $fs;

    public function __construct(TCPDF $pdf, FsServiceInterface $fs)
    {
        $this->pdf = $pdf;
        $this->fs = $fs;
    }

    public function createPdf(array $params): void
    {
        if (empty($params)) {
            throw new InvalidArgumentException("No parameters provided for PDF creation");
        }

        $this->setDocumentInfo($params);
        $this->setHeaderAndFooter($params);
        $this->setDefaultSettings();
        $this->addContent($params);
        $this->outputPdf($params);
    }

    private function setDocumentInfo($params)
    {
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor($params["author"] ?? '');
        $this->pdf->SetTitle($params["title"] ?? '');
        $this->pdf->SetSubject($params["subject"] ?? '');
        $this->pdf->SetKeywords($params["keywords"] ?? '');
    }

    private function setHeaderAndFooter($params)
    {
        if (! isset($params["header"]) || $params["header"] !== false) {
            $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $this->pdf->setHeaderFont([PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN]);
        } else {
            $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 20, PDF_MARGIN_RIGHT);
            $this->pdf->setPrintHeader(false);
        }

        if (! isset($params["footer"]) || $params["footer"] !== false) {
            $this->pdf->setFooterFont([PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA]);
        } else {
            $this->pdf->setPrintFooter(false);
        }
    }

    private function setDefaultSettings()
    {
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $this->pdf->setFontSubsetting(true);
        $this->pdf->SetFont('dejavusans', '', 14, '', true);
    }

    private function addContent($params)
    {
        if (! empty($params["htmls"])) {
            foreach ($params["htmls"] as $html) {
                $this->pdf->AddPage();
                $this->setTextShadow();
                $this->pdf->writeHTML($html, true, false, false, false, '');
            }
        } elseif (isset($params["html"]) || isset($params["tplData"])) {
            $this->pdf->AddPage();
            $html = empty($params["tplData"]) ? $params["html"] : self::{ $params["tplData"]}($params);
            $this->setTextShadow();
            $this->pdf->writeHTML($html, true, false, false, false, '');
        }
    }

    private function setTextShadow()
    {
        $this->pdf->setTextShadow([
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => [196, 196, 196],
            'opacity' => 1,
            'blend_mode' => 'Normal',
        ]);
    }

    private function outputPdf($params)
    {
        $saveOption = $params["saveOption"] ?? "I";
        $docPath = $params["urlPath"] ?? '';
        $docPath .= $params["docName"] ?? ($params["title"] ?? "pdf") . '.pdf';

        ob_end_clean();

        if ($saveOption === 'I') {
            $this->pdf->Output($docPath, $saveOption);
        } elseif ($saveOption === "F") {
            $this->fs->write($docPath, $this->pdf->Output($docPath, 'S'), [
                'visibility' => 'public',
            ]);
        } else {
            $this->pdf->Output($docPath, $saveOption);
        }
    }
}
