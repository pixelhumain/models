<?php

namespace PixelHumain\Models\Services\Interfaces;

use PixelHumain\Models\Services\Interfaces\Mailer\MailerInterface;
use PixelHumain\Models\Services\Interfaces\Mailer\TransportInterface;

interface MailerServiceInterface extends MailerInterface
{
    /**
     * Définit le transport pour l'envoi des e-mails.
     * @param array|TransportInterface $transport Le transport à utiliser.
     */
    public function setTransport($transport): void;
}
