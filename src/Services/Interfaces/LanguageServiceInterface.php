<?php

namespace PixelHumain\Models\Services\Interfaces;

interface LanguageServiceInterface
{
    public function get(): ?string;

    public function set(?string $language): void;

    /**
     * Translates a message to the specified language.
     *
     * @param string $category the message category.
     * @param string $message the message to be translated.
     * @param array|null $params the parameters to be inserted into the translated message.
     * @param string|null $source the source language of the message. If null, the application language will be used.
     * @param string|null $language the target language to translate the message to. If null, the application language will be used.
     * @return string the translated message.
     */
    public function t($category, $message, $params = [], $source = null, $language = null): string;
}
