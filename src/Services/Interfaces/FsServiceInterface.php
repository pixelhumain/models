<?php

namespace PixelHumain\Models\Services\Interfaces;

use IteratorAggregate;

interface FsServiceInterface
{
    public const LIST_SHALLOW = false;

    public const LIST_DEEP = true;

    public function fileExists(string $location): bool;

    // public function directoryExists(string $location): bool;

    // public function has(string $location): bool;

    public function read(string $location): string;

    public function has(string $location): bool;

    public function directoryExists(string $location): bool;

    /**
     * @return resource
     */
    public function readStream(string $location);

    public function listContents(string $location, bool $deep = self::LIST_SHALLOW): IteratorAggregate;

    public function lastModified(string $path): int;

    public function fileSize(string $path): int;

    public function mimeType(string $path): string;

    public function visibility(string $path): string;

    public function write(string $location, string $contents, array $config = []): void;

    /**
      * @param mixed $contents
      *
      */
    public function writeStream(string $location, $contents, array $config = []): void;

    public function setVisibility(string $path, string $visibility): void;

    public function delete(string $location): void;

    public function deleteDirectory(string $location): void;

    public function createDirectory(string $location, array $config = []): void;

    public function move(string $source, string $destination, array $config = []): void;

    public function copy(string $source, string $destination, array $config = []): void;
}
