<?php

namespace PixelHumain\Models\Services\Interfaces;

interface ViewServiceInterface
{
    /**
     * Crée et retourne une instance de la vue.
     *
     * @return mixed
     */
    public function createView();

    /**
     * Définit le module pour la vue.
     *
     * @param mixed $module
     * @return void
     */
    public function setModule($module);

    /**
     * Trouve et retourne le chemin du fichier de vue.
     *
     * @param string $path
     * @return string|null
     */
    public function findViewFile($path);

    /**
     * Rend une vue et retourne le résultat sous forme de chaîne.
     *
     * @param string $view
     * @param array $params
     * @return string
     */
    public function renderView($view, $params);

    /**
     * Réinitialise l'objet View.
     *
     * @return void
     */
    public function resetView();
}
