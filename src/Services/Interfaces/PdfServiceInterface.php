<?php

namespace PixelHumain\Models\Services\Interfaces;

use PixelHumain\Models\Interfaces\PdfInterface;

interface PdfServiceInterface
{
    /**
     * Creates a PDF using the given parameters.
     *
     * @param array $params The parameters for creating the PDF.
     * @return void
     */
    public function createPdf(array $params): void;
}
