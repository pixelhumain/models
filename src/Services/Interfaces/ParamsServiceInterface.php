<?php

namespace PixelHumain\Models\Services\Interfaces;

interface ParamsServiceInterface
{
    /**
     * Get a value from the params by key.
     *
     * @param string $key The key of the value to retrieve from the params.
     * @param mixed $default The default value to return if the key is not found in the params.
     * @return mixed The value from the params if found, otherwise the default value.
     */
    public function get(string $key, $default = null);

    /**
     * Set a value in the params by key.
     *
     * @param string $key The key of the value to set in the params.
     * @param mixed $value The value to set in the params.
     * @return void
     */
    public function set(string $key, $value): void;
}
