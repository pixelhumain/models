<?php

namespace PixelHumain\Models\Services\Interfaces;

use PixelHumain\Models\Interfaces\PdfInterface;

interface RocketChatServiceInterface
{
    /**
     * Retrieves the RocketChat token for the given email and password.
     *
     * @param string $email The user's email.
     * @param string $pwd The user's password.
     * @return array The RocketChat token.
     */
    public function getToken(string $email, string $pwd): array;

    /**
     * Retrieves the token service for the given email and service encryption.
     *
     * @param string $email The email address.
     * @param string $serviceEncrypt The service encryption.
     * @return array The token service.
     */
    public function getTokenService(string $email, string $serviceEncrypt): array;

    /**
     * Creates a new group in RocketChat.
     *
     * @param string $name The name of the group.
     * @param string|null $type The type of the group (optional).
     * @param string $username The username of the group creator.
     * @param mixed|null $inviteOnly Determines if the group is invite-only (optional).
     * @param bool $owner Determines if the group creator is the owner (default: false).
     * @return object The response from the API.
     */
    public function createGroup(string $name, ?string $type = null, string $username, $inviteOnly = null, bool $owner = false): object;

    /**
     * Renames a RocketChat service.
     *
     * @param string $name The current name of the service.
     * @param string $new The new name for the service.
     * @param bool $type The type of the service.
     * @return object The response from the API.
     */
    public function rename(string $name, string $new, bool $type = false): object;

    /**
     * Sets the custom fields for a RocketChat service.
     *
     * @param string $name The name of the custom field.
     * @param array $customFields An array containing the custom fields.
     * @param bool $type The type of the custom field. Default is false.
     * @return object The updated RocketChatService object.
     */
    public function setCustomFields(string $name, array $customFields, bool $type = false): object;

    /**
     * Sends a post request to RocketChat.
     *
     * @param string $to The recipient of the message.
     * @param string $msg The content of the message.
     * @param string $url The URL to be included in the message.
     * @param bool $type The type of the message.
     * @param string $alias The alias to be displayed in the message.
     * @param string $avatar The URL of the avatar to be displayed in the message.
     * @param string $lien The link to be displayed in the message.
     * @return array The response from the API.
     */
    public function post(string $to, string $msg, string $url = "", bool $type = false, string $alias = "oceco", string $avatar = "https://oce.co.tools/avatar.png", string $lien = "Lien oceco");

    /**
     * Lists the channels that a user is a member of.
     *
     * @param string $userEmail The email address of the user.
     * @return mixed An array of channel names.
     */
    public function listUserChannels(string $userEmail);

    /**
     * Invite a user to a group in RocketChat.
     *
     * @param string $name The name of the group.
     * @param string $username The username of the user to be invited.
     * @return object The response from the API.
     */
    public function inviteGroup(string $name, string $username): object;
}
