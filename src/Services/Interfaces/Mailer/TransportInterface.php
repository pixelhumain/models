<?php

namespace PixelHumain\Models\Services\Interfaces\Mailer;

interface TransportInterface
{
    /**
     * Envoie un message.
     * @param MessageInterface $message Le message à envoyer.
     * @return bool Retourne vrai si le message a été envoyé avec succès.
     */
    public function send($message): bool;
}
