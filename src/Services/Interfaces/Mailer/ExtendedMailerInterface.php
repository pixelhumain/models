<?php

namespace PixelHumain\Models\Services\Interfaces\Mailer;

interface ExtendedMailerInterface extends MailerInterface
{
    /**
     * Définit le transport pour l'envoi des e-mails.
     * @param array|TransportInterface $transport Le transport à utiliser.
     */
    public function setTransport($transport): void;
}
