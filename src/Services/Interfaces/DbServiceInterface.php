<?php

namespace PixelHumain\Models\Services\Interfaces;

interface DbServiceInterface
{
    public function find(string $collection, array $where = [], ?array $fields = null): array;

    public function findByIds(string $collection, array $ids, ?array $fields = null): array;

    public function findAndSort(string $collection, array $where = [], array $sortCriteria, int $limit = 0, ?array $fields = null): array;

    public function distinct(string $collection, string $key, array $where = []): array;

    public function count(string $collection, array $where = []): int;

    public function countWFields(string $collection, array $where = [], array $fields = []): int;

    public function findOne(string $collection, array $where, ?array $fields = null): ?array;

    public function findOneById(string $collection, string $id, ?array $fields = null): ?array;

    public function update(string $collection, array $where, array $action): array;

    public function updateWithOptions(string $collection, array $where, array $action, array $options): array;

    /**
     * Insère une nouvelle entrée dans une collection spécifiée.
     *
     * @param string $collection Le nom de la collection dans laquelle insérer les données.
     * @param array $info Les informations à insérer dans la collection.
     * @return mixed
     */
    public function insert(string $collection, array $info);

    /**
     * Removes documents from a collection based on a given condition.
     *
     * @param string $collection The name of the collection to remove documents from.
     * @param array $where The condition to match documents for removal.
     * @return mixed
     */
    public function remove(string $collection, array $where);

    public function batchInsert(string $collection, array $rows): array;

    /**
     * Check if a given value is a valid MongoDB ObjectId.
     *
     * @param mixed $id The value to check.
     * @return bool True if the value is a valid MongoDB ObjectId, false otherwise.
     */
    public function isValidMongoId($id): bool;

    /**
     * Set or get the MongoId.
     *
     * @param string|null $id The MongoId to set or null to get the current MongoId.
     * @return mixed
     */
    public function MongoId(?string $id = null);

    /**
     * Converts a value to a MongoDate object.
     *
     * @param mixed $value The value to convert.
     * @param bool $toBSONType Whether to convert the value to a BSON type.
     * @return mixed The converted MongoDate object.
     */
    public function MongoDate($value = null, bool $toBSONType = true);

    /**
     * Applies a regular expression to the given value in a MongoDB query.
     *
     * @param mixed $value The value to apply the regular expression to.
     * @return mixed The modified value with the regular expression applied.
     */
    public function MongoRegex($value);

    public function getIdFromUpsertResult(array $result): string;

    public function findAndLimitAndIndex(string $collection, array $where = [], int $limit = 0, int $index = 0): array;

    public function findAndSortAndLimitAndIndex(string $collection, array $where = [], array $sortCriteria = [], int $limit = 0, int $index = 0): array;

    public function findAndFieldsAndSortAndLimitAndIndex(string $collection, array $where = [], array $fields = ["name", "collection"], array $sortCriteria = [], int $limit = 0, int $index = 30): array;

    public function aggregate(string $collection, array $aggregate = []): array;
}
