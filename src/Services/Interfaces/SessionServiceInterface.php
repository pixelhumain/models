<?php

namespace PixelHumain\Models\Services\Interfaces;

use PixelHumain\Models\Interfaces\SessionInterface;

interface SessionServiceInterface
{
    /**
     * Get a value from the session by key.
     *
     * @param string $key The key of the value to retrieve from the session.
     * @param mixed $default The default value to return if the key is not found in the session.
     * @return mixed The value from the session if found, otherwise the default value.
     */
    public function get(string $key, $default = null);

    /**
     * Set a value in the session by key.
     *
     * @param string $key The key of the value to set in the session.
     * @param mixed $value The value to set in the session.
     * @return void
     */
    public function set(string $key, $value): void;

    /**
     * Check if a session variable is set.
     *
     * @param string $key The name of the session variable.
     * @return bool Returns true if the session variable is set, false otherwise.
     */
    public function isset(string $key): bool;

    /**
     * Unsets a session variable by key.
     *
     * @param string $key The key of the session variable to unset.
     * @return void
     */
    public function unset(string $key): void;

    /**
     * Retrieves the session value from an array.
     *
     * @param array $data The array containing the session value.
     * @return array The session value extracted from the array.
     */
    public function getFromArray(array $data): array;

    /**
     * Set session value from an array.
     *
     * @param array $data The data array.
     * @return void
     */
    public function setFromArray(array $data): void;

    /**
     * Unsets session values from an array.
     *
     * @param array $data The array containing the session values.
     * @return void
     */
    public function unsetFromArray(array $data): void;

    /**
     * Get the user ID from the session.
     *
     * @return string|null The user ID from the session, or null if not found.
     */
    public function getUserId(): ?string;

    /**
     * Returns the username associated with the current session.
     *
     * @return string|null The username, or null if no session is active.
     */
    public function getUserUsername(): ?string;

    /**
     * Returns the name associated with the current session.
     *
     * @return string|null The name, or null if no session is active.
     */
    public function getUserName(): ?string;

    /**
     * Get the session instance.
     *
     * @return SessionInterface The session instance.
     */
    public function getSession(): SessionInterface;
}
