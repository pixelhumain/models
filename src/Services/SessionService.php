<?php

namespace PixelHumain\Models\Services;

use PixelHumain\Models\Interfaces\SessionInterface;

use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;

class SessionService implements SessionServiceInterface
{
    public SessionInterface $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Get a value from the session by key.
     *
     * @param string $key The key of the value to retrieve from the session.
     * @param mixed $default The default value to return if the key is not found in the session.
     * @return mixed The value from the session if found, otherwise the default value.
     */
    public function get(string $key, $default = null)
    {
        return $this->session->get($key) ?? $default;
    }

    /**
     * Set a value in the session by key.
     *
     * @param string $key The key of the value to set in the session.
     * @param mixed $value The value to set in the session.
     * @return void
     */
    public function set(string $key, $value): void
    {
        $this->session->set($key, $value);
    }

    /**
     * Check if a session variable is set.
     *
     * @param string $key The name of the session variable.
     * @return bool Returns true if the session variable is set, false otherwise.
     */
    public function isset(string $key): bool
    {
        return $this->session->has($key);
    }

    /**
     * Unsets a session variable by key.
     *
     * @param string $key The key of the session variable to unset.
     * @return void
     */
    public function unset(string $key): void
    {
        $this->session->remove($key);
    }

    /**
     * Retrieves the session value from an array.
     *
     * @param array $data The array containing the session value.
     * @return array The session value extracted from the array.
     */
    public function getFromArray(array $data): array
    {
        $result = [];
        foreach ($data as $key => $value) {
            $result[$key] = $this->get((string) $key);
        }
        return $result;
    }

    /**
     * Set session value from an array.
     *
     * @param array $data The data array.
     * @return void
     */
    public function setFromArray(array $data): void
    {
        foreach ($data as $key => $value) {
            $this->set((string) $key, $value);
        }
    }

    /**
     * Unsets session values from an array.
     *
     * @param array $data The array containing the session values.
     * @return void
     */
    public function unsetFromArray(array $data): void
    {
        foreach ($data as $key) {
            $this->unset((string) $key);
        }
    }

    /**
     * Get the user ID from the session.
     *
     * @return string|null The user ID from the session, or null if not found.
     */
    public function getUserId(): ?string
    {
        $userId = $this->session->get("userId");
        if (is_string($userId) && ! empty($userId)) {
            return $userId;
        }
        return null;
    }

    /**
     * Returns the username associated with the current session.
     *
     * @return string|null The username, or null if no session is active.
     */
    public function getUserUsername(): ?string
    {
        $user = $this->session->get("user");
        if ($user && ! empty($user["username"]) && is_string($user["username"])) {
            return $user["username"];
        }
        return null;
    }

    /**
     * Returns the name associated with the current session.
     *
     * @return string|null The name, or null if no session is active.
     */
    public function getUserName(): ?string
    {
        $user = $this->session->get("user");
        if ($user && ! empty($user["name"]) && is_string($user["name"])) {
            return $user["name"];
        }
        return null;
    }

    /**
     * Get the session instance.
     *
     * @return SessionInterface The session instance.
     */
    public function getSession(): SessionInterface
    {
        return $this->session;
    }
}
