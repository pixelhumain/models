<?php

namespace PixelHumain\Models\Services;

use PixelHumain\Models\Interfaces\PhdbDiInterface;

use PixelHumain\Models\Services\Interfaces\DbServiceInterface;

class DbServiceMongo implements DbServiceInterface
{
    protected PhdbDiInterface $phdb;

    public function __construct(PhdbDiInterface $phdb)
    {
        $this->phdb = $phdb;
    }

    public function find(string $collection, array $where = [], ?array $fields = null): array
    {
        return $this->phdb->find($collection, $where, $fields);
    }

    public function findByIds(string $collection, array $ids, ?array $fields = null): array
    {
        return $this->phdb->findByIds($collection, $ids, $fields);
    }

    public function findAndSort(string $collection, array $where = [], array $sortCriteria, int $limit = 0, ?array $fields = null): array
    {
        return $this->phdb->findAndSort($collection, $where, $sortCriteria, $limit, $fields);
    }

    public function distinct(string $collection, string $key, array $where = []): array
    {
        return $this->phdb->distinct($collection, $key, $where);
    }

    public function count(string $collection, array $where = []): int
    {
        return $this->phdb->count($collection, $where);
    }

    public function countWFields(string $collection, array $where = [], array $fields = []): int
    {
        return $this->phdb->countWFields($collection, $where, $fields);
    }

    public function findOne(string $collection, array $where, ?array $fields = null): ?array
    {
        return $this->phdb->findOne($collection, $where, $fields);
    }

    public function findOneById(string $collection, string $id, ?array $fields = null): ?array
    {
        return $this->phdb->findOneById($collection, $id, $fields);
    }

    public function update(string $collection, array $where, array $action): array
    {
        return $this->phdb->update($collection, $where, $action);
    }

    public function updateWithOptions(string $collection, array $where, array $action, array $options): array
    {
        return $this->phdb->updateWithOptions($collection, $where, $action, $options);
    }

    public function insert(string $collection, array $info)
    {
        return $this->phdb->insert($collection, $info);
    }

    public function remove(string $collection, array $where)
    {
        return $this->phdb->remove($collection, $where);
    }

    public function batchInsert(string $collection, array $rows): array
    {
        return $this->phdb->batchInsert($collection, $rows);
    }

    public function isValidMongoId($id): bool
    {
        return $this->phdb->isValidMongoId($id);
    }

    public function MongoId(?string $id = null)
    {
        return $this->phdb->MongoId($id);
    }

    public function MongoDate($value = null, bool $toBSONType = true)
    {
        return $this->phdb->MongoDate($value, $toBSONType);
    }

    public function MongoRegex($value)
    {
        return $this->phdb->MongoRegex($value);
    }

    public function getIdFromUpsertResult(array $result): string
    {
        return $this->phdb->getIdFromUpsertResult($result);
    }

    public function findAndLimitAndIndex(string $collection, array $where = [], int $limit = 0, int $index = 0): array
    {
        return $this->phdb->findAndLimitAndIndex($collection, $where, $limit, $index);
    }

    public function findAndSortAndLimitAndIndex(string $collection, array $where = [], array $sortCriteria = [], int $limit = 0, int $index = 0): array
    {
        return $this->phdb->findAndSortAndLimitAndIndex($collection, $where, $sortCriteria, $limit, $index);
    }

    public function findAndFieldsAndSortAndLimitAndIndex(string $collection, array $where = [], array $fields = ["name", "collection"], array $sortCriteria = [], int $limit = 0, int $index = 30): array
    {
        return $this->phdb->findAndFieldsAndSortAndLimitAndIndex($collection, $where, $fields, $sortCriteria, $limit, $index);
    }

    public function aggregate(string $collection, array $aggregate = []): array
    {
        return $this->phdb->aggregate($collection, $aggregate);
    }
}
