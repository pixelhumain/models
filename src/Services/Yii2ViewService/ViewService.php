<?php

namespace PixelHumain\Models\Services\Yii2ViewService;

use PixelHumain\Models\Services\Interfaces\ViewServiceInterface;

use Yii;
use yii\base\View;

class ViewService implements ViewServiceInterface
{
    /**
     * @var View|null
     */
    private $view;

    public function createView()
    {
        $this->view = Yii::createObject([
            'class' => View::class,
        ]);
        return $this->view;
    }

    public function setModule($module)
    {
        if ($this->view !== null) {
            $this->view->setModule(Yii::$app->getModule($module));
        }
    }

    public function findViewFile($path)
    {
        if ($this->view !== null) {
            return $this->view->findViewFile($path);
        }
        return null;
    }

    public function renderView($view, $params)
    {
        if ($this->view !== null) {
            return $this->view->render($view, $params);
        }
        return '';
    }

    /**
     * Réinitialise l'objet View.
     */
    public function resetView()
    {
        $this->view = null;
    }
}
