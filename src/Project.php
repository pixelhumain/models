<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ProjectInterface;


use PixelHumain\Models\Traits\BaseModel\DataHandlerTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

class Project extends BaseModel implements ProjectInterface, DataValidatorTraitInterface, DocumentTraitInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;
    use DbTrait;

    use DataHandlerTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DataValidatorTrait;
    use DocumentTrait;
    use ElementTrait;

    //From Post/Form name to database field name
    public array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "slug" => [
            "name" => "slug",
            "rules" => ["checkSlug"],
        ],
        "email" => [
            "name" => "email",
            "rules" => ["email"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "public" => [
            "name" => "public",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
            "rules" => ["geoValid"],
        ],
        "geoPosition" => [
            "name" => "geoPosition",
            "rules" => ["geoPositionValid"],
        ],
        "description" => [
            "name" => "description",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "startDate" => [
            "name" => "startDate",
        ],
        "endDate" => [
            "name" => "endDate",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "url" => [
            "name" => "url",
        ],
        "licence" => [
            "name" => "licence",
        ],
        "avancement" => [
            "name" => "properties.avancement",
        ],
        "state" => [
            "name" => "state",
        ],
        "warnings" => [
            "name" => "warnings",
        ],
        "modules" => [
            "name" => "modules",
        ],
        "badges" => [
            "name" => "badges",
        ],
        "category" => [
            "name" => "category",
        ],
        "source" => [
            "name" => "source",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "type" => [
            "name" => "type",
        ],
        "contacts" => [
            "name" => "contacts",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "locality" => [
            "name" => "address",
        ],
        "descriptionHTML" => [
            "name" => "descriptionHTML",
        ],
        "facebook" => [
            "name" => "socialNetwork.facebook",
        ],
        "twitter" => [
            "name" => "socialNetwork.twitter",
        ],
        "gpplus" => [
            "name" => "socialNetwork.googleplus",
        ],
        "github" => [
            "name" => "socialNetwork.github",
        ],
        "gitlab" => [
            "name" => "socialNetwork.gitlab",
        ],
        "diaspora" => [
            "name" => "socialNetwork.diaspora",
        ],
        "mastodon" => [
            "name" => "socialNetwork.mastodon",
        ],
        "signal" => [
            "name" => "socialNetwork.signal",
        ],
        "telegram" => [
            "name" => "socialNetwork.telegram",
        ],
        "onepageEdition" => [
            "name" => "onepageEdition",
        ],
        "instagram" => [
            "name" => "socialNetwork.instagram",
        ],
        "scope" => [
            "name" => "scope",
        ],
        "expected" => [
            "name" => "expected",
        ],
        "costum" => [
            "name" => "costum",
        ],
        "categ" => [
            "name" => "categ",
        ],
        "subcateg" => [
            "name" => "subcateg",
        ],
        "subsubcateg" => [
            "name" => "subsubcateg",
        ],
        "timeZone" => [
            "name" => "timeZone",
        ],
        "actionPrincipal" => [
            "name" => "actionPrincipal",
        ],
        "cibleDDPrincipal" => [
            "name" => "cibleDDPrincipal",
        ],
        "indicateur" => [
            "name" => "indicateur",
        ],
        "innovativeProject" => [
            "name" => "innovativeProject",
        ],
        "financialHelp" => [
            "name" => "financialHelp",
        ],
        "prototype" => [
            "name" => "prototype",
        ],
        "nameStructure" => [
            "name" => "nameStructure",
        ],
        "siret" => [
            "name" => "siret",
        ],
    ];

    public array $avancement = [
        "" => "Not specified",
        "idea" => "idea",
        "concept" => "concept",
        "started" => "started",
        "development" => "development",
        "testing" => "testing",
        "mature" => "mature",
        "abandoned" => "abandoned",
    ];

    /**
    * Configure the model with the given configuration.
    *
    * @param array $config The configuration array.
    * @return void
    */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Get the data binding for the project.
     *
     * @return array The data binding.
     */
    public function getDataBinding(): array
    {
        return self::$dataBinding;
    }

    /**
     * Retrieves a project by its ID.
     *
     * @param string $id The ID of the project.
     * @return array The project details.
     */
    public function getById(string $id): array
    {
        $project = $this->getDataById($id);

        if ($project === null) {
            return $this->getModelElement()->getGhost(ProjectInterface::COLLECTION);
        }

        return $project;
    }

    /**
     * Process project data.
     *
     * @param string $id The project ID.
     * @param array $project The project data.
     * @return array The processed project data.
     */
    protected function processData(string $id, array $project): array
    {
        $project = $this->enrichData($id, $project);
        ;
        $project["files"] = $this->getModelDataValidator()->getListDocumentsWhere([
            "type" => ProjectInterface::COLLECTION,
            "id" => $id,
            "doctype" => "file",
        ], "file");

        $project["typeSig"] = "projects";
        $project["type"] = ProjectInterface::COLLECTION;

        return $project;
    }

    /**
     * Retrieves a project by an array of IDs.
     *
     * @param array $arrayId The array of IDs to search for.
     * @param array $fields The optional fields to retrieve for each project.
     * @param bool $simply Whether to return a simplified version of the project.
     * @return array The array of projects matching the given IDs.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array
    {
        return $this->getDataByArrayId($arrayId, $fields, $simply);
    }

    /**
     * Format the enrich data.
     *
     * @param array $data The data to be enriched.
     * @return array The formatted enriched data.
     */
    protected function formatEnrichData(array $data): array
    {
        $data = $this->formatDates($data);
        return $data;
    }

    /**
     * Format the dates in the given project array.
     *
     * @param array $project The project array to format dates for.
     * @return array The formatted project array.
     */
    private function formatDates(array $project): array
    {
        date_default_timezone_set('UTC'); // Définir le fuseau horaire sur UTC

        $yester2day = mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"));
        $yesterday = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));

        if (isset($project["startDate"])) {
            $project["startDate"] = $this->formatMongoDate($project["startDate"], $yester2day, 'Y-m-d H:i:s');
        }

        if (isset($project["startDate"]) && isset($project["endDate"])) {
            $project["endDate"] = $this->formatMongoDate($project["endDate"], $yesterday, 'Y-m-d H:i:s');
        } else {
            $project["endDate"] = isset($project["endDate"]) ?
                $this->formatMongoDate($project["endDate"], $yesterday, 'Y-m-d H:i:s') :
                date('Y-m-d H:i:s', $yesterday);
        }

        return $project;
    }

    /**
     * Retrieves a simple project by its ID.
     *
     * @param string $id The ID of the project.
     * @param array|null $project Optional project data to be passed.
     * @return array The simple project data.
     */
    public function getSimpleProjectById(string $id, ?array $project = null): array
    {
        return $this->getSimpleDataById($id, $project);
    }

    /**
     * Returns the fields of the project.
     *
     * @return array The fields of the project.
     */
    protected function getDataFields(): array
    {
        return [
            "id" => 1,
            "collection" => 1,
            "name" => 1,
            "shortDescription" => 1,
            "description" => 1,
            "address" => 1,
            "geo" => 1,
            "tags" => 1,
            "links" => 1,
            "profilImageUrl" => 1,
            "profilThumbImageUrl" => 1,
            "profilMarkerImageUrl" => 1,
            "profilMediumImageUrl" => 1,
            "addresses" => 1,
        ];
    }

    /**
     * Returns an array of link types.
     *
     * @return array The link types.
     */
    protected function getlinkTypes(): array
    {
        return ["contributors", "followers"];
    }

    /**
     * Retrieves simple data for a project.
     *
     * @param int $id The project ID.
     * @param array $data The project data.
     * @return array The simple data for the project.
     */
    protected function getDataSimple($id, array $data): array
    {
        return [
            "id" => $id,
            "name" => $data["name"] ?? '',
            "address" => $data["address"] ?? [
                "addressLocality" => $this->language->t("common", "Unknown Locality"),
            ],
            "addresses" => $data["addresses"] ?? [],
            "geo" => $data["geo"] ?? [],
            "tags" => $data["tags"] ?? [],
            "shortDescription" => $data["shortDescription"] ?? '',
            "description" => $data["description"] ?? '',
        ];
    }

    //TODO SBAR => should be private ?
    /**
     * Retrieves records from the database based on the specified parameters.
     *
     * @param array $params The parameters used to filter the records.
     * @return array The retrieved records.
     */
    public function getWhere(array $params): array
    {
        return $this->db->findAndSort(ProjectInterface::COLLECTION, $params, ["created"]);
    }

    /**
     * Format the date for rendering.
     *
     * @param array $project The project data.
     * @return array The formatted project data.
     */
    public function formatDateRender(array $project): array
    {
        if (! empty($project["startDate"]) || ! empty($project["endDate"])) {
            $project = $this->formatDates($project);
        }
        return $project;
    }
}
