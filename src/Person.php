<?php

namespace PixelHumain\Models;

use DateTime;
use Exception;
use InvalidArgumentException;
use MongoDB\BSON\ObjectId;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\ArrayHelperInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\CronInterface;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\ActionRoomTrait;
use PixelHumain\Models\Traits\ActivityStreamTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DataHandlerTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CO2StatTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\ImportTrait;
use PixelHumain\Models\Traits\Interfaces\ActionRoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CO2StatTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ImportTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LogTraitInterface;
use PixelHumain\Models\Traits\Interfaces\MailTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\RoleTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SurveyTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TagsTraitInterface;
use PixelHumain\Models\Traits\LogTrait;
use PixelHumain\Models\Traits\MailTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\RoleTrait;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\SlugTrait;
use PixelHumain\Models\Traits\SurveyTrait;
use PixelHumain\Models\Traits\TagsTrait;

class Person extends BaseModel implements PersonInterface, RoleTraitInterface, TagsTraitInterface, SurveyTraitInterface, SlugTraitInterface, ActionRoomTraitInterface, ActivityStreamTraitInterface, AuthorisationTraitInterface, CO2StatTraitInterface, CostumTraitInterface, DataValidatorTraitInterface, DocumentTraitInterface, ElementTraitInterface, ImportTraitInterface, LogTraitInterface, MailTraitInterface, PreferenceTraitInterface, SIGTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;

    use DataHandlerTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use RoleTrait;
    use TagsTrait;
    use SurveyTrait;
    use SlugTrait;
    use ActionRoomTrait;
    use ActivityStreamTrait;
    use AuthorisationTrait;
    use CO2StatTrait;
    use CostumTrait;
    use DataValidatorTrait;
    use DocumentTrait;
    use ElementTrait;
    use ImportTrait;
    use LogTrait;
    use MailTrait;
    use PreferenceTrait;
    use SIGTrait;

    protected ArrayHelperInterface $arrayHelper;

    protected CookieHelperInterface $cookieHelper;

    /**
     * @var string|null $moduleId The module ID.
     */
    protected ?string $moduleId = null;

    public array $jsonLD = [];

    //From Post/Form name to database field name with rules
    public array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "slug" => [
            "name" => "slug",
            "rules" => ["checkSlug"],
        ],
        "username" => [
            "name" => "username",
            "rules" => ["required", "checkUsername"],
        ],
        "birthDate" => [
            "name" => "birthDate",
            "rules" => ["required"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "email" => [
            "name" => "email",
            "rules" => ["email"],
        ],
        "pwd" => [
            "name" => "pwd",
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "codeInsee" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
            "rules" => ["geoValid"],
        ],
        "geoPosition" => [
            "name" => "geoPosition",
            "rules" => ["geoPositionValid"],
        ],
        "telephone" => [
            "name" => "telephone",
        ],
        "mobile" => [
            "name" => "telephone.mobile",
        ],
        "fixe" => [
            "name" => "telephone.fixe",
        ],
        "fax" => [
            "name" => "telephone.fax",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "description" => [
            "name" => "description",
        ],
        "facebook" => [
            "name" => "socialNetwork.facebook",
        ],
        "twitter" => [
            "name" => "socialNetwork.twitter",
        ],
        "gpplus" => [
            "name" => "socialNetwork.googleplus",
        ],
        "github" => [
            "name" => "socialNetwork.github",
        ],
        "gitlab" => [
            "name" => "socialNetwork.gitlab",
        ],
        "telegram" => [
            "name" => "socialNetwork.telegram",
        ],
        "instagram" => [
            "name" => "socialNetwork.instagram",
        ],
        "diaspora" => [
            "name" => "socialNetwork.diaspora",
        ],
        "mastodon" => [
            "name" => "socialNetwork.mastodon",
        ],
        "signal" => [
            "name" => "socialNetwork.signal",
        ],
        "bgClass" => [
            "name" => "preferences.bgClass",
        ],
        "bgUrl" => [
            "name" => "preferences.bgUrl",
        ],
        "roles" => [
            "name" => "roles",
        ],
        "two_steps_register" => [
            "name" => "two_steps_register",
        ],
        "source" => [
            "name" => "source",
        ],
        "warnings" => [
            "name" => "warnings",
        ],
        "isOpenData" => [
            "name" => "isOpenData",
        ],
        "modules" => [
            "name" => "modules",
        ],
        "badges" => [
            "name" => "badges",
        ],
        "multitags" => [
            "name" => "multitags",
        ],
        "multiscopes" => [
            "name" => "multiscopes",
        ],
        "url" => [
            "name" => "url",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "lastLoginDate" => [
            "name" => "lastLoginDate",
        ],
        "seePreferences" => [
            "name" => "seePreferences",
        ],
        "locality" => [
            "name" => "address",
        ],
        "language" => [
            "name" => "language",
        ],
        "curiculum" => [
            "name" => "curiculum",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "descriptionHTML" => [
            "name" => "descriptionHTML",
        ],
        "onepageEdition" => [
            "name" => "onepageEdition",
        ],
        "mailToResend" => [
            "name" => "mailToResend",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
        // Valider le type de chaque propriété
        if (! $this->arrayHelper instanceof ArrayHelperInterface) {
            throw new InvalidArgumentException("arrayHelper must be an instance of ArrayHelperInterface");
        }
        if (! $this->cookieHelper instanceof CookieHelperInterface) {
            throw new InvalidArgumentException("cookieHelper must be an instance of CookieHelperInterface");
        }

        if (empty($this->moduleId)) {
            $this->moduleId = "communecter";
        }
    }

    /**
     * Sets the person ID.
     *
     * @param string $userId The person ID.
     * @return void
     */
    protected function setUserId(string $userId): void
    {
        $this->session->set("userId", $userId);
    }

    /**
     * Checks if the person is logged in and their account is valid.
     *
     * @return bool Returns true if the person is logged in and their account is valid, false otherwise.
     */
    public function logguedAndValid(): bool
    {
        return $this->logguedAndAuthorized();
    }

    /**
     * Check if the person is logged in and authorized.
     *
     * @return bool Returns true if the person is logged in and authorized, false otherwise.
     */
    public function logguedAndAuthorized(): bool
    {
        $userId = $this->session->getUserId();

        if (! empty($userId)) {
            $user = $this->db->findOneById(PersonInterface::COLLECTION, $userId);

            if ($user) {
                $validityCheck = $this->getModelRole()->canUserLogin($user, boolval($this->session->get('isRegisterProcess') ?? false));
                return boolval($validityCheck["result"] ?? false);
            }
        }
        return false;
    }

    /**
     * Saves the user session data.
     *
     * @param array {
     *     slug?: string, // Identifiant unique pour l'utilisateur, facultatif.
     *     cp?: string, // Code postal, facultatif.
     *     address?: mixed, // Adresse, facultatif.
     *     roles?: array<mixed>, // Rôles de l'utilisateur, facultatif.
     *     preferences?: array<mixed>, // Préférences de l'utilisateur, facultatif.
     *     lastLoginDate?: int, // Timestamp de la dernière connexion, facultatif.
     *     _id: string|ObjectId, // Identifiant MongoDB de l'utilisateur.
     *     serviceName?: ?string, // Nom du service, facultatif.
     *     services?: array<string, mixed>,
     *     name?: string, // Nom de l'utilisateur, facultatif.
     *     email?: string, // Email de l'utilisateur, facultatif.
     *     username?: string, // Nom d'utilisateur de l'utilisateur, facultatif.
     *     profilThumbImageUrl?: string, // URL de l'image de profil de l'utilisateur, facultatif.
     * } $account Tableau associatif contenant les données utilisateur avec une structure détaillée.
     * @param bool $isRegisterProcess Indicates if the process is a registration process.
     * @param string|null $pwd The password.
     * @return void
     */
    public function saveUserSessionData(array $account, bool $isRegisterProcess = false, ?string $pwd = null): void
    {
        $this->initializeSessionData($account, $pwd);
        $this->handleUserService($account);
        $this->populateUserData($account);
        $this->finalizeSessionData($account, $isRegisterProcess);
    }

    /**
     * Initializes the session data for a person.
     *
     * @param array $account The account information.
     * @param string|null $pwd The password.
     * @return void
     */
    private function initializeSessionData(array $account, ?string $pwd): void
    {
        $this->setUserId((string) $account["_id"]);
        $this->session->set("userEmail", (string) $account["email"]);
        if (! empty($pwd)) {
            $this->session->set("pwd", $pwd);
        }
    }

    /**
     * Handles the user service data.
     *
     * Cette fonction gère les données de service utilisateur spécifiques en mettant à jour et
     * enregistre les informations de service dans la session, basées sur les données fournies dans `$account`.
     *
     * @param array {
     *     slug?: string, // Identifiant unique pour l'utilisateur, facultatif.
     *     cp?: string, // Code postal, facultatif.
     *     address?: mixed, // Adresse, facultatif.
     *     roles?: array<mixed>, // Rôles de l'utilisateur, facultatif.
     *     preferences?: array<mixed>, // Préférences de l'utilisateur, facultatif.
     *     lastLoginDate?: int, // Timestamp de la dernière connexion, facultatif.
     *     _id: string|ObjectId, // Identifiant MongoDB de l'utilisateur.
     *     serviceName?: ?string, // Nom du service, facultatif.
     *     services?: array<string, mixed>,
     *     name?: string, // Nom de l'utilisateur, facultatif.
     *     email?: string, // Email de l'utilisateur, facultatif.
     *     username?: string, // Nom d'utilisateur de l'utilisateur, facultatif.
     *     profilThumbImageUrl?: string, // URL de l'image de profil de l'utilisateur, facultatif.
     * } $account Tableau associatif contenant les données utilisateur avec une structure détaillée.
     * @return void
     */
    private function handleUserService(array $account): void
    {
        if (empty($account["serviceName"]) || empty($account["services"])) {
            return;
        }

        if (! isset($account['services']) || ! is_array($account['services'])) {
            return;
        }

        $serviceName = (string) $account["serviceName"];

        if (empty($account["services"][$serviceName]) || ! is_array($account["services"][$serviceName])) {
            return;
        }

        $serviceData = $account["services"][$serviceName];

        $serviceData["name"] = $account["name"] ?? null;
        $serviceData["email"] = $account["email"] ?? null;
        $serviceData["username"] = $account["username"] ?? null;

        if (isset($account["profilThumbImageUrl"])) {
            $serviceData["profilThumbImageUrl"] = $account["profilThumbImageUrl"];
        }

        $account["services"][$serviceName] = $serviceData;
        $this->session->set("service", $serviceData);
        $this->session->set("serviceName", $serviceName);
    }

    /**
     * Peuple les données utilisateur dans la session à partir du tableau `$account`.
     *
     * Cette méthode extrait et organise les informations utilisateur à partir du tableau `$account`
     * et les enregistre dans la session. La structure du tableau `$account` doit être conforme aux attentes de la méthode.
     *
     * @param array {
     *     slug?: string, // Identifiant unique pour l'utilisateur, facultatif.
     *     cp?: string, // Code postal, facultatif.
     *     address?: mixed, // Adresse, facultatif.
     *     roles?: array<mixed>, // Rôles de l'utilisateur, facultatif.
     *     preferences?: array<mixed>, // Préférences de l'utilisateur, facultatif.
     *     lastLoginDate?: int, // Timestamp de la dernière connexion, facultatif.
     *     id: string|ObjectId, // Identifiant MongoDB de l'utilisateur.
     *     serviceName?: ?string, // Nom du service, facultatif.
     *     services?: array<string, mixed>,
     *     name?: string, // Nom de l'utilisateur, facultatif.
     *     email?: string, // Email de l'utilisateur, facultatif.
     *     username?: string, // Nom d'utilisateur de l'utilisateur, facultatif.
     *     profilThumbImageUrl?: string, // URL de l'image de profil de l'utilisateur, facultatif.
     * } $account Tableau associatif contenant les données utilisateur avec une structure détaillée.
     * @return void
     */
    private function populateUserData(array $account): void
    {
        $name = $account["name"] ?? "Anonymous";
        $user = [
            "name" => $name,
        ];

        if (isset($account["username"])) {
            $user["username"] = $account["username"];
        }
        if (isset($account["slug"])) {
            $user["slug"] = $account["slug"];
        }
        if (isset($account["cp"])) {
            $user["postalCode"] = $account["cp"];
        }
        if (isset($account["address"]) && is_array($account["address"])) {
            if (isset($account["address"]["postalCode"])) {
                $user["postalCode"] = $account["address"]["postalCode"];
            }
            if (isset($account["address"]["codeInsee"])) {
                $user["codeInsee"] = $account["address"]["codeInsee"];
            }
            if (isset($account["address"]["addressCountry"])) {
                $user["addressCountry"] = $account["address"]["addressCountry"];
            }
        }
        if (isset($account["roles"])) {
            $user["roles"] = $account["roles"];
        }
        if (isset($account["preferences"])) {
            $user["preferences"] = $account["preferences"];
        }

        //Last login date
        $user["lastLoginDate"] = $account["lastLoginDate"] ?? time();

        //Image profil
        // todo : getById fait d'autre chose que de la récupération d'image peut être à changer pour ne faire que ce qui est lié à l'image
        $simpleUser = $this->getById((string) $account["_id"]);
        $user["profilImageUrl"] = $simpleUser["profilImageUrl"] ?? null;
        $user["profilMediumImageUrl"] = $simpleUser["profilMediumImageUrl"] ?? null;
        $user["profilThumbImageUrl"] = $simpleUser["profilThumbImageUrl"] ?? null;
        $user["profilMarkerImageUrl"] = $simpleUser["profilMarkerImageUrl"] ?? null;
        $this->session->set("user", $user);
    }

    /**
     * Finalizes the session data for a person.
     *
     * @param array $account The account data.
     * @param bool $isRegisterProcess Indicates if it is a registration process.
     * @return void
     */
    private function finalizeSessionData(array $account, bool $isRegisterProcess): void
    {
        $this->session->set("isRegisterProcess", $isRegisterProcess);
        $roles = ! empty($account["roles"]) && is_array($account["roles"]) ? $account["roles"] : [];
        $this->session->set("userIsAdmin", $this->getModelRole()->isUserSuperAdmin($roles));
        $this->session->set("userIsAdminPublic", $this->getModelRole()->isSourceAdmin($roles));
        $this->session->set("logguedIntoApp", $this->moduleId ?? "communecter");
    }

    /**
     * Clears the user session data.
     *
     * @return void
     */
    public function clearUserSessionData(): void
    {
        $this->session->unsetFromArray(["user", "userId", "userEmail", "pwd", "isRegisterProcess", "userIsAdmin", "userIsAdminPublic", "logguedIntoApp", "requestedUrl", "loginToken", "rocketUserId", "rocketLoginError", "service", "serviceName", "userEmail", "pwd", "costum"]);

        $this->cookieHelper->setCookie("communexionActivated", false);
        $this->cookieHelper->setCookie("drowsp", false);
        $this->cookieHelper->setCookie("lyame", false);
    }

    /**
     * get a Person By Id
     * @param string $id is the mongoId of the person
     * @param boolean $clearAttribute by default true. Will clear the confidential attributes
     * @return array|null person document as in db
     */
    public function getById(string $id, bool $clearAttribute = true): ?array
    {
        $person = $this->getDataById($id);

        if ($person === null) {
            return $this->handleMissingPerson();
        }

        if ($clearAttribute) {
            $person = $this->clearAttributesByConfidentiality($person);
        }
        return $person;
    }

    /**
     * Returns the fields of the project.
     *
     * @return array The fields of the project.
     */
    protected function getDataFields(): array
    {
        return [
            "id" => 1,
            "name" => 1,
            "username" => 1,
            "email" => 1,
            "shortDescription" => 1,
            "description" => 1,
            "address" => 1,
            "geo" => 1,
            "roles" => 1,
            "tags" => 1,
            "links" => 1,
            "pending" => 1,
            "profilImageUrl" => 1,
            "profilThumbImageUrl" => 1,
            "profilMarkerImageUrl" => 1,
            "profilMediumImageUrl" => 1,
            "numberOfInvit" => 1,
            "updated" => 1,
            "addresses" => 1,
            "slug" => 1,
        ];
    }

    /**
     * Returns an array of link types.
     *
     * @return array The link types.
     */
    protected function getlinkTypes(): array
    {
        return ["followers", "follows", "members", "memberOf", "contributors", "friends", "projects", "poi"];
    }

    /**
     * Handles the case when a person is missing.
     *
     * @return array The result of handling the missing person.
     */
    private function handleMissingPerson(): array
    {
        // Votre logique pour gérer une personne manquante
        return [
            "name" => "Unknown (deleted)",
            "slug" => "unknown",
            "type" => PersonInterface::COLLECTION,
            "typeSig" => PersonInterface::COLLECTION,
        ];
    }

    /**
     * Format the enrich data.
     *
     * @param array $data The data to be enriched.
     * @return array The formatted enriched data.
     */
    protected function formatEnrichData(array $data): array
    {
        $data = $this->formatDateBirthday($data);
        // todo est ce que typeSig doit rester en sachant que sur getById il a été supprimer
        $data["typeSig"] = "people";
        $data = $this->setDefaultAddress($data);
        return $data;
    }

    /**
     * Format the birthday date in the given data array.
     *
     * @param array $data The data array containing the birthday date.
     * @return array The updated data array with the formatted birthday date.
     */
    protected function formatDateBirthday(array $data): array
    {
        if (! empty($data["birthDate"])) {
            date_default_timezone_set('UTC');
            $data["birthDate"] = $this->formatMongoDate($data["birthDate"], null, 'Y-m-d H:i:s');
        }
        return $data;
    }

    // /**
    //  * Process the count links data for a person.
    //  *
    //  * @param array $person The person data.
    //  * @return array The processed count links data.
    //  */
    // private function processCountLinksData(array $person): array
    // {
    //     if (! empty($person["links"]) && is_array($person["links"])) {
    //         // Calculer la taille de 'links' de manière plus robuste
    //         $person["linkSize"] = count($person["links"], COUNT_RECURSIVE);

    //         $person["counts"] = [];

    //         $linksKeys = ["followers", "follows", "members", "memberOf", "contributors", "friends", "projects", "poi"];

    //         // Initialiser les compteurs à 0
    //         foreach ($linksKeys as $key) {
    //             $person["counts"][$key] = 0;
    //         }

    //         // Mettre à jour les compteurs pour les clés existantes
    //         foreach ($person["links"] as $key => $value) {
    //             if (in_array($key, $linksKeys, true) && is_countable($value) && ! empty($person["counts"][$key])) {
    //                 $person["counts"][$key] = count($value);
    //             }
    //         }
    //     }
    //     return $person;
    // }

    /**
     * Sets the default address for a person.
     *
     * @param array $person The person data.
     * @return array The updated person data with the default address set.
     */
    private function setDefaultAddress(array $person): array
    {
        if (! isset($person["address"])) {
            $person["address"] = [
                "codeInsee" => "",
                "postalCode" => "",
                "addressLocality" => "",
                "streetAddress" => "",
                "addressCountry" => "",
            ];
        }
        return $person;
    }

    /**
     * Retrieves a person by their array ID.
     *
     * @param array $arrayId The array ID of the person.
     * @param array $fields The fields to retrieve. Default is an empty array.
     * @param bool $clearAttribute Whether to clear the attributes of the person. Default is true.
     * @param bool $simpleUser Whether the person is a simple user. Default is false.
     * @return array The array of persons.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $clearAttribute = true, bool $simpleUser = false): array
    {
        $datas = $this->db->find(PersonInterface::COLLECTION, [
            "_id" => [
                '$in' => $arrayId,
            ],
        ], $fields);
        $res = [];

        foreach ($datas as $id => $person) {
            if (empty($person)) {
                continue;
            }

            if (! empty($person) && is_array($person)) {
                $person = $this->enrichData((string) $id, $person);
            }

            if ($simpleUser && ! empty($person) && is_array($person)) {
                $person = $this->formatSimpleData((string) $id, $person, $this->getDataSimple((string) $id, $person), $this->getlinkTypes());
            } elseif ($clearAttribute && ! empty($person) && is_array($person)) {
                $person = $this->clearAttributesByConfidentiality($person);
            }

            $res[$id] = $person;
        }

        return $res;
    }

    /**
     * Retrieves the minimal user information by their ID.
     *
     * @param string $id The ID of the user.
     * @param array|null $person The person.
     * @return array The minimal user information.
     */
    public function getMinimalUserById(string $id, ?array $person = null): array
    {
        if (! $person) {
            $person = $this->db->findOneById(
                PersonInterface::COLLECTION,
                $id,
                [
                    "id" => 1,
                    "name" => 1,
                    "username" => 1,
                    "email" => 1,
                    "roles" => 1,
                    "tags" => 1,
                    "profilImageUrl" => 1,
                    "profilThumbImageUrl" => 1,
                    "profilMarkerImageUrl" => 1,
                ]
            );
        }

        if (empty($person)) {
            return [];
        }

        $simplePerson = $this->processMinimalUsernData($person);

        return $simplePerson;
    }

    /**
     * Process minimal user data.
     *
     * @param array $person The person data.
     * @return array The processed data.
     */
    private function processMinimalUsernData(array $person): array
    {
        $simplePerson = [
            "_id" => $person["_id"],
            "id" => (string) $person["_id"],
            "typeSig" => "people",
        ];

        // Ajout ou modification des données dans simplePerson en fonction de person
        $fieldsToCopy = ["name", "username", "email", "tags"];
        foreach ($fieldsToCopy as $field) {
            if (isset($person[$field])) {
                $simplePerson[$field] = $person[$field];
            }
        }

        $simplePerson["address"] = empty($person["address"]) ? [
            "addressLocality" => "Unknown",
        ] : $person["address"];

        //images
        $simplePerson = $this->mergeWithImages((string) $person["_id"], $person, $simplePerson);

        $simplePerson = $this->clearAttributesByConfidentiality($simplePerson);

        return $simplePerson ?? [];
    }

    /**
     * Retrieves a simple user by their ID.
     *
     * @param string $id The ID of the user.
     * @param array|null $person The person object.
     * @return array The simple user.
     */
    public function getSimpleUserById(string $id, ?array $person = null): array
    {
        return $this->getSimpleDataById($id, $person);
    }

    // /**
    //  * Builds a simple person structure based on the given array.
    //  *
    //  * @param array $person The person array.
    //  * @return array The simple person structure.
    //  */
    // private function processSimplePersonData(array $person): array
    // {
    //     $simplePerson = [
    //         "_id" => $person["_id"],
    //         "id" => (string) $person["_id"],
    //         "typeSig" => "people",
    //         // Autres champs initiaux...
    //     ];

    //     // Ajout ou modification des données dans simplePerson en fonction de person
    //     $fieldsToCopy = ["name", "username", "email", "tags", "links", "shortDescription", "description", "pending", "updated", "slug"];
    //     foreach ($fieldsToCopy as $field) {
    //         if (isset($person[$field])) {
    //             $simplePerson[$field] = $person[$field];
    //         }
    //     }

    //     // Gestion spéciale pour 'tobeactivated'
    //     if (isset($person["roles"]["tobeactivated"])) {
    //         $simplePerson["tobeactivated"] = $person["roles"]["tobeactivated"];
    //     }

    //     // Compte des liens
    //     if (! empty($person["links"]) && ! empty($person["links"]["follows"]) && ! empty($person["links"]["followers"]) && is_countable($person["links"]["follows"]) && is_countable($person["links"]["followers"])) {
    //         $simplePerson["counts"] = [
    //             "follows" => count($person["links"]["follows"] ?? []),
    //             "followers" => count($person["links"]["followers"] ?? []),
    //         ];
    //     }

    //     // Adresse et géolocalisation
    //     $simplePerson["address"] = $person["address"] ?? [];
    //     $simplePerson["geo"] = $person["geo"] ?? [];

    //     // Gestion de betaTest
    //     if ($this->params->get('betaTest')) {
    //         $simplePerson["numberOfInvit"] = $person["numberOfInvit"] ?? 0;
    //     }

    //     //images
    //     $simplePerson = array_merge($simplePerson, $this->getModelDocument()->retrieveAllImagesUrl((string) $person["_id"], self::COLLECTION, $person));

    //     $simplePerson = $this->clearAttributesByConfidentiality($simplePerson);

    //     return $simplePerson ?? [];
    // }

    protected function getDataSimple($id, array $data): array
    {
        $simplePerson = [
            "_id" => $data["_id"],
            "id" => (string) $data["_id"],
            "typeSig" => "people",
            // Autres champs initiaux...
        ];

        // Ajout ou modification des données dans simplePerson en fonction de person
        $fieldsToCopy = ["name", "username", "email", "tags", "links", "shortDescription", "description", "pending", "updated", "slug"];
        foreach ($fieldsToCopy as $field) {
            if (isset($data[$field])) {
                $simplePerson[$field] = $data[$field];
            }
        }

        // Gestion spéciale pour 'tobeactivated'
        if (isset($data["roles"]["tobeactivated"])) {
            $simplePerson["tobeactivated"] = $data["roles"]["tobeactivated"];
        }

        // Compte des liens
        if (! empty($data["links"]) && ! empty($data["links"]["follows"]) && ! empty($data["links"]["followers"]) && is_countable($data["links"]["follows"]) && is_countable($data["links"]["followers"])) {
            $simplePerson["counts"] = [
                "follows" => count($data["links"]["follows"] ?? []),
                "followers" => count($data["links"]["followers"] ?? []),
            ];
        }

        // Adresse et géolocalisation
        $simplePerson["address"] = $data["address"] ?? [];
        $simplePerson["geo"] = $data["geo"] ?? [];

        // Gestion de betaTest
        if ($this->params->get('betaTest')) {
            $simplePerson["numberOfInvit"] = $data["numberOfInvit"] ?? 0;
        }

        //images
        $simplePerson = $this->clearAttributesByConfidentiality($simplePerson);

        return $simplePerson ?? [];
    }

    //TODO SBAR => should be private ? pas de limite ?
    /**
     * Get a list of persons by condition with all property
     *
     * @param array $params The parameters to use to retrieve the persons.
     * @return array The list of persons with all property.
     */
    public function getWhere(array $params): array
    {
        $persons = $this->db->findAndSort(PersonInterface::COLLECTION, $params, ["created"]);

        return array_map(fn (array $person) => $this->clearAttributesByConfidentiality($person), $persons);
    }

    /**
     * Retrieves the organizations associated with a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array An array of organizations associated with the person.
     */
    public function getOrganizationsById(string $id): array
    {
        $person = $this->getById($id);
        $organizationIds = [];

        if (isset($person["links"]) && ! empty($person["links"]["memberOf"]) && is_array($person["links"]["memberOf"])) {
            $organizationIds = array_keys($person["links"]["memberOf"]);
        }

        // Continuez avec la requête batch si vous avez des IDs d'organisation
        if (empty($organizationIds)) {
            return [];
        }

        return $this->getOrganizationsBatch($organizationIds);
    }

    /**
     * Retrieves a batch of organizations based on the provided organization IDs.
     *
     * @param array $organizationIds An array of organization IDs.
     * @return array An array of organizations.
     */
    private function getOrganizationsBatch(array $organizationIds): array
    {
        // Convertir les strings en MongoIds si nécessaire
        /**
         * @psalm-suppress MissingClosureReturnType
         */
        $mongoIds = array_map(fn ($id) => $this->db->MongoId((string) $id), $organizationIds);

        // Requête pour récupérer toutes les organisations
        $organizations = $this->db->find(OrganizationInterface::COLLECTION, [
            "_id" => [
                '$in' => $mongoIds,
            ],
        ]);

        // Filtrer les organisations désactivées
        // Retourne `true` si `disabled` n'est pas défini ou si `disabled` est `false`
        $filteredOrganizations = array_filter($organizations, fn ($org) => ! isset($org["disabled"]) || ! $org["disabled"]);
        $filteredOrganizationsIndexed = array_values($filteredOrganizations);
        return $filteredOrganizationsIndexed;
    }

    /**
     * Initializes the link array.
     *
     * @return array The initialized link array.
     */
    private function initializeLinkArray(): array
    {
        return [
            PersonInterface::COLLECTION => [],
            OrganizationInterface::COLLECTION => [],
            ProjectInterface::COLLECTION => [],
            EventInterface::COLLECTION => [],
            "follows" => [],
        ];
    }

    /**
     * Retrieves the person links associated with a given person ID.
     *
     * @param string $id The ID of the person.
     * @return array The person links associated with the given person ID.
     */
    public function getPersonLinksByPersonId(string $id): array
    {
        /**
         * @var array<string, array<mixed>> $res
         */
        $res = $this->initializeLinkArray();
        /**
         * @var array<string, array<mixed>> $resRetour
         */
        $resRetour = $this->initializeLinkArray();
        /**
         * @var array<string, array<mixed>> $resFormat
         */
        $resFormat = $this->initializeLinkArray();

        $person = $this->getModelElement()->getElementSimpleById($id, PersonInterface::COLLECTION, null, ["links"]);

        if (empty($person)) {
            throw new CTKException("The person id is unkown : contact your admin");
        }

        $myContacts = [];
        if (! empty($person["links"]) && is_array($person["links"])) {
            $myContacts = $person["links"];
        }

        $validKeys = ["memberOf", "projects", "events", "friends", "follows"];

        // Filtrer les contacts par clés valides
        $filteredContacts = array_filter($myContacts, fn ($key) => in_array($key, $validKeys), ARRAY_FILTER_USE_KEY);

        // Transformer les données
        foreach ($filteredContacts as $connectKey => $links) {
            foreach ($links as $key => $value) {
                if (! empty($value["type"]) && is_string($value["type"]) && isset($res[$value["type"]])) {
                    $res[$value["type"]][] = $this->db->MongoId((string) $key);
                }
            }
        }

        $infos = ["name", "collection", "slug", "username", "shortDescription", "address", "type", "profilThumbImageUrl", "profilImageUrl", "profilMediumImageUrl", "preferences", "hasRC", "endDate", "startDate", "email"];
        // WARNING
        $costum = $this->getModelCostum()->getCostum();
        if (! empty($costum)) {
            $infos[] = "source";
        }

        if (! empty($res["citoyens"])) {
            $resRetour['citoyens'] = $this->getModelElement()->getElementSimpleByIds($res['citoyens'], 'citoyens', null, $infos);
        }
        if (! empty($res["organizations"])) {
            $resRetour['organizations'] = $this->getModelElement()->getElementSimpleByIds($res['organizations'], 'organizations', null, $infos);
        }
        if (! empty($res["projects"])) {
            $resRetour['projects'] = $this->getModelElement()->getElementSimpleByIds($res['projects'], 'projects', null, $infos);
        }
        if (! empty($res["events"])) {
            $resRetour['events'] = $this->getModelElement()->getElementSimpleByIds($res['events'], 'events', null, $infos);
        }

        foreach ($filteredContacts as $connectKey => $links) {
            foreach ($links as $key => $value) {
                if (! is_array($value)) {
                    continue;
                }
                if (! isset($value["type"]) || ! isset($res[(string) $value["type"]])) {
                    continue;
                }

                $type = $value["type"];
                $contactComplet = $resRetour[(string) $type][(string) $key] ?? null;

                if (empty($contactComplet) || (isset($contactComplet["disabled"]) && $contactComplet["disabled"])) {
                    continue;
                }

                /**
                 * @var array $contactComplet
                 */
                // Traitement spécifique pour "follows" et "friends"
                if ($connectKey == "follows") {
                    $contactComplet["isFollowed"] = true;
                }
                if (in_array($connectKey, ["follows", "friends"])) {
                    $contactComplet["type"] = $value["type"];
                }

                // Mise à jour de $contactComplet avec les informations de $value
                foreach ($value as $label => $v) {
                    if ($label != "type") {
                        $contactComplet[$label] = $v;
                    }
                }

                // Gestion des cas spéciaux pour les événements
                if ($type == EventInterface::COLLECTION) {
                    if (! empty($contactComplet["startDate"]) && is_object($contactComplet["startDate"]) && isset($contactComplet["startDate"]->sec)) {
                        $contactComplet["startDateTime"] = date(DateTime::ISO8601, (int) $contactComplet["startDate"]->sec);
                    }
                    if (! empty($contactComplet["endDate"]) && is_object($contactComplet["endDate"]) && isset($contactComplet["endDate"]->sec)) {
                        $contactComplet["endDateTime"] = date(DateTime::ISO8601, (int) $contactComplet["endDate"]->sec);
                    }
                }

                // Mise à jour de $resFormat avec le contact complet
                if ($connectKey == "follows") {
                    $resFormat["follows"][(string) $key] = $contactComplet;
                } elseif ($connectKey == "friends") {
                    $resFormat["citoyens"][(string) $key] = $contactComplet;
                } else {
                    $resFormat[(string) $type][(string) $key] = $contactComplet;
                }
            }
        }

        if (isset($resFormat["citoyens"])) {
            $resFormat["citoyens"] = $this->sortContact($resFormat["citoyens"], [
                "name" => SORT_ASC,
            ]);
        }
        if (isset($resFormat["organizations"])) {
            $resFormat["organizations"] = $this->sortContact($resFormat["organizations"], [
                "name" => SORT_ASC,
            ]);
        }
        if (isset($resFormat["projects"])) {
            $resFormat["projects"] = $this->sortContact($resFormat["projects"], [
                "name" => SORT_ASC,
            ]);
        }
        if (isset($resFormat["events"])) {
            $resFormat["events"] = $this->sortContact($resFormat["events"], [
                "name" => SORT_ASC,
            ]);
        }
        if (isset($resFormat["follows"])) {
            $resFormat["follows"] = $this->sortContact($resFormat["follows"], [
                "name" => SORT_ASC,
            ]);
        }

        if ($id != $this->session->getUserId()) {
            $resFormat["citoyens"][$id] = $person;
        }
        return $resFormat;
    }

    /**
     * Sorts an array of contacts based on specified columns.
     *
     * @param array $array The array of contacts to be sorted.
     * @param array $cols The columns to sort the contacts by.
     * @return array The sorted array of contacts.
     */
    private function sortContact(array $array, array $cols): array
    {
        uasort($array, function ($a, $b) use ($cols) {
            foreach ($cols as $col => $order) {
                if (is_array($a) && is_array($b) && isset($a[$col]) && isset($b[$col]) && strtolower((string) $a[$col]) < strtolower((string) $b[$col])) {
                    return $order === SORT_ASC ? -1 : 1;
                } elseif (is_array($a) && is_array($b) && isset($a[$col]) && isset($b[$col]) && strtolower((string) $a[$col]) > strtolower((string) $b[$col])) {
                    return $order === SORT_ASC ? 1 : -1;
                }
            }
            return 0;
        });

        return $array;
    }

    // NOT USED - delete ?
    // public function newPersonFromPost($person)
    // {
    //     $newPerson = array();

    //     //Location
    //     if (isset($person['streetAddress'])) {
    //         $newPerson["address"]["streetAddress"] = rtrim($person['streetAddress']);
    //     }
    //     if (isset($person['postalCode'])) {
    //         $newPerson["address"]["postalCode"] = $person['postalCode'];
    //     }
    //     if (isset($person['addressLocality'])) {
    //         $newPerson["address"]["addressLocality"] = $person['addressLocality'];
    //     }
    //     if (isset($person['addressCountry'])) {
    //         $newPerson["address"]["addressCountry"] = $person['addressCountry'];
    //     }
    //     if (isset($person['codeInsee'])) {
    //         $newPerson["address"]["codeInsee"] = $person['codeInsee'];
    //     }

    //     if (isset($person['two_steps_register'])) {
    //         $newPerson["two_steps_register"] = $person['two_steps_register'];
    //     }

    //     if (isset($person['description'])) {
    //         $newPerson["description"] = rtrim($person['description']);
    //     }
    //     if (isset($person['role'])) {
    //         $newPerson["role"] = $person['role'];
    //     }

    //     //error_log("latitude : ".$person['geoPosLatitude']);
    //     if (!empty($person['geoPosLatitude']) && !empty($person["geoPosLongitude"])) {
    //         $newPerson["geo"] = array(
    //             "@type" => "GeoCoordinates",
    //             "latitude" => $person['geoPosLatitude'],
    //             "longitude" => $person['geoPosLongitude'],
    //         );

    //         $newPerson["geoPosition"] = array(
    //             "type" => "Point",
    //             "coordinates" =>
    //                 array(
    //                     floatval($person['geoPosLongitude']),
    //                     floatval($person['geoPosLatitude']),
    //                 ),
    //         );
    //     }

    //     return $newPerson;
    // }

    /**
     * Creates a new person and sends an invitation.
     *
     * @param array $param The parameters for creating the person.
     * - invitedBy: The ID of the person who invited the new person.
     * - name: The name of the new person.
     * - email: The email of the new person.
     * @param string|null $msg The message to include in the invitation. DEPRECATED.
     * @param string|null $gmail The Gmail address to send the invitation to. DEPRECATED.
     * @return array The created person and invitation details.
     */
    public function createAndInvite(array $param, ?string $msg = null, ?string $gmail = null): array
    {
        try {
            // todo : les arguments $msg et $gmail ne sont jamais utilisé dans la fonction peut être présent pour garder la compatibilité ou elle est utilisé

            if (empty($param) || empty($param["invitedBy"]) || empty($param["name"]) || empty($param["email"])) {
                return [
                    "result" => false,
                    "msg" => "Problem inviting the new person: name, email, and invitedBy are required",
                ];
            }

            if (! is_string($param["invitedBy"]) || ! is_string($param["name"]) || ! is_string($param["email"])) {
                return [
                    "result" => false,
                    "msg" => "Problem inviting the new person: name, email, and invitedBy must be strings",
                ];
            }

            $person = $this->db->findOneById(PersonInterface::COLLECTION, $param["invitedBy"], ["_id", "name"]);
            if (! empty($person)) {
                $res = $this->insert($param, PersonInterface::REGISTER_MODE_MINIMAL);
            } else {
                $res = [
                    "result" => false,
                    "msg" => "the person who invited cannot be found",
                ];
            }
        } catch (CTKException $e) {
            $res = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        return $res;
    }

    /**
     * Retrieves and checks the data of a person.
     *
     * @param array $person The person data.
     * @param string $mode The mode of operation.
     * @param bool $uniqueEmail Whether to check for unique email or not. Default is true.
     * @return array The person data.
     */
    private function getAndcheckPersonData(array $person, string $mode, bool $uniqueEmail = true): array
    {
        $dataPersonMinimal = ["name", "email"];

        $servicesCopy = ! empty($person["services"]) && is_array($person["services"]) ? $person["services"] : false;

        //xss clean up
        $person = $this->getModelDataValidator()->clearUserInput($person);

        $newPerson = [];
        if ($mode == PersonInterface::REGISTER_MODE_MINIMAL) {
            //generate unique temporary userName for Meteor app when inviting
            $newPerson["username"] = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 32);
            //Add pending boolean
            $newPerson["pending"] = true;
        } elseif ($mode == PersonInterface::REGISTER_MODE_NORMAL) {
            array_push($dataPersonMinimal, "username", "pwd");
        } elseif ($mode == PersonInterface::REGISTER_MODE_TWO_STEPS) {
            array_push($dataPersonMinimal, "username", "pwd");
            $newPerson[PersonInterface::REGISTER_MODE_TWO_STEPS] = true;
        } elseif ($mode == PersonInterface::REGISTER_MODE_SERVICE) {
            if (is_array($servicesCopy)) {
                $newPerson["services"] = $servicesCopy;
            }
            array_push($dataPersonMinimal, "username", "services");
            $newPerson[PersonInterface::REGISTER_MODE_SERVICE] = true;
        }

        //Check the minimal data
        foreach ($dataPersonMinimal as $data) {
            if (empty($person[$data])) {
                throw new CTKException($this->language->t("person", "Problem inserting the new person : ") . $data . $this->language->t("person", " is missing"));
            }
        }

        $newPerson["name"] = trim((string) $person["name"]);

        //Check email
        $checkEmail = $this->getModelDataValidator()->email((string) $person["email"]);
        if ($checkEmail != "") {
            throw new CTKException($this->language->t("common", $checkEmail));
        }

        //Check if the email of the person is already in the database
        if ($uniqueEmail) {
            $account = $this->getPersonByEmail((string) $person["email"]);
            if ($account) {
                throw new CTKException($this->language->t("person", "Problem inserting the new person : a person with this email already exists in the plateform"));
            }
        }
        $newPerson["email"] = trim((string) $person["email"]);

        if (! empty($person["invitedBy"])) {
            $newPerson["invitedBy"] = (string) $person["invitedBy"];
        }

        if (! empty($person["preferences"])) {
            $newPerson["preferences"] = $person["preferences"];
        }

        if ($mode == PersonInterface::REGISTER_MODE_NORMAL || $mode == PersonInterface::REGISTER_MODE_TWO_STEPS || $mode == PersonInterface::REGISTER_MODE_SERVICE) {
            //user name
            $newPerson["username"] = trim((string) $person["username"]);
            if (strlen($newPerson["username"]) < 4 || strlen($newPerson["username"]) > 32) {
                throw new CTKException($this->language->t("person", "The username length should be between 4 and 32 characters"));
            }
            if (! $this->isUniqueUsername($newPerson["username"])) {
                if ($mode == PersonInterface::REGISTER_MODE_SERVICE) {
                    $newPerson["username"] = $this->checkAndCreateUniqueUsername($newPerson["username"]);
                } else {
                    throw new CTKException($this->language->t("person", "Problem inserting the new person : a person with this username already exists in the plateform"));
                }
            }
        }

        if ($mode == PersonInterface::REGISTER_MODE_NORMAL || $mode == PersonInterface::REGISTER_MODE_TWO_STEPS) {
            //Password is mandatory : it will be encoded later
            if (empty($person["pwd"])) {
                throw new CTKException($this->language->t("person", "The password could not be empty on this register mode !"));
            }
        }
        if (! empty($person["address"]) && is_array($person["address"])) {
            if (! isset($person["address"]["addressCountry"])) {
                $person["address"]["addressCountry"] = "FR";
            }
            $resAddress = $this->getModelImport()->getAndCheckAddressForEntity($person["address"]);
            if ($resAddress["result"] == true) {
                if (isset($resAddress["address"]) && ! empty($resAddress["address"])) {
                    $newPerson["address"] = $resAddress["address"];
                }
                if (isset($resAddress["geo"]) && ! empty($resAddress["geo"])) {
                    $newPerson["geo"] = $resAddress["geo"];
                }
                if (isset($resAddress["geoPosition"]) && ! empty($resAddress["geoPosition"])) {
                    $newPerson["geoPosition"] = $resAddress["geoPosition"];
                }
            }
        }
        return $newPerson;
    }

    /**
     * Insert a person into the database.
     *
     * @param array $person The person data to be inserted.
     * @param string $mode The registration mode. Defaults to self::REGISTER_MODE_NORMAL.
     * REGISTER_MODE_MINIMAL : a person can be created using only name and email.
     * REGISTER_MODE_NORMAL : name, email, username, password, postalCode, city
     * REGISTER_MODE_TWO_STEPS : name, username, email, password
     * REGISTER_MODE_SERVICE : name, username, email, services
     * @param string|null $inviteCode The invite code. Defaults to null.
     * @param array|null $forced The forced data. Defaults to null.
     * @return array The inserted person data.
     */
    public function insert(array $person, string $mode = PersonInterface::REGISTER_MODE_NORMAL, ?string $inviteCode = null, ?array $forced = null): array
    {
        //Keep the password
        $pwd = $person["pwd"] ?? null;

        //Check Person data + business rules
        $person = $this->getAndcheckPersonData($person, $mode);

        $person["roles"] = $this->getModelRole()->getDefaultRoles();

        if ($mode == PersonInterface::REGISTER_MODE_SERVICE) {
            unset($person["roles"]["tobeactivated"]);
        }

        // j'ai commenté la fonction addPersonDataForBetaTest car elle ne renvoie rien
        // $this->addPersonDataForBetaTest($person, $mode, $inviteCode);

        if (! empty($forced)) {
            $person = array_merge($person, $forced);
        }
        $person["created"] = $this->db->MongoDate(time());
        $person["collection"] = PersonInterface::COLLECTION;
        $person["preferences"] = $this->getModelPreference()->initPreferences(PersonInterface::COLLECTION);
        $person["seePreferences"] = true;
        $person["slug"] = $this->getModelSlug()->checkAndCreateSlug((string) $person["username"]);

        $costum = $this->getModelCostum()->getCostum();
        if (! empty($costum) && is_array($costum) && ! empty($costum["slug"]) && is_string($costum["slug"])) {
            $person["source"] = [
                "origin" => "costum",
                "key" => $costum["slug"],
                "keys" => [$costum["slug"]],
            ];
        }

        $person = $this->db->insert(PersonInterface::COLLECTION, $person);
        if (is_array($person) && ! empty($person["_id"])) {
            $newpersonId = (string) $person["_id"];
            if (! empty($pwd)) {
                //Encode the password
                $encodedpwd = $this->hashPassword($newpersonId, (string) $pwd);
                $this->updatePersonField($newpersonId, "pwd", $encodedpwd, $newpersonId);
            }

            $this->getModelSlug()->save(PersonInterface::COLLECTION, (string) $person["_id"], (string) $person["slug"]);
        } else {
            throw new CTKException("Problem inserting the new person");
        }

        $res = [
            "result" => true,
            "msg" => "You are now communnected",
            "id" => $newpersonId,
            "person" => $person,
        ];

        if (! empty($person["invitedBy"])) {
            $res['invitedBy'] = $person["invitedBy"];
        }
        return $res;
    }

    // NOT USED - delete ?
    // /**
    //  * In betaTest mode, manage roles, invitation code, and invitation numbers on person data
    //  * @param array $person A person ready to insert
    //  * @param string $mode Register mode
    //  * @param string $inviteCode Invitation code
    //  * @return void
    //  */
    // // Todo la fonction ne sert à rien car elle ne retourne rien
    // public function addPersonDataForBetaTest(array $person, $mode, $inviteCode)
    // {
    //     //if we are in mode minimal it's an invitation. The invited user is then betaTester by default
    //     if (@$this->params['betaTest'] && $mode == self::REGISTER_MODE_MINIMAL) {
    //         $person["roles"]['betaTester'] = true;
    //     }

    //     //if valid invite code , user is automatically beta tester
    //     //inviteCodes are server configs
    //     if (@$this->params['betaTest'] && $inviteCode && in_array($inviteCode, $this->params['validInviteCodes'])) {
    //         $person["roles"]['betaTester'] = true;
    //         $person["inviteCode"] = $inviteCode;
    //     }
    //     if (@$this->params['betaTest'] || @$person["roles"]["betaTester"] == true) {
    //         $person["numberOfInvit"] = empty($this->params['numberOfInvitByPerson']) ? 0 : $this->params['numberOfInvitByPerson'];
    //     }
    //     return;
    // }

    /**
     * Retrieves the public data of a person.
     *
     * @param string $id The ID of the person.
     * @return array|null The public data of the person, or null if the person is not found.
     */
    public function getPublicData(string $id): ?array
    {
        //TODO SBAR = filter data to retrieve only publi data
        $person = $this->getById($id, true);
        return $person;
    }

    // TODO : NOT USED - delete ?
    // /**
    //  * answers to show or not to show a field by it's name
    //  * @param string $id : is the mongoId of the action room
    //  * @param array $person : is the mongoId of the action room
    //  * @param boolean $isLinked : is the mongoId of the action room
    //  * @return array|null : the action room
    //  */
    // public function showField($fieldName, $person, $isLinked)
    // {
    //     $res = null;

    //     $attConfName = $fieldName;
    //     if ($fieldName == "address.streetAddress") {
    //         $attConfName = "locality";
    //     }
    //     if ($fieldName == "telephone") {
    //         $attConfName = "phone";
    //     }

    //     if (
    //         $this->session->getUserId() == (string) $person["_id"]
    //         || (isset($person["preferences"]) && isset($person["preferences"]["publicFields"]) && in_array($attConfName, $person["preferences"]["publicFields"]))
    //         || ($isLinked && isset($person["preferences"]) && isset($person["preferences"]["privateFields"]) && in_array($attConfName, $person["preferences"]["privateFields"]))
    //     ) {
    //         $res = $this->arrayHelper->getValueByDotPath($person, $fieldName);
    //     }

    //     return $res;
    // }

    // NOT USED - delete ?
    // /**
    //  * get all events details of a Person By a person Id
    //  * @param string $id : is the mongoId (String) of the person
    //  * @return array The events of the person.
    //  */
    // public function getEventsByPersonId($id): array
    // {
    //     $person = $this->getById($id);
    //     $events = array();

    //     //Load events
    //     if (isset($person["links"]) && !empty($person["links"]["events"])) {
    //         foreach ($person["links"]["events"] as $id => $e) {
    //             $event = $this->db->findOne(PHType::TYPE_EVENTS, array("_id" => $this->db->MongoId($id)));
    //             if (!empty($event)) {
    //                 array_push($events, $event);
    //             } else {
    //                 // throw new CTKException("Données inconsistentes pour le citoyen : ".$this->session->getUserId());
    //             }
    //         }
    //     }
    //     return $events;
    // }


    // NOT USED - delete ?
    // /**
    //  * Retrieves the person map for a given ID.
    //  *
    //  * @param string $id The ID of the person.
    //  * @return array The person map.
    //  */
    // public function getPersonMap($id)
    // {
    //     $person = $this->getById($id);
    //     $organizations = $this->getOrganizationsById($id);
    //     $events = $this->getEventsByPersonId($id);
    //     $personMap = array(
    //         "person" => $person,
    //         "organizations" => $organizations,
    //         "events" => $events,
    //     );
    //     return $personMap;
    // }

    /**
     * Update a specific field of a person.
     *
     * @param string $personId The ID of the person.
     * @param string $personFieldName The name of the field to update.
     * @param mixed $personFieldValue The new value for the field.
     * @param string $userId The ID of the user performing the update.
     * @return array Result status and message.
     */
    public function updatePersonField(string $personId, string $personFieldName, $personFieldValue, string $userId): array
    {
        if (! $this->getModelAuthorisation()->canEditItem($userId, PersonInterface::COLLECTION, $personId)) {
            throw new CTKException("Can not update the person : you are not authorized to update that person !");
        }

        if (is_string($personFieldValue)) {
            $personFieldValue = trim($personFieldValue);
        }

        $dataFieldName = $this->getCollectionFieldNameAndValidate($personFieldName, $personFieldValue);

        //Tags
        if ($dataFieldName == "tags") {
            if (is_array($personFieldValue) || is_null($personFieldValue)) {
                $personFieldValue = $this->getModelTags()->filterAndSaveNewTags($personFieldValue);
            }
        }

        if ($dataFieldName == "email" && (empty($personFieldValue) || ! is_string($personFieldValue) || strlen($personFieldValue) == 0)) {
            throw new CTKException("L'email " . $this->language->t("person", "is missing"));
        }

        if (($personFieldName == "mobile" || $personFieldName == "fixe" || $personFieldName == "fax")) {
            if (is_null($personFieldValue)) {
                $personFieldValue = [];
            } elseif (is_string($personFieldValue)) {
                $personFieldValue = explode(",", $personFieldValue);
            }
        }

        $user = ! empty($this->session->get("user")) && is_array($this->session->get("user")) ? $this->session->get("user") : [];

        /** @var array $user */

        //address
        if ($dataFieldName == "address") {
            $thisUser = $this->getById($personId);

            if (! empty($personFieldValue["postalCode"]) && ! empty($personFieldValue["codeInsee"])) {
                $insee = (string) $personFieldValue["codeInsee"];
                $postalCode = (string) $personFieldValue["postalCode"];

                $user["codeInsee"] = $insee;
                $user["postalCode"] = $postalCode;

                $address = $this->getModelSIG()->getAdressSchemaLikeByCodeInsee($insee, $postalCode);

                if (! empty($personFieldValue["streetAddress"])) {
                    $address["streetAddress"] = $personFieldValue["streetAddress"];
                }

                if (! empty($personFieldValue["addressCountry"])) {
                    $address["addressCountry"] = $personFieldValue["addressCountry"];
                    $user["addressCountry"] = $personFieldValue["addressCountry"];
                }

                // mise à jour de la session avec codeInsee codeInsee addressCountry
                $this->session->set("user", $user);

                // tableau pour la mise à jour du champ address
                $set = [
                    "address" => $address,
                ];

                // mise à jour de la géolocalisation de Person
                if (empty($thisUser["geo"])) {
                    $geo = $this->getModelSIG()->getGeoPositionByInseeCode($insee, $postalCode);
                    if (! empty($geo["latitude"]) && ! empty($geo["longitude"]) && is_string($geo["latitude"]) && is_string($geo["longitude"])) {
                        $this->getModelSIG()->updateEntityGeoposition(PersonInterface::COLLECTION, $personId, $geo["latitude"], $geo["longitude"]);
                    }
                }

                // rajoute address dans user pour le retour
                $user["address"] = $address;
            } else {
                throw new CTKException("Error updating the Person : address is not well formated !");
            }
        } elseif ($dataFieldName == "birthDate") {
            if (! empty($personFieldValue) || ! is_string($personFieldValue)) {
                throw new CTKException("Error updating the Person : birthDate is not well formated !");
            }
            date_default_timezone_set('UTC');
            $dt = DateTime::createFromFormat('Y-m-d H:i', $personFieldValue);
            if (empty($dt)) {
                $dt = DateTime::createFromFormat('Y-m-d', $personFieldValue);
            }

            if (! empty($dt)) {
                $newMongoDate = $this->db->MongoDate($dt->getTimestamp());
                $set = [
                    (string) $dataFieldName => $newMongoDate,
                ];
            } else {
                throw new CTKException("Error updating the Person : birthDate is not well formated !");
            }
        } else {
            $set = [
                (string) $dataFieldName => $personFieldValue,
            ];

            if ($personFieldName == "bgClass") {
                //save to session for all page reuse
                $user["bg"] = $personFieldValue;
                $this->session->set("user", $user);
            } elseif ($personFieldName == "bgUrl") {
                //save to session for all page reuse
                $user["bgUrl"] = $personFieldValue;
                $this->session->set("user", $user);
            }
        }

        //update the person
        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($personId),
            ],
            [
                '$set' => $set,
            ]
        );

        return [
            "result" => true,
            "user" => ! empty($user) ? $user : null,
            "personFieldName" => $personFieldName,
            "msg" => $this->language->t("person", "The person has been updated"),
        ];
    }

    /**
     * Retrieves the collection field name and validates the person field value.
     *
     * @param string $personFieldName The name of the person field.
     * @param mixed $personFieldValue The value of the person field.
     * @return mixed The collection field name and the validated person field value.
     */
    private function getCollectionFieldNameAndValidate(string $personFieldName, $personFieldValue)
    {
        return $this->getModelDataValidator()->getCollectionFieldNameAndValidate($this->dataBinding, $personFieldName, $personFieldValue);
    }

    /**
     * Logs in a person.
     *
     * @param string|null $emailOrUsername The email or username of the person.
     * @param string|null $pwd The password of the person.
     * @param bool $isRegisterProcess (optional) Whether the login is part of a registration process.
     * @param bool $firstConnection (optional) Whether it is the person's first connection.
     * @return array The result of the login method.
     */
    public function login(?string $emailOrUsername, ?string $pwd, bool $isRegisterProcess = false, bool $firstConnection = false): array
    {
        if (empty($emailOrUsername) || empty($pwd)) {
            return [
                "result" => false,
                "msg" => "Cette requête ne peut aboutir. Merci de bien vouloir réessayer en complétant les champs nécessaires",
            ];
        }

        $this->clearUserSessionData();

        $account = $this->db->findOne(
            PersonInterface::COLLECTION,
            [
                '$or' => [[
                    "email" => $this->db->MongoRegex('/^' . $emailOrUsername . '$/i'),
                ], [
                    "username" => $emailOrUsername,
                ]],
            ]
        );

        //return an error when email does not exist
        if ($account == null) {
            return [
                "result" => false,
                "msg" => "emailNotFound",
            ];
        }

        //Roles validation
        $res = $this->getModelRole()->canUserLogin($account, $isRegisterProcess);
        // firstConnection bypass la validation d'email notValidatedEmail pour permettre à l'utilisateur de se connecter
        // mais renvois quand même le message d'erreur en retour, c'est utilisé dans RegisterAction sans utilisé le retour de l'erreur
        if ($res["result"] || $firstConnection) {
            //Check the password
            if ($this->checkPassword($pwd, $account)) {
                $this->saveUserSessionData($account, $isRegisterProcess, $pwd);
                //Update login history
                $this->updateLoginHistory((string) $account["_id"]);
                // firstConnection bypass : msg peut être notValidatedEmail
                if ($res["msg"] == "notValidatedEmail") {
                    return $res;
                } else {
                    $adress = ! empty($account["address"]) && is_array($account["address"]) ? $account["address"] : null;
                    $this->updateCookieCommunexion((string) $account["_id"], $adress);
                    unset($account["pwd"]);
                    $res = [
                        "result" => true,
                        "id" => (string) $account["_id"],
                        "isCommunected" => isset($account["cp"]),
                        "account" => $account,
                        "msg" => "Vous êtes maintenant identifié : bienvenue sur communecter.",
                    ];
                    //save login action for statistic
                    // todo: est ce que c'est encore utile ?
                    $this->getModelCO2Stat()->incNbLoad("co2-login");
                }
            } else {
                $res = [
                    "result" => false,
                    "msg" => "emailAndPassNotMatch",
                ];
            }
        }

        return $res;
    }

    /**
     * Logs in a user and create authentication token.
     *
     * @param string|null $emailOrUsername The email or username of the user.
     * @param string|null $pwd The password of the user.
     * @param string $tokenName The name of the authentication token.
     * @param bool $isRegisterProcess Indicates if the login is part of a registration process.
     * @return array The result of the login method.
     */
    public function loginAuthToken(?string $emailOrUsername, ?string $pwd, string $tokenName = "comobi", bool $isRegisterProcess = false): array
    {
        if (empty($emailOrUsername) || empty($pwd)) {
            return [
                "result" => false,
                "msg" => "Cette requête ne peut aboutir. Merci de bien vouloir réessayer en complétant les champs nécessaires",
            ];
        }

        $this->clearUserSessionData();

        $account = $this->db->findOne(
            PersonInterface::COLLECTION,
            [
                '$or' => [[
                    "email" => $this->db->MongoRegex('/^' . preg_quote(trim($emailOrUsername)) . '$/i'),
                ], [
                    "username" => $emailOrUsername,
                ]],
            ]
        );

        //return an error when email does not exist
        if ($account == null) {
            return [
                "result" => false,
                "msg" => "emailNotFound",
            ];
        }

        //Roles validation
        $res = $this->getModelRole()->canUserLogin($account, $isRegisterProcess);
        if ($res["result"] === true) {
            //Check the password
            if ($this->checkPassword($pwd, $account)) {
                $token = $this->addToken((string) $account["_id"], $tokenName);
                unset($account["pwd"]);
                // todo: limiter les données de $account retournées
                $res = [
                    "result" => true,
                    "id" => (string) $account["_id"],
                    "isCommunected" => isset($account["cp"]),
                    "account" => $account,
                    "msg" => "Vous êtes maintenant identifié : bienvenue sur communecter.",
                ];
                if (! empty($token)) {
                    $res["token"] = $token;
                }
            } else {
                $res = [
                    "result" => false,
                    "msg" => "emailAndPassNotMatch",
                ];
            }
        }

        return $res;
    }

    /**
     * Updates the login history for a person.
     *
     * @param string $accountId The account ID of the person.
     * @return array The updated login history.
     */
    public function updateLoginHistory(string $accountId): array
    {
        return $this->updatePersonField($accountId, "lastLoginDate", time(), $accountId);
    }

    /**
     * Adds a token to the person's account.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token.
     * @param bool $regenerate Whether to regenerate the token if it already exists.
     * @return string|null The token if it was added, false else.
     */
    public function addToken(string $accountId, string $tokenName, bool $regenerate = false): ?string
    {
        $accountExists = $this->checkAccountTokenExists($accountId, $tokenName);
        $token = $accountExists ? ($regenerate ? $this->regenerateToken($accountId, $tokenName) : null) : $this->createNewToken($accountId, $tokenName);
        return $token;
    }

    /**
     * Check if an account token exists for a given account ID and token name.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token.
     * @return bool Returns true if the account token exists, false otherwise.
     */
    private function checkAccountTokenExists(string $accountId, string $tokenName): bool
    {
        $loginTokensExists = $this->db->findOne(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($accountId),
                'loginTokens.name' => $tokenName,
                'loginTokens.type' => 'personalAccessToken',
            ]
        );
        return $loginTokensExists ? true : false;
    }

    /**
     * Regenerates a token for the specified account ID and token name.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token.
     * @return string The regenerated token.
     */
    private function regenerateToken(string $accountId, string $tokenName): string
    {
        $token = $this->generateRandomToken();
        $hashedToken = $this->generateHashToken($token, $accountId);
        $loginTokens = [
            'loginTokens.$.createdAt' => time(),
            'loginTokens.$.hashedToken' => $hashedToken,
            'loginTokens.$.lastTokenPart' => substr($token, -6),
        ];
        $this->db->update(PersonInterface::COLLECTION, [
            "_id" => $this->db->MongoId($accountId),
            'loginTokens.name' => $tokenName,
        ], [
            '$set' => $loginTokens,
        ]);
        return $token;
    }

    /**
     * Creates a new token for the specified account ID and token name.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token.
     * @return string The newly created token.
     */
    private function createNewToken(string $accountId, string $tokenName): string
    {
        $token = $this->generateRandomToken();
        $hashedToken = $this->generateHashToken($token, $accountId);
        $loginTokens = [
            'createdAt' => time(),
            'hashedToken' => $hashedToken,
            'type' => 'personalAccessToken',
            'name' => $tokenName,
            'lastTokenPart' => substr($token, -6),
        ];
        $set = [
            'loginTokens' => $loginTokens,
        ];
        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($accountId),
            ],
            [
                '$addToSet' => $set,
            ]
        );

        return $token;
    }

    /**
     * Generates a random token.
     *
     * @return string The generated random token.
     */
    private function generateRandomToken(): string
    {
        $token = openssl_random_pseudo_bytes(16);
        $token = bin2hex($token);
        return $token;
    }

    /**
     * Generates a hash token for a given token and account ID.
     *
     * @param string $token The token to generate a hash for.
     * @param string $accountId The account ID associated with the token.
     * @return string The generated hash token.
     */
    private function generateHashToken(string $token, string $accountId): string
    {
        $hashedToken = hash_hmac('sha256', $token, $accountId, true);
        $hashedToken = base64_encode($hashedToken);
        return $hashedToken;
    }

    /**
     * Removes a token from the person's account.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token to remove.
     * @return bool Returns true if the token was successfully removed, false otherwise.
     */
    public function removeToken(string $accountId, string $tokenName): bool
    {
        $pull = [
            'loginTokens' => [
                'name' => $tokenName,
            ],
        ];
        $this->db->update(PersonInterface::COLLECTION, [
            "_id" => $this->db->MongoId($accountId),
        ], [
            '$pull' => $pull,
        ]);
        return true;
    }

    /**
     * Service Oauth.
     *
     * This method is used to perform OAuth service authentication.
     *
     * @param array|null $serviceDecrypt The decrypted service data.
     * @param string|null $tokenName The name of the token.
     * @return array The result of the OAuth service authentication.
     */
    public function serviceOauth(?array $serviceDecrypt, ?string $tokenName = null): array
    {
        if (empty($serviceDecrypt)) {
            return [
                "result" => false,
                "msg" => "no serviceDecrypt",
            ];
        }

        if (empty($serviceDecrypt["serviceName"]) || ! is_string($serviceDecrypt["serviceName"])) {
            return [
                "result" => false,
                "msg" => "no serviceDecrypt serviceName",
            ];
        }

        // Préparer les données du service
        $serviceData = $this->prepareServiceData($serviceDecrypt);
        if ($serviceData === null) {
            return [
                "result" => false,
                "msg" => "Invalid service data",
            ];
        }

        // Trouver le compte associé au service
        $account = $this->findAccountByServiceData($serviceData, $serviceDecrypt["serviceName"]);

        // Vérifier si $account est un tableau avec 'result' et 'msg'
        if (is_array($account) && isset($account["result"]) && $account["result"] === false) {
            return $account; // Retourne directement le message d'erreur
        }

        // Créer ou mettre à jour le compte
        if ($account === null) {
            return $this->createAccountFromServiceData($serviceData, $serviceDecrypt["serviceName"], $tokenName);
        } else {
            return $this->updateAccountFromServiceData($account, $serviceData, $serviceDecrypt["serviceName"], $tokenName);
        }
    }

    /**
     * Prepares the service data.
     *
     * @param array $serviceDecrypt The decrypted service data.
     * @return array|null The prepared service data.
     */
    private function prepareServiceData(array $serviceDecrypt): ?array
    {
        if (empty($serviceDecrypt['serviceData']) || ! is_array($serviceDecrypt['serviceData'])) {
            return null;
        }

        $serviceData = $serviceDecrypt['serviceData'];

        if (empty($serviceData['id']) && ! empty($serviceData['sub'])) {
            $serviceData['id'] = (string) $serviceData['sub'];
        }

        if (! empty($serviceData['preferred_username'])) {
            $serviceData['username'] = (string) $serviceData['preferred_username'];
        } elseif (! empty($serviceData['username'])) {
            // $serviceData['username'] est déjà défini, pas besoin de le modifier.
        } else {
            // Utiliser la partie avant l'arobase de $serviceData['email'] si $serviceData['email'] est défini.
            $serviceData['username'] = ! empty($serviceData['email']) ? strstr((string) $serviceData['email'], '@', true) : null;
        }

        // Use 'username' as 'name' if 'name' is not set
        $serviceData['name'] ??= $serviceData['username'];

        return $serviceData;
    }

    /**
     * Finds an account by service data and service name.
     *
     * @param array $serviceData The service data.
     * @param string $serviceName The service name.
     * @return array|null The account data if found, null otherwise.
     */
    private function findAccountByServiceData(array $serviceData, string $serviceName): ?array
    {
        $serviceId = "services." . $serviceName . ".id";
        $account = $this->db->findOne(PersonInterface::COLLECTION, [
            $serviceId => (string) $serviceData['id'],
        ]);

        if ($account === null && isset($serviceData['email'])) {
            // Check if account exists with the provided email
            $account = $this->db->findOne(PersonInterface::COLLECTION, [
                "email" => (string) $serviceData['email'],
            ]);

            if ($account !== null && isset($account["roles"]["tobeactivated"])) {
                // Account with unvalidated email
                return [
                    "result" => false,
                    "msg" => "emailNotValid",
                ];
            }
        }
        return $account; // Returns the account data or null if not found
    }

    /**
     * Creates an account from service data.
     *
     * @param array $serviceData The service data.
     * @param string $serviceName The name of the service.
     * @param string|null $tokenName The name of the token (optional).
     * @return array The created account.
     */
    private function createAccountFromServiceData(array $serviceData, string $serviceName, ?string $tokenName = null): array
    {
        try {
            $account = $this->createNewAccount($serviceData, $serviceName);

            if (! $account) {
                return [
                    "result" => false,
                    "msg" => "Failed to create account",
                ];
            }

            $loginResult = $this->getModelRole()->canUserLogin($account);
            if ($loginResult["result"] === true) {
                return $this->finalizeAccountCreation($account, $tokenName);
            }

            return $loginResult;
        } catch (CTKException $e) {
            return [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
    }

    /**
     * Crée un nouveau compte utilisateur.
     *
     * @param array $serviceData Les données du service.
     * @param string $serviceName Le nom du service.
     * @return array|null Les informations du compte utilisateur créé, ou null si la création a échoué.
     */
    private function createNewAccount(array $serviceData, string $serviceName): ?array
    {
        $serviceId = "services." . $serviceName . ".id";
        $params = $this->prepareAccountParams($serviceData, $serviceName);

        $this->insert($params, PersonInterface::REGISTER_MODE_SERVICE);
        return $this->db->findOne(PersonInterface::COLLECTION, [
            $serviceId => (string) $serviceData['id'],
        ]);
    }

    /**
     * Prepares the account parameters for a given service.
     *
     * @param array $serviceData The service data.
     * @param string $serviceName The name of the service.
     * @return array The prepared account parameters.
     */
    private function prepareAccountParams(array $serviceData, string $serviceName): array
    {
        return [
            "name" => (string) $serviceData['name'],
            "email" => (string) $serviceData['email'],
            "username" => (string) $serviceData['username'],
            "services" => [
                $serviceName => [
                    "id" => (string) $serviceData['id'],
                    "username" => (string) $serviceData['username'],
                    "email" => (string) $serviceData['email'],
                ],
            ],
        ];
    }

    /**
     * Finalizes the account creation process.
     *
     * @param array $account The account details.
     * @param string|null $tokenName The name of the token.
     * @return array The updated account details.
     */
    private function finalizeAccountCreation(array $account, ?string $tokenName): array
    {
        if (! empty($tokenName)) {
            $token = $this->addToken((string) $account["_id"], $tokenName);
        }
        unset($account["pwd"]);
        $response = [
            "result" => true,
            "id" => (string) $account["_id"],
            "isCommunected" => isset($account["cp"]),
            "account" => $account,
            "msg" => "Vous êtes maintenant identifié : bienvenue sur communecter.",
        ];
        if (isset($token)) {
            $response["token"] = $token;
        }
        return $response;
    }

    /**
     * Update the account from service data.
     *
     * @param array $account The account data.
     * @param array $serviceData The service data.
     * @param string $serviceName The name of the service.
     * @param string|null $tokenName The name of the token (optional).
     * @return array The updated account data.
     */
    private function updateAccountFromServiceData(array $account, array $serviceData, string $serviceName, ?string $tokenName = null): array
    {
        $params = [
            "services" => [
                $serviceName => [
                    "id" => (string) $serviceData['id'],
                    "username" => (string) $serviceData['username'],
                    "email" => (string) $serviceData['email'],
                ],
            ],
        ];

        $this->db->update(PersonInterface::COLLECTION, [
            "_id" => $account["_id"],
        ], [
            '$set' => $params,
        ]);

        //Roles validation
        $res = $this->getModelRole()->canUserLogin($account);
        if ($res["result"] === true) {
            if (! empty($tokenName)) {
                $token = $this->addToken((string) $account["_id"], $tokenName);
            }
            unset($account["pwd"]);
            $res = [
                "result" => true,
                "id" => (string) $account["_id"],
                "isCommunected" => isset($account["cp"]),
                "account" => $account,
                "msg" => "Vous êtes maintenant identifié : bienvenue sur communecter.",
            ];
            if (isset($token) && ! empty($token)) {
                $res["token"] = $token;
            }
        }
        return $res;
    }

    /**
     * Logs in a user using the authentication service token.
     *
     * @param string $service The authentication service details.
     * @param string|null $tokenName The name of the authentication token.
     * @return array The user information.
     */
    public function loginAuthServiceToken(string $service, ?string $tokenName = null): array
    {
        if (empty($service)) {
            return [
                "result" => false,
                "msg" => "service is empty",
            ];
        }

        if (empty($tokenName)) {
            $tokenName = "comobi";
        }

        $passphrase = $this->params->get('passphrase');
        $serviceDecrypt = (! empty($passphrase) && is_string($passphrase)) ? $this->cryptoJsAesDecrypt($passphrase, $service) : null;

        if (empty($serviceDecrypt) || ! is_array($serviceDecrypt)) {
            return [
                "result" => false,
                "msg" => "serviceDecrypt is empty",
            ];
        }
        $this->clearUserSessionData();

        $res = $this->serviceOauth($serviceDecrypt, $tokenName);

        if (isset($res["account"]["services"])) {
            unset($res["account"]["services"]);
        }

        return $res;
    }

    /**
     * Decrypts a JSON string using the CryptoJS AES algorithm.
     *
     * @param string $passphrase The passphrase used for decryption.
     * @param string $jsonString The JSON string to be decrypted.
     * @return mixed The decrypted JSON string.
     */
    private function cryptoJsAesDecrypt(string $passphrase, string $jsonString)
    {
        $jsondata = json_decode($jsonString, true);

        if (! isset($jsondata["s"]) || ! isset($jsondata["iv"]) || ! isset($jsondata["ct"])) {
            // Les données JSON ne sont pas dans le format attendu
            return null;
        }
        if (! is_string($jsondata["s"]) || ! is_string($jsondata["iv"]) || ! is_string($jsondata["ct"])) {
            return null;
        }

        try {
            $salt = hex2bin($jsondata["s"]);
            $iv = hex2bin($jsondata["iv"]);
        } catch (Exception $e) {
            return null; // Erreur lors de la conversion des données hexadécimales
        }

        $cipherText = base64_decode($jsondata["ct"]);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = [md5($concatedPassphrase, true)];
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);

        $decryptedData = openssl_decrypt($cipherText, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);

        return $decryptedData !== false ? json_decode($decryptedData, true) : null;
    }

    /**
     * Encrypts a value using the CryptoJS AES algorithm.
     *
     * @param string $passphrase The passphrase used for encryption.
     * @param mixed $value The value to be encrypted.
     * @return string The encrypted value.
     */
    public function cryptoJsAesEncrypt(string $passphrase, $value): string
    {
        // Génère un sel aléatoire
        $salt = openssl_random_pseudo_bytes(8);

        // Génère une clé et un IV en utilisant le sel et le mot de passe
        $keyIv = $this->generateKeyIv($passphrase, $salt);
        $key = substr($keyIv, 0, 32);
        $iv = substr($keyIv, 32, 16);

        // Chiffre la valeur
        $encryptedData = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        if ($encryptedData === false) {
            throw new Exception("Encryption failed");
        }

        // Prépare les données pour le retour
        $data = [
            "ct" => base64_encode($encryptedData),
            "iv" => bin2hex($iv),
            "s" => bin2hex($salt),
        ];
        return json_encode($data);
    }

    /**
     * Generates a key and initialization vector (IV) for encryption.
     *
     * @param string $passphrase The passphrase used for encryption.
     * @param string $salt The salt used for encryption.
     * @return string The generated key and IV.
     */
    private function generateKeyIv(string $passphrase, string $salt): string
    {
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }
        return $salted;
    }

    /**
     * Check if the provided password matches the password stored in the account.
     *
     * @param string $pwd The password to check.
     * @param array $account The account information containing the stored password.
     * @return bool Returns true if the password matches, false otherwise.
     */
    public function checkPassword(string $pwd, array $account): bool
    {
        if (empty($account["pwd"]) || empty($account["email"]) || empty($account["_id"])) {
            return false;
        }
        if (! is_string($account["pwd"]) || ! is_string($account["email"])) {
            return false;
        }

        $passwordHash = hash('sha256', $account["email"] . $pwd);
        if ($account["pwd"] === $passwordHash) {
            $this->updatePasswordToNewSalt($account, $pwd);
            return true;
        }

        return $account["pwd"] === $this->hashPassword((string) $account["_id"], $pwd);
    }

    /**
     * Updates the password to a new salt.
     *
     * @param array $account The account information.
     * @param string $pwd The new password.
     * @return void
     */
    private function updatePasswordToNewSalt(array $account, string $pwd): void
    {
        $newPassword = $this->hashPassword((string) $account["_id"], $pwd);
        $this->updatePersonField((string) $account["_id"], "pwd", $newPassword, (string) $account["_id"]);
        $this->logPasswordChange($account);
    }

    /**
     * Logs the password change for a given account.
     *
     * @param array $account The account details.
     * @return void
     */
    private function logPasswordChange(array $account): void
    {
        $this->getModelLog()->save([
            "userId" => $account["_id"],
            "browser" => $_SERVER["HTTP_USER_AGENT"] ?? '',
            "ipAddress" => $_SERVER["REMOTE_ADDR"] ?? '',
            "created" => $this->db->MongoDate(time()),
            "action" => "person/newsaltpassword",
        ]);
    }

    /**
     * Retrieves the action rooms associated with a person by their ID.
     *
     * @param string $id The ID of the person.
     * @param bool|null $archived Whether to include archived action rooms. Defaults to null.
     * @return array The action rooms associated with the person.
     */
    public function getActionRoomsByPersonId(string $id, ?bool $archived = null): array
    {
        if (empty($id) || empty($this->session->get("userEmail"))) {
            return [];
        }

        //get action Rooms I created
        $where = [
            "email" => $this->session->get("userEmail"),
        ];
        if (isset($archived)) {
            $where['status'] = ActionRoomInterface::STATE_ARCHIVED;
        } else {
            $where['status'] = [
                '$exists' => 0,
            ];
        }
        $actionRooms = $this->db->find(ActionRoomInterface::COLLECTION, $where);
        $actions = [];
        $person = $this->getById($id);

        if (empty($person["_id"])) {
            throw new CTKException("The person id is unkown : contact your admin");
        }

        //get rooms connected to user communected city
        if (! empty($person["address"]) && ! empty($person["address"]["postalCode"]) && ! empty($person["address"]["codeInsee"]) && ! empty($person["address"]["addressCountry"]) && is_string($person["address"]["postalCode"]) && is_string($person["address"]["codeInsee"]) && is_string($person["address"]["addressCountry"])) {
            $myCityRooms = $this->db->find(
                ActionRoomInterface::COLLECTION,
                [
                    'parentType' => CityInterface::COLLECTION,
                    'parentId' => $person["address"]["addressCountry"] . '_' . $person["address"]["codeInsee"] . '-' . $person["address"]["postalCode"],
                ]
            );
            foreach ($myCityRooms as $roomId => $room) {
                if (! isset($actionRooms[$roomId])) {
                    $actionRooms[$roomId] = $room;
                }
            }
        }
        //get rooms connected to all users actions
        if (! empty($person["actions"]) && ! empty($person["actions"]["surveys"])) {
            foreach ($person["actions"]["surveys"] as $entryId => $action) {
                $entry = $this->getModelSurvey()->getById($entryId);
                if (! empty($entry) && is_array($entry)) {
                    $entry['action'] = $action;
                    $actions[(string) $entryId] = $entry;

                    if (! empty($entry['survey']) && empty($actionRooms[(string) $entry['survey']])) {
                        $actionRoom = $this->getModelActionRoom()->getById($entry['survey']);
                        if (! empty($actionRoom) && is_array($actionRoom)) {
                            $actionRooms[(string) $entry['survey']] = $actionRoom;
                        }
                    }
                }
            }
        }

        return [
            "rooms" => $actionRooms,
            "actions" => $actions,
        ];
    }

    /**
     * Retrieves the action rooms associated with a person based on their ID and type.
     *
     * @param string $uid The ID of the person.
     * @param string $type The type of action room.
     * @param string $id The ID of the action room.
     * @param bool|null $archived (Optional) Whether to include archived action rooms. Defaults to null.
     * @return array The action rooms associated with the person.
     */
    public function getActionRoomsByPersonIdByType(string $uid, string $type, string $id, ?bool $archived = null): array
    {
        if (empty($uid) || empty($type) || empty($id)) {
            throw new CTKException("Missing parameters");
        }
        $where = [];
        $where["parentType"] = $type;
        $where["parentId"] = $id;

        if (isset($archived)) {
            $where['status'] = ActionRoomInterface::STATE_ARCHIVED;
        } else {
            $where['status'] = [
                '$exists' => 0,
            ];
        }

        $actionRooms = $this->getModelActionRoom()->getWhereSortLimit($where, [
            "date" => 1,
        ], 0);

        $actions = [];
        $person = $this->getById($uid);

        if (empty($person["_id"])) {
            throw new CTKException("The person id is unkown : contact your admin");
        }

        if (! empty($person["actions"]["surveys"]) && is_array($person["actions"]["surveys"])) {
            foreach ($person["actions"]["surveys"] as $entryId => $action) {
                $entry = $this->getModelSurvey()->getById($entryId);
                if (! empty($entry['survey']) && is_string($entry['survey']) && is_array($actionRooms) && isset($actionRooms[$entry['survey']])) {
                    $entry['action'] = $action;
                    $actions[$entryId] = $entry;
                }
            }
        }

        return [
            "rooms" => $actionRooms,
            "actions" => $actions,
        ];
    }

    /**
     * Change the password for a user.
     *
     * @param string $oldPassword The old password.
     * @param string $newPassword The new password.
     * @param string $userId The user ID.
     * @return array The result of the password change.
     */
    public function changePassword(string $oldPassword, string $newPassword, string $userId): array
    {
        if (empty($oldPassword) || empty($newPassword) || empty($userId)) {
            throw new CTKException("Missing parameters");
        }

        $person = $this->getById($userId, false);

        if (empty($person["_id"])) {
            throw new CTKException("The person id is unkown : contact your admin");
        }

        // Vérifier l'ancien mot de passe
        if (! $this->checkPassword($oldPassword, $person)) {
            return [
                "result" => false,
                "msg" => $this->language->t("person", "Your current password is incorrect"),
            ];
        }

        // Vérifier la longueur du nouveau mot de passe
        if (! $this->isValidePassword($newPassword)) {
            return [
                "result" => false,
                "msg" => $this->language->t("person", "The new password should be 8 characters long"),
            ];
        }

        // Chiffrer le nouveau mot de passe et le mettre à jour
        $encodedPwd = $this->hashPassword((string) $person["_id"], $newPassword);
        $this->updatePersonField($userId, "pwd", $encodedPwd, $userId);

        return [
            "result" => true,
            "msg" => $this->language->t("person", "Your password has been changed with success!"),
        ];
    }

    /**
     * Validates the email account with the given account ID and validation key.
     *
     * @param string $accountId The ID of the email account to validate.
     * @param string $validationKey The validation key for the email account.
     * @return array An array containing the validation result.
     */
    public function validateEmailAccount(string $accountId, string $validationKey): array
    {
        if ($this->isRightValidationKey($accountId, $validationKey)) {
            //remove tobeactivated attribute on account
            $res = $this->validateUser($accountId);
        } else {
            $res = [
                "result" => false,
                "msg" => "The validation key is incorrect !",
            ];
        }

        return $res;
    }

    /**
     * Check if the provided validation key is correct for the given account ID.
     *
     * @param string $accountId The account ID to check the validation key for.
     * @param string $validationKey The validation key to check.
     * @return bool Returns true if the validation key is correct, false otherwise.
     */
    public function isRightValidationKey(string $accountId, string $validationKey): bool
    {
        $validationKeycheck = $this->getValidationKeyCheck($accountId);
        return ($validationKeycheck == $validationKey);
    }

    /**
     * Validates a user.
     *
     * @param string $accountId The account ID of the user.
     * @param bool $admin (optional) Whether the user is an admin or not. Default is false.
     * @return array The result of the validation method.
     */
    public function validateUser(string $accountId, bool $admin = false): array
    {
        $account = $this->db->findOneById(PersonInterface::COLLECTION, $accountId, ["name", "email", "source", "mailToResend"]);

        // todo : pourquoi on fait getEmailById pour recupérer l'email alors qu'on l'a déjà dans $account normalement ?
        // $email = $this->getEmailById($accountId);
        // $account["email"] = $email["email"];

        if (empty($account)) {
            $res = [
                "result" => false,
                "msg" => "Unknown account !",
            ];
        }

        $sendMailSource = $account['source']['key'] ?? "communecter";

        $actionArr = [
            '$unset' => [
                "roles.tobeactivated" => "",
            ],
            '$set' => [
                "preferences.sendMail" => [
                    "source" => [$sendMailSource],
                ],
            ],
        ];

        if (! empty($account['mailToResend'])) {
            $actionArr['$unset']['mailToResend'] = "";
        }

        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($accountId),
            ],
            $actionArr
        );

        $res = [
            "result" => true,
            "account" => $account,
            "msg" => "The account and email is now validated !",
        ];

        return $res;
    }

    /**
     * Returns the validation key check for a given account ID.
     *
     * @param string $accountId The account ID to check the validation key for.
     * @return string The validation key check.
     */
    public function getValidationKeyCheck(string $accountId): string
    {
        $account = $this->getEmailById($accountId);
        if ($account && ! empty($account["email"]) && is_string($account["email"])) {
            $validationKeycheck = hash('sha256', $accountId . $account["email"]);
        } else {
            throw new CTKException("The account is unknwon !");
        }

        return $validationKeycheck;
    }

    /**
     * Updates the minimal data of a person.
     *
     * @param string $personId The ID of the person.
     * @param array $person The updated person data.
     * @return array The updated person data.
     */
    public function updateMinimalData(string $personId, array $person): array
    {
        if (empty($personId) || empty($person)) {
            throw new CTKException("Missing parameters");
        }

        //Check if it's a minimal user
        $account = $this->getById($personId, false);

        if (empty($account["_id"])) {
            throw new CTKException("The person id is unkown : contact your admin");
        }

        if (! isset($account["pending"])) {
            throw new CTKException("Impossible to update an account not pending !");
        }

        if (! isset($person["pwd"]) || empty($person["pwd"])) {
            throw new CTKException("Password is required for update.");
        }

        $person["email"] = $account["email"];

        if (! is_string($person["pwd"]) || ! $this->isValidePassword($person["pwd"])) {
            throw new CTKException("The password not valid");
        }

        $pwd = $this->hashPassword($personId, $person["pwd"]);

        //Update des infos minimales
        try {
            $personToUpdate = $this->getAndcheckPersonData($person, PersonInterface::REGISTER_MODE_TWO_STEPS, false);
        } catch (CTKException $e) {
            return [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }

        $personToUpdate["pwd"] = $pwd;
        // CREATE SLUG FOR CITOYENS
        $personToUpdate["slug"] = $this->getModelSlug()->checkAndCreateSlug((string) $personToUpdate["username"]);

        $sendMailSource = $account['source']['key'] ?? "communecter";
        $personToUpdate["preferences.sendMail"] = [
            "source" => [$sendMailSource],
        ];

        $this->getModelSlug()->save(PersonInterface::COLLECTION, $personId, $personToUpdate["slug"]);

        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($personId),
            ],
            [
                '$set' => $personToUpdate,
                '$unset' => [
                    "pending" => "",
                    "roles.tobeactivated" => "",
                ],
            ]
        );

        $res = [
            "result" => true,
            "msg" => "The pending user has been updated and is now complete",
            "personId" => $personId,
        ];

        return $res;
    }

    // todo : est ce que ça sert ? dans RemoveHelpBlockAction on fait aucun test si la personne est connectée ou pas
    // je pense pas que l'action est utilisé je commente pour le moment
    // public function updateNotSeeHelpCo(string $id): array
    // {
    //     if(empty($id)) {
    //         throw new CTKException("Missing parameters");
    //     }

    //     $this->db->update(
    //         self::COLLECTION,
    //         array("_id" => $this->db->MongoId($id)),
    //         array('$set' => array("preferences.unseenHelpCo" => true))
    //     );

    //     $account = $this->getById($id);

    //     if(empty($account) || empty($account["_id"])) {
    //         throw new CTKException("The person id is unkown : contact your admin");
    //     }

    //     $this->saveUserSessionData($account);

    //     return array("result" => true);
    // }

    /**
     * Hashes the password using SHA256 algorithm.
     *
     * @param string $personId The person ID.
     * @param string $pwd The password to be hashed.
     * @return string The hashed password.
     */
    private function hashPassword(string $personId, string $pwd): string
    {
        return hash('sha256', $personId . $pwd);
    }

    /**
     * Valide le mot de passe.
     *
     * @param string $pwd Le mot de passe à valider.
     * @return bool Retourne true si le mot de passe est valide, sinon false.
     */
    private function isValidePassword(string $pwd): bool
    {
        return strlen($pwd) >= 8;
    }

    /**
     * Check if the given username is unique.
     *
     * @param string $username The username to check.
     * @return bool Returns true if the username is unique, false otherwise.
     */
    public function isUniqueUsername(string $username): bool
    {
        $checkUsername = $this->db->findOne(PersonInterface::COLLECTION, [
            "username" => $username,
        ]);
        return $checkUsername ? false : true;
    }

    /**
     * Check and create a unique username.
     *
     * @param string $username The username to be checked and created.
     * @return string The unique username.
     * @throws CTKException If a unique username cannot be generated.
     */
    protected function checkAndCreateUniqueUsername(string $username): string
    {
        if ($this->isUniqueUsername($username)) {
            return $username;
        }

        $suffix = 1;
        $uniqueUsername = $username . $suffix;
        $maxAttempts = 100; // Nombre maximum d'essais pour trouver un nom d'utilisateur unique

        while (! $this->isUniqueUsername($uniqueUsername) && $suffix < $maxAttempts) {
            $suffix++;
            $uniqueUsername = $username . $suffix;
        }

        if ($suffix >= $maxAttempts) {
            // Gérer le cas où un nom d'utilisateur unique n'a pas été trouvé après le nombre maximum d'essais
            throw new CTKException("Impossible de générer un nom d'utilisateur unique après $maxAttempts essais.");
        }

        return $uniqueUsername;
    }

    /**
     * Check if the given email is unique.
     *
     * @param string $email The email to check.
     * @return bool Returns true if the email is unique, false otherwise.
     */
    public function isUniqueEmail(string $email): bool
    {
        $checkEmail = $this->db->findOne(PersonInterface::COLLECTION, [
            "email" => $email,
        ]);
        return $checkEmail ? false : true;
    }

    /**
     * Check if the person is the first citizen with the given INSEE number.
     *
     * @param string $insee The INSEE number to check.
     * @return bool Returns true if the person is the first citizen, false otherwise.
     */
    public function isFirstCitizen(string $insee): bool
    {
        $checkInsee = $this->db->findOne(PersonInterface::COLLECTION, [
            "address.codeInsee" => $insee,
        ]);
        return $checkInsee ? false : true;
    }

    /**
     * Retrieves a person by their email address.
     *
     * @param string $email The email address of the person.
     * @return array|null The person's information as an array, or null if not found.
     */
    public function getPersonByEmail(string $email): ?array
    {
        if (empty($email)) {
            return null;
        }
        //Check if the email of the person is already in the database
        return $this->db->findOne(PersonInterface::COLLECTION, [
            "email" => $this->db->MongoRegex('/^' . preg_quote(trim($email)) . '$/i'),
        ]);
    }

    // TODO // Just for an id, we call a lot of datas ! To clean and delete it, function avaible in element !
    /**
     * Retrieves the person ID associated with the given email.
     *
     * @param string|null $email The email address of the person.
     * @return mixed The person ID if found, false or null otherwise.
     */
    public function getPersonIdByEmail(?string $email)
    {
        if (! empty($email)) {
            $account = $this->getPersonByEmail($email);
            return $account["_id"] ?? false;
        }

        return null;
    }

    // todo : n'est pas utilisé, à supprimer ?
    // /**
    //  * This function checks if $userId is linked to this email
    //  *
    //  * @autors
    //  **/
    // public function isLinkedEmail($email, $userId)
    // {
    //     $res = false;
    //     $id = $this->getPersonIdByEmail($email);
    //     //var_dump($id);
    //     if ($id != false) {
    //         $res = Link::isLinked($id, self::COLLECTION, $userId);
    //         //var_dump($res);
    //     }
    //     return $res;
    // }

    // todo : fonction dangereurse et pas optimiser, je la vois dans CheckLinkMailWithUserAction InviteAction
    /**
     * Retrieves the list of Person objects that are followed by a specific user.
     *
     * @param string $id The ID of the user.
     * @return array The list of Person objects.
     */
    public function getPersonFollowsByUser(string $id): array
    {
        $res = [];
        $person = $this->getById($id);

        if (empty($person)) {
            throw new CTKException($this->language->t("common", "Something went wrong : please contact your admin"));
        }
        if (! empty($person["links"]) && ! empty($person["links"]["follows"]) && is_array($person["links"]["follows"])) {
            foreach ($person["links"]["follows"] as $key => $follow) {
                if (! empty($follow["type"]) && ! empty($key) && is_string($follow["type"]) && is_string($key)) {
                    if ($follow["type"] == "citoyens" || $follow["type"] == "organizations" || $follow["type"] == "projects") {
                        $entity = $this->db->findOneById($follow["type"], $key);
                        if (! empty($entity)) {
                            $res[$key] = $entity;
                        } else {
                            error_log("[DATA-INCORRECT] - Impossible to find the " . $follow["type"] . " with the id " . $key . " ! Link follow on the person id : " . $id);
                        }
                    } else {
                        error_log("[DATA-INCORRECT] - Impossible to find the " . $follow["type"] . " with the id " . $key . " ! Link follow on the person id : " . $id);
                    }
                }
            }
        }
        return $res;
    }


    // todo : est ce que ça sert ? je la vois dans SourceAdminAction
    /**
     * Retrieves the source admin for a person.
     *
     * @param string $id The ID of the person.
     * @return array|null The source admin.
     */
    // public function getSourceAdmin($id): ?array
    // {
    //     $result = $this->db->findOneById(self::COLLECTION, $id, array('sourceAdmin'));

    //     return $result['sourceAdmin'] ?? null;
    // }

    // todo : je crois que ce n'est plus utilisé, à supprimer ?
    // /**
    //  * update a person in database
    //  * @param string $personId :
    //  * @param array $personChangedFields fields to update
    //  * @param string $userId : the userId making the update
    //  * @return array of result (result => boolean, msg => string)
    //  */
    // public function update($personId, $personChangedFields, $userId)
    // {
    //     $person = $this->getById($personId);

    //     if (!$person) {
    //         error_log("Unknown person Id : " . $personId);
    //         return array("result" => false, "msg" => $this->language->t("person", "Something went really wrong ! "), "id" => $personId);
    //     }

    //     foreach ($personChangedFields as $fieldName => $fieldValue) {
    //         if (is_array($fieldValue) && $fieldName != "address") {
    //             foreach ($fieldValue as $fieldName2 => $fieldValue2) {
    //                 $this->updatePersonField($personId, $fieldName2, $fieldValue2, $userId);
    //             }
    //         } else {
    //             $this->updatePersonField($personId, $fieldName, $fieldValue, $userId);
    //         }
    //     }

    //     return array("result" => true, "msg" => $this->language->t("person", "The person has been updated"), "id" => $personId);
    // }


    // todo : je crois que ce n'est plus utilisé, à supprimer ?
    // /**
    //  * Crée une personne à partir des données d'importation.
    //  *
    //  * @param array $personImportData Les données d'importation de la personne.
    //  * @param array|null $warnings Les avertissements éventuels.
    //  * @return array La personne créée.
    //  */
    // public function createPersonFromImportData($personImportData, $warnings = null)
    // {
    //     $newPerson = [];
    //     if (!empty($personImportData['name'])) {
    //         $newPerson["name"] = $personImportData["name"];
    //     }

    //     if (!empty($personImportData['email'])) {
    //         $newPerson["email"] = $personImportData["email"];
    //     }

    //     if (!empty($personImportData['username'])) {
    //         $newPerson["username"] = $personImportData["username"];
    //     }

    //     if (!empty($personImportData['pwd'])) {
    //         $newPerson["pwd"] = $personImportData["pwd"];
    //     }

    //     if (!empty($personImportData['created'])) {
    //         $newPerson["created"] = $personImportData["created"];
    //     }

    //     if (!empty($personImportData['tags'])) {
    //         $newPerson["tags"] = $personImportData["tags"];
    //     }

    //     if (!empty($personImportData['sourceAdmin'])) {
    //         $newPerson["sourceAdmin"] = $personImportData["sourceAdmin"];
    //     }

    //     if (!empty($personImportData['msgInvite'])) {
    //         $newPerson["msgInvite"] = $personImportData["msgInvite"];
    //     }

    //     if (!empty($personImportData['nameInvitor'])) {
    //         $newPerson["nameInvitor"] = $personImportData["nameInvitor"];
    //     }

    //     if (!empty($personImportData['source'])) {
    //         if (!empty($personImportData['source']['id'])) {
    //             $newPerson["source"]['id'] = $personImportData["source"]['id'];
    //         }
    //         if (!empty($personImportData['source']['url'])) {
    //             $newPerson["source"]['url'] = $personImportData["source"]['url'];
    //         }
    //         if (!empty($personImportData['source']['key'])) {
    //             $newPerson["source"]['key'] = $personImportData['source']['key'];
    //         }
    //     }

    //     if (!empty($personImportData['warnings'])) {
    //         $newPerson["warnings"] = $personImportData["warnings"];
    //     }

    //     if (!empty($personImportData['image'])) {
    //         $newPerson["image"] = $personImportData["image"];
    //     }

    //     if (!empty($personImportData["badges"])) {
    //         $newPerson["badges"] = $personImportData["badges"];
    //     }

    //     if (!empty($personImportData['shortDescription'])) {
    //         $newPerson["shortDescription"] = $personImportData["shortDescription"];
    //     }

    //     if (!empty($personImportData['geo']['latitude'])) {
    //         $newPerson['geo']['latitude'] = $personImportData['geo']['latitude'];
    //     }

    //     if (!empty($personImportData['geo']['longitude'])) {
    //         $newPerson['geo']['longitude'] = $personImportData['geo']['longitude'];
    //     }

    //     if (!empty($personImportData['telephone'])) {
    //         $tel = array();
    //         $fixe = array();
    //         $mobile = array();
    //         $fax = array();
    //         if (!empty($personImportData['telephone']["fixe"])) {
    //             foreach ($personImportData['telephone']["fixe"] as $key => $value) {
    //                 $trimValue = trim($value);
    //                 if (!empty($trimValue)) {
    //                     $fixe[] = $trimValue;
    //                 }
    //             }
    //         }
    //         if (!empty($personImportData['telephone']["mobile"])) {
    //             foreach ($personImportData['telephone']["mobile"] as $key => $value) {
    //                 $trimValue = trim($value);
    //                 if (!empty($trimValue)) {
    //                     $mobile[] = $trimValue;
    //                 }
    //             }
    //         }
    //         if (!empty($personImportData['telephone']["fax"])) {
    //             foreach ($personImportData['telephone']["fax"] as $key => $value) {
    //                 $trimValue = trim($value);
    //                 if (!empty($trimValue)) {
    //                     $fax[] = $trimValue;
    //                 }
    //             }
    //         }
    //         if (count($mobile) != 0) {
    //             $tel["mobile"] = $mobile;
    //         }
    //         if (count($fixe) != 0) {
    //             $tel["fixe"] = $fixe;
    //         }
    //         if (count($fax) != 0) {
    //             $tel["fax"] = $fax;
    //         }
    //         if (count($tel) != 0) {
    //             $newPerson['telephone'] = $tel;
    //         }
    //     }
    //     //var_dump($personImportData['address']);
    //     if (!empty($personImportData['address'])) {
    //         $details = Import::getAndCheckAddressForEntity($personImportData['address'], (empty($newPerson['geo']) ? null : $newPerson['geo']));
    //         $newPerson['address'] = $details['address'];
    //         //var_dump($newPerson['address']);
    //         if (!empty($newPerson['warnings'])) {
    //             $newPerson['warnings'] = array_merge($newPerson['warnings'], $details['warnings']);
    //         } else {
    //             $newPerson['warnings'] = $details['warnings'];
    //         }
    //     }
    //     //var_dump("---------------------------------");
    //     return $newPerson;
    // }


    // todo : je crois que ce n'est plus utilisé, à supprimer ?
    // public function getAndCheckPersonFromImportData($person, $invite = null, $insert = null, $update = null, $warnings = null)
    // {
    //     $newPerson = array();
    //     if (empty($person['name'])) {
    //         if ($warnings) {
    //             $newPerson["warnings"][] = "201";
    //         } else {
    //             throw new CTKException($this->language->t("import", "201", null, $this->moduleId));
    //         }
    //     } else {
    //         $newPerson['name'] = $person['name'];
    //     }

    //     if (empty($person['email'])) {
    //         throw new CTKException($this->language->t("import", "203"));
    //     } else {
    //         //Check email
    //         $checkEmail = DataValidator::email($person["email"]);
    //         if ($checkEmail != "") {
    //             throw new CTKException($this->language->t("import", "205", null, $this->moduleId));
    //         }
    //         //Check if the email of the person is already in the database
    //         $account = self::getPersonByEmail($person["email"]);
    //         //$account = $this->db->findOne(self::COLLECTION,array("email"=>$person["email"]));
    //         if ($account) {
    //             throw new CTKException($this->language->t("import", "206", null, $this->moduleId));
    //         }
    //         $newPerson['email'] = $person['email'];
    //     }

    //     if (!$update) {
    //         $newPerson["created"] = $this->db->MongoDate(time());
    //     }

    //     if (empty($person['username'])) {
    //         if (!empty($person['email'])) {
    //             $newPerson['username'] = $this->generedUserNameByEmail($person['email'], true);
    //             if ($warnings) {
    //                 $newPerson["warnings"][] = "211";
    //             }
    //         } else {
    //             if ($warnings) {
    //                 $newPerson["warnings"][] = "210";
    //             } else {
    //                 throw new CTKException($this->language->t("import", "210", null, $this->moduleId));
    //             }
    //         }
    //     } else {
    //         if (!$this->isUniqueUsername($person["username"])) {
    //             if (!empty($invite)) {
    //                 $newPerson['username'] = $this->generedUserNameByEmail($person['email'], true);
    //             } else {
    //                 throw new CTKException($this->language->t("import", "207", null, $this->moduleId));
    //             }
    //         } else {
    //             $newPerson['username'] = $person['username'];
    //         }
    //     }



    //     if (!empty($person["badges"])) {
    //         $newPerson["badges"] = Badge::conformeBadges($person["badges"]);
    //     }

    //     if (empty($invite)) {
    //         if (empty($person['pwd'])) {
    //             if ($warnings) {
    //                 $newPerson["warnings"][] = "204";
    //             } else {
    //                 throw new CTKException($this->language->t("import", "204", null, $this->moduleId));
    //             }
    //         } else {
    //             $newPerson['pwd'] = $person['pwd'];
    //         }

    //         if (!empty($person['geo']) && !empty($person["geoPosition"])) {
    //             $newPerson["geo"] = $person['geo'];
    //             $newPerson["geoPosition"] = $person['geoPosition'];
    //         } elseif (!empty($person["geo"]['latitude']) && !empty($person["geo"]["longitude"])) {
    //             $newPerson["geo"] = array(
    //                 "@type" => "GeoCoordinates",
    //                 "latitude" => $person["geo"]['latitude'],
    //                 "longitude" => $person["geo"]["longitude"],
    //             );

    //             $newPerson["geoPosition"] = array(
    //                 "type" => "Point",
    //                 "coordinates" =>
    //                     array(
    //                         floatval($person["geo"]['latitude']),
    //                         floatval($person["geo"]['longitude']),
    //                     ),
    //             );
    //         } elseif ($insert) {
    //             if ($warnings) {
    //                 $newPerson["warnings"][] = "150";
    //             } else {
    //                 throw new CTKException($this->language->t("import", "150", null, $this->moduleId));
    //             }
    //         } elseif ($warnings) {
    //             $newPerson["warnings"][] = "150";
    //         }

    //         if (!empty($person['address'])) {
    //             if (empty($person['address']['postalCode']) /*&& $insert*/) {
    //                 if ($warnings) {
    //                     $newPerson["warnings"][] = "101";
    //                 } else {
    //                     throw new CTKException($this->language->t("import", "101", null, $this->moduleId));
    //                 }
    //             }
    //             if (empty($person['address']['codeInsee']) /*&& $insert*/) {
    //                 if ($warnings) {
    //                     $newPerson["warnings"][] = "102";
    //                 } else {
    //                     throw new CTKException($this->language->t("import", "102", null, $this->moduleId));
    //                 }
    //             }
    //             if (empty($person['address']['addressCountry']) /*&& $insert*/) {
    //                 if ($warnings) {
    //                     $newPerson["warnings"][] = "104";
    //                 } else {
    //                     throw new CTKException($this->language->t("import", "104", null, $this->moduleId));
    //                 }
    //             }
    //             if (empty($person['address']['addressLocality']) /*&& $insert*/) {
    //                 if ($warnings) {
    //                     $newPerson["warnings"][] = "105";
    //                 } else {
    //                     throw new CTKException($this->language->t("import", "105", null, $this->moduleId));
    //                 }
    //             }


    //             $newPerson['address'] = $person['address'];
    //         } else {
    //             if (!empty($newPerson["geo"])) {
    //                 $resLocality = json_decode(Import::getLocalityByLatLonNominatim($newPerson["geo"]["latitude"], $newPerson["geo"]["longitude"]), true);

    //                 //var_dump($resLocality);
    //                 if (!empty($resLocality["address"])) {
    //                     $newPerson['address']['addressCountry'] = "FR";
    //                     $city = SIG::getInseeByLatLngCp($newPerson["geo"]["latitude"], $newPerson["geo"]["longitude"], (empty($resLocality["address"]["postcode"]) ? null : $resLocality["address"]["postcode"]));
    //                     /*if($city != null){
    //                         foreach ($city as $key => $value) {
    //                             $insee = $value["insee"];
    //                         }
    //                         $newPerson['address']['codeInsee'] = $insee ;
    //                         $newPerson['address']['postalCode'] = (empty($resLocality["address"]["postcode"])?"":$resLocality["address"]["postcode"]);
    //                         $newPerson['address']['streetAddress'] = (empty($resLocality["address"]["road"])?"":$resLocality["address"]["road"]);
    //                         $locality = City::getAlternateNameByInseeAndCP($newPerson['address']['codeInsee'], $newPerson['address']['postalCode']);
    //                         $newPerson['address']['addressLocality'] = $locality['alternateName'];


    //                     }*/
    //                     if (!empty($city)) {
    //                         foreach ($city["postalCodes"] as $keyCp => $valueCp) {
    //                             if (!empty($resLocality["address"]["postcode"]) && $valueCp["postalCode"] == $resLocality["address"]["postcode"]) {
    //                                 $newPerson['address']['addressCountry'] = "FR";
    //                                 $newAddress["codeInsee"] = $city["insee"];
    //                                 $newAddress['addressCountry'] = $city["country"];
    //                                 $newAddress['addressLocality'] = $valueCp["name"];
    //                                 $newAddress['postalCode'] = $valueCp["postalCode"];
    //                                 $erreur = false;
    //                                 break;
    //                             }
    //                         }
    //                         if (!empty($newAddress)) {
    //                             $newPerson['address'] = $newAddress;
    //                         }
    //                     }


    //                     //Result DataGouv
    //                     /*$newPerson['address']['addressCountry'] = "FR";
    //                     $newPerson['address']['codeInsee'] = $resLocality["features"][0]["properties"]["citycode"];
    //                     $newPerson['address']['postalCode'] = $resLocality["features"][0]["properties"]["postcode"];
    //                     $newPerson['address']['streetAddress'] = $resLocality["features"][0]["properties"]["street"];
    //                     $newPerson['address']['addressLocality'] = City::getAlternateNameByInseeAndCP($newPerson['address']['codeInsee'], $newPerson['address']['postalCode']);
    //                     */
    //                 } elseif ($warnings) {
    //                     $newPerson["warnings"][] = "100";
    //                 } else {
    //                     throw new CTKException($this->language->t("import", "100", null, $this->moduleId));
    //                 }
    //             } elseif ($warnings) {
    //                 $newPerson["warnings"][] = "100";
    //             } else {
    //                 throw new CTKException($this->language->t("import", "100", null, $this->moduleId));
    //             }
    //         }
    //     } else {
    //         if (!empty($person['msgInvite'])) {
    //             $newPerson["msgInvite"] = $person['msgInvite'];
    //         }
    //         if (!empty($person['nameInvitor'])) {
    //             $newPerson["nameInvitor"] = $person['nameInvitor'];
    //         }
    //     }





    //     if (!empty($person["invitedBy"])) {
    //         $newPerson["invitedBy"] = $person["invitedBy"];
    //     }

    //     if (!empty($person['telephone'])) {
    //         $newPerson["telephone"] = $person['telephone'];
    //     }

    //     if (!empty($person['sourceAdmin'])) {
    //         $newPerson["sourceAdmin"] = $person['sourceAdmin'];
    //     }

    //     if (!empty($person['source'])) {
    //         $newPerson["source"] = $person['source'];
    //     }

    //     if (!empty($person['image'])) {
    //         $newPerson["image"] = $person['image'];
    //     }

    //     if (!empty($person['shortDescription'])) {
    //         $newPerson["shortDescription"] = $person["shortDescription"];
    //     }

    //     //Tags
    //     if (isset($person['tags'])) {
    //         if (is_array($person['tags'])) {
    //             $tags = $person['tags'];
    //         } elseif (is_string($person['tags'])) {
    //             $tags = explode(",", $person['tags']);
    //         }
    //         $newPerson["tags"] = $tags;
    //     }

    //     if (!empty($person['source'])) {
    //         $newPerson["source"] = $person['source'];
    //     }

    //     return $newPerson;
    // }

    // todo : je crois que ce n'est plus utilisé, à supprimer ?
    // /**
    //  * Insert a new project, checking if the project is well formated
    //  * @param array $params Array with all fields for a project
    //  * @param string $userId UserId doing the insertion
    //  * @return array as result type
    //  */
    // public function insertPersonFromImportData($person, $warnings, $invite = null, $isKissKiss = null, $invitorUrl = null, $pathFolderImage = null, $moduleId = null, $paramsLink, $sendMail)
    // {
    //     $personmail = [];
    //     $msgMail = null;
    //     $nameInvitor = null;
    //     $account = self::getPersonByEmail($person["email"]);
    //     if ($account) {
    //         $msg = "Déja inscrits :";
    //         if (!empty($sendMail)) {
    //             $personmail["_id"] = (string) $account["_id"];
    //             $personmail["email"] = $account["email"];
    //             if (empty($account["roles"]["tobeactivated"]) || $account["roles"]["tobeactivated"] == false) {
    //                 if (!empty($invite)) {
    //                     if (!empty($isKissKiss)) {
    //                         Mail::inviteKKBB($personmail, false);
    //                     }
    //                 }
    //                 $msg .= "Compte déjà activé";
    //             } else {
    //                 if (!empty($invite)) {
    //                     if (empty($isKissKiss) && !empty($account["roles"]["tobeactivated"]) && $account["roles"]["tobeactivated"] == true) {
    //                         Mail::invitePerson($personmail, $person["msgInvite"], $person["nameInvitor"], $invitorUrl);
    //                     } elseif (!empty($isKissKiss)) {
    //                         Mail::inviteKKBB($personmail, true);
    //                     }
    //                 }
    //             }
    //         }

    //         if (!empty($person["badges"])) {
    //             $badges = Badge::conformeBadges($person["badges"]);
    //             $res = Badge::addAndUpdateBadges($badges, (string) $account["_id"], self::COLLECTION);
    //             $msg .= " " . $res["msg"];
    //         }

    //         if (!empty($paramsLink) && $paramsLink["link"] == true) {
    //             if ($paramsLink["typeLink"] == \Organization::class) {
    //                 if (Link::isLinked($paramsLink["idLink"], OrganizationInterface::COLLECTION, $personmail["_id"]) == false) {
    //                     Link::connect($paramsLink["idLink"], OrganizationInterface::COLLECTION, $personmail["_id"], self::COLLECTION, $this->session->getUserId(), "members", false);
    //                     Link::connect($personmail["_id"], self::COLLECTION, $paramsLink["idLink"], OrganizationInterface::COLLECTION, $this->session->getUserId(), "memberOf", false);
    //                     //Link::addMember($paramsLink["idLink"], OrganizationInterface::COLLECTION, $personmail["_id"], self::COLLECTION, $this->session->getUserId(), $paramsLink["isAdmin"]);
    //                 }

    //                 //Link::addMember($paramsLink["idLink"], OrganizationInterface::COLLECTION, $newpersonId, self::COLLECTION, $this->session->getUserId(), $paramsLink["isAdmin"]);
    //             }
    //         }

    //         /*if(!empty($person["source"]["key"])){
    //             //var_dump($person["source"]["key"]);
    //             $res = Import::addAndUpdateSourceKey($person["source"]["key"], (String)$account["_id"], self::COLLECTION);
    //             $msg +=" "+$res["msg"];
    //         }*/

    //         return array("result" => true, "msg" => $msg, "id" => $personmail["_id"]);
    //     } else {
    //         $newPerson = $this->getAndCheckPersonFromImportData($person, $invite, null, null, $warnings);

    //         if (!empty($newPerson["warnings"]) && $warnings == true) {
    //             $newPerson["warnings"] = Import::getAndCheckWarnings($newPerson["warnings"]);
    //         }

    //         $newPerson["@context"] = array(
    //             "@vocab" => "http://schema.org",
    //             "ph" => "http://pixelhumain.com/ph/ontology/",
    //         );
    //         $newPerson["roles"] = $this->getRole()->getDefaultRoles();
    //         $newPerson["created"] = $this->db->MongoDate(time());
    //         $newPerson["preferences"] = array("seeExplanations" => true);

    //         if (!empty($newPerson["image"])) {
    //             $nameImage = $newPerson["image"];
    //             unset($newPerson["image"]);
    //         }

    //         if (!empty($invite)) {
    //             $msgMail = $person["msgInvite"];
    //             $nameInvitor = (empty($person["nameInvitor"]) ? "Communecter" : $person["nameInvitor"]);
    //             $newPerson["roles"]['betaTester'] = true;
    //             $newPerson["pending"] = true;
    //             $newPerson["numberOfInvit"] = 10;
    //             unset($newPerson["msgInvite"]);
    //             unset($newPerson["nameInvitor"]);
    //         }

    //         $newPerson = $this->db->insert(self::COLLECTION, $newPerson);

    //         if (isset($newPerson["_id"])) {
    //             $newpersonId = (string) $newPerson["_id"];
    //         } else {
    //             throw new CTKException("Problem inserting the new person");
    //         }

    //         if (!empty($nameImage)) {
    //             try {
    //                 $res = Document::uploadDocumentFromURL($moduleId, self::COLLECTION, $newpersonId, "avatar", false, $pathFolderImage, $nameImage);
    //                 if (!empty($res["result"]) && $res["result"] == true) {
    //                     $params = array();
    //                     $params['id'] = $newpersonId;
    //                     $params['type'] = self::COLLECTION;
    //                     $params['moduleId'] = $moduleId;
    //                     $params['folder'] = self::COLLECTION . "/" . $newpersonId;
    //                     $params['name'] = $res['name'];
    //                     $params['author'] = $this->session->getUserId();
    //                     $params['size'] = $res["size"];
    //                     $params["contentKey"] = "profil";
    //                     $res2 = Document::save($params);
    //                     if ($res2["result"] == false) {
    //                         throw new CTKException("Impossible de save.");
    //                     }
    //                 } else {
    //                     throw new CTKException("Impossible uploader le document.");
    //                 }
    //             } catch (CTKException $e) {
    //                 throw new CTKException($e);
    //             }
    //         }


    //         if (!empty($paramsLink) && $paramsLink["link"] == true) {
    //             if ($paramsLink["typeLink"] == \Organization::class) {
    //                 if (Link::isLinked($paramsLink["idLink"], OrganizationInterface::COLLECTION, $newpersonId) == false) {
    //                     Link::connect($paramsLink["idLink"], OrganizationInterface::COLLECTION, $newpersonId, self::COLLECTION, $this->session->getUserId(), "members", false);
    //                     Link::connect($newpersonId, self::COLLECTION, $paramsLink["idLink"], OrganizationInterface::COLLECTION, $this->session->getUserId(), "memberOf", false);
    //                     //Link::addMember($paramsLink["idLink"], OrganizationInterface::COLLECTION, $newpersonId, self::COLLECTION, $this->session->getUserId(), $paramsLink["isAdmin"]);
    //                 }
    //             }
    //             if ($paramsLink["typeLink"] == \self::class) {
    //                 //Link::connect($newOrganizationId, OrganizationInterface::COLLECTION, $paramsLink["idLink"], self::COLLECTION, $creatorId,"members",$paramsLink["isAdmin"]);
    //                 //Link::connect($paramsLink["idLink"], self::COLLECTION, $newOrganizationId, OrganizationInterface::COLLECTION, $creatorId,"memberOf",$paramsLink["isAdmin"]);
    //                 //Link::addMember($newOrganizationId, OrganizationInterface::COLLECTION, $paramsLink["idLink"], self::COLLECTION, $creatorId, $paramsLink["isAdmin"]);
    //             }
    //         }
    //         if (!empty($sendMail)) {
    //             if (!empty($invite)) {
    //                 if (empty($isKissKiss)) {
    //                     Mail::invitePerson($newPerson, $msgMail, $nameInvitor, $invitorUrl);
    //                 } else {
    //                     Mail::inviteKKBB($newPerson, true);
    //                 }
    //             }
    //         }
    //         return array("result" => true, "msg" => "Cette personne est communecté.", "id" => $newPerson["_id"]);
    //     }
    // }



    // todo : je crois que ce n'est plus utilisé, à supprimer ?
    // public function generedUserNameByEmail($chaine, $isEmail = null)
    // {
    //     if ($isEmail == true) {
    //         $arrayEmail = explode("@", $chaine);
    //         $name = $arrayEmail[0];
    //     } else {
    //         $name = $chaine;
    //     }
    //     $name = strtr($name, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ._', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY--'); // Replaces all spaces with hyphens.
    //     $name = preg_replace('/[^A-Za-z0-9\-]/', '', $name);

    //     if (strlen($name) >= 4 && strlen($name) <= 20) {
    //         if (!$this->isUniqueUsername($name)) {
    //             $name = $this->generedUserNameByEmail($name . "1");
    //         }
    //     } else {
    //         if (strlen($name) < 4) {
    //             while (strlen($name) < 4) {
    //                 $name = $name . "1";
    //             }
    //         }

    //         if (strlen($name) > 20) {
    //             $name = substr($name, 0, 20);
    //         }
    //     }
    //     return $name;
    // }

    /**
     * Clear attributes based on the confidentiality level of the entity.
     *
     * @param array|null $entity The entity to check the confidentiality level for.
     * @return array|null The entity with the attributes cleared, or null if the entity is not valid.
     */
    public function clearAttributesByConfidentiality(?array $entity = null): ?array
    {
        //si l'entité n'est pas valable on ne fait rien
        if (! isset($entity) || $entity == null) {
            return $entity;
        }

        //recupere l'id de l'entité (2 cas possibles)
        $id = $entity['$id'] ?? "";
        if ($id == "") {
            $id = $entity['_id'] ?? "";
        }
        if ($id == "") {
            $id = $entity['id'] ?? "";
        }
        if ($id == "") {
            return $entity;
        }

        $entity = $this->getModelPreference()->clearByPreference($entity, PersonInterface::COLLECTION, (string) $this->session->getUserId());

        if (! empty($entity["pwd"])) {
            unset($entity["pwd"]);
        }
        return $entity;
    }

    /**
     * Retrieves the email of a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array|null The email of the person, or null if not found.
     */
    public function getEmailById(string $id): ?array
    {
        $person = $this->db->findOneById(PersonInterface::COLLECTION, $id, [
            "email" => 1,
        ]);
        return $person;
    }

    /**
     * Retrieves the roles of a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array|null The roles of the person.
     */
    public function getRolesById(string $id): ?array
    {
        return $this->db->findOneById(PersonInterface::COLLECTION, $id, [
            "roles" => 1,
        ]);
    }

    /**
     * Update the role of a person.
     *
     * @param string $userId The ID of the user.
     * @param string $mongoAction The action to perform on the MongoDB.
     * @param string $role The role to update.
     * @param mixed $roleValue The new value for the role.
     * @return array The result of the update.
     */
    public function updatePersonRole(string $userId, string $mongoAction, string $role, $roleValue): array
    {
        return $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($userId),
            ],
            [
                $mongoAction => [
                    'roles.' . $role => $roleValue,
                ],
            ]
        );
    }

    // todo : je vois la fonction dans l'action UpdateWithJsonAction, est ce que l'action est encore utilisée ?
    // public function updateWithJson($json)
    // {
    //     $data = json_decode($json, true);
    //     $user = $this->getById($this->session->getUserId());
    //     $res = array();
    //     if (!empty($data["identity"]["name"]) && $data["identity"]["name"] != $user["name"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "name", $data["identity"]["name"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["email"]) && $data["identity"]["email"] != $user["email"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "email", $data["identity"]["email"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["username"]) && $data["identity"]["username"] != $user["username"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "username", $data["identity"]["username"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["geo"]) && $data["identity"]["geo"] != $user["geo"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "geo", $data["identity"]["geo"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["address"]) && $data["identity"]["address"] != $user["address"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "address", $data["identity"]["address"], $this->session->getUserId());
    //     }

    //     /*if(!empty($data["identity"]["birthDate"]) && $data["identity"]["birthDate"] != $user["birthDate"]){
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "birthDate", $data["identity"]["birthDate"], $this->session->getUserId());
    //     }*/

    //     if (!empty($data["identity"]["telephone"]) && $data["identity"]["telephone"] != $user["telephone"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "telephone", $data["identity"]["telephone"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["tags"]) && $data["identity"]["tags"] != $user["tags"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "tags", $data["identity"]["tags"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["shortDescription"]) && $data["identity"]["shortDescription"] != $user["shortDescription"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "shortDescription", $data["identity"]["shortDescription"], $this->session->getUserId());
    //     }

    //     if (!empty($data["identity"]["socialNetwork"]) && $data["identity"]["socialNetwork"] != $user["socialNetwork"]) {
    //         $res[] = $this->updatePersonField($this->session->getUserId(), "socialNetwork", $data["identity"]["socialNetwork"], $this->session->getUserId());
    //     }

    //     return $res;
    // }

    /**
     * Retrieves a pending user by email.
     *
     * @param string|null $email The email of the pending user.
     * @return string|null The ID of the pending user, or null if not found.
     */
    public function getPendingUserByEmail(?string $email): ?string
    {
        if (empty($email)) {
            throw new CTKException("Please fill the email of the user");
        }

        $account = $this->getPersonByEmail($email);
        return ! empty($account["pending"]) ? (string) $account["_id"] : null;
    }

    /**
     * Returns the data binding model.
     *
     * @return array The data binding configuration.
     */
    public function getDataBinding(): array
    {
        return $this->dataBinding;
    }

    /**
     * Deletes a person.
     *
     * @param string $id The ID of the person to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @param bool|null $tobeactivated Optional. Specifies whether the person should be marked as to be activated.
     * @return array An array containing the result of the deletion operation.
     */
    public function deletePerson(string $id, string $userId, ?bool $tobeactivated = null): array
    {
        //Only super admin can delete a person or user himself
        if (! $this->getModelAuthorisation()->isUserSuperAdmin($userId) && $id != $userId && $tobeactivated == null) {
            return [
                "result" => false,
                "msg" => "You must be a superadmin to delete a person",
            ];
        }

        //$person = $this->getById($id);
        $person = $this->db->find(PersonInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
        if (empty($person)) {
            return [
                "result" => false,
                "msg" => "Unknown person id",
            ];
        }

        //Delete email
        $this->db->remove(CronInterface::COLLECTION, [
            "to" => $person["email"],
        ]);

        //Delete links on elements collections
        $links2collection = [
            //Person => Person that follows the user we want to delete and the
            PersonInterface::COLLECTION => ["follows", "followers"],
            //Organization => members, followers
            OrganizationInterface::COLLECTION => ["followers", "members"],
            //Projects => contibutors
            ProjectInterface::COLLECTION => ["contributors", "followers"],
            //Events => attendees / organizer
            EventInterface::COLLECTION => ["attendees", "organizer"],
            //Form => links/members
            FormInterface::COLLECTION => ["members"],
        ];

        foreach ($links2collection as $collection => $linkTypes) {
            foreach ($linkTypes as $linkType) {
                $where = [
                    "links." . $linkType . "." . $id => [
                        '$exists' => true,
                    ],
                ];
                $action = [
                    '$unset' => [
                        "links." . $linkType . "." . $id => "",
                    ],
                ];

                $elt = $this->db->find($collection, $where, ["name"]);

                foreach ($elt as $keyElt => $valueElt) {
                    $this->db->update($collection, [
                        "_id" => $keyElt,
                    ], $action);
                }
            }
        }

        //Delete Notifications
        $this->getModelActivityStream()->removeNotificationsByUser($id);

        $this->getModelSlug()->removeByParentIdAndType($id, PersonInterface::COLLECTION);

        if ($tobeactivated == null) {
            $paramsMail = [
                "tpl" => "deleted",
                "tplObject" => "[COmmunecter] Votre compte a été supprimer",
                "tplMail" => $person["email"],
                "name" => $person["name"],
            ];
            $this->getModelMail()->createAndSend($paramsMail);
        }

        //Check if the user got activity (news, comments, votes)
        $res = $this->checkActivity($id);
        if (! empty($res["result"])) {
            //Anonymize the user : Remove all fields from the person
            $where = [
                "_id" => $this->db->MongoId($id),
            ];
            $action = [
                "username" => $id,
                "email" => $id . "@communecter.org",
                "name" => "Citoyen supprimé",
                "deletedDate" => $this->db->MongoDate(time()),
                "status" => "deleted",
            ];
            $this->db->update(PersonInterface::COLLECTION, $where, $action);
            $this->getModelLog()->save([
                "userId" => $userId,
                "browser" => $_SERVER["HTTP_USER_AGENT"] ?? null,
                "ipAddress" => $_SERVER["REMOTE_ADDR"] ?? null,
                "created" => $this->db->MongoDate(time()),
                "action" => "deleteUser",
                "params" => [
                    "id" => $id,
                ],
            ]);
        } else {
            //Delete the person
            $where = [
                "_id" => $this->db->MongoId($id),
            ];
            $this->db->remove(PersonInterface::COLLECTION, $where);
        }

        //Documents => Profil Images
        $docType = [DocumentInterface::IMG_PROFIL, DocumentInterface::IMG_BANNER, DocumentInterface::IMG_SLIDER, DocumentInterface::IMG_SLIDER];
        $profilImages = $this->getModelDocument()->listMyDocumentByIdAndType($id, PersonInterface::COLLECTION, $docType, DocumentInterface::DOC_TYPE_IMAGE, [
            'created' => -1,
        ]);

        foreach ($profilImages as $docId => $document) {
            $this->getModelDocument()->removeDocumentById($docId, $userId);
        }

        if ($id == $userId) {
            self::clearUserSessionData();
        }

        return [
            "result" => true,
            "msg" => "The person has been deleted succesfully",
        ];
    }

    // private function checkActivity($id)
    // {
    //     //Check if the person got news/comments/votes
    //     //Comments => author
    //     $where = array("author" => $id);
    //     $count = $this->db->count(CommentInterface::COLLECTION, $where);
    //     if ($count > 0) {
    //         return array("result" => true, "msg" => "This person had made comments");
    //     }
    //     //news => author ou target (type = citoyens && id = $id) ou mentions.id contient $id
    //     $where = array(
    //         '$or' => array(
    //             array("author" => $id),
    //             array("mentions.id" => $id),
    //             array(
    //                 '$and' => array(
    //                     array("target.type" => self::COLLECTION),
    //                     array("target.id" => $id),
    //                 ),
    //             ),
    //         ),
    //     );
    //     $count = $this->db->count(NewsInterface::COLLECTION, $where);
    //     if ($count > 0) {
    //         return array("result" => true, "msg" => "This person had made news");
    //     }
    //     //surveys => VoteUp, VoteMoreInfo, VoteDown...
    //     $where = array(
    //         '$or' => array(
    //             array("vote." . ActionInterface::ACTION_VOTE_UP . "." . $id => array('$exists' => 1)),
    //             array("vote." . ActionInterface::ACTION_VOTE_ABSTAIN . "." . $id => array('$exists' => 1)),
    //             array("vote." . ActionInterface::ACTION_VOTE_UNCLEAR . "." . $id => array('$exists' => 1)),
    //             array("vote." . ActionInterface::ACTION_VOTE_MOREINFO . "." . $id => array('$exists' => 1)),
    //             array("vote." . ActionInterface::ACTION_VOTE_DOWN . "." . $id => array('$exists' => 1)),
    //         ),
    //     );
    //     $count = $this->db->count(SurveyInterface::COLLECTION, $where);
    //     if ($count > 0) {
    //         return array("result" => true, "msg" => "This person had made votes");
    //     }

    //     return array("result" => false, "msg" => "No activity");
    // }

    /**
     * Update the cookie communexion for a person.
     *
     * @param string|null $userId The user ID.
     * @param array|null $address The address array.
     * @return array The updated cookie communexion.
     */
    public function updateCookieCommunexion(?string $userId, ?array $address): array
    {
        $result = [
            "result" => false,
            "msg" => "User not connected",
        ];
        if (! empty($userId)) {
            try {
                if (! empty($address)) {
                } else {
                    $this->cookieHelper->setCookie("communexionType", null);
                    $this->cookieHelper->setCookie("communexion", null);
                    $this->cookieHelper->setCookie("communexionActivated", false);
                }

                $result = [
                    "result" => true,
                    "msg" => "Cookies is updated",
                ];
            } catch (CTKException $e) {
                $result = [
                    "result" => false,
                    "msg" => $e->getMessage(),
                ];
            }
        }

        return $result;
    }


    // /*public function removeCookie($person){
    //     if(!empty($person["removeCookie"]) && $person["removeCookie"] == true ){
    //         $this->cookieHelper->removeCookie("communexionType");
    //         $this->cookieHelper->removeCookie("communexionValue");
    //         $this->cookieHelper->removeCookie("communexionName");
    //         $this->cookieHelper->removeCookie("communexionLevel");
    //         $this->cookieHelper->removeCookie("inseeCommunexion");
    //         $this->cookieHelper->removeCookie("cpCommunexion");
    //         $this->cookieHelper->removeCookie("cityNameCommunexion");
    //     }
    // }*/

    // public function getCurrentSuperAdmins()
    // {
    //     $superAdmins = array();
    //     $superAdmins = $this->db->find(self::COLLECTION, array('roles.superAdmin' => true), array("_id"));
    //     return $superAdmins;
    // }


    // public function updateScopeInter($id)
    // {
    //     $res = array("result" => false, "msg" => "You are not connected");
    //     $find = $this->getById($id);

    //     if (!empty($find)) {
    //         $res = $this->db->update(
    //             self::COLLECTION,
    //             array("_id" => $this->db->MongoId($id)),
    //             array('$unset' => array("inter" => null))
    //         );

    //         $res = array("result" => true, "msg" => "Ce message ne s'affichera plus");
    //     }

    //     return $res;
    // }


    // public function getCommunexion($address)
    // {
    //     $res = array();


    //     if (!empty($address) && !empty($address["localityId"])) {
    //         $res = $address;

    //         $city = City::getById($address["localityId"]);
    //         $res["type"] = "city";
    //         $res["id"] = $address["localityId"];
    //         $res["city"] = $address["localityId"];
    //         if (!empty($city["postalCodes"])) {
    //             $res["postalCodes"] = $city["postalCodes"];
    //         }

    //         if (!empty($address["addressCountry"])) {
    //             $res["country"] = $address["addressCountry"];
    //         }

    //         if (!empty($address["addressCountry"])) {
    //             $res["name"] = $address["addressLocality"];
    //         }

    //         $res["cityName"] = City::getNameCity($res["localityId"]);

    //         unset($res["@type"]);
    //         unset($res["codeInsee"]);
    //         unset($res["addressCountry"]);
    //         unset($res["addressLocality"]);
    //         unset($res["streetAddress"]);


    //         if (!empty($res["postalCodes"]) && (is_countable($res["postalCodes"]) ? count($res["postalCodes"]) : 0) == 1) {
    //             $res["allCP"] = true;

    //             if (!empty($res["postalCodes"][0]["postalCode"])) {
    //                 $where = array(
    //                     "country" => $res["country"],
    //                     "postalCodes.postalCode" => $res["postalCodes"][0]["postalCode"],
    //                 );
    //                 $countCP = $this->db->count(CityInterface::COLLECTION, $where);
    //                 $res["uniqueCp"] = (($countCP > 1) ? false : true);
    //             }
    //         } else {
    //             $res["allCP"] = false;
    //         }
    //     }

    //     return $res;
    // }

    // /**
    //  * Generates a random password.
    //  *
    //  * @param int $length The length of the password. Default is 8.
    //  * @return string The randomly generated password.
    //  */
    // public function random_password($length = 8)
    // {
    //     $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    //     $password = substr(str_shuffle($chars), 0, $length);
    //     return $password;
    // }
}
