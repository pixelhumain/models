<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;


use PixelHumain\Models\Traits\ActionsTrait;
use PixelHumain\Models\Traits\ActionTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ActionsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;

use PixelHumain\Models\Traits\Interfaces\SurveyTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\NotificationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\SurveyTrait;

class ActionRoom extends BaseModel implements ActionRoomInterface, AuthorisationTraitInterface, CommentTraitInterface, SurveyTraitInterface, ElementTraitInterface, NotificationTraitInterface, LinkTraitInterface, PersonTraitInterface, ActionsTraitInterface, ActionTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AuthorisationTrait;
    use CommentTrait;
    use SurveyTrait;
    use ElementTrait;
    use NotificationTrait;
    use LinkTrait;
    use PersonTrait;
    use ActionsTrait;
    use ActionTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Retrieves an ActionRoom by its ID.
     *
     * @param string $id The ID of the ActionRoom.
     * @return array|null The ActionRoom data as an array or null if not found.
     */
    public function getById(string $id): ?array
    {
        return $this->db->findOne(ActionRoomInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data if found, null otherwise.
     */
    public function getActionById(string $id): ?array
    {
        return $this->db->findOne(ActionRoomInterface::COLLECTION_ACTIONS, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Retrieves records from the ActionRoom table based on the provided parameters, sorting, and limit.
     *
     * @param array $params The parameters to filter the records.
     * @param array $sort The sorting criteria for the records.
     * @param int $limit The maximum number of records to retrieve. Default is 1.
     * @return array The retrieved records.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array
    {
        return $this->db->findAndSort(ActionRoomInterface::COLLECTION, $params, $sort, $limit);
    }

    /**
     * Retrieves a single action room by its parent organization.
     *
     * @param string $idOrga The ID of the parent organization.
     * @return array|null The action room data as an array or null if not found.
     */
    public function getSingleActionRoomByOrgaParent(string $idOrga): ?array
    {
        return $this->db->findOne(
            ActionRoomInterface::COLLECTION,
            [
                "parentId" => $idOrga,
            ]
        );
    }

    /**
     * Check if a user can participate in an action room.
     *
     * @param string $userId The ID of the user.
     * @param string|null $id The ID of the action room (optional).
     * @param string|null $type The type of the action room (optional).
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, ?string $id = null, ?string $type = null): bool
    {
        $showAddBtn = false;
        if (($type == OrganizationInterface::COLLECTION && $this->getModelAuthorisation()->isOrganizationMember($userId, $id))
            || ($type == ProjectInterface::COLLECTION && $this->getModelAuthorisation()->isProjectMember($userId, $id))
            || ($type == EventInterface::COLLECTION && $this->getModelAuthorisation()->isEventMember($userId, $id))) {
            $showAddBtn = true;
        }
        return $showAddBtn;
    }

    // TODO : check if this function is used
    /**
     * Check if a user is a moderator in a specific app.
     *
     * @param string|null $userId The user ID.
     * @param string $app The app name.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $app): bool
    {
        $appResult = $this->db->findOne(PHType::TYPE_APPLICATIONS, [
            "key" => $app,
        ]);
        $res = false;
        if (! empty($appResult["moderator"]) && is_array($appResult["moderator"])) {
            $res = (isset($userId) && in_array($this->session->getUserId(), $appResult["moderator"])) ? true : false;
        }
        return $res;
    }

    /**
     * Check if a user can administrate a specific action room.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the action room.
     * @return bool Returns true if the user can administrate the action room, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool
    {
        $actionRoom = $this->getById($id);

        $parentId = $actionRoom["parentId"] ?? null;
        $parentType = $actionRoom["parentType"] ?? null;

        $isAdmin = false;
        if ($parentType == OrganizationInterface::COLLECTION || $parentType == ProjectInterface::COLLECTION || $parentType == EventInterface::COLLECTION) {
            $isAdmin = $this->getModelAuthorisation()->canDeleteElement($parentId, $parentType, $userId);
        }
        return $isAdmin;
    }

    /**
     * Insert a new action room.
     *
     * @param array $parentRoom The parent room.
     * @param array $type The type of the action room.
     * @param mixed $copyOf The room to copy from (optional).
     * @return mixed The new action room data.
     */
    public function insert(array $parentRoom, array $type, $copyOf = null)
    {
        $newInfos = [];
        $newInfos['email'] = $this->session->get('userEmail');
        $newInfos['name'] = $parentRoom['name'];
        $newInfos['type'] = $type;
        if (@$copyOf) {
            $newInfos['copyOf'] = $copyOf;
        }
        $newInfos['parentType'] = $parentRoom['parentType'];
        $newInfos['parentId'] = $parentRoom['parentId'];
        if ((is_countable(@$parentRoom['tags']) ? count(@$parentRoom['tags']) : 0) > 0) {
            $newInfos['tags'] = $parentRoom['tags'];
        }
        $newInfos['created'] = time();
        $newInfos = $this->db->insert(ActionRoomInterface::COLLECTION, $newInfos);
        return $newInfos;
    }

    public function deleteActionRoom(string $id, string $userId): array
    {
        $res = [
            "result" => false,
            "msg" => "Something went wrong : contact your admin !",
        ];
        ;

        $actionRoom = $this->getById($id);
        if (empty($actionRoom)) {
            return [
                "result" => false,
                "The action room does not exist",
            ];
        }

        if (! $this->canAdministrate($userId, $id)) {
            return [
                "result" => false,
                "msg" => "You must be admin of the parent of this room if you want delete it",
            ];
        }
        //Remove actionRoom of type discuss : remove all comments linked
        if (! empty($actionRoom["type"]) && $actionRoom["type"] == ActionRoomInterface::TYPE_DISCUSS) {
            $resChildren = $this->getModelComment()->deleteAllContextComments($id, ActionRoomInterface::COLLECTION, $userId);
            //Remove actionRoom of type vote : remove all survey linked
        } elseif (! empty($actionRoom["type"]) && $actionRoom["type"] == ActionRoomInterface::TYPE_VOTE) {
            //Delete all surveys of this action room
            $resChildren = $this->getModelSurvey()->deleteAllSurveyOfTheRoom($id, $userId);
        } elseif (! empty($actionRoom["type"]) && $actionRoom["type"] == ActionRoomInterface::TYPE_ACTIONS) {
            //Delete all actions of this action room
            $resChildren = $this->getModelActions()->deleteAllActionsOfTheRoom($id, $userId);
        } else {
            if (! empty($actionRoom["type"])) {
                $resChildren = [
                    "result" => false,
                    "msg" => "The delete of this type of action room '" . (string) $actionRoom["type"] . "' is not yet implemented.",
                ];
            } else {
                $resChildren = [
                    "result" => false,
                    "msg" => "The delete of this type of action room is not yet implemented.",
                ];
            }
        }

        if (isset($resChildren["result"]) && ! $resChildren["result"]) {
            return (array) $resChildren;
        }

        //Remove the action room
        if ($this->db->remove(ActionRoomInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ])) {
            $res = [
                "result" => true,
                "msg" => "The action room has been deleted with success",
            ];
        }

        return $res;
    }

    // TODO : check if this function is used
    /**
     * Deletes the action rooms associated with a specific element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user performing the action.
     * @return array An array containing the deleted action rooms.
     */
    public function deleteElementActionRooms(string $elementId, string $elementType, string $userId): array
    {
        //Check if the $userId can delete the element
        $canDelete = $this->getModelAuthorisation()->canDeleteElement($elementId, $elementType, $userId);
        if (! $canDelete) {
            return [
                "result" => false,
                "msg" => "You do not have enough credential to delete this element rooms.",
            ];
        }

        //get all actions
        $actionRooms2delete = $this->getElementActionRooms($elementId, $elementType);
        $nbActionRoom = 0;
        foreach ($actionRooms2delete as $id => $anActionRoom) {
            $resDeleteActionRoom = $this->deleteActionRoom($id, $userId);
            if ($resDeleteActionRoom["result"] == false) {
                // error_log("Error during the process try to delete the action room " . $id . " : " . $resDeleteActionRoom["msg"]);
                return $resDeleteActionRoom;
            }
            $nbActionRoom++;
        }

        return [
            "result" => true,
            "msg" => $nbActionRoom . " actionRoom of the element " . $elementId . " of type " . $elementType . " have been removed with succes.",
        ];
    }

    /**
     * Closes the action.
     *
     * @param array $params The parameters for closing the action.
     * @return array The updated array after closing the action.
     */
    public function closeAction(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(ActionRoomInterface::COLLECTION_ACTIONS, [
                "_id" => $this->db->MongoId($params["id"]),
            ])) {
                if ($this->session->get("userEmail") == $action["email"]) {
                    //then remove the parent survey
                    $status = (@$action["status"] == ActionRoomInterface::ACTION_CLOSED) ? ActionRoomInterface::ACTION_INPROGRESS : ActionRoomInterface::ACTION_CLOSED;
                    $this->db->update(
                        ActionRoomInterface::COLLECTION_ACTIONS,
                        [
                            "_id" => $action["_id"],
                        ],
                        [
                            '$set' => [
                                "status" => $status,
                            ],
                        ]
                    );
                    $this->getModelAction()->updateParent($_POST['id'], ActionRoomInterface::COLLECTION_ACTIONS);
                    $res["result"] = true;
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    //TODO :  $_POST
    /**
     * Assigns the current user to the action room.
     *
     * @param array $params The parameters for the assignment.
     * @return array The updated action room details.
     */
    public function assignMe(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(ActionRoomInterface::COLLECTION_ACTIONS, [
                "_id" => $this->db->MongoId($params["id"]),
            ])) {
                if ($this->getModelAuthorisation()->canParticipate($this->session->getUserId(), $action["parentType"], $action["parentId"])) {
                    $res = $this->getModelLink()->connect($params["id"], ActionRoomInterface::COLLECTION_ACTIONS, $this->session->getUserId(), PersonInterface::COLLECTION, $this->session->getUserId(), "contributors", true);
                    $this->getModelAction()->updateParent($_POST['id'], ActionRoomInterface::COLLECTION_ACTIONS);
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    /**
     * Assigns people to the action room.
     *
     * @param array $params The parameters for assigning people.
     * @return array The updated array after assigning people.
     */
    public function assignPeople(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(ActionRoomInterface::COLLECTION_ACTIONS, [
                "_id" => $this->db->MongoId($params["id"]),
            ])) {
                if ($this->getModelAuthorisation()->canParticipate($params["child"]["id"], $action["parentType"], $action["parentId"])) {
                    $res = $this->getModelLink()->connect($params["id"], ActionRoomInterface::COLLECTION_ACTIONS, $params["child"]["id"], PersonInterface::COLLECTION, $this->session->getUserId(), "contributors", true);

                    $res["newElement"] = $this->db->findOne(PersonInterface::COLLECTION, [
                        "_id" => $this->db->MongoId($params["child"]["id"]),
                    ], ["name"]);
                    $res["parentType"] = ActionRoomInterface::COLLECTION_ACTIONS;
                    $res["parent"] = $action;
                    $res["newElementType"] = PersonInterface::COLLECTION;
                    $this->getModelAction()->updateParent($params['id'], ActionRoomInterface::COLLECTION_ACTIONS);

                    $pendingChild = $this->getModelElement()->getElementSimpleById($params["child"]["id"], $params["child"]["type"], null, ["_id", "name", "profilThumbImageUrl"]);
                    $levelNotif = ActionRoomInterface::COLLECTION_ACTIONS;
                    $typeOfDemand = "assign";
                    $valNotif = [
                        "typeOfDemand" => $typeOfDemand,
                        "verb" => ActStrInterface::VERB_INVITE,
                    ];
                    $this->getModelNotification()->constructNotification(
                        ActStrInterface::VERB_INVITE,
                        $pendingChild,
                        [
                            "type" => ActionRoomInterface::COLLECTION_ACTIONS,
                            "id" => $params["id"],
                            "name" => $action["name"],
                        ],
                        null,
                        $levelNotif,
                        null,
                        $valNotif
                    );
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    /**
     * Récupère toutes les salles en fonction du type et de l'identifiant.
     *
     * @param string $type Le type de la salle.
     * @param string $id L'identifiant de la salle.
     * @param bool|null $archived (optionnel) Indique si les salles archivées doivent être incluses.
     * @return array Les salles correspondantes.
     */
    public function getAllRoomsByTypeId(string $type, string $id, ?bool $archived = null): array
    {
        $where = [
            "created" => [
                '$exists' => 1,
            ],
        ];
        $where["status"] = ($archived) ? ActionRoomInterface::STATE_ARCHIVED : [
            '$exists' => 0,
        ];

        if (! empty($type)) {
            $where["parentType"] = $type;
        }
        if (! empty($id)) {
            $where["parentId"] = $id;
        }

        if ($type == PersonInterface::COLLECTION) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonId($id, $archived);
        } elseif ($this->session->getUserId()) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonIdByType($this->session->getUserId(), $type, $id, $archived);
        } else {
            $rooms = $this->getWhereSortLimit($where, [
                "date" => 1,
            ], 0);
        }

        $actionHistory = [];
        if (isset($roomsActions) && isset($roomsActions["rooms"]) && isset($roomsActions["actions"])) {
            $rooms = $roomsActions["rooms"];
            $actionHistory = $roomsActions["actions"];
        }

        $discussions = [];
        $votes = [];
        $actions = [];
        foreach ($rooms as $e) {
            if (in_array($e["type"], [ActionRoomInterface::TYPE_DISCUSS, ActionRoomInterface::TYPE_FRAMAPAD])) {
                array_push($discussions, $e);
            } elseif ($e["type"] == ActionRoomInterface::TYPE_VOTE) {
                array_push($votes, $e);
            } elseif ($e["type"] == ActionRoomInterface::TYPE_ACTIONS) {
                array_push($actions, $e);
            }
        }
        $params = [
            "discussions" => $discussions,
            "votes" => $votes,
            "actions" => $actions,
            "history" => $actionHistory,
        ];
        return $params;
    }

    // TODO : check if this function is used
    /**
     * Récupère toutes les activités des salles par type et identifiant.
     *
     * @param string $type Le type de salle.
     * @param string $id L'identifiant de la salle.
     * @param bool|null $archived (optionnel) Indique si les activités archivées doivent être incluses.
     * @return array Les activités des salles correspondantes.
     */
    public function getAllRoomsActivityByTypeId(string $type, string $id, ?bool $archived = null): array
    {
        $where = [
            "created" => [
                '$exists' => 1,
            ],
        ];
        $where["status"] = ($archived) ? ActionRoomInterface::STATE_ARCHIVED : [
            '$exists' => 0,
        ];

        if (! empty($type)) {
            $where["parentType"] = $type;
        }
        if (! empty($id)) {
            $where["parentId"] = $id;
        }

        if ($type == PersonInterface::COLLECTION) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonId($id, $archived);
        } elseif ($this->session->getUserId()) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonIdByType($this->session->getUserId(), $type, $id, $archived);
        } else {
            $rooms = $this->getWhereSortLimit($where, [
                "date" => 1,
            ], 0);
        }

        if (isset($roomsActions) && isset($roomsActions["rooms"]) && isset($roomsActions["actions"])) {
            $rooms = $roomsActions["rooms"];
        }

        $discussions = [];
        $proposals = [];
        $actions = [];
        foreach ($rooms as $e) {
            if (in_array($e["type"], [ActionRoomInterface::TYPE_DISCUSS, ActionRoomInterface::TYPE_FRAMAPAD])) {
                //ordonner par updated
                array_push($discussions, $e);
            } elseif ($e["type"] == ActionRoomInterface::TYPE_VOTE) {
                //get all survey for this room by sorting
                $surveys = $this->db->findAndSort(SurveyInterface::COLLECTION, [
                    "survey" => (string) $e["_id"],
                    "updated" => [
                        '$exists' => 1,
                    ],
                ], [
                    "updated" => 1,
                ], 10);
                foreach ($surveys as $s) {
                    array_push($proposals, $s);
                }
            } elseif ($e["type"] == ActionRoomInterface::TYPE_ACTIONS) {
                //get all survey for this room by sorting
                $actionElements = $this->db->findAndSort(ActionRoomInterface::TYPE_ACTIONS, [
                    "room" => (string) $e["_id"],
                    "updated" => [
                        '$exists' => 1,
                    ],
                ], [
                    "updated" => 1,
                ], 10);
                foreach ($actionElements as $a) {
                    array_push($actions, $a);
                }
            }
        }

        function mySort($a, $b)
        {
            if (isset($a['updated']) && isset($b['updated'])) {
                return (strtolower(@$b['updated']) > strtolower(@$a['updated']));
            } else {
                return false;
            }
        }

        $list = [...$discussions, ...$proposals, ...$actions];
        usort($list, "mySort");

        return $list;
    }

    /**
     * Récupère les ActionRooms d'un élément spécifié.
     *
     * @param string $elementId L'identifiant de l'élément.
     * @param string $elementType Le type de l'élément.
     * @return array Les ActionRooms de l'élément.
     */
    public function getElementActionRooms(string $elementId, string $elementType): array
    {
        $where = [
            '$and' => [[
                "parentType" => $elementType,
            ], [
                "parentId" => $elementId,
            ]],
        ];

        $actionRooms = $this->db->find(ActionRoomInterface::COLLECTION, $where);

        return $actionRooms;
    }
}
