<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ButtonCtkInterface;
use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;

// TODO : cette class ne devrait pas être dans les modeles
// TODO : Yii::get('module')->getModule("costum")->assetsUrl

class ButtonCtk extends BaseModel implements ButtonCtkInterface, CostumTraitInterface, AuthorisationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use AuthorisationTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateI18nProperty();
    }

    /**
     * Generates a search bar HTML element.
     *
     * @param array $params The parameters for the search bar.
     * @return string The generated search bar HTML element.
     */
    public function searchBar(array $params): string
    {
        $eventTargetInput = (! @$params["dropdownResult"]) ? "main-search-bar" : "second-search-bar";
        $eventTargetAddon = (! @$params["dropdownResult"]) ? "main-search-bar-addon" : "second-search-bar-addon";
        if (@$params["eventTargetInput"]) {
            $eventTargetInput;
        }
        if (@$params["eventTargetAddon"]) {
            $eventTargetAddon;
        }
        $classes = $params["classes"] ?? "hidden-xs col-sm-4 col-md-4";
        $placeholder = (@$params["placeholder"]) ? $this->language->t("common", $params["placeholder"]) : $this->language->t("common", "what are you looking for ?");
        $icon = (@$params["icon"]) ? $params["icon"] : "arrow-circle-right";
        $str = '<div class="' . $classes . '">';

        if (! empty($params["pull-right"]) && ["pull-right"] == true) {
            $str .= '<span class="text-white input-group-addon pull-right ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
                       '<i class="fa fa-' . $icon . '"></i>' .
                   '</span>' .
                   '<input type="text" class="form-control pull-right text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">';
        } else {
            $str .= '<input type="text" class="form-control pull-left text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">' .
                    '<span class="text-white input-group-addon pull-left ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
                        '<i class="fa fa-' . $icon . '"></i>' .
                    '</span>';
        }

        $str .= '<div class="dropdown-result-global-search hidden-xs col-sm-6 col-md-5 col-lg-5 no-padding" style="display:none;"></div>';
        $str .= "</div>";
        return $str;
    }

    /**
     * Méthode pour gérer l'application du bouton.
     *
     * @param array $params Les paramètres de l'application.
     * @return string Le résultat de l'application du bouton.
     */
    public function app(array $params): string
    {
        $costum = $this->getModelCostum()->getCostum();
        $pages = $costum["appConfig"]["pages"];

        $tooltips = $params["tooltips"] ?? false;
        $img = $params["img"] ?? false;
        $nameMenuTop = $params["nameMenuTop"] ?? false;
        $str = "";

        foreach ($pages as $key => $value) {
            if ($this->getModelAuthorisation()->accessMenuButton($value, "Top")) {
                $labelStr = $this->language->t("common", $value["subdomainName"]);
                $titleStr = $labelStr;
                if (empty($titleStr) && ! empty($value["placeholderMainSearch"])) {
                    $titleStr = $this->language->t("common", $value["placeholderMainSearch"]);
                }

                if (! empty($value["urlExtern"])) {
                    $str .= '<a href="' . $value["urlExtern"] . '"' .
                                'target="_blanc" ' .
                                'title="' . $titleStr . '" ' .
                                'class=" btn btn-link menu-button btn-menu text-dark btn-menu-tooltips">';
                } else {
                    $str .= '<button title="' . $titleStr . '" class="btn btn-link menu-button btn-menu lbh-menu-app text-dark btn-menu-tooltips" data-hash="' . $key . '">';
                }

                if (isset($img) && ! empty($value["img"])) {
                    $str .= '<img width="45px" height="45px" src="' . Yii::get('module')->getModule("costum")->assetsUrl . $value["img"] . '" class="imgMenuTop hidden-xs">' . '<h6 class="nameMenuTop hidden-xs">' . $value["nameMenuTop"] . '</h6>';
                } elseif (! empty($value["icon"])) {
                    $str .= '<i class="fa fa-' . $value["icon"] . '"></i>';
                }
                if (! empty($value["subdomainName"])) {
                    $str .= '<span>' . $labelStr . '</span>';
                }
                if ($tooltips) {
                    $str .= $this->buttonTooltips($tooltips, $labelStr);
                }
                if (! empty($value["urlExtern"])) {
                    $str .= "</a>";
                } else {
                    $str .= "</button>";
                }
            }
        }
        return $str;
    }

    /**
     * Generates a button with tooltips.
     *
     * @param string $pos The position of the tooltips.
     * @param string $label The label of the button.
     * @return string The generated button with tooltips.
     */
    public function buttonTooltips(string $pos, string $label): string
    {
        return '<span class="tooltips-' . $pos . '-menu-btn">' . $this->language->t("common", $label) . '</span>';
    }
}
