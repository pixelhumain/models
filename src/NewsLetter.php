<?php

namespace PixelHumain\Models;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CronInterface;

use PixelHumain\Models\Interfaces\NewsLetterInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\MailTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\MailTrait;

use PixelHumain\Models\Traits\PersonTrait;
use RuntimeException;

class NewsLetter extends BaseModel implements NewsLetterInterface, PersonTraitInterface, ElementTraitInterface, MailTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use ElementTrait;
    use MailTrait;

    protected ?CacheHelperInterface $cacheHelper = null;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();

        if (isset($this->cacheHelper)) {
            if (! $this->cacheHelper instanceof CacheHelperInterface) {
                throw new InvalidArgumentException("cacheHelper must be an instance of CacheHelperInterface");
            }
        }
    }

    /**
     * Sends a newsletter email.
     *
     * @param string $authorId The ID of the author sending the newsletter.
     * @param string $contextType The type of context for the newsletter (e.g., "article", "event").
     * @param string $contextId The ID of the context for the newsletter.
     * @param array $mail The email details (e.g., subject, body, recipients).
     * @return string The result of the email sending process.
     */
    public function send(string $authorId, string $contextType, string $contextId, array $mail): string
    {
        $author = $this->getModelPerson()->getById($authorId, false);

        $this->validateAuthor($authorId, $author);

        $context = $this->getModelElement()->getByTypeAndId($contextType, $contextId);

        $this->validateContext($context, $contextType, $contextId);

        [$communityEmails, $extenalEmails] = $this->prepareEmails($mail);

        //save newsletter
        $newsletterUUID = uniqid("newsletter_");

        $this->saveNewsletter($newsletterUUID, $authorId, $contextType, $contextId, $mail, $communityEmails, $extenalEmails);

        $this->scheduleEmails($mail, $communityEmails, $extenalEmails, $newsletterUUID);

        return $newsletterUUID;
    }

    /**
     * Validates the author of the newsletter.
     *
     * @param string $authorId The ID of the author.
     * @param array|null $author The author's information.
     * @return void
     */
    private function validateAuthor(string $authorId, ?array $author): void
    {
        if (! $author) {
            throw new Exception("Person with id '" . $authorId . "' not found");
        }
    }

    /**
     * Validates the context of the NewsLetter.
     *
     * @param array|null $context The context to validate.
     * @param string $contextType The type of the context.
     * @param string $contextId The ID of the context.
     * @return void
     */
    private function validateContext(?array $context, string $contextType, string $contextId): void
    {
        if (! $context) {
            throw new Exception("Element '" . $contextType . "' with id '" . $contextId . "' not found.");
        }
    }

    /**
     * Prepares the emails for sending.
     *
     * @param array $mail The array of emails to be prepared.
     * @return array The prepared emails.
     */
    private function prepareEmails(array $mail): array
    {
        $externalEmails = $mail["receivers"]["external"] ?? [];

        $communityIds = $mail["receivers"]["community"] ?? [];
        $communityMongoIds = array_map(fn ($id) => $this->db->MongoId($id), $communityIds);

        $communityEmails = isset($mail["receivers"]["community"])
            ? $this->getCommunityEmails($communityMongoIds)
            : [];

        return [...$communityEmails, ...$externalEmails];
    }

    /**
     * Retrieves the email addresses of the communities specified by their IDs.
     *
     * @param array $communityIds An array of community IDs.
     * @return array An array of email addresses.
     */
    private function getCommunityEmails(array $communityIds): array
    {
        $people = $this->getModelPerson()->getByArrayId($communityIds);
        return array_map(fn ($person) => $person["email"] ?? null, $people);
    }

    /**
     * Saves a newsletter.
     *
     * @param string $newsletterUUID The UUID of the newsletter.
     * @param string $authorId The ID of the author.
     * @param string $contextType The type of the context.
     * @param string $contextId The ID of the context.
     * @param array $mail The mail data.
     * @param array $communityEmails The community emails.
     * @param array $externalEmails The external emails.
     * @return void
     */
    private function saveNewsletter(string $newsletterUUID, string $authorId, string $contextType, string $contextId, array $mail, array $communityEmails, array $externalEmails): void
    {
        $newsletterData = [
            "uuid" => $newsletterUUID,
            "type" => NewsLetterInterface::TYPE_MAIL,
            "authorId" => $authorId,
            "context" => [
                "type" => $contextType,
                "id" => $contextId,
            ],
            "receivers" => [
                "community" => $communityEmails,
                "external" => $externalEmails,
            ],
            "subject" => $mail["subject"],
            "data" => $mail["content"],
        ];

        $this->db->insert(NewsLetterInterface::COLLECTION, $newsletterData);
    }

    /**
     * Schedule emails to be sent to the specified recipients.
     *
     * @param array $mail The email content.
     * @param array $communityEmails The emails of the community members.
     * @param array $externalEmails The external emails.
     * @param string $newsletterUUID The UUID of the newsletter.
     * @return void
     */
    private function scheduleEmails(array $mail, array $communityEmails, array $externalEmails, string $newsletterUUID): void
    {
        $allEmails = array_merge($communityEmails, $externalEmails);

        foreach ($allEmails as $receiver) {
            $mailParams = [
                "type" => CronInterface::TYPE_MAIL, // Assumons que 'Cron' est une classe gérant les tâches planifiées
                "tpl" => "newsletter",     // Nom du template d'email à utiliser
                "subject" => $mail["subject"],
                "from" => $mail["from"],   // L'expéditeur de l'email
                "to" => $receiver,         // Le destinataire de l'email
                "tplParams" => [
                    // Paramètres supplémentaires pour le template
                    "newsletter" => $newsletterUUID,
                ],
                // Ajouter d'autres paramètres requis par le système d'envoi d'emails
            ];

            // Planifier l'envoi de l'email
            $this->getModelMail()->schedule($mailParams);
        }
    }

    /**
     * Saves a template for a newsletter.
     *
     * @param string $authorId The ID of the author.
     * @param string $contextType The type of the context.
     * @param string $contextId The ID of the context.
     * @param array $template The template to be saved.
     * @return string The ID of the saved template.
     * @throws Exception If the author or the context is not found or if the template is invalid.
     */
    public function saveTemplate(string $authorId, string $contextType, string $contextId, array $template): string
    {
        $author = $this->getModelPerson()->getById($authorId, false);

        $this->validateAuthor($authorId, $author);

        $context = $this->getModelElement()->getByTypeAndId($contextType, $contextId);

        $this->validateContext($context, $contextType, $contextId);

        $this->validateTemplateParameter($template);

        $uuid = uniqid("newsletter_");

        $data = [
            "uuid" => $uuid,
            "type" => NewsLetterInterface::TYPE_TEMPLATE,
            "name" => $template["name"],
            "authorId" => $authorId,
            "context" => [
                "type" => $contextType,
                "id" => $contextId,
            ],
            "data" => $template["page"],
        ];

        if (isset($template["image"])) {
            $data["image"] = $template["image"];
        }

        if (isset($template["ressources"])) {
            $data["ressources"] = $template["ressources"];
        }

        $this->db->insert(NewsLetterInterface::COLLECTION, $data);

        return $uuid;
    }

    /**
     * Validates the template parameter.
     *
     * @param array $template The template parameter to validate.
     * @return void
     */
    public function validateTemplateParameter(array $template): void
    {
        if (! isset($template["name"]) || ! isset($template["page"])) {
            throw new Exception("Missing template parameters");
        }
    }

    /**
     * Retrieves the newsletter page based on the provided UUID.
     *
     * @param string $uuid The UUID of the newsletter page.
     * @return string The newsletter page content.
     */
    public function getNewsLetterPage(string $uuid): string
    {
        if ($page = $this->cacheHelper->get($uuid)) {
            return $page;
        }

        $newsletter = $this->db->findOne(NewsLetterInterface::COLLECTION, [
            "uuid" => $uuid,
        ]);
        if (! $newsletter) {
            return "";
        }

        $htmlContent = $this->fetchHtmlFromApi($newsletter["data"]);

        $this->cacheHelper->set($uuid, $htmlContent);

        return $htmlContent;
    }

    /**
     * Fetches HTML from the API.
     *
     * @param string $data The data to send to the API.
     * @return string The HTML response from the API.
     */
    private function fetchHtmlFromApi(string $data): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, NewsLetterInterface::URL_API_MJML);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:application/json']);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
            "data" => $data,
        ]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        if ($response === false) {
            throw new RuntimeException('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);

        $decodedResponse = json_decode($response, true);
        return $decodedResponse["html"] ?? "";
    }

    /**
     * Retrieves the template for the newsletter.
     *
     * @param string|null $uuid The UUID of the template (optional).
     * @return array The template data.
     */
    public function getTemplate(?string $uuid = null): array
    {
        if ($uuid) {
            $template = $this->db->findOne(NewsLetterInterface::COLLECTION, [
                "uuid" => $uuid,
            ]);
            if ($template) {
                $template["author"] = $this->getModelPerson()->getById($template["authorId"]);
            }
            return $template;
        } else {
            $templates = $this->db->find(NewsLetterInterface::COLLECTION, [
                "type" => NewsLetterInterface::TYPE_TEMPLATE,
            ]);
            foreach ($templates as $key => $template) {
                $templates[$key]["author"] = $this->getModelPerson()->getById($template["authorId"]);
            }
            return $templates;
        }
    }

    /**
     * Updates the template for a newsletter.
     *
     * @param string $userId The ID of the user.
     * @param string $templateId The ID of the template.
     * @param array $data The data to update the template with.
     * @return string The updated template.
     * @throws Exception If the template is not found or the user is not authorized to update it.
     */
    public function updateTemplate(string $userId, string $templateId, array $data): string
    {
        $template = $this->db->findOneById(NewsLetterInterface::COLLECTION, $templateId);
        if (! $template) {
            throw new Exception("Template not found.");
        }

        if ($template["authorId"] !== $userId) {
            throw new Exception("You can't update this template.");
        }

        $this->db->update(NewsLetterInterface::COLLECTION, [
            "_id" => $this->db->MongoId($templateId),
        ], [
            '$set' => $data,
        ]);

        return $templateId;
    }

    /**
     * Deletes a template from the newsletter.
     *
     * @param string $userId The ID of the user deleting the template.
     * @param string $templateId The ID of the template to be deleted.
     * @return string The ID of the deleted template.
     * @throws Exception If the template is not found or the user is not authorized to delete it.
     */
    public function deleteTemplate(string $userId, string $templateId): string
    {
        $template = $this->db->findOneById(NewsLetterInterface::COLLECTION, $templateId);
        if (! $template) {
            throw new Exception("Template not found.");
        }

        if ($template["authorId"] !== $userId) {
            throw new Exception("You can't delete this template.");
        }

        $this->db->remove(NewsLetterInterface::COLLECTION, [
            "_id" => $this->db->MongoId($templateId),
        ]);

        return $templateId;
    }
}
