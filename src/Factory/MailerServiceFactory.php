<?php

namespace PixelHumain\Models\Factory;

use PixelHumain\Models\Factory\Interfaces\FactoryAbstract;
use PixelHumain\Models\Services\Interfaces\Mailer\ExtendedMailerInterface;
use PixelHumain\Models\Services\MailerService;
use Psr\Container\ContainerInterface;

class MailerServiceFactory extends FactoryAbstract
{
    /**
     * Creates a MailerService instance.
     *
     * @param ContainerInterface $container The container interface.
     * @param array $config The configuration array.
     * @param callable|null $postCreationCallback The callback function to be executed after creation.
     * @return MailerService The created MailerService instance.
     */
    protected static function createFactory(ContainerInterface $container, array $config = [], callable $postCreationCallback = null): MailerService
    {
        $mailer = $container->get(ExtendedMailerInterface::class);

        // lier Yii 2 mailer à Symfony mailer
        if (! isset($config['viewPath'])) {
            $config['viewPath'] = '@common/mail';
        }

        if (isset($config['htmlLayout'])) {
            $mailer->htmlLayout = $config['htmlLayout'];
        }
        if (isset($config['textLayout'])) {
            $mailer->textLayout = $config['textLayout'];
        }

        $mailer->setViewPath($config['viewPath']);

        $mailerService = new MailerService($mailer);
        if (is_callable($postCreationCallback)) {
            $postCreationCallback($mailerService, $container);
        }

        return $mailerService;
    }
}
