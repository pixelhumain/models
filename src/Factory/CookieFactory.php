<?php

namespace PixelHumain\Models\Factory;

use PixelHumain\Models\Factory\Interfaces\CookieFactoryInterface;
use yii\web\Cookie;

class CookieFactory implements CookieFactoryInterface
{
    public function createCookie($config = [])
    {
        return new Cookie($config);
    }
}
