<?php

namespace PixelHumain\Models\Factory;

use PixelHumain\Models\Factory\Interfaces\FactoryAbstract;
use PixelHumain\Models\Interfaces\ArrayHelperInterface;
use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\PhdbDiInterface;
use PixelHumain\Models\Person;
use PixelHumain\Models\Services\Interfaces\DbServiceInterface;
use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;
use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;
use Psr\Container\ContainerInterface;

class PersonFactory extends FactoryAbstract
{
    /**
     * Creates a new Person object.
     *
     * @param ContainerInterface $container The container object.
     * @param array $config The configuration options for the Person object.
     * @param callable|null $postCreationCallback A callback function to be executed after the Person object is created.
     * @return Person The newly created Person object.
     * @psalm-suppress MixedMethodCall
     */
    protected static function createFactory(ContainerInterface $container, array $config = [], callable $postCreationCallback = null): Person
    {
        $db = $container->get(DbServiceInterface::class);
        $session = $container->get(SessionServiceInterface::class);
        $language = $container->get(LanguageServiceInterface::class);
        $params = $container->get(ParamsServiceInterface::class);
        $arrayHelper = $container->get(ArrayHelperInterface::class);
        $cookieHelper = $container->get(CookieHelperInterface::class);

        $configClass = [
            'db' => $db,
            'session' => $session,
            'params' => $params,
            'arrayHelper' => $arrayHelper,
            'cookieHelper' => $cookieHelper,
            'language' => $language,
        ];

        // $config = ['params' => $params,
        //         'language' => $language,
        //         'moduleId' => $moduleId,
        //         // 'classNames' => [
        //         //     'organizationClass' => Organization::class,
        //         //     'projectClass' => Project::class,
        //         //     'eventClass' => Event::class,
        //         //     'actionRoomClass' => ActionRoom::class,
        //         //     'actionClass' => Action::class,
        //         //     'cityClass' => City::class,
        //         //     'cronClass' => Cron::class,
        //         //     'formClass' => Form::class,
        //         //     'commentClass' => Comment::class,
        //         //     'surveyClass' => Survey::class,
        //         //     'documentClass' => Document::class,
        //         // ]
        // ];

        $configClass = array_merge($configClass, $config);
        $person = new Person($configClass);
        if (is_callable($postCreationCallback)) {
            $postCreationCallback($person, $container);
        }

        return $person;
    }
}
