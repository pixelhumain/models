<?php

namespace PixelHumain\Models\Factory;

use PixelHumain\Models;
use PixelHumain\Models\Action;
use PixelHumain\Models\ActionRoom;
use PixelHumain\Models\ActivityStream;
use PixelHumain\Models\ActStr;
use PixelHumain\Models\Comment;
use PixelHumain\Models\Factory\Interfaces\FactoryAbstract;
use PixelHumain\Models\Form;
use PixelHumain\Models\Interfaces\ArrayHelperInterface;
use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\PhdbDiInterface;
use PixelHumain\Models\News;
use PixelHumain\Models\Person;
use PixelHumain\Models\Services\Interfaces\DbServiceInterface;
use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;
use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;
use PixelHumain\Models\Survey;
use Psr\Container\ContainerInterface;

class ActionFactory extends FactoryAbstract
{
    /**
     * Creates an instance of the Action class using the provided container, configuration, and post-creation callback.
     *
     * @param ContainerInterface $container The container object used for dependency injection.
     * @param array $config An array of configuration options for the Action instance.
     * @param callable|null $postCreationCallback A callback function to be executed after the Action instance is created.
     * @return Action The created Action instance.
     * @psalm-suppress MixedMethodCall
     */
    public static function createFactory(ContainerInterface $container, array $config = [], callable $postCreationCallback = null): Action
    {
        $db = $container->get(DbServiceInterface::class);
        $session = $container->get(SessionServiceInterface::class);
        $language = $container->get(LanguageServiceInterface::class);
        $params = $container->get(ParamsServiceInterface::class);
        $arrayHelper = $container->get(ArrayHelperInterface::class);
        $cookieHelper = $container->get(CookieHelperInterface::class);

        $configClass = [
            'phdb' => $db,
            'session' => $session,
            'language' => $language,
            'arrayHelper' => $arrayHelper,
            'cookieHelper' => $cookieHelper,
            'params' => $params,
            // 'classNames' => [
            //     'personClass' => Person::class,
            //     'newsClass' => News::class,
            //     // 'serviceClass' => Models\Service::class,
            //     // 'productClass' => Models\Product::class,
            //     'surveyClass' => Survey::class,
            //     'commentClass' => Comment::class,
            //     'formClass' => Form::class,
            //     'activityStreamClass' => ActivityStream::class,
            //     'actionRoomClass' => ActionRoom::class,
            //     'actStrClass' => ActStr::class,
            // ]
        ];

        $configClass = array_merge($configClass, $config);
        $action = new Action($configClass);

        if (is_callable($postCreationCallback)) {
            $postCreationCallback($action, $container);
        }

        return $action;
    }
}
