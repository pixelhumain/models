<?php

namespace PixelHumain\Models\Factory\Interfaces;

use Psr\Container\ContainerInterface;

interface FactoryInterface
{
    /**
     * Creates a person object.
     *
     * @param ContainerInterface $container The container interface.
     * @param callable|null $postCreationCallback The callback function to be executed after creating the person object.
     * @return mixed The created person object.
     */
    public static function create(ContainerInterface $container, array $config = [], callable $postCreationCallback = null);
}
