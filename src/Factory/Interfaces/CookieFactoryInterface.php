<?php

namespace PixelHumain\Models\Factory\Interfaces;

interface CookieFactoryInterface
{
    public function createCookie($config = []);
}
