<?php

namespace PixelHumain\Models\Factory\Interfaces;

use Psr\Container\ContainerInterface;

abstract class FactoryAbstract implements FactoryInterface
{
    /**
     * Creates a factory object.
     *
     * @param ContainerInterface $container The container object.
     * @param array $config The configuration array.
     * @param callable|null $postCreationCallback The callback function to be executed after creation.
     * @return mixed The created factory object.
     */
    abstract protected static function createFactory(ContainerInterface $container, array $config = [], callable $postCreationCallback = null);

    /**
     * Create a new instance of the factory.
     *
     * @param ContainerInterface $container The container object.
     * @param array $config The configuration array.
     * @param callable|null $postCreationCallback The callback function to be executed after creation.
     * @return mixed The created instance.
     */
    public static function create(ContainerInterface $container, array $config = [], callable $postCreationCallback = null)
    {
        return static::createFactory($container, $config, $postCreationCallback);
    }
}
