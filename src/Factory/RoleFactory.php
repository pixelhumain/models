<?php

namespace PixelHumain\Models\Factory;

use PixelHumain\Models\Factory\Interfaces\FactoryAbstract;
use PixelHumain\Models\Interfaces\ArrayHelperInterface;
use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CookieHelperInterface;
use PixelHumain\Models\Interfaces\PhdbDiInterface;
use PixelHumain\Models\Role;
use PixelHumain\Models\Services\Interfaces\DbServiceInterface;
use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;
use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;
use Psr\Container\ContainerInterface;

class RoleFactory extends FactoryAbstract
{
    /**
     * Crée une instance de la classe Role.
     *
     * @param ContainerInterface $container Le conteneur de dépendances.
     * @param array $config La configuration pour la création du rôle.
     * @param callable|null $postCreationCallback Le rappel à exécuter après la création du rôle.
     * @return Role L'instance de la classe Role créée.
     * @psalm-suppress MixedMethodCall
     */
    public static function createFactory(ContainerInterface $container, array $config = [], callable $postCreationCallback = null): Role
    {
        $db = $container->get(DbServiceInterface::class);
        $session = $container->get(SessionServiceInterface::class);
        $language = $container->get(LanguageServiceInterface::class);
        $params = $container->get(ParamsServiceInterface::class);
        $arrayHelper = $container->get(ArrayHelperInterface::class);
        $cookieHelper = $container->get(CookieHelperInterface::class);

        $configClass = [
            'db' => $db,
            'session' => $session,
            'language' => $language,
            'params' => $params,
            'arrayHelper' => $arrayHelper,
            'cookieHelper' => $cookieHelper,
        ];
        $configClass = array_merge($configClass, $config);
        $role = new Role($configClass);

        if (is_callable($postCreationCallback)) {
            $postCreationCallback($role, $container);
        }

        return $role;
    }
}
