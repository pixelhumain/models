<?php

namespace PixelHumain\Models;

use InvalidArgumentException;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\CacheHelperInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\CostumInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;

use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\SlugInterface;
use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BadgeTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CmsTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\BadgeTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CmsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;

use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\SlugTrait;
use PixelHumain\Models\Traits\ZoneTrait;

use Psr\Container\ContainerInterface;

// CO2::getModuleContextList(self::MODULE, "categories", $context),
// Yii::get('urlManager')->createUrl
// Yii::get('module')->getModule("costum")->getAssetsUrl(true)
// ArrayHelper::getValueByDotPath
// Menu::showButton($pageDetail)

class Costum extends BaseModel implements CostumInterface, SlugTraitInterface, ElementTraitInterface, AuthorisationTraitInterface, LinkTraitInterface, DocumentTraitInterface, BadgeTraitInterface, PreferenceTraitInterface, ZoneTraitInterface, CmsTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SlugTrait;
    use ElementTrait;
    use AuthorisationTrait;
    use LinkTrait;
    use DocumentTrait;
    use BadgeTrait;
    use PreferenceTrait;
    use ZoneTrait;
    use CmsTrait;

    protected ?CacheHelperInterface $cacheHelper = null;

    protected ?ContainerInterface $container = null;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();

        if (isset($this->cacheHelper)) {
            if (! $this->cacheHelper instanceof CacheHelperInterface) {
                throw new InvalidArgumentException("cacheHelper must be an instance of CacheHelperInterface");
            }
        }
        if (isset($this->container) && ! $this->container instanceof ContainerInterface) {
            throw new InvalidArgumentException("container must be an instance of ContainerInterface");
        }
    }

    // Todo : ne devrais pas être dans le model
    public function getConfig($context = null)
    {
        return [
            "collection" => CostumInterface::COLLECTION,
            "controller" => CostumInterface::CONTROLLER,
            "module" => CostumInterface::MODULE,
            "categories" => CO2::getModuleContextList(CostumInterface::MODULE, "categories", $context),
            "lbhp" => true,
        ];
    }

    public $paramsCo = [
        "label",
        "subdomainName",
        "placeholderMainSearch",
        "icon",
        "useFilter",
        "slug",
        "formCreate",
        "initType",
        "inMenu",
        "types",
        "actions",
        "height",
        "href",
        //ELEMENT CONFIG
        "labelClass",
        "view",
        "action",
        "dir",
        "edit",
        "dataTarget",
        "id",
        "class",
        "dataAttr",
        "target",
        "toggle",
        "typeAllow",
        "userConnected",
    ];

    // Todo : ne devrais pas être dans le model car il y a un redirect
    /**
     * Initializes the Costum object.
     *
     * @param mixed $ctrl If null, it indicates that session data is being initialized and no rendering is required.
     * @param mixed $id $id If provided with a type, it refers to an element. Otherwise, it can be the slug name or digital ID.
     * @param mixed $type If provided with an ID, it refers to an element.
     * @param mixed $slug The slug of an element.
     * @param bool $edit Whether the Costum is being edited.
     * @param string $source The source of the Costum.
     * @param mixed $test The test value.
     * @param mixed $where The where clause for the Costum.
     * @return mixed The Costum array or void or redirect.
     */
    public function init($ctrl, $id = null, $type = null, $slug = null, $edit = false, $source = "db", $test = null, $where = null)
    {
        $elParams = ["shortDescription", "profilImageUrl", "profilThumbImageUrl", "profilMediumImageUrl", "profilBannerUrl", "name", "tags", "description", "costum", "links", "profilRealBannerUrl", "slug", "rocketchatMultiEnabled", "reference"];
        $tmp = [];
        // ex : http://127.0.0.1/ph/costum/co/index/slug/cocampagne
        // le slug appartient à un élément
        $debug = false;
        if ($debug) {
            var_dump("where " . $where);
            var_dump("id " . $id);
            var_dump("type " . $type);
            var_dump("slug " . $slug);
            // exit;
        }
        if (isset($test)) {
            if ($debug) {
                var_dump('by test ' . $test);
            }
            $id = $test;
            $el = $this->getModelSlug()->getElementBySlug($slug, $elParams);
            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            $tmp['contextSlug'] = $el["el"]["slug"];
        } elseif (isset($slug)) {
            if ($debug) {
                var_dump('by element slug ' . $slug);
            }
            $el = $this->getModelSlug()->getElementBySlug($slug, $elParams);

            if ($debug) {
                var_dump("el exists:" . ! empty($el));
            }

            $tmp['contextType'] = $el["type"];
            $tmp['contextId'] = $el["id"];
            $tmp['contextSlug'] = $el["el"]["slug"];
            if (isset($el["el"]["costum"]['slug'])) {
                $id = $el["el"]["costum"]['id'] ?? $el["el"]["costum"]['slug'];
            } else {
                if ($debug) {
                    var_dump('by default costum slug costumBuilder');
                }
                // generate the costum tag on the element with a default COstum template
                // can be changed in the costum admin
                $cacheCostum = $this->getCostumCache();
                $id = $cacheCostum["slug"] ?? "costumBuilder";
            }
        }

        // ex : http://127.0.0.1/ph/costum/co/index/id/xxxx/type/organizations
        // l'id et le type permettent de retrouver un élément
        elseif (isset($type) && isset($id)) {
            $el = [
                "el" => $this->getModelElement()->getByTypeAndId($type, $id, $elParams),
            ];
            $tmp['contextType'] = $type;
            $tmp['contextId'] = $id;
            $tmp['contextSlug'] = $el["el"]["slug"];
            if (@$el["el"]["costum"]) {
                $id = (@$el["el"]["costum"]['id']) ? $el["el"]["costum"]['id'] : $el["el"]["costum"]['slug'];
            }
        }
        // ex : http://127.0.0.1/ph/costum/co/index/id/cocampagne
        // l'id est celui du costum
        elseif (isset($_GET["id"])) {
            $id = $_GET["id"];
        }

        if ($debug) {
            var_dump('contextId : ' . @$tmp['contextId']);
            var_dump('contextType : ' . @$tmp['contextType']);
            var_dump('contextSlug : ' . @$tmp['contextSlug']);
            var_dump("id " . $id);
        }
        if (! empty($id)) {
            if (strlen($id) == 24 && ctype_xdigit($id)) {
                $c = $this->db->findOne(CostumInterface::COLLECTION, [
                    "_id" => $this->db->MongoId($id),
                ]);
            } else {
                $c = $this->db->findOne(CostumInterface::COLLECTION, [
                    "slug" => $id,
                ]);
            }

            //ATTENTION : only for dev and debug purposes
            //to be sure this never appears on prod
            /*if(YII_DEBUG) {
                if(isset($c["slug"])) {
                $docsJSON = file_get_contents("../../modules/costum/data/".$c["slug"].".json", FILE_USE_INCLUDE_PATH);
                $c = json_decode($docsJSON,true);
                if($source == "json"){
                    echo "********** FROM costum/data/".$c["slug"].".json **********<br/>";
                    if($c == null){
                        echo "********************<br/>";
                        echo "Check you json file : BAD STRUCTURE. <br/>";
                        echo "Le data ".$c["slug"]." json ne charge<br/>";
                        echo "********************<br/>";
                        echo @$docsJSON;
                        exit;
                    }
                    else {
                        //could be cool to show in json render
                        var_dump($c);
                        exit;
                    }
                    echo "********** FROM db custom/slug : ".$c["slug"]." **********<br/>";
                }

            } else {
                $docsJSON = file_get_contents("../../modules/costum/data/".$id.".json", FILE_USE_INCLUDE_PATH);
                $c = json_decode($docsJSON,true);
            }
            }*/
        } elseif (@$_GET["host"]) {
            $c = $this->db->findOne(CostumInterface::COLLECTION, [
                "host" => $_GET["host"],
            ]);

            if (empty($c)) {
                $s = $this->db->findOne(SlugInterface::COLLECTION, [
                    "host" => $_GET["host"],
                ]);

                $el = $this->getModelSlug()->getElementBySlug($s["name"], $elParams);

                $tmp['contextType'] = $el["type"];
                $tmp['contextId'] = $el["id"];
                $tmp['contextSlug'] = $el["el"]["slug"];
                if (isset($el["el"]["costum"]['slug'])) {
                    $id = $el["el"]["costum"]['id'] ?? $el["el"]["costum"]['slug'];
                } else {
                    // generate the costum tag on the element with a default COstum template
                    // can be changed in the costum admin
                    $sessionCostum = $this->session->get('costum');
                    if (isset($sessionCostum["slug"])) {
                        $id = $sessionCostum["slug"];
                    } else {
                        $id = "costumBuilder";
                    }
                }
                $c = $this->db->findOne(CostumInterface::COLLECTION, [
                    "slug" => $el["el"]["costum"]['slug'],
                ]);
            }
        }

        if ($debug) {
            var_dump("el exists:" . ! empty($el));
        }
        if (isset($c) && ! empty($c)) {
            if (isset($c["redirect"])) {
                $redirect = $this->db->findOne(CostumInterface::COLLECTION, [
                    "slug" => $c["redirect"],
                ]);
                $url = (isset($redirect["host"])) ? "https://" . $redirect["host"] : Yii::get('urlManager')->createUrl("/costum/co/index/slug/" . $redirect["slug"]);
                header('Location: ' . $url);
                exit;
            }

            if (! isset($slug) && empty($el)) {
                $el = $this->getModelSlug()->getElementBySlug($c["slug"], $elParams);
                $slug = $c["slug"];
                $tmp['contextType'] = $el["type"];
                $tmp['contextId'] = $el["id"];
                $tmp['contextSlug'] = $el["el"]["slug"];
            }
            if (isset($tmp['contextType']) && empty($el)) {
                $el = $this->getModelSlug()->getElementBySlug($c["slug"], $elParams);
                $c['contextType'] = $tmp['contextType'];
                $c['contextId'] = $tmp['contextId'];
                $c['contextSlug'] = $tmp['contextSlug'];
            }

            if ($debug) {
                var_dump($el);
            }
            if ($debug) {
                var_dump("el exists:" . ! empty($el));
            }

            if (! empty($el["el"])) {
                $element = [
                    "contextType" => $el["type"],
                    "contextId" => $el["id"],
                    "contextSlug" => $el["el"]["slug"],
                    "title" => $el["el"]["name"],
                    "description" => @$el["el"]["shortDescription"],
                    "descriptions" => @$el["el"]["description"],
                    "tags" => @$el["el"]["tags"],
                    "communityLinks" => @$el["el"]["links"],
                    "isCostumAdmin" => false,
                    "assetsUrl" => Yii::get('module')->getModule("costum")->getAssetsUrl(true),
                    "url" => "/costum/co/index/slug/" . $c["slug"],
                    "rocketchatMultiEnabled" => @$el["el"]["rocketchatMultiEnabled"] ?: false,
                    "banner" => @$el["el"]["profilRealBannerUrl"],
                    "reference" => @$el["el"]["reference"],
                ];

                if (isset($c["logo"]) && ! empty($c["logo"])) {
                    $c["logo"] = Yii::get('module')->getModule("costum")->getAssetsUrl() . $c["logo"];
                } elseif (isset($el["el"]["profilImageUrl"])) {
                    $c["logo"] = Yii::get('urlManager')->createUrl($el["el"]["profilImageUrl"]);
                }

                if (isset($c["mailsConfig"]["logo"])) {
                    $c["mailsConfig"]["logo"] = Yii::get('module')->getModule("costum")->getAssetsUrl() . $c["mailsConfig"]["logo"];
                }

                if (isset($c["logoMin"])) {
                    $c["logoMin"] = Yii::get('module')->getModule("costum")->getAssetsUrl() . $c["logoMin"];
                }

                $c = array_merge($c, $element);

                // possibly overload the costum template
                if (isset($el["el"]["costum"]) && $c["slug"] == @$el["el"]["costum"]["slug"]) {
                    unset($el["el"]["costum"]['id']);
                    unset($el["el"]["costum"]['slug']);
                    $c = $this->combine($c, $el["el"]["costum"]);
                    $c["assetsSlug"] = $c["slug"];
                    $c["slug"] = $el["el"]["slug"];
                }

                if (isset($c["htmlConstruct"]["loadingModal"]["logo"])) {
                    $c["htmlConstruct"]["loadingModal"]["logo"] = Yii::get('module')->getModule("costum")->getAssetsUrl() . $c["htmlConstruct"]["loadingModal"]["logo"];
                }

                // Besoin d'approfindir ce sujet des jsons et de leurs utilisations
                // Ici je l'ai utilisé pour injecter des tags spécifiques au pacte / on pourrait aussi penser au catégories d'événement

                if (isset($c["lists"])) {
                    $c = $this->getAndConvertLists($c);
                }
                if (isset($c["json"])) {
                    foreach ($c["json"] as $k => $v) {
                        $c[$k] = $this->getContextList($c["slug"], $v);
                        if ($k == "tags") {
                            $c["request"]["searchTag"] = $c["tags"];
                        }
                    }
                }

                if (isset($c["searchTag"])) {
                    $c["request"]["searchTag"] = [$c["searchTag"]];
                }

                if (isset($c["searchTag"])) {
                    $c["request"]["searchTag"] = [$c["searchTag"]];
                }
                if (isset($c["scopeSelector"])) {
                    $c = $this->initScopeSelector($c);
                }
            }

            // Vérifie si le slug du costum est générique ou non
            if ([$c["slug"]] != [$el["el"]["slug"]]) {
                $c["request"]["sourceKey"] = [$el["el"]["slug"]];
            } else {
                $c["request"]["sourceKey"] = [$c["slug"]];
            }

            if ($this->isSameFunction("init")) {
                $c = $this->sameFunction("init", $c);
            }

            if (! empty($c["forms"]) && $c["forms"] === true) {
                $c["forms"] = $this->db->find(
                    FormInterface::COLLECTION,
                    [
                        "parent." . $c['contextId'] => [
                            '$exists' => true,
                        ],
                        "source.keys" => [
                            '$in' => [$c["slug"]],
                        ],
                    ],
                    ["name", "mapping"]
                );
            }

            $c["appConfig"] = $this->filterThemeInCustom($c, $this->cacheHelper->get("appConfig"));

            return $c;
        }
    }

    /**
     * Sets a nested array value at the specified path.
     *
     * @param array $array The array to modify.
     * @param string $path The path to the nested value, using the specified delimiter.
     * @param mixed $value The value to set.
     * @param string $delimiter The delimiter used to separate nested keys in the path. Default is '/'.
     * @return array The modified array.
     */
    public function set_nested_array_value(&$array, $path, &$value, $delimiter = '/'): array
    {
        $pathParts = explode($delimiter, $path);

        $current = &$array;
        foreach ($pathParts as $key) {
            $current = &$current[$key];
        }

        $backup = $current;
        $current = $value;
        return $backup;
    }

    /**
     * Retrieves the costum cache.
     *
     * @return mixed The costum cache.
     */
    public function getCostumCache()
    {
        return $this->getCostum();
    }

    /**
     * Retrieves the check custom user.
     *
     * @param array $c The first parameter representing an array.
     * @param array $el The second parameter representing an array.
     * @return void|array void or costum user array .
     */
    public function getCheckCustomUser(array $c, array $el)
    {
        if ($this->session->getUserId()) {
            $costumUserArray = [];
            $costumUserArray["admins"] = $this->getModelElement()->getCommunityByTypeAndId($el["type"], $el["id"], PersonInterface::COLLECTION, "isAdmin");
            $communityLinks = $this->getModelElement()->getElementById($el["id"], $el["type"], null, ["links"]);
            $costumUserArray["isMember"] = ($el["type"] != CityInterface::COLLECTION) ? $this->getModelLink()->isLinked($el["id"], $el["type"], $this->session->getUserId(), @$communityLinks["links"]) : null;

            if ($this->getModelAuthorisation()->isUserSuperAdmin($this->session->getUserId()) && ! isset($costumUserArray["admins"][$this->session->getUserId()]) || $this->getModelAuthorisation()->isUserSuperAdminCms()) {
                $costumUserArray["admins"][$this->session->getUserId()] = [
                    "type" => PersonInterface::COLLECTION,
                    "isAdmin" => true,
                ];
            }
            if (isset($costumUserArray["admins"][$this->session->getUserId()])) {
                $costumUserArray["isCostumAdmin"] = true;
            } else {
                $costumUserArray["isCostumAdmin"] = false;
            }
            //hasRoles
            $links = $this->getModelElement()->getElementById($this->session->getUserId(), PersonInterface::COLLECTION, null, ["links"]);
            $costumRoles = [];

            if ($el["type"] != CityInterface::COLLECTION && isset($links["links"][Link::$linksTypes[PersonInterface::COLLECTION][$el["type"]]])) {
                if (isset($communityLinks["links"][Link::$linksTypes[$el["type"]][PersonInterface::COLLECTION]])) {
                    foreach ($links["links"][Link::$linksTypes[PersonInterface::COLLECTION][$el["type"]]] as $k => $v) {
                        if (! empty($v["isAdmin"]) && ! isset($v["isAdminPending"]) && isset($communityLinks["links"][Link::$linksTypes[$el["type"]][PersonInterface::COLLECTION]][$k]["roles"])) {
                            $costumRoles = array_merge($costumRoles, $communityLinks["links"][Link::$linksTypes[$el["type"]][PersonInterface::COLLECTION]][$k]["roles"]);
                        }
                    }
                }
            }

            if (! empty($costumRoles)) {
                $costumUserArray["hasRoles"] = $costumRoles;
            }

            if ($this->isSameFunction("getCheckCustomUser")) {
                // CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
                $costumUserArray = $this->sameFunction("getCheckCustomUser", [
                    "costumUserArray" => $costumUserArray,
                    "communityLinks" => $communityLinks,
                    "el" => $el,
                    "userLinks" => $links,
                ]);
            }

            $costumUserArray = $this->checkUserPreferences($costumUserArray, $this->session->getUserId(), $c["slug"]);

            $this->session->set('costum', [
                $c["slug"] => $costumUserArray,
            ]);

            $sessionCostum = $this->session->get("costum");

            return $sessionCostum[$c["slug"]];
        }
    }

    // merge and update existing fields
    // to be used when applying specific changes defined on an element into a template costum object
    /**
     * Combines two arrays into a single array.
     *
     * @param array $a1 The first array to combine.
     * @param array $a2 The second array to combine.
     * @return array The combined array.
     */
    public function combine(array $a1, array $a2): array
    {
        $deleteValueOfParentCostum = ["buttonList"];
        foreach ($a2 as $k => $v) {
            if (is_array($v)) {
                if (! isset($a1[$k])) {
                    $a1[$k] = null;
                }
                if (! in_array($k, $deleteValueOfParentCostum)) {
                    $a1[$k] = $this->combine($a1[$k], $v);
                } else {
                    $a1[$k] = $v;
                }
            } else {
                $a1[$k] = $v;
            }
        }
        return $a1;
    }

    // Todo  ne trouve pas de reference à cette fonction
    // /**
    //  * Retrieves a Costum by its ID.
    //  *
    //  * @param string $id The ID of the Costum object.
    //  * @return array|null
    //  */
    // public function getById(string $id)
    // {
    //     $classified = $this->db->findOneById(self::COLLECTION, $id);
    //     $classified["typeClassified"] = @$classified["type"];
    //     $classified["type"] = self::COLLECTION;
    //     $where = [
    //         "id" => @$id,
    //         "type" => self::COLLECTION,
    //         "doctype" => "image",
    //     ];
    //     $classified["images"] = $this->getModelDocument()->getListDocumentsWhere($where, "image");
    //     return $classified;
    // }

    /**
     * Retrieves a Costum by its slug.
     *
     * @param string $id The slug of the Costum.
     * @return array|null The Costum data as an array or null if not found.
     */
    public function getBySlug(string $id): ?array
    {
        return $this->db->findOne(CostumInterface::COLLECTION, [
            "slug" => $id,
        ]);
    }

    /**
     * Retrieves and converts lists.
     *
     * @param array $costum The costum array.
     * @return array The converted lists.
     */
    public function getAndConvertLists(array $costum): array
    {
        foreach ($costum["lists"] as $key => $v) {
            if (isset($v["type"]) && $v["type"] == BadgeInterface::COLLECTION) {
                $costum = $this->getListByBadges($key, $v, $costum);
            } elseif (isset($v["collection"]) && isset($v["distinct"])) {
                $costum["lists"][$key] = $this->db->distinct($v["collection"], $v["distinct"], $v["where"]);
            } elseif (isset($v["collection"]) && isset($v["where"])) {
                if (isset($v["fields"])) {
                    $costum["lists"][$key] = $this->db->find($v["collection"], $v["where"], $v["fields"]);
                } else {
                    $costum["lists"][$key] = $this->db->find($v["collection"], $v["where"]);
                }
            }
        }
        return $costum;
    }

    /**
     * Update the Costum model with the given parameters.
     *
     * @param array $params The parameters to update the model.
     * @return bool Returns true if the update was successful, false otherwise.
     */
    public function update(array $params): bool
    {
        $cacheCostum = $this->getCostumCache();
        if (isset($cacheCostum["assetsSlug"])) {
            $set = [];
            $unset = [];

            foreach ($params as $k => $v) {
                if (is_string($v) && $v === '$unset') {
                    $unset["costum." . $k] = "";
                } else {
                    $set["costum." . $k] = $v;
                }
            }
            $cond = [];
            if (! empty($set)) {
                $cond['$set'] = $set;
            }
            if (! empty($unset)) {
                $cond['$unset'] = $unset;
            }
            if (! empty($cond)) {
                $this->db->update(
                    $cacheCostum["contextType"],
                    [
                        "_id" => $this->db->MongoId($cacheCostum["contextId"]),
                    ],
                    $cond
                );
                $this->forceCacheReset();
            }
        }
        return true;
    }

    /**
     * Forces a cache reset.
     *
     * @return void
     */
    public function forceCacheReset(): void
    {
        $cacheCostum = $this->getCostumCache();
        if (! isset($cacheCostum["resetCache"])) {
            $cacheCostum["resetCache"] = true;
            $this->cacheHelper->set($cacheCostum["contextSlug"], $cacheCostum);
            if ($this->cacheHelper->get($_SERVER['SERVER_NAME'])) {
                $serverCostum = $this->cacheHelper->get($_SERVER['SERVER_NAME']);
                $serverCostum["resetCache"] = true;
                $this->cacheHelper->set($_SERVER['SERVER_NAME'], $serverCostum);
            }
        }
    }

    /**
     * Get a list of Costum objects by badges.
     *
     * @param string $keyList The key list.
     * @param mixed $search The search parameter.
     * @param array $costum The costum array.
     * @param string|null $parentId The parent ID (optional).
     * @return array The list of Costum objects.
     */
    public function getListByBadges(string $keyList, $search, array $costum, ?string $parentId = null): array
    {
        if (empty($parentId)) {
            $where = [
                "parent" => [
                    '$exists' => false,
                ],
            ];
        } else {
            $where = [
                "parent." . $parentId => [
                    '$exists' => true,
                ],
            ];
        }
        if (isset($search["sourceKey"]) && ! empty($search["sourceKey"])) {
            $where["source.key"] = $costum["slug"];
        }
        if (isset($search["category"])) {
            $where["category"] = $search["category"];
        }
        $badges = $this->getModelBadge()->getByWhere($where, true);

        $typeList = null;

        if (! empty($costum["lists"][$keyList]["typeList"]) && $costum["lists"][$keyList]["typeList"] == "array") {
            $typeList = "array";
        }

        if (empty($parentId)) {
            $costum["lists"][$keyList] = [];
        }
        if (! empty($badges)) {
            if (! isset($costum["badges"])) {
                $costum["badges"] = [];
            }
            if (! isset($costum["badges"][$keyList])) {
                $costum["badges"][$keyList] = [];
            }
            foreach ($badges as $k => $v) {
                $costum["badges"][$keyList][$k] = $v;
                if (! empty($parentId)) {
                    array_push($costum["lists"][$keyList][$costum["badges"][$keyList][$parentId]["name"]], $v["name"]);
                } elseif (! empty($typeList) && $typeList == "array") {
                    $costum["lists"][$keyList][] = $v["name"];
                } else {
                    $costum["lists"][$keyList][$v["name"]] = [];
                }
                $costum = $this->getListByBadges($keyList, $search, $costum, $k);
            }
        } elseif (empty($badges) && empty($parentId)) {
            if (! isset($costum["badges"])) {
                $costum["badges"] = [];
            }
            if (! isset($costum["badges"][$keyList])) {
                $costum["badges"][$keyList] = [];
            }
        }

        return $costum;
    }

    /**
     * Check the Costum list.
     *
     * @param array $check The array to check.
     * @param array|null $filter The filter to apply (optional).
     * @param mixed|null $k The value of k (optional).
     * @return array The filtered array.
     */
    public function checkCOstumList(array $check, ?array $filter, $k = null): array
    {
        $newObj = [];
        $arrayInCustom = ["label", "subdomainName", "placeholderMainSearch", "icon", "height", "imgPath", "useFilter", "dropdownResult", "showMap", "useMapBtn", "slug", "formCreate", "setParams", "show", "initType", "inMenu", "types", "actions", "dataTarget", "dynform", "id", "filters", "extendFilters", "tagsList", "calendar", "class", "onlyAdmin", "restricted", "img", "nameMenuTop", "header", "name", "inMenuTop", "results", "filterObj", "module", "hash", "loadEvent", "urlExtra"];
        foreach ($check as $key => $v) {
            if (! empty($v)) {
                $newObj[$key] = $filter[$key] ?? $v;
                $checkArray = true;
                foreach ($arrayInCustom as $entry) {
                    if (isset($v[$entry])) {
                        $newObj[$key][$entry] = $v[$entry];
                        $checkArray = false;
                    }
                }
            }
            if (is_array($v) && $checkArray) {
                $newObj[$key] = $this->checkCOstumList($v, $newObj[$key], $k);
            }
        }

        return $newObj;
    }

    /**
     * Check and replace the values in the $costum array based on the $coParams array.
     *
     * @param array|null $costum The array to check and replace values in.
     * @param array $coParams The array containing the replacement values.
     * @return mixed The filtered array.
     */
    public function checkAndReplace(?array $costum, array $coParams)
    {
        $filteringObj = [];
        if (empty($costum)) {
            return false;
        }
        foreach ($costum as $key => $v) {
            if (! empty($v)) {
                $filteringObj[$key] = $coParams[$key] ?? $v;
                // Si simplement à true on récupére la valeur des paramètres de communecter
                if (is_bool($v) && isset($coParams[$key]) && is_bool($coParams[$key])) {
                    $filteringObj[$key] = $v;
                } elseif ($v !== true && (is_string($v) || is_numeric($v)) && isset($filteringObj[$key])) {
                    $filteringObj[$key] = $v;
                } elseif (is_array($v) && isset($filteringObj[$key])) {
                    foreach ($filteringObj[$key] as $i => $p) {
                        if (! isset($v[$i]) && in_array($i, self::$paramsCo)) {
                            $v[$i] = $p;
                        }
                    }
                    $filteringObj[$key] = $this->checkAndReplace($v, $filteringObj[$key]);
                }
            }
        }
        return (! empty($filteringObj)) ? $filteringObj : false;
    }

    /**
     * Filters the theme in the custom array.
     *
     * @param array $c The custom array.
     * @param array $params The parameters array.
     * @return array The filtered custom array.
     */
    public function filterThemeInCustom(array $c, array $params): array
    {
        // filter menu app custom
        // Html construction of communecter
        $paramCombine = $params;
        $appToKeep = $params["pages"];
        $menuApp = ["#annonces", "#search", "#agenda", "#live", "#dda", "#admin", "#badge"];
        if (isset($c["app"]) && ! empty($c["app"])) {
            $paramCombine["numberOfApp"] = is_countable($c["app"]) ? count($c["app"]) : 0;
            $menuPages = $this->checkCOstumList($c["app"], $paramCombine["pages"]);

            foreach ($paramCombine["pages"] as $hash => $v) {
                if (! in_array($hash, $menuApp) && ! isset($c["app"][$hash])) {
                    $menuPages[$hash] = $v;
                }
            }
            foreach ($menuApp as $v) {
                if (! isset($menuPages[$v])) {
                    $menuPages[$v] = $appToKeep[$v];
                }
            }
            $paramCombine["pages"] = $menuPages;
        }
        if (isset($c["htmlConstruct"])) {
            $constructParams = $c["htmlConstruct"];
            $listButtonMenus = [
                "pages" => $paramCombine["pages"],
                "mainMenuButtons" => @$paramCombine["mainMenuButtons"],
                "elementMenuButtons" => @$paramCombine["elementMenuButtons"],
            ];
            // Check about app (#search, #live, #etc) if should costumized

            // Check about header construction
            // - Entry banner to adda tpl path to a custom banner, by default not isset
            // - Entry menuTop will config btn present in menu top (navLeft & navRight)
            if (isset($constructParams["header"])) {
                if (isset($constructParams["header"]["banner"])) { //URL
                    $paramCombine["header"]["banner"] = $constructParams["header"]["banner"];
                }
                if (isset($constructParams["header"]["css"])) {
                    $paramCombine["header"]["css"] = $constructParams["header"]["css"];
                }
            }
            foreach (["header.menuTop", "subMenu", "menuLeft", "menuRight", "menuBottom", "element.menuTop.left", "element.menuTop.right", "element.menuLeft"] as $keyMenu) {
                $costumMenuConfig = (strpos($keyMenu, ".") !== false) ? ArrayHelper::getValueByDotPath($constructParams, $keyMenu) : @$constructParams[$keyMenu];
                $themeMenuConfig = (strpos($keyMenu, ".") !== false) ? ArrayHelper::getValueByDotPath($paramCombine, $keyMenu) : @$paramCombine[$keyMenu];

                // Get value of menu themeParams config with costum config
                $appConfigMenu = (! empty($costumMenuConfig)) ? $this->constructMenu($costumMenuConfig, $themeMenuConfig, $listButtonMenus, null, @$c["logo"]) : false;
                if (strpos($keyMenu, ".") !== false) {
                    $paramCombine = ArrayHelper::setValueByDotPath($paramCombine, $keyMenu, $appConfigMenu);
                } else {
                    $paramCombine[$keyMenu] = $appConfigMenu;
                }
            }
            if (isset($constructParams["element"]["menuTop"]) && empty($constructParams["element"]["menuTop"])) {
                $paramCombine["element"]["menuTop"] = false;
            }

            if (isset($constructParams["footer"])) {
                if (! empty($constructParams["footer"])) {
                    $paramCombine["footer"] = $this->checkCOstumList($constructParams["footer"], $paramCombine["footer"]);
                } else {
                    $paramCombine["footer"] = $constructParams["footer"];
                }
            }

            // Check about admin Panel navigation (cosstumized space for costum administration expectations)
            if (isset($constructParams["adminPanel"])) {
                if (isset($constructParams["adminPanel"]["js"])) {
                    $paramCombine["adminPanel"]["js"] = $constructParams["adminPanel"]["js"];
                }

                if (isset($constructParams["adminPanel"]["add"])) {
                    $paramCombine["adminPanel"]["add"] = $constructParams["adminPanel"]["add"];
                }
                if (isset($constructParams["adminPanel"]["menu"])) {
                    $paramCombine["adminPanel"]["menu"] = $this->checkCOstumList($constructParams["adminPanel"]["menu"], $paramCombine["adminPanel"]["menu"]);
                }
            }

            if (isset($constructParams["element"])) {
                if (isset($constructParams["element"]["initView"])) {
                    $paramCombine["element"]["initView"] = $constructParams["element"]["initView"];
                }
                if (isset($constructParams["element"]["tplCss"])) {
                    $paramCombine["element"]["tplCss"] = $constructParams["element"]["tplCss"];
                }

                if (isset($constructParams["element"]["banner"])) {
                    if (is_string($constructParams["element"]["banner"])) {
                        $paramCombine["element"]["banner"] = $constructParams["element"]["banner"];
                    } else {
                        $paramCombine["element"]["banner"] = $this->checkAndReplace($constructParams["element"]["banner"], $paramCombine["element"]["banner"]);
                    }
                }
                if (isset($constructParams["element"]["containerClass"])) {
                    $paramCombine["element"]["containerClass"] = $this->checkAndReplace($constructParams["element"]["containerClass"], $paramCombine["element"]["containerClass"]);
                }
                if (isset($constructParams["element"]["js"])) {
                    $paramCombine["element"]["js"] = $constructParams["element"]["js"];
                }

                if (isset($constructParams["element"]["css"])) {
                    $paramCombine["element"]["css"] = $constructParams["element"]["css"];
                }
            }
            if (isset($constructParams["preview"])) {
                if (isset($constructParams["preview"]["toolBar"])) {
                    $paramCombine["preview"]["toolBar"] = $this->checkAndReplace($constructParams["preview"]["toolBar"], $paramCombine["preview"]["toolBar"]);
                }
                if (isset($constructParams["preview"]["banner"])) {
                    $paramCombine["preview"]["banner"] = $this->checkAndReplace($constructParams["preview"]["banner"], $paramCombine["preview"]["banner"]);
                }
                if (isset($constructParams["preview"]["body"])) {
                    $paramCombine["preview"]["body"] = $this->checkAndReplace($constructParams["preview"]["body"], $paramCombine["preview"]["body"]);
                }
            }
            // #appRendering permit to get horizontal and vertical organization of menu
            //   - horizontal = first version of co2 with menu of apps in horizontal
            //   - vertical = current version of co2 with menu of apps on the left
            if (isset($constructParams["appRendering"])) {
                $paramCombine["appRendering"] = $constructParams["appRendering"];
            }
            if (isset($constructParams["loadingModal"]["url"])) {
                $paramCombine["loadingModal"] = $constructParams["loadingModal"]["url"];
            }

            if (isset($constructParams["directory"])) {
                $directoryP = $paramCombine["directory"];
                $paramCombine["directory"] = $constructParams["directory"];
                foreach ($directoryP as $k => $v) {
                    if (! isset($paramCombine["directory"][$k])) {
                        $paramCombine["directory"][$k] = $v;
                    }
                }
            }

            // # redirect permits to application in case of undefined view or error or automatic redirection to fall on costum redirect
            // To be setting for logged user && unlogged user
            if (isset($constructParams["redirect"])) {
                $paramCombine["pages"]["#app.index"]["redirect"] = $constructParams["redirect"];
            }
        }
        if (isset($c["typeObj"])) {
            $paramCombine["typeObj"] = $c["typeObj"];
        }
        return $paramCombine;
    }

    /**
     * Construct a menu based on the provided parameters.
     *
     * @param array $costum The costum array.
     * @param array $config The configuration array.
     * @param array $completeMenuButton The complete menu button array.
     * @param array $listConfigParams The list configuration parameters array (optional).
     * @param string $logoUrl The URL of the logo (optional).
     * @return array The constructed menu.
     */
    public function constructMenu(array $costum, array $config, array $completeMenuButton, array $listConfigParams = [], string $logoUrl = ""): array
    {
        $menuConfig = $config;
        $menuKeyList = $config["keyListButton"] ?? "mainMenuButtons";
        foreach ($costum as $key => $v) {
            if (! empty($key) && $key == "buttonList" && ! empty($v)) {
                foreach ($v as $k => $button) {
                    $menuList = (empty($listConfigParams) && isset($completeMenuButton[$menuKeyList])) ? $completeMenuButton[$menuKeyList] : $listConfigParams;
                    $btnEntry = "";

                    if (isset($menuList[$k])) {
                        $btnEntry = $menuList[$k];
                        if (isset($btnEntry['keyListButton']) && isset($completeMenuButton[$btnEntry['keyListButton']])) {
                            $menuList = $completeMenuButton[$btnEntry['keyListButton']];
                        }
                        if (! is_bool($button) && ! is_string($button)) {
                            $btnEntry = $this->constructMenu($button, $btnEntry, $completeMenuButton, $menuList);
                        }
                    } elseif (! empty($button) && is_array($button)) {
                        $btnEntry = $button;
                    }

                    if ($k == "logo" && ! empty($logoUrl)) {
                        $btnEntry["url"] = $logoUrl;
                    }

                    if (isset($btnEntry["lbhAnchor"]) && $btnEntry["lbhAnchor"] === "true") {
                        $btnEntry["lbhAnchor"] = true;
                    }
                    if (isset($btnEntry["lbhAnchor"]) && $btnEntry["lbhAnchor"] === "false") {
                        $btnEntry["lbhAnchor"] = false;
                    }

                    $menuConfig[$key][$k] = $btnEntry;
                }
            } elseif (is_string($v) || is_bool($v) || is_numeric($v)) {
                $menuConfig[$key] = $v;
            } elseif (is_array($v)) {
                if (isset($menuConfig[$key])) {
                    $menuConfig[$key] = $this->constructMenu($v, $menuConfig[$key], $completeMenuButton, [], $logoUrl);
                } else {
                    $menuConfig[$key] = $v;
                }
            }
        }

        return $menuConfig;
    }

    /**
     * Get the list of contexts for a given slug and context name.
     *
     * @param string $slug The slug value.
     * @param string $contextName The context name.
     * @return array The list of contexts.
     */
    public function getContextList(string $slug, string $contextName): array
    {
        $layoutPath = "../../modules/costum/json/" . $slug . "/" . $contextName . ".json";

        $str = file_get_contents($layoutPath);
        $list = json_decode($str, true);
        return $list;
    }

    /**
     * Check user preferences for a specific costum.
     *
     * @param array $costum The costum data.
     * @param string $userId The user ID.
     * @param string $slug The costum slug.
     * @return array The user preferences for the costum.
     */
    public function checkUserPreferences(array $costum, string $userId, string $slug): array
    {
        $userPref = $this->getModelPreference()->getPreferencesByTypeId($userId, PersonInterface::COLLECTION);
        if (! empty($userPref) && isset($userPref["costum"]) && isset($userPref["costum"][$slug])) {
            $costum["userPreferences"] = $userPref["costum"][$slug];
        }
        return $costum;
    }

    /**
     * Check if the user and reference are valid.
     *
     * @return void
     */
    public function checkUserAndReference(): void
    {
        if ($this->session->getUserId()) {
            $cacheCostum = $this->getCostumCache();
            $getPerson = $this->getModelElement()->getElementById($this->session->getUserId(), PersonInterface::COLLECTION, null, ["source", "reference"]);
            $hasCostumK = false;
            if (! empty($getPerson["source"])) {
                if (isset($getPerson["source.key"]) && $getPerson["source.key"] == $cacheCostum["slug"]) {
                    $hasCostumK = true;
                }
                if (isset($getPerson["source.keys"]) && in_array($cacheCostum["slug"], $getPerson["source.keys"])) {
                    $hasCostumK = true;
                }
            }
            if (! empty($getPerson["reference"]) && ! empty($getPerson["reference"]["costum"]) && in_array($cacheCostum["slug"], $getPerson["reference"]["costum"])) {
                $hasCostumK = true;
            }
            if (! $hasCostumK) {
                if (empty($getPerson["reference"])) {
                    $set = [
                        "costum" => [$cacheCostum["slug"]],
                    ];
                } elseif (! isset($getPerson["reference"]["costum"])) {
                    $set = $getPerson["reference"]["costum"] = [$cacheCostum["slug"]];
                } elseif (! empty($getPerson["reference"]["costum"])) {
                    $set = array_push($getPerson["reference"]["costum"], $cacheCostum["slug"]);
                }
                $this->db->update(
                    PersonInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($this->session->getUserId()),
                    ],
                    [
                        '$set' => [
                            "reference" => $set,
                        ],
                    ]
                );
            }
        }
    }

    /**
     * Initializes the scope selector.
     *
     * @param array $params The parameters for initialization.
     * @return array The initialized scope selector.
     */
    public function initScopeSelector(array $params): array
    {
        $params["scopeSelector"] = $this->getModelZone()->getScopeByIds($params["scopeSelector"])["scopes"];

        return $params;
    }

    // Todo : doit tester si la class existe et si la methode existe et elle n'est pas static
    // le probleme c'est que l'on sait pas d'ou vient la fonction donc il faut que l'on test si la class est présente dans les injections
    // et si c'est pas le cas voir comment on peut faire pour l'instancier sans créer de depndance circulaire
    // public function sameFunction(string $function, $params = null): ?array
    // {
    //     if ($this->isSameFunction($function)) {
    //         $cacheCostum = $this->getCostumCache();
    //         $slugContext = $cacheCostum["assetsSlug"] ?? $cacheCostum["slug"];
    //         $slugContext = ucfirst($slugContext);

    //         if (class_exists($slugContext)) {
    //             $params = $slugContext::$function($params);
    //         }
    //     }
    //     return $params;
    // }

    public function sameFunction(string $function, $params = null)
    {
        if ($this->isSameFunction($function)) {
            $cacheCostum = $this->getCostumCache();
            $slugContext = $cacheCostum["assetsSlug"] ?? $cacheCostum["slug"];
            $slugContext = ucfirst($slugContext);

            // Obtenir le namespace complet en utilisant le namespace de la classe courante
            $fullClassName = __NAMESPACE__ . '\\' . $slugContext;

            // On test si la class est une propriété de la classe courante et si elle est instancié dans le namespace courant
            // Vérifier si la propriété existe et est une instance de la classe spécifiée
            $propertyName = lcfirst($slugContext); // SlugContext en minuscule
            if (property_exists($this, $propertyName) && $this->$propertyName instanceof $fullClassName) {
                // La propriété existe et est une instance de la classe spécifiée
                if (method_exists($this->$propertyName, $function)) {
                    $params = $this->$propertyName->$function($params);
                }
                // On test si la fonction existe dans la classe courante
            } elseif ($this instanceof $fullClassName) {
                if (method_exists($this, $function)) {
                    $params = $this->$function($params);
                }
                // on test si present dans le container d'injection de dépendance
            } elseif ($this->container->has($fullClassName)) {
                $class = $this->container->get($fullClassName);
                if (method_exists($class, $function)) {
                    $params = $class->$function($params);
                }
            }
            // Todo : si la class est bien dans le namespace courant mais n'est pas instancié dans la class courante et quel a des dependances à d'autres modeles pour fonctionner ?

            // Todo : si la class est une class dans le module costum comment je fais ? reponse :  tout les models des différents modules seront dans le namespace PixelHumain\Models
        }
        return $params;
    }

    /**
     * Check if the given function name is the same as the current function.
     *
     * @param string $function The function name to compare.
     * @return bool Returns true if the function names are the same, false otherwise.
     */
    public function isSameFunction(string $function): bool
    {
        $res = false;
        $cacheCostum = $this->getCostumCache();
        if (! empty($cacheCostum["slug"]) &&
            ! empty($cacheCostum["class"]) &&
            ! empty($cacheCostum["class"]["function"]) &&
            in_array($function, $cacheCostum["class"]["function"])) {
            Print_r($cacheCostum);
            $res = true;
        }
        return $res;
    }

    /**
     * Preps the data based on the given parameters.
     *
     * @param array $params The parameters to be used for data preparation.
     * @return array The prepared data.
     */
    public function prepData(array $params): array
    {
        $cacheCostum = $this->getCostumCache();
        if (! empty($cacheCostum)) {
            if (! empty($params["preferences"]) && ! empty($params["preferences"]["toBeValidated"])) {
                $params["preferences"]["toBeValidated"] = [
                    $cacheCostum["slug"] => true,
                ];
            }
        }

        if ($this->isSameFunction("prepData")) {
            $params = $this->sameFunction("prepData", $params);
        }
        return $params;
    }

    /**
     * Get the value of the Costum.
     *
     * @param string $value The value to get.
     * @return mixed The value of the Costum.
     */
    public function value(string $value)
    {
        $cacheCostum = $this->getCostumCache();
        if (substr_count($value, 'this.') > 0) {
            $field = explode(".", $value);
            if (isset($cacheCostum[$field[1]])) {
                return $cacheCostum[$field[1]];
            }
        } else {
            return $cacheCostum[$value];
        }
    }

    /**
     * Get the redirect URL.
     *
     * @return string|null The redirect URL, or null if not set.
     */
    public function getRedirect(): ?string
    {
        $url = null;
        $cacheCostum = $this->getCostumCache();
        if (isset($cacheCostum) && isset($cacheCostum["slug"])) {
            $redirect = $this->db->findOne(CostumInterface::COLLECTION, [
                "slug" => $cacheCostum["slug"],
            ]);
            $url = (isset($redirect["host"])) ? "https://" . $redirect["host"] : Yii::get('urlManager')->createUrl("/costum/co/index/slug/" . $redirect["slug"]);
        }
        return $url;
    }

    /**
     * Get the source of a costum by its slug.
     *
     * @param string $slug The slug of the costum.
     * @return array The source of the costum.
     */
    public function getSource(string $slug): array
    {
        $s = [
            "key" => $slug,
            "keys" => [$slug],
            "insertOrign" => "costum",
            "date" => $this->db->MongoDate(time()),
        ];
        return $s;
    }

    // Todo : ne devrait pas être dans le modele vu que c'est lier à la vue/asset
    /**
     * Retrieve all fonts.
     *
     * @param array|null $costum Optional. An array of custom parameters. Default is null.
     * @return array|null An array of fonts, or null if no fonts are found.
     */
    public function getAllFonts(?array $costum = null): ?array
    {
        $cacheFonts = $this->cacheHelper->get("costumFonts");
        if ($costum == null) {
            return null;
        }
        if ($cacheFonts === false) {
            $fonts = [];
            $fontsParsed = [];
            $fontsKeyValue = [];
            //get costum font directory
            $dirs = array_filter(glob('../../modules/costum/assets/font/*'), 'is_dir');
            if (isset($costum["contextType"]) && isset($costum["contextId"])) {
                $fontUpload = "../../pixelhumain/ph/upload/communecter/" . $costum["contextType"] . "/" . $costum["contextId"] . "/file";
                $dirs[] = $fontUpload;
            }
            $dirs[] = "../../modules/costum/assets/font/blockcms/fontpack";
            // get font otf or ttf on each directory
            foreach ($dirs as $kdir => $vdir) {
                $fonts = array_merge($fonts, glob($vdir . "/*.{ttf,otf}", GLOB_BRACE));
            }
            //parse font to assetsUrl directory
            $fontsParsed = array_map(
                function ($str) {
                    if (strpos($str, "../../modules/costum/assets") !== false) {
                        return str_replace('../../modules/costum/assets', '', $str);
                    } elseif (strpos($str, "../../pixelhumain/ph") !== false) {
                        return str_replace('../../pixelhumain/ph', Yii::get('url')::base(), $str);
                    }
                },
                $fonts
            );
            //convert to array assoc
            foreach ($fontsParsed as $fk => $fv) {
                $explode = explode('.', basename($fv));
                if (end($explode) == "ttf") {
                    $fontsKeyValue[(string) $fv] = basename($fv, '.ttf');
                }
                if (end($explode) == "otf") {
                    $fontsKeyValue[(string) $fv] = basename($fv, '.otf');
                }
            }
            asort($fontsKeyValue);
            $this->cacheHelper->set("costumFonts", $fontsKeyValue);
            return $this->cacheHelper->get("costumFonts");
        } else {
            return $this->cacheHelper->get("costumFonts");
        }
    }

    /**
     * Retrieves the slug based on the host.
     *
     * @param string $host The host value.
     * @return string|null The slug value or null if not found.
     */
    public function getSlugByHost(string $host): ?string
    {
        $costum = $this->db->findOne(CostumInterface::COLLECTION, [
            "host" => $host,
        ]);
        if ($costum) {
            return $costum["slug"];
        }

        $organization = $this->db->findOne(OrganizationInterface::COLLECTION, [
            "costum.host" => $host,
        ]);
        if ($organization) {
            return $organization["slug"];
        }

        $project = $this->db->findOne(ProjectInterface::COLLECTION, [
            "costum.host" => $host,
        ]);
        if ($project) {
            return $project["slug"];
        }

        $event = $this->db->findOne(EventInterface::COLLECTION, [
            "costum.host" => $host,
        ]);
        if ($event) {
            return $event["slug"];
        }

        $slug = $this->db->findOne(SlugInterface::COLLECTION, [
            "host" => $host,
        ]);
        if ($slug["name"]) {
            return $slug["name"];
        }

        return null;
    }

    /**
     * Returns the page parameters for a given costum.
     *
     * @param array $costum The costum data.
     * @param string $page The page name.
     * @return array The page parameters.
     */
    public function getPageParams(array $costum, string $page): array
    {
        if (isset($costum["type"]) && $costum["type"] !== "aap" && ! isset($costum["app"]["#" . $page])) {
            $page = "welcome";
        }

        $params = [
            "slug" => $costum["slug"],
            "page" => $page,
            "tplUsingId" => "",
            "tplKey" => "",
            "tplInitImage" => "",
            "nbrTplUser" => 0,
            "nbrTplViewer" => 0,
            "paramsData" => [],
            "insideTplInUse" => [],
            "cmsList" => [],
            "newCmsId" => [],
            "costum" => $costum,
            "cmsInUseId" => [],
        ];

        $params["cmsList"] = [];
        if ($page !== "") {
            $params["cmsList"] = $this->getModelCms()->getCmsWithChildren($costum["contextId"], $page, [
                "position" => 1,
            ]);
        }

        foreach ($params["cmsList"] as $cmsId => $cmsData) {
            if (isset($cmsData["advanced"])) {
                if (isset($cmsData["advanced"]["persistent"])) {
                    // Keep block banner into the top of the page
                    if ($cmsData["advanced"]["persistent"] == "banner") {
                        unset($params["cmsList"][$cmsId]);

                        $params["cmsList"] = [
                            $cmsId => $cmsData,
                        ] + $params["cmsList"];
                    } elseif ($cmsData["advanced"]["persistent"] == "footer") {
                        // Keep block footer into the end of the page
                        if (array_key_exists($cmsId, $params["cmsList"])) {
                            unset($params["cmsList"][$cmsId]);

                            $params["cmsList"][$cmsId] = $cmsData;
                        }
                    }
                }
            }
        }

        return $params;
    }

    /**
     * Returns a custom error message for unauthorized access.
     *
     * @param string $page The page that the user is not authorized to access.
     * @return string The custom error message.
     */
    public function getCostumMsgNotAuthorized(string $page): string
    {
        $costum = $this->getCostum();

        $pageDetail = ! empty($costum["app"]["#" . $page]) ? $costum["app"]["#" . $page] : [];
        if ($costum == false || $this->getMenu()->showButton($pageDetail)) {
            return "";
        }
        $msgNotAuthorized = "";
        if (! isset($costum["msgNotAuthorized"])) {
            $msgNotAuthorized = '<div class="col-lg-offset-1 col-lg-10 col-xs-12 margin-top-50 text-center text-red">	
                                    <i class="fa fa-lock fa-4x "></i><br/>
                                    <h1 class="">' . $this->language->t("organization", "Unauthorized Access.") . '</h1>
                                </div>';
        } else {
            $msgNotAuthorized = $costum["msgNotAuthorized"];
        }
        return $msgNotAuthorized;
    }

    public function getCostum(?string $id = null, ?string $type = null, ?string $slug = null)
    {
        return [
            'slug' => 'aap',
            'class' => [
                'function' => ['testFon'],
            ],
        ];
    }

    public function testFon(...$args)
    {
        return 'yes';
    }
}
