<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\SlugInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;

class Slug extends BaseModel implements SlugInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Saves the slug for a specific type and ID.
     *
     * @param string $type The type of the entity.
     * @param string $id The ID of the entity.
     * @param string $slug The slug to be saved.
     * @return bool Returns true.
     */
    public function save(string $type, string $id, string $slug): bool
    {
        $data = [
            "id" => $id,
            "type" => $type,
            "name" => $slug,
            "elemUpdated" => time(),
        ];
        $data = $this->db->insert(SlugInterface::COLLECTION, $data);
        return true;
    }

    /**
     * Check and create a slug for the given string.
     *
     * @param string $str The string to create a slug for.
     * @return string The created slug.
     */
    public function checkAndCreateSlug($str): string
    {
        $str = self::cleanString($str);
        $slug = self::createSlug($str);
        return $this->ensureUniqueSlug($slug);
    }

    /**
     * Cleans a string by removing any special characters or spaces.
     *
     * @param string $str The string to be cleaned.
     * @return string The cleaned string.
     */
    private static function cleanString($str)
    {
        $str = mb_substr($str, 0, 50);
        $unwanted_array = [
            'Š' => 'S',
            'š' => 's',
            'Ž' => 'Z',
            'ž' => 'z',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ý' => 'Y',
            'Þ' => 'B',
            'ß' => 'Ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'a',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'o',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ý' => 'y',
            'þ' => 'b',
            'ÿ' => 'y',
        ];
        $str = strtr($str, $unwanted_array);
        return $str;
    }

    /**
     * Creates a slug from a given string.
     *
     * @param string $str The string to create a slug from.
     * @return string The generated slug.
     */
    private static function createSlug($str)
    {
        $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $str = preg_replace('~[^\-\w]+~', '', strtolower($str));
        return trim($str, '-');
    }

    /**
     * Ensure that the given slug is unique.
     *
     * @param string $slug The slug to be checked for uniqueness.
     * @return string The unique slug.
     */
    private function ensureUniqueSlug($slug): string
    {
        if (! $this->check($slug)) {
            $suffix = 1;
            while (! self::check($slug . $suffix)) {
                $suffix++;
            }
            $slug .= $suffix;
        }
        return $slug;
    }

    /**
     * Check if a slug is valid for a given type and ID.
     *
     * @param string $slug The slug to check.
     * @param string|null $type The type of the entity associated with the slug (optional).
     * @param string|null $id The ID of the entity associated with the slug (optional).
     * @return bool Returns true if the slug is valid for the given type and ID, false otherwise.
     */
    public function check(string $slug, ?string $type = null, ?string $id = null): bool
    {
        // todo $type n'est pas utilisé
        if (! in_array($slug, ["search", "agenda", "annonces", "live", "ressources", "settings", "store", "welcome", "home", "admin", "info", "docs", "default"])) {
            $where = [
                "name" => $slug,
            ];
            if (! empty($id)) {
                $where["id"] = [
                    '$ne' => $id,
                ];
            }
            $res = $this->db->find(SlugInterface::COLLECTION, $where);
            if (! empty($res)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Removes a slug by its parent ID and type.
     *
     * @param string $id The ID of the parent entity.
     * @param string $type The type of the parent entity.
     * @return void
     */
    public function removeByParentIdAndType(string $id, string $type): void
    {
        $where = [
            "id" => $id,
            "type" => $type,
        ];
        $this->db->remove(SlugInterface::COLLECTION, $where);
    }
}
