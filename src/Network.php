<?php

namespace PixelHumain\Models;

use Exception;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\NetworkInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;

// TODO : Yii::get('view')->theme->basePath
// TODO : use yii\web\HttpException;

class Network extends BaseModel implements NetworkInterface, LinkTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use LinkTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
    }

    public static array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "visible" => [
            "name" => "visible",
        ],
        "skin" => [
            "name" => "skin",
        ],
        "title" => [
            "name" => "skin.title",
        ],
        "paramsLogo" => [
            "name" => "paramsLogo",
        ],
        "origin" => [
            "name" => "origin",
        ],
        "add" => [
            "name" => "add",
        ],
        "filter" => [
            "name" => "filter",
        ],
        "types" => [
            "name" => "types",
        ],
        "result" => [
            "name" => "result",
        ],
        "displayImage" => [
            "name" => "displayImage",
        ],
        "request" => [
            "name" => "request",
        ],
        "searchTag" => [
            "name" => "searchTag",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "scope" => [
            "name" => "scope",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Retrieves the network JSON data based on the provided network parameters.
     *
     * @param string|null $networkParams The network parameters.
     * @return array|null The network JSON data as an array, or null if no data is found.
     */
    public function getNetworkJson(?string $networkParams): ?array
    {
        $configPath = "";

        if (empty($networkParams)) {
            $configPath = "default";
        } else {
            $configPath = $networkParams;
        }

        if (stripos($configPath, "http") === false) {
            error_log("chargement du fichier de config en local");
            $configPath = Yii::get('view')->theme->basePath . '/views/layouts/params/' . $configPath . ".json";
        }

        try {
            $json = file_get_contents($configPath, null, null, 0, 10000);
            if ($json === false) {
                throw new HttpException(404, "Impossible to find the network configuration file.");
            }
        } catch (Exception $e) {
            throw new HttpException(404, "Error Reading the network configuration file.");
        }

        $retour = json_decode($json, true);
        return is_array($retour) ? $retour : [];
    }

    /**
     * Preps the data.
     *
     * @param array $params The parameters.
     * @return array The prepped data.
     */
    public function prepData(array $params): array
    {
        if (isset($params["skin"]["displayCommunexion"]) && ! is_bool($params["skin"]["displayCommunexion"])) {
            if ($params["skin"]["displayCommunexion"] == "true") {
                $params["skin"]["displayCommunexion"] = true;
            } else {
                $params["skin"]["displayCommunexion"] = false;
            }
        }

        if (! empty($params["add"])) {
            $newAdd = [];
            foreach ($params["add"] as $key => $value) {
                $newAdd[$value] = true;
            }
            $params["add"] = $newAdd;
        }

        return $params;
    }

    /**
     * Retrieves a network by its ID.
     *
     * @param string $id The ID of the network.
     * @param array|null $fields The optional fields to include in the network data.
     * @return array|null The network data as an array, or null if the network is not found.
     */
    public function getById(string $id, ?array $fields = null): ?array
    {
        $network = $this->db->findOneById(NetworkInterface::COLLECTION, $id, $fields);
        return $network;
    }

    /**
     * Retrieves the network information for a given user ID.
     *
     * @param string $id The user ID.
     * @param array|null $fields Optional. An array of fields to retrieve. Default is null.
     * @return array The network information.
     */
    public function getNetworkByUserId(string $id, ?array $fields = null): array
    {
        $where = [
            "creator" => $id,
        ];

        if ($this->session->getUserId() != $id) {
            $isLinked = false;
            if (! empty($this->session->getUserId())) {
                $isLinked = $this->getModelLink()->isLinked($id, PersonInterface::COLLECTION, $this->session->getUserId());
            }

            if ($isLinked) {
                $where = [
                    '$and' => [
                        $where, [
                            '$or' => [[
                                "visible" => "public",
                            ], [
                                "visible" => "network",
                            ]],
                        ]],
                ];
            } else {
                $where = [
                    '$and' => [
                        $where, [
                            "visible" => "public",
                        ]],
                ];
            }
        }

        $networks = $this->db->find(NetworkInterface::COLLECTION, $where, $fields);
        return $networks;
    }

    /**
     * Get the list of networks by user ID.
     *
     * @param string $id The user ID.
     * @return array The list of networks.
     */
    public function getListNetworkByUserId(string $id): array
    {
        $networks = $this->getNetworkByUserId($id);
        $list = [];
        foreach ($networks as $key => $value) {
            $value["type"] = "network";
            $list[$key] = $value;
        }
        return $list;
    }

    /**
     * Retrieves a network based on its ID and type.
     *
     * @param string $id The ID of the network.
     * @param string $type The type of the network.
     * @return array The network data.
     */
    public function getNetwork(string $id, string $type): array
    {
        $res = [];
        $listElt = [OrganizationInterface::COLLECTION, PersonInterface::COLLECTION, ProjectInterface::COLLECTION, EventInterface::COLLECTION];
        if (in_array($type, $listElt)) {
            $res = $this->db->findOne($type, [
                "_id" => $this->db->MongoId($id),
            ], ["urls"]);
            $res = (! empty($res["urls"]) && is_array($res["urls"]) ? $res["urls"] : []);
        }
        return $res;
    }

    /**
     * Executes after saving a network.
     *
     * @param array $network The network data.
     * @param string $creatorId The ID of the creator.
     * @return array The updated network data.
     */
    public function afterSave(array $network, string $creatorId): array
    {
        $networkId = (string) $network['_id'];
        $isAdmin = true;
        $this->getModelLink()->connect($networkId, NetworkInterface::COLLECTION, $creatorId, PersonInterface::COLLECTION, $creatorId, "followers", $isAdmin);
        $this->getModelLink()->connect($creatorId, PersonInterface::COLLECTION, $networkId, NetworkInterface::COLLECTION, $creatorId, "networks", $isAdmin);

        return [
            "result" => true,
            "msg" => "Votre network est communectée.",
            "id" => $networkId,
            "network" => $network,
        ];
    }
}
