<?php

namespace PixelHumain\Models;

use DateTime;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\CooperationInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Interfaces\ResolutionInterface;
use PixelHumain\Models\Interfaces\RoomInterface;

use PixelHumain\Models\Traits\ActionTrait;
use PixelHumain\Models\Traits\ActivityStreamTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ActionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProposalTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ResolutionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\RoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TranslateTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;
use PixelHumain\Models\Traits\NewsTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProposalTrait;
use PixelHumain\Models\Traits\ResolutionTrait;
use PixelHumain\Models\Traits\RoomTrait;
use PixelHumain\Models\Traits\TranslateTrait;
use PixelHumain\Models\Traits\ZoneTrait;

class Cooperation extends BaseModel implements CooperationInterface, RoomTraitInterface, NewsTraitInterface, ActionTraitInterface, ResolutionTraitInterface, ProposalTraitInterface, ElementTraitInterface, PersonTraitInterface, ZoneTraitInterface, ActivityStreamTraitInterface, TranslateTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use RoomTrait;
    use NewsTrait;
    use ActionTrait;
    use ProposalTrait;
    use ResolutionTrait;
    use ElementTrait;
    use PersonTrait;
    use ZoneTrait;
    use ActivityStreamTrait;
    use TranslateTrait;

    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
    }

    public static array $iconCoop = [
        "mine" => "user",
        "amendable" => "pencil",
        "todo" => "ticket",
        "tovote" => "gavel",
        "done" => "check",
        "closed" => "trash",
        "resolved" => "certificate",
        "disabled" => "times",
    ];

    public static array $colorCoop = [
        "mine" => "blue",
        "amendable" => "purple",
        "todo" => "green-k",
        "progress" => "green-k",
        "startingsoon" => "green-k",
        "tovote" => "green-k",
        "done" => "red",
        "closed" => "red",
        "resolved" => "dark",
        "nodate" => "red",
        "disabled" => "orange",
        "late" => "orange",
    ];

    public static array $colorVoted = [
        "up" => "green",
        "down" => "red",
        "white" => "white",
        "uncomplet" => "orange",
    ];

    /**
     * Returns the icon for a cooperation based on the given key.
     *
     * @param string $key The key of the cooperation.
     * @return string The icon for the cooperation.
     */
    public function getIconCoop(string $key): string
    {
        return array_key_exists($key, self::$iconCoop) ? (string) self::$iconCoop[$key] : "ban";
    }

    /**
     * Returns the color of the cooperation based on the given key.
     *
     * @param string $key The key used to determine the color of the cooperation.
     * @return string The color of the cooperation.
     */
    public function getColorCoop(string $key): string
    {
        return array_key_exists($key, self::$colorCoop) ? (string) self::$colorCoop[$key] : "dark";
    }

    /**
     * Returns the color voted by the user.
     *
     * @param string $voted The vote casted by the user.
     * @return string The color voted by the user.
     */
    public function getColorVoted(string $voted): string
    {
        return array_key_exists($voted, self::$colorVoted) ? (string) self::$colorVoted[$voted] : "dark";
    }

    /**
     * Retrieves cooperation data based on the provided parameters.
     *
     * @param string $parentType The type of the parent entity.
     * @param string $parentId The ID of the parent entity.
     * @param string $type The type of the cooperation entity.
     * @param string|null $status The status of the cooperation entity (optional).
     * @param string|null $dataId The ID of the data associated with the cooperation entity (optional).
     * @return array The cooperation data.
     */
    public function getCoopData(string $parentType, string $parentId, string $type, ?string $status = null, ?string $dataId = null): array
    {
        $res = [];

        $this->updateStatusProposal($parentType, $parentId);

        if ($type == RoomInterface::CONTROLLER) {
            if (empty($dataId)) { //si pas d'id : prend toutes les rooms pour un element parent
                $status = empty($status) ? "open" : $status;
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                    "status" => $status,
                ];
                $res["roomList"] = $this->db->findAndSort(
                    RoomInterface::COLLECTION,
                    $query,
                    [
                        "amendementDateEnd" => 1,
                        "voteDateEnd" => 1,
                    ]
                );

                $res["allCount"] = $this->getAllCount($parentType, $parentId);
            } else { //si un d'id : prend récupère toutes les proposals & actions & resolutions de la room
                if (isset($dataId) && $dataId != "undefined") {
                    $res["room"] = $this->getModelRoom()->getById($dataId);
                }

                $query = [
                    "idParentRoom" => $dataId,
                    "status" => [
                        '$in' => ['amendable', "tovote", "disabled", "todo", "adopted", "refused", "amendementAndVote"],
                    ],
                ];

                $res["proposalList"] = $this->db->findAndSort(
                    ProposalInterface::COLLECTION,
                    $query,
                    [
                        "status" => -1,
                        "amendementDateEnd" => 1,
                        "voteDateEnd" => 1,
                    ]
                );

                $res["actionList"] = $this->db->findAndSort(
                    ActionInterface::COLLECTION,
                    $query,
                    [
                        "status" => -1,
                        "voteDateEnd" => 1,
                    ]
                );

                $res["resolutionList"] = $this->db->findAndSort(
                    "resolutions",
                    $query,
                    [
                        "status" => 1,
                        "voteDateEnd" => -1,
                    ]
                );
            }
        } elseif ($type == ProposalInterface::CONTROLLER) {
            if (empty($dataId) && $parentType != NewsInterface::CONTROLLER) { //si pas d'id : prend toutes les proposal pour un element parent
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                ];

                if (! empty($status)) {
                    if ($status == "mine") {
                        $myId = @$this->session->getUserId() ? $this->session->getUserId() : false;
                        if ($myId != false) {
                            $query["creator"] = $myId;
                        }
                    } else {
                        $query["status"] = $status;
                    }
                } else {
                    $query["status"] = [
                        '$in' => ['amendable', "tovote", "amendementAndVote"],
                    ];

                    $res["roomList"] = $this->db->findAndSort(
                        RoomInterface::COLLECTION,
                        [
                            "parentType" => $parentType,
                            "parentId" => $parentId,
                        ],
                        []
                    );
                }

                $res["proposalList"] = $this->db->findAndSort(
                    ProposalInterface::COLLECTION,
                    $query,
                    [
                        "status" => -1,
                        "amendementDateEnd" => 1,
                        "voteDateEnd" => 1,
                    ]
                );
            } else { //si un d'id : prend récupère toutes les proposals & actions & resolutions de la room
                if ($parentType != NewsInterface::CONTROLLER) { //parentType == "news" => modération de la news
                    $res["proposal"] = $this->getModelProposal()->getById($dataId);
                } else {
                    $res["proposal"] = $this->db->findOne(
                        ProposalInterface::COLLECTION,
                        [
                            "parentType" => NewsInterface::CONTROLLER,
                            "parentId" => $parentId,
                        ]
                    );
                    $res["news"] = $this->getModelNews()->getById($parentId);
                }
            }
        } elseif ($type == ActionInterface::CONTROLLER) {
            if (empty($dataId)) { //si pas d'id : prend toutes les rooms pour un element parent
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                ];
                if ($status == "mine") {
                    $myId = @$this->session->getUserId() ? $this->session->getUserId() : false;
                    if ($myId != false) {
                        $query["creator"] = $myId;
                    }
                } else {
                    $query["status"] = $status;
                }
                $res["actionList"] = $this->db->findAndSort(
                    ActionInterface::COLLECTION,
                    $query,
                    [
                        "dateEnd" => 1,
                    ]
                );
            } else { //si un d'id : prend récupère toutes les proposals & actions & resolutions de la room
                $res["action"] = $this->getModelAction()->getById($dataId);
            }
        } elseif ($type == ResolutionInterface::CONTROLLER) {
            if (empty($dataId)) { //si pas d'id : prend toutes les Resolution pour un element parent
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                ];
                if ($status == "mine") {
                    $myId = @$this->session->getUserId() ? $this->session->getUserId() : false;
                    if ($myId != false) {
                        $query["creator"] = $myId;
                    }
                } else {
                    $query["status"] = $status;
                }
                $res["resolutionList"] = $this->db->findAndSort(
                    ResolutionInterface::COLLECTION,
                    $query,
                    [
                        "status" => 1,
                        "dateEnd" => -1,
                    ]
                );
            } else { //si un d'id : prend récupère toutes les proposals & actions & resolutions de la room
                $res["resolution"] = $this->getModelResolution()->getById($dataId);
                $res["resolution"]["actions"] = $this->db->findAndSort(
                    ActionInterface::COLLECTION,
                    [
                        "idParentResolution" => $dataId,
                    ],
                    [
                        "status" => 1,
                        "dateEnd" => -1,
                    ]
                );
            }
        }

        $res = $this->checkRoleAccess($res);

        $res["post"]["collection"] = $type;
        $res["post"]["status"] = $status;
        $res["post"]["parentId"] = $parentId;
        $res["post"]["parentType"] = $parentType;

        return $res;
    }

    /**
     * Check role access for a given resource.
     *
     * @param array $res The resource to check access for.
     * @return array The result of the role access check.
     */
    public function checkRoleAccess(array $res): array
    {
        $me = $this->getModelElement()->getByTypeAndId("citoyens", $this->session->getUserId());

        foreach (["proposal", "proposalList", "action", "actionList", "resolution", "resolutionList", "tovote", "amendable", "resolved", "closed", "disabled", "mine", "todo", "done", "disabled"] as $list) {
            if (isset($res[$list])) {
                $listCoop = ! @$res[$list]["_id"] ? $res[$list] : [$res[$list]];
                foreach ($listCoop as $k => $coop) {
                    if ($coop["parentType"] == "projects") {
                        $link = "projects";
                    }
                    if ($coop["parentType"] == "organizations") {
                        $link = "memberOf";
                    }
                    $myRoles = @$me["links"][$link][@$coop["parentId"]]["roles"] ?: [];

                    if (@$coop["idParentRoom"]) {
                        $roomId = $coop["idParentRoom"];

                        $parentRoom = $this->getModelRoom()->getById($roomId);
                        $accessRoom = @$parentRoom ? $this->getModelRoom()->getAccessByRole($parentRoom, $myRoles) : "";

                        if ($accessRoom == "lock") {
                            unset($res[$list][$k]);
                        }
                    }
                }
            }
        }
        return $res;
    }

    /**
     * Check the role access in the news list.
     *
     * @param array $newsList The list of news.
     * @return array The updated news list with role access information.
     */
    public function checkRoleAccessInNews(array $newsList): array
    {
        $me = $this->getModelElement()->getByTypeAndId("citoyens", $this->session->getUserId());
        foreach ($newsList as $k => $news) {
            if (@$news["object"] && @$news["object"]["type"] == "proposals") {
                $proposal = $this->getModelProposal()->getById(@$news["object"]["id"]);
                $parentRoom = $this->getModelRoom()->getById(@$proposal["idParentRoom"]);
                if (@$parentRoom["roles"]) {
                    if ($proposal["parentType"] == "projects") {
                        $link = "projects";
                    }
                    if ($proposal["parentType"] == "organizations") {
                        $link = "memberOf";
                    }
                    $myRoles = @$me["links"][@$link][@$proposal["parentId"]]["roles"] ?: [];

                    $accessRoom = @$parentRoom ? $this->getModelRoom()->getAccessByRole($parentRoom, $myRoles) : "";

                    if ($accessRoom == "lock") {
                        unset($newsList[$k]);
                    }
                }
            }
        }
        return $newsList;
    }

    /**
     * Check if a user has voted.
     *
     * @param string $userId The ID of the user.
     * @param array $obj The object to check for votes.
     * @return bool|string Returns the vote if the user has voted, false otherwise.
     */
    public function userHasVoted(string $userId, array $obj)
    {
        foreach ($obj as $keyVal => $arr) {
            foreach ($arr as $keyId) {
                if ($keyId == $userId) {
                    return (string) $keyVal;
                }
            }
        }
        return false;
    }

    /**
     * Get the count of all items.
     *
     * @param string $parentType The type of the parent.
     * @param string $parentId   The ID of the parent.
     *
     * @return array The count of all items.
     */
    public function getAllCount(string $parentType, string $parentId): array
    {
        $myId = @$this->session->getUserId() ? $this->session->getUserId() : false;
        $allCount = [];

        //count proposals
        foreach (["tovote", "amendable", "resolved", "closed", "disabled"] as $status) {
            $query = [
                "parentType" => $parentType,
                "parentId" => $parentId,
                "status" => $status,
            ];
            $allCount["proposals"][$status] = $this->db->find(ProposalInterface::COLLECTION, $query, []);

            if ($myId != false) {
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                    "creator" => $myId,
                ];
                $allCount["proposals"]["mine"] = $this->db->find(ProposalInterface::COLLECTION, $query, []);
            }
        }

        //check roles proposals
        $allCount["proposals"] = $this->checkRoleAccess($allCount["proposals"]);
        foreach (["tovote", "amendable", "resolved", "closed", "disabled", "mine"] as $status) {
            if (isset($allCount["proposals"][$status])) {
                $allCount["proposals"][$status] = is_countable($allCount["proposals"][$status]) ? count($allCount["proposals"][$status]) : 0;
            }
        }

        //count actions
        foreach (["todo", "done", "disabled"] as $status) {
            $query = [
                "parentType" => $parentType,
                "parentId" => $parentId,
                "status" => $status,
            ];
            $allCount["actions"][$status] = $this->db->find(ActionInterface::COLLECTION, $query, []);

            if ($myId != false) {
                $query = [
                    "parentType" => $parentType,
                    "parentId" => $parentId,
                    "creator" => $myId,
                ];
                $allCount["actions"]["mine"] = $this->db->find(ActionInterface::COLLECTION, $query, []);
            }
        }

        //check roles actions
        $allCount["actions"] = $this->checkRoleAccess($allCount["actions"]);
        foreach (["todo", "done", "disabled", "mine"] as $status) {
            if (isset($allCount["actions"][$status])) {
                $allCount["actions"][$status] = is_countable($allCount["actions"][$status]) ? count($allCount["actions"][$status]) : 0;
            }
        }

        return $allCount;
    }

    /**
     * Update the status of a proposal.
     *
     * @param string $parentType The type of the parent entity.
     * @param string $parentId   The ID of the parent entity.
     *
     * @return array The updated proposal status.
     */
    public function updateStatusProposal(string $parentType, string $parentId): array
    {
        $query = [
            "parentType" => $parentType,
            "parentId" => $parentId,
            "status" => [
                '$in' => ["amendable", "tovote"],
            ],
        ];

        $proposalList = $this->db->findAndSort(ProposalInterface::COLLECTION, $query, []);

        foreach ($proposalList as $key => $proposal) {
            //amendement TO tovote
            if (@$proposal["amendementDateEnd"] && @$proposal["amendementActivated"] == true &&
                $proposal["status"] == "amendable") {
                $amDateEnd = strtotime($proposal["amendementDateEnd"]);
                $today = time();

                if ($amDateEnd < $today) {
                    $proposalList[$key]["status"] = "tovote";

                    $this->db->update(
                        ProposalInterface::COLLECTION,
                        [
                            "_id" => $this->db->MongoId($key),
                        ],
                        [
                            '$set' => [
                                "status" => "tovote",
                            ],
                        ]
                    );
                    /* TODO : Add notification */
                }
            }

            //tovote TO closed
            if (@$proposal["voteDateEnd"] && $proposal["voteActivated"] == true && $proposal["status"] == "tovote") {
                $voteDateEnd = strtotime($proposal["voteDateEnd"]);
                $today = time();

                if ($voteDateEnd < $today) {
                    $resolution = $this->getModelProposal()->getById($key);
                    $voteRes = $this->getModelProposal()->getAllVoteRes($resolution);

                    $adopted = @$voteRes["up"] &&
                                @$voteRes["up"]["percent"] &&
                                $voteRes["up"]["percent"] > intval(@$resolution["majority"]);

                    $resolution["status"] = $adopted ? "adopted" : "refused";

                    if (@$resolution["answers"]) {
                        $resolution["status"] = "adopted";
                    }

                    $resolutionExist = $this->getModelResolution()->getById($key);

                    if (! $resolutionExist) {
                        $resolution = $this->db->insert(ResolutionInterface::COLLECTION, $resolution);
                        $this->afterSave($resolution, ResolutionInterface::COLLECTION);
                    }

                    $proposalList[$key]["idResolution"] = $proposal["_id"];
                    $proposalList[$key]["status"] = "resolved";
                    $this->db->update(
                        ProposalInterface::COLLECTION,
                        [
                            "_id" => $this->db->MongoId($key),
                        ],
                        [
                            '$set' => [
                                "status" => "resolved",
                                "idResolution" => $proposal["_id"],
                            ],
                        ]
                    );

                    //moderation news
                    if ($proposal["parentType"] == NewsInterface::COLLECTION) {
                        if ($resolution["status"] == "adopted") {
                            $res = $this->db->update(
                                NewsInterface::COLLECTION,
                                [
                                    "_id" => $this->db->MongoId($proposal["parentId"]),
                                ],
                                [
                                    '$set' => [
                                        "isAnAbuse" => true,
                                    ],
                                ]
                            );
                        } else {
                            error_log("IS NOT AN ABUSE " . $proposal["parentId"]);
                            $res = $this->db->update(
                                NewsInterface::COLLECTION,
                                [
                                    "_id" => $this->db->MongoId($proposal["parentId"]),
                                ],
                                [
                                    '$unset' => [
                                        "reportAbuse" => null,
                                        "reportAbuseCount" => null,
                                    ],
                                ]
                            );
                        }
                    }
                }
            }
        }
        return $proposalList;
    }

    /**
     * Formats the date before saving.
     *
     * @param string $date The date to be formatted.
     * @return mixed The formatted date.
     */
    public function formatDateBeforeSaving(string $date)
    {
        $date = DateTime::createFromFormat('d/m/Y H:s', $date);
        $date = $this->db->MongoDate($date->getTimestamp());
        return $date;
    }

    /**
     * Executes after saving the cooperation model.
     *
     * @param array $params The parameters passed to the method.
     * @param string $type The type of save operation (create or update).
     * @return void
     */
    public function afterSave(array $params, string $type): void
    {
        $localityId = null;
        $geo = null;
        $id = (string) $params['_id'];
        $name = $params["name"];

        $targetId = @$params["parentId"];
        $targetType = @$params["parentType"];
        $scopeType = ($targetType != PersonInterface::COLLECTION) ? "private" : "restricted";

        //si c'est une proposal sans room, qui n'est pas une modération (!= News::COLLECTION)
        //on parle d'un sondage

        $object = [
            "type" => $type,
            "id" => $id,
            "displayName" => $name,
        ];

        if (! isset($params["idParentRoom"]) &&
                $type == ProposalInterface::COLLECTION &&
                @$params["parentType"] != NewsInterface::COLLECTION) {
            $object["isSurvey"] = true;
        }

        $buildArray = [
            "type" => ActivityStreamInterface::COLLECTION,
            "verb" => ActStrInterface::VERB_PUBLISH,
            "target" => [
                "id" => $targetId,
                "type" => $targetType,
            ],
            "author" => $this->session->getUserId(),
            "object" => $object,
            "scope" => [
                "type" => $scopeType,
            ],
            "created" => $this->db->MongoDate(time()),
            "sharedBy" => [[
                "id" => $this->session->getUserId(),
                "type" => "citoyens",
                "updated" => $this->db->MongoDate(time()),
            ]],
        ];

        if (! empty($params["address"])) {
            $buildArray["scope"]["type"] = "public";
            $address = null;
            if (! empty($params["address"])) {
                $localityId = $params["address"]["localityId"];
                $address = $params["address"];
            }

            if (isset($params["geo"])) {
                $geo = $params["geo"];
            }

            if (! @$localityId) {
                $author = $this->getModelPerson()->getSimpleUserById($this->session->getUserId());

                if (@$author["address"] && @$author["address"]["localityId"]) {
                    $localityId = $author["address"]["localityId"];
                    $address = $author["address"];
                    if (! @$geo) {
                        $geo = $author["geo"];
                    }
                }
            }

            $scope = [
                "parentId" => $localityId,
                "parentType" => CityInterface::COLLECTION,
                "name" => $address["addressLocality"],
                "geo" => $geo,
            ];
            if (! (empty($address["postalCode"]))) {
                $scope["postalCode"] = $address["postalCode"];
            }

            $scope = array_merge($scope, $this->getModelZone()->getLevelIdById($localityId, $address, CityInterface::COLLECTION));

            $buildArray["scope"]["localities"][] = $scope;
        }

        $newsShared = $this->getModelActivityStream()->addEntry($buildArray);
    }

    /**
     * Returns the count of notifications.
     *
     * @return int The count of notifications.
     */
    public function getCountNotif(): int
    {
        $userId = @$this->session->getUserId();
        $me = $this->getModelPerson()->getById($userId);
        $memberOfOrga = (@$me["links"] && @$me["links"]["memberOf"]) ? $me["links"]["memberOf"] : [];
        $memberOfProject = (@$me["links"] && @$me["links"]["projects"]) ? $me["links"]["projects"] : [];
        $memberOf = array_merge($memberOfOrga, $memberOfProject);

        $res = [];
        $count = 0;
        foreach ($memberOf as $id => $element) {
            $allElement = $this->getModelElement()->getByTypeAndId($element["type"], $id);

            $amendable = $this->getCoopData($element["type"], $id, "proposal", "amendable");
            $tovote = $this->getCoopData($element["type"], $id, "proposal", "tovote");
            $actions = $this->getCoopData($element["type"], $id, "action", "todo");
            $adopted = $this->getCoopData($element["type"], $id, "resolution", "adopted");
            $refused = $this->getCoopData($element["type"], $id, "resolution", "refused");

            foreach ($tovote["proposalList"] as $key => $proposal) {
                $hasVote = @$proposal["votes"] ? $this->userHasVoted($userId, $proposal["votes"]) : false;
                if ($hasVote) {
                    unset($tovote["proposalList"][$key]);
                }
            }

            foreach ($actions["actionList"] as $key => $action) {
                $participate = @$action["links"] ? @$action["links"]["contributors"][$this->session->getUserId()] : false;
                if (! $participate && sizeof(@$action["links"]["contributors"]) > 0) {
                    unset($actions["actionList"][$key]);
                }
            }

            foreach ($adopted["resolutionList"] as $key => $resolution) {
                $dayDiff = $this->getModelTranslate()->dayDifference($resolution["created"], "timestamp");
                if ($dayDiff > 7) {
                    unset($adopted["resolutionList"][$key]);
                }
            }

            foreach ($refused["resolutionList"] as $key => $resolution) {
                $dayDiff = $this->getModelTranslate()->dayDifference($resolution["created"], "timestamp");
                if ($dayDiff > 7) {
                    unset($refused["resolutionList"][$key]);
                }
            }

            $resolved["resolutionList"] = array_merge($adopted["resolutionList"], $refused["resolutionList"]);

            $count += (is_countable($amendable["proposalList"]) ? count($amendable["proposalList"]) : 0) + (is_countable($tovote["proposalList"]) ? count($tovote["proposalList"]) : 0) +
                     (is_countable($actions["actionList"]) ? count($actions["actionList"]) : 0) + count($resolved["resolutionList"]);
        }

        return $count;
    }
}
