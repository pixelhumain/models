<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CsvInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\SearchNewTrait;

// TODO : ArrayHelper::getAllPathJson

class Csv extends BaseModel implements CsvInterface, CostumTraitInterface, SearchNewTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use SearchNewTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Retrieves the CSV data for the admin.
     *
     * @param array $post The post data.
     * @return array The CSV data for the admin.
     */
    public function getCsvAdmin(array $post): array
    {
        if ($this->getModelCostum()->isSameFunction("csvElement")) {
            $resS = $this->getModelCostum()->sameFunction("csvElement", $post);
        } else {
            // Recherche la données en fonction des filtres
            $resS = $this->getModelSearchNew()->searchAdmin($post);
        }

        $fields = [];
        $elements = [];
        foreach ($resS["results"] as $key => $value) {
            // On parse les champs pour restruturer la données ou pour vérifier les caracteère qui pourrait faire buguer le parser csv
            if (isset($_POST["roles"]) && is_array($_POST["roles"])) {
                $value = $this->parseFieldsRoles($value, $_POST["roles"]);
            } else {
                $value = $this->parseFields($value);
            }
            $elements[] = $value;

            // On récupere la liste des fields
            $fields = ArrayHelper::getAllPathJson(json_encode($value), $fields, false);
        }

        if (! empty($post["fields"])) {
            $sortOrder = [];
            $fieldsMultiple = [];
            // On parcoure les fields afin de les remettre dans le bonne ordre
            foreach ($post["fields"] as $keysortOrder => $valsortOrder) {
                if (empty($fieldsMultiple[$valsortOrder])) {
                    $fieldsMultiple[$valsortOrder] = [];
                }
                foreach ($fields as $key => $value) {
                    $pos = strpos($value, (string) $valsortOrder);
                    if ($pos !== false) {
                        $fieldsMultiple[$valsortOrder][] = $value;
                    }
                }
                sort($fieldsMultiple[$valsortOrder], SORT_FLAG_CASE);
                $sortOrder = [...$sortOrder, ...$fieldsMultiple[$valsortOrder]];
            }
            $fields = $sortOrder;
        }
        // On retourne la donnée pour le parser csv
        $res = [
            "results" => $elements,
            "fields" => $fields,
        ];
        return $res;
    }

    /**
     * Parse the fields of a CSV row.
     *
     * @param array $elt The CSV row to parse.
     * @return array The parsed fields.
     */
    public function parseFields(array $elt): array
    {
        if (! empty($elt["_id"])) {
            $elt["id"] = (string) $elt["_id"];
            unset($elt["_id"]);
        }

        // Restructuration des scopes
        if (! empty($elt["scope"])) {
            $scope = [];
            foreach ($elt["scope"] as $keyS => $valScope) {
                if (! empty($valScope["name"])) {
                    $scope[] = $valScope["name"];
                } elseif (! empty($valScope["cityName"]) && ! empty($valScope["postalCode"])) {
                    $scope[] = $valScope["cityName"] . " " . $valScope["postalCode"];
                }
            }
            $elt["scope"] = $scope;
        }

        // Restructuration des parents
        if (! empty($elt["parent"])) {
            $parent = [];
            foreach ($elt["parent"] as $keyS => $valScope) {
                if (! empty($valScope["name"])) {
                    $parent[] = $valScope["name"];
                }
            }
            $elt["parent"] = $parent;
        }

        // On enleve les # qui emepche le fonctionne du parser csv
        foreach ($elt as $key => $value) {
            if (is_string($value)) {
                $elt[$key] = str_replace("#", "", $value);
            }
        }

        return $elt;
    }

    /**
     * Parse the fields of an element based on the given roles.
     *
     * @param array $elt The element to parse.
     * @param array $roles The roles to use for parsing.
     * @return array The parsed fields.
     */
    public function parseFieldsRoles(array $elt, array $roles): array
    {
        $elt = $this->parseFields($elt);

        // Restructuration des roles
        if (isset($elt["links"]) && isset($elt["links"]["contributors"]) && is_array($elt["links"]["contributors"])) {
            foreach ($elt["links"]["contributors"] as $keyS => $valScope) {
                if (isset($valScope["roles"]) && is_array($valScope["roles"])) {
                    $result = array_intersect($valScope["roles"], $roles);
                    foreach ($result as $keyI => $valueI) {
                        if (! empty($this->db->findOneById($valScope["type"], $keyS, ["name"])["name"])) {
                            ${$valueI}[] = $this->db->findOneById($valScope["type"], $keyS, ["name"])["name"];
                        } else {
                            ${$valueI}[] = null;
                        }
                    }
                }
            }

            foreach ($roles as $keyR => $valueR) {
                if (isset(${$valueR})) {
                    $elt[$valueR] = ${$valueR};
                }
            }
        }

        unset($elt["links"]);
        return $elt;
    }
}
