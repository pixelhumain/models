<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\ZoneInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;

use PixelHumain\Models\Traits\SearchNewTrait;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\ZoneTrait;

class City extends BaseModel implements CityInterface, SIGTraitInterface, ZoneTraitInterface, DataValidatorTraitInterface, SearchNewTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    use SIGTrait;
    use ZoneTrait;
    use DataValidatorTrait;
    use SearchNewTrait;

    public array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "alternateName" => [
            "name" => "alternateName",
            "rules" => ["required"],
        ],
        "insee" => [
            "name" => "insee",
            "rules" => ["required"],
        ],
        "country" => [
            "name" => "birthDate",
            "rules" => ["required"],
        ],
        "geo" => [
            "name" => "geo",
            "rules" => ["required", "geoValid"],
        ],
        "geoPosition" => [
            "name" => "geoPosition",
            "rules" => ["required", "geoPositionValid"],
        ],
        "geoShape" => [
            "name" => "geoShape",
        ],
        "postalCodes" => [
            "name" => "postalCodes",
        ],
        "osmID" => [
            "name" => "osmID",
        ],
        "wikidataID" => [
            "name" => "wikidataID",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "new" => [
            "name" => "new",
        ],
        "level1" => [
            "name" => "level1",
        ],
        "level2" => [
            "name" => "level2",
        ],
        "level3" => [
            "name" => "level3",
        ],
        "level4" => [
            "name" => "level4",
        ],
        "level1Name" => [
            "name" => "level1Name",
        ],
        "level2Name" => [
            "name" => "level2Name",
        ],
        "level3Name" => [
            "name" => "level3Name",
        ],
        "level4Name" => [
            "name" => "level4Name",
        ],
        "betweenCP" => [
            "name" => "betweenCP",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    public function insert($city, $userid)
    {
        ini_set('memory_limit', '-1');
        unset($city["save"]);
        $city["modified"] = $this->db->MongoDate(time());
        $city["updated"] = time();
        $city["creator"] = $userid;
        $city["created"] = time();
        $city["new"] = true;
        $city["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($city["geo"]["latitude"], $city["geo"]["longitude"]);
        $city["geoShape"] = self::getGeoShape($city["name"], $city["country"], $city["osmID"]);
        if ($city["geoShape"] == null) {
            unset($city["geoShape"]);
        }
        $postalCodes = [];

        if (! empty($city["postalCodes"])) {
            foreach ($city["postalCodes"] as $keyCP => $cp) {
                $newCP = [];
                $newCP["postalCode"] = $cp["postalCode"];
                $newCP["name"] = $cp["name"];
                $newCP["geo"] = $cp["geo"];
                $newCP["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($cp["geo"]["latitude"], $cp["geo"]["longitude"]);
                $postalCodes[] = $newCP;
            }
        }
        $city["postalCodes"] = $postalCodes;

        $level1 = $this->getModelZone()->getCountryByCountryCode($city["country"]);
        if (empty($level1)) {
            $level1 = $this->getModelZone()->createLevel(OpenData::$phCountries[$city["country"]], $city["country"], "1");
            $savelevel1 = $this->getModelZone()->save($level1);
            if ($savelevel1["result"] == true) {
                $level1 = $this->getModelZone()->getCountryByCountryCode($city["country"]);
            }
        }
        $city["level1"] = (string) $level1["_id"];
        $city["level1Name"] = $this->getModelZone()->getNameOrigin($city["level1"]);

        if (! empty($city["level2Name"])) {
            $level2 = $this->getModelZone()->getLevelByNameAndCountry($city["level2Name"], "2", $city["country"]);
            if (empty($level2)) {
                $level2 = $this->getModelZone()->createLevel($city["level2Name"], $city["country"], "2");
                $savelevel2 = $this->getModelZone()->save($level2);
                if ($savelevel2["result"] == true) {
                    $level2 = $this->getModelZone()->getLevelByNameAndCountry($city["level2Name"], "2", $city["country"]);
                }
            }
            if (! empty($level2["_id"])) {
                $city["level2"] = (string) $level2["_id"];
                $city["level2Name"] = $this->getModelZone()->getNameOrigin($city["level2"]);
            } else {
                unset($city["level2Name"]);
                unset($city["level2"]);
            }
        } else {
            unset($city["level2Name"]);
            unset($city["level2"]);
        }

        if (! empty($city["level3Name"])) {
            $level3 = $this->getModelZone()->getLevelByNameAndCountry($city["level3Name"], "3", $city["country"]);
            if (empty($level3)) {
                $level3 = $this->getModelZone()->createLevel($city["level3Name"], $city["country"], "3", ((! empty($city["level2Name"])) ? $city["level2Name"] : null));
                $savelevel3 = $this->getModelZone()->save($level3);
                if ($savelevel3["result"] == true) {
                    $level3 = $this->getModelZone()->getLevelByNameAndCountry($city["level3Name"], "3", $city["country"]);
                }
            }
            if (! empty($level3["_id"])) {
                $city["level3"] = (string) $level3["_id"];
                $city["level3Name"] = $this->getModelZone()->getNameOrigin($city["level3"]);
            } else {
                unset($city["level3Name"]);
                unset($city["level3"]);
            }
        } else {
            unset($city["level3Name"]);
            unset($city["level3"]);
        }

        if (! empty($city["level4Name"])) {
            $level4 = $this->getModelZone()->getLevelByNameAndCountry($city["level4Name"], "4", $city["country"]);
            if (empty($level4)) {
                $level4 = $this->getModelZone()->createLevel($city["level4Name"], $city["country"], "4", ((! empty($city["level2Name"])) ? $city["level2Name"] : null), ((! empty($city["level3Name"])) ? $city["level3Name"] : null));
                $savelevel4 = $this->getModelZone()->save($level4);
                if ($savelevel4["result"] == true) {
                    $level4 = $this->getModelZone()->getLevelByNameAndCountry($city["level4Name"], "4", $city["country"]);
                }
            }
            if (! empty($level4["_id"])) {
                $city["level4"] = (string) $level4["_id"];
                $city["level4Name"] = $this->getModelZone()->getNameOrigin($city["level4"]);
            } else {
                unset($city["level4Name"]);
                unset($city["level4"]);
            }
        } else {
            unset($city["level4Name"]);
            unset($city["level4"]);
        }

        //var_dump($city);
        try {
            $valid = $this->getModelDataValidator()->validate(ucfirst(CityInterface::CONTROLLER), json_decode(json_encode($city), true));
        } catch (CTKException $e) {
            $valid = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        //check insee
        if ($valid["result"]) {
            $exist = $this->db->findOne(CityInterface::COLLECTION, [
                "osmID" => $city["osmID"],
            ]);
            //var_dump(json_encode($city));
            if (empty($exist)) {
                //Yii::get('mongodb')->selectCollection(self::COLLECTION)->insert( $city );
                $city = $this->db->insert(CityInterface::COLLECTION, $city);
                $this->getModelZone()->insertTranslate(
                    (string) $city["_id"],
                    CityInterface::COLLECTION,
                    $city["country"],
                    $city["name"],
                    (! empty($city["osmID"]) ? $city["osmID"] : null),
                    (! empty($city["wikidataID"]) ? $city["wikidataID"] : null)
                );

                if (empty($level1["hasCity"])) {
                    $res = $this->db->update(
                        ZoneInterface::COLLECTION,
                        [
                            "_id" => $this->db->MongoId($city["level1"]),
                        ],
                        [
                            '$set' => [
                                "hasCity" => true,
                            ],
                        ]
                    );
                }

                $res = [
                    "result" => true,
                    "msg" => "La commune a été enregistrer.",
                    "city" => $city,
                    "id" => (string) $city["_id"],
                ];
            } else {
                $res = [
                    "result" => true,
                    "msg" => "La commune existe déjà",
                    "city" => $exist,
                    "id" => (string) $exist["_id"],
                ];
            }
        } else {
            $res = [
                "result" => false,
                "msg" => $this->language->t("common", "Something went really bad : " . $valid['msg']),
                "city" => json_encode($city),
            ];
        }
        return $res;
    }

    public function getGeoShape($name, $country, $osmID)
    {
        $resNominatim = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, $name, $country, true, true), true);
        foreach ($resNominatim as $key => $value) {
            if ($osmID == $value["osm_id"]) {
                return $value["geojson"];
            }
        }
        return null;
    }

    public function getInseeWikidataIDByCountry($country)
    {
        $wiki = [
            "FR" => "P374",
            "CH" => "P771",
            "ES" => "P772",
            "BR​" => "P1585",
            "MX" => null,
            "PE" => null,
        ];
        return ($wiki[$country] ?? false);
    }

    public function countryNotSplitCP($country)
    {
        $wiki = ["BR"];
        return (in_array($country, $wiki) ? false : true);
    }

    /* Retourne des infos sur la commune dans la collection cities" */
    public function getWhere($params, $fields = null, $limit = 20)
    {
        $city = $this->db->findAndSort(CityInterface::COLLECTION, $params, [
            "created" => 1,
        ], $limit, $fields);
        return $city;
    }

    public function getWhereFindOne($where, $fields = null)
    {
        $city = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);
        return $city;
    }

    public function getById($id, $fields = null)
    {
        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ], $fields);
        return $city;
    }

    public function getByInsee($insee)
    {
        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "insee" => $insee,
        ]);
        return $city;
    }

    public function getByOsmId($osmID)
    {
        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "osmID" => $osmID,
        ]);
        return $city;
    }

    public function getByPostalCode($cp, $countryCode = null, $fields = [])
    {
        $params = [
            'postalCodes' => [
                '$elemMatch' => [
                    'postalCode' => $cp,
                ],
            ],
        ];

        if (! empty($countryCode)) {
            $params["country"] = $countryCode;
        }

        $city = self::getWhere($params, $fields, 0);

        return $city;
    }

    /* Retourne des infos sur la commune dans la collection cityData" */
    public function getWhereData($params, $fields = null, $limit = 20, $sort = null)
    {
        ini_set('memory_limit', '-1');
        if (isset($sort)) {
            //var_dump($sort);
            $cityData = $this->db->findAndSort(CityInterface::COLLECTION_DATA, $params, $sort, $limit, $fields);
        } else {
            $cityData = $this->db->findAndSort(CityInterface::COLLECTION_DATA, $params, [
                "created" => 1,
            ], $limit, $fields);
        }

        return $cityData;
    }

    /* Retourne l'id d'une commune par rapport a son code insee */
    public function getIdByInsee($insee)
    {
        $id = null;
        $where = [
            "insee" => $insee,
        ];
        $cities = self::getWhere($where);
        foreach ($cities as $key => $value) {
            $id = $value["_id"];
        }
        return $id;
    }

    public function getUnikey($city)
    {
        //var_dump($city);
        if (@$city["cp"]) {
            return $city["country"] . "_" . $city["insee"] . "-" . $city["cp"];
        } elseif (@$city["postalCode"]) {
            return $city["country"] . "_" . $city["insee"] . "-" . $city["postalCode"];
        } elseif (@$city["postalCodes"]) {
            return $city["country"] . "_" . $city["insee"] . "-" . $city["postalCodes"][0]["postalCode"];
        }

        return false;
    }

    /**
     * Retrieves the Unikey map for a given key.
     *
     * @param string $key The key to retrieve the Unikey map for.
     * @return array The Unikey map for the given key.
     */
    public function getUnikeyMap(string $key): array
    {
        $keyStr = str_replace("-", "_", $key);
        $keyT = explode("_", $keyStr);
        $res = [
            "country" => $keyT[0],
            "insee" => $keyT[1],
        ];
        if (strpos($key, "-")) {
            $res["cp"] = $keyT[2];
        }
        return $res;
    }

    /* format unikey : COUNTRY_insee-cp */
    public function getByUnikey($unikey)
    {
        $country = substr($unikey, 0, strpos($unikey, "_"));
        //No cp for the unikey
        if (! strpos($unikey, "-")) {
            $insee = substr($unikey, strpos($unikey, "_") + 1);
        } else {
            $insee = substr($unikey, strpos($unikey, "_") + 1, strpos($unikey, "-") - strpos($unikey, "_") - 1);
            $cp = substr($unikey, strpos($unikey, "-") + 1, strlen($unikey));
        }
        //error_log("INSEE : ".$insee);
        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "insee" => $insee,
            "country" => $country,
        ]); // self::getWhere(array("insee"=>$insee, "country"=>$country));
        if (isset($cp)) {
            if (isset($city["postalCodes"])) {
                foreach ($city["postalCodes"] as $key => $value) {
                    if ($value["postalCode"] == $cp) {
                        $city["name"] = $value["name"];
                        $city["cp"] = $value["postalCode"];
                        $city["geo"] = $value["geo"];
                        $city["geoPosition"] = $value["geoPosition"];
                        return $city;
                    }
                }
            }
            //If look for a city with only the insee code and without the cp
        } elseif (! empty($city)) {
            return $city;
        }
        return null;
        //return array("country" => $country, "insee" => $insee, "cp" => $cp);
    }

    /* Retourne le code de la region d'une commune par rapport a son code insee */
    public function getCodeRegion($insee)
    {
        $where = [
            "insee" => $insee,
        ];
        $fields = ["region"];
        $region = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);
        return $region;
    }

    public function getLevelById($id)
    {
        $where = [
            "_id" => $this->db->MongoId($id),
        ];
        $fields = ["level1", "level1Name", "insee", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name", "level5", "level5Name", "country"];
        $city = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);
        return $city;
    }

    public function getLevelForNowList($loc)
    {
        $res = null;

        if (! empty($loc["level5"])) {
            $res = $loc["level5"];
        } elseif (! empty($loc["level4"])) {
            $res = $loc["level4"];
        } elseif (! empty($loc["level3"])) {
            $res = $loc["level3"];
        } elseif (! empty($loc["level2"])) {
            $res = $loc["level2"];
        } else {
            $res = $loc["level1"];
        }

        return $res;
    }

    /* Retourne le code du departement d'une commune par rapport a son code insee */
    public function getCodeDepartement($insee)
    {
        $where = [
            "insee" => $insee,
        ];
        $fields = ["dep"];
        $dep = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);
        return $dep;
    }

    /* Retourne le code de la region d'une commune par rapport a son code insee */
    public function getRegionCitiesByInsee($insee, $fields = null)
    {
        $region = self::getCodeRegion($insee);
        $where = [
            "region" => $region["region"],
        ];
        $cities = self::getWhere($where, $fields, 0);
        return $cities;
    }

    public function getDepartementCitiesByInsee($insee, $fields = null)
    {
        $region = self::getCodeRegion($insee);
        $dep = self::getCodeDepartement($insee);
        $where = [
            "region" => $region["region"],
            "dep" => $dep["dep"],
        ];
        $cities = self::getWhere($where, $fields, 0);
        return $cities;
    }

    public function getDepartementByInsee($insee, $fields, $typeData, $option = null, $inseeCities = null)
    {
        $sort = [];
        $mapDataDep = [];
        $cities = self::getDepartementCitiesByInsee($insee);
        if ($inseeCities == null) {
            $tabInsee = [];
            foreach ($cities as $key => $value) {
                array_push($tabInsee, $value["insee"]);
            }

            $where = [
                "insee" => [
                    '$in' => $tabInsee,
                ],
                $typeData => [
                    '$exists' => 1,
                ],
            ];
        } else {
            $where = [
                "insee" => [
                    '$in' => $inseeCities,
                ],
                $typeData => [
                    '$exists' => 1,
                ],
            ];
        }

        $fields[] = "insee";
        if (! empty($option)) {
            foreach ($option as $key => $value) {
                $fields[] = $typeData . $value;
                $sort[] = [
                    $typeData . $value => -1,
                ];
            }
        } else {
            $fields[] = $typeData;
            $sort = [
                $typeData => -1,
            ];
        }

        $cityData = $this->getWhereData($where, $fields, 30, $sort);
        foreach ($cityData as $key => $value) {
            foreach ($cities as $k => $v) {
                if (strcmp($v["insee"], $value["insee"]) == 0) {
                    $mapDataDep[$v["name"]] = [
                        $value["insee"] => [
                            $typeData => $value[$typeData],
                        ],
                    ];
                }
            }
        }
        return $mapDataDep;
    }

    public function getRegionByInsee($insee, $fields, $typeData, $option = null, $inseeCities = null)
    {
        $sort = [];
        $mapDataRegion = [];
        $cities = self::getRegionCitiesByInsee($insee);

        if ($inseeCities == null) {
            $tabInsee = [];
            foreach ($cities as $key => $value) {
                array_push($tabInsee, $value["insee"]);
            }

            $where = [
                "insee" => [
                    '$in' => $tabInsee,
                ],
                $typeData => [
                    '$exists' => 1,
                ],
            ];
        } else {
            $where = [
                "insee" => [
                    '$in' => $inseeCities,
                ],
                $typeData => [
                    '$exists' => 1,
                ],
            ];
        }

        $fields[] = "insee";
        if (! empty($option)) {
            foreach ($option as $key => $value) {
                $fields[] = $typeData . $value;
                $sort[] = [
                    $typeData . $value => -1,
                ];
            }
        } else {
            $fields[] = $typeData;
            $sort = [
                $typeData => -1,
            ];
        }
        $cityData = $this->getWhereData($where, $fields, 30, $sort);
        foreach ($cityData as $key => $value) {
            foreach ($cities as $k => $v) {
                if (strcmp($v["insee"], $value["insee"]) == 0) {
                    $mapDataRegion[$v["name"]] = [
                        $value["insee"] => [
                            $typeData => $value[$typeData],
                        ],
                    ];
                }
            }
        }

        return $mapDataRegion;
    }

    public function getDataByListInsee($listInsee, $type)
    {
        $mapData = [];

        $where = [
            "insee" => [
                '$in' => $listInsee,
            ],
            $type => [
                '$exists' => 1,
            ],
        ];
        $fields = [];
        $sort = [
            $type . ".2011.total" => -1,
        ];

        $cityData = $this->getWhereData($where, $fields, 30, $sort);
        foreach ($cityData as $key => $value) {
            $whereCity = [
                "insee" => $value["insee"],
            ];
            $fieldsCity = ["name"];
            $city = $this->getWhere($whereCity, $fieldsCity);
            foreach ($city as $keyCity => $valueCity) {
                $name = $valueCity["name"];
            }

            $mapData[$name] = [
                $value["insee"] => [
                    $type => $value[$type],
                ],
            ];
        }

        return $mapData;
    }

    public function getPopulationTotalInsee($insee, $years)
    {
        $where = [
            "insee" => $insee,
            "population" => [
                '$exists' => 1,
            ],
        ];
        $fields = ["population." . $years . ".total"];
        $cityData = $this->getWhereData($where, $fields);

        $totalPop = 1;
        foreach ($cityData as $key => $valueCity) {
            $totalPop = $valueCity['population'][$years]['total']['value'];
        }
        return $totalPop;
    }

    public function getPopulationHommesInsee($insee, $years)
    {
        $where = [
            "insee" => $insee,
            "population" => [
                '$exists' => 1,
            ],
        ];
        $fields = ["population." . $years . ".hommes.total"];
        $cityData = $this->getWhereData($where, $fields);

        $totalPop = 1;
        foreach ($cityData as $key => $valueCity) {
            $totalPop = $valueCity['population'][$years]['hommes']['total']['value'];
        }
        return $totalPop;
    }

    public function getPopulationFemmesInsee($insee, $years)
    {
        $where = [
            "insee" => $insee,
            "population" => [
                '$exists' => 1,
            ],
        ];
        $fields = ["population." . $years . ".femmes.total"];
        $cityData = $this->getWhereData($where, $fields);

        $totalPop = 1;
        foreach ($cityData as $key => $valueCity) {
            $totalPop = $valueCity['population'][$years]['femmes']['total']['value'];
        }
        return $totalPop;
    }

    public function getPopulationTotalInseeDepartement($insee, $years)
    {
        $fields = ["insee"];
        $cities = $this->getDepartementCitiesByInsee($insee, $fields);

        $count = 1;
        foreach ($cities as $idCities => $value) {
            $data = $this->getPopulationTotalInsee($value['insee'], $years);
            $count = $count + $data;
        }

        return $count;
    }

    public function getSimpleCityById($id)
    {
        $simpleCity = [];
        $city = $this->db->findOneById(CityInterface::COLLECTION, $id, [
            "id" => 1,
            "name" => 1,
            "insee" => 1,
            "cp" => 1,
            "geo" => 1,
        ]);

        $simpleCity["id"] = $id;
        $simpleCity["name"] = @$city["name"];
        $simpleCity["insee"] = @$city["insee"];
        $simpleCity["cp"] = @$city["cp"];
        $simpleCity["country"] = @$city["country"];
        $simpleCity["geo"] = @$city["geo"];
        $simpleCity["type"] = "city";

        return $simpleCity;
    }

    //rajoute les attributs "geoPosition" sur chaque City
    //en recopiant les valeurs de l'attribut "geo.longitude" et "geo.latitude"
    public function updateGeoPositions()
    {
        //récupère les villes qui on un attribut geo mais qui n'ont pas de geoPosition
        $request = [
            "geo" => [
                '$exists' => true,
            ],
            "geoPosition.float" => [
                '$exists' => false,
            ],
        ];

        //$maxCities = 300;
        $nbCities = 0;
        $cnt = 0;

        $allCities = $this->db->findAndSort(CityInterface::COLLECTION, $request, [
            "insee" => 1,
        ], 1000);
        error_log("------------------------------------------------------------");
        error_log("START update geoPosition Cities : " . (is_countable($allCities) ? count($allCities) : 0) . " trouvées");
        error_log("------------------------------------------------------------");
        error_log("L'opération peut durer entre 5 et 10 minutes");
        error_log("------------------------------------------------------------");

        for ($i = 0; $i < 40 && (is_countable($allCities) ? count($allCities) : 0) > 0; $i++) {
            $allCities = $this->db->findAndSort(CityInterface::COLLECTION, $request, [
                "insee" => 1,
            ], 1000);
            error_log("iteration n°" . $i . " - update geoPosition Cities : " . (is_countable($allCities) ? count($allCities) : 0) . " trouvées");
            error_log("###");
            error_log("###");

            //si on a trouvé une ville
            if ((is_countable($allCities) ? count($allCities) : 0) > 0) {
                foreach ($allCities as $key => $city) {
                    //if($nbCities > $maxCities) return null;
                    $nbCities++;
                    //on rajoute l'attribut geoPosition (type Point)
                    if (isset($city["geo"]["latitude"]) && isset($city["geo"]["longitude"])) {
                        $lat = $city["geo"]["latitude"];
                        $lng = $city["geo"]["longitude"];
                        if ($cnt <= 0) {
                            error_log("update " . $city["insee"] . " to geoPosition lat:" . $lat . " lng:" . $lng . " num : " . $nbCities);
                            $cnt = 50;
                        }
                        $cnt--;
                        $this->db->update(
                            CityInterface::COLLECTION,
                            [
                                "insee" => $city["insee"],
                                "name" => $city["name"],
                                "cp" => $city["cp"],
                            ],
                            [
                                '$set' =>
                                                                    [
                                                                        "geoPosition" =>
                                                                                                                [
                                                                                                                    "type" => "Point",
                                                                                                                    "float" => "true",
                                                                                                                    "coordinates" => [floatval($lng), floatval($lat)],
                                                                                                                ],
                                                                    ],
                            ]
                        );
                    }
                }
            }
        }
        error_log("La mise à jour est terminée. " . $nbCities . " communes ont été traitées");
        return true;
    }

    /* Retourne le code du departement d'une commune par rapport a son code insee */
    public function getAlternateNameByInseeAndCP($insee, $cp)
    {
        $where = [
            "insee" => $insee,
            "cp" => $cp,
        ];
        $fields = ["alternateName"];
        $dep = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);
        return $dep;
    }

    public function getCitiesForcheck()
    {
        $res = null;
        $where = [];
        $count = $this->db->count(CityInterface::COLLECTION, $where);

        $cities = [];
        $limit = 50;

        $zero = 0;
        if ($count % $limit > 0) {
            $zero = 1;
            $res = round($count / $limit, 0);
        }

        $result = $zero + $res;

        $i = 0;
        $index = 0;

        while ($i < $result) {
            set_time_limit(30);
            $rescities = $this->db->findAndLimitAndIndex(CityInterface::COLLECTION, $where, $limit, $index);
            $cities = array_merge($cities, $rescities);
            //$cities = $rescities;
            $index += $limit;
            $i++;
        }

        return $cities;
    }

    public function getCityByInseeCp($insee, $cp = null)
    {
        $where = [
            "insee" => $insee,
        ];

        if (! empty($cp)) {
            $where["postalCodes.postalCode"] = $cp;
        }

        $city = $this->db->findOne(CityInterface::COLLECTION, $where);

        if (isset($city["postalCodes"])) {
            foreach ($city["postalCodes"] as $key => $value) {
                if (! empty($cp) && $value["postalCode"] == $cp) {
                    $city["namePc"] = $value["name"];
                    $city["cp"] = $value["postalCode"];
                    $city["geo"] = $value["geo"];
                    $city["geoPosition"] = $value["geoPosition"];
                    return $city;
                } else {
                    $city["namePc"][] = $value["name"];
                    $city["cpArray"][] = $value["postalCode"];

                    if ($value["name"] == $city["name"]) {
                        $city["cp"] = $value["postalCode"];
                    }
                }
            }
        }

        if (empty($city["cp"])) {
            $city["cp"] = $city["cpArray"][0];
        }

        return $city;
    }

    public function getZone($insee, $cp = null, $zone = null)
    {
        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "insee" => $insee,
        ]);

        $zone = $this->db->findOne(CityInterface::ZONES, [
            "_id" => $insee,
        ]);

        return $city;
    }

    public function getCityByInsee($insee)
    {
        $where = [
            "insee" => $insee,
        ];
        //$fields = array("_id");
        $city = $this->db->findOne(CityInterface::COLLECTION, $where);
        return $city;
    }

    public function getInternationalCity($cp, $insee, $country = null, $name = null)
    {
        $where = [];

        if ($cp != null) {
            $where["postalCodes.postalCode"] = $cp;
        }
        if ($insee != null) {
            $where["insee"] = $insee;
        }
        if ($country != null) {
            $where["country"] = $country;
        }
        if ($name != null) {
            $where["postalCodes.name"] = $this->db->MongoRegex("/^" . (Yii::get('stringHelper')::wd_remove_accents($name)) . "/i");
            error_log($this->db->MongoRegex("/^" . (Yii::get('stringHelper')::wd_remove_accents($name)) . "/i"));
        }

        $city = $this->db->findOne(CityInterface::COLLECTION, $where);

        if (isset($city["postalCodes"])) {
            foreach ($city["postalCodes"] as $key => $value) {
                if ($value["postalCode"] == $cp) {
                    $city["name"] = $value["name"];
                    $city["cp"] = $value["postalCode"];
                    $city["geo"] = $value["geo"];
                    $city["geoPosition"] = $value["geoPosition"];
                    error_log("found : " . $city["name"]);
                    return $city;
                }
            }
        }
        return $city;
    }

    public function getCitiesWithWikiData($wikidataID, $newCities)
    {
        $newCities["wikidataID"] = $wikidataID;
        $wikidata = json_decode($this->getModelSIG()->getWikidata($wikidataID), true);
        $valWiki = @$wikidata["entities"][$wikidataID]["claims"];
        $arrayAdd = [];
        $arrayCp = [];
        $wikiInsee = $this->getInseeWikidataIDByCountry($newCities["country"]);
        if (! empty($wikiInsee)) {
            $newCities["insee"] = $valWiki[$wikiInsee][0]["mainsnak"]["datavalue"]["value"] . "*" . $newCities["country"];
        }

        $postalCodes = [];
        if (! empty($valWiki)) {
            if (! empty($valWiki["P281"])) {
                foreach ($valWiki["P281"] as $key => $cp) {
                    if ((strpos($cp["mainsnak"]["datavalue"]["value"], "–") > 0 || strpos($cp["mainsnak"]["datavalue"]["value"], "-") > 0)
                        && self::countryNotSplitCP($newCities["country"])) {
                        if (strpos($cp["mainsnak"]["datavalue"]["value"], "–")) {
                            $split = explode("–", $cp["mainsnak"]["datavalue"]["value"]);
                        } else {
                            $split = explode("-", $cp["mainsnak"]["datavalue"]["value"]);
                        }

                        $betweenCP["start"] = $split[0];
                        $betweenCP["end"] = $split[1];
                    } else {
                        $arrayCp[] = $cp["mainsnak"]["datavalue"]["value"];
                    }

                    foreach ($arrayCp as $keyCP => $valueCP) {
                        if (! in_array($valueCP, $arrayAdd)) {
                            $arrayAdd[] = $valueCP;
                            $postalCodes[] = [
                                "name" => $newCities["name"],
                                "postalCode" => $valueCP,
                                "geo" => [
                                    "@type" => "GeoCoordinates",
                                    "latitude" => $newCities["geo"]["latitude"],
                                    "longitude" => $newCities["geo"]["longitude"],
                                ],
                                "geoPosition" => [
                                    "type" => "Point",
                                    "float" => true,
                                    "coordinates" => [floatval($newCities["geo"]["longitude"]), floatval($newCities["geo"]["latitude"])],
                                ],
                            ];
                        }
                    }
                }
            }
        }
        $newCities["postalCodes"] = $postalCodes;

        if (! empty($betweenCP)) {
            $newCities["betweenCP"] = $betweenCP;
        }

        return $newCities;
    }

    public function checkCitySimply($city)
    {
        $res = false;
        if (! empty($city["name"]) && ! empty($city["alternateName"]) && ! empty($city["country"]) && ! empty($city["insee"])
            && ! empty($city["geo"]) && ! empty($city["geoPosition"])) {
            $res = true;
        }

        return $res;
    }

    public function getAllCities()
    {
        $cities = $this->getWhere([]);

        $res = [
            "goods" => [],
            "errors" => [],
            "news" => [],
        ];

        foreach ($cities as $key => $city) {
            $msg = self::checkCity($city);
            if (! empty($city["new"]) && $city["new"] == true) {
                if ($msg != "") {
                    $city["msgErrors"] = $msg;
                }
                $res["news"][(string) $city["_id"]] = $city;
            } elseif ($msg != "") {
                $city["msgErrors"] = $msg;
                $res["errors"][(string) $city["_id"]] = $city;
            } else {
                $res["goods"][(string) $city["_id"]] = $city;
            }
        }
        return $res;
    }

    public function checkCity($city)
    {
        $msgErrors = "";

        if (empty($city["name"])) {
            $msgErrors = "The name is missing.<br\>";
        }
        if (empty($city["alternateName"])) {
            $msgErrors = "The alternateName is missing.<br\>";
        }
        if (empty($city["insee"])) {
            $msgErrors = "The insee is missing.<br\>";
        }
        if (empty($city["country"])) {
            $msgErrors = "The country is missing.<br\>";
        }
        if (empty($city["dep"])) {
            $msgErrors = "The dep is missing.<br\>";
        }
        if (empty($city["depName"])) {
            $msgErrors = "The depName is missing.<br\>";
        }
        if (empty($city["region"])) {
            $msgErrors = "The region is missing.<br\>";
        }
        if (empty($city["regionName"])) {
            $msgErrors = "The regionName is missing.<br\>";
        }

        if (empty($city["geo"])) {
            $msgErrors = "The geo is missing.<br\>";
        }
        if (empty($city["geoPosition"])) {
            $msgErrors = "The geoPosition is missing.<br\>";
        }

        if (empty($city["geoShape"])) {
            $msgErrors = "The geoShape is missing.<br\>";
        }

        if (empty($city["postalCodes"])) {
            foreach ($city["postalCodes"] as $keyPC => $postalCode) {
                if (empty($postalCode["postalCode"])) {
                    $msgErrors = "The postalCode is missing.<br\>";
                }
                if (empty($postalCode["name"])) {
                    $msgErrors = "The name is missing for postal code " . $postalCode["postalCode"] . ".<br\>";
                }
                if (empty($postalCode["geo"])) {
                    $msgErrors = "The geo is missing for postal code " . $postalCode["postalCode"] . ".<br\>";
                }
                if (empty($postalCode["geoPosition"])) {
                    $msgErrors = "The geoPosition is missing for postal code " . $postalCode["postalCode"] . ".<br\>";
                }
            }
        }

        return $msgErrors;
    }

    public function getCityByCedex($cp)
    {
        $cityCedex = $this->db->findOne(CityInterface::COLLECTION, [
            "postalCodes.postalCode" => $cp,
            "postalCodes.complement" => [
                '$exists' => 1,
            ],
        ]);
        $res = null;
        if (! empty($cityCedex)) {
            foreach ($cityCedex["postalCodes"] as $key => $value) {
                if (! empty($value["complement"]) && $value["postalCode"] == $cp) {
                    $res["name"] = $value["name"];
                    $res["insee"] = $cityCedex["insee"];
                    $res["cp"] = $value["postalCode"];
                    $res["geo"] = $value["geo"];
                    $res["geoPosition"] = $value["geoPosition"];
                    $res["regionName"] = $cityCedex["regionName"];
                    $res["depName"] = $cityCedex["depName"];
                    $res["country"] = $cityCedex["country"];
                }
            }
        }
        return $res;
    }

    public function prepCity($params)
    {
        if (! empty($params["name"])) {
            $params["alternateName"] = mb_strtoupper($params["name"]);
        }

        if (! empty($params["latitude"]) && ! empty($params["longitude"])) {
            $params["geo"] = $this->getModelSIG()->getFormatGeo($params["latitude"], $params["longitude"]);
            $params["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($params["latitude"], $params["longitude"]);
        }

        if (! empty($params["wikidata"])) {
            $params["wikidataID"] = $params["wikidata"];
        }

        if (! empty($params["osmid"])) {
            $params["osmID"] = $params["osmid"];
        }

        if (! empty($params["postalCodes"])) {
            $newPostalCodes = [];
            foreach ($params["postalCodes"] as $keyCP => $valueCP) {
                $newCP = [];
                $newCP["postalCode"] = $valueCP["postalCode"];
                $newCP["name"] = $valueCP["name"];
                $newCP["geo"] = $this->getModelSIG()->getFormatGeo($valueCP["latitude"], $valueCP["longitude"]);
                $newCP["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($valueCP["latitude"], $valueCP["longitude"]);
                $newPostalCodes[] = $newCP;
            }
            $params["postalCodes"] = $newPostalCodes;
        }

        unset($params["osmid"]);
        unset($params["wikidata"]);
        unset($params["latitude"]);
        unset($params["longitude"]);

        return $params;
    }

    public function getZones($insee)
    {
        $zones = $this->db->findAndSort(CityInterface::ZONES, [
            "insee" => $insee,
        ], [
            "name" => 1,
        ]);
        return $zones;
    }

    public function getDetail($collection, $id)
    {
        $where = [
            "_id" => $this->db->MongoId($id),
        ];
        $zone = $this->db->findOne($collection, $where);
        return $zone;
    }

    public function getDetailFormInMap($id)
    {
        $where = [
            "_id" => $this->db->MongoId($id),
        ];
        $fields = ["geoShape", "osmID", "wikidataID", "betweenCP"];
        $city = $this->db->findOne(CityInterface::COLLECTION, $where, $fields);

        if (! empty($city["geoShape"]) && $city["geoShape"]["type"] == "Point") {
            unset($city["geoShape"]);
        }
        return $city;
    }

    public function getGeoShapeCity($params)
    {
        $where = [];
        $city = [];

        if (! empty($params["id"])) {
            $a = [];
            foreach ($params["id"] as $key => $value) {
                $a[] = $this->db->MongoId($value);
            }
            $where = [
                "_id" => [
                    '$in' => $a,
                ],
            ];
        } elseif (! empty($params["insee"])) {
            $a = [];
            foreach ($params["insee"] as $key => $value) {
                $a[] = $value;
            }
            $where = [
                "insee" => [
                    '$in' => $a,
                ],
            ];
        } elseif (! empty($params["name"])) {
            $a = [];
            foreach ($params["name"] as $key => $value) {
                $a[] = $value;
            }
            $where = [
                "name" => [
                    '$in' => $a,
                ],
            ];
        }
        if (! empty($where)) {
            $fields = ["geoShape", "name"];
            $city = $this->db->find(CityInterface::COLLECTION, $where, $fields);

            if (! empty($city["geoShape"]) && $city["geoShape"]["type"] == "Point") {
                unset($city["geoShape"]);
            }
        }

        return $city;
    }

    public function detailKeysLevels($key)
    {
        $keyArray = explode("@", $key);
        $res = [];
        if (isset($keyArray[1])) {
            $res["level1Key"] = $keyArray[0] . "@" . $keyArray[1];
            $res["level1"] = $keyArray[1];
        }

        if (isset($keyArray[2])) {
            $res["level2Key"] = $res["level1Key"] . "@" . $keyArray[2];
            $res["level2"] = $keyArray[2];
        }

        if (isset($keyArray[3])) {
            $res["level3Key"] = $res["level2Key"] . "@" . $keyArray[3];
            $res["level3"] = $keyArray[3];
        }

        if (isset($keyArray[4])) {
            $res["level4Key"] = $res["level3Key"] . "@" . $keyArray[4];
            $res["level4"] = $keyArray[4];
        }

        if (isset($keyArray[5])) {
            $res["cityKey"] = $res["level4Key"] . "@" . $keyArray[5];
            $res["city"] = $keyArray[5];
        }

        if (isset($keyArray[6])) {
            $res["cpKey"] = $res["cityKey"] . "@" . $keyArray[6];
            $res["cp"] = $keyArray[6];
        }
        return $res;
    }

    public function detailLevels($locality)
    {
        $res = [];
        if (isset($locality["level1"])) {
            $res["level1Name"] = $locality["level1Name"];
            $res["level1"] = $locality["level1"];
        }

        if (isset($locality["level2"])) {
            $res["level2Name"] = $locality["level2Name"];
            $res["level2"] = $locality["level2"];
        }

        if (isset($locality["level3"])) {
            $res["level3Name"] = $locality["level3Name"];
            $res["level3"] = $locality["level3"];
        }

        if (isset($locality["level4"])) {
            $res["level4Name"] = $locality["level4Name"];
            $res["level4"] = $locality["level4"];
        }

        if (isset($locality["level5"])) {
            $res["level5Name"] = $locality["level5Name"];
            $res["level5"] = $locality["level5"];
        }

        if (isset($locality["localityId"])) {
            $res["cityName"] = $locality["addressLocality"];
            $res["name"] = $locality["addressLocality"];
            $res["city"] = $locality["localityId"];
        }

        if (isset($locality["insee"])) {
            $res["insee"] = $locality["insee"];
        }

        if (isset($locality["postalCode"])) {
            $res["postalCode"] = $locality["postalCode"];
        }

        if (isset($locality["addressCountry"])) {
            $res["countryCode"] = $locality["addressCountry"];
        } elseif (isset($locality["country"])) {
            $res["country"] = $locality["country"];
        } elseif (isset($locality["countryCode"])) {
            $res["countryCode"] = $locality["countryCode"];
        }
        return $res;
    }

    public function detailsLocality($locality)
    {
        $res = self::detailLevels($locality);
        $userT = strtoupper($this->language->get());

        $res["level1Name"] = $this->getModelZone()->getNameCountry($res["level1"]);

        if (isset($res["localityId"])) {
            $res["cityName"] = self::getNameCity($res["localityId"]);
        }

        if (! empty($res["level2"])) {
            $res["level2Name"] = $this->getModelZone()->getNameCountry($res["level2"]);
        }

        if (! empty($res["level3"])) {
            $res["level3Name"] = $this->getModelZone()->getNameCountry($res["level3"]);
        }

        if (! empty($res["level4"])) {
            $res["level4Name"] = $this->getModelZone()->getNameCountry($res["level4"]);
        }

        if (! empty($res["level5"])) {
            $res["level5Name"] = $this->getModelZone()->getNameCountry($res["level5"]);
        }

        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "_id" => $this->db->MongoId($res["city"]),
        ], ["postalCodes"]);
        if (! empty($city["postalCodes"])) {
            $res["postalCodes"] = $city["postalCodes"];
        }
        if (! empty($city["postalCodes"]) && (is_countable($city["postalCodes"]) ? count($city["postalCodes"]) : 0) > 1) {
            foreach ($city["postalCodes"] as $key => $v) {
                $citiesNames[] = $v["name"];
            }
            $res["cities"] = $citiesNames;
        } elseif (is_string($res["postalCode"]) && strlen($res["postalCode"]) > 0) {
            if ($res["postalCode"]) {
                $where = [
                    "postalCodes.postalCode" => $this->db->MongoRegex("/^" . $res["postalCode"] . "/i"),
                    "country" => $res["countryCode"],
                ];
                $citiesResult = $this->db->find(CityInterface::COLLECTION, $where, ["_id"]);
                $citiesNames = [];
                foreach ($citiesResult as $key => $v) {
                    $citiesNames[] = $this->getNameCity($key);
                }
                $res["cities"] = $citiesNames;
            }
        }
        return $res;
    }

    public function createKey($city)
    {
        $key = $city["country"];
        $key .= "@" . ((empty($city["level1"])) ? "" : $city["level1"]);
        $key .= "@" . ((empty($city["level2"])) ? "" : $city["level2"]);
        $key .= "@" . ((empty($city["level3"])) ? "" : $city["level3"]);
        $key .= "@" . ((empty($city["level4"])) ? "" : $city["level4"]);
        $key .= "@" . ((empty($city["level5"])) ? "" : $city["level5"]);
        $key .= "@" . (string) $city["_id"];
        return $key;
    }

    public function alreadyExists($params)
    {
        $result = [
            "result" => false,
        ];
        if (! empty($params["osmId"])) {
            $where = [
                "osmId" => $params["osmId"],
            ];
            $city = $this->db->findOne(CityInterface::COLLECTION, $where);
            if (! empty($city)) {
                $result = [
                    "result" => true,
                    "city" => $city,
                ];
            }
        }

        return $result;
    }

    public function checkAndAddPostalCode($id, $postalCode)
    {
        $where = [
            "_id" => $this->db->MongoId($id),
        ];

        $city = $this->db->findOne(CityInterface::COLLECTION, $where, ["postalCodes", "geo", "name", "geoPosition"]);

        if (! empty($city)) {
            $add = true;
            foreach ($city["postalCodes"] as $key => $value) {
                if ($value["postalCode"] == $postalCode) {
                    $add = false;
                    break;
                }
            }

            if ($add == true) {
                $city["postalCodes"][] = [
                    "postalCode" => $postalCode,
                    "geo" => $city["geo"],
                    "geoPosition" => $city["geoPosition"],
                    "name" => $city["name"],
                ];
                $this->db->update(
                    CityInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($id),
                    ],
                    [
                        '$set' => [
                            "postalCodes" => $city["postalCodes"],
                        ],
                    ]
                );
            }
        }
    }

    public function getNameOrigin($id)
    {
        $translates = $this->getModelZone()->getTranslateById($id, CityInterface::COLLECTION);
        return $translates["origin"];
    }

    public function getNameCity($id)
    {
        $translates = $this->getModelZone()->getTranslateById($id, CityInterface::COLLECTION);
        $userT = strtoupper($this->language->get());
        if (! empty($translates)) {
            $name = (! empty($translates["translates"][$userT]) ? $translates["translates"][$userT] : $translates["origin"]);
        } else {
            $name = "";
        }
        return $name;
    }

    public function searchCity($countryCode, $scopeValue, $formInMap, $geoShape = false, $subParams = [])
    {
        $att = ["name", "alternateName", "country", "countryCode", "postalCodes", "insee", "level1", "level1Name", "level2", "level2Name", "level", "level3", "level3Name", "level4", "level4Name", "level5", "level5Name", "geo"];
        if ($geoShape) {
            $att[] = "geoShape";
        }
        $regex = $this->getModelSearchNew()->accentToRegex($scopeValue);
        $where = [
            '$or' => [
                [
                    "origin" => $this->db->MongoRegex("/" . $regex . "/i"),
                ],
                [
                    "translates." . strtoupper($this->language) => [
                        '$in' => [$this->db->MongoRegex("/" . $regex . "/i")],
                    ],
                ],
                [
                    "postalCodes.origin" => $this->db->MongoRegex("/" . $regex . "/i"),
                ],
                [
                    "postalCodes.postalCode" => $this->db->MongoRegex("/^" . $regex . "/i"),
                ],
            ],
        ];

        if (! empty($countryCode)) {
            $where = [
                '$and' => [
                    $where, [
                        "countryCode" => strtoupper($countryCode),
                    ]],
            ];
        } elseif (! empty($subParams["country"])) {
            $where = [
                '$and' => [
                    $where, [
                        "countryCode" => [
                            '$in' => $subParams["country"],
                        ],
                    ]],
            ];
        }

        $translate = $this->getModelZone()->getWhereTranlate($where);

        $cities = [];
        $zones = [];
        $valIDCity = [];
        $valIDZone = [];
        $nameArray = [];
        foreach ($translate as $key => $value) {
            if ($value["parentType"] == CityInterface::COLLECTION) {
                $valIDCity[] = $this->db->MongoId($value["parentId"]);
            }
            if ($value["parentType"] == ZoneInterface::COLLECTION) {
                $valIDZone[] = $this->db->MongoId($value["parentId"]);
            }
        }

        if (! empty($valIDCity) &&
            (empty($subParams["type"]) ||
                (! empty($subParams) && in_array(CityInterface::COLLECTION, $subParams["type"])))) {
            //$fields = array("name", "postalCodes", "country", "level1", "level1Name", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name", "insee","level5", "level5Name", "geo");
            $citiesWithTrad = $this->db->find(CityInterface::COLLECTION, [
                "_id" => [
                    '$in' => $valIDCity,
                ],
            ], $att);
            foreach ($citiesWithTrad as $keyTran => $city) {
                if (! empty($translate[$keyTran]["translates"][strtoupper($this->language->get())])) {
                    $city["name"] = $translate[$keyTran]["translates"][strtoupper($this->language->get())];
                }

                $city = self::getTranslateLevelCity($city);
                $cities[$keyTran] = $city;
            }
        }

        if (! empty($valIDZone) &&
            (empty($subParams["type"]) ||
                (! empty($subParams) && in_array(ZoneInterface::COLLECTION, $subParams["type"])))) {
            //$fields = array("name", "level", "countryCode", "level1", "level1Name", "level2", "level2Name",
            //"level3", "level3Name", "level4", "level4Name", "insee","level5", "level5Name", "geo");
            $pWh = [];
            $pWh[] = [
                "_id" => [
                    '$in' => $valIDZone,
                ],
            ];
            if (! empty($subParams["exists"])) {
                foreach ($subParams["exists"] as $keyE => $valE) {
                    $pWh[] = [
                        $keyE => [
                            '$exists' => $valE,
                        ],
                    ];
                }
            }

            $where = [
                '$and' => $pWh,
            ];

            $zonesWithTrad = $this->db->find(ZoneInterface::COLLECTION, $where, $att);
            foreach ($zonesWithTrad as $keyTran => $zone) {
                if (! empty($translate[$keyTran]["translates"][strtoupper($this->language->get())])) {
                    $zone["name"] = $translate[$keyTran]["translates"][strtoupper($this->language->get())];
                }
                $zones[$keyTran] = $zone;
            }
        }

        if (empty($cities) && ! empty($formInMap)) {
            $countryCode = mb_convert_encoding($countryCode, "ASCII");
            if (strlen($countryCode) > 2) {
                $countryCode = substr($countryCode, 0, 2);
            }
            $countryCode = mb_convert_encoding($countryCode, "UTF-8");
            $resNominatimCity = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, $scopeValue, trim($countryCode), true, true, true), true);

            if (empty($resNominatimCity)) {
                $resNominatimState = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, null, trim($countryCode), true, true, true, $scopeValue, true), true);

                $resNominatimCountry = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, null, trim($countryCode), true, true, true, $scopeValue, false, true), true);
                if (! empty($resNominatimCountry)) {
                    $resNominatim = array_merge($resNominatimState, $resNominatimCountry);
                } elseif (! empty($resNominatimState)) {
                    $resNominatim = $resNominatimState;
                } else {
                    $resNominatim = [];
                }
            } else {
                $resNominatim = $resNominatimCity;
            }

            $typeCities = ["city", "village", "town", "hamlet", "state", "county"];
            $typePlace = ["city", "village", "town", "hamlet"];
            $typeZone = ["state", "county"];

            $typeCities = ["city", "village", "town", "hamlet", "state"];
            $typePlace = ["city", "village", "town", "hamlet"];

            if (! empty($resNominatim)) {
                foreach (@$resNominatim as $key => $value) {
                    if (($value["class"] == "place" && $value["type"] == "city") ||
                        ($value["class"] == "boundary" && $value["type"] == "administrative") ||
                        ($value["class"] == "place" && $value["type"] == "town")) {
                        foreach ($typeCities as $keyType => $valueType) {
                            if (! empty($value["address"][$valueType])
                                && $countryCode == strtoupper(@$value["address"]["country_code"])) {
                                $name = null;
                                if (! empty($value["namedetails"])) {
                                    if (! empty($value["namedetails"]["name:" . strtolower($this->language->get())])) {
                                        $name = $value["namedetails"]["name:" . strtolower($this->language->get())];
                                    } elseif (! empty($value["namedetails"]["name"])) {
                                        $name = $value["namedetails"]["name"];
                                    }
                                }

                                $name = (! empty($name) ? $name : $value["address"][$valueType]);

                                if ((! in_array($keyType, $typeZone) ||
                                        (in_array($keyType, $typeZone) &&
                                            ! empty($value["extratags"]["place"]) &&
                                            in_array($value["extratags"]["place"], $typePlace))) &&
                                     ! in_array($value["osm_id"], $nameArray)) {
                                    $wikidata = (empty($value["extratags"]["wikidata"]) ? null : $value["extratags"]["wikidata"]);
                                    $newCities = [
                                        "name" => $name,
                                        "alternateName" => mb_strtoupper($name),
                                        "country" => $countryCode,
                                        "geo" => [
                                            "@type" => "GeoCoordinates",
                                            "latitude" => $value["lat"],
                                            "longitude" => $value["lon"],
                                        ],
                                        "geoPosition" => [
                                            "type" => "Point",
                                            "float" => true,
                                            "coordinates" => [floatval($value["lon"]), floatval($value["lat"])],
                                        ],
                                        "level3Name" => (empty($value["address"]["state"]) ? null : $value["address"]["state"]),
                                        "level3" => null,
                                        "level4Name" => (empty($value["address"]["county"]) ? null : $value["address"]["county"]),
                                        "level4" => null,
                                        "osmID" => $value["osm_id"],
                                        "save" => true,
                                    ];

                                    $nameArray[] = $value["osm_id"];

                                    if (! empty($wikidata)) {
                                        $newCities = $this->getCitiesWithWikiData($wikidata, $newCities);
                                    }

                                    if (empty($newCities["insee"])) {
                                        $newCities["insee"] = $value["osm_id"] . "*" . $countryCode;
                                    }

                                    if (empty($newCities["postalCodes"])) {
                                        $newCities["postalCodes"] = [];
                                    }

                                    if ($this->checkCitySimply($newCities)) {
                                        $cities[] = $newCities;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (! empty($cities) && empty($formInMap)) {
            $newCities = [];
            foreach ($cities as $key => $value) {
                $value["type"] = "city";
                if ((is_countable($value["postalCodes"]) ? count($value["postalCodes"]) : 0) == 1) {
                    $value["allCP"] = true;

                    if (! empty($value["postalCodes"][0]["postalCode"])) {
                        $where = [
                            "country" => $value["country"],
                            "postalCodes.postalCode" => $value["postalCodes"][0]["postalCode"],
                        ];
                        $countCP = $this->db->count(CityInterface::COLLECTION, $where);
                        $value["uniqueCp"] = (($countCP > 1) ? false : true);
                        $value["postalCode"] = $value["postalCodes"][0]["postalCode"];
                    }
                    $newCities[] = $value;
                } else {
                    if (empty($subParams["cp"]) || (! empty($subParams["cp"]) && $subParams["cp"] === true)) {
                        $newCities[] = $value;
                    }
                    $value["allCP"] = false;
                    foreach ($value["postalCodes"] as $keyCP => $valueCP) {
                        $cp = $value;
                        $where = [
                            "country" => $value["country"],
                            "postalCodes.postalCode" => $valueCP["postalCode"],
                        ];
                        $countCP = $this->db->count(CityInterface::COLLECTION, $where);
                        $cp["uniqueCp"] = (($countCP > 1) ? false : true);
                        $cp["postalCode"] = $valueCP["postalCode"];
                        $cp["nameCity"] = $value["name"];
                        $cp["name"] = $valueCP["name"];
                        $newCities[] = $cp;
                    }
                }
            }

            $cities = $newCities;
        }

        if (! empty($zones) && empty($formInMap)) {
            $newZones = [];
            foreach ($zones as $key => $value) {
                $value["type"] = "zone";
                $newZones[] = $value;
            }
            $cities = array_merge($cities, $newZones);
        }

        return $cities;
    }

    public function getTranslateLevelCity($city)
    {
        if (isset($city["level1"])) {
            $city["level1Name"] = $this->getModelZone()->getNameCountry($city["level1"]);
        }

        if (! empty($city["level2"])) {
            $city["level2Name"] = $this->getModelZone()->getNameCountry($city["level2"]);
        }

        if (! empty($city["level3"])) {
            $city["level3Name"] = $this->getModelZone()->getNameCountry($city["level3"]);
        }

        if (! empty($city["level4"])) {
            $city["level4Name"] = $this->getModelZone()->getNameCountry($city["level4"]);
        }

        return $city;
    }

    public function setCitiesByScope($scope)
    {
        $city = null;
        foreach ($scope as $key => $value) {
            $city = self::getById($value);
        }

        return $city;
    }

    public function createAddressByCity($city, $street = null, $postalCode = null)
    {
        $newA = [
            '@type' => 'PostalAddress',
            'addressCountry' => strtoupper($city["country"]),
            'localityId' => (string) $city["_id"],
            'level1' => $city["level1"],
            'level1Name' => $city["level1Name"],
        ];

        if (! empty($postalCode) && ! empty($city["postalCodes"])) {
            foreach ($city["postalCodes"] as $keyCp => $valueCp) {
                if ($valueCp["postalCode"] == $postalCode) {
                    $newA['addressLocality'] = $valueCp["name"];
                    $newA['postalCode'] = $postalCode;
                }
            }
        } elseif (empty($postalCode) && ! empty($city["postalCodes"])) {
            $newA['addressLocality'] = $city["postalCodes"][0]["name"];
            $newA['postalCode'] = $city["postalCodes"][0]["postalCode"];
        } else {
            $newA["addressLocality"] = $city["name"];
        }

        if (! empty($street)) {
            $newA["streetAddress"] = $street;
        }

        if (! empty($city["insee"])) {
            $newA["codeInsee"] = $city["insee"];
        }

        if (! empty($city["level2"])) {
            $newA["level2"] = $city["level2"];
            $newA["level2Name"] = $city["level2Name"];
        }
        if (! empty($city["level3"])) {
            $newA["level3"] = $city["level3"];
            $newA["level3Name"] = $city["level3Name"];
        }
        if (! empty($city["level4"])) {
            $newA["level4"] = $city["level4"];
            $newA["level4Name"] = $city["level4Name"];
        }

        if (empty($newA["postalCode"]) && ! empty($postalCode)) {
            $newA["postalCode"] = $postalCode;
        }

        $newA["name"] = $this->createNameAdress($newA);
        return $newA;
    }

    public function createNameAdress($address)
    {
        $name = "";

        if (! empty($address["streetAddress"])) {
            $name .= $address["streetAddress"];
        }

        if (! empty($address["postalCode"])) {
            if ($name != "") {
                $name .= ", ";
            }
            $name .= $address["postalCode"];
        }

        if (! empty($address["addressLocality"])) {
            if ($name != "") {
                $name .= ", ";
            }
            $name .= $address["addressLocality"];
        }

        if (! empty($address["level1Name"])) {
            if ($name != "") {
                $name .= ", ";
            }
            $name .= $address["level1Name"];
        }

        return $name;
    }

    public function getCitiesByLatLng($lat, $lon, $cp = null, $countryCode = null)
    {
        $request = [
            "postalCodes.geoPosition" => [
                '$exists' => true,
                '$near' => [
                    '$geometry' => [
                        "type" => "Point",
                        "coordinates" => [floatval($lon), floatval($lat)],
                    ],
                    '$maxDistance' => 5000,
                    '$minDistance' => 10,
                ],
            ],
        ];

        if ($cp != null) {
            $request = array_merge([
                "postalCodes.postalCode" => [
                    '$in' => [$cp],
                ],
            ], $request);
        }

        if ($countryCode != null) {
            $request = array_merge([
                "country" => [
                    '$in' => [$countryCode],
                ],
            ], $request);
        }

        $cities = $this->db->findAndSort(CityInterface::COLLECTION, $request, []);

        return $cities;
    }

    public function getCityByLatLng($lat, $lon, $cp = null, $countryCode = null)
    {
        $request = [
            "geoShape" => [
                '$exists' => true,
                '$near' => [
                    '$geometry' => [
                        "type" => "Point",
                        "coordinates" => [floatval($lon), floatval($lat)],
                    ],
                    '$maxDistance' => 5000,
                    '$minDistance' => 0,
                ],
            ],
        ];
        if (! empty($requestCP)) {
            $city = $this->db->findOne(CityInterface::COLLECTION, $requestCP, []);
        }

        if (empty($city)) {
            $city = $this->db->findOne(CityInterface::COLLECTION, $request, []);
        }

        return $city;
    }

    public function createLocality($city, $street, $lat, $lon, $cp = null)
    {
        $address = $this->createAddressByCity($city, $street, $cp);
        $geo = $this->getModelSIG()->getFormatGeo($lat, $lon);
        $geoPosition = $this->getModelSIG()->getFormatGeoPosition($lat, $lon);
        $res = [
            "address" => $address,
            "geo" => $geo,
            "geoPosition" => $geoPosition,
        ];
        return $res;
    }
}
