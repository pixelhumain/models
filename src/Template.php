<?php

namespace PixelHumain\Models;

use Exception;
use PixelHumain\Models\Interfaces\TemplateInterface;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;

class Template extends BaseModel implements TemplateInterface, DocumentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DocumentTrait;

    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public static array $dataBinding = [
        "id" => [
            "name" => "id",
        ],
        "section" => [
            "name" => "section",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "category" => [
            "name" => "category",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "name" => [
            "name" => "name",
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "geo" => [
            "name" => "geo",
        ],
        "geoPosition" => [
            "name" => "geoPosition",
        ],
        "description" => [
            "name" => "description",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "position" => [
            "name" => "position",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "structags" => [
            "name" => "structags",
        ],
        "level" => [
            "name" => "level",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "rank" => [
            "name" => "rank",
        ],
        "path" => [
            "name" => "path",
        ],
        "haveTpl" => [
            "name" => "haveTpl",
        ],
        "page" => [
            "name" => "page",
        ],
        "allPage" => [
            "name" => "choosePage",
        ],
        "gitlab" => [
            "name" => "gitlab",
        ],
        "tplParent" => [
            "name" => "tplParent",
        ],
        "templateParent" => [
            "name" => "templateParent",
        ],
        "order" => [
            "name" => "order",
        ],
        "tplsUser" => [
            "name" => "tplsUser",
        ],
        "cmsList" => [
            "name" => "cmsList",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "source" => [
            "name" => "source",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "color" => [
            "name" => "color",
        ],
        "siteParams" => [
            "name" => "siteParams",
        ],
        "dontRender" => [
            "name" => "dontRender",
        ],
    ];

    /**
     * Get a template by its ID.
     *
     * @param string $id The ID of the template.
     * @return array The template data.
     */
    public function getById(string $id): array
    {
        $template = $this->db->findOneById(TemplateInterface::COLLECTION, $id);

        if ($template == null) {
            throw new Exception("Template not found");
        }

        // Use case notragora
        $template["images"] = $this->listImages($id);
        $template["files"] = $this->listFiles($id);
        return $template;
    }
}
