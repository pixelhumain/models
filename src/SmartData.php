<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\SmartDataInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

// TODO : est ce que cette class est utile ?
class SmartData extends BaseModel implements SmartDataInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Retrieves scalar data from the SmartData object.
     *
     * @param int $indexStep The step size for retrieving the data. Default is 10.
     * @param int $indexMin The minimum index value for retrieving the data. Default is 0.
     * @return array An array containing the scalar data.
     */
    public function getScalairData(int $indexStep = 10, int $indexMin = 0): array
    {
        $datas = $this->db->findAndSortAndLimitAndIndex(
            SmartDataInterface::SCALAIR_COLLECTION,
            [],
            [],
            $indexStep,
            $indexMin
        );
        return $datas;
    }
}
