<?php

namespace PixelHumain\Models;

use Exception;
use PixelHumain\Models\Interfaces\RoleInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;

/**
 * Represents a role in the system.
 */
class Role extends BaseModel implements RoleInterface, PersonTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Returns the default roles.
     *
     * @return array The default roles.
     */
    public function getDefaultRoles(): array
    {
        // Manage roles of user
        $roles = [];

        // if(strpos($_SERVER['SERVER_NAME'], "qa.")! == 0 ){
        // 	$roles["tobeactivated"] = true;
        // }

        if (! $this->params->get('personNotValidated') || $this->params->get('personNotValidated') !== true) {
            $roles["tobeactivated"] = true;
        }

        // By default no one is beta tester in a betaTest mode
        if ($this->params->get('betaTest')) {
            $roles["betaTester"] = false;
        }

        // Can access standalone Pages : true
        $roles["standalonePageAccess"] = true;

        return $roles;
    }

    /**
     * Check the roles of a user.
     *
     * @param array|null $person The user object.
     * @return array The roles of the user.
     */
    private function checkUserRoles(?array $person): array
    {
        $roles = [];
        if (empty($person)) {
            return $roles;
        }

        if (empty($person["roles"]) && ! empty($person["_id"])) {
            //Maybe it's an old user : we add the default role
            //And retrieve the tobevalidated indicator

            $roles = $this->getDefaultRoles();

            //desactiver tobeactivated
            if (stripos($_SERVER['SERVER_NAME'] ?? '', "127.0.0.1") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "localhost") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "::1") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "localhost:8080") === false && strpos($_SERVER['SERVER_NAME'] ?? '', "local.") !== 0
                && strpos($_SERVER['SERVER_NAME'] ?? '', "qa.") !== 0) {
                if (isset($person["tobeactivated"])) {
                    $roles["tobeactivated"] = $person["tobeactivated"];
                }
            }

            $this->getModelPerson()->updatePersonField((string) $person["_id"], "roles", $roles, (string) $person["_id"]);
        } else {
            $roles = $person["roles"] ?? [];
        }

        return (array) $roles;
    }

    /**
     * Check user roles authorization.
     *
     * @param array $person The person object.
     * @return array The roles of the user.
     */
    public function checkUserRolesAuthorization($person): array
    {
        return $this->checkUserRoles($person);
    }

    /**
     * Check if a user can login.
     *
     * @param array $person The person object.
     * @param bool $isRegisterProcess Whether the login is part of the registration process. (not used in this function)
     * @return array The result of the login check.
     */
    public function canUserLogin(array $person, bool $isRegisterProcess = false): array
    {
        $res = [
            "result" => true,
            "msg" => "Everything is ok : user can login !",
        ];

        //check if the user has been created with minimal data
        if (isset($person["pending"])) {
            return [
                "result" => false,
                "pendingUserId" => (string) $person["_id"],
                "pendingUserEmail" => $person["email"],
                "msg" => "accountPending",
            ];
        }

        $roles = $this->checkUserRoles($person);

        if ($this->params->get('betaTest')) {
            if ($this->params->get('betaTest') === true) {
                if (isset($roles["betaTester"])) {
                    if (! $roles["betaTester"]) {
                        return [
                            "result" => false,
                            "msg" => "betaTestNotOpen",
                        ];
                    }
                }
            }
        }

        //The account is not validated.
        if (isset($roles["tobeactivated"])) {
            return [
                "result" => false,
                "id" => @$person['_id'],
                "msg" => "notValidatedEmail",
            ];
        }
        //The account is not validated.
        if (isset($roles["isBanned"])) {
            return [
                "result" => false,
                "id" => @$person['_id'],
                "msg" => $this->language->t("common", "Your account has been certified as fraudulent towards the policies of respect"),
            ];
        }

        return $res;
    }

    /**
     * Check if the user has the super admin role.
     *
     * @param array|null $roles The roles of the user.
     * @return bool Returns true if the user has the super admin role, false otherwise.
     */
    public function isUserSuperAdmin(?array $roles): bool
    {
        if (is_array($roles) && isset($roles[RoleInterface::SUPERADMIN])) {
            return (bool) $roles[RoleInterface::SUPERADMIN];
        }
        return false;
    }

    /**
     * Check if the user has the super admin CMS role.
     *
     * @param array|null $roles The roles of the user.
     * @return bool Returns true if the user has the super admin CMS role, false otherwise.
     */
    public function isUserSuperAdminCms(?array $roles): bool
    {
        return ($roles && isset($roles[RoleInterface::SUPERADMINCMS])) ? (bool) $roles[RoleInterface::SUPERADMINCMS] : false;
    }

    // /**
    //  * Check if the user is a beta tester.
    //  *
    //  * @param array $roles The roles of the user.
    //  * @return bool Returns true if the user is a beta tester, false otherwise.
    //  */
    // public function isUserBetaTester($roles) {
    // 	return (@$this->params['betaTest'] && @$roles["betaTester"]) ? true : false;
    // }

    /**
     * Check if a user has the specified roles.
     *
     * @param array $userRoles The roles of the user.
     * @param array $roles The roles to check against.
     * @return bool Returns true if the user has any of the specified roles, false otherwise.
     */
    public function isUser(array $userRoles = [], array $roles): bool
    {
        return count(array_intersect($userRoles, $roles)) > 0;
    }

    /**
     * Check if the given roles belong to a super admin.
     *
     * @param array|null $roles The roles to check.
     * @return bool Returns true if the roles belong to a super admin, false otherwise.
     */
    public function isSuperAdmin(?array $roles): bool
    {
        return ($roles && isset($roles[RoleInterface::SUPERADMIN])) ? (bool) $roles[RoleInterface::SUPERADMIN] : false;
    }

    // /**
    //  * Check if the given roles include the developer role.
    //  *
    //  * @param array $roles The roles to check.
    //  * @return bool Returns true if the given roles include the developer role, false otherwise.
    //  */
    // public function isDeveloper($roles) {
    // 	return (@$roles[self::DEVELOPER]) ? true : false;
    // }

    /**
     * Check if the role is a source admin.
     *
     * @param array|null $roles The roles to check.
     * @return bool Returns true if the role is a source admin, false otherwise.
     */
    public function isSourceAdmin(?array $roles): bool
    {
        if (is_array($roles) && array_key_exists(RoleInterface::SOURCEADMIN, $roles)) {
            return (bool) $roles[RoleInterface::SOURCEADMIN];
        }
        return false;
    }

    /**
     * Check if a user is activated.
     *
     * @param string $id The user ID.
     * @return bool Returns true if the user is activated, false otherwise.
     */
    public function isUserActivated(string $id): bool
    {
        $personRole = $this->getModelPerson()->getRolesById($id);
        if (is_array($personRole) && isset($personRole["roles"])) {
            // Vérifie si le rôle "tobeactivated" est défini et retourne l'inverse de sa valeur
            return empty($personRole["roles"]["tobeactivated"]);
        }
        return false;
    }

    /**
     * Update the role of a person.
     *
     * @param string $action The action to perform.
     * @param string $userId The ID of the user.
     * @return array The result of the update.
     */
    public function updatePersonRole(string $action, string $userId): array
    {
        $role = null;
        $mongoAction = '$set';
        $roleValue = true;
        if ($action == RoleInterface::REVOKE_BETA_TESTER || $action == RoleInterface::REVOKE_SUPER_ADMIN || $action == RoleInterface::REVOKE_BANNED_USER) {
            $mongoAction = '$unset';
            $roleValue = "";
        }

        if ($action == RoleInterface::ADD_BETA_TESTER || $action == RoleInterface::REVOKE_BETA_TESTER) {
            $role = 'betaTester';
        } elseif ($action == RoleInterface::ADD_SUPER_ADMIN || $action == RoleInterface::REVOKE_SUPER_ADMIN) {
            $role = RoleInterface::SUPERADMIN;
        } elseif ($action == RoleInterface::ADD_BANNED_USER || $action == RoleInterface::REVOKE_BANNED_USER) {
            $role = 'isBanned';
        }

        // si role est null on renvoit false
        $result = $role ? $this->getModelPerson()->updatePersonRole($userId, $mongoAction, $role, $roleValue) : false;

        return $result ? [
            "result" => true,
            "msg" => "The role has been updated",
        ] : [
            "result" => false,
            "msg" => "The role has not been updated",
        ];
    }


    // //TODO SBAR - is it still used ?
    // /**
    //  * Sets the roles for a specific item.
    //  *
    //  * @param array $roleTab The array of roles to set.
    //  * @param string $itemId The ID of the item.
    //  * @param string $itemType The type of the item.
    //  * @return void
    //  */
    // // public function setRoles($roleTab, $itemId, $itemType){
    // // 	PHDB::update( $itemType,
    // // 					array("_id" => PHDB::MongoId($itemId)),
    // //                     array('$set' => array( 'roles' => $roleTab))
    // //                 );
    // // }

    /**
     * Retrieves the roles of a user by their ID.
     *
     * @param string $id The ID of the user.
     * @return array The roles of the user.
     */
    public function getRolesUserId(string $id): array
    {
        $personRole = $this->getModelPerson()->getRolesById($id);
        if (! empty($personRole["roles"])) {
            return (array) $personRole["roles"];
        } else {
            return [];
        }
    }
}
