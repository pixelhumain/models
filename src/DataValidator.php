<?php

namespace PixelHumain\Models;

use DateTime;
use Exception;
use InvalidArgumentException;
use MongoDate;
use PixelHumain\Models\Exception\CTKException;

use PixelHumain\Models\Interfaces\DataValidatorInterface;
use PixelHumain\Models\Interfaces\HtmlPurifierInterface;



use PixelHumain\Models\Traits\BaseModel\I18NTrait;

use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;

class DataValidator extends BaseModel implements DataValidatorInterface, CityTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CityTrait;

    protected HtmlPurifierInterface $htmlPurifier;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateI18nProperty();

        if (! $this->htmlPurifier instanceof HtmlPurifierInterface) {
            throw new InvalidArgumentException('The htmlPurifier property is not set or is not an instance of HtmlPurifierInterface.');
        }
    }

    /**
     * Clears the user input by removing any unwanted characters or formatting.
     *
     * @param mixed $data The user input data to be cleared.
     * @return mixed The cleared user input data.
     */
    public function clearUserInput($data)
    {
        if (is_string($data)) {
            return $this->cleanString($data);
        } elseif (is_array($data)) {
            return $this->cleanArray($data);
        }
        return $data;
    }

    /**
     * Cleans a string by using HTMLPurifier to remove any potentially malicious code.
     *
     * @param string $string The string to be cleaned.
     * @return string The cleaned string.
     */
    private function cleanString($string)
    {
        return $this->htmlPurifier::process($string, $this->getPurifierConfig());
    }

    /**
     * Cleans an array by removing any potentially harmful or unwanted data.
     *
     * @param array $data The array to be cleaned.
     * @return array The cleaned array.
     */
    private function cleanArray(array $data)
    {
        $keys = ["tags", "urls"];
        foreach ($data as $key => $value) {
            if (in_array($key, $keys)) {
                $data[$key] = $this->cleanSpecialKeys($data[$key], $key);
            } elseif (is_array($value)) {
                $data[$key] = $this->cleanArray($value);
            } elseif (is_string($value)) {
                $data[$key] = $this->cleanString($value);
            }
        }
        return $data;
    }

    /**
     * Cleans special keys from an array of values.
     *
     * @param array $values The array of values to clean.
     * @param mixed $key The key to clean.
     * @return array The cleaned array of values.
     */
    private function cleanSpecialKeys(array $values, $key)
    {
        $values = array_map(fn ($item) => $key === 'tags' ? str_replace("&amp;", "&", $item) : $item, $values);

        $values = array_filter($values, fn ($item) => $item !== '');

        return array_values($values);
    }

    /**
     * Returns the configuration for the HTMLPurifier library.
     *
     * @return array The configuration array for HTMLPurifier.
     */
    private function getPurifierConfig()
    {
        return [
            'URI.AllowedSchemes' => [
                'http' => true,
                'https' => true,
            ],
            'HTML.TargetBlank' => true,
            'HTML.SafeIframe' => true,
            'URI.SafeIframeRegexp' => '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.dailymotion\.com/embed/)%',
        ];
    }

    // /**
    //  * Clears the user input by removing any unwanted characters or formatting.
    //  *
    //  * @param mixed $data The user input data to be cleared.
    //  * @return mixed The cleared user input data.
    //  */
    // public function clearUserInput($data){
    // 	if(is_string($data)){
    // 		$this->htmlPurifier::process($data, array('URI.AllowedSchemes'=>array(
    //                       'http' => true,
    //                       'https' => true,
    //                     ),
    //                     'HTML.TargetBlank'=>true,
    //                     'HTML.SafeIframe' => true,
    //                     'URI.SafeIframeRegexp' =>  '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/|www\.dailymotion\.com/embed/)%'
    //             ));
    // 	}else if(is_array($data)){
    // 		foreach($data as $k => $v){
    // 			$data[$k] = $this->clearUserInput($v);

    // 			$keys = array("tags", "urls");
    // 			if (in_array($k, $keys)){
    // 				foreach($data[$k] as $kv => $vv){
    // 					if($k == 'tags' && !empty($data[$k][$kv]))
    // 						$data[$k][$kv] = str_replace("&amp;", "&", $data[$k][$kv]);

    // 					if($data[$k][$kv] === '')
    // 						unset($data[$k][$kv]);
    // 				}

    // 				if(count($v) !== count($data[$k] )){
    // 					$data[$k] = array_filter($data[$k]);
    // 					$data[$k] = array_values($data[$k]);
    // 				}
    // 				if(is_array($data[$k])){
    // 					if(count($data[$k]) < 1){
    // 						unset($data[$k]);
    // 					}
    // 				}
    // 			}
    // 		}
    // 	}

    // 	return $data;
    // }

    /**
     * Validates an email address.
     *
     * @param string|null $toValidate The email address to validate.
     * @param mixed $object (optional) The object associated with the email address.
     * @param mixed $objectId (optional) The ID of the object associated with the email address.
     * @return string The validation result message.
     */
    public function email(?string $toValidate, $object = null, $objectId = null): string
    {
        if (empty($toValidate)) {
            return "";
        }

        if (! $this->isValidEmailFormat($toValidate)) {
            return $this->language->t("common", "The email is not well formatted");
        }

        if (! $this->isValidEmailDomain($toValidate)) {
            return $this->language->t("common", "Unknown domain: please check your email!");
        }

        return "";
    }

    /**
     * Check if the given email has a valid format.
     *
     * @param string $email The email to validate.
     * @return bool Returns true if the email has a valid format, false otherwise.
     */
    private function isValidEmailFormat(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }

    /**
     * Check if the email domain is valid.
     *
     * @param string $email The email address to validate.
     * @return bool Returns true if the email domain is valid, false otherwise.
     */
    private function isValidEmailDomain(string $email): bool
    {
        $domain = substr(strrchr($email, "@"), 1);
        // Attention il peut y avoir des faux positif suivant la config du serveur du domaine (si pas de MX)
        return checkdnsrr($domain, "MX") || checkdnsrr($domain, "ANY");
    }

    // /**
    //  * Validates an email address.
    //  *
    //  * @param string|null $toValidate The email address to validate.
    //  * @return string The error message if the email address is invalid, an empty string otherwise.
    //  */
    // public function email(?string $toValidate):string {
    // 	$res = "";
    // 	if (! preg_match('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#',$toValidate) && !empty($toValidate)) {
    // 		$res = $this->language->t("common", "The email is not well formated");
    // 	} else {
    // 		$domain = explode("@",$toValidate);
    //     	$domain = array_pop($domain);
    // 		if(!empty($domain) && ! (checkdnsrr($domain,"MX") || checkdnsrr($domain,"ANY"))){
    // 			$res = $this->language->t("common", "Unknown domain : please check your email !");
    // 		}
    // 	}
    // 	return $res;
    // }

    /**
     * Retrieves the field name from the collection and performs validation on the field value.
     *
     * @param array $dataBinding The collection of data bindings.
     * @param string $fieldName The name of the field to retrieve and validate.
     * @param mixed $fieldValue The value of the field to validate.
     * @param mixed $object The object associated with the field (optional).
     * @param bool|null $import Indicates if the validation is for import (optional).
     * @param string|null $id The ID of the field (optional).
     * @return string The name of the field.
     */
    public function getCollectionFieldNameAndValidate(array $dataBinding, string $fieldName, $fieldValue, $object = null, ?bool $import = null, ?string $id = null): string
    {
        if (! isset($dataBinding[$fieldName])) {
            throw new CTKException("Champ inconnu : " . $fieldName);
        }

        $data = $dataBinding[$fieldName];
        $name = $data["name"] ?? "noname";

        if (! isset($data["rules"])) {
            return $name;
        }

        $rules = $data["rules"];
        $functionImport = ["organizationSameName", "getDateTimeFromString", "eventEndDate", "eventStartDate"];

        foreach ($rules as $rule) {
            if (! empty($import) && in_array($rule, $functionImport)) {
                continue;
            }

            if (! method_exists($this, $rule)) {
                throw new CTKException("Méthode inconnue : " . $rule);
            }

            $arguments = [$fieldValue];
            if ($object !== null) {
                $arguments[] = $object;
            }
            if ($id !== null) {
                $arguments[] = $id;
            }

            try {
                $isDataValidated = $this->$rule(...$arguments);
                if ($isDataValidated != "") {
                    throw new CTKException($isDataValidated);
                }
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $name;
    }

    /**
     * Validates the address data.
     *
     * @param array $toValidate The address data to validate.
     * @return string The validation result.
     */
    public function addressValid(array $toValidate): string
    {
        $res = "";

        // error_log("AddressValid = ".json_encode($toValidate));

        if (empty($toValidate["addressCountry"])) {
            return "Country missing in the address !";
        }

        if (empty($toValidate["addressLocality"])) {
            return "City name missing in the address !";
        }

        if (empty($toValidate["osmID"]) && empty($toValidate["localityId"])) {
            return "CityId missing in the address !";
        }

        if (! empty($toValidate["localityId"])) {
            $city = $this->city->getById($toValidate["localityId"]);
            if ($city["country"] != $toValidate["addressCountry"]) {
                return "Invalid CityId with that country !";
            }
        }

        return $res;
    }

    /**
     * Validates the geo data.
     *
     * @param array $toValidate The data to be validated.
     * @return string The validation result.
     */
    public function geoValid(array $toValidate): string
    {
        $res = "";
        // error_log("geoValid = ".json_encode($toValidate));
        if (! empty($toValidate["addressesIndex"])) {
            unset($toValidate["addressesIndex"]);
        }

        if (! empty($toValidate)) {
            //Check type
            if (empty($toValidate["@type"])) {
                return "Type missing in the geo !";
            }
            if ($toValidate["@type"] != "GeoCoordinates") {
                return "Type missing in the geo !";
            }
            //Check latitude
            if (empty($toValidate["latitude"])) {
                return "latitude missing in the geo !";
            }
            if (! is_string($toValidate["latitude"])) {
                return "latitude is not a string in the geo !";
            }
            //Check longitude
            if (empty($toValidate["longitude"])) {
                return "longitude Code missing in the geo !";
            }
            if (! is_string($toValidate["longitude"])) {
                return "longitude is not a string in the geo !";
            }
        }
        return $res;
    }

    /**
     * Validates the geo position data.
     *
     * @param array $toValidate The data to validate.
     * @return string The validation result.
     */
    public function geoPositionValid(array $toValidate): string
    {
        $res = "";
        // error_log("geoPosition = ".json_encode($toValidate));
        if (! empty($toValidate["addressesIndex"])) {
            unset($toValidate["addressesIndex"]);
        }

        if (! empty($toValidate)) {
            //Check type
            if (empty($toValidate["type"])) {
                return "Type missing in the geoPosition !";
            }
            if ($toValidate["type"] != "Point") {
                return "Type missing in the geoPosition !";
            }
            //Check longitude
            if (empty($toValidate["coordinates"][0])) {
                return "longitude Code missing in the geoPosition !";
            }
            if (! is_float($toValidate["coordinates"][0])) {
                return "longitude is not a float in the geoPosition !";
            }
            //Check latitude
            if (empty($toValidate["coordinates"][1])) {
                return "latitude missing in the geoPosition !";
            }
            if (! is_float($toValidate["coordinates"][1])) {
                return "latitude is not a float in the geoPosition !";
            }
        }

        return $res;
    }

    /**
     * Converts a string representation of a date and time into a DateTime object.
     *
     * @param string|MongoDate $myDate The string representation of the date and time value or a MongoDate object.
     * @param string $label The label for the date and time value.
     * @return DateTime|MongoDate The DateTime object or the MongoDate object.
     */
    public function getDateTimeFromString($myDate, string $label)
    {
        if ($myDate instanceof MongoDate) {
            return $myDate;
        }

        $formats = [
            'Y-m-d H:i', 'Y-m-d', 'Y-m-d H:i:s',
            'Y-d-m', 'Y-d-m H:i:s', 'd/m/Y H:i:s',
            'd/m/Y', 'd/m/Y H:i', DateTime::ISO8601,
            "Y-m-d\TH:i",
        ];

        foreach ($formats as $format) {
            $result = DateTime::createFromFormat($format, $myDate);
            if ($result) {
                return $result;
            }
        }

        throw new CTKException("The format of '$label' is incorrect. Unable to parse '$myDate'.");
    }
}
