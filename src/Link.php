<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActionsInterface;
use PixelHumain\Models\Interfaces\CrowdfundingInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\LinkInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;

use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Traits\AuthorisationTrait;



use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;

class Link extends BaseModel implements LinkInterface, AuthorisationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AuthorisationTrait;

    public static array $linksTypes = [
        PersonInterface::COLLECTION =>
            [
                PersonInterface::COLLECTION => "friends",
                ProjectInterface::COLLECTION => "projects",
                EventInterface::COLLECTION => "events",
                OrganizationInterface::COLLECTION => "memberOf",
                ProposalInterface::COLLECTION => "proposals",
                ActionsInterface::COLLECTION => "contributors",
                FormInterface::ANSWER_COLLECTION => "contributors",
            ],
        OrganizationInterface::COLLECTION =>
            [
                ProjectInterface::COLLECTION => "projects",
                EventInterface::COLLECTION => "events",
                PersonInterface::COLLECTION => "members",
                OrganizationInterface::COLLECTION => "members",
                ProposalInterface::COLLECTION => "proposals",
            ],
        EventInterface::COLLECTION =>
                [
                    EventInterface::COLLECTION => "subEvent",
                    PersonInterface::COLLECTION => "attendees",
                    OrganizationInterface::COLLECTION => "attendees",
                    ProposalInterface::COLLECTION => "proposals",
                ],
        ProjectInterface::COLLECTION =>
            [
                OrganizationInterface::COLLECTION => "contributors",
                PersonInterface::COLLECTION => "contributors",
                ProjectInterface::COLLECTION => "projects",
                ActionInterface::COLLECTION => "actions",
                ProposalInterface::COLLECTION => "proposals",
            ],
        ActionInterface::COLLECTION => [
            ProjectInterface::COLLECTION => "projects",
            ProposalInterface::COLLECTION => "proposals",
            OrganizationInterface::COLLECTION => "contributors",
            PersonInterface::COLLECTION => "contributors",
        ],
        ActionsInterface::COLLECTION => [
            ProjectInterface::COLLECTION => "projects",
            ProposalInterface::COLLECTION => "proposals",
            OrganizationInterface::COLLECTION => "contributors",
            PersonInterface::COLLECTION => "contributors",
        ],
        ProposalInterface::COLLECTION => [
            ProjectInterface::COLLECTION => "projects",
            OrganizationInterface::COLLECTION => "contributors",
            PersonInterface::COLLECTION => "contributors",
            ProposalInterface::COLLECTION => "proposals",
        ],
        CrowdfundingInterface::COLLECTION => [
            ProjectInterface::COLLECTION => "projects",
            EventInterface::COLLECTION => "events",
            PersonInterface::COLLECTION => "members",
            OrganizationInterface::COLLECTION => "members",
        ],
        FormInterface::ANSWER_COLLECTION => [
            PersonInterface::COLLECTION => "contributors",
            OrganizationInterface::COLLECTION => "contributors",
        ],
    ];

    public static $connectTypes = [
        OrganizationInterface::COLLECTION => "member",
        ProjectInterface::COLLECTION => "contributor",
        EventInterface::COLLECTION => "attendee",
        PersonInterface::COLLECTION => "friend",
        ActionInterface::COLLECTION => "contributor",
        FormInterface::ANSWER_COLLECTION => "contributor",
    ];

    public static $connectLink = ["parent", "organizer", "producer"];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function addOrganizer(string $organizerId, string $organizerType, string $eventId, string $userId): array
    {
        // error_log("Try to add organizer ".$organizerId."/".$organizerType." from event ".$eventId);

        if (in_array($organizerType, [OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION])) {
            $isUserAdmin = $this->getModelAuthorisation()->isElementAdmin($organizerId, $organizerType, $userId);
        } elseif ($organizerType == PersonInterface::COLLECTION) {
            $isUserAdmin = ($userId == $organizerId) || $this->getModelAuthorisation()->isUserSuperAdmin($userId);
        } elseif ($organizerType == EventInterface::NO_ORGANISER || $organizerType == EventInterface::COLLECTION) {
            $isUserAdmin = true;
        } else {
            throw new CTKException("Unknown organizer type = " . $organizerType);
        }

        if ($isUserAdmin != true) {
            $isUserAdmin = $this->getModelAuthorisation()->isOpenEdition($organizerId, $organizerType);
        }

        if ($isUserAdmin != true) {
            return [
                "result" => false,
                "msg" => "You can't remove the organizer of this event !",
            ];
        }

        if ($organizerType != EventInterface::NO_ORGANISER) {
            $this->db->update(
                $organizerType,
                [
                    "_id" => $this->db->MongoId($organizerId),
                ],
                [
                    '$set' => [
                        "links.events." . $eventId . ".type" => EventInterface::COLLECTION,
                    ],
                ]
            );
            $this->db->update(
                EventInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($eventId),
                ],
                [
                    '$set' => [
                        "links.organizer." . $organizerId . ".type" => $organizerType,
                    ],
                ]
            );
        }
        //TODO SBAR : add notification for new organizer
        $res = [
            "result" => true,
            "msg" => "The event organizer has been added with success",
        ];

        return $res;
    }
}
