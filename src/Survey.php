<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;

use PixelHumain\Models\Traits\ActionRoomTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\Interfaces\ActionRoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;

class Survey extends BaseModel implements SurveyInterface, CommentTraitInterface, ActionRoomTraitInterface, AuthorisationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CommentTrait;
    use ActionRoomTrait;
    use AuthorisationTrait;

    public array $dataBinding = [
        //TODO : make field name as event endDate + make it as mongoDate
        "endDate" => [
            "name" => "dateEnd",
            "rules" => ["required"],
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    /**
     * Returns the data binding for the Survey model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array
    {
        return $this->dataBinding;
    }

    /**
     * Retrieves a survey by its ID.
     *
     * @param string $id The ID of the survey.
     * @return array|null The survey data as an array or null if not found.
     */
    public function getById(string $id): ?array
    {
        $survey = $this->db->findOneById(SurveyInterface::COLLECTION, $id);
        return $survey;
    }

    /**
     * Check if a survey can be updated.
     *
     * @param string $id The ID of the survey.
     * @param string $fieldName The name of the field to update.
     * @param mixed $fieldValue The new value for the field.
     * @return array An array containing the result of the check.
     */
    public function canUpdateSurvey(string $id, string $fieldName, $fieldValue): array
    {
        $res = [
            "result" => true,
        ];
        $survey = $this->db->findOneById(SurveyInterface::COLLECTION, $id);

        if (empty($survey)) {
            return [
                "result" => false,
                "msg" => "The survey does not exist",
            ];
        }

        $hasVote = (isset($survey["voteUpCount"])
        || isset($survey["voteAbstainCount"])
        || isset($survey["voteUnclearCount"])
        || isset($survey["voteMoreInfoCount"])
        || isset($survey["voteDownCount"])) ? true : false;

        //the survey has vote. Only can modify the endDate field
        if ($hasVote) {
            if ($fieldName == "dateEnd") {
                //If the endDate is modified after votes, the new end date should be after the previous
                //Surveys can be only delayed
                if ($survey["dateEnd"] > strtotime((string) $fieldValue)) {
                    $res = [
                        "result" => false,
                        "msg" => $this->language->t("survey", "The end date can only be delayed after votes have been done."),
                    ];
                    return $res;
                }
            } else {
                $res = [
                    "result" => false,
                    "msg" => $this->language->t("survey", "Can not update the survey after votes have been done."),
                ];
                return $res;
            }
        }

        return $res;
    }

    /**
     * Modère une entrée du sondage.
     *
     * @param array $params Les paramètres de l'entrée à modérer.
     * @return array Les informations de l'entrée modérée.
     */
    public function moderateEntry(array $params): array
    {
        $res = [
            "result" => false,
        ];
        //check if user is set as admin
        if ($this->session->getUserId()) {
            if ($this->isModerator($this->session->getUserId(), (string) $params["app"])) {
                $survey = $this->db->findOne(SurveyInterface::COLLECTION, [
                    "_id" => $this->db->MongoId((string) $params["survey"]),
                ]);
                if (isset($survey["applications"][(string) $params["app"]]["cleared"])) {
                    if ($params["action"]) {
                        $this->db->update(
                            SurveyInterface::COLLECTION,
                            [
                                "_id" => $this->db->MongoId((string) $params["survey"]),
                            ],
                            [
                                '$unset' => [
                                    'applications.' . (string) $params["app"] . '.cleared' => true,
                                ],
                            ]
                        );
                        $res["msg"] = "EntryCleared";
                        $res["result"] = true;
                    } else {
                        $this->db->update(
                            SurveyInterface::COLLECTION,
                            [
                                "_id" => $this->db->MongoId((string) $params["survey"]),
                            ],
                            [
                                '$set' => [
                                    'applications.' . (string) $params["app"] . '.cleared' => "refused",
                                ],
                            ]
                        );
                        $res["msg"] = "EntryRefused";
                    }
                } else {
                    $res["msg"] = "Nothing to clear on this entry";
                }

                $res["survey"] = $this->db->findOne(SurveyInterface::COLLECTION, [
                    "_id" => $this->db->MongoId((string) $params["survey"]),
                ]);
            } else {
                $res["msg"] = "mustBeModerator";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }

        return $res;
    }

    /**
     * Check if a user is a moderator for a survey.
     *
     * @param string|null $userId The ID of the user to check. Can be null.
     * @param string $key The key of the survey.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $key)
    {
        $app = $this->db->findOne(PHType::TYPE_APPLICATIONS, [
            "key" => $key,
        ]);
        $res = false;
        if (isset($app["moderator"]) && is_array($app["moderator"])) {
            $res = (isset($userId) && in_array($this->session->getUserId(), $app["moderator"])) ? true : false;
        }
        return $res;
    }

    /**
     * Deletes an entry from the survey.
     *
     * @param string $id The ID of the entry to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function deleteEntry(string $id, string $userId): array
    {
        $resComment = [];
        $res = [
            "result" => false,
            "msg" => "Something went wrong : contact your admin !",
        ];

        $entry = $this->getById($id);
        if (empty($entry)) {
            return [
                "result" => false,
                "msg" => "The survey does not exist",
            ];
        }

        if (! $this->canAdministrate($userId, $id)) {
            return [
                "result" => false,
                "msg" => "You must be admin of the parent of this entry if you want delete it",
            ];
        }

        //Remove all comments linked
        if (isset($entry["comment"])) {
            $resComment = $this->getModelComment()->deleteAllContextComments($id, SurveyInterface::COLLECTION, $userId);
        }

        if (isset($resComment["result"]) && ! $resComment["result"]) {
            return (array) $resComment;
        }

        //Remove the entry (survey)
        if ($this->db->remove(SurveyInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ])) {
            $res = [
                "result" => true,
                "msg" => "The survey has been deleted with success",
            ];
        }

        return $res;
    }

    /**
     * Deletes all surveys of the room.
     *
     * @param string $actionRoomId The ID of the action room.
     * @param string $userId The ID of the user.
     * @return array The result of the deletion operation.
     */
    public function deleteAllSurveyOfTheRoom(string $actionRoomId, string $userId): array
    {
        $canDelete = $this->getModelActionRoom()->canAdministrate($userId, $actionRoomId);
        $res = [
            "result" => true,
        ];
        if ($canDelete) {
            $where = [
                "survey" => $actionRoomId,
            ];
            $surveys = $this->db->find(SurveyInterface::COLLECTION, $where);
            foreach ($surveys as $id => $survey) {
                $res = $this->deleteEntry((string) $id, $userId);
            }
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "You are not allowed to delete this action room"),
            ];
        }

        if ($res["result"]) {
            $res = [
                "result" => true,
                "msg" => $this->language->t("common", "The surveys of this action room have been deleted with success"),
            ];
        }

        return $res;
    }

    /**
     * Closes the entry of the survey.
     *
     * @param array $params The parameters for closing the entry.
     * @return array The updated survey data.
     */
    public function closeEntry(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($survey = $this->db->findOne(SurveyInterface::COLLECTION, [
                "_id" => $this->db->MongoId((string) $params["id"]),
            ])) {
                if ($this->session->get('userEmail') == $survey["email"]) {
                    //then remove the parent survey
                    $this->db->update(
                        SurveyInterface::COLLECTION,
                        [
                            "_id" => $survey["_id"],
                        ],
                        [
                            '$set' => [
                                "dateEnd" => time(),
                            ],
                        ]
                    );
                    $res["result"] = true;
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    // Todo : n'est pas utilisé
    // public function getChartBarResult(array $survey): string
    // {
    //     $voteDownCount = $survey[ActionInterface::ACTION_VOTE_DOWN . "Count"] ?? 0;
    //     $voteAbstainCount = $survey[ActionInterface::ACTION_VOTE_ABSTAIN . "Count"] ?? 0;
    //     $voteUnclearCount = $survey[ActionInterface::ACTION_VOTE_UNCLEAR . "Count"] ?? 0;
    //     $voteMoreInfoCount = $survey[ActionInterface::ACTION_VOTE_MOREINFO . "Count"] ?? 0;
    //     $voteUpCount = $survey[ActionInterface::ACTION_VOTE_UP . "Count"] ?? 0;

    //     $totalVotes = $voteDownCount + $voteAbstainCount + $voteUpCount + $voteUnclearCount + $voteMoreInfoCount;

    //     $oneVote = ($totalVotes != 0) ? 100 / $totalVotes : 1;

    //     $percentVoteDownCount = round($voteDownCount * $oneVote, 2);
    //     $percentVoteAbstainCount = round($voteAbstainCount * $oneVote, 2);
    //     $percentVoteUpCount = round($voteUpCount * $oneVote, 2);
    //     $percentVoteUnclearCount = round($voteUnclearCount * $oneVote, 2);
    //     $percentVoteMoreInfoCount = round($voteMoreInfoCount * $oneVote, 2);

    //     $html = "";

    //     $percentNoVote = "0";
    //     if ($totalVotes == 0) {
    //         $percentNoVote = "100";
    //     }

    //     if ($totalVotes > 1) {
    //         $msgVote = "votes exprimés";
    //     } else {
    //         $msgVote = "vote exprimé";
    //     }

    //     $html .= "<div class='col-md-12 no-padding'>" .

    //                 "<div class='pull-left text-dark' style='margin-top:5px; margin-left:5px; font-size:13px;'>" .
    //                     $totalVotes . " " . $msgVote .
    //                 "</div>" .

    //                 "<hr>";

    //     $html .= '<div class="progress">' .
    //                   '<div class="progress-bar progress-bar-green progress-bar-striped" style="width: ' . $percentVoteUpCount . '%">' .
    //                     $voteUpCount . ' <i class="fa fa-thumbs-up"></i> (' . $percentVoteUpCount . '%)' .
    //                   '</div>' .
    //                   '<div class="progress-bar progress-bar-blue progress-bar-striped" style="width: ' . $percentVoteUnclearCount . '%">' .
    //                     $voteUnclearCount . ' <i class="fa fa-pencil"></i> (' . $percentVoteUnclearCount . '%)' .
    //                   '</div>' .
    //                   '<div class="progress-bar progress-bar-white progress-bar-striped" style="width: ' . $percentVoteAbstainCount . '%">' .
    //                     $voteAbstainCount . ' <i class="fa fa-circle"></i> (' . $percentVoteAbstainCount . '%)' .
    //                   '</div>' .
    //                   '<div class="progress-bar progress-bar-purple progress-bar-striped" style="width: ' . $percentVoteMoreInfoCount . '%">' .
    //                     $voteMoreInfoCount . ' <i class="fa fa-question-circle"></i> (' . $percentVoteMoreInfoCount . '%)' .
    //                   '</div>' .
    //                   '<div class="progress-bar progress-bar-red progress-bar-striped" style="width: ' . $percentVoteDownCount . '%">' .
    //                     $voteDownCount . ' <i class="fa fa-thumbs-down"></i> (' . $percentVoteDownCount . '%)' .
    //                   '</div>' .
    //                   '<div class="progress-bar progress-bar-white progress-bar-striped" style="width: ' . $percentNoVote . '%">' .
    //                   '</div>' .
    //                 '</div>' .
    //               '</div>';

    //     return $html;
    // }

    // Todo : n'est pas utilisé
    // public function getChartCircle(array $survey, $voteLinksAndInfos, string $parentType, string $parentId)
    // {
    //     $voteDownCount = $survey[ActionInterface::ACTION_VOTE_DOWN . "Count"] ?? 0;
    //     $voteAbstainCount = $survey[ActionInterface::ACTION_VOTE_ABSTAIN . "Count"] ?? 0;
    //     $voteUnclearCount = $survey[ActionInterface::ACTION_VOTE_UNCLEAR . "Count"] ?? 0;
    //     $voteMoreInfoCount = $survey[ActionInterface::ACTION_VOTE_MOREINFO . "Count"] ?? 0;
    //     $voteUpCount = $survey[ActionInterface::ACTION_VOTE_UP . "Count"] ?? 0;

    //     $totalVotes = $voteDownCount + $voteAbstainCount + $voteUpCount + $voteUnclearCount + $voteMoreInfoCount;

    //     $oneVote = ($totalVotes != 0) ? 100 / $totalVotes : 1;

    //     $percentVoteDown = $voteDownCount * $oneVote;
    //     $percentVoteAbstain = $voteAbstainCount * $oneVote;
    //     $percentVoteUp = $voteUpCount * $oneVote;
    //     $percentVoteUnclear = $voteUnclearCount * $oneVote;
    //     $percentVoteMoreInfo = $voteMoreInfoCount * $oneVote;

    //     $actionUp = "javascript:addaction('" . (string) $survey["_id"] . "','" . ActionInterface::ACTION_VOTE_UP . "')";
    //     $actionAbstain = "javascript:addaction('" . (string) $survey["_id"] . "','" . ActionInterface::ACTION_VOTE_ABSTAIN . "')";
    //     $actionUnclear = "javascript:addaction('" . (string) $survey["_id"] . "','" . ActionInterface::ACTION_VOTE_UNCLEAR . "')";
    //     $actionMoreInfo = "javascript:addaction('" . (string) $survey["_id"] . "','" . ActionInterface::ACTION_VOTE_MOREINFO . "')";
    //     $actionDown = "javascript:addaction('" . (string) $survey["_id"] . "','" . ActionInterface::ACTION_VOTE_DOWN . "')";

    //     $isAuth = $this->getAuthorisation()->canParticipate($this->session->getUserId(), $parentType, $parentId);

    //     $canVote = ! $isAuth || $voteLinksAndInfos["hasVoted"] || ($voteLinksAndInfos["avoter"] == "closed");

    //     $html = '<div class="col-md-1"></div>';
    //     $html .= $this->getOneChartCircle($percentVoteUp, $voteUpCount, $actionUp, "Pour", "green", "thumbs-up", $canVote);
    //     $html .= $this->getOneChartCircle($percentVoteMoreInfo, $voteMoreInfoCount, $actionMoreInfo, "Incomplet", "blue", "pencil", $canVote);
    //     $html .= $this->getOneChartCircle($percentVoteAbstain, $voteAbstainCount, $actionAbstain, "Blanc", "white", "circle", $canVote);
    //     $html .= $this->getOneChartCircle($percentVoteUnclear, $voteUnclearCount, $actionUnclear, "Incompris", "purple", "question-circle", $canVote);
    //     $html .= $this->getOneChartCircle($percentVoteDown, $voteDownCount, $actionDown, "Contre", "red", "thumbs-down", $canVote);
    //     $html .= '<div class="col-md-1"></div>';
    //     if (! $isAuth) {
    //         $html .= '<div class="col-md-12 center text-red"><br/> Vous n&apos;avez pas les droits pour voter. </div>';
    //     }
    //     return $html;
    // }

    // Todo : n'est pas utilisé
    // private function getOneChartCircle($percent, $voteCount, $action, $label, $color, $icon, $canVote)
    // {
    //     $voteLinksAndInfos = [];
    //     $colorTxt = ($color == "white") ? "black" : $color;
    //     $colXS = ($color == "white") ? "col-xs-12" : "col-xs-6";

    //     $tooltips = [
    //         "green" => $this->language->t("common", "I am in favor of this proposal"),
    //         "blue" => $this->language->t("common", "I think that this proposal is not complete"),
    //         "white" => $this->language->t("common", "I have not reviews"),
    //         "purple" => $this->language->t("common", "I don't understand, it miss informations"),
    //         "red" => $this->language->t("common", "I am not in favor of this proposal"),
    //     ];

    //     $tooltip = $tooltips[$color];

    //     $html = '<div class="col-md-2 col-sm-2 ' . $colXS . ' text-center">
    //               <div class="col-md-12 no-padding">
    //                 <div class="c100 p' . $percent . ' ' . $color . ' small center text-center">
    //                 <span>' . $percent . '%</span>
    //                 <div class="slice"> <div class="bar"></div> <div class="fill"></div>
    //               </div>
    //             </div>
    //             <div class="col-md-12 no-padding">
    //                 <h5 class="text-' . $colorTxt . ' bold h5voteres"><i class="fa fa-' . $icon . '"></i> ' . $label . ' (' . $voteCount . ')</h5>' .
    //                 '</div>' .
    //               '</div>';

    //     if (! $canVote) {
    //         $html .= '<button onclick="' . $action . '" data-original-title="' . $tooltip . '" data-toggle="tooltip" data-placement="bottom" ' .
    //                         'class="btn btn-default tooltips btn-sm text-' . $colorTxt . '"><i class="fa fa-gavel"></i> Voter' . @$voteLinksAndInfos["avoter"] . '</button>';
    //     }

    //     $html .= '</div>';

    //     return $html;
    // }

    /**
     * Check if a user can administrate a survey.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the survey.
     * @return bool Returns true if the user can administrate the survey, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool
    {
        $entry = $this->getById($id);
        if (empty($entry)) {
            return false;
        }
        $actionRoomId = $entry["survey"];

        $isAdmin = $this->getModelActionRoom()->canAdministrate($userId, $actionRoomId);
        return (bool) $isAdmin;
    }
}
