<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\StatInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\LogTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\LogTrait;
use PixelHumain\Models\Traits\PersonTrait;

class Stat extends BaseModel implements StatInterface, LogTraitInterface, PersonTraitInterface
{
    /**
    * Les traits définis ci-dessous correspondent à des fonctionnalités
    * qui peuvent être utilisés par la classe.
    */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use LogTrait;
    use PersonTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    // TODO : est ce que c'est utilisé ?
    // TODO : il y a des echo dans cette fonction, ce n'est pas normal
    /**
     * Consolidate all the data
    */
    public function createGlobalStat(): void
    {
        $stat = [];
        $cities = [];

        $stat['created'] = $this->db->MongoDate(time());
        echo date('Y-m-d H:i:s') . " - Enregistrement pour récupérer l'id <br/>";
        $this->save($stat);
        $stat = $this->db->findOne('stats', [
            'created' => $stat['created'],
        ]);

        echo date('Y-m-d H:i:s') . " - Calcul global<br/>";
        $stat['global']['citoyens'] = $this->consolidateCitoyens();
        $stat['global']['projects'] = $this->consolidateProjects();
        $stat['global']['organizations'] = $this->consolidateFromListAndCollection('organisationTypes', OrganizationInterface::COLLECTION);
        $stat['global']['events'] = $this->consolidateFromListAndCollection('eventTypes', EventInterface::COLLECTION);
        $stat['global']['actionRooms'] = $this->consolidateFromListAndCollection('listRoomTypes', ActionRoomInterface::COLLECTION);
        $stat['global']['links'] = $this->consolidateLinksCitoyen();
        $stat['global']['survey'] = $this->consolidateSurveys();
        $stat['global']['modules'] = $this->consolidateModules();
        echo date('Y-m-d H:i:s') . " - Enregistrement<br/>";
        $this->save($stat);
        unset($stat['global']);

        echo date('Y-m-d H:i:s') . " - Calcul Cities<br/>";
        $stat['cities']['citoyens'] = $this->consolidateCitoyensByCity();
        $stat['cities']['organizations'] = $this->consolidateOrganizationsByCity();
        $stat['cities']['events'] = $this->consolidateEventsByCity();
        $stat['cities']['projects'] = $this->consolidateProjectsByCity();
        $stat['cities']['links'] = $this->consolidateLinksCitoyenByCity();
        $stat['cities']['modules'] = $this->consolidateModulesOrgaByCity();

        echo date('Y-m-d H:i:s') . " - Calcul Logs<br/>";
        $stat['logs'] = $this->consolidateLogs();

        echo date('Y-m-d H:i:s') . " - Enregistrement<br/>";
        $res = $this->save($stat);
    }

    // TODO : est ce que c'est utilisé ?
    // TODO : il y a des echo dans cette fonction, ce n'est pas normal
    /**
     * Consolidate all the citoyens by cities
    */
    /**
     * Consolidates the logs.
     *
     * @return array The consolidated logs.
     */
    public function consolidateLogs(): array
    {
        echo date('Y-m-d H:i:s') . " - consolidateLogs<br/>";
        $datas = [];

        //We took the last log consolidate
        $lastLogStat = $this->getWhere([
            'logs' => [
                '$exists' => 1,
            ],
        ], ['created'], 1);
        if (is_countable($lastLogStat) ? count($lastLogStat) : 0) {
            $lastLogStat = array_pop($lastLogStat);
            $where = [
                'created' => [
                    '$gt' => $lastLogStat['created'],
                ],
            ];
        } else {
            $where = [];
        }

        $allLogs = $this->getModelLog()->getWhere($where);
        if (is_array($allLogs)) {
            foreach ($allLogs as $key => $value) {
                $action = @$value['action'];

                //If result => Consolidate by result
                if (! empty($action)) {
                    if (isset($value['result'])) {
                        $res_res = @$value['result']['result'];
                        if (! isset($datas[$action][$res_res])) {
                            $datas[$action][$res_res] = 0;
                        }
                        $datas[$action][$res_res] += 1;
                    } else {
                        if (! isset($datas[$action])) {
                            $datas[$action] = 0;
                        }
                        $datas[$action] += 1;
                    }
                }
            }
        }
        ksort($datas);
        return $datas;
    }

    // TODO : est ce que c'est utilisé ?
    // TODO : il y a des echo dans cette fonction, ce n'est pas normal
    /**
     * Consolidate all the citoyens by cities
    */
    /**
     * Consolidates the citizens by city.
     *
     * @return array The consolidated citizens by city.
     */
    public function consolidateCitoyensByCity(): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateCitoyensByCity<br/>";
        $opts = [[
            '$group' => [
                '_id' => '$address.codeInsee',
                "population" => [
                    '$sum' => 1,
                ],
            ],
        ], [
            '$match' => [
                'population' => [
                    '$gt' => 0,
                ],
            ],
        ], [
            '$sort' => [
                'insee' => 1,
            ],
        ]];

        $result = $this->db->aggregate(PersonInterface::COLLECTION, $opts);

        if (is_array($result['result'])) {
            foreach ($result['result'] as $key => $value) {
                //We check if the city is well present in cities
                $insee = $this->checkAndGetInsee(@$value['_id']);
                $datas[$insee] = @$datas[$insee] + $value['population'];
            }
        }
        ksort($datas);
        return $datas;
    }

    // TODO : est ce que c'est utilisé ?
    // TODO : il y a des echo dans cette fonction, ce n'est pas normal
    /**
     * Consolidate all the organizations by cities
    */
    /**
     * Consolidates organizations by city.
     *
     * @return array The consolidated organizations by city.
     */
    public function consolidateOrganizationsByCity(): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateOrganizationsByCity<br/>";

        $opts = [[
            '$group' => [
                '_id' => [
                    'insee' => '$address.codeInsee',
                    'type' => '$type',
                ],
                "organisation" => [
                    '$sum' => 1,
                ],
            ],
        ], [
            '$match' => [
                'organisation' => [
                    '$gt' => 0,
                ],
            ],
        ], [
            '$sort' => [
                'insee' => 1,
            ],
        ]];

        $result = $this->db->aggregate(OrganizationInterface::COLLECTION, $opts);

        if (is_array($result['result'])) {
            foreach ($result['result'] as $key => $value) {
                $insee = $this->checkAndGetInsee(@$value['_id']['insee']);

                if (! isset($value['_id']['type'])) {
                    $value['_id']['type'] = "Unknow";
                }
                $datas[$insee][$value['_id']['type']] = @$datas[$insee][$value['_id']['type']] + $value['organisation'];
            }
        }
        ksort($datas);
        return $datas;
    }

    // TODO : est ce que c'est utilisé ?
    // TODO : il y a des echo dans cette fonction, ce n'est pas normal
    /**
     * Consolidate all the organizations by cities
    */
    /**
     * Consolidates events by city.
     *
     * @return array The consolidated events by city.
     */
    public function consolidateEventsByCity(): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateEventsByCity<br/>";

        $opts = [[
            '$group' => [
                '_id' => [
                    'insee' => '$address.codeInsee',
                    'type' => '$type',
                ],
                "event" => [
                    '$sum' => 1,
                ],
            ],
        ], [
            '$match' => [
                'event' => [
                    '$gt' => 0,
                ],
            ],
        ], [
            '$sort' => [
                'insee' => 1,
            ],
        ]];

        $result = $this->db->aggregate(EventInterface::COLLECTION, $opts);

        if (is_array($result['result'])) {
            foreach ($result['result'] as $key => $value) {
                //We check if the city is well present in cities
                $insee = $this->checkAndGetInsee(@$value['_id']['insee']);
                if (! isset($value['_id']['type'])) {
                    $value['_id']['type'] = "Unknow";
                }
                $datas[$insee][$value['_id']['type']] = $value['event'];
            }
        }
        ksort($datas);
        return $datas;
    }

    /**
     * Consolidate all the citoyens by cities
    */
    /**
     * Consolidates projects by city.
     *
     * @return array The consolidated projects by city.
     */
    public function consolidateProjectsByCity(): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateProjectsByCity<br/>";

        $opts = [[
            '$group' => [
                '_id' => '$address.codeInsee',
                "projet" => [
                    '$sum' => 1,
                ],
            ],
        ], [
            '$match' => [
                'projet' => [
                    '$gt' => 0,
                ],
            ],
        ], [
            '$sort' => [
                'insee' => 1,
            ],
        ]];
        $result = $this->db->aggregate(ProjectInterface::COLLECTION, $opts);

        if (is_array($result['result'])) {
            foreach ($result['result'] as $key => $value) {
                //We check if the city is well present in cities
                $insee = $this->checkAndGetInsee(@$value['_id']);
                $datas[$insee] = $value['projet'];
            }
        }
        ksort($datas);
        return $datas;
    }

    /**
     * Consolidate all the links by cities
    */
    public function consolidateLinksCitoyenByCity(): array
    {
        echo date('Y-m-d H:i:s') . " - consolidateLinksCitoyenByCity<br/>";
        $datas = [];
        $persons = $this->db->find('citoyens', [
            'links' => [
                '$exists' => 1,
            ],
        ], [
            "address" => 1,
            'links' => 1,
        ]);
        if (is_array($persons)) {
            foreach ($persons as $key => $value) {
                $insee = $this->checkAndGetInsee(@$value['address']['codeInsee']);
                if (! isset($datas[$insee])) {
                    $datas[$insee] = [];
                }

                //total by links type
                if (is_array($value['links'])) {
                    foreach ($value['links'] as $type => $list) {
                        $inc = 0;
                        if (is_array($list)) {
                            $inc = count($list);
                        }
                        if (! isset($datas[$insee][$type])) {
                            $datas[$insee][$type] = 0;
                        }

                        $datas[$insee][$type] += $inc;
                    }
                }
            }
        }
        ksort($datas);
        return $datas;
    }

    /**
     * Consolidate all the modules of organizations by cities
    */
    public function consolidateModulesOrgaByCity(): array
    {
        echo date('Y-m-d H:i:s') . " - consolidateModulesOrgaByCity<br/>";

        $result = $this->db->find(OrganizationInterface::COLLECTION, [
            'address.codeInsee' => [
                '$exists' => 1,
            ],
            'modules' => [
                '$exists' => 1,
            ],
        ]);
        $aggregate = [];
        foreach ($result as $key => $value) {
            $insee = $this->checkAndGetInsee(@$value['address']['codeInsee']);
            foreach ($value['modules'] as $idModule => $moduleName) {
                $aggregate[$insee][$moduleName] = @$aggregate[$insee][$moduleName] + 1;
            }
        }
        ksort($aggregate);
        return $aggregate;
    }

    /**
     * create data statistics for surveys
    */
    public function consolidateSurveys(): array
    {
        $surveys = [];
        echo date('Y-m-d H:i:s') . " - consolidateSurveys<br/>";

        //All the projects
        $countSurvey = $this->db->count(SurveyInterface::COLLECTION);
        $surveys['total'] = $countSurvey;

        return $surveys;
    }

    /**
     * create data statistics for modules
    */
    public function consolidateModules(): array
    {
        echo date('Y-m-d H:i:s') . " - consolidateModules<br/>";
        $datas = [];
        $datas['total'] = 0;
        $organizations = $this->db->find(OrganizationInterface::COLLECTION, [
            "modules" => [
                '$exists' => 1,
            ],
        ]);
        foreach ($organizations as $key => $value) {
            if (is_array($value['modules'])) {
                foreach ($value['modules'] as $keyModule => $nameModule) {
                    if (! isset($datas[$nameModule])) {
                        $datas[$nameModule] = 0;
                    }
                    $datas[$nameModule] += 1;
                    $datas['total'] += 1;
                }
            }
        }
        return $datas;
    }

    /**
     * create data statistics for Links
    */
    public function consolidateLinksCitoyen(): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateLinksCitoyen<br/>";

        $datas['total'] = 0;
        $persons = $this->getModelPerson()->getWhere([
            'links' => [
                '$exists' => 1,
            ],
        ]);
        if (is_array($persons)) {
            foreach ($persons as $key => $value) {
                if (is_array($value['links'])) {
                    foreach ($value['links'] as $type => $list) {
                        foreach ($list as $key => $value) {
                            if (isset($datas[$type])) {
                                $datas[$type] += 1;
                            } else {
                                $datas[$type] = 1;
                            }
                            $datas['total'] += 1;
                        }
                    }
                }
            }
        }
        return $datas;
    }

    /**
     * create data statistics for citoyens
    */
    public function consolidateCitoyens(): array
    {
        $citoyens = [];
        echo date('Y-m-d H:i:s') . " - consolidateCitoyens<br/>";

        //All the citoyens
        $countCitoyen = $this->db->count(PersonInterface::COLLECTION);
        $citoyens['total'] = $countCitoyen;

        return $citoyens;
    }

    /**
     * create data statistics for projects
    */
    public function consolidateProjects(): array
    {
        $projects = [];
        echo date('Y-m-d H:i:s') . " - consolidateProjects<br/>";

        //All the projects
        $countProject = $this->db->count(ProjectInterface::COLLECTION);
        $projects['total'] = $countProject;

        return $projects;
    }

    /**
     * create data statistics from an list and collection
    */
    /**
     * Consolidates data from a list and a collection.
     *
     * @param mixed $list The list of data to consolidate.
     * @param string $collection The name of the collection.
     * @return array The consolidated data.
     */
    public function consolidateFromListAndCollection($list, string $collection): array
    {
        $datas = [];
        echo date('Y-m-d H:i:s') . " - consolidateFromListAndCollection $list<br/>";

        //Get all the organizations types
        $types = $this->getLists()->get([$list]);
        $datas['total'] = 0;
        foreach ($types[$list] as $key => $val) {
            $datas[$key] = $this->db->count($collection, [
                "type" => $key,
            ]);
            $datas['total'] += $datas[$key];
        }

        return $datas;
    }

    /**
     * Get records from the database based on specified conditions.
     *
     * @param array $params The conditions to filter the records.
     * @param array|null $fields The fields to retrieve from the records. If null, all fields will be retrieved.
     * @param int $limit The maximum number of records to retrieve. Default is 20.
     * @return array The retrieved records.
     */
    public function getWhere(array $params, ?array $fields = null, int $limit = 20): array
    {
        $stat = $this->db->findAndSort(StatInterface::COLLECTION, $params, [
            "created" => -1,
        ], $limit, $fields);
        return $stat;
    }

    /**
     * Saves the given stat data.
     *
     * @param array $stat The stat data to be saved.
     * @return mixed The result of the save operation.
     */
    public function save(array $stat)
    {
        //Update
        if (isset($stat['_id'])) {
            $id = $stat['_id'];
            unset($stat['_id']);
            return $this->db->update(
                StatInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $id),
                ],
                [
                    '$set' => $stat,
                ]
            );
        }//Insert
        else {
            return $this->db->insert(StatInterface::COLLECTION, $stat);
        }
    }

    /**
     * Check and get the INSEE code.
     *
     * @param string|array $insee The INSEE code to check.
     * @return string The validated INSEE code or null if invalid.
     */
    public function checkAndGetInsee($insee): string
    {
        if (is_array($insee)) {
            return 'Unknow';
        }
        $city = $this->db->find('cities', [
            'insee' => $insee,
        ]);
        if ($city) {
            return $insee;
        }
        return 'Unknow';
    }
}
