<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\MediaInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

// TODO : est ce que cette class est utilisée ?
class Media extends BaseModel implements MediaInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Get media by ID.
     *
     * @param string $id The ID of the media.
     * @return array|null The media data or null if not found.
     */
    public function getById(string $id): ?array
    {
        $event = $this->db->findOne(MediaInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
        return $event;
    }
}
