<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ClassifiedInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\GamificationInterface;
use PixelHumain\Models\Interfaces\LinkInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\RessourceInterface;
use PixelHumain\Models\Traits\ActivityStreamTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

class Gamification extends BaseModel implements GamificationInterface, ElementTraitInterface, ActivityStreamTraitInterface, DocumentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;
    use ActivityStreamTrait;
    use DocumentTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Calculates the points for a given element type and user.
     *
     * @param string $eltype The element type.
     * @param string $userId The user ID.
     * @param string|null $filter The filter to apply (optional).
     * @return int The calculated points.
     */
    public function calcPoints(string $eltype, string $userId, ?string $filter = null): int
    {
        $res = 0;
        $el = $this->getModelElement()->getByTypeAndId($eltype, $userId);
        if (isset($el['links'])) {
            foreach ($el['links'] as $type => $list) {
                if (($type == LinkInterface::person2events || $type == LinkInterface::event2person) && (! $filter || $filter == LinkInterface::person2events)) {
                    foreach ($list as $key => $value) {
                        $res += GamificationInterface::POINTS_LINK_USER_2_EVENT;
                    }
                }
                if (($type == LinkInterface::person2projects || $type == LinkInterface::project2person) && (! $filter || $filter == LinkInterface::person2projects)) {
                    foreach ($list as $key => $value) {
                        $res += GamificationInterface::POINTS_LINK_USER_2_PROJECT;
                    }
                }
                if (($type == LinkInterface::person2organization || $type == LinkInterface::organization2person) && (! $filter || $filter == LinkInterface::person2organization)) {
                    foreach ($list as $key => $value) {
                        $res += GamificationInterface::POINTS_LINK_USER_2_ORGANIZATION;
                    }
                }
                if ($type == LinkInterface::person2person && (! $filter || $filter == LinkInterface::person2person)) {
                    foreach ($list as $key => $value) {
                        $res += GamificationInterface::POINTS_LINK_USER_2_USER;
                    }
                }
            }
        }

        if ($eltype == PersonInterface::COLLECTION) {
            $cols = [OrganizationInterface::COLLECTION, EventInterface::COLLECTION, ProjectInterface::COLLECTION, NewsInterface::COLLECTION, ClassifiedInterface::COLLECTION, RessourceInterface::COLLECTION];

            foreach ($cols as $key => $col) {
                $c = $this->db->count($col, [
                    "creator" => $userId,
                ]);
                $res += $c * GamificationInterface::POINTS_CREATE;
            }
        }

        return $res;
    }

    /**
     * Inserts a gamification user into the system.
     *
     * @param string $userId The ID of the user to insert.
     * @return void
     */
    public function insertGamificationUser(string $userId): void
    {
        $entry = [
            '_id' => $this->db->MongoId($userId),
        ];
        $entry[time()] = $this->consolidatePoints($userId);
        $this->save($entry);
    }

    /**
     * Calculates the points for a specific user.
     *
     * @param string $userId The ID of the user.
     * @return array The calculated points for the user.
     */
    public function calcPointsAction(string $userId): array
    {
        $total = [];
        $total['total'] = 0;

        //We take action to consolidate
        $actions = $this->getModelActivityStream()->getWhere([
            "who" => (string) $userId,
        ]);
        foreach ($actions as $key => $action) {
            if (isset($action['self'])) {
                //if action is defined => We aggregate the points
                if (defined("self::POINTS_" . strtoupper($action['self']))) {
                    /******* ATTENTION ********/
                    //Choix prix de comptabiliser toutes les actions même s'il en y a plusieurs
                    //Une regle de gestion devra être ajoutée pour éviter plusieurs like/dislike/...
                    if (isset($total[$action['self']])) {
                        $total[$action['self']] += constant("self::POINTS_" . strtoupper($action['self']));
                    } else {
                        $total[$action['self']] = constant("self::POINTS_" . strtoupper($action['self']));
                    }
                    $total['total'] += constant("self::POINTS_" . strtoupper($action['self']));
                }
            }
        }

        return $total;
    }

    // TODO : $filter is not used
    /**
     * Consolidates the points for a given user.
     *
     * @param string $userId The ID of the user.
     * @param mixed $filter (Optional) The filter to apply when consolidating points.
     * @return array The consolidated points for the user.
     */
    public function consolidatePoints(string $userId, $filter = null): array
    {
        $gammificationUser = $this->getModelElement()->getElementById($userId, PersonInterface::COLLECTION, null, ["gamification"]);
        $points = [];
        $points['created'] = $this->db->MongoDate(time());
        $points['total'] = 0;

        /***** ACTIONS ******/
        $points['actions'] = $this->calcPointsAction($userId);
        $points['total'] += $points['actions']['total'];

        /***** LINKS ******/
        $points['links']['total'] = $this->calcPoints(PersonInterface::COLLECTION, $userId);
        $points['total'] += $points['links']['total'];

        if (! empty($gammificationUser["gamification"])) {
            foreach ($gammificationUser["gamification"] as $k => $v) {
                if (! isset($points[$k])) {
                    $points[$k] = $v;
                }
            }
        }

        return $points;
    }

    /**
     * Récupère tous les niveaux et le niveau actuel d'un utilisateur.
     *
     * @param int $total Le nombre total de niveaux.
     * @param string $userId L'identifiant de l'utilisateur.
     * @param string $path Le chemin du fichier.
     * @return array Les niveaux et le niveau actuel de l'utilisateur.
     */
    public function getAllLevelAndCurent(int $total, string $userId, string $path): array
    {
        $arrayLevel = [
            "air" => [
                "label" => $this->badge($userId, 1),
                "icon" => $this->badge($userId, 1, $path),
                "points" => 0,
                "nextStep" => GamificationInterface::BADGE_SEED_LIMIT,
            ],
        ];
        $currentKey = "air";
        if ($total >= GamificationInterface::BADGE_SEED_LIMIT) {
            $currentKey = "seed";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_SEED_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_SEED_LIMIT, $path),
                "points" => GamificationInterface::BADGE_SEED_LIMIT,
                "nextStep" => GamificationInterface::BADGE_GERM_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_GERM_LIMIT) {
            $currentKey = "germ";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_GERM_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_GERM_LIMIT, $path),
                "points" => GamificationInterface::BADGE_GERM_LIMIT,
                "nextStep" => GamificationInterface::BADGE_PLANT_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_PLANT_LIMIT) {
            $currentKey = "plant";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_PLANT_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_PLANT_LIMIT, $path),
                "points" => GamificationInterface::BADGE_PLANT_LIMIT,
                "nextStep" => GamificationInterface::BADGE_TREE_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_TREE_LIMIT) {
            $currentKey = "tree";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_TREE_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_TREE_LIMIT, $path),
                "points" => GamificationInterface::BADGE_TREE_LIMIT,
                "nextStep" => GamificationInterface::BADGE_FOREST_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_FOREST_LIMIT) {
            $currentKey = "forest";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_FOREST_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_FOREST_LIMIT, $path),
                "points" => GamificationInterface::BADGE_FOREST_LIMIT,
                "nextStep" => GamificationInterface::BADGE_COUNTRY_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_COUNTRY_LIMIT) {
            $currentKey = "country";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_COUNTRY_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_COUNTRY_LIMIT, $path),
                "points" => GamificationInterface::BADGE_COUNTRY_LIMIT,
                "nextStep" => GamificationInterface::BADGE_PLANET_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_PLANET_LIMIT) {
            $currentKey = "planet";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_PLANET_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_PLANET_LIMIT, $path),
                "points" => GamificationInterface::BADGE_PLANET_LIMIT,
                "nextStep" => GamificationInterface::BADGE_SUN_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_SUN_LIMIT) {
            $currentKey = "sun";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_SUN_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_SUN_LIMIT, $path),
                "points" => GamificationInterface::BADGE_SUN_LIMIT,
                "nextStep" => GamificationInterface::BADGE_SYSTEM_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_SYSTEM_LIMIT) {
            $currentKey = "system";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_SYSTEM_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_SYSTEM_LIMIT, $path),
                "points" => GamificationInterface::BADGE_SYSTEM_LIMIT,
                "nextStep" => GamificationInterface::BADGE_GALAXY_LIMIT,
            ];
        }if ($total >= GamificationInterface::BADGE_GALAXY_LIMIT) {
            $currentKey = "galaxy";
            $arrayLevel[$currentKey] = [
                "label" => $this->badge($userId, GamificationInterface::BADGE_GALAXY_LIMIT),
                "icon" => $this->badge($userId, GamificationInterface::BADGE_GALAXY_LIMIT, $path),
                "points" => GamificationInterface::BADGE_GALAXY_LIMIT,
                "nextStep" => false,
            ];
        }
        $arrayLevel[$currentKey]["current"] = true;
        return $arrayLevel;
    }

    /**
     * Assigns a badge to a user.
     *
     * @param string $userId The ID of the user.
     * @param int|null $total The total number of badges assigned to the user (optional).
     * @param string|null $iconPath The path to the badge icon (optional).
     * @return string The assigned badge.
     */
    public function badge(string $userId, ?int $total = null, ?string $iconPath = null): string
    {
        $total = (@$total && ! empty($total)) ? $total : $this->calcPoints(PersonInterface::COLLECTION, $userId);
        if (! $iconPath) {
            $res = $this->language->t("common", "air");
            if ($total >= GamificationInterface::BADGE_SEED_LIMIT && $total < GamificationInterface::BADGE_GERM_LIMIT) {
                $res = $this->language->t("common", "seed");
            }
            if ($total >= GamificationInterface::BADGE_GERM_LIMIT && $total < GamificationInterface::BADGE_PLANT_LIMIT) {
                $res = $this->language->t("common", "germ");
            }
            if ($total >= GamificationInterface::BADGE_PLANT_LIMIT && $total < GamificationInterface::BADGE_TREE_LIMIT) {
                $res = $this->language->t("common", "plant");
            }
            if ($total >= GamificationInterface::BADGE_TREE_LIMIT && $total < GamificationInterface::BADGE_FOREST_LIMIT) {
                $res = $this->language->t("common", "tree");
            }
            if ($total >= GamificationInterface::BADGE_FOREST_LIMIT && $total < GamificationInterface::BADGE_COUNTRY_LIMIT) {
                $res = $this->language->t("common", "forest");
            }
            if ($total >= GamificationInterface::BADGE_COUNTRY_LIMIT && $total < GamificationInterface::BADGE_PLANET_LIMIT) {
                $res = $this->language->t("common", "country");
            }
            if ($total >= GamificationInterface::BADGE_PLANET_LIMIT && $total < GamificationInterface::BADGE_SUN_LIMIT) {
                $res = $this->language->t("common", "planet");
            }
            if ($total >= GamificationInterface::BADGE_SUN_LIMIT && $total < GamificationInterface::BADGE_SYSTEM_LIMIT) {
                $res = $this->language->t("common", "sun");
            }
            if ($total >= GamificationInterface::BADGE_SYSTEM_LIMIT && $total < GamificationInterface::BADGE_GALAXY_LIMIT) {
                $res = $this->language->t("common", "solar system");
            }
            if ($total >= GamificationInterface::BADGE_GALAXY_LIMIT) {
                $res = $this->language->t("common", "galaxy");
            }
        } else {
            $res = $iconPath . "/images/gamification/air.png";
            if ($total >= GamificationInterface::BADGE_SEED_LIMIT && $total < GamificationInterface::BADGE_GERM_LIMIT) {
                $res = $iconPath . "/images/gamification/seed.png";
            }
            if ($total >= GamificationInterface::BADGE_GERM_LIMIT && $total < GamificationInterface::BADGE_PLANT_LIMIT) {
                $res = $iconPath . "/images/gamification/germ.png";
            }
            if ($total >= GamificationInterface::BADGE_PLANT_LIMIT && $total < GamificationInterface::BADGE_TREE_LIMIT) {
                $res = $iconPath . "/images/gamification/plant.png";
            }
            if ($total >= GamificationInterface::BADGE_TREE_LIMIT && $total < GamificationInterface::BADGE_FOREST_LIMIT) {
                $res = $iconPath . "/images/gamification/tree.png";
            }
            if ($total >= GamificationInterface::BADGE_FOREST_LIMIT && $total < GamificationInterface::BADGE_COUNTRY_LIMIT) {
                $res = $iconPath . "/images/gamification/forest.png";
            }
            if ($total >= GamificationInterface::BADGE_COUNTRY_LIMIT && $total < GamificationInterface::BADGE_PLANET_LIMIT) {
                $res = $iconPath . "/images/gamification/earth.png";
            }
            if ($total >= GamificationInterface::BADGE_PLANET_LIMIT && $total < GamificationInterface::BADGE_SUN_LIMIT) {
                $res = $iconPath . "/images/gamification/planet.png";
            }
            if ($total >= GamificationInterface::BADGE_SUN_LIMIT && $total < GamificationInterface::BADGE_SYSTEM_LIMIT) {
                $res = $iconPath . "/images/gamification/sun.png";
            }
            if ($total >= GamificationInterface::BADGE_SYSTEM_LIMIT && $total < GamificationInterface::BADGE_GALAXY_LIMIT) {
                $res = $iconPath . "/images/gamification/system.png";
            }
            if ($total >= GamificationInterface::BADGE_GALAXY_LIMIT) {
                $res = $iconPath . "/images/gamification/galaxy.png";
            }
        }

        return $res;
    }

    /**
     * Updates the gamification data.
     *
     * @param array $params The parameters for the update.
     * @return void
     */
    public function update(array $params): void
    {
        $new = [
            "id" => $params['id'],
            "person" => $params['type'],
            "folder" => $params['folder'],
            "moduleId" => $params['moduleId'],
            "doctype" => $this->getModelDocument()->getDoctype($params['name']),
            "author" => $params['author'],
            "name" => $params['name'],
            "size" => $params['size'],
            'created' => time(),
        ];

        $new = $this->db->insert(GamificationInterface::COLLECTION, $new);
    }

    /**
     * Saves the given entry.
     *
     * @param array $entry The entry to be saved.
     * @return void
     */
    public function save(array $entry): void
    {
        //Update
        if (isset($entry['_id'])) {
            $id = $entry['_id'];
            unset($entry['_id']);
            $this->db->update(
                GamificationInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                [
                    '$set' => $entry,
                ]
            );
        }//Insert
        else {
            $entry = $this->db->insert(GamificationInterface::COLLECTION, $entry);
        }
    }

    // TODO : $this->db->update params en trop option upsert
    /**
     * Updates a user.
     *
     * @param string $id The ID of the user to update.
     * @return void
     */
    public function updateUser(string $id): void
    {
        $gamification = $this->consolidatePoints($id);
        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    'gamification' => $gamification,
                ],
            ],
            [
                "upsert" => true,
            ]
        );
    }

    /**
     * Increments the user's gamification score based on the given action.
     *
     * @param string|null $id The ID of the user.
     * @param string|null $action The action performed by the user.
     * @return void
     */
    public function incrementUser(?string $id, ?string $action): void
    {
        if ($id != "" && $action != "") {
            if (defined("self::POINTS_" . strtoupper($action))) {
                $toAdd = constant("self::POINTS_" . strtoupper($action));

                $this->db->update(
                    PersonInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($id),
                    ],
                    [
                        '$inc' =>
                                                [
                                                    'gamification.actions.' . $action => $toAdd,
                                                    'gamification.actions.total' => $toAdd,
                                                    'gamification.total' => $toAdd,
                                                ],
                    ]
                );
            }
        }
    }
}
