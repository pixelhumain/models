<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\ClassifiedInterface;
use PixelHumain\Models\Interfaces\CmsInterface;
use PixelHumain\Models\Interfaces\CommentInterface;
use PixelHumain\Models\Interfaces\CrowdfundingInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PoiInterface;

use PixelHumain\Models\Interfaces\RessourceInterface;
use PixelHumain\Models\Traits\ActionRoomTrait;
use PixelHumain\Models\Traits\ActionTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\FormTrait;
use PixelHumain\Models\Traits\Interfaces\ActionRoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\FormTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTraitInterface;

use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\NewsTrait;
use PixelHumain\Models\Traits\NotificationTrait;

use PixelHumain\Models\Traits\PersonTrait;

class Comment extends BaseModel implements CommentInterface, CostumTraitInterface, DataValidatorTraitInterface, ActionRoomTraitInterface, NewsTraitInterface, AuthorisationTraitInterface, ActionTraitInterface, PersonTraitInterface, NotificationTraitInterface, FormTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use DataValidatorTrait;
    use ActionRoomTrait;
    use NewsTrait;
    use AuthorisationTrait;
    use ActionTrait;
    use PersonTrait;
    use NotificationTrait;
    use FormTrait;

    //From Post/Form name to database field name
    private array $dataBinding = [
        "content" => [
            "name" => "text",
            "rules" => ["required"],
        ],
        "author" => [
            "name" => "author",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "contextId" => [
            "name" => "contextId",
            "rules" => ["required"],
        ],
        "contextType" => [
            "name" => "contextType",
            "rules" => ["required"],
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "source" => [
            "name" => "source",
        ],
    ];

    private array $defaultDiscussOptions = [
        CommentInterface::COMMENT_ON_TREE => true,
        CommentInterface::COMMENT_ANONYMOUS => false,
        CommentInterface::ONE_COMMENT_ONLY => false,
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    // Todo : $commentId n'est pas utilisé
    /**
     * Get the collection field name and validate the comment field value.
     *
     * @param string $commentFieldName The name of the comment field.
     * @param mixed $commentFieldValue The value of the comment field.
     * @param string|null $commentId The ID of the comment (optional).
     * @return string The validated collection field name.
     */
    private function getCollectionFieldNameAndValidate(string $commentFieldName, $commentFieldValue, ?string $commentId = null): string
    {
        return $this->getModelDataValidator()->getCollectionFieldNameAndValidate($this->dataBinding, $commentFieldName, $commentFieldValue);
    }

    /**
     * Retrieves a comment by its ID.
     *
     * @param string $id The ID of the comment.
     * @return array|null The comment data as an array or null if not found.
     */
    public function getById(string $id): ?array
    {
        $comment = $this->db->findOne(CommentInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);

        if (isset($comment)) {
            $comment["author"] = $this->getModelPerson()->getSimpleUserById((string) $comment["author"]);
        }

        return $comment;
    }

    // FIXME : pas assez de parametre pour findAndSort il manque sort
    // TODO : est ce que c'est utilisé vu que c'est pas valide actuellement ?
    /**
     * Retrieves comments based on the specified parameters.
     *
     * @param array $params The parameters used to filter the comments.
     * @return array An array of comments that match the specified parameters.
     */
    public function getWhere(array $params): array
    {
        return $this->db->findAndSort(CommentInterface::COLLECTION, $params);
    }

    /**
     * Counts the number of comments from a specific source.
     *
     * @param mixed|null $from The source from which to count the comments. Defaults to null.
     * @param string $type The type of the source.
     * @param string $id The ID of the source.
     * @param string|null $path The path of the source. Defaults to null.
     * @return int The number of comments from the specified source.
     */
    public function countFrom($from = null, string $type, string $id, ?string $path = null): int
    {
        $res = 0;
        $params = [
            "contextType" => $type,
            "contextId" => $id,
        ];
        if (! empty($from)) {
            $params["created"] = [
                '$gt' => intval($from),
            ];
        }
        if (! empty($path)) {
            $params["path"] = $path;
        }

        $res = $this->db->count(CommentInterface::COLLECTION, $params);

        return $res;
    }

    /**
     * Retrieves comments based on specified parameters, sorts them, and limits the result.
     *
     * @param array $params The parameters used to filter the comments.
     * @param array $sort The sorting criteria for the comments.
     * @param int $limit The maximum number of comments to retrieve (default is 1).
     * @return array The array of comments that match the specified parameters, sorted and limited.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array
    {
        return $this->db->findAndSort(CommentInterface::COLLECTION, $params, $sort, $limit);
    }

    public function insert(array $comment, string $userId): array
    {
        $comment = $this->getModelDataValidator()->clearUserInput($comment);
        $options = $this->getCommentOptions((string) $comment["contextId"], (string) $comment["contextType"]);
        $costum = $this->getModelCostum()->getCostum();
        $content = ! empty($comment["text"]) ? trim((string) $comment["text"]) : null;
        if (empty($content)) {
            return [
                "result" => false,
                "msg" => $this->language->t("comment", "Please add content to your comment !"),
            ];
        }

        //TODO SBAR - add check
        $newComment = [
            "contextId" => $comment["contextId"],
            "contextType" => $comment["contextType"],
            "parentId" => @$comment["parentCommentId"],
            "text" => $content,
            "created" => time(),
            "author" => $userId,
            "collection" => CommentInterface::COLLECTION,
            "tags" => @$comment["tags"],
            "status" => CommentInterface::STATUS_POSTED,
            "argval" => @$comment["argval"],
        ];

        if (! empty($comment["path"])) {
            $newComment["path"] = $comment["path"];
        }

        if (! empty($comment["rating"])) {
            $newComment["rating"] = (int) $comment["rating"];
        }
        if (! empty($comment["mentions"])) {
            $newComment["mentions"] = $comment["mentions"];
        }
        if (is_array($costum) && ! empty($costum) && ! empty($costum["slug"])) {
            $newComment["source"] = [
                "insertOrigin" => "costum",
                "key" => (string) $costum["slug"],
                "keys" => [(string) $costum["slug"]],
            ];
        }
        if ($this->canUserComment((string) $comment["contextId"], (string) $comment["contextType"], $userId, $options)) {
            $newComment = $this->db->insert(CommentInterface::COLLECTION, $newComment);
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("comment", "Error calling the serveur : contact your administrator."),
            ];
        }

        if (is_array($newComment)) {
            $newComment["author"] = $this->getCommentAuthor($newComment, $options);
            $res = [
                "result" => true,
                "time" => time(),
                "msg" => $this->language->t("comment", "The comment has been posted"),
                "newComment" => $newComment,
                "id" => $newComment["_id"],
            ];
            if (! empty($newComment["mentions"])) {
                $author = [
                    "id" => $this->session->getUserId(),
                    "type" => PersonInterface::COLLECTION,
                    "name" => $this->session->getUserName(),
                ];
                $target = [
                    "type" => $comment["contextType"],
                    "id" => $comment["contextId"],
                ];
                $object = [
                    "type" => CommentInterface::COLLECTION,
                    "id" => (string) $newComment["_id"],
                ];
                $this->getModelNotification()->notifyMentionOn($author, $target, $newComment["mentions"], $object);
            }

            $objectNotif = null;
            $typeAction = $comment["contextType"];
            if (! empty($comment["parentCommentId"])) {
                $objectNotif = [
                    "id" => $comment["parentCommentId"],
                    "type" => CommentInterface::COLLECTION,
                ];
                $typeAction = CommentInterface::COLLECTION;
            } else {
                $objectNotif = [
                    "id" => (string) $newComment["_id"],
                    "type" => CommentInterface::COLLECTION,
                ];
            }
            if ($comment["contextType"] != FormInterface::ANSWER_COLLECTION) {
                $this->getModelNotification()->constructNotification(
                    ActStrInterface::VERB_COMMENT,
                    [
                        "id" => $this->session->getUserId(),
                        "name" => $this->session->getUserName(),
                    ],
                    [
                        "type" => $comment["contextType"],
                        "id" => $comment["contextId"],
                        "name" => ! empty($options["name"]) ? (string) $options["name"] : '',
                    ],
                    $objectNotif,
                    $typeAction,
                    null,
                    $newComment["text"]
                );
            } else {
                $this->getModelForm()->createNotificationAnswer($comment);
            }
            //Increment comment count (can have multiple comment by user)
            $resAction = $this->getModelAction()->addAction($userId, $comment["contextId"], $comment["contextType"], ActionInterface::ACTION_COMMENT, false, true, null, (! empty($comment["path"]) ? $comment["path"] : null));
            if (! empty($resAction["result"]) && $resAction["result"] === false) {
                $res = [
                    "result" => false,
                    "msg" => $this->language->t("comment", "Something went really bad"),
                ];
            }
        } else {
            $res = [
                "result" => false,
                "msg" => $this->language->t("comment", "Something went really bad"),
            ];
        }

        return $res;
    }

    /**
     * Builds the comments tree for a given context.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $userId The ID of the user.
     * @param array|null $filters Optional filters to apply.
     * @param mixed $path Optional path parameter.
     * @return array The comments tree.
     */
    public function buildCommentsTree(string $contextId, string $contextType, string $userId, ?array $filters = null, $path = null): array
    {
        $commentTree = [];

        $options = $this->getCommentOptions($contextId, $contextType);

        //1. Retrieve all comments of that context that are, root of the comment tree (parentId = "" or empty)
        $whereContext = [
            "contextId" => $contextId,
            "contextType" => $contextType,
        ];

        if (! empty($path)) {
            $whereContext['path'] = $path;
        }

        if (is_array($filters) && ! empty($filters)) {
            foreach ($filters as $k => $value) {
                if ($k == "currentDay") {
                    $start = strtotime((string) $value . " 00:00:00");
                    $end = strtotime((string) $value . " 23:59:59");

                    // find dates between 1/15/2010 and 1/30/2010
                    $whereContext["created"] = [
                        '$gt' => $start,
                        '$lte' => $end,
                    ];
                }
                if ($value == "rating") {
                    $whereContext["rating"] = [
                        '$exists' => true,
                    ];
                }
            }
        }
        $nbComment = $this->db->count(CommentInterface::COLLECTION, $whereContext);

        $whereRoot = $whereContext;
        $whereRoot['$or'] = [[
            "parentId" => "",
        ], [
            "parentId" => [
                '$exists' => false,
            ],
        ]];
        $sort = [
            "created" => -1,
        ];
        $commentsRoot = $this->db->findAndSort(CommentInterface::COLLECTION, $whereRoot, $sort);

        foreach ($commentsRoot as $commentId => $comment) {
            if (is_array($comment)) {
                //Get SubComment if option "tree" is set to true
                if ($options[CommentInterface::COMMENT_ON_TREE] == true) {
                    //2. Get all the children of the comments recurslivly
                    $subComments = $this->getSubComments((string) $commentId, $options);
                    $comment["replies"] = $subComments;
                } else {
                    $comment["replies"] = [];
                }
                $comment["author"] = $this->getCommentAuthor($comment, $options);

                //Manage comment declared as abused
                if (@$comment["status"] == CommentInterface::STATUS_DELETED || @$comment["status"] == CommentInterface::STATUS_DECLARED_ABUSED) {
                    $comment["text"] = $this->language->t("comment", "This comment has been deleted because of his content.");
                }
                $commentTree[$commentId] = $comment;
            }
        }

        //3. Manage the oneCommentOnly option
        $canComment = $this->canUserComment($contextId, $contextType, $userId, $options);

        //4. Get community selected comments
        $communitySelectedComments = $this->getCommunitySelectedComments($contextId, $contextType, $options);
        //5. Abused comments
        $abusedComments = $this->getAbusedComments($contextId, $contextType, $options);

        return [
            "result" => true,
            "msg" => "The comment tree has been retrieved with success",
            "options" => $options,
            "canComment" => $canComment,
            "nbComment" => $nbComment,
            "comments" => $commentTree,
            "communitySelectedComments" => $communitySelectedComments,
            "abusedComments" => $abusedComments,
        ];
    }

    /**
     * Retrieves the sub-comments for a given comment.
     *
     * @param string $commentId The ID of the comment.
     * @param array|null $options Additional options for retrieving the sub-comments.
     * @return array The array of sub-comments.
     */
    private function getSubComments(string $commentId, ?array $options): array
    {
        $comments = [];

        $where = [
            "parentId" => $commentId,
        ];
        $comments = $this->db->find(CommentInterface::COLLECTION, $where);

        foreach ($comments as $commentId => $comment) {
            if (is_array($comment)) {
                $subComments = $this->getSubComments((string) $commentId, $options);
                $comment["author"] = $this->getCommentAuthor($comment, $options);
                if (@$comment["status"] == CommentInterface::STATUS_DELETED) {
                    $comment["text"] = $this->language->t("comment", "This comment has been deleted because of his content.");
                }

                $comment["replies"] = $subComments;
                $comments[$commentId] = $comment;
            }
        }
        return $comments;
    }

    /**
     * Retrieves the author of a comment.
     *
     * @param array $comment The comment data.
     * @param array|null $options Additional options for retrieving the author.
     * @return array The author of the comment.
     */
    private function getCommentAuthor(array $comment, ?array $options): array
    {
        if (! empty($comment["author"])) {
            $author = $this->getModelPerson()->getMinimalUserById((string) $comment["author"]);
        } else {
            $author = [
                "name" => "Unknown",
            ];
        }

        //If anonymous option is set the author of the comment will not displayed
        if (is_array($options) && $options[CommentInterface::COMMENT_ANONYMOUS] == true) {
            $author = [
                "name" => "Anonymous",
                "address" => [
                    "addressLocality" => $author["address"]["addressLocality"] ?? "",
                ],
            ];
        }

        return $author;
    }

    /**
     * Check if a user can comment on a specific context.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $userId The ID of the user.
     * @param array|null $options Additional options for checking the permission (optional).
     * @return bool Returns true if the user can comment, false otherwise.
     */
    private function canUserComment(string $contextId, string $contextType, string $userId, ?array $options): bool
    {
        $canComment = true;
        if (is_array($options) && $options[CommentInterface::ONE_COMMENT_ONLY] == true) {
            $where = [
                "contextId" => $contextId,
                "contextType" => $contextType,
                "author" => $userId,
            ];
            $nbComments = $this->db->count(CommentInterface::COLLECTION, $where);
            if ($nbComments > 0) {
                $canComment = false;
            }
        }

        return $canComment;
    }

    /**
     * Récupère les commentaires sélectionnés de la communauté.
     *
     * @param string $contextId L'identifiant du contexte.
     * @param string $contextType Le type de contexte.
     * @param array|null $options Les options de récupération des commentaires.
     * @return array Les commentaires sélectionnés de la communauté.
     */
    public function getCommunitySelectedComments(string $contextId, string $contextType, ?array $options): array
    {
        $res = [];
        //1. Retrieve average number of like on the comment tree
        $opts = [[
            '$match' => [
                "contextId" => $contextId,
                "contextType" => "$contextType",
            ],
        ], [
            '$group' => [
                '_id' => '1',
                'avgVoteUp' => [
                    '$avg' => '$voteUpCount',
                ],
            ],
        ]];
        $result = $this->db->aggregate(CommentInterface::COLLECTION, $opts);

        if (@$result["ok"]) {
            $avgVoteUp = $result["result"][0]["avgVoteUp"] ?? 0;
        } else {
            throw new CTKException($this->language->t("comment", "Something went wrong retrieving the average vote up !"));
        }

        $whereContext = [
            "contextId" => $contextId,
            "contextType" => $contextType,
            "voteUpCount" => [
                '$gte' => $avgVoteUp,
            ],
        ];
        $sort = [
            "voteUpCount" => -1,
        ];
        $comments = $this->db->findAndSort(CommentInterface::COLLECTION, $whereContext, $sort);

        foreach ($comments as $commentId => $comment) {
            if (is_array($comment)) {
                $comment["author"] = $this->getCommentAuthor($comment, $options);
                $comment["replies"] = [];
                $res[(string) $commentId] = $comment;
            }
        }
        return $res;
    }

    /**
     * Retrieves the abused comments based on the given context ID, context type, and options.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param array|null $options Additional options for retrieving the abused comments.
     * @return array The array of abused comments.
     */
    public function getAbusedComments(string $contextId, string $contextType, ?array $options): array
    {
        $res = [];
        //1. Retrieve the comments with at least one abuse
        $whereContext = [
            "contextId" => $contextId,
            "contextType" => $contextType,
            "status" => CommentInterface::STATUS_DECLARED_ABUSED,
            "reportAbuseCount" => [
                '$gte' => 0,
            ],
        ];
        $sort = [
            "created" => -1,
        ];
        $comments = $this->db->findAndSort(CommentInterface::COLLECTION, $whereContext, $sort);

        foreach ($comments as $commentId => $comment) {
            if (is_array($comment)) {
                $comment["author"] = $this->getCommentAuthor($comment, $options);
                $comment["replies"] = [];
                $res[(string) $commentId] = $comment;
            }
        }
        return $res;
    }

    /**
     * Retrieves the comment options for a given ID and type.
     *
     * @param string $id The ID of the comment.
     * @param string $type The type of the comment.
     * @return array The comment options.
     */
    public function getCommentOptions(string $id, string $type): array
    {
        $res = $this->defaultDiscussOptions;

        $collection = $this->db->findOneById($type, $id, [
            "commentOptions" => 1,
        ]);

        if (is_array($collection) && ! empty($collection["commentOptions"])) {
            $res = $collection;
        }

        return $res;
    }

    /**
     * Saves the comment options.
     *
     * @param string $id The comment ID.
     * @param string $type The comment type.
     * @param array|null $options The comment options.
     * @return array The updated comment options.
     */
    public function saveCommentOptions(string $id, string $type, ?array $options): array
    {
        $res = [
            "result" => true,
            "msg" => $this->language->t("comment", "The comment options has been saved successfully"),
        ];

        $where = [
            "_id" => $this->db->MongoId($id),
        ];
        $set = [
            "commentOptions" => $options,
        ];

        //Update the collection
        $res = $this->db->update($type, $where, [
            '$set' => $set,
        ]);

        return $res;
    }


    //------------------------------------------------------------//
    //---------------------- Abuse Process -----------------------//
    //------------------------------------------------------------//

    /**
     * Signale un abus sur un commentaire.
     *
     * @param string $userId L'identifiant de l'utilisateur signalant l'abus.
     * @param string $commentId L'identifiant du commentaire signalé.
     * @param string $reason La raison du signalement.
     * @return array Les informations sur le signalement effectué.
     */
    public function reportAbuse(string $userId, string $commentId, string $reason): array
    {
        $map = [];
        $action = ActionInterface::ACTION_REPORT_ABUSE;
        $collection = CommentInterface::COLLECTION;
        $user = $this->getModelPerson()->getById($userId);
        $comment = ($commentId) ? $this->db->findOne($collection, [
            "_id" => $this->db->MongoId($commentId),
        ]) : null;

        if ($user && $comment) {
            //check user hasn't allready done the action
            if (! isset($comment[$action])) {
                $dbMethod = '$set';

                $map[ActionInterface::NODE_ACTIONS . "." . $collection . "." . $commentId . "." . $action] = $action;
                //update the user table
                //adds or removes an action
                $this->db->update(
                    PersonInterface::COLLECTION,
                    [
                        "_id" => $user["_id"],
                    ],
                    [
                        $dbMethod => $map,
                    ]
                );

                //push unique user Ids into action node list
                $dbMethod = '$set';

                //increment according to specifications
                $inc = 1;

                $this->db->update(
                    $collection,
                    [
                        "_id" => $this->db->MongoId($commentId),
                    ],
                    [
                        $dbMethod => [
                            $action . "." . $userId => $reason,
                            'status' => CommentInterface::STATUS_DECLARED_ABUSED,
                        ],
                        '$inc' => [
                            $action . "Count" => $inc,
                        ],
                    ]
                );
                $this->getModelAction()->addActionHistory($userId, $commentId, $collection, $action);

                $res = [
                    "result" => true,
                    "userActionSaved" => true,
                    "user" => $this->db->findOne(PersonInterface::COLLECTION, [
                        "_id" => $this->db->MongoId($userId),
                    ], ["actions"]),
                    "element" => $this->db->findOne($collection, [
                        "_id" => $this->db->MongoId($commentId),
                    ], [$action]),
                    "msg" => "Ok !",
                ];
            } else {
                $res = [
                    "result" => true,
                    "userAllreadyDidAction" => true,
                ];
            }
        } else {
            $res = [
                "result" => false,
                "msg" => "An error occured",
            ];
        }
        return $res;
    }

    /**
     * Change the status of a comment.
     *
     * @param string $userId The ID of the user performing the action.
     * @param string $commentId The ID of the comment to change the status of.
     * @param string $action The action to perform on the comment status.
     * @return array The updated comment data.
     */
    public function changeStatus(string $userId, string $commentId, string $action): array
    {
        $comment = $this->getById($commentId);
        if (empty($comment)) {
            return [
                "result" => false,
                "msg" => "This comment doesn't exist",
            ];
        }

        if ($this->getModelAuthorisation()->canEditItem($userId, (string) $comment["contextType"], (string) $comment["contextId"])) {
            $this->db->update(
                CommentInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($commentId),
                ],
                [
                    '$set' => [
                        "status" => $action,
                    ],
                ]
            );
        }

        $res = [
            "result" => true,
            "userActionSaved" => true,
            "user" => $this->db->findOne(PersonInterface::COLLECTION, [
                "_id" => $this->db->MongoId($userId),
            ], ["actions"]),
            "element" => $this->db->findOne(CommentInterface::COLLECTION, [
                "_id" => $this->db->MongoId($commentId),
            ], [$action]),
            "msg" => "Ok !",
        ];
        return $res;
    }

    // TODO : $userId not used
    // TODO : on ne verifie pas $name et $value
    // TODO : estce que cette fonction est utilisée ?
    /**
     * Update a field in the comment.
     *
     * @param string $commentId The ID of the comment.
     * @param string $name The name of the field to update.
     * @param mixed $value The new value for the field.
     * @param string|null $userId The ID of the user performing the update (optional).
     * @return array The updated comment.
     */
    public function updateField(string $commentId, string $name, $value, ?string $userId = null): array
    {
        $set = [
            $name => $value,
        ];
        //update the project
        $this->db->update(
            CommentInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($commentId),
            ],
            [
                '$set' => $set,
            ]
        );

        return [
            "result" => true,
            "msg" => $this->language->t("common", "Comment well updated"),
            "id" => $commentId,
        ];
    }

    /**
     * Update a comment.
     *
     * @param string $id The ID of the comment to update.
     * @param array $params The parameters to update the comment with.
     * @return array The updated comment.
     */
    public function update(string $id, array $params): array
    {
        $params = $this->getModelDataValidator()->clearUserInput($params);
        if (! empty($params["text"]) && $this->session->getUserId()) {
            $comment = $this->getById($id);
            if (! empty($comment)) {
                if ((! empty($comment["author"]["id"]) && $comment["author"]["id"] == $this->session->getUserId()) || ($this->session->get("userIsAdmin") && $this->session->get("userIsAdmin") == true)) {
                    $set = [
                        "text" => $params["text"],
                    ];
                    $unset = [];
                    if (@$params["mentions"]) {
                        $set["mentions"] = $params["mentions"];
                    } else {
                        $unset["mentions"] = "";
                    }
                    $modify = [
                        '$set' => $set,
                    ];
                    if (@$unset && ! empty($unset)) {
                        $modify['$unset'] = $unset;
                    }
                    //update the project
                    $comment = $this->db->update(
                        CommentInterface::COLLECTION,
                        [
                            "_id" => $this->db->MongoId($id),
                        ],
                        $modify
                    );

                    return [
                        "result" => true,
                        "msg" => $this->language->t("common", "Your comment is well updated"),
                        "comment" => $comment,
                    ];
                } else {
                    return [
                        "result" => false,
                        "msg" => $this->language->t("common", "you are not the author of the comment"),
                        "comment" => $comment,
                    ];
                }
            } else {
                return [
                    "result" => false,
                    "msg" => $this->language->t("comment", "This comment doesn't exist"),
                    "comment" => $comment,
                ];
            }
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("comment", "Something went really bad"),
            ];
        }
    }

    /**
     * Deletes a comment.
     *
     * @param string $id The ID of the comment to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function delete(string $id, string $userId): array
    {
        $comment = $this->getById($id);
        if (empty($comment)) {
            return [
                "result" => false,
                "msg" => "This comment doesn't exist",
            ];
        }

        if (! empty($comment["author"]["id"]) && $comment["author"]["id"] == $userId || ($this->session->get("userIsAdmin") && $this->session->get("userIsAdmin") == true) || ($this->session->get("userIsAdminPublic") && $this->session->get("userIsAdminPublic") == true)) {
            $this->getModelAction()->addAction($userId, (string) $comment["contextId"], (string) $comment["contextType"], ActionInterface::ACTION_COMMENT, true, false);
            $this->db->remove(CommentInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "The comment has been deleted with success"),
            ];
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "you are not the author of the comment"),
                "comment" => $comment,
            ];
        }
    }

    /**
     * Deletes all comments associated with a specific context.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $userId The ID of the user.
     * @param bool $deleteProcess Whether to delete the associated process or not. Default is false.
     * @return array The deleted comments.
     */
    public function deleteAllContextComments(string $contextId, string $contextType, string $userId, bool $deleteProcess = false): array
    {
        error_log("try to delete comments of context : " . $contextId . "/" . $contextType);
        if ($contextType == ActionRoomInterface::COLLECTION) {
            $canDelete = $this->getModelActionRoom()->canAdministrate($userId, $contextId);
        } elseif ($contextType == NewsInterface::COLLECTION) {
            $canDelete = $this->getModelNews()->canAdministrate($userId, $contextId, $deleteProcess);
        } elseif (in_array($contextType, [RessourceInterface::COLLECTION, ClassifiedInterface::COLLECTION, PoiInterface::COLLECTION, CrowdfundingInterface::COLLECTION, CmsInterface::COLLECTION, BadgeInterface::COLLECTION])) {
            $canDelete = $this->getModelAuthorisation()->canDeleteElement($contextId, $contextType, $this->session->getUserId());
        } else {
            return [
                "result" => false,
                "msg" => "This contextType '" . $contextType . "' is not yet implemented.",
            ];
        }

        if ($canDelete) {
            $where = [
                '$and' => [[
                    "contextId" => $contextId,
                ], [
                    "contextType" => $contextType,
                ]],
            ];
            $res = $this->db->remove(CommentInterface::COLLECTION, $where);
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "You are not allowed to delete the comments of this context"),
            ];
        }

        if ($res) {
            return [
                "result" => true,
                "msg" => $this->language->t("common", "The comments of the context have been deleted with success"),
            ];
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "Something went really bad"),
            ];
        }
    }

    /**
     * Retrieves an array of comments to be moderated.
     *
     * @param array|null $whereAdditional Additional conditions to filter the comments (optional).
     * @param int $limit The maximum number of comments to retrieve (optional).
     * @return array An array of comments to be moderated.
     */
    public function getCommentsToModerate(?array $whereAdditional = null, int $limit = 0): array
    {
        $userId = $this->session->getUserId();
        if ($userId !== null) {
            $where = [
                "reportAbuse" => [
                    '$exists' => 1,
                ],
                "moderate.isAnAbuse" => [
                    '$exists' => 0,
                ],
                "reportAbuseCount" => [
                    '$lt' => 5,
                ],
                "moderate." . $userId => [
                    '$exists' => 0,
                ],
            ];
            if (is_countable($whereAdditional) ? count($whereAdditional) : 0) {
                $where = array_merge($where, $whereAdditional ?? []);
            }
            return $this->db->findAndSort(CommentInterface::COLLECTION, $where, [
                "date" => 1,
            ], $limit);
        } else {
            return [];
        }
    }
}
