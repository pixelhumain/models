<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MailErrorInterface;

trait MailErrorTrait
{
    protected ?MailErrorInterface $mailError = null;

    /**
     * Set the mailError.
     *
     * @param MailErrorInterface $mailError The mailError object to set.
     * @return void
     */
    public function setModelMailError(MailErrorInterface $mailError): void
    {
        $this->mailError = $mailError;
    }

    /**
     * Gets the mailError.
     *
     * @return MailErrorInterface The mailError.
     */
    public function getModelMailError(): MailErrorInterface
    {
        if ($this->mailError === null) {
            throw new Exception(MailErrorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->mailError;
    }
}
