<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\AdresseDataGouvInterface;

trait AdresseDataGouvTrait
{
    protected ?AdresseDataGouvInterface $adresseDataGouv = null;

    /**
     * Set the adresseDataGouv.
     *
     * @param AdresseDataGouvInterface $adresseDataGouv The adresseDataGouv object to set.
     * @return void
     */
    public function setModelAdresseDataGouv(AdresseDataGouvInterface $adresseDataGouv): void
    {
        $this->adresseDataGouv = $adresseDataGouv;
    }

    /**
     * Gets the adresseDataGouv.
     *
     * @return AdresseDataGouvInterface The adresseDataGouv.
     */
    public function getModelAdresseDataGouv(): AdresseDataGouvInterface
    {
        if ($this->adresseDataGouv === null) {
            throw new Exception(AdresseDataGouvInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->adresseDataGouv;
    }
}
