<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\AuthorisationInterface;

trait AuthorisationTrait
{
    protected ?AuthorisationInterface $authorisation = null;

    /**
     * Set the authorisation.
     *
     * @param AuthorisationInterface $authorisation The authorisation object to set.
     * @return void
     */
    public function setModelAuthorisation(AuthorisationInterface $authorisation): void
    {
        $this->authorisation = $authorisation;
    }

    /**
     * Gets the authorisation.
     *
     * @return AuthorisationInterface The authorisation.
     */
    public function getModelAuthorisation(): AuthorisationInterface
    {
        if ($this->authorisation === null) {
            throw new Exception(AuthorisationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->authorisation;
    }
}
