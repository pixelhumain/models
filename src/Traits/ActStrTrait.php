<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActStrInterface;

trait ActStrTrait
{
    protected ?ActStrInterface $actStr = null;

    /**
     * Set the actStr.
     *
     * @param ActStrInterface $actStr The actStr object to set.
     * @return void
     */
    public function setModelActStr(ActStrInterface $actStr): void
    {
        $this->actStr = $actStr;
    }

    /**
     * Gets the actStr.
     *
     * @return ActStrInterface The actStr.
     */
    public function getModelActStr(): ActStrInterface
    {
        if ($this->actStr === null) {
            throw new Exception(ActStrInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->actStr;
    }
}
