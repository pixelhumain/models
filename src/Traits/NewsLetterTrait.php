<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NewsLetterInterface;

trait NewsLetterTrait
{
    protected ?NewsLetterInterface $newsLetter = null;

    /**
     * Set the newsLetter.
     *
     * @param NewsLetterInterface $newsLetter The newsLetter object to set.
     * @return void
     */
    public function setModelNewsLetter(NewsLetterInterface $newsLetter): void
    {
        $this->newsLetter = $newsLetter;
    }

    /**
     * Gets the newsLetter.
     *
     * @return NewsLetterInterface The newsLetter.
     */
    public function getModelNewsLetter(): NewsLetterInterface
    {
        if ($this->newsLetter === null) {
            throw new Exception(NewsLetterInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->newsLetter;
    }
}
