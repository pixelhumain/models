<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ApplicationInterface;

trait ApplicationTrait
{
    protected ?ApplicationInterface $application = null;

    /**
     * Set the application.
     *
     * @param ApplicationInterface $application The application object to set.
     * @return void
     */
    public function setModelApplication(ApplicationInterface $application): void
    {
        $this->application = $application;
    }

    /**
     * Gets the application.
     *
     * @return ApplicationInterface The application.
     */
    public function getModelApplication(): ApplicationInterface
    {
        if ($this->application === null) {
            throw new Exception(ApplicationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->application;
    }
}
