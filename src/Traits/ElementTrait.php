<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ElementInterface;

trait ElementTrait
{
    protected ?ElementInterface $element = null;

    /**
     * Set the element.
     *
     * @param ElementInterface $element The element object to set.
     * @return void
     */
    public function setModelElement(ElementInterface $element): void
    {
        $this->element = $element;
    }

    /**
     * Gets the element.
     *
     * @return ElementInterface The element.
     */
    public function getModelElement(): ElementInterface
    {
        if ($this->element === null) {
            throw new Exception(ElementInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->element;
    }
}
