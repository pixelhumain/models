<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\RiskInterface;

trait RiskTrait
{
    protected ?RiskInterface $risk = null;

    /**
     * Set the risk.
     *
     * @param RiskInterface $risk The risk object to set.
     * @return void
     */
    public function setModelRisk(RiskInterface $risk): void
    {
        $this->risk = $risk;
    }

    /**
     * Gets the risk.
     *
     * @return RiskInterface The risk.
     */
    public function getModelRisk(): RiskInterface
    {
        if ($this->risk === null) {
            throw new Exception(RiskInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->risk;
    }
}
