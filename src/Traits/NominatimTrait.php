<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NominatimInterface;

trait NominatimTrait
{
    protected ?NominatimInterface $nominatim = null;

    /**
     * Set the nominatim.
     *
     * @param NominatimInterface $nominatim The nominatim object to set.
     * @return void
     */
    public function setModelNominatim(NominatimInterface $nominatim): void
    {
        $this->nominatim = $nominatim;
    }

    /**
     * Gets the nominatim.
     *
     * @return NominatimInterface The nominatim.
     */
    public function getModelNominatim(): NominatimInterface
    {
        if ($this->nominatim === null) {
            throw new Exception(NominatimInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->nominatim;
    }
}
