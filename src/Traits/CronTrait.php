<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CronInterface;

trait CronTrait
{
    protected ?CronInterface $cron = null;

    /**
     * Set the cron.
     *
     * @param CronInterface $cron The cron object to set.
     * @return void
     */
    public function setModelCron(CronInterface $cron): void
    {
        $this->cron = $cron;
    }

    /**
     * Gets the cron.
     *
     * @return CronInterface The cron.
     */
    public function getModelCron(): CronInterface
    {
        if ($this->cron === null) {
            throw new Exception(CronInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->cron;
    }
}
