<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MicroFormatsInterface;

trait MicroFormatsTrait
{
    protected ?MicroFormatsInterface $microFormats = null;

    /**
     * Set the microFormats.
     *
     * @param MicroFormatsInterface $microFormats The microFormats object to set.
     * @return void
     */
    public function setModelMicroFormats(MicroFormatsInterface $microFormats): void
    {
        $this->microFormats = $microFormats;
    }

    /**
     * Gets the microFormats.
     *
     * @return MicroFormatsInterface The microFormats.
     */
    public function getModelMicroFormats(): MicroFormatsInterface
    {
        if ($this->microFormats === null) {
            throw new Exception(MicroFormatsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->microFormats;
    }
}
