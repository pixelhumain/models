<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ResolutionInterface;

trait ResolutionTrait
{
    protected ?ResolutionInterface $resolution = null;

    /**
     * Set the resolution.
     *
     * @param ResolutionInterface $resolution The resolution object to set.
     * @return void
     */
    public function setModelResolution(ResolutionInterface $resolution): void
    {
        $this->resolution = $resolution;
    }

    /**
     * Gets the resolution.
     *
     * @return ResolutionInterface The resolution.
     */
    public function getModelResolution(): ResolutionInterface
    {
        if ($this->resolution === null) {
            throw new Exception(ResolutionInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->resolution;
    }
}
