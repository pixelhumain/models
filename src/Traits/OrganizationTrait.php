<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\OrganizationInterface;

trait OrganizationTrait
{
    protected ?OrganizationInterface $organization = null;

    /**
     * Set the organization.
     *
     * @param OrganizationInterface $organization The organization object to set.
     * @return void
     */
    public function setModelOrganization(OrganizationInterface $organization): void
    {
        $this->organization = $organization;
    }

    /**
     * Gets the organization.
     *
     * @return OrganizationInterface The organization.
     */
    public function getModelOrganization(): OrganizationInterface
    {
        if ($this->organization === null) {
            throw new Exception(OrganizationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->organization;
    }
}
