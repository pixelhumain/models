<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActivityStreamReferenceInterface;

trait ActivityStreamReferenceTrait
{
    protected ?ActivityStreamReferenceInterface $activityStreamReference = null;

    /**
     * Set the activityStreamReference.
     *
     * @param ActivityStreamReferenceInterface $activityStreamReference The activityStreamReference object to set.
     * @return void
     */
    public function setModelActivityStreamReference(ActivityStreamReferenceInterface $activityStreamReference): void
    {
        $this->activityStreamReference = $activityStreamReference;
    }

    /**
     * Gets the activityStreamReference.
     *
     * @return ActivityStreamReferenceInterface The activityStreamReference.
     */
    public function getModelActivityStreamReference(): ActivityStreamReferenceInterface
    {
        if ($this->activityStreamReference === null) {
            throw new Exception(ActivityStreamReferenceInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activityStreamReference;
    }
}
