<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ConvertInterface;

trait ConvertTrait
{
    protected ?ConvertInterface $convert = null;

    /**
     * Set the convert.
     *
     * @param ConvertInterface $convert The convert object to set.
     * @return void
     */
    public function setModelConvert(ConvertInterface $convert): void
    {
        $this->convert = $convert;
    }

    /**
     * Gets the convert.
     *
     * @return ConvertInterface The convert.
     */
    public function getModelConvert(): ConvertInterface
    {
        if ($this->convert === null) {
            throw new Exception(ConvertInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->convert;
    }
}
