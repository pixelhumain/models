<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CircuitInterface;

trait CircuitTrait
{
    protected ?CircuitInterface $circuit = null;

    /**
     * Set the circuit.
     *
     * @param CircuitInterface $circuit The circuit object to set.
     * @return void
     */
    public function setModelCircuit(CircuitInterface $circuit): void
    {
        $this->circuit = $circuit;
    }

    /**
     * Gets the circuit.
     *
     * @return CircuitInterface The circuit.
     */
    public function getModelCircuit(): CircuitInterface
    {
        if ($this->circuit === null) {
            throw new Exception(CircuitInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->circuit;
    }
}
