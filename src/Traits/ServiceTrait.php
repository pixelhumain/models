<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ServiceInterface;

trait ServiceTrait
{
    protected ?ServiceInterface $service = null;

    /**
     * Set the service.
     *
     * @param ServiceInterface $service The service object to set.
     * @return void
     */
    public function setModelService(ServiceInterface $service): void
    {
        $this->service = $service;
    }

    /**
     * Gets the service.
     *
     * @return ServiceInterface The service.
     */
    public function getModelService(): ServiceInterface
    {
        if ($this->service === null) {
            throw new Exception(ServiceInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->service;
    }
}
