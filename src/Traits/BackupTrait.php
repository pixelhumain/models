<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\BackupInterface;

trait BackupTrait
{
    protected ?BackupInterface $backup = null;

    /**
     * Set the backup.
     *
     * @param BackupInterface $backup The backup object to set.
     * @return void
     */
    public function setModelBackup(BackupInterface $backup): void
    {
        $this->backup = $backup;
    }

    /**
     * Gets the backup.
     *
     * @return BackupInterface The backup.
     */
    public function getModelBackup(): BackupInterface
    {
        if ($this->backup === null) {
            throw new Exception(BackupInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->backup;
    }
}
