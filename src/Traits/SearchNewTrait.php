<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SearchNewInterface;

trait SearchNewTrait
{
    protected ?SearchNewInterface $searchNew = null;

    /**
     * Set the searchNew.
     *
     * @param SearchNewInterface $searchNew The searchNew object to set.
     * @return void
     */
    public function setModelSearchNew(SearchNewInterface $searchNew): void
    {
        $this->searchNew = $searchNew;
    }

    /**
     * Gets the searchNew.
     *
     * @return SearchNewInterface The searchNew.
     */
    public function getModelSearchNew(): SearchNewInterface
    {
        if ($this->searchNew === null) {
            throw new Exception(SearchNewInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->searchNew;
    }
}
