<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\FolderInterface;

trait FolderTrait
{
    protected ?FolderInterface $folder = null;

    /**
     * Set the folder.
     *
     * @param FolderInterface $folder The folder object to set.
     * @return void
     */
    public function setModelFolder(FolderInterface $folder): void
    {
        $this->folder = $folder;
    }

    /**
     * Gets the folder.
     *
     * @return FolderInterface The folder.
     */
    public function getModelFolder(): FolderInterface
    {
        if ($this->folder === null) {
            throw new Exception(FolderInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->folder;
    }
}
