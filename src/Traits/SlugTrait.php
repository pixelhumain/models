<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SlugInterface;

trait SlugTrait
{
    protected ?SlugInterface $slug = null;

    /**
     * Set the slug utility of the person.
     *
     * @param SlugInterface $slug The slug object to set.
     * @return void
     */
    public function setModelSlug(SlugInterface $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * Gets the slug utility of the person.
     *
     * @return SlugInterface The slug of the person.
     */
    public function getModelSlug(): SlugInterface
    {
        if ($this->slug === null) {
            throw new Exception(SlugInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->slug;
    }
}
