<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\DataValidatorInterface;

trait DataValidatorTrait
{
    protected ?DataValidatorInterface $dataValidator = null;

    /**
     * Set the dataValidator.
     *
     * @param DataValidatorInterface $dataValidator The dataValidator object to set.
     * @return void
     */
    public function setModelDataValidator(DataValidatorInterface $dataValidator): void
    {
        $this->dataValidator = $dataValidator;
    }

    /**
     * Gets the dataValidator.
     *
     * @return DataValidatorInterface The dataValidator.
     */
    public function getModelDataValidator(): DataValidatorInterface
    {
        if ($this->dataValidator === null) {
            throw new Exception(DataValidatorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->dataValidator;
    }

    /**
     * Retrieves the field name and validates the field value.
     *
     * @param array $dataBinding The data binding array.
     * @param string $fieldName The name of the field.
     * @param mixed $fieldValue The value of the field.
     * @param mixed $id The ID of the field.
     * @return string The validated field name.
     */
    protected function getFieldNameAndValidate(array $dataBinding, string $fieldName, $fieldValue, $id): string
    {
        // Assurez-vous que la classe utilisant ce trait dispose de `getDataValidator`.
        return $this->getDataValidator()->getCollectionFieldNameAndValidate($dataBinding, $fieldName, $fieldValue, $id);
    }
}
