<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CooperationInterface;

trait CooperationTrait
{
    protected ?CooperationInterface $cooperation = null;

    /**
     * Set the cooperation.
     *
     * @param CooperationInterface $cooperation The cooperation object to set.
     * @return void
     */
    public function setModelCooperation(CooperationInterface $cooperation): void
    {
        $this->cooperation = $cooperation;
    }

    /**
     * Gets the cooperation.
     *
     * @return CooperationInterface The cooperation.
     */
    public function getModelCooperation(): CooperationInterface
    {
        if ($this->cooperation === null) {
            throw new Exception(CooperationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->cooperation;
    }
}
