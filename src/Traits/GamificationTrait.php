<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\GamificationInterface;

trait GamificationTrait
{
    protected ?GamificationInterface $gamification = null;

    /**
     * Set the gamification.
     *
     * @param GamificationInterface $gamification The gamification object to set.
     * @return void
     */
    public function setModelGamification(GamificationInterface $gamification): void
    {
        $this->gamification = $gamification;
    }

    /**
     * Gets the gamification.
     *
     * @return GamificationInterface The gamification.
     */
    public function getModelGamification(): GamificationInterface
    {
        if ($this->gamification === null) {
            throw new Exception(GamificationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->gamification;
    }
}
