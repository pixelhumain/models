<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\LinkInterface;

trait LinkTrait
{
    protected ?LinkInterface $link = null;

    /**
     * Set the link.
     *
     * @param LinkInterface $link The link object to set.
     * @return void
     */
    public function setModelLink(LinkInterface $link): void
    {
        $this->link = $link;
    }

    /**
     * Gets the link.
     *
     * @return LinkInterface The link.
     */
    public function getModelLink(): LinkInterface
    {
        if ($this->link === null) {
            throw new Exception(LinkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->link;
    }
}
