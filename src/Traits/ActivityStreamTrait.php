<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;

trait ActivityStreamTrait
{
    protected ?ActivityStreamInterface $activityStream = null;

    /**
     * Set the activityStream.
     *
     * @param ActivityStreamInterface $activityStream The activityStream object to set.
     * @return void
     */
    public function setModelActivityStream(ActivityStreamInterface $activityStream): void
    {
        $this->activityStream = $activityStream;
    }

    /**
     * Gets the activityStream.
     *
     * @return ActivityStreamInterface The activityStream.
     */
    public function getModelActivityStream(): ActivityStreamInterface
    {
        if ($this->activityStream === null) {
            throw new Exception(ActivityStreamInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activityStream;
    }
}
