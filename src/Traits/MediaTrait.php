<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MediaInterface;

trait MediaTrait
{
    protected ?MediaInterface $media = null;

    /**
     * Set the media.
     *
     * @param MediaInterface $media The media object to set.
     * @return void
     */
    public function setModelMedia(MediaInterface $media): void
    {
        $this->media = $media;
    }

    /**
     * Gets the media.
     *
     * @return MediaInterface The media.
     */
    public function getModelMedia(): MediaInterface
    {
        if ($this->media === null) {
            throw new Exception(MediaInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->media;
    }
}
