<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MailInterface;

trait MailTrait
{
    protected ?MailInterface $mail = null;

    /**
     * Set the mail.
     *
     * @param MailInterface $mail The mail object to set.
     * @return void
     */
    public function setModelMail(MailInterface $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * Gets the mail.
     *
     * @return MailInterface The mail.
     */
    public function getModelMail(): MailInterface
    {
        if ($this->mail === null) {
            throw new Exception(MailInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->mail;
    }
}
