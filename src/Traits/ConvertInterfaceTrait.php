<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ConvertInterfaceInterface;

trait ConvertInterfaceTrait
{
    protected ?ConvertInterfaceInterface $convertInterface = null;

    /**
     * Set the convertInterface.
     *
     * @param ConvertInterfaceInterface $convertInterface The convertInterface object to set.
     * @return void
     */
    public function setModelConvertInterface(ConvertInterfaceInterface $convertInterface): void
    {
        $this->convertInterface = $convertInterface;
    }

    /**
     * Gets the convertInterface.
     *
     * @return ConvertInterfaceInterface The convertInterface.
     */
    public function getModelConvertInterface(): ConvertInterfaceInterface
    {
        if ($this->convertInterface === null) {
            throw new Exception(ConvertInterfaceInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->convertInterface;
    }
}
