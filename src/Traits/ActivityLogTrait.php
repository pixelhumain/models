<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActivityLogInterface;

trait ActivityLogTrait
{
    protected ?ActivityLogInterface $activityLog = null;

    /**
     * Set the activityLog.
     *
     * @param ActivityLogInterface $activityLog The activityLog object to set.
     * @return void
     */
    public function setModelActivityLog(ActivityLogInterface $activityLog): void
    {
        $this->activityLog = $activityLog;
    }

    /**
     * Gets the activityLog.
     *
     * @return ActivityLogInterface The activityLog.
     */
    public function getModelActivityLog(): ActivityLogInterface
    {
        if ($this->activityLog === null) {
            throw new Exception(ActivityLogInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activityLog;
    }
}
