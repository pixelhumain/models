<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ListsInterface;

trait ListsTrait
{
    protected ?ListsInterface $lists = null;

    /**
     * Set the lists.
     *
     * @param ListsInterface $lists The lists object to set.
     * @return void
     */
    public function setModelLists(ListsInterface $lists): void
    {
        $this->lists = $lists;
    }

    /**
     * Gets the lists.
     *
     * @return ListsInterface The lists.
     */
    public function getModelLists(): ListsInterface
    {
        if ($this->lists === null) {
            throw new Exception(ListsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->lists;
    }
}
