<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\LogInterface;

trait LogTrait
{
    protected ?LogInterface $log = null;

    /**
     * Set the log.
     *
     * @param LogInterface $log The log object to set.
     * @return void
     */
    public function setModelLog(LogInterface $log): void
    {
        $this->log = $log;
    }

    /**
     * Gets the log.
     *
     * @return LogInterface The log.
     */
    public function getModelLog(): LogInterface
    {
        if ($this->log === null) {
            throw new Exception(LogInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->log;
    }
}
