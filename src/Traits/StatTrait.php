<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\StatInterface;

trait StatTrait
{
    protected ?StatInterface $stat = null;

    /**
     * Set the stat.
     *
     * @param StatInterface $stat The stat object to set.
     * @return void
     */
    public function setModelStat(StatInterface $stat): void
    {
        $this->stat = $stat;
    }

    /**
     * Gets the stat.
     *
     * @return StatInterface The stat.
     */
    public function getModelStat(): StatInterface
    {
        if ($this->stat === null) {
            throw new Exception(StatInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->stat;
    }
}
