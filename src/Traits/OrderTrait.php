<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\OrderInterface;

trait OrderTrait
{
    protected ?OrderInterface $order = null;

    /**
     * Set the order.
     *
     * @param OrderInterface $order The order object to set.
     * @return void
     */
    public function setModelOrder(OrderInterface $order): void
    {
        $this->order = $order;
    }

    /**
     * Gets the order.
     *
     * @return OrderInterface The order.
     */
    public function getModelOrder(): OrderInterface
    {
        if ($this->order === null) {
            throw new Exception(OrderInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->order;
    }
}
