<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\OrderItemInterface;

trait OrderItemTrait
{
    protected ?OrderItemInterface $orderItem = null;

    /**
     * Set the orderItem.
     *
     * @param OrderItemInterface $orderItem The orderItem object to set.
     * @return void
     */
    public function setModelOrderItem(OrderItemInterface $orderItem): void
    {
        $this->orderItem = $orderItem;
    }

    /**
     * Gets the orderItem.
     *
     * @return OrderItemInterface The orderItem.
     */
    public function getModelOrderItem(): OrderItemInterface
    {
        if ($this->orderItem === null) {
            throw new Exception(OrderItemInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->orderItem;
    }
}
