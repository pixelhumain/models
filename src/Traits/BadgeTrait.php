<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\BadgeInterface;

trait BadgeTrait
{
    protected ?BadgeInterface $badge = null;

    /**
     * Set the badge.
     *
     * @param BadgeInterface $badge The badge object to set.
     * @return void
     */
    public function setModelBadge(BadgeInterface $badge): void
    {
        $this->badge = $badge;
    }

    /**
     * Gets the badge.
     *
     * @return BadgeInterface The badge.
     */
    public function getModelBadge(): BadgeInterface
    {
        if ($this->badge === null) {
            throw new Exception(BadgeInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->badge;
    }
}
