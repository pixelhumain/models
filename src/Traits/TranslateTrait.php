<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\TranslateInterface;

trait TranslateTrait
{
    protected ?TranslateInterface $translate = null;

    /**
     * Set the translate.
     *
     * @param TranslateInterface $translate The translate object to set.
     * @return void
     */
    public function setModelTranslate(TranslateInterface $translate): void
    {
        $this->translate = $translate;
    }

    /**
     * Gets the translate.
     *
     * @return TranslateInterface The translate.
     */
    public function getModelTranslate(): TranslateInterface
    {
        if ($this->translate === null) {
            throw new Exception(TranslateInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translate;
    }
}
