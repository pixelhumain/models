<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;

use PixelHumain\Models\Services\Interfaces\FsServiceInterface;

trait FsTrait
{
    protected FsServiceInterface $fs;

    protected function validateFsProperty(): void
    {
        if (! $this->fs instanceof FsServiceInterface) {
            throw new InvalidArgumentException("fs must be an instance of FsServiceInterface");
        }
    }
}
