<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;

use PixelHumain\Models\Services\Interfaces\PdfServiceInterface;

trait PdfTrait
{
    protected PdfServiceInterface $pdf;

    /**
     * Validate the PDF property.
     *
     * @return void
     */
    protected function validatePdfProperty(): void
    {
        if (! $this->pdf instanceof PdfServiceInterface) {
            throw new InvalidArgumentException();
        }
    }
}
