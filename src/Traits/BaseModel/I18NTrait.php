<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;

use PixelHumain\Models\Services\Interfaces\LanguageServiceInterface;

trait I18NTrait
{
    protected LanguageServiceInterface $language;

    /**
     * Validates the I18n property.
     *
     * @return void
     */
    protected function validateI18nProperty(): void
    {
        if (! $this->language instanceof LanguageServiceInterface) {
            throw new InvalidArgumentException("i18n must be an instance of I18NInterface");
        }
        if (! is_string($this->language->get())) {
            throw new InvalidArgumentException("language must be a string");
        }
    }
}
