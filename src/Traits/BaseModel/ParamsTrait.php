<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;
use PixelHumain\Models\Services\Interfaces\ParamsServiceInterface;

trait ParamsTrait
{
    protected ParamsServiceInterface $params;

    /**
     * Validates the params property.
     *
     * @return void
     */
    protected function validateParamsProperty(): void
    {
        if (! $this->params instanceof ParamsServiceInterface) {
            throw new InvalidArgumentException("params must be an instance of ParamsInterface");
        }
    }
}
