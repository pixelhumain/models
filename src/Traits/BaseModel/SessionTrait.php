<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;

use PixelHumain\Models\Services\Interfaces\SessionServiceInterface;

trait SessionTrait
{
    public SessionServiceInterface $session;

    /**
     * Validates the session property.
     *
     * @return void
     */
    protected function validateSessionProperty(): void
    {
        if (! $this->session instanceof SessionServiceInterface) {
            throw new InvalidArgumentException("session must be an instance of SessionInterface");
        }
    }
}
