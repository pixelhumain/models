<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;
use PixelHumain\Models\Adapter\Interfaces\UrlHelperInterface;

trait UrlHelperTrait
{
    protected UrlHelperInterface $urlHelper;

    public string $baseUrl = '';

    /**
     * Validates the urlHelper property.
     *
     * @return void
     */
    protected function validateUrlHelperProperty(): void
    {
        if (! $this->urlHelper instanceof UrlHelperInterface) {
            throw new InvalidArgumentException("urlHelper must be an instance of " . UrlHelperInterface::class . " (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }

        if ($this->params instanceof ParamsServiceInterface) {
            if ($this->params->get('baseUrl')) {
                $this->baseUrl = $this->params->get('baseUrl');
                $this->urlHelper->setBaseUrl($this->baseUrl);
            }
        }
        if ($this->baseUrl == '') {
            $this->baseUrl = $this->urlHelper->base(true);
        }
    }
}
