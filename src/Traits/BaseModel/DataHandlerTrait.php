<?php

namespace PixelHumain\Models\Traits\BaseModel;

use DateTime;
use DateTimeZone;

/**
 * Trait: DataHandlerTrait
 *
 * This trait provides methods for handling data in models. It includes methods for retrieving data from the database, enriching data with additional information, formatting data, and more.
 *
 * Methods:
 * - getCollectionName(): Returns the name of the collection associated with the model.
 * - getDataById(): Retrieves data by ID from the database.
 * - getDataByArrayId(): Retrieves multiple records based on an array of IDs.
 * - enrichData(): Enriches data with additional information.
 * - getSimpleDataById(): Retrieves and formats simplified data for a specific ID.
 * - addLinkCounts(): Adds link counts to a dataset based on specified link types.
 * - formatSimpleData(): Formats simplified data by adding images and counting links.
 * - getPublicData(): Retrieves public data for a specific model.
 * - processData(): Processes data by merging it with images.
 * - formatMongoDate(): Formats a MongoDB date into a readable string.
 *
 * Abstract Methods:
 * - getByArrayId(): Retrieves data by an array of IDs. Must be implemented by classes using this trait.
 * - getById(): Retrieves data by ID. Must be implemented by classes using this trait.
 * - formatEnrichData(): Formats enriched data. Must be implemented by classes using this trait.
 * - getDataFields(): Retrieves data fields. Must be implemented by classes using this trait.
 * - getDataSimple(): Retrieves simplified data. Must be implemented by classes using this trait.
 * - getlinkTypes(): Retrieves link types. Must be implemented by classes using this trait.
 * - mergeWithImages(): Merges data with images. Must be implemented by classes using this trait.
 */

trait DataHandlerTrait
{
    /**
     * Returns the name of the collection for this model.
     *
     * @return string The name of the collection.
     */
    protected function getCollectionName(): string
    {
        return self::COLLECTION;
    }

    /**
     * Retrieves data by ID.
     *
     * @param string $id The ID of the data to retrieve.
     * @return array|null The retrieved data as an array, or null if not found.
     */
    protected function getDataById(string $id): ?array
    {
        $data = $this->db->findOneById($this->getCollectionName(), $id);

        $data = $data ? $this->processData($id, $data) : null;

        return $data;
    }

    /**
     * Retrieves records from the database based on an array of IDs.
     *
     * @param array $arrayId The array of IDs to search for.
     * @param array $fields The fields to retrieve from the records. Default is an empty array (all fields).
     * @param bool $simply Whether to return a simplified version of the records. Default is false.
     * @return array The retrieved records.
     */
    protected function getDataByArrayId(array $arrayId, array $fields = [], bool $simply = false): array
    {
        $datas = $this->db->find($this->getCollectionName(), [
            "_id" => [
                '$in' => $arrayId,
            ],
        ], $fields);
        $res = [];

        foreach ($datas as $id => $data) {
            if (empty($data)) {
                continue;
            }

            $data = $simply ? $this->formatSimpleData($id, $data, $this->getDataSimple($id, $data), $this->getlinkTypes() ?? []) : $this->enrichData($id, $data);
            $res[$id] = $data;
        }

        return $res;
    }

    /**
     * Enriches the data with additional information.
     *
     * @param string $id The ID of the data.
     * @param array $data The data to be enriched.
     * @return array The enriched data.
     */
    protected function enrichData(string $id, array $data): array
    {
        $data = $this->formatEnrichData($data);
        $data = $this->mergeWithImages($id, $data);
        if (empty($data["typeSig"])) {
            $data["typeSig"] = $this->getCollectionName();
        }
        return $data;
    }

    /**
     * Retrieve simple data by ID.
     *
     * @param string $id The ID of the item.
     * @param array|null $data Optional data to be passed.
     * @return array The simple data.
     */
    protected function getSimpleDataById(string $id, ?array $data = null): array
    {
        if (empty($data)) {
            $data = $this->db->findOneById($this->getCollectionName(), $id, $this->getDataFields());
        }

        if (empty($data)) {
            return [];
        }

        return $this->formatSimpleData($id, $data, $this->getDataSimple($id, $data), $this->getlinkTypes() ?? []);
    }

    /**
     * Adds link counts to the given data.
     *
     * @param array $data The data to which link counts will be added.
     * @param array $links The links data.
     * @param array $linkTypes The types of links to count.
     * @return array The updated data with link counts.
     */
    private function addLinkCounts(array $data, array $links, array $linkTypes): array
    {
        if (empty($links)) {
            return $data;
        }

        foreach ($linkTypes as $key) {
            $data["counts"][$key] = isset($links[$key]) && is_countable($links[$key]) ? count($links[$key]) : 0;
        }
        return $data;
    }

    /**
     * Format the simple data.
     *
     * @param string $id The data ID.
     * @param array $data The data.
     * @param array $simple The simple data.
     * @param array $linkTypes The types of links to count.
     * @return array The formatted data.
     */
    protected function formatSimpleData(string $id, array $data, array $simple = [], array $linkTypes = []): array
    {
        $simple = $this->mergeWithImages($id, $simple);

        $simple = $this->addLinkCounts($simple, $data["links"] ?? [], $linkTypes);

        return $simple;
    }

    /**
     * Récupère les données publiques d'un modèle.
     *
     * @param string $id L'identifiant du modèle.
     * @param array $publicData Les données publiques à ajouter (facultatif).
     * @return array Les données publiques du modèle.
     */
    public function getPublicData(string $id, array $publicData = []): array
    {
        //TODO SBAR = filter data to retrieve only publi data
        $data = $this->getById($id);
        $data["type"] = $this->getCollectionName();
        return $data;
    }

    /**
     * Process the data for a specific ID.
     *
     * @param string $id The ID of the data.
     * @param array $data The data to be processed.
     * @return array The processed data.
     */
    protected function processData(string $id, array $data): array
    {
        return $this->enrichData($id, $data);
    }

    /**
     * Formats a MongoDB date.
     *
     * @param mixed $mongoDate The MongoDB date to format.
     * @param int|null $defaultTimestamp The default timestamp to use if the MongoDB date is null.
     * @return string|null The formatted date.
     */
    protected function formatMongoDate($mongoDate, ?int $defaultTimestamp = null, string $format = DateTime::ISO8601): ?string
    {
        if (is_object($mongoDate) && isset($mongoDate->sec) && is_int($mongoDate->sec)) {
            $dt = new DateTime("@{$mongoDate->sec}");
            $dt->setTimezone(new DateTimeZone('UTC'));
            return $dt->format($format);
        } elseif (is_numeric($mongoDate)) {
            return date($format, $mongoDate);
        } elseif (is_string($mongoDate)) {
            return $mongoDate;
        }

        return $defaultTimestamp !== null ? date($format, $defaultTimestamp) : null;
    }

    // /**
    //  * Retrieves records from the database based on an array of IDs.
    //  *
    //  * @param array $arrayId The array of IDs to search for.
    //  * @param array $fields The fields to retrieve from the records. Default is an empty array (retrieve all fields).
    //  * @param bool $simply Whether to simplify the result or not. Default is false (return the full records).
    //  * @return array The retrieved records.
    //  */
    // abstract public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array;

    // /**
    //  * Retrieves a record from the database by its ID.
    //  *
    //  * @param string $id The ID of the record to retrieve.
    //  * @return array The retrieved record as an associative array.
    //  */
    // abstract public function getById(string $id): array;

    /**
     * Format the data for rendering.
     *
     * @param array $data The data to be formatted.
     * @return array The formatted data.
     */
    abstract protected function formatEnrichData(array $data): array;

    /**
     * Get the fields for the data.
     *
     * @return array The fields of the data.
     */
    abstract protected function getDataFields(): array;

    /**
     * Retrieves simple data for a specific ID and project.
     *
     * @param string $id The ID of the data.
     * @param array $data The project array.
     * @return array The simple data.
     */
    abstract protected function getDataSimple(string $id, array $data): array;

    /**
     * Returns an array of link types.
     *
     * @return array The array of link types.
     */
    abstract protected function getlinkTypes(): array;

    /**
     * Merge the given data with images.
     *
     * @param string $id The identifier of the data item.
     * @param array $data The data to be merged with images.
     * @param array|null $dataMerge The data to be merged with images (optional).
     * @return array The data merged with images.
     */
    abstract protected function mergeWithImages(string $id, array $data, ?array $dataMerge = null): array;
}
