<?php

namespace PixelHumain\Models\Traits\BaseModel;

use PixelHumain\Models\Services\Interfaces\RocketChatServiceInterface;
use PixelHumain\Models\Services\RocketChatService;

trait RocketChatTrait
{
    public function getRocketChat(): RocketChatServiceInterface
    {
        return new RocketChatService([
            'rocketchatURL' => $this->params->get('rocketchatURL'),
            'rocketchatAPiURI' => $this->params->get('rocketchatAPiURI'),
            'loginToken' => $this->session->get('loginToken') ?? null,
            'rocketUserId' => $this->session->get('rocketUserId') ?? null,
            'adminLoginToken' => $this->params->get('adminLoginToken'),
            'adminRocketUserId' => $this->params->get('adminRocketUserId'),
            'admin' => null,
        ]);
        ;
    }
}
