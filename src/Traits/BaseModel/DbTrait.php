<?php

namespace PixelHumain\Models\Traits\BaseModel;

use InvalidArgumentException;

use PixelHumain\Models\Services\Interfaces\DbServiceInterface;

trait DbTrait
{
    protected DbServiceInterface $db;

    protected function validateDbProperty(): void
    {
        if (! $this->db instanceof DbServiceInterface) {
            throw new InvalidArgumentException("db must be an instance of DbServiceInterface");
        }
    }
}
