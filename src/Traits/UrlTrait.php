<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\UrlInterface;

trait UrlTrait
{
    protected ?UrlInterface $url = null;

    /**
     * Set the url.
     *
     * @param UrlInterface $url The url object to set.
     * @return void
     */
    public function setModelUrl(UrlInterface $url): void
    {
        $this->url = $url;
    }

    /**
     * Gets the url.
     *
     * @return UrlInterface The url.
     */
    public function getModelUrl(): UrlInterface
    {
        if ($this->url === null) {
            throw new Exception(UrlInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->url;
    }
}
