<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\PreferenceInterface;

trait PreferenceTrait
{
    protected ?PreferenceInterface $preference = null;

    /**
     * Set the preference.
     *
     * @param PreferenceInterface $preference The preference object to set.
     * @return void
     */
    public function setModelPreference(PreferenceInterface $preference): void
    {
        $this->preference = $preference;
    }

    /**
     * Gets the preference.
     *
     * @return PreferenceInterface The preference.
     */
    public function getModelPreference(): PreferenceInterface
    {
        if ($this->preference === null) {
            throw new Exception(PreferenceInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->preference;
    }
}
