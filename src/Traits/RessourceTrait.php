<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\RessourceInterface;

trait RessourceTrait
{
    protected ?RessourceInterface $ressource = null;

    /**
     * Set the ressource.
     *
     * @param RessourceInterface $ressource The ressource object to set.
     * @return void
     */
    public function setModelRessource(RessourceInterface $ressource): void
    {
        $this->ressource = $ressource;
    }

    /**
     * Gets the ressource.
     *
     * @return RessourceInterface The ressource.
     */
    public function getModelRessource(): RessourceInterface
    {
        if ($this->ressource === null) {
            throw new Exception(RessourceInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->ressource;
    }
}
