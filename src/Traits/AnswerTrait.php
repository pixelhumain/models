<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\AnswerInterface;

trait AnswerTrait
{
    protected ?AnswerInterface $answer = null;

    /**
     * Set the answer.
     *
     * @param AnswerInterface $answer The answer object to set.
     * @return void
     */
    public function setModelAnswer(AnswerInterface $answer): void
    {
        $this->answer = $answer;
    }

    /**
     * Gets the answer.
     *
     * @return AnswerInterface The answer.
     */
    public function getModelAnswer(): AnswerInterface
    {
        if ($this->answer === null) {
            throw new Exception(AnswerInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->answer;
    }
}
