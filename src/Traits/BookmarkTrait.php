<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\BookmarkInterface;

trait BookmarkTrait
{
    protected ?BookmarkInterface $bookmark = null;

    /**
     * Set the bookmark.
     *
     * @param BookmarkInterface $bookmark The bookmark object to set.
     * @return void
     */
    public function setModelBookmark(BookmarkInterface $bookmark): void
    {
        $this->bookmark = $bookmark;
    }

    /**
     * Gets the bookmark.
     *
     * @return BookmarkInterface The bookmark.
     */
    public function getModelBookmark(): BookmarkInterface
    {
        if ($this->bookmark === null) {
            throw new Exception(BookmarkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->bookmark;
    }
}
