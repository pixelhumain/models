<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\RoomInterface;

trait RoomTrait
{
    protected ?RoomInterface $room = null;

    /**
     * Set the room.
     *
     * @param RoomInterface $room The room object to set.
     * @return void
     */
    public function setModelRoom(RoomInterface $room): void
    {
        $this->room = $room;
    }

    /**
     * Gets the room.
     *
     * @return RoomInterface The room.
     */
    public function getModelRoom(): RoomInterface
    {
        if ($this->room === null) {
            throw new Exception(RoomInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->room;
    }
}
