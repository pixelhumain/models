<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CssInterface;

trait CssTrait
{
    protected ?CssInterface $css = null;

    /**
     * Set the css.
     *
     * @param CssInterface $css The css object to set.
     * @return void
     */
    public function setModelCss(CssInterface $css): void
    {
        $this->css = $css;
    }

    /**
     * Gets the css.
     *
     * @return CssInterface The css.
     */
    public function getModelCss(): CssInterface
    {
        if ($this->css === null) {
            throw new Exception(CssInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->css;
    }
}
