<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActionsInterface;

trait ActionsTrait
{
    protected ?ActionsInterface $actions = null;

    /**
     * Set the actions.
     *
     * @param ActionsInterface $actions The actions object to set.
     * @return void
     */
    public function setModelActions(ActionsInterface $actions): void
    {
        $this->actions = $actions;
    }

    /**
     * Gets the actions.
     *
     * @return ActionsInterface The actions.
     */
    public function getModelActions(): ActionsInterface
    {
        if ($this->actions === null) {
            throw new Exception(ActionsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->actions;
    }
}
