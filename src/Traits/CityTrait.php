<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CityInterface;

trait CityTrait
{
    protected ?CityInterface $city = null;

    /**
     * Set the city.
     *
     * @param CityInterface $city The city object to set.
     * @return void
     */
    public function setModelCity(CityInterface $city): void
    {
        $this->city = $city;
    }

    /**
     * Gets the city.
     *
     * @return CityInterface The city.
     */
    public function getModelCity(): CityInterface
    {
        if ($this->city === null) {
            throw new Exception(CityInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->city;
    }
}
