<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SIGInterface;

trait SIGTrait
{
    protected ?SIGInterface $sig = null;

    /**
     * Set the sig.
     *
     * @param SIGInterface $sig The sig object to set.
     * @return void
     */
    public function setModelSIG(SIGInterface $sig): void
    {
        $this->sig = $sig;
    }

    /**
     * Gets the sig.
     *
     * @return SIGInterface The sig.
     */
    public function getModelSIG(): SIGInterface
    {
        if ($this->sig === null) {
            throw new Exception(SIGInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->sig;
    }
}
