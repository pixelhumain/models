<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CollectionInterface;

trait CollectionTrait
{
    protected ?CollectionInterface $collection = null;

    /**
     * Set the collection.
     *
     * @param CollectionInterface $collection The collection object to set.
     * @return void
     */
    public function setModelCollection(CollectionInterface $collection): void
    {
        $this->collection = $collection;
    }

    /**
     * Gets the collection.
     *
     * @return CollectionInterface The collection.
     */
    public function getModelCollection(): CollectionInterface
    {
        if ($this->collection === null) {
            throw new Exception(CollectionInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->collection;
    }
}
