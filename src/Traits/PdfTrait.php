<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\PdfInterface;

trait PdfTrait
{
    protected ?PdfInterface $pdf = null;

    /**
     * Set the pdf.
     *
     * @param PdfInterface $pdf The pdf object to set.
     * @return void
     */
    public function setModelPdf(PdfInterface $pdf): void
    {
        $this->pdf = $pdf;
    }

    /**
     * Gets the pdf.
     *
     * @return PdfInterface The pdf.
     */
    public function getModelPdf(): PdfInterface
    {
        if ($this->pdf === null) {
            throw new Exception(PdfInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->pdf;
    }
}
