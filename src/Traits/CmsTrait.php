<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CmsInterface;

trait CmsTrait
{
    protected ?CmsInterface $cms = null;

    /**
     * Set the cms.
     *
     * @param CmsInterface $cms The cms object to set.
     * @return void
     */
    public function setModelCms(CmsInterface $cms): void
    {
        $this->cms = $cms;
    }

    /**
     * Gets the cms.
     *
     * @return CmsInterface The cms.
     */
    public function getModelCms(): CmsInterface
    {
        if ($this->cms === null) {
            throw new Exception(CmsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->cms;
    }
}
