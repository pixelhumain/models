<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ApiInterface;

trait ApiTrait
{
    protected ?ApiInterface $api = null;

    /**
     * Set the api.
     *
     * @param ApiInterface $api The api object to set.
     * @return void
     */
    public function setModelApi(ApiInterface $api): void
    {
        $this->api = $api;
    }

    /**
     * Gets the api.
     *
     * @return ApiInterface The api.
     */
    public function getModelApi(): ApiInterface
    {
        if ($this->api === null) {
            throw new Exception(ApiInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->api;
    }
}
