<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\AdminInterface;

trait AdminTrait
{
    protected ?AdminInterface $admin = null;

    /**
     * Set the admin.
     *
     * @param AdminInterface $admin The admin object to set.
     * @return void
     */
    public function setModelAdmin(AdminInterface $admin): void
    {
        $this->admin = $admin;
    }

    /**
     * Gets the admin.
     *
     * @return AdminInterface The admin.
     */
    public function getModelAdmin(): AdminInterface
    {
        if ($this->admin === null) {
            throw new Exception(AdminInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->admin;
    }
}
