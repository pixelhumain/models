<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ProjectInterface;

trait ProjectTrait
{
    protected ?ProjectInterface $project = null;

    /**
     * Set the project.
     *
     * @param ProjectInterface $project The project object to set.
     * @return void
     */
    public function setModelProject(ProjectInterface $project): void
    {
        $this->project = $project;
    }

    /**
     * Gets the project.
     *
     * @return ProjectInterface The project.
     */
    public function getModelProject(): ProjectInterface
    {
        if ($this->project === null) {
            throw new Exception(ProjectInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->project;
    }
}
