<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\MenuInterface;

trait MenuTrait
{
    protected ?MenuInterface $menu = null;

    /**
     * Set the menu.
     *
     * @param MenuInterface $menu The menu object to set.
     * @return void
     */
    public function setModelMenu(MenuInterface $menu): void
    {
        $this->menu = $menu;
    }

    /**
     * Gets the menu.
     *
     * @return MenuInterface The menu.
     */
    public function getModelMenu(): MenuInterface
    {
        if ($this->menu === null) {
            throw new Exception(MenuInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->menu;
    }
}
