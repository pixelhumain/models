<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ProductInterface;

trait ProductTrait
{
    protected ?ProductInterface $product = null;

    /**
     * Set the product.
     *
     * @param ProductInterface $product The product object to set.
     * @return void
     */
    public function setModelProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }

    /**
     * Gets the product.
     *
     * @return ProductInterface The product.
     */
    public function getModelProduct(): ProductInterface
    {
        if ($this->product === null) {
            throw new Exception(ProductInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->product;
    }
}
