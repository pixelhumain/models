<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActionRoomInterface;

trait ActionRoomTrait
{
    protected ?ActionRoomInterface $actionRoom = null;

    /**
     * Set the actionRoom.
     *
     * @param ActionRoomInterface $actionRoom The actionRoom object to set.
     * @return void
     */
    public function setModelActionRoom(ActionRoomInterface $actionRoom): void
    {
        $this->actionRoom = $actionRoom;
    }

    /**
     * Gets the actionRoom.
     *
     * @return ActionRoomInterface The actionRoom.
     */
    public function getModelActionRoom(): ActionRoomInterface
    {
        if ($this->actionRoom === null) {
            throw new Exception(ActionRoomInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->actionRoom;
    }
}
