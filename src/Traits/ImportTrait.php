<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ImportInterface;

trait ImportTrait
{
    protected ?ImportInterface $import = null;

    /**
     * Set the import.
     *
     * @param ImportInterface $import The import object to set.
     * @return void
     */
    public function setModelImport(ImportInterface $import): void
    {
        $this->import = $import;
    }

    /**
     * Gets the import.
     *
     * @return ImportInterface The import.
     */
    public function getModelImport(): ImportInterface
    {
        if ($this->import === null) {
            throw new Exception(ImportInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->import;
    }
}
