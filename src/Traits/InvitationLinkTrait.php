<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\InvitationLinkInterface;

trait InvitationLinkTrait
{
    protected ?InvitationLinkInterface $invitationLink = null;

    /**
     * Set the invitationLink.
     *
     * @param InvitationLinkInterface $invitationLink The invitationLink object to set.
     * @return void
     */
    public function setModelInvitationLink(InvitationLinkInterface $invitationLink): void
    {
        $this->invitationLink = $invitationLink;
    }

    /**
     * Gets the invitationLink.
     *
     * @return InvitationLinkInterface The invitationLink.
     */
    public function getModelInvitationLink(): InvitationLinkInterface
    {
        if ($this->invitationLink === null) {
            throw new Exception(InvitationLinkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->invitationLink;
    }
}
