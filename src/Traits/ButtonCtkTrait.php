<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ButtonCtkInterface;

trait ButtonCtkTrait
{
    protected ?ButtonCtkInterface $buttonCtk = null;

    /**
     * Set the buttonCtk.
     *
     * @param ButtonCtkInterface $buttonCtk The buttonCtk object to set.
     * @return void
     */
    public function setModelButtonCtk(ButtonCtkInterface $buttonCtk): void
    {
        $this->buttonCtk = $buttonCtk;
    }

    /**
     * Gets the buttonCtk.
     *
     * @return ButtonCtkInterface The buttonCtk.
     */
    public function getModelButtonCtk(): ButtonCtkInterface
    {
        if ($this->buttonCtk === null) {
            throw new Exception(ButtonCtkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->buttonCtk;
    }
}
