<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ActionInterface;

trait ActionTrait
{
    protected ?ActionInterface $action = null;

    /**
     * Set the action.
     *
     * @param ActionInterface $action The action object to set.
     * @return void
     */
    public function setModelAction(ActionInterface $action): void
    {
        $this->action = $action;
    }

    /**
     * Gets the action.
     *
     * @return ActionInterface The action.
     */
    public function getModelAction(): ActionInterface
    {
        if ($this->action === null) {
            throw new Exception(ActionInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->action;
    }
}
