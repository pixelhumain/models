<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\PreferenceInterface;

interface PreferenceTraitInterface
{
    /**
     * Set the preference.
     *
     * @param PreferenceInterface $preference The preference object to set.
     * @return void
     */
    public function setModelPreference(PreferenceInterface $preference): void;

    /**
     * Gets the preference.
     *
     * @return PreferenceInterface The preference.
     */
    public function getModelPreference(): PreferenceInterface;
}
