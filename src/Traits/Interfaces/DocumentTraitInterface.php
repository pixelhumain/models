<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\DocumentInterface;

interface DocumentTraitInterface
{
    /**
     * Set the document.
     *
     * @param DocumentInterface $document The document object to set.
     * @return void
     */
    public function setModelDocument(DocumentInterface $document): void;

    /**
     * Gets the document.
     *
     * @return DocumentInterface The document.
     */
    public function getModelDocument(): DocumentInterface;
}
