<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\LogInterface;

interface LogTraitInterface
{
    /**
     * Set the log.
     *
     * @param LogInterface $log The log object to set.
     * @return void
     */
    public function setModelLog(LogInterface $log): void;

    /**
     * Gets the log.
     *
     * @return LogInterface The log.
     */
    public function getModelLog(): LogInterface;
}
