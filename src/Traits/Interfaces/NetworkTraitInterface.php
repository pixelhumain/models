<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NetworkInterface;

interface NetworkTraitInterface
{
    /**
     * Set the network.
     *
     * @param NetworkInterface $network The network object to set.
     * @return void
     */
    public function setModelNetwork(NetworkInterface $network): void;

    /**
     * Gets the network.
     *
     * @return NetworkInterface The network.
     */
    public function getModelNetwork(): NetworkInterface;
}
