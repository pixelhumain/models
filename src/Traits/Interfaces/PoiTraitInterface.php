<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\PoiInterface;

interface PoiTraitInterface
{
    /**
     * Set the poi.
     *
     * @param PoiInterface $poi The poi object to set.
     * @return void
     */
    public function setModelPoi(PoiInterface $poi): void;

    /**
     * Gets the poi.
     *
     * @return PoiInterface The poi.
     */
    public function getModelPoi(): PoiInterface;
}
