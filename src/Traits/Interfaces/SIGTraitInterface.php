<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SIGInterface;

interface SIGTraitInterface
{
    /**
     * Set the sig.
     *
     * @param SIGInterface $sig The sig object to set.
     * @return void
     */
    public function setModelSIG(SIGInterface $sig): void;

    /**
     * Gets the sig.
     *
     * @return SIGInterface The sig.
     */
    public function getModelSIG(): SIGInterface;
}
