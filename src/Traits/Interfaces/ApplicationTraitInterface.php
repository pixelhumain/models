<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ApplicationInterface;

interface ApplicationTraitInterface
{
    /**
     * Set the application.
     *
     * @param ApplicationInterface $application The application object to set.
     * @return void
     */
    public function setModelApplication(ApplicationInterface $application): void;

    /**
     * Gets the application.
     *
     * @return ApplicationInterface The application.
     */
    public function getModelApplication(): ApplicationInterface;
}
