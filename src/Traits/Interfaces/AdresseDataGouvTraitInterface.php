<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\AdresseDataGouvInterface;

interface AdresseDataGouvTraitInterface
{
    /**
     * Set the adresseDataGouv.
     *
     * @param AdresseDataGouvInterface $adresseDataGouv The adresseDataGouv object to set.
     * @return void
     */
    public function setModelAdresseDataGouv(AdresseDataGouvInterface $adresseDataGouv): void;

    /**
     * Gets the adresseDataGouv.
     *
     * @return AdresseDataGouvInterface The adresseDataGouv.
     */
    public function getModelAdresseDataGouv(): AdresseDataGouvInterface;
}
