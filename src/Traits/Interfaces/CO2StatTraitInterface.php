<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CO2StatInterface;

interface CO2StatTraitInterface
{
    /**
     * Set the cO2Stat.
     *
     * @param CO2StatInterface $cO2Stat The cO2Stat object to set.
     * @return void
     */
    public function setModelCO2Stat(CO2StatInterface $cO2Stat): void;

    /**
     * Gets the cO2Stat.
     *
     * @return CO2StatInterface The cO2Stat.
     */
    public function getModelCO2Stat(): CO2StatInterface;
}
