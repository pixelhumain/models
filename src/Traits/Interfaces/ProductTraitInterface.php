<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ProductInterface;

interface ProductTraitInterface
{
    /**
     * Set the product.
     *
     * @param ProductInterface $product The product object to set.
     * @return void
     */
    public function setModelProduct(ProductInterface $product): void;

    /**
     * Gets the product.
     *
     * @return ProductInterface The product.
     */
    public function getModelProduct(): ProductInterface;
}
