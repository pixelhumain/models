<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CsvInterface;

interface CsvTraitInterface
{
    /**
     * Set the csv.
     *
     * @param CsvInterface $csv The csv object to set.
     * @return void
     */
    public function setModelCsv(CsvInterface $csv): void;

    /**
     * Gets the csv.
     *
     * @return CsvInterface The csv.
     */
    public function getModelCsv(): CsvInterface;
}
