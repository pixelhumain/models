<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\PersonInterface;

interface PersonTraitInterface
{
    /**
     * Set the person.
     *
     * @param PersonInterface $person The person object to set.
     * @return void
     */
    public function setModelPerson(PersonInterface $person): void;

    /**
     * Gets the person.
     *
     * @return PersonInterface The person.
     */
    public function getModelPerson(): PersonInterface;
}
