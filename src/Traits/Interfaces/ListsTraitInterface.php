<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ListsInterface;

interface ListsTraitInterface
{
    /**
     * Set the lists.
     *
     * @param ListsInterface $lists The lists object to set.
     * @return void
     */
    public function setModelLists(ListsInterface $lists): void;

    /**
     * Gets the lists.
     *
     * @return ListsInterface The lists.
     */
    public function getModelLists(): ListsInterface;
}
