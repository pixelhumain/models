<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SurveyInterface;

interface SurveyTraitInterface
{
    /**
     * Set the survey.
     *
     * @param SurveyInterface $survey The survey to set.
     * @return void
     */
    public function setModelSurvey(SurveyInterface $survey): void;

    /**
     * Returns the survey.
     *
     * @return SurveyInterface The survey.
     */
    public function getModelSurvey(): SurveyInterface;
}
