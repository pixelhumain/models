<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CircuitInterface;

interface CircuitTraitInterface
{
    /**
     * Set the circuit.
     *
     * @param CircuitInterface $circuit The circuit object to set.
     * @return void
     */
    public function setModelCircuit(CircuitInterface $circuit): void;

    /**
     * Gets the circuit.
     *
     * @return CircuitInterface The circuit.
     */
    public function getModelCircuit(): CircuitInterface;
}
