<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SmartDataInterface;

interface SmartDataTraitInterface
{
    /**
     * Set the smartData.
     *
     * @param SmartDataInterface $smartData The smartData object to set.
     * @return void
     */
    public function setModelSmartData(SmartDataInterface $smartData): void;

    /**
     * Gets the smartData.
     *
     * @return SmartDataInterface The smartData.
     */
    public function getModelSmartData(): SmartDataInterface;
}
