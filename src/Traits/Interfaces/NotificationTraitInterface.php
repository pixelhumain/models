<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NotificationInterface;

interface NotificationTraitInterface
{
    /**
     * Set the notification.
     *
     * @param NotificationInterface $notification The notification object to set.
     * @return void
     */
    public function setModelNotification(NotificationInterface $notification): void;

    /**
     * Gets the notification.
     *
     * @return NotificationInterface The notification.
     */
    public function getModelNotification(): NotificationInterface;
}
