<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\RoomInterface;

interface RoomTraitInterface
{
    /**
     * Set the room.
     *
     * @param RoomInterface $room The room object to set.
     * @return void
     */
    public function setModelRoom(RoomInterface $room): void;

    /**
     * Gets the room.
     *
     * @return RoomInterface The room.
     */
    public function getModelRoom(): RoomInterface;
}
