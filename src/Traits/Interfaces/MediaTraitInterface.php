<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MediaInterface;

interface MediaTraitInterface
{
    /**
     * Set the media.
     *
     * @param MediaInterface $media The media object to set.
     * @return void
     */
    public function setModelMedia(MediaInterface $media): void;

    /**
     * Gets the media.
     *
     * @return MediaInterface The media.
     */
    public function getModelMedia(): MediaInterface;
}
