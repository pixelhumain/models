<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CmsInterface;

interface CmsTraitInterface
{
    /**
     * Set the cms.
     *
     * @param CmsInterface $cms The cms object to set.
     * @return void
     */
    public function setModelCms(CmsInterface $cms): void;

    /**
     * Gets the cms.
     *
     * @return CmsInterface The cms.
     */
    public function getModelCms(): CmsInterface;
}
