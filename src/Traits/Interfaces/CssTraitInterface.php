<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CssInterface;

interface CssTraitInterface
{
    /**
     * Set the css.
     *
     * @param CssInterface $css The css object to set.
     * @return void
     */
    public function setModelCss(CssInterface $css): void;

    /**
     * Gets the css.
     *
     * @return CssInterface The css.
     */
    public function getModelCss(): CssInterface;
}
