<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ConvertInterface;

interface ConvertTraitInterface
{
    /**
     * Set the convert.
     *
     * @param ConvertInterface $convert The convert object to set.
     * @return void
     */
    public function setModelConvert(ConvertInterface $convert): void;

    /**
     * Gets the convert.
     *
     * @return ConvertInterface The convert.
     */
    public function getModelConvert(): ConvertInterface;
}
