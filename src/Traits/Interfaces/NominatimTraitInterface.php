<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NominatimInterface;

interface NominatimTraitInterface
{
    /**
     * Set the nominatim.
     *
     * @param NominatimInterface $nominatim The nominatim object to set.
     * @return void
     */
    public function setModelNominatim(NominatimInterface $nominatim): void;

    /**
     * Gets the nominatim.
     *
     * @return NominatimInterface The nominatim.
     */
    public function getModelNominatim(): NominatimInterface;
}
