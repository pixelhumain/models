<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\RessourceInterface;

interface RessourceTraitInterface
{
    /**
     * Set the ressource.
     *
     * @param RessourceInterface $ressource The ressource object to set.
     * @return void
     */
    public function setModelRessource(RessourceInterface $ressource): void;

    /**
     * Gets the ressource.
     *
     * @return RessourceInterface The ressource.
     */
    public function getModelRessource(): RessourceInterface;
}
