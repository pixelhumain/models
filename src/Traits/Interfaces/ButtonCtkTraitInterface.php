<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ButtonCtkInterface;

interface ButtonCtkTraitInterface
{
    /**
     * Set the buttonCtk.
     *
     * @param ButtonCtkInterface $buttonCtk The buttonCtk object to set.
     * @return void
     */
    public function setModelButtonCtk(ButtonCtkInterface $buttonCtk): void;

    /**
     * Gets the buttonCtk.
     *
     * @return ButtonCtkInterface The buttonCtk.
     */
    public function getModelButtonCtk(): ButtonCtkInterface;
}
