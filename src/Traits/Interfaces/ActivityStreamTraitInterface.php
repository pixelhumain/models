<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActivityStreamInterface;

interface ActivityStreamTraitInterface
{
    /**
     * Set the activityStream.
     *
     * @param ActivityStreamInterface $activityStream The activityStream object to set.
     * @return void
     */
    public function setModelActivityStream(ActivityStreamInterface $activityStream): void;

    /**
     * Gets the activityStream.
     *
     * @return ActivityStreamInterface The activityStream.
     */
    public function getModelActivityStream(): ActivityStreamInterface;
}
