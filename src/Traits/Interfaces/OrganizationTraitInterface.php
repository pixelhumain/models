<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\OrganizationInterface;

interface OrganizationTraitInterface
{
    /**
     * Set the organization.
     *
     * @param OrganizationInterface $organization The organization object to set.
     * @return void
     */
    public function setModelOrganization(OrganizationInterface $organization): void;

    /**
     * Gets the organization.
     *
     * @return OrganizationInterface The organization.
     */
    public function getModelOrganization(): OrganizationInterface;
}
