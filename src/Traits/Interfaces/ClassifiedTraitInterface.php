<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ClassifiedInterface;

interface ClassifiedTraitInterface
{
    /**
     * Set the classified.
     *
     * @param ClassifiedInterface $classified The classified object to set.
     * @return void
     */
    public function setModelClassified(ClassifiedInterface $classified): void;

    /**
     * Gets the classified.
     *
     * @return ClassifiedInterface The classified.
     */
    public function getModelClassified(): ClassifiedInterface;
}
