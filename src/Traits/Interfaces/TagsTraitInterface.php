<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\TagsInterface;

interface TagsTraitInterface
{
    /**
     * Set the tags.
     *
     * @param TagsInterface $tags The tags to set.
     * @return void
     */
    public function setModelTags(TagsInterface $tags): void;

    /**
     * Returns the tags.
     *
     * @return TagsInterface The tags.
     */
    public function getModelTags(): TagsInterface;
}
