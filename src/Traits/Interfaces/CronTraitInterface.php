<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CronInterface;

interface CronTraitInterface
{
    /**
     * Set the cron.
     *
     * @param CronInterface $cron The cron object to set.
     * @return void
     */
    public function setModelCron(CronInterface $cron): void;

    /**
     * Gets the cron.
     *
     * @return CronInterface The cron.
     */
    public function getModelCron(): CronInterface;
}
