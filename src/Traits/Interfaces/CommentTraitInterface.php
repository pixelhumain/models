<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CommentInterface;

interface CommentTraitInterface
{
    /**
     * Set the comment.
     *
     * @param CommentInterface $comment The comment object to set.
     * @return void
     */
    public function setModelComment(CommentInterface $comment): void;

    /**
     * Gets the comment.
     *
     * @return CommentInterface The comment.
     */
    public function getModelComment(): CommentInterface;
}
