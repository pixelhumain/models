<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\AnswerInterface;

interface AnswerTraitInterface
{
    /**
     * Set the answer.
     *
     * @param AnswerInterface $answer The answer object to set.
     * @return void
     */
    public function setModelAnswer(AnswerInterface $answer): void;

    /**
     * Gets the answer.
     *
     * @return AnswerInterface The answer.
     */
    public function getModelAnswer(): AnswerInterface;
}
