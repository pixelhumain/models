<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\FolderInterface;

interface FolderTraitInterface
{
    /**
     * Set the folder.
     *
     * @param FolderInterface $folder The folder object to set.
     * @return void
     */
    public function setModelFolder(FolderInterface $folder): void;

    /**
     * Gets the folder.
     *
     * @return FolderInterface The folder.
     */
    public function getModelFolder(): FolderInterface;
}
