<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ProposalInterface;

interface ProposalTraitInterface
{
    /**
     * Set the proposal.
     *
     * @param ProposalInterface $proposal The proposal object to set.
     * @return void
     */
    public function setModelProposal(ProposalInterface $proposal): void;

    /**
     * Gets the proposal.
     *
     * @return ProposalInterface The proposal.
     */
    public function getModelProposal(): ProposalInterface;
}
