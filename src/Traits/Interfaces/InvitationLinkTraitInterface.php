<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\InvitationLinkInterface;

interface InvitationLinkTraitInterface
{
    /**
     * Set the invitationLink.
     *
     * @param InvitationLinkInterface $invitationLink The invitationLink object to set.
     * @return void
     */
    public function setModelInvitationLink(InvitationLinkInterface $invitationLink): void;

    /**
     * Gets the invitationLink.
     *
     * @return InvitationLinkInterface The invitationLink.
     */
    public function getModelInvitationLink(): InvitationLinkInterface;
}
