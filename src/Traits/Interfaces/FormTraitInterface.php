<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\FormInterface;

interface FormTraitInterface
{
    /**
     * Set the form.
     *
     * @param FormInterface $form The form object to set.
     * @return void
     */
    public function setModelForm(FormInterface $form): void;

    /**
     * Gets the form.
     *
     * @return FormInterface The form.
     */
    public function getModelForm(): FormInterface;
}
