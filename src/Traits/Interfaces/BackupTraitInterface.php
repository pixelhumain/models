<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\BackupInterface;

interface BackupTraitInterface
{
    /**
     * Set the backup.
     *
     * @param BackupInterface $backup The backup object to set.
     * @return void
     */
    public function setModelBackup(BackupInterface $backup): void;

    /**
     * Gets the backup.
     *
     * @return BackupInterface The backup.
     */
    public function getModelBackup(): BackupInterface;
}
