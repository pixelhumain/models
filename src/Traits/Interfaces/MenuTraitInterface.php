<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MenuInterface;

interface MenuTraitInterface
{
    /**
     * Set the menu.
     *
     * @param MenuInterface $menu The menu object to set.
     * @return void
     */
    public function setModelMenu(MenuInterface $menu): void;

    /**
     * Gets the menu.
     *
     * @return MenuInterface The menu.
     */
    public function getModelMenu(): MenuInterface;
}
