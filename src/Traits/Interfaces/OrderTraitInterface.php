<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\OrderInterface;

interface OrderTraitInterface
{
    /**
     * Set the order.
     *
     * @param OrderInterface $order The order object to set.
     * @return void
     */
    public function setModelOrder(OrderInterface $order): void;

    /**
     * Gets the order.
     *
     * @return OrderInterface The order.
     */
    public function getModelOrder(): OrderInterface;
}
