<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SearchNewInterface;

interface SearchNewTraitInterface
{
    /**
     * Set the searchNew.
     *
     * @param SearchNewInterface $searchNew The searchNew object to set.
     * @return void
     */
    public function setModelSearchNew(SearchNewInterface $searchNew): void;

    /**
     * Gets the searchNew.
     *
     * @return SearchNewInterface The searchNew.
     */
    public function getModelSearchNew(): SearchNewInterface;
}
