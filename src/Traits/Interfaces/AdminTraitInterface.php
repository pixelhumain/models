<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\AdminInterface;

interface AdminTraitInterface
{
    /**
     * Set the admin.
     *
     * @param AdminInterface $admin The admin object to set.
     * @return void
     */
    public function setModelAdmin(AdminInterface $admin): void;

    /**
     * Gets the admin.
     *
     * @return AdminInterface The admin.
     */
    public function getModelAdmin(): AdminInterface;
}
