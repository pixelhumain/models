<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\PdfInterface;

interface PdfTraitInterface
{
    /**
     * Set the pdf.
     *
     * @param PdfInterface $pdf The pdf object to set.
     * @return void
     */
    public function setModelPdf(PdfInterface $pdf): void;

    /**
     * Gets the pdf.
     *
     * @return PdfInterface The pdf.
     */
    public function getModelPdf(): PdfInterface;
}
