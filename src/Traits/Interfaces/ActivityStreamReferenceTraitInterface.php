<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActivityStreamReferenceInterface;

interface ActivityStreamReferenceTraitInterface
{
    /**
     * Set the activityStreamReference.
     *
     * @param ActivityStreamReferenceInterface $activityStreamReference The activityStreamReference object to set.
     * @return void
     */
    public function setModelActivityStreamReference(ActivityStreamReferenceInterface $activityStreamReference): void;

    /**
     * Gets the activityStreamReference.
     *
     * @return ActivityStreamReferenceInterface The activityStreamReference.
     */
    public function getModelActivityStreamReference(): ActivityStreamReferenceInterface;
}
