<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CityInterface;

interface CityTraitInterface
{
    /**
     * Set the city.
     *
     * @param CityInterface $city The city object to set.
     * @return void
     */
    public function setModelCity(CityInterface $city): void;

    /**
     * Gets the city.
     *
     * @return CityInterface The city.
     */
    public function getModelCity(): CityInterface;
}
