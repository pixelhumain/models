<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ProjectInterface;

interface ProjectTraitInterface
{
    /**
     * Set the project.
     *
     * @param ProjectInterface $project The project object to set.
     * @return void
     */
    public function setModelProject(ProjectInterface $project): void;

    /**
     * Gets the project.
     *
     * @return ProjectInterface The project.
     */
    public function getModelProject(): ProjectInterface;
}
