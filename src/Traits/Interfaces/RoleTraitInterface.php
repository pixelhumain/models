<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\RoleInterface;

interface RoleTraitInterface
{
    /**
    * Set the role of the person.
    *
    * @param RoleInterface $role The role object to set.
    * @return void
    */
    public function setModelRole(RoleInterface $role): void;

    /**
     * Gets the role of the person.
     *
     * @return RoleInterface The role of the person.
     */
    public function getModelRole(): RoleInterface;
}
