<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\DataValidatorInterface;

interface DataValidatorTraitInterface
{
    /**
     * Set the dataValidator.
     *
     * @param DataValidatorInterface $dataValidator The dataValidator object to set.
     * @return void
     */
    public function setModelDataValidator(DataValidatorInterface $dataValidator): void;

    /**
     * Gets the dataValidator.
     *
     * @return DataValidatorInterface The dataValidator.
     */
    public function getModelDataValidator(): DataValidatorInterface;
}
