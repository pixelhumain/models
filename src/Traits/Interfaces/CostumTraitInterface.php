<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CostumInterface;

interface CostumTraitInterface
{
    /**
     * Set the costum.
     *
     * @param CostumInterface $costum The costum object to set.
     * @return void
     */
    public function setModelCostum(CostumInterface $costum): void;

    /**
     * Gets the costum.
     *
     * @return CostumInterface The costum.
     */
    public function getModelCostum(): CostumInterface;
}
