<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ZoneInterface;

interface ZoneTraitInterface
{
    /**
     * Set the zone.
     *
     * @param ZoneInterface $zone The zone object to set.
     * @return void
     */
    public function setModelZone(ZoneInterface $zone): void;

    /**
     * Gets the zone.
     *
     * @return ZoneInterface The zone.
     */
    public function getModelZone(): ZoneInterface;
}
