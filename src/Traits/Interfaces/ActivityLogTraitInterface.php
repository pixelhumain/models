<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActivityLogInterface;

interface ActivityLogTraitInterface
{
    /**
     * Set the activityLog.
     *
     * @param ActivityLogInterface $activityLog The activityLog object to set.
     * @return void
     */
    public function setModelActivityLog(ActivityLogInterface $activityLog): void;

    /**
     * Gets the activityLog.
     *
     * @return ActivityLogInterface The activityLog.
     */
    public function getModelActivityLog(): ActivityLogInterface;
}
