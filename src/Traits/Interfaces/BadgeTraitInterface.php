<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\BadgeInterface;

interface BadgeTraitInterface
{
    /**
     * Set the badge.
     *
     * @param BadgeInterface $badge The badge object to set.
     * @return void
     */
    public function setModelBadge(BadgeInterface $badge): void;

    /**
     * Gets the badge.
     *
     * @return BadgeInterface The badge.
     */
    public function getModelBadge(): BadgeInterface;
}
