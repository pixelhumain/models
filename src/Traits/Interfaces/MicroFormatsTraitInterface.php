<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MicroFormatsInterface;

interface MicroFormatsTraitInterface
{
    /**
     * Set the microFormats.
     *
     * @param MicroFormatsInterface $microFormats The microFormats object to set.
     * @return void
     */
    public function setModelMicroFormats(MicroFormatsInterface $microFormats): void;

    /**
     * Gets the microFormats.
     *
     * @return MicroFormatsInterface The microFormats.
     */
    public function getModelMicroFormats(): MicroFormatsInterface;
}
