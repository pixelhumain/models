<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActStrInterface;

interface ActStrTraitInterface
{
    /**
     * Set the actStr.
     *
     * @param ActStrInterface $actStr The actStr object to set.
     * @return void
     */
    public function setModelActStr(ActStrInterface $actStr): void;

    /**
     * Gets the actStr.
     *
     * @return ActStrInterface The actStr.
     */
    public function getModelActStr(): ActStrInterface;
}
