<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\UrlInterface;

interface UrlTraitInterface
{
    /**
     * Set the url.
     *
     * @param UrlInterface $url The url object to set.
     * @return void
     */
    public function setModelUrl(UrlInterface $url): void;

    /**
     * Gets the url.
     *
     * @return UrlInterface The url.
     */
    public function getModelUrl(): UrlInterface;
}
