<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ExportInterface;

interface ExportTraitInterface
{
    /**
     * Set the export.
     *
     * @param ExportInterface $export The export object to set.
     * @return void
     */
    public function setModelExport(ExportInterface $export): void;

    /**
     * Gets the export.
     *
     * @return ExportInterface The export.
     */
    public function getModelExport(): ExportInterface;
}
