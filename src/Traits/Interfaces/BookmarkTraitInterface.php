<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\BookmarkInterface;

interface BookmarkTraitInterface
{
    /**
     * Set the bookmark.
     *
     * @param BookmarkInterface $bookmark The bookmark object to set.
     * @return void
     */
    public function setModelBookmark(BookmarkInterface $bookmark): void;

    /**
     * Gets the bookmark.
     *
     * @return BookmarkInterface The bookmark.
     */
    public function getModelBookmark(): BookmarkInterface;
}
