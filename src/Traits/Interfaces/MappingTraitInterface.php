<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MappingInterface;

interface MappingTraitInterface
{
    /**
     * Set the mapping.
     *
     * @param MappingInterface $mapping The mapping object to set.
     * @return void
     */
    public function setModelMapping(MappingInterface $mapping): void;

    /**
     * Gets the mapping.
     *
     * @return MappingInterface The mapping.
     */
    public function getModelMapping(): MappingInterface;
}
