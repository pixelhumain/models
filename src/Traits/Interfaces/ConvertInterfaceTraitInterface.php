<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ConvertInterfaceInterface;

interface ConvertInterfaceTraitInterface
{
    /**
     * Set the convertInterface.
     *
     * @param ConvertInterfaceInterface $convertInterface The convertInterface object to set.
     * @return void
     */
    public function setModelConvertInterface(ConvertInterfaceInterface $convertInterface): void;

    /**
     * Gets the convertInterface.
     *
     * @return ConvertInterfaceInterface The convertInterface.
     */
    public function getModelConvertInterface(): ConvertInterfaceInterface;
}
