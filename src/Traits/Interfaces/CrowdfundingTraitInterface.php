<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CrowdfundingInterface;

interface CrowdfundingTraitInterface
{
    /**
     * Set the crowdfunding.
     *
     * @param CrowdfundingInterface $crowdfunding The crowdfunding object to set.
     * @return void
     */
    public function setModelCrowdfunding(CrowdfundingInterface $crowdfunding): void;

    /**
     * Gets the crowdfunding.
     *
     * @return CrowdfundingInterface The crowdfunding.
     */
    public function getModelCrowdfunding(): CrowdfundingInterface;
}
