<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MailInterface;

interface MailTraitInterface
{
    /**
     * Set the mail.
     *
     * @param MailInterface $mail The mail object to set.
     * @return void
     */
    public function setModelMail(MailInterface $mail): void;

    /**
     * Gets the mail.
     *
     * @return MailInterface The mail.
     */
    public function getModelMail(): MailInterface;
}
