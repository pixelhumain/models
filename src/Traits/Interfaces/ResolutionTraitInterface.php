<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ResolutionInterface;

interface ResolutionTraitInterface
{
    /**
     * Set the resolution.
     *
     * @param ResolutionInterface $resolution The resolution object to set.
     * @return void
     */
    public function setModelResolution(ResolutionInterface $resolution): void;

    /**
     * Gets the resolution.
     *
     * @return ResolutionInterface The resolution.
     */
    public function getModelResolution(): ResolutionInterface;
}
