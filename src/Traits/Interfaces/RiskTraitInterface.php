<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\RiskInterface;

interface RiskTraitInterface
{
    /**
     * Set the risk.
     *
     * @param RiskInterface $risk The risk object to set.
     * @return void
     */
    public function setModelRisk(RiskInterface $risk): void;

    /**
     * Gets the risk.
     *
     * @return RiskInterface The risk.
     */
    public function getModelRisk(): RiskInterface;
}
