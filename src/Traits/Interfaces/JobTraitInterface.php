<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\JobInterface;

interface JobTraitInterface
{
    /**
     * Set the job.
     *
     * @param JobInterface $job The job object to set.
     * @return void
     */
    public function setModelJob(JobInterface $job): void;

    /**
     * Gets the job.
     *
     * @return JobInterface The job.
     */
    public function getModelJob(): JobInterface;
}
