<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ImportInterface;

interface ImportTraitInterface
{
    /**
     * Set the import.
     *
     * @param ImportInterface $import The import object to set.
     * @return void
     */
    public function setModelImport(ImportInterface $import): void;

    /**
     * Gets the import.
     *
     * @return ImportInterface The import.
     */
    public function getModelImport(): ImportInterface;
}
