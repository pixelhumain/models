<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\GamificationInterface;

interface GamificationTraitInterface
{
    /**
     * Set the gamification.
     *
     * @param GamificationInterface $gamification The gamification object to set.
     * @return void
     */
    public function setModelGamification(GamificationInterface $gamification): void;

    /**
     * Gets the gamification.
     *
     * @return GamificationInterface The gamification.
     */
    public function getModelGamification(): GamificationInterface;
}
