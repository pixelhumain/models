<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CooperationInterface;

interface CooperationTraitInterface
{
    /**
     * Set the cooperation.
     *
     * @param CooperationInterface $cooperation The cooperation object to set.
     * @return void
     */
    public function setModelCooperation(CooperationInterface $cooperation): void;

    /**
     * Gets the cooperation.
     *
     * @return CooperationInterface The cooperation.
     */
    public function getModelCooperation(): CooperationInterface;
}
