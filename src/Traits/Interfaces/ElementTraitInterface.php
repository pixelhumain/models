<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ElementInterface;

interface ElementTraitInterface
{
    /**
     * Set the element.
     *
     * @param ElementInterface $element The element object to set.
     * @return void
     */
    public function setModelElement(ElementInterface $element): void;

    /**
     * Gets the element.
     *
     * @return ElementInterface The element.
     */
    public function getModelElement(): ElementInterface;
}
