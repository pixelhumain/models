<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ServiceInterface;

interface ServiceTraitInterface
{
    /**
     * Set the service.
     *
     * @param ServiceInterface $service The service object to set.
     * @return void
     */
    public function setModelService(ServiceInterface $service): void;

    /**
     * Gets the service.
     *
     * @return ServiceInterface The service.
     */
    public function getModelService(): ServiceInterface;
}
