<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NewsLetterInterface;

interface NewsLetterTraitInterface
{
    /**
     * Set the newsLetter.
     *
     * @param NewsLetterInterface $newsLetter The newsLetter object to set.
     * @return void
     */
    public function setModelNewsLetter(NewsLetterInterface $newsLetter): void;

    /**
     * Gets the newsLetter.
     *
     * @return NewsLetterInterface The newsLetter.
     */
    public function getModelNewsLetter(): NewsLetterInterface;
}
