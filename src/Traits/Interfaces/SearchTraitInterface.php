<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SearchInterface;

interface SearchTraitInterface
{
    /**
     * Set the search.
     *
     * @param SearchInterface $search The search object to set.
     * @return void
     */
    public function setModelSearch(SearchInterface $search): void;

    /**
     * Gets the search.
     *
     * @return SearchInterface The search.
     */
    public function getModelSearch(): SearchInterface;
}
