<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NewsTranslatorInterface;

interface NewsTranslatorTraitInterface
{
    /**
     * Set the newsTranslator.
     *
     * @param NewsTranslatorInterface $newsTranslator The newsTranslator object to set.
     * @return void
     */
    public function setModelNewsTranslator(NewsTranslatorInterface $newsTranslator): void;

    /**
     * Gets the newsTranslator.
     *
     * @return NewsTranslatorInterface The newsTranslator.
     */
    public function getModelNewsTranslator(): NewsTranslatorInterface;
}
