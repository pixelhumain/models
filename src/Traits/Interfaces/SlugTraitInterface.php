<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\SlugInterface;

interface SlugTraitInterface
{
    /**
     * Set the slug utility of the person.
     *
     * @param SlugInterface $slug The slug object to set.
     * @return void
     */
    public function setModelSlug(SlugInterface $slug): void;

    /**
     * Gets the slug utility of the person.
     *
     * @return SlugInterface The slug of the person.
     */
    public function getModelSlug(): SlugInterface;
}
