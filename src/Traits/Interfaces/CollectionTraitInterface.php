<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CollectionInterface;

interface CollectionTraitInterface
{
    /**
     * Set the collection.
     *
     * @param CollectionInterface $collection The collection object to set.
     * @return void
     */
    public function setModelCollection(CollectionInterface $collection): void;

    /**
     * Gets the collection.
     *
     * @return CollectionInterface The collection.
     */
    public function getModelCollection(): CollectionInterface;
}
