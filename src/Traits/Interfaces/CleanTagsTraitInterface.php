<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\CleanTagsInterface;

interface CleanTagsTraitInterface
{
    /**
     * Set the cleanTags.
     *
     * @param CleanTagsInterface $cleanTags The cleanTags object to set.
     * @return void
     */
    public function setModelCleanTags(CleanTagsInterface $cleanTags): void;

    /**
     * Gets the cleanTags.
     *
     * @return CleanTagsInterface The cleanTags.
     */
    public function getModelCleanTags(): CleanTagsInterface;
}
