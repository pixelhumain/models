<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\NewsInterface;

interface NewsTraitInterface
{
    /**
     * Set the news.
     *
     * @param NewsInterface $news The news object to set.
     * @return void
     */
    public function setModelNews(NewsInterface $news): void;

    /**
     * Gets the news.
     *
     * @return NewsInterface The news.
     */
    public function getModelNews(): NewsInterface;
}
