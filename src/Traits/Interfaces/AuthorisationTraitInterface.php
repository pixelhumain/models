<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\AuthorisationInterface;

interface AuthorisationTraitInterface
{
    /**
     * Set the authorisation.
     *
     * @param AuthorisationInterface $authorisation The authorisation object to set.
     * @return void
     */
    public function setModelAuthorisation(AuthorisationInterface $authorisation): void;

    /**
     * Gets the authorisation.
     *
     * @return AuthorisationInterface The authorisation.
     */
    public function getModelAuthorisation(): AuthorisationInterface;
}
