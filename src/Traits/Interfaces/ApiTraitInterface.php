<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ApiInterface;

interface ApiTraitInterface
{
    /**
     * Set the api.
     *
     * @param ApiInterface $api The api object to set.
     * @return void
     */
    public function setModelApi(ApiInterface $api): void;

    /**
     * Gets the api.
     *
     * @return ApiInterface The api.
     */
    public function getModelApi(): ApiInterface;
}
