<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\TranslateInterface;

interface TranslateTraitInterface
{
    /**
     * Set the translate.
     *
     * @param TranslateInterface $translate The translate object to set.
     * @return void
     */
    public function setModelTranslate(TranslateInterface $translate): void;

    /**
     * Gets the translate.
     *
     * @return TranslateInterface The translate.
     */
    public function getModelTranslate(): TranslateInterface;
}
