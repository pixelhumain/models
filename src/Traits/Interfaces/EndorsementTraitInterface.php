<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\EndorsementInterface;

interface EndorsementTraitInterface
{
    /**
     * Set the endorsement.
     *
     * @param EndorsementInterface $endorsement The endorsement object to set.
     * @return void
     */
    public function setModelEndorsement(EndorsementInterface $endorsement): void;

    /**
     * Gets the endorsement.
     *
     * @return EndorsementInterface The endorsement.
     */
    public function getModelEndorsement(): EndorsementInterface;
}
