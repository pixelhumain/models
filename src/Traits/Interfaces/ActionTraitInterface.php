<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\ActionInterface;

interface ActionTraitInterface
{
    /**
     * Set the action.
     *
     * @param ActionInterface $action The action object to set.
     * @return void
     */
    public function setModelAction(ActionInterface $action): void;

    /**
     * Gets the action.
     *
     * @return ActionInterface The action.
     */
    public function getModelAction(): ActionInterface;
}
