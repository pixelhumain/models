<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\LinkInterface;

interface LinkTraitInterface
{
    /**
     * Set the link.
     *
     * @param LinkInterface $link The link object to set.
     * @return void
     */
    public function setModelLink(LinkInterface $link): void;

    /**
     * Gets the link.
     *
     * @return LinkInterface The link.
     */
    public function getModelLink(): LinkInterface;
}
