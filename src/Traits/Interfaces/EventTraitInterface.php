<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\EventInterface;

interface EventTraitInterface
{
    /**
     * Set the event.
     *
     * @param EventInterface $event The event object to set.
     * @return void
     */
    public function setModelEvent(EventInterface $event): void;

    /**
     * Gets the event.
     *
     * @return EventInterface The event.
     */
    public function getModelEvent(): EventInterface;
}
