<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\MailErrorInterface;

interface MailErrorTraitInterface
{
    /**
     * Set the mailError.
     *
     * @param MailErrorInterface $mailError The mailError object to set.
     * @return void
     */
    public function setModelMailError(MailErrorInterface $mailError): void;

    /**
     * Gets the mailError.
     *
     * @return MailErrorInterface The mailError.
     */
    public function getModelMailError(): MailErrorInterface;
}
