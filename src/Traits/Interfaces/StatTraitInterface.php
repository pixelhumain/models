<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\StatInterface;

interface StatTraitInterface
{
    /**
     * Set the stat.
     *
     * @param StatInterface $stat The stat object to set.
     * @return void
     */
    public function setModelStat(StatInterface $stat): void;

    /**
     * Gets the stat.
     *
     * @return StatInterface The stat.
     */
    public function getModelStat(): StatInterface;
}
