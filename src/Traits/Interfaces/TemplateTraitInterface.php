<?php

namespace PixelHumain\Models\Traits\Interfaces;

use PixelHumain\Models\Interfaces\TemplateInterface;

interface TemplateTraitInterface
{
    /**
     * Set the template.
     *
     * @param TemplateInterface $template The template object to set.
     * @return void
     */
    public function setModelTemplate(TemplateInterface $template): void;

    /**
     * Gets the template.
     *
     * @return TemplateInterface The template.
     */
    public function getModelTemplate(): TemplateInterface;
}
