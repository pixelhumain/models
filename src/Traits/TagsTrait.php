<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\TagsInterface;

trait TagsTrait
{
    protected ?TagsInterface $tags = null;

    /**
     * Set the tags.
     *
     * @param TagsInterface $tags The tags to set.
     * @return void
     */
    public function setModelTags(TagsInterface $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * Returns the tags.
     *
     * @return TagsInterface The tags.
     */
    public function getModelTags(): TagsInterface
    {
        if ($this->tags === null) {
            throw new Exception(TagsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->tags;
    }
}
