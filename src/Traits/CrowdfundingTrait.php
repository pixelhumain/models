<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CrowdfundingInterface;

trait CrowdfundingTrait
{
    protected ?CrowdfundingInterface $crowdfunding = null;

    /**
     * Set the crowdfunding.
     *
     * @param CrowdfundingInterface $crowdfunding The crowdfunding object to set.
     * @return void
     */
    public function setModelCrowdfunding(CrowdfundingInterface $crowdfunding): void
    {
        $this->crowdfunding = $crowdfunding;
    }

    /**
     * Gets the crowdfunding.
     *
     * @return CrowdfundingInterface The crowdfunding.
     */
    public function getModelCrowdfunding(): CrowdfundingInterface
    {
        if ($this->crowdfunding === null) {
            throw new Exception(CrowdfundingInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->crowdfunding;
    }
}
