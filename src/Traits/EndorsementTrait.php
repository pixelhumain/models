<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\EndorsementInterface;

trait EndorsementTrait
{
    protected ?EndorsementInterface $endorsement = null;

    /**
     * Set the endorsement.
     *
     * @param EndorsementInterface $endorsement The endorsement object to set.
     * @return void
     */
    public function setModelEndorsement(EndorsementInterface $endorsement): void
    {
        $this->endorsement = $endorsement;
    }

    /**
     * Gets the endorsement.
     *
     * @return EndorsementInterface The endorsement.
     */
    public function getModelEndorsement(): EndorsementInterface
    {
        if ($this->endorsement === null) {
            throw new Exception(EndorsementInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->endorsement;
    }
}
