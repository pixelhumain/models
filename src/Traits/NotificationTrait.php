<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NotificationInterface;

trait NotificationTrait
{
    protected ?NotificationInterface $notification = null;

    /**
     * Set the notification.
     *
     * @param NotificationInterface $notification The notification object to set.
     * @return void
     */
    public function setModelNotification(NotificationInterface $notification): void
    {
        $this->notification = $notification;
    }

    /**
     * Gets the notification.
     *
     * @return NotificationInterface The notification.
     */
    public function getModelNotification(): NotificationInterface
    {
        if ($this->notification === null) {
            throw new Exception(NotificationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->notification;
    }
}
