<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\RoleInterface;

trait RoleTrait
{
    protected ?RoleInterface $role = null;

    /**
     * Set the role of the person.
     *
     * @param RoleInterface $role The role object to set.
     * @return void
     */
    public function setModelRole(RoleInterface $role): void
    {
        $this->role = $role;
    }

    /**
     * Gets the role of the person.
     *
     * @return RoleInterface The role of the person.
     */
    public function getModelRole(): RoleInterface
    {
        if ($this->role === null) {
            throw new Exception(RoleInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->role;
    }
}
