<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SurveyInterface;

trait SurveyTrait
{
    protected ?SurveyInterface $survey = null;

    /**
     * Set the survey.
     *
     * @param SurveyInterface $survey The survey to set.
     * @return void
     */
    public function setModelSurvey(SurveyInterface $survey): void
    {
        $this->survey = $survey;
    }

    /**
     * Returns the survey.
     *
     * @return SurveyInterface The survey.
     */
    public function getModelSurvey(): SurveyInterface
    {
        if ($this->survey === null) {
            throw new Exception(SurveyInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }

        return $this->survey;
    }
}
