<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\PoiInterface;

trait PoiTrait
{
    protected ?PoiInterface $poi = null;

    /**
     * Set the poi.
     *
     * @param PoiInterface $poi The poi object to set.
     * @return void
     */
    public function setModelPoi(PoiInterface $poi): void
    {
        $this->poi = $poi;
    }

    /**
     * Gets the poi.
     *
     * @return PoiInterface The poi.
     */
    public function getModelPoi(): PoiInterface
    {
        if ($this->poi === null) {
            throw new Exception(PoiInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->poi;
    }
}
