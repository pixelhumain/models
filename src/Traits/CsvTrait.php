<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CsvInterface;

trait CsvTrait
{
    protected ?CsvInterface $csv = null;

    /**
     * Set the csv.
     *
     * @param CsvInterface $csv The csv object to set.
     * @return void
     */
    public function setModelCsv(CsvInterface $csv): void
    {
        $this->csv = $csv;
    }

    /**
     * Gets the csv.
     *
     * @return CsvInterface The csv.
     */
    public function getModelCsv(): CsvInterface
    {
        if ($this->csv === null) {
            throw new Exception(CsvInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->csv;
    }
}
