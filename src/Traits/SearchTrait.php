<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\SearchInterface;

trait SearchTrait
{
    protected ?SearchInterface $search = null;

    /**
     * Set the search.
     *
     * @param SearchInterface $search The search object to set.
     * @return void
     */
    public function setModelSearch(SearchInterface $search): void
    {
        $this->search = $search;
    }

    /**
     * Gets the search.
     *
     * @return SearchInterface The search.
     */
    public function getModelSearch(): SearchInterface
    {
        if ($this->search === null) {
            throw new Exception(SearchInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->search;
    }
}
