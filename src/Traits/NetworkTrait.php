<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NetworkInterface;

trait NetworkTrait
{
    protected ?NetworkInterface $network = null;

    /**
     * Set the network.
     *
     * @param NetworkInterface $network The network object to set.
     * @return void
     */
    public function setModelNetwork(NetworkInterface $network): void
    {
        $this->network = $network;
    }

    /**
     * Gets the network.
     *
     * @return NetworkInterface The network.
     */
    public function getModelNetwork(): NetworkInterface
    {
        if ($this->network === null) {
            throw new Exception(NetworkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->network;
    }
}
