<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NewsInterface;

trait NewsTrait
{
    protected ?NewsInterface $news = null;

    /**
     * Set the news.
     *
     * @param NewsInterface $news The news object to set.
     * @return void
     */
    public function setModelNews(NewsInterface $news): void
    {
        $this->news = $news;
    }

    /**
     * Gets the news.
     *
     * @return NewsInterface The news.
     */
    public function getModelNews(): NewsInterface
    {
        if ($this->news === null) {
            throw new Exception(NewsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->news;
    }
}
