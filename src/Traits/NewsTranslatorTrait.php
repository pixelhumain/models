<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\NewsTranslatorInterface;

trait NewsTranslatorTrait
{
    protected ?NewsTranslatorInterface $newsTranslator = null;

    /**
     * Set the newsTranslator.
     *
     * @param NewsTranslatorInterface $newsTranslator The newsTranslator object to set.
     * @return void
     */
    public function setModelNewsTranslator(NewsTranslatorInterface $newsTranslator): void
    {
        $this->newsTranslator = $newsTranslator;
    }

    /**
     * Gets the newsTranslator.
     *
     * @return NewsTranslatorInterface The newsTranslator.
     */
    public function getModelNewsTranslator(): NewsTranslatorInterface
    {
        if ($this->newsTranslator === null) {
            throw new Exception(NewsTranslatorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->newsTranslator;
    }
}
