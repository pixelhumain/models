<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CommentInterface;

trait CommentTrait
{
    protected ?CommentInterface $comment = null;

    /**
     * Set the comment.
     *
     * @param CommentInterface $comment The comment object to set.
     * @return void
     */
    public function setModelComment(CommentInterface $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * Gets the comment.
     *
     * @return CommentInterface The comment.
     */
    public function getModelComment(): CommentInterface
    {
        if ($this->comment === null) {
            throw new Exception(CommentInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->comment;
    }
}
