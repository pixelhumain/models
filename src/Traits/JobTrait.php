<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\JobInterface;

trait JobTrait
{
    protected ?JobInterface $job = null;

    /**
     * Set the job.
     *
     * @param JobInterface $job The job object to set.
     * @return void
     */
    public function setModelJob(JobInterface $job): void
    {
        $this->job = $job;
    }

    /**
     * Gets the job.
     *
     * @return JobInterface The job.
     */
    public function getModelJob(): JobInterface
    {
        if ($this->job === null) {
            throw new Exception(JobInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->job;
    }
}
