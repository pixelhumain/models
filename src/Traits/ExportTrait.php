<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\ExportInterface;

trait ExportTrait
{
    protected ?ExportInterface $export = null;

    /**
     * Set the export.
     *
     * @param ExportInterface $export The export object to set.
     * @return void
     */
    public function setModelExport(ExportInterface $export): void
    {
        $this->export = $export;
    }

    /**
     * Gets the export.
     *
     * @return ExportInterface The export.
     */
    public function getModelExport(): ExportInterface
    {
        if ($this->export === null) {
            throw new Exception(ExportInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->export;
    }
}
