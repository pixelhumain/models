<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\CostumInterface;

trait CostumTrait
{
    protected ?CostumInterface $costum = null;

    /**
     * Set the costum.
     *
     * @param CostumInterface $costum The costum object to set.
     * @return void
     */
    public function setModelCostum(CostumInterface $costum): void
    {
        $this->costum = $costum;
    }

    /**
     * Gets the costum.
     *
     * @return CostumInterface The costum.
     */
    public function getModelCostum(): CostumInterface
    {
        if ($this->costum === null) {
            throw new Exception(CostumInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->costum;
    }
}
