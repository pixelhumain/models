<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\DocumentInterface;

trait DocumentTrait
{
    protected ?DocumentInterface $document = null;

    /**
     * Set the document.
     *
     * @param DocumentInterface $document The document object to set.
     * @return void
     */
    public function setModelDocument(DocumentInterface $document): void
    {
        $this->document = $document;
    }

    /**
     * Gets the document.
     *
     * @return DocumentInterface The document.
     */
    public function getModelDocument(): DocumentInterface
    {
        if ($this->document === null) {
            throw new Exception(DocumentInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->document;
    }

    /**
     * Merge the given data with images.
     *
     * @param string $id The identifier of the data item.
     * @param array $data The data to be merged with images.
     * @param array|null $dataMerge The data to be merged with images (optional).
     * @return array The data merged with images.
     */
    protected function mergeWithImages(string $id, array $data, ?array $dataMerge = null): array
    {
        $images = $this->getModelDocument()->retrieveAllImagesUrl($id, self::COLLECTION, $data);
        return array_merge($dataMerge ?? $data, $images);
    }

    /**
     * List all images for a given ID.
     *
     * @param string $id The ID to retrieve images for.
     * @return array An array of images.
     */
    protected function listImages(string $id): array
    {
        $where = [
            "id" => $id,
            "type" => self::COLLECTION,
            "doctype" => DocumentInterface::DOC_TYPE_IMAGE,
        ];
        return $this->getListDocumentsWhere($where, 'image');
    }

    /**
     * List all files associated with the given ID.
     *
     * @param string $id The ID of the document.
     * @return array An array of file names.
     */
    protected function listFiles(string $id): array
    {
        $where = [
            "id" => $id,
            "type" => self::COLLECTION,
            "doctype" => DocumentInterface::DOC_TYPE_FILE,
        ];
        return $this->getListDocumentsWhere($where, 'file');
    }

    /**
     * Returns a list of documents based on the given conditions.
     *
     * @param array $where An array of conditions to filter the documents.
     * @param string $type The type of documents to retrieve.
     * @return array An array of documents that match the given conditions.
     */
    private function getListDocumentsWhere(array $where, string $type): array
    {
        return $this->getModelDocument()->getListDocumentsWhere($where, $type);
        ;
    }
}
