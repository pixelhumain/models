<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\TemplateInterface;

trait TemplateTrait
{
    protected ?TemplateInterface $template = null;

    /**
     * Set the template.
     *
     * @param TemplateInterface $template The template object to set.
     * @return void
     */
    public function setModelTemplate(TemplateInterface $template): void
    {
        $this->template = $template;
    }

    /**
     * Gets the template.
     *
     * @return TemplateInterface The template.
     */
    public function getModelTemplate(): TemplateInterface
    {
        if ($this->template === null) {
            throw new Exception(TemplateInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->template;
    }
}
