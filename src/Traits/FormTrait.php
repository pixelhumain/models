<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\FormInterface;

trait FormTrait
{
    protected ?FormInterface $form = null;

    /**
     * Set the form.
     *
     * @param FormInterface $form The form object to set.
     * @return void
     */
    public function setModelForm(FormInterface $form): void
    {
        $this->form = $form;
    }

    /**
     * Gets the form.
     *
     * @return FormInterface The form.
     */
    public function getModelForm(): FormInterface
    {
        if ($this->form === null) {
            throw new Exception(FormInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->form;
    }
}
