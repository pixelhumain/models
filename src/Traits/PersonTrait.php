<?php

namespace PixelHumain\Models\Traits;

use Exception;
use PixelHumain\Models\Interfaces\PersonInterface;

trait PersonTrait
{
    protected ?PersonInterface $person = null;

    /**
     * Set the person.
     *
     * @param PersonInterface $person The person object to set.
     * @return void
     */
    public function setModelPerson(PersonInterface $person): void
    {
        $this->person = $person;
    }

    /**
     * Gets the person.
     *
     * @return PersonInterface The person.
     */
    public function getModelPerson(): PersonInterface
    {
        if ($this->person === null) {
            throw new Exception(PersonInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->person;
    }
}
