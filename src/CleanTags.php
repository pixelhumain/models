<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CleanTagsInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class CleanTags extends BaseModel implements CleanTagsInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Cleans all tags in a given collection.
     *
     * @param string $collection The name of the collection.
     * @param array $doc The document containing the tags to be cleaned.
     * @param string $key The key of the tags in the document.
     * @param string $key3 The third key of the tags in the document.
     * @return void
     */
    public function cleanAllTags(string $collection, array $doc, string $key, string $key3): void
    {
        $this->db->update(
            $collection,
            [
                "_id" => $doc['_id'],
            ],
            [
                '$unset' => [
                    "tags." . $key3 . "" => "",
                ],
            ]
        );
        $this->db->update(
            $collection,
            [
                "_id" => $doc['_id'],
            ],
            [
                '$addToSet' => [
                    "tags" => $key,
                ],
            ]
        );
    }
}
