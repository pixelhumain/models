<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\CommentInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Interfaces\ResolutionInterface;
use PixelHumain\Models\Interfaces\RoomInterface;
use PixelHumain\Models\Interfaces\ZoneInterface;
use PixelHumain\Models\Traits\ActivityStreamTrait;


use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\FsTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTranslatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProjectTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;
use PixelHumain\Models\Traits\NewsTranslatorTrait;
use PixelHumain\Models\Traits\NotificationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\ProjectTrait;
use PixelHumain\Models\Traits\ZoneTrait;
use PixelHumain\Models\Utils\ImagesUtils;
use SimpleXMLElement;

// TODO: Yii::get('module')->getModule(self::MODULE)->assetsUrl
// TODO : Yii::get('url')::base(true)

class News extends BaseModel implements NewsInterface, NewsTranslatorTraitInterface, ActivityStreamTraitInterface, PersonTraitInterface, DataValidatorTraitInterface, ElementTraitInterface, NotificationTraitInterface, CostumTraitInterface, CityTraitInterface, ZoneTraitInterface, DocumentTraitInterface, CommentTraitInterface, AuthorisationTraitInterface, PreferenceTraitInterface, ProjectTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;
    use FsTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use NewsTranslatorTrait;
    use ActivityStreamTrait;
    use PersonTrait;
    use DataValidatorTrait;
    use ElementTrait;
    use NotificationTrait;
    use CostumTrait;
    use CityTrait;
    use ZoneTrait;
    use DocumentTrait;
    use CommentTrait;
    use AuthorisationTrait;
    use PreferenceTrait;
    use ProjectTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
        $this->validateFsProperty();
    }

    // TODO : ne devrait pas être dans un model je pense
    /**
     * Retrieves the configuration for the News class.
     *
     * @return array The configuration array.
     */
    public function getConfig()
    {
        return [
            "collection" => NewsInterface::COLLECTION,
            "controller" => NewsInterface::CONTROLLER,
            "module" => NewsInterface::MODULE,
            "assets" => Yii::get('module')->getModule(NewsInterface::MODULE)->assetsUrl,
            "init" => Yii::get('module')->getModule(NewsInterface::MODULE)->assetsUrl . "/js/init.js",
            "form" => Yii::get('module')->getModule(NewsInterface::MODULE)->assetsUrl . "/js/dynForm.js",
            "lbhp" => true,
        ];
    }

    /**
     * Retrieves a news item by its ID.
     *
     * @param string $id The ID of the news item.
     * @param array|null $followsArrayIds An optional array of IDs representing the users who follow the news item.
     * @param bool $convertNews Specifies whether to convert the news item to an array.
     * @return array|null The news item as an array, or null if not found.
     */
    public function getById(string $id, ?array $followsArrayIds = null, bool $convertNews = true): ?array
    {
        $news = $this->db->findOne(NewsInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
        if (! empty($news["type"]) && $convertNews) {
            $news = $this->getModelNewsTranslator()->convertParamsForNews($news, true, $followsArrayIds);
        }
        return $news;
    }

    /**
     * Get news by their IDs.
     *
     * @param array $ids The array of news IDs.
     * @param bool $convertNews Whether to convert the news objects.
     * @return array The array of news objects.
     */
    public function getByIds(array $ids, bool $convertNews = true): array
    {
        $news = $this->db->findByIds(NewsInterface::COLLECTION, $ids);
        if ($convertNews) {
            $convertNews = [];
            foreach ($news as $id => $item) {
                $convertNews[] = $this->getModelNewsTranslator()->convertParamsForNews($item, true);
            }

            $news = $convertNews;
        }
        return $news;
    }

    /**
     * Retrieves the author information for a given ID.
     *
     * @param string $id The ID of the author.
     * @return array|null The author information as an array, or null if not found.
     */
    public function getAuthor(string $id): ?array
    {
        return $this->db->findOneById(
            NewsInterface::COLLECTION,
            $id,
            [
                "author" => 1,
            ]
        );
    }

    // TODO : $type, $followsArrayIds n'est pas utilisé
    /**
     * Retrieves news for a given object ID.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting criteria for the query. Default is ["created" => -1].
     * @param string|null $type The type of news to retrieve. Default is null.
     * @param int $limit The maximum number of news to retrieve. Default is 6.
     * @param array|null $followsArrayIds The array of IDs to filter the news by. Default is null.
     * @return array The array of news matching the given criteria.
     */
    public function getNewsForObjectId(array $param, array $sort = [
        "created" => -1,
    ], ?string $type = null, int $limit = 6, ?array $followsArrayIds = null): array
    {
        $param["type"] = [
            '$ne' => ActivityStreamInterface::COLLECTION,
        ];

        $res = $this->db->findAndSort(NewsInterface::COLLECTION, $param, $sort, $limit);
        return $res;
    }

    // TODO : $_POST
    /**
     * Saves the news data.
     *
     * @param array $params The parameters for saving the news.
     * @return array The saved news data.
     */
    public function save(array $params): array
    {
        $userId = $this->session->getUserId();
        //check a user is loggued
        $user = $this->getModelPerson()->getById($userId);
        //TODO : if type is Organization check the connected user isAdmin

        if (empty($user)) {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "You must be logged in to add a news entry !"),
            ];
        }

        //xss clean up
        $params = $this->getModelDataValidator()->clearUserInput($params);
        if ((isset($_POST["text"]) && ! empty($_POST["text"]))
            || (isset($_POST["media"]) && ! empty($_POST["media"]))
            || (isset($_POST["mediaImg"]) && ! empty($_POST["mediaImg"]))
            || (isset($_POST["mediaFile"]) && ! empty($_POST["mediaFile"]))
        ) {
            $news = $this->prepData($params);
            $news = $this->db->insert(NewsInterface::COLLECTION, $news);

            //NOTIFICATION MENTIONS
            if (isset($news["mentions"])) {
                $target = [
                    "id" => (string) $news["_id"],
                    "type" => NewsInterface::COLLECTION,
                ];
                if (@$news["targetIsAuthor"] && @$news["parentType"]) {
                    $authorName = $this->getModelElement()->getElementSimpleById($news["parentId"], $news["parentType"]);
                    $author = [
                        "id" => $news["parentId"],
                        "type" => $news["parentType"],
                        "name" => $authorName["name"],
                    ];
                } else {
                    $author = [
                        "id" => $this->session->getUserId(),
                        "type" => PersonInterface::COLLECTION,
                        "name" => $this->session->getUserName(),
                    ];
                }
                $this->getModelNotification()->notifyMentionOn($author, $target, $news["mentions"], null, $news["scope"]);
            }

            //NOTIFICATION POST
            $target = $news["target"];
            if (@$news["targetIsAuthor"]) {
                $target["targetIsAuthor"] = true;
            } elseif ($params["parentType"] == PersonInterface::COLLECTION && $params["parentId"] != $this->session->getUserId()) {
                $target["userWall"] = true;
            }
            if ($params["parentType"] != PersonInterface::COLLECTION || $params["parentId"] != $this->session->getUserId()) {
                $this->getModelNotification()->constructNotification(ActStrInterface::VERB_POST, [
                    "id" => $this->session->getUserId(),
                    "name" => $this->session->getUserName(),
                ], $target, null, null, null, $news["text"]);
            }
            $news = $this->getModelNewsTranslator()->convertParamsForNews($news);
            $news["author"] = $this->getModelPerson()->getSimpleUserById($this->session->getUserId());

            return [
                "result" => true,
                "msg" => "Votre message est enregistré.",
                "id" => $news["_id"],
                "object" => $news,
            ];
        } else {
            return [
                "result" => false,
                "msg" => "Please Fill required Fields.",
            ];
        }
    }

    /**
     * Preps the data for the News model.
     *
     * @param array $params The parameters for the data preparation.
     * @return array The prepared data.
     */
    public function prepData(array $params): array
    {
        $typeNews = @$params["type"] ? $params["type"] : "news";
        $news = [
            "type" => $typeNews,
            "text" => $params["text"],
            "author" => $this->session->getUserId(),
            "date" => $this->db->MongoDate(time()),
            "collection" => NewsInterface::COLLECTION,
            "sharedBy" => [[
                "id" => $this->session->getUserId(),
                "type" => PersonInterface::COLLECTION,
                "updated" => $this->db->MongoDate(time()),
            ]],
            'target' => [
                'id' => @$params["parentId"],
                'type' => @$params["parentType"],
            ],
            "created" => $this->db->MongoDate(time()),
            "markdownActive" => isset($params["markdownActive"]) && $params["markdownActive"],
        ];

        if (isset($params["targetIsAuthor"]) && $params["targetIsAuthor"] == true) {
            $news["sharedBy"] = [[
                "id" => $params["parentId"],
                "type" => $params["parentType"],
                'on' => 'activitypub',
                "updated" => $this->db->MongoDate(time()),
            ]];
        }

        if (isset($params["date"])) {
            $news["date"] = $this->db->MongoDate(strtotime(str_replace('/', '-', $params["date"])));
        }
        if (isset($params["media"])) {
            $news["media"] = $params["media"];
            if (isset($params["media"]["content"]) && isset($params["media"]["content"]["image"]) && ! isset($params["media"]["content"]["imageId"])) {
                $urlImage = $this->uploadNewsImage($params["media"]["content"]["image"], $params["media"]["content"]["imageSize"], $this->session->getUserId());
                $news["media"]["content"]["image"] = Yii::get('url')::base(true) . "/" . $urlImage;
            }
        }
        if (isset($params["mediaImg"])) {
            $news["mediaImg"] = $params["mediaImg"];
        }

        if (isset($params["mediaFile"])) {
            $news["mediaFile"] = $params["mediaFile"];
        }

        $costum = $this->getModelCostum()->getCostum();
        if (isset($params["tags"])) {
            $news["tags"] = $params["tags"];
        }
        if (isset($params["targetIsAuthor"])) {
            $news["targetIsAuthor"] = $params["targetIsAuthor"];
        }
        if (isset($params["mentions"])) {
            $news["mentions"] = $params["mentions"];
        }
        if (isset($params["source"])) {
            $news["source"] = $params["source"];
        } elseif (isset($costum) && isset($costum["slug"])) {
            $news["source"] = [
                "origin" => "costum",
                "key" => $costum["slug"],
                "keys" => [$costum["slug"]],
            ];
        }
        $news["scope"] = $this->formatedScope($params);
        return $news;
    }

    /**
     * Returns the formatted scope based on the given parameters.
     *
     * @param array $params The parameters to filter the scope.
     * @return array The formatted scope.
     */
    public function formatedScope(array $params): array
    {
        $scopes = [
            "type" => $params["scope"],
        ];
        if ($params["scope"] == "public" && ! empty($params["localities"])) {
            $localities = $params["localities"];
            if (! empty($localities)) {
                foreach ($localities as $key => $locality) {
                    if (! empty($locality)) {
                        if ($locality["type"] == CityInterface::COLLECTION) {
                            $city = $this->getModelCity()->getById($locality["id"]);
                            $scope = [
                                "parentId" => (string) $city["_id"],
                                "parentType" => $locality["type"],
                                "name" => $city["name"],
                                "geo" => $city["geo"],
                            ];
                            if (! empty($city["postalCode"])) {
                                $scope["postalCode"] = $city["postalCode"];
                            }

                            $scope = array_merge($scope, $this->getModelZone()->getLevelIdById((string) $city["_id"], $city, CityInterface::COLLECTION));
                            $scopes["localities"][] = $scope;
                        } elseif ($locality["type"] == "cp") {
                            $where = [
                                "postalCodes.postalCode" => strval($locality["name"]),
                            ];
                            if (@$locality["countryCode"]) {
                                $where["country"] = $locality["countryCode"];
                            }
                            $city = $this->getModelCity()->getWhereFindOne($where, $fields = null);
                            if (! empty($city)) {
                                $scope = [
                                    "postalCode" => strval($locality["name"]),
                                ];
                                $scope = array_merge($scope, $this->getModelZone()->getLevelIdById((string) $city["_id"], $city, CityInterface::COLLECTION));
                                $scopes["localities"][] = $scope;
                            }
                        } else {
                            $zone = $this->getModelZone()->getById($locality["id"]);
                            $scope = [
                                "parentId" => $locality["id"],
                                "parentType" => $locality["type"],
                                "name" => $zone["name"],
                                "geo" => $zone["geo"],
                            ];
                            $scope = array_merge($scope, $this->getModelZone()->getLevelIdById($locality["id"], $zone, ZoneInterface::COLLECTION));

                            $scopes["localities"][] = $scope;
                        }
                    }
                }
            }
        }
        return $scopes;
    }

    /**
     * Deletes a news item.
     *
     * @param string $id The ID of the news item to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @param bool $removeComments (optional) Whether to remove comments associated with the news item. Default is false.
     * @param bool $deleteProcess (optional) Whether to delete the associated process. Default is false.
     * @param bool $forced (optional) Whether to force the deletion. Default is false.
     * @return array An array containing the result of the deletion operation.
     */
    public function delete(string $id, string $userId, bool $removeComments = false, bool $deleteProcess = false, bool $forced = false): array
    {
        $countShare = null;
        $shareUpdate = null;
        $news = $this->getById($id);
        $nbCommentsDeleted = 0;

        //Check if the userId can delete the news
        $authorization = $this->canAdministrate($userId, $id, $deleteProcess);

        if (! $authorization && $forced == false) {
            return [
                "result" => false,
                "userId" => $userId,
                "msg" => $this->language->t("common", "You are not allowed to delete this news"),
                "id" => $id,
            ];
        }

        if ($authorization == "share" && ! empty($news["sharedBy"])) {
            $countShare = is_countable($news["sharedBy"]) ? count($news["sharedBy"]) : 0;
        }

        if ($authorization === true || (@$countShare && $countShare == 1)) {
            //Delete image
            if (isset($news["media"]) && isset($news["media"]["content"]) && isset($news["media"]["content"]["image"]) && ! isset($news["media"]["content"]["imageId"])) {
                $endPath = explode($this->params->get('uploadUrl'), $news["media"]["content"]["image"]);
                $pathFileDelete = $this->params->get('uploadDir') . $endPath[1];
                if ($this->fs->has($pathFileDelete)) {
                    $this->fs->delete($pathFileDelete);
                }
            }

            if (isset($news["mediaImg"]) && isset($news["mediaImg"]["images"]) && is_array($news["mediaImg"]["images"])) {
                foreach ($news["mediaImg"]["images"] as $document) {
                    $documentId = (string) $document["_id"];
                    $retour = $this->getModelDocument()->removeDocumentById($documentId);
                }
            }

            if (isset($news["mediaFile"]) && isset($news["mediaFile"]["files"]) && is_array($news["mediaFile"]["files"])) {
                foreach ($news["mediaFile"]["files"] as $document) {
                    $documentId = (string) $document["_id"];
                    $retour = $this->getModelDocument()->removeDocumentById($documentId);
                }
            }

            //récupère les activityStream liés à la news (les partages)
            $actStream = $this->db->find(NewsInterface::COLLECTION, [
                "type" => "activityStream",
                "verb" => ActStrInterface::TYPE_ACTIVITY_SHARE,
                "object.type" => "news",
                "object.id" => $id,
            ]);
            //efface les commentaires des activityStream liés à la news
            if (! empty($actStream)) {
                foreach ($actStream as $key => $value) {
                    $this->db->remove(CommentInterface::COLLECTION, [
                        "contextType" => "news",
                        "contextId" => $key,
                    ]);
                }
            }
            //efface les activityStream lié à la news
            $this->db->remove(NewsInterface::COLLECTION, [
                "type" => "activityStream",
                "verb" => ActStrInterface::TYPE_ACTIVITY_SHARE,
                "object.type" => "news",
                "object.id" => $id,
            ]);

            if ($removeComments) {
                $res = $this->getModelComment()->deleteAllContextComments($id, NewsInterface::COLLECTION, $userId, $deleteProcess);
                if (! $res["result"]) {
                    return $res;
                }
            }

            //Remove the news
            $res = $this->db->remove(NewsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
        } elseif ($authorization == "share" && $countShare > 1) {
            $key = array_search($userId, array_column($news["sharedBy"], "id"));
            $shareUpdate = true;
            $res = $this->db->update(NewsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ], [
                '$pull' => [
                    "sharedBy" => [
                        "id" => $userId,
                    ],
                ],
            ]);
        }
        $res = [
            "result" => true,
            "msg" => "The news with id " . $id . " and " . $nbCommentsDeleted . " comments have been removed with succes.",
            "type" => @$news["type"],
            "commentsDeleted" => $nbCommentsDeleted,
        ];
        if (@$shareUpdate) {
            $followsArrayIds = [];
            $parent = $this->getModelElement()->getElementSimpleById($this->session->getUserId(), PersonInterface::COLLECTION, null, ["links"]);
            if (@$parent["links"]["follows"] && ! empty($parent["links"]["follows"])) {
                foreach ($parent["links"]["follows"] as $key => $data) {
                    array_push($followsArrayIds, $key);
                }
            }
            $res["newsUp"] = $this->getById($id, $followsArrayIds);
        }
        return $res;
    }

    /**
     * Deletes news of an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param bool $removeComments (optional) Whether to remove comments associated with the news. Default is false.
     * @param bool $forced (optional) Whether to force the deletion. Default is false.
     * @return array An array containing the result of the deletion.
     */
    public function deleteNewsOfElement(string $elementId, string $elementType, string $userId, bool $removeComments = false, bool $forced = false): array
    {
        //Check if the $userId can delete the element
        $canDelete = $this->getModelAuthorisation()->canDeleteElement($elementId, $elementType, $userId);
        if (! $canDelete && $forced == false) {
            return [
                "result" => false,
                "msg" => "You do not have enough credential to delete this element news.",
            ];
        }

        $where = [
            '$or' => [[
                "target.id" => $elementId,
            ], [
                "object.id" => $elementId,
            ]],
        ];

        $news2delete = $this->db->find(NewsInterface::COLLECTION, $where);
        $nbNews = 0;

        foreach ($news2delete as $id => $aNews) {
            $res = $this->delete($id, $userId, true, true, $forced);
            if ($res["result"] == false) {
                return $res;
            }
            $nbNews++;
        }

        return [
            "result" => true,
            "msg" => $nbNews . " news of the element " . $elementId . " of type " . $elementType . " have been removed with succes.",
        ];
    }

    /**
     * Share the news.
     *
     * @param string $verb The action verb.
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string|null $comment The comment (optional).
     * @param array|null $activityValue The activity value (optional).
     * @return array The result of the share operation.
     */
    public function share(string $verb, string $targetId, string $targetType, ?string $comment = null, ?array $activityValue = null): array
    {
        $share = $this->db->findOne(
            NewsInterface::COLLECTION,
            [
                "verb" => $verb,
                "object.id" => @$activityValue["id"],
                "object.type" => @$activityValue["type"],
            ]
        );

        if ($share != null) {
            $allShare = [];
            //regarde tous les sharedBy
            foreach ($share["sharedBy"] as $key => $value) {
                if ($value["id"] != $this->session->getUserId()) { //si ce n'est pas moi je garde ce partage
                    $allShare[] = $value;
                }
            }

            //je me rajoute à la liste des allShare
            $share["sharedBy"] = [
                ...$allShare, [
                    "id" => $this->session->getUserId(),
                    "type" => PersonInterface::COLLECTION,
                    "comment" => @$comment,
                    "updated" => $this->db->MongoDate(time()),
                ]];

            $this->db->update(
                NewsInterface::COLLECTION,
                [
                    "_id" => $share["_id"],
                ],
                $share
            );
            $idNews = (string) $share["_id"];
        } else {
            $buildArray = [
                "type" => ActivityStreamInterface::COLLECTION,
                "verb" => $verb,
                "target" => [
                    "id" => $targetId,
                    "type" => $targetType,
                ],
                "author" => $this->session->getUserId(),
                "object" => $activityValue,
                "scope" => [
                    "type" => "restricted",
                ],
                "created" => $this->db->MongoDate(time()),
                "sharedBy" => [[
                    "id" => $this->session->getUserId(),
                    "type" => PersonInterface::COLLECTION,
                    "comment" => @$comment,
                    "updated" => $this->db->MongoDate(time()),
                ]],
            ];

            $newsShared = $this->getModelActivityStream()->addEntry($buildArray);
            $idNews = (string) $newsShared["_id"];
        }
        $newsShared = $this->getById($idNews);
        return [
            "result" => true,
            "msg" => $this->language->t("common", "News has been shared with your network"),
            "data" => $newsShared,
            "idNews" => $idNews,
        ];
    }

    /**
     * Removes news by image ID.
     *
     * @param string $imageId The ID of the image.
     * @return mixed The result of the operation.
     */
    public function removeNewsByImageId(string $imageId)
    {
        return $this->db->remove(NewsInterface::COLLECTION, [
            "media.content.imageId" => $imageId,
        ]);
    }

    // TODO : $userId n'est pas utilisé
    /**
     * Update a field in the news object.
     *
     * @param string $newsId The ID of the news object.
     * @param string $name The name of the field to update.
     * @param mixed $value The new value for the field.
     * @param string|null $userId The ID of the user performing the update (optional).
     * @return array The updated news object.
     */
    public function updateField(string $newsId, string $name, $value, ?string $userId): array
    {
        $set = [
            $name => $value,
        ];
        $this->db->update(
            NewsInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($newsId),
            ],
            [
                '$set' => $set,
            ]
        );

        return [
            "result" => true,
            "msg" => $this->language->t("common", "News well updated"),
            "id" => $newsId,
        ];
    }

    /**
     * Update the comment mentions for a news item.
     *
     * @param array $mentionsComment The mentions in the comment.
     * @param string $id The ID of the news item.
     * @return bool Returns true if the comment mentions were successfully updated, false otherwise.
     */
    public function updateCommentMentions(array $mentionsComment, string $id): bool
    {
        $news = $this->db->findOneById(NewsInterface::COLLECTION, $id);
        if (@$news["commentMentions"]) {
            foreach ($news["commentMentions"] as $value) {
                array_push($mentionsComment, $value);
            }
        }
        $this->db->update(
            NewsInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    "commentMentions" => $mentionsComment,
                ],
            ]
        );
        return true;
    }

    // TODO : $_POST
    /**
     * Update the news with the given parameters.
     *
     * @param array $params The parameters to update the news.
     * @return array The updated news.
     */
    public function update(array $params): array
    {
        $params = $this->getModelDataValidator()->clearUserInput($params);
        if ((isset($params["text"]) && ! empty($params["text"])) || (isset($params["media"]) && ! empty($params["media"]))) {
            $set = [
                "text" => $params["text"],
                "updated" => $this->db->MongoDate(time()),
            ];
            $unset = [];
            if (isset($params["media"])) {
                if ($params["media"] == "unset") {
                    $unset["media"] = "";
                } else {
                    $set["media"] = $params["media"];
                    if (isset($params["media"]["content"]) && isset($params["media"]["content"]["image"]) && ! isset($params["media"]["content"]["imageId"])) {
                        $endPath = explode($this->params->get('uploadUrl'), $params["media"]["content"]["image"]);
                        if (isset($endPath[1]) && ! empty($endPath[1]) && ! file_exists($this->params->get('uploadDir') . $endPath[1])) {
                            $urlImage = $this->uploadNewsImage($params["media"]["content"]["image"], $params["media"]["content"]["imageSize"], $this->session->getUserId());
                            $set["media"]["content"]["image"] = Yii::get('url')::base(true) . "/" . $urlImage;
                        }
                    }
                }
            }
            if (isset($params["mediaImg"])) {
                if ($params["mediaImg"] == "unset") {
                    $unset["mediaImg"] = "";
                } else {
                    $set["mediaImg"] = $params["mediaImg"];
                }
            }
            if (isset($params["mediaFile"])) {
                if ($params["mediaFile"] == "unset") {
                    $unset["mediaFile"] = "";
                } else {
                    $set["mediaFile"] = $params["mediaFile"];
                }
            }

            if (isset($params["tags"])) {
                $set["tags"] = $params["tags"];
            } else {
                $unset["tags"] = "";
            }
            if (isset($params["mentions"])) {
                $set["mentions"] = $params["mentions"];
            } else {
                $unset["mentions"] = "";
            }
            $set["scope"] = $this->formatedScope($params);
            $modify = [
                '$set' => $set,
            ];
            if (isset($unset) && ! empty($unset)) {
                $modify['$unset'] = $unset;
            }
            //update the project
            $this->db->update(
                NewsInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($params["idNews"]),
                ],
                $modify
            );
            $news = $this->getById($_POST["idNews"]);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "News well updated"),
                "object" => $news,
            ];
        } else {
            return [
                "result" => false,
                "msg" => "Please Fill required Fields.",
            ];
        }
    }

    /**
     * Sorts the news array based on the specified columns.
     *
     * @param array $array The array of news to be sorted.
     * @param array $cols The columns to sort the news by.
     * @return array The sorted news array.
     */
    public function sortNews(array $array, array $cols): array
    {
        $colarr = [];
        foreach ($cols as $col => $order) {
            $colarr[$col] = [];
            foreach ($array as $k => $row) {
                $colarr[$col]['_' . $k] = strtolower(@$row[$col]);
            }
        }
        $eval = 'array_multisort(';
        foreach ($cols as $col => $order) {
            $eval .= '$colarr[\'' . $col . '\'],' . $order . ',';
        }
        $eval = substr($eval, 0, -1) . ');';
        eval($eval);
        $ret = [];
        foreach ($colarr as $col => $arr) {
            foreach ($arr as $k => $v) {
                $k = substr($k, 1);
                if (! isset($ret[$k])) {
                    $ret[$k] = $array[$k];
                }
                $ret[$k][$col] = @$array[$k][$col];
            }
        }
        return $ret;
    }

    /**
     * Get news to moderate.
     *
     * @param array|null $whereAdditional Additional conditions for the query.
     * @param int $limit The maximum number of news to retrieve.
     * @return array The array of news to moderate.
     */
    public function getNewsToModerate(?array $whereAdditional = null, int $limit = 0): array
    {
        $where = [
            "reportAbuse" => [
                '$exists' => 1,
            ],
            "isAnAbuse" => [
                '$exists' => 0,
            ],
            "target.id" => [
                '$exists' => 1,
            ],
            "target.type" => [
                '$exists' => 1,
            ],
            "scope.type" => [
                '$exists' => 1,
            ],
            "reportAbuseCount" => [
                '$gt' => 0,
            ],
            "moderate." . $this->session->getUserId() => [
                '$exists' => 0,
            ],
        ];
        if (is_countable($whereAdditional) ? count($whereAdditional) : 0) {
            $where = array_merge($where, $whereAdditional);
        }
        return $this->db->findAndSort(NewsInterface::COLLECTION, $where, [
            "date" => 1,
        ], $limit);
    }

    /**
     * Uploads a news image.
     *
     * @param string $urlImage The URL of the image to upload.
     * @param string $size The size of the image.
     * @param string $authorId The ID of the author.
     * @param bool $actionUpload Whether to perform the upload action or not. Default is true.
     * @return string The uploaded image URL.
     */
    public function uploadNewsImage(string $urlImage, string $size, string $authorId, bool $actionUpload = true): string
    {
        $allowed_ext = ['jpg', 'jpeg', 'png', 'gif'];
        ini_set('user_agent', 'Mozilla/4.0 (compatible; MSIE 6.0)');
        $ext = strtolower(pathinfo($urlImage, PATHINFO_EXTENSION));
        if (empty($ext)) {
            $ext = "png";
        }
        if (strstr($ext, "?")) {
            $ext = explode("?", $ext);
            $ext = $ext[0];
        }
        $dir = "communecter";
        $folder = "news";

        $upload_dir = $dir . '/' . $folder;
        $upload_dir_tmp = $dir . '/tmp';
        $returnUrl = $this->params->get('uploadUrl') . $dir . '/' . $folder;
        $returnUrl_tmp = $this->params->get('uploadUrl') . $dir . '/tmp';

        if (! $this->fs->has($upload_dir)) {
            $this->fs->createDirectory($upload_dir);
        }

        $name = time() . "_" . $authorId . "." . $ext;

        if ($size = "large") {
            $maxWidth = 500;
            $maxHeight = 500;
        } else {
            $maxWidth = 100;
            $maxHeight = 100;
        }
        $quality = 100;
        $streamRes = true;

        if (! $this->fs->has($upload_dir_tmp)) {
            $this->fs->createDirectory($upload_dir_tmp);
        }

        $context = stream_context_create(
            [
                "http" => [
                    "header" => "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
                ],
            ]
        );
        $img = file_get_contents($urlImage, false, $context);

        $this->fs->write($upload_dir_tmp . "/" . $name, $img, [
            'visibility' => 'public',
        ]);

        $imageUtils = new ImagesUtils($this->fs, $upload_dir_tmp . "/" . $name);
        $destPathThumb = $upload_dir . "/" . $name;
        $returnUrl = $returnUrl . "/" . $name;
        $imageUtils->resizePropertionalyImage($maxWidth, $maxHeight)->save($destPathThumb, $quality);

        if ($this->fs->has($upload_dir_tmp . "/" . $name)) {
            $this->fs->delete($upload_dir_tmp . "/" . $name);
        }

        return $returnUrl;
    }

    /**
     * Retrieves the structure of the RSS channel for a given element name.
     *
     * @param string $elementName The name of the element.
     * @return SimpleXMLElement The structure of the RSS channel.
     */
    public function getStrucChannelRss(string $elementName): SimpleXMLElement
    {
        $xmlElement = new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0">
				<channel></channel>
				<title> ' . $elementName . ' </title>
				<description>Communecter, un site fait par les communs pour les communs </description>
					<image>
      					<url>http://127.0.0.1/ph/assets/7d331fe5/images/Communecter-32x32.svg</url>
      					</image>
				</rss>'
        );

        return $xmlElement;
    }

    /**
     * Returns the KML structure of the news.
     *
     * @return SimpleXMLElement The KML structure of the news.
     */
    public function getStrucKml(): SimpleXMLElement
    {
        $kmlElement = new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?>
			<kml xmlns="http://www.opengis.net/kml/2.2">
			

			</kml>'
        );

        return $kmlElement;
    }

    /**
     * Check if a user can administrate a news item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the news item.
     * @param bool $deleteProcess Whether it is a delete process or not. Default is false.
     * @return bool Returns true if the user can administrate the news item, false otherwise.
     */
    public function canAdministrate(string $userId, string $id, bool $deleteProcess = false): bool
    {
        $news = $this->getById($id, false, false);

        if (empty($news)) {
            return false;
        }
        if (@$news["author"] == $userId && (! @$news["verb"] || $news["verb"] != "share")) {
            return true;
        }
        if (@$news["sharedBy"] && in_array($userId, array_column($news["sharedBy"], "id"))) {
            return "share";
        }

        $DDATypes = [ProposalInterface::COLLECTION, ActionInterface::COLLECTION, RoomInterface::COLLECTION, ResolutionInterface::COLLECTION];

        if (in_array(@$news["object"]["type"], $DDATypes)) {
            return $this->getModelAuthorisation()->canEditItem($userId, $news["object"]["type"], $news["object"]["id"], null, null, true);
        }

        $what = (@$news["verb"] == "create") ? "object" : "target";
        $parentId = @$news[$what]["id"];
        $parentType = @$news[$what]["type"];
        if (@$deleteProcess) {
            return $this->getModelAuthorisation()->isOpenEdition($parentId, $parentType);
        }
        if ($this->getModelAuthorisation()->isElementAdmin($parentId, $parentType, $userId)) {
            return true;
        }
        if (count($this->getModelAuthorisation()->listAdmins($parentId, $parentType, false)) == 0 && $this->getModelAuthorisation()->canParticipate($userId, $parentType, $parentId)) {
            return true;
        }
    }

    /**
     * Ajoute des paramètres pour les nouvelles à partir d'ActivityPub.
     *
     * @param string $type Le type de l'objet ActivityPub.
     * @param string $id L'identifiant de l'objet ActivityPub.
     * @param array $params Les paramètres à ajouter.
     * @return array Les paramètres mis à jour.
     */
    public function addParamsForNewsFromActivitypub(string $type, string $id, array $params): array
    {
        $connectedUserId = $this->session->getUserId();
        $connectedUser = $this->getModelPerson()->getById($connectedUserId);
        $connectedUser_ap_follows = [];

        $activitypubEnable = $this->getModelPreference()->isActivitypubActivate(@$connectedUser["preferences"]);
        if ($type == PersonInterface::COLLECTION && $id == $connectedUserId && $activitypubEnable) {
            $activitypubParams = [
                "type" => "activitypub",
            ];
            // get project contributor and followers
            if (isset($connectedUser['links']) && isset($connectedUser['links']['projects'])) {
                foreach ($connectedUser['links']['projects'] as $key => $value) {
                    if (isset($value['activitypub'])) {
                        $prj = $this->getModelProject()->getById($key);
                        if (isset($prj['links']['activitypub'])) {
                            if (isset($prj["links"]["activitypub"]["followers"])) {
                                foreach ($prj["links"]["activitypub"]["followers"] as $followers) {
                                    if (isset($followers["invitorId"])) {
                                        $connectedUser_ap_follows[] = $followers["invitorId"];
                                    }
                                }
                            }
                            if (isset($prj["links"]["activitypub"]["contributors"])) {
                                foreach ($prj["links"]["activitypub"]["contributors"] as $contributor) {
                                    if (isset($contributor["invitorId"])) {
                                        $connectedUser_ap_follows[] = $contributor["invitorId"];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (isset($params["sharedBy.updated"])) {
                $activitypubParams["sharedBy.updated"] = $params["sharedBy.updated"];
            }

            if (isset($connectedUser["links"]["activitypub"]["follows"])) {
                foreach ($connectedUser["links"]["activitypub"]["follows"] as $follow) {
                    $connectedUser_ap_follows[] = $follow["invitorId"];
                }
            }

            $activitypubParams["sharedBy.id"] = [
                '$in' => $connectedUser_ap_follows,
            ];

            $params = [
                '$or' => [
                    $params,
                    $activitypubParams,
                ],
            ];
        }

        return $params;
    }
}
