<?php

namespace PixelHumain\Models\Activitypub;

use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\TypeResolver;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\NewsTraitInterface;
use PixelHumain\Models\Traits\NewsTrait;

class ActivitypubObject extends BaseModel implements ActivitypubObjectInterface, NewsTraitInterface, UtilsTraitInterface, ActivitypubTranslatorTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use NewsTrait;

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivitypubTranslatorTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Saves the activitypub object.
     *
     * @param AbstractActorInterface $creator The creator of the object.
     * @param array $targets The targets of the object.
     * @param AbstractObjectInterface $object The object to be saved.
     * @param mixed $payload Additional payload data.
     * @return string|null The ID of the saved object, or null if saving failed.
     */
    public function save(AbstractActorInterface $creator, array $targets, AbstractObjectInterface $object, $payload = null): ?string
    {
        $uuid = uniqid();

        //set object id if it doesn't have one
        if (! $object->get("id")) {
            $object->set("id", $this->getModelUtils()->createUrl("api/activitypub/object/id/" . $uuid));
        } else {
            $res = $this->db->findOne(self::COLLECTION, [
                "object.id" => $object->get("id"),
            ]);
            if ($res) {
                $query = @$payload ? [
                    "object" => $object->toArray(),
                    "payload" => is_array($payload) ? $this->getModelUtils()->addOrRemoveIntoArray((@$res['payload'] && is_array($res['payload'])) ? $res['payload'] : [], $payload) : $payload,
                ] : [
                    "object" => $object->toArray(),
                ];
                $this->db->update(self::COLLECTION, [
                    "object.id" => $object->get("id"),
                ], [
                    '$set' => $query,
                ]);

                return $res["uuid"];
            }
        }

        //filter object to save
        $allowedObjectTypes = array_merge(TypeResolver::$objectTypes, TypeResolver::$actorTypes);
        $isAllowedObject = in_array($object->get("type"), $allowedObjectTypes);
        $isNotExist = ! self::getObjectById($object->get("id"));
        if ($isNotExist && $isAllowedObject) {
            $data = [
                "uuid" => $uuid,
                "createAt" => $this->db->MongoDate(time()),
                "object" => $object->toArray(),
            ];

            if (in_array($object->get("type"), TypeResolver::$objectTypes)) {
                $data["creator"] = $creator->get("id");
                $data["targets"] = $this->getModelUtils()->retrieveAllIdsFromObjects($targets);
            }

            if ($payload) {
                $data["payload"] = $payload;
            }

            $this->db->insert(self::COLLECTION, $data);

            return $uuid;
        }

        return null;
    }

    /**
     * Retrieves an object by its ID.
     *
     * @param string $id The ID of the object.
     * @return AbstractObjectInterface|null The object with the specified ID, or null if not found.
     */
    public function getObjectById(string $id): ?AbstractObjectInterface
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "object.id" => $id,
        ]);
        if (! $res) {
            return null;
        }
        return $this->getModelTypeAp()->createFromAnyValue($res["object"]);
    }

    /**
     * Retrieve an object by its ID and return it as an array.
     *
     * @param string $id The ID of the object.
     * @return array|null The object as an array or null if not found.
     */
    public function getObjectByIdAsArray(string $id): ?array
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "object.id" => $id,
        ]);
        if (! $res) {
            return null;
        }
        return $res["object"];
    }

    /**
     * Retrieves an object by its UUID.
     *
     * @param string $uuid The UUID of the object.
     * @return AbstractObjectInterface|null The object with the specified UUID, or null if not found.
     */
    public function getObjectByUUID(string $uuid): ?AbstractObjectInterface
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "uuid" => $uuid,
        ]);
        if (! $res) {
            return null;
        }
        return $this->getModelTypeAp()->createFromAnyValue($res["object"]);
    }

    /**
     * Retrieves the replies of a note.
     *
     * @param string $noteId The ID of the note.
     * @param int|null $index The index of the first reply to retrieve (optional).
     * @param int $limit The maximum number of replies to retrieve (default: 5).
     * @return array An array containing the replies of the note.
     */
    public function getRepliesOfNote(string $noteId, ?int $index = null, int $limit = 5): array
    {
        $res = [];
        if ($index) {
            $res = $this->db->findAndSortAndLimitAndIndex(
                self::COLLECTION,
                [
                    'object.inReplyTo' => $noteId,
                ],
                [
                    "createAt" => -1,
                ],
                $limit,
                $index
            );
        } else {
            $res = $this->db->findAndSort(self::COLLECTION, [
                'object.inReplyTo' => $noteId,
            ], [
                "createAt" => -1,
            ]);
        }

        $replies = [];
        foreach ($res as $row) {
            $replies[] = $this->getModelTypeAp()->createFromAnyValue($row["object"]);
        }

        return $replies;
    }

    /**
     * Retrieves the comments for a news article.
     *
     * @param string $newsId The ID of the news article.
     * @return array An array of comments for the news article.
     */
    public function getNewsComments(string $newsId): array
    {
        $res = $this->db->findOneById(NewsInterface::CONTROLLER, $newsId);
        if (! $res && ! isset($res["objectUUID"])) {
            return [];
        }

        $object = self::getObjectByUUID($res["objectUUID"]);
        if (! $object) {
            return [];
        }

        $res = $this->db->findAndSort(self::COLLECTION, [
            'object.inReplyTo' => $object->get("id"),
        ], [
            "createAt" => -1,
        ]);

        $comments = [];
        foreach ($res as $row) {
            $comment = $this->getModelTypeAp()->createFromAnyValue($row["object"]);
            $comment = $this->getModelActivitypubTranslator()->noteToNewsMedia($comment);
            $comment["created"] = $row["createAt"];
            $comment["uuid"] = $row["uuid"];

            $comments[] = $comment;
        }

        return $comments;
    }

    /**
     * Retrieves an object by its news ID.
     *
     * @param string $newsId The news ID.
     * @return AbstractObjectInterface|null The object corresponding to the news ID, or null if not found.
     */
    public function getObjectByNewsId(string $newsId): ?AbstractObjectInterface
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "payload.newsId" => $newsId,
        ]);
        if (! $res) {
            return null;
        }
        return $this->getModelTypeAp()->createFromAnyValue($res["object"]);
    }

    /**
     * Removes an object by its ID.
     *
     * @param string $objectId The ID of the object to be removed.
     * @return void
     */
    public function removeByObjectId(string $objectId): void
    {
        $this->db->remove(self::COLLECTION, [
            "object.id" => $objectId,
        ]);
    }

    /**
     * Retrieves the notes associated with the activitypub object for a given user.
     *
     * @param string $userAdress The user's address.
     * @return array An array of notes.
     */
    public function getNotes(string $userAdress): array
    {
        $notes = [];
        $res = $this->db->findAndSort(self::COLLECTION, [
            "object.type" => "Note",
            "targets" => $userAdress,
        ], [
            "createAt" => -1,
        ]);

        foreach ($res as $item) {
            $notes[] = [
                "note" => $this->getModelTypeAp()->createFromAnyValue($item["object"]),
                "creator" => $this->getModelTypeAp()->createFromAnyValue($item["creator"]),
                "date" => $item["createAt"],
            ];
        }

        return $notes;
    }

    /**
     * Returns an array of projects for a given user.
     *
     * @param string $user The username of the user.
     * @return array An array of projects.
     */
    public function getProjects(string $user): array
    {
        $res = $this->db->findAndSort(self::COLLECTION, [
            "object.type" => "Project",
            "object.attributedTo" => $user,
        ], [
            "createAt" => -1,
        ]);
        return $res;
    }

    /**
     * Retrieves activities within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return array An array of activities within the specified date range.
     */
    public function getActivityByDateRange(string $startDate, string $endDate): array
    {
        $referDomain = Config::SCHEMA . '://' . Config::HOST;
        $matchStage = [
            '$match' => [
                'createAt' => [
                    '$gte' => $this->db->MongoDate(strtotime($startDate)),
                    '$lte' => $this->db->MongoDate(strtotime($endDate)),
                ],
            ],
        ];
        $groupStage = [
            '$group' => [
                '_id' => '$object.type',
                'documents' => [
                    '$push' => '$$ROOT',
                ],
            ],
        ];
        $pipeline = [$matchStage, $groupStage];
        ;
        $results = $this->db->aggregate(self::COLLECTION, $pipeline);
        $finalResult = [];
        $typesToEnsure = ['Note', 'Event', 'Project'];
        $categories = ['all', 'in', 'out'];
        foreach ($typesToEnsure as $type) {
            $finalResult[$type] = [];
            foreach ($categories as $category) {
                $finalResult[$type][$category] = [];
            }
        }
        foreach ($results['result'] as $result) {
            $type = $result['_id'];
            if ($type != 'Person' && $type != 'Group' && $type != 'Offer') {
                foreach ($result['documents'] as $document) {
                    if (strpos($document['object']['id'], $referDomain) !== false) {
                        $finalResult[$type]['out'][] = $document;
                    } else {
                        $finalResult[$type]['in'][] = $document;
                    }
                    $finalResult[$type]['all'][] = $document;
                }
            }
        }
        return $finalResult;
    }
}
