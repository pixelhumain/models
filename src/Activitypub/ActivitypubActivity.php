<?php

namespace PixelHumain\Models\Activitypub;

use Exception;
use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubObjectTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubObjectTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProjectTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProjectTrait;

class ActivitypubActivity extends BaseModel implements ActivitypubActivityInterface, PersonTraitInterface, OrganizationTraitInterface, ProjectTraitInterface, UtilsTraitInterface, ActivitypubObjectTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use OrganizationTrait;
    use ProjectTrait;

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivitypubObjectTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Saves an activity.
     *
     * @param ActivityInterface $activity The activity to be saved.
     * @param array $receivers The receivers of the activity.
     * @param mixed $payload The payload associated with the activity.
     * @return array The saved activity.
     */
    public function save(ActivityInterface $activity, array $receivers, $payload = null): array
    {
        $activityUUID = uniqid();

        //set activity id if it doesn't have one
        if (! $activity->get("id")) {
            $activity->setId($this->getModelUtils()->createUrl("api/activitypub/activity/id/" . $activityUUID));
        }

        //retrieve the actor of the activity
        $actor = $this->getModelTypeAp()->createFromAnyValue($activity->get("actor"));
        //retrieve the activity object
        $object = $this->getModelTypeAp()->createFromAnyValue($activity->get("object"));

        //save object
        $objectUUID = $this->getModelActivitypubObject()->save($actor, $receivers, $object, $payload);

        //simplify the object of the activity using just the id
        $activity->setObject($object->get("id"));

        $data = [
            "uuid" => $activityUUID,
            "from" => $actor->get("id"),
            "to" => $this->getModelUtils()->retrieveAllIdsFromObjects($receivers),
            "activity" => $activity->toArray(),
            "createAt" => $this->db->MongoDate(time()),
        ];
        if ($payload) {
            $data["payload"] = $payload;
        }
        $this->db->insert(self::COLLECTION, $data);

        $UUIDS = [
            "activity" => $activityUUID,
            "object" => $objectUUID,
        ];

        return $UUIDS;
    }

    /**
     * Retrieves an activity by its ID.
     *
     * @param string $id The ID of the activity.
     * @return array|bool The activity if it exists, false otherwise.
     */
    public function getByActivityId(string $id)
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "activity.id" => $id,
        ]);
        if (! $res) {
            return false;
        }
        return $res["activity"];
    }

    /**
     * Retrieves an activity by its object ID.
     *
     * @param string $id The object ID of the activity.
     * @return array|bool The activity if it exists, false otherwise.
     */
    public function getByActivityByObjectId(string $id)
    {
        $res = $this->db->findOne(self::COLLECTION, [
            "activity.object" => $id,
        ]);
        if (! $res) {
            return false;
        }
        return $res["activity"];
    }

    /**
     * Retrieves an activity by its UUID.
     *
     * @param string $uuid The UUID of the activity.
     * @return ActivityInterface|null The activity object if found, null otherwise.
     */
    public function getByUUID(string $uuid): ?ActivityInterface
    {
        $activity = $this->db->findOne(self::COLLECTION, [
            "uuid" => $uuid,
        ]);
        if (! $activity) {
            return null;
        }

        $activity = $this->getModelTypeAp()->createFromAnyValue($activity["activity"]);
        $activity->set("object", $this->getModelTypeAp()->createFromAnyValue($activity->get("object")));

        return $activity;
    }

    /**
     * Returns a link activity.
     *
     * @param string $linkType The type of link.
     * @param string $localUserId The ID of the local user.
     * @param AbstractActorInterface $invitor The invitor actor.
     * @param string $actorType The type of actor.
     * @return ActivityInterface The link activity.
     */
    public function getLinkActivity(string $linkType, string $localUserId, AbstractActorInterface $invitor, string $actorType): ActivityInterface
    {
        if ($actorType == "group") {
            $collection = OrganizationInterface::COLLECTION;
        } elseif ($actorType == "project") {
            $collection = ProjectInterface::COLLECTION;
        } else {
            $collection = PersonInterface::COLLECTION;
        }
        $user = $this->db->findOne($collection, [
            "_id" => $this->db->MongoId($localUserId),
            "links.activitypub." . $linkType => [
                '$elemMatch' => [
                    "invitorId" => $invitor->get("id"),
                ],
            ],
        ]);

        if (! $user) {
            throw new Exception("Link not found");
        }

        $link = null;
        foreach ($user["links"]["activitypub"][$linkType] as $follower) {
            if ($follower["invitorId"] == $invitor->get("id")) {
                $link = $follower;
                break;
            }
        }

        if (! $link) {
            throw new Exception("Link not found");
        }

        return $this->getByUUID($link["activityId"]);
    }

    /**
     * Returns an activity for linking a project.
     *
     * @param string $linkType The type of link.
     * @param AbstractActorInterface $subject The subject of the activity.
     * @param AbstractActorInterface $invitor The invitor of the activity.
     * @return ActivityInterface The activity for linking a project.
     */
    public function getLinkActivityProject(string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): ActivityInterface
    {
        $project = $this->db->findOne(ProjectInterface::COLLECTION, [
            "objectId" => $subject->get("id"),
        ]);
        if (! $project) {
            throw new Exception("Link not found");
        }

        $link = null;
        foreach ($project["links"]["activitypub"][$linkType] as $follower) {
            if ($follower["invitorId"] == $invitor->get("id")) {
                $link = $follower;
                break;
            }
        }

        if (! $link) {
            throw new Exception("Link not found");
        }

        return $this->getByUUID($link["activityId"]);
    }

    /**
     * Undocumented function
     *
     * @param string $elementType
     * @param string $linkTypes
     * @param AbstractActorInterface $subject
     * @param AbstractActorInterface $invitor
     * @return mixed
     */
    public function getElementLinkActivity(string $elementType, string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): ActivityInterface
    {
        $element = $this->db->findOne($elementType, [
            "objectId" => $subject->get("id"),
        ]);
        if (! $element) {
            throw new Exception("Link not found");
        }
        $link = null;
        foreach ($element["links"]["activitypub"][$linkType] as $el) {
            if ($el["invitorId"] == $invitor->get("id")) {
                $link = $el;
                break;
            }
        }

        if (! $link) {
            throw new Exception("Link not found");
        }

        return $this->getByUUID($link["activityId"]);
    }

    /**
     * Returns an array of all CC recipients for the given element type, link type, subject, invitor, and admin status.
     *
     * @param string $elementType The element type.
     * @param string $linkType The link type.
     * @param AbstractActorInterface $subject The subject.
     * @param AbstractActorInterface $invitor The invitor.
     * @param bool $isAdmin Whether the user is an admin or not.
     * @return array An array of all CC recipients.
     */
    public function getAllCCRecipients(string $elementType, string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor, bool $isAdmin = false): array
    {
        if ($elementType == PersonInterface::COLLECTION) {
            $element = $this->db->findOne($elementType, [
                "slug" => $subject->get("preferredUsername"),
            ]);
        } else {
            $element = $this->db->findOne($elementType, [
                "objectId" => $subject->get("id"),
            ]);
        }
        if (! $element) {
            throw new Exception("Link not found");
        }
        $links = [];
        if (is_string($linkType)) {
            if (isset($element["links"]["activitypub"])) {
                foreach ($element["links"]["activitypub"][$linkType] as $el) {
                    if ($isAdmin) {
                        if ($el["invitorId"] != $invitor->get("id") && isset($el["isAdmin"])) {
                            $links[] = $el['invitorId'];
                            break;
                        }
                    } else {
                        if ($el["invitorId"] != $invitor->get("id")) {
                            $links[] = $el['invitorId'];
                            break;
                        }
                    }
                }
            }
        } else {
            if (is_countable($linkType) ? count($linkType) : 0) {
                foreach ($linkType as $lt) {
                    if (isset($element["links"]["activitypub"])) {
                        foreach ($element["links"]["activitypub"][$linkType] as $el) {
                            if ($el["invitorId"] != $invitor->get("id")) {
                                $links[] = $el['invitorId'];
                                break;
                            }
                        }
                    }
                }
            }
        }
        return $links;
    }

    /**
     * Retrieves activities within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain to filter activities by.
     * @param bool $isLocalActivity Whether to include only local activities.
     * @param string $type The type of activities to retrieve.
     * @return array An array of activities within the specified date range.
     */
    public function getActivityByDateRange(string $startDate, string $endDate, string $domain, bool $isLocalActivity, string $type): array
    {
        if ($isLocalActivity) {
            if ($type == 'all') {
                $queryDate = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                    ],
                ];
            } else {
                $queryDate = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                        [
                            'activity.type' => ucfirst(strtolower($type)),
                        ],
                    ],
                ];
            }
        } else {
            $queryDate = $this->getQueryDate($type, $startDate, $endDate, $domain);
        }
        return $this->db->find(self::COLLECTION, $queryDate);
    }

    /**
     * Returns an array of instances interacting within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return array An array of instances interacting within the specified date range.
     */
    public function getInstanceInteractingByDateRange(string $startDate, string $endDate): array
    {
        return $this->db->distinct(self::COLLECTION, 'from', [
            'createAt' => [
                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                '$lte' => $this->db->MongoDate(strtotime($endDate)),
            ],
        ]);
    }

    /**
     * Retrieves interactions within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain of the interactions.
     * @param string $refdomain The reference domain for the interactions.
     * @return array An array of interactions within the specified date range.
     */
    public function getInteractionByDateRange(string $startDate, string $endDate, string $domain, string $refdomain): array
    {
        $referDomain = $refdomain ? Config::SCHEMA . '://' . $refdomain : Config::SCHEMA . '://' . Config::HOST;
        return $this->db->find(self::COLLECTION, [
            '$and' => [
                [
                    'createAt' => [
                        '$gte' => $this->db->MongoDate(strtotime($startDate)),
                        '$lte' => $this->db->MongoDate(strtotime($endDate)),
                    ],
                ],
                [
                    'from' => [
                        '$regex' => "^$referDomain",
                        '$options' => 'i',
                    ],
                ],
                [
                    'to' => [
                        '$elemMatch' => [
                            '$regex' => "^https:\/\/$domain",
                            '$options' => 'i',
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Retrieves activities by type and date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain of the activities.
     * @param string $refdomain The reference domain of the activities.
     * @return array An array of activities matching the specified criteria.
     */
    public function getActivityByTypeAndDateRange(string $startDate, string $endDate, string $domain, string $refdomain): array
    {
        $referDomain = $refdomain ? Config::SCHEMA . '://' . $refdomain : Config::SCHEMA . '://' . Config::HOST;
        $matchStage = [
            '$match' => [
                'createAt' => [
                    '$gte' => $this->db->MongoDate(strtotime($startDate)),
                    '$lte' => $this->db->MongoDate(strtotime($endDate)),
                ],
                'from' => [
                    '$regex' => "^$referDomain",
                    '$options' => 'i',
                ],
                'to' => [
                    '$elemMatch' => [
                        '$regex' => "^https:\/\/$domain",
                        '$options' => 'i',
                    ],
                ],
            ],
        ];
        $groupStage = [
            '$group' => [
                '_id' => '$activity.type',
                'documents' => [
                    '$push' => '$$ROOT',
                ],
            ],
        ];
        $pipeline = [$matchStage, $groupStage];
        ;
        return $this->db->aggregate(self::COLLECTION, $pipeline);
    }

    /**
     * Retrieves activities within a specified date range and of a specific type.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain to filter activities by.
     * @param string $type The type of activities to retrieve.
     * @param string|null $refDomain The reference domain to filter activities by (optional).
     * @return array An array of activities matching the specified criteria.
     */
    public function getActivityByDateRangeAndType(string $startDate, string $endDate, string $domain, string $type, ?string $refDomain = null): array
    {
        $activities = [];
        if ($type == 'all') {
            if ($refDomain != "all") {
                $queryDateForLocalActivity = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                        [
                            'to' => [
                                '$elemMatch' => [
                                    '$regex' => "^https:\/\/$refDomain",
                                    '$options' => 'i',
                                ],
                            ],
                        ],
                    ],
                ];
            } else {
                $queryDateForLocalActivity = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                    ],
                ];
            }
        } else {
            if ($refDomain != "all") {
                $queryDateForLocalActivity = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                        [
                            'activity.type' => ucfirst(strtolower($type)),
                        ],
                        [
                            'to' => [
                                '$elemMatch' => [
                                    '$regex' => "^https:\/\/$refDomain",
                                    '$options' => 'i',
                                ],
                            ],
                        ],
                    ],
                ];
            } else {
                $queryDateForLocalActivity = [
                    '$and' => [
                        [
                            'createAt' => [
                                '$gte' => $this->db->MongoDate(strtotime($startDate)),
                                '$lte' => $this->db->MongoDate(strtotime($endDate)),
                            ],
                        ],
                        [
                            'from' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                        [
                            'activity.type' => ucfirst(strtolower($type)),
                        ],
                    ],
                ];
            }
        }
        $activities["out"] = $this->db->find(self::COLLECTION, $queryDateForLocalActivity);

        $queryDateForIntercepted = $this->getQueryDate($type, $startDate, $endDate, $domain);
        $activities["in"] = $this->db->find(self::COLLECTION, $queryDateForIntercepted);

        return $activities;
    }

    /**
     * Returns an array of query dates based on the provided parameters.
     *
     * @param string $type The type of activity.
     * @param string $startDate The start date of the query.
     * @param string $endDate The end date of the query.
     * @param string $domain The domain for which the query is performed.
     * @return array The array of query dates.
     */
    private function getQueryDate(string $type, string $startDate, string $endDate, string $domain): array
    {
        if ($type == 'all') {
            $queryDate = [
                '$and' => [
                    [
                        'createAt' => [
                            '$gte' => $this->db->MongoDate(strtotime($startDate)),
                            '$lte' => $this->db->MongoDate(strtotime($endDate)),
                        ],
                    ],
                    [
                        'from' => [
                            '$not' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                    ],
                ],
            ];
        } else {
            $queryDate = [
                '$and' => [
                    [
                        'createAt' => [
                            '$gte' => $this->db->MongoDate(strtotime($startDate)),
                            '$lte' => $this->db->MongoDate(strtotime($endDate)),
                        ],
                    ],
                    [
                        'from' => [
                            '$not' => [
                                '$regex' => "^$domain",
                                '$options' => 'i',
                            ],
                        ],
                    ],
                    [
                        'activity.type' => ucfirst(strtolower($type)),
                    ],
                ],
            ];
        }
        return $queryDate;
    }
}
