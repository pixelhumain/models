<?php

namespace PixelHumain\Models\Activitypub\notification;

use PixelHumain\Models\Activitypub\notification\Interfaces\NotificationTransformerInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;
use PixelHumain\Models\BaseModel;

/**
 * Class NotificationTransformer
 *
 * This class is responsible for transforming activitypub notifications.
 */
class NotificationTransformer extends BaseModel implements NotificationTransformerInterface, TypeTraitInterface
{
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     *
     * @return void
     */
    protected function validateProperties(): void
    {
    }

    /**
     * Get the actor name from the activity.
     *
     * @param array $activity The activity array.
     * @return string The actor name.
     */
    public function getActorName($activity)
    {
        if (isset($activity['actor'])) {
            $actor = $activity['actor'];
            if (is_string($actor)) {
                return $actor;
            } elseif (is_array($actor) && isset($actor['name'])) {
                return $actor['name'];
            }
        }

        return '';
    }

    /**
     * Get the activity type from the activity.
     *
     * @param array $activity The activity array.
     * @return string The activity type.
     */
    public function getActivityType($activity)
    {
        if (isset($activity['type'])) {
            if (filter_var($activity['type'], FILTER_VALIDATE_URL)) {
                $atv = $this->getModeltypeAp()->createFromAnyValue($activity['type']);
                return $atv->get('type');
            } else {
                return $activity['type'];
            }
        }

        return '';
    }

    /**
     * Get the object type from the activity.
     *
     * @param array $activity The activity array.
     * @return string The object type.
     */
    public function getObjectType($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];

            if (is_string($object)) {
                if (filter_var($object, FILTER_VALIDATE_URL)) {
                    $obj = $this->getModeltypeAp()->createFromAnyValue($object);
                    return $obj->get('type');
                } else {
                    return $object;
                }
            } elseif (is_array($object) && isset($object['name'])) {
                return $object['type'];
            }
        }

        return '';
    }

    /**
     * Get the object name from the activity.
     *
     * @param array $activity The activity array.
     * @return string The object name.
     */
    public function getObjectName($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];
            if (is_string($object)) {
                return $object;
            } elseif (is_array($object) && isset($object['name'])) {
                return $object['name'];
            }
        }

        return '';
    }

    /**
     * Get the object id from the activity.
     *
     * @param array $activity The activity array.
     * @return string The object id.
     */
    public function getObjectId($activity)
    {
        if (isset($activity['object'])) {
            $object = $activity['object'];
            if (is_string($object)) {
                return $object;
            } elseif (is_array($object) && isset($object['id'])) {
                return $object['id'];
            }
        }

        return '';
    }
}
