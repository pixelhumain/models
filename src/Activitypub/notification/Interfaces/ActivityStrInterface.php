<?php

namespace PixelHumain\Models\Activitypub\notification\Interfaces;

interface ActivityStrInterface
{
    // activity
    public const ACCEPT_ACTIVITY = "Accept";

    // FOLLOW
    public const ACCEPT_FOLLOW_ACTIVITY = "AcceptFollow";

    public const ADD_ACTIVITY = "Add";

    public const ANNOUNCE_ACTIVITY = "Announce";

    public const ARRIVE_ACTIVITY = "Arrive";

    public const BLOCK_ACTIVITY = "Block";

    public const CREATE_ACTIVITY = "Create";

    public const DELETE_ACTIVITY = "Delete";

    public const DISLIKE_ACTIVITY = "Dislike";

    public const FLAG_ACTIVITY = "Flag";

    public const FOLLOW_ACTIVITY = "Follow";

    public const IGNORE_ACTIVITY = "Ignore";

    public const JOIN_ACTIVITY = "Join";

    public const LEAVE_ACTIVITY = "Leave";

    public const LIKE_ACTIVITY = "Like";

    public const LISTEN_ACTIVITY = "Listen";

    public const MOVE_ACTIVITY = "Move";

    public const OFFER_ACTIVITY = "Offer";

    public const PROJECT_ACTIVITY = "Project";

    public const QUESTION_ACTIVITY = "Question";

    public const READ_ACTIVITY = "Read";

    // Reject
    public const REJECT_ACTIVITY = "Reject";

    public const REJECT_FOLLOW_ACTIVITY = 'RejectFollow';

    public const REMOVE_ACTIVITY = "Remove";

    public const REMOVE_FOLLOW_ACTIVITY = "RemoveFollow";

    public const TENTATIVE_ACCEPT_ACTIVITY = "TentativeAccept";

    public const TENTATIVE_REJECT_ACTIVITY = "TentativeReject";

    public const TRAVEL_ACTIVITY = "Travel";

    public const UNDO_ACTIVITY = "Undo";

    // Follow
    public const UNDO_FOLLOW_ACTIVITY = "UndoFollowActivity";

    public const UPDATE_ACTIVITY = "Update";

    public const VIEW_ACTIVITY = "View";

    // actor
    public const APPLICATION_ACTOR = "Application";

    public const GROUP_ACTOR = "Group";

    public const ORGANIZATION_ACTOR = "Organization";

    public const PERSON_ACTOR = "Person";

    public const PROJECT_ACTOR = "Project";

    public const SERVICE_ACTOR = "Service";

    // object
    public const ARTICLE_OBJECT = "Article";

    public const AUDIO_OBJECT = "Audio";

    public const DOCUMENT_OBJECT = "Document";

    public const EVENT_OBJECT = "Event";

    public const IMAGE_OBJECT = "Image";

    public const MENTION_OBJECT = "Mention";

    public const NOTE_OBJECT = "Note";

    public const PAGE_OBJECT = "Page";

    public const PLACE_OBJECT = "Place";

    public const PROFILE_OBJECT = "Profile";

    public const PROJECT_OBJECT = "Project";

    public const RELATIONSHIP_OBJECT = "Relationship";

    public const TOMBSTONE_OBJECT = "Tombstone";

    public const VIDEO_OBJECT = "Video";

    public const INVITE_BECOME_ADMIN_PROJECT = 'InviteToBecomeAdminProject';

    public const INVITE_FOLLOW_ACTIVITY = "InviteFollow";

    public const INVITE_REJECT_ACTIVITY = "InviteReject";

    public const UNDO_FOLLOW_PROJECT_ACTIVITY = "UndoFollowProject";
}
