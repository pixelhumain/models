<?php

namespace PixelHumain\Models\Activitypub\notification;

use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\notification\Interfaces\ActivityNotificationInterface;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\NotificationTransformerTraitInterface;
use PixelHumain\Models\Activitypub\notification\Traits\NotificationTransformerTrait;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\ActivityStreamTrait;
use PixelHumain\Models\Traits\ActStrTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActStrTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\PreferenceTrait;

class ActivityNotification extends BaseModel implements ActivityNotificationInterface, PreferenceTraitInterface, UtilsTraitInterface, ActStrTraitInterface, ActivityStreamTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, NotificationTransformerTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PreferenceTrait;
    use ActStrTrait;
    use ActivityStreamTrait;

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use NotificationTransformerTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    private function getLocalUrlFromObjectId($object): string
    {
        $type = PersonInterface::COLLECTION;
        if ($object->get('type') == 'Event') {
            $type = EventInterface::COLLECTION;
        } elseif ($object->get('type') == 'Project') {
            $type = ProjectInterface::COLLECTION;
        } elseif ($object->get('type') == 'Note') {
            $type = NewsInterface::COLLECTION;
        } else {
            if (! empty($object->get('target'))) {
                $target = $this->getModeltypeAp()->createFromAnyValue($object->get('target'));
                if ($target->get('type') == "Project") {
                    $type = ProjectInterface::COLLECTION;
                }
            }
        }
        if (! empty($object->get('target'))) {
            $target = $this->getModeltypeAp()->createFromAnyValue($object->get('target'));
            $element = $this->db->findOne($type, [
                "objectId" => $target->get("id"),
            ]);
        } else {
            $element = $this->db->findOne($type, [
                "objectId" => $object->get("id"),
            ]);
        }

        if (! $element) {
            $url = "";
        } else {
            if ($type == EventInterface::COLLECTION) {
                $url = "agenda";
            } else {
                $url = "page.type." . $type . ".id." . $element['_id'];
            }
        }
        return $url;
    }

    public function send($activity, $actor, $targets = null)
    {
        $activityArray = $activity->toArray();
        $object = $this->getModeltypeAp()->createFromAnyValue($activityArray['object']);

        $asParam = [
            "type" => "activitypub",
            "author" => [
                'id' => $actor->get('id'),
                'type' => $actor->get('type'),
                'name' => $actor->get('name'),
                'profilThumbImageUrl' => $actor->get('profilThumbImageUrl'),
            ],
            "verb" => $this->getModelNotificationTransformer()->getActivityType($activityArray),
            "object" => [
                "type" => $this->getModelNotificationTransformer()->getObjectType($activityArray),
                "id" => $object->get('id'),
            ],
        ];
        $actionMsg = $activityArray["summary"] ?? "Activité inconnue ...";
        $usersToNotify = [];
        if (isset($targets) && (is_countable($targets) ? count($targets) : 0) > 0) {
            foreach ($targets as $target) {
                $user = $this->db->findOne(PersonInterface::COLLECTION, [
                    "username" => $target->get("preferredUsername"),
                ]);
                if ($user && isset($user["preferences"]) && $this->getModelPreference()->isActivitypubActivate($user["preferences"])) {
                    $usersToNotify[] = $user;
                }
            }
        }
        if (isset($activityArray["cc"]) && (is_countable($activityArray['cc']) ? count($activityArray['cc']) : 0) > 0) {
            foreach ($activityArray['cc'] as $cc) {
                if ($this->getModelUtils()->getWebsiteDomain($cc) == Config::HOST) {
                    $usersToNotify[] = $this->db->findOne(PersonInterface::COLLECTION, [
                        'username' => $this->getModelUtils()->getLinkToUsername($cc),
                    ]);
                }
            }
        }
        if ($object->get('type') == "Follow" || $object->get('type') == "Accept") {
            $usersToNotify[] = $this->db->findOne(PersonInterface::COLLECTION, [
                'username' => $this->getModelUtils()->getLinkToUsername($object->get('actor')),
            ]);
        }
        foreach ($usersToNotify as $user) {
            $asParam['id'] = (string) $user['_id'];
            $stream = $this->getModelActStr()->buildEntry($asParam);
            $stream["notify"] = $this->getModelActivityStream()->addNotification([
                "persons" => [
                    (string) $user['_id'] => [
                        "isUnread" => true,
                        "isUnseen" => true,
                    ],
                ],
                "label" => $actionMsg,
                "icon" => $this->getModelUtils()->getWebsiteFavicon($object->get('id')),
                "url" => $this->getModelActivityNotification()->getLocalUrlFromObjectId($object),
            ]);
            $this->getModelActivityStream()->addEntry($stream);
        }
    }
}
