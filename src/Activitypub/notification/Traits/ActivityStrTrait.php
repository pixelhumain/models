<?php

namespace PixelHumain\Models\Activitypub\notification\Traits;

use Exception;
use PixelHumain\Models\Activitypub\notification\Interfaces\ActivityStrInterface;

trait ActivityStrTrait
{
    protected ?ActivityStrInterface $activityStr = null;

    /**
     * Set the activityStr.
     *
     * @param ActivityStrInterface $activityStr The activityStr object to set.
     * @return void
     */
    public function setModelActivityStr(ActivityStrInterface $activityStr): void
    {
        $this->activityStr = $activityStr;
    }

    /**
     * Gets the activityStr.
     *
     * @return ActivityStrInterface The activityStr.
     */
    public function getModelActivityStr(): ActivityStrInterface
    {
        if ($this->activityStr === null) {
            throw new Exception(ActivityStrInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activityStr;
    }
}
