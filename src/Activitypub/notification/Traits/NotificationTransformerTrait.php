<?php

namespace PixelHumain\Models\Activitypub\notification\Traits;

use Exception;
use PixelHumain\Models\Activitypub\notification\Interfaces\NotificationTransformerInterface;

trait NotificationTransformerTrait
{
    protected ?NotificationTransformerInterface $notificationTransformer = null;

    /**
     * Set the notificationTransformer.
     *
     * @param NotificationTransformerInterface $notificationTransformer The notificationTransformer object to set.
     * @return void
     */
    public function setModelNotificationTransformer(NotificationTransformerInterface $notificationTransformer): void
    {
        $this->notificationTransformer = $notificationTransformer;
    }

    /**
     * Gets the notificationTransformer.
     *
     * @return NotificationTransformerInterface The notificationTransformer.
     */
    public function getModelNotificationTransformer(): NotificationTransformerInterface
    {
        if ($this->notificationTransformer === null) {
            throw new Exception(NotificationTransformerInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->notificationTransformer;
    }
}
