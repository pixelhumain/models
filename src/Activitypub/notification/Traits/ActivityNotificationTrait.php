<?php

namespace PixelHumain\Models\Activitypub\notification\Traits;

use Exception;
use PixelHumain\Models\Activitypub\notification\Interfaces\ActivityNotificationInterface;

trait ActivityNotificationTrait
{
    protected ?ActivityNotificationInterface $activityNotification = null;

    /**
     * Set the activityNotification.
     *
     * @param ActivityNotificationInterface $activityNotification The activityNotification object to set.
     * @return void
     */
    public function setModelActivityNotification(ActivityNotificationInterface $activityNotification): void
    {
        $this->activityNotification = $activityNotification;
    }

    /**
     * Gets the activityNotification.
     *
     * @return ActivityNotificationInterface The activityNotification.
     */
    public function getModelActivityNotification(): ActivityNotificationInterface
    {
        if ($this->activityNotification === null) {
            throw new Exception(ActivityNotificationInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activityNotification;
    }
}
