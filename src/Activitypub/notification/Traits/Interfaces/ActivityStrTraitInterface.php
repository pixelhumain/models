<?php

namespace PixelHumain\Models\Activitypub\notification\Traits\Interfaces;

use PixelHumain\Models\Activitypub\notification\Interfaces\ActivityStrInterface;

interface ActivityStrTraitInterface
{
    /**
     * Set the activityStr.
     *
     * @param ActivityStrInterface $activityStr The activityStr object to set.
     * @return void
     */
    public function setModelActivityStr(ActivityStrInterface $activityStr): void;

    /**
     * Gets the activityStr.
     *
     * @return ActivityStrInterface The activityStr.
     */
    public function getModelActivityStr(): ActivityStrInterface;
}
