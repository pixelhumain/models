<?php

namespace PixelHumain\Models\Activitypub\notification\Traits\Interfaces;

use PixelHumain\Models\Activitypub\notification\Interfaces\NotificationTransformerInterface;

interface NotificationTransformerTraitInterface
{
    /**
     * Set the notificationTransformer.
     *
     * @param NotificationTransformerInterface $notificationTransformer The notificationTransformer object to set.
     * @return void
     */
    public function setModelNotificationTransformer(NotificationTransformerInterface $notificationTransformer): void;

    /**
     * Gets the notificationTransformer.
     *
     * @return NotificationTransformerInterface The notificationTransformer.
     */
    public function getModelNotificationTransformer(): NotificationTransformerInterface;
}
