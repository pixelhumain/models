<?php

namespace PixelHumain\Models\Activitypub\type\Interfaces;

interface TypeResolverInterface
{
    /**
     * Get the class name for the given type.
     *
     * @param string $type The type to resolve.
     * @return string The fully qualified class name.
     */
    public static function getClass(string $type): string;

    /**
     * Check if the given type is an anonymous type.
     *
     * @param string $type The type to check.
     * @return bool True if the type is anonymous, false otherwise.
     */
    public static function isAnonymousType(string $type): bool;
}
