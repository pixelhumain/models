<?php

namespace PixelHumain\Models\Activitypub\type\Interfaces;

use PixelHumain\Models\Activitypub\type\core\Interfaces\AbstractActivityInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;

interface TypeInterface
{
    /**
     * Create a new instance of the specified type with the given attributes.
     *
     * @param array|string $type The type of the object to create.
     * @param array $attributes The attributes to set on the object.
     * @return AbstractObjectInterface|AbstractActivityInterface|AbstractActorInterface The newly created object.
     */
    public function create($type, array $attributes = []);

    /**
     * Creates an instance of Type from any value.
     *
     * @param mixed $value The value to create the Type from.
     * @return AbstractObjectInterface|AbstractActivityInterface|AbstractActorInterface The newly created object.
     */
    public function createFromAnyValue($value);
}
