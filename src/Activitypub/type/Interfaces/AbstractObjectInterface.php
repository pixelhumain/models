<?php

namespace PixelHumain\Models\Activitypub\type\Interfaces;

interface AbstractObjectInterface
{
    /**
     * Set the context of the object.
     *
     * @param mixed $value The value to set as the context.
     * @return void
     */
    public function set_context($value): void;

    /**
     * Set the value of a extended property.
     *
     * @param string $name The name of the property.
     * @param mixed $value The value of the property.
     * @return void
     */
    public function set_extend_props(string $name, $value): void;

    /**
     * Set the value of a property.
     *
     * @param string $name The name of the property.
     * @param mixed $value The value to set.
     * @return self Returns the instance of the object.
     */
    public function set(string $name, $value): self;

    /**
     * Set multiple values for the object.
     *
     * @param array $values The array of values to set.
     * @return void
     */
    public function setMultiple(array $values): void;

    /**
     * Get the value of a property by name.
     *
     * @param string $name The name of the property.
     * @return mixed The value of the property.
     */
    public function get(string $name);

    /**
     * Converts the object to an array representation.
     *
     * @return array The array representation of the object.
     */
    public function toArray(): array;
}
