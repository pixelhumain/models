<?php

namespace PixelHumain\Models\Activitypub\type;

use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;

abstract class AbstractObject implements AbstractObjectInterface
{
    /**
     * Les traits activitypub
     */
    use TypeTrait;

    /**
     * This variable represents the context of the object.
     * It is set to "https://www.w3.org/ns/activitystreams" by default.
     *
     * @var string|array|null
     */
    protected $_context = "https://www.w3.org/ns/activitystreams";

    /**
     * This property holds additional properties that are not defined by the ActivityPub specification.
     *
     * @var array
     */
    protected array $_extend_props = [];

    /**
     * Set the context of the object.
     *
     * @param mixed $value The value to set as the context.
     * @return void
     */
    public function set_context($value): void
    {
        $this->set("_context", $value);
    }

    /**
     * Set the value of a extended property.
     *
     * @param string $name The name of the property.
     * @param mixed $value The value of the property.
     * @return void
     */
    public function set_extend_props(string $name, $value): void
    {
        $this->_extend_props[$name] = $this->transform($value);
    }

    /**
     * Transforms the given value.
     *
     * @param mixed $value The value to transform.
     * @return mixed The transformed value.
     */
    private function transform($value)
    {
        if (is_array($value)) {
            if (isset($value["type"])) {
                // Crée un objet Type si le tableau a une clé "type"
                return $this->getModelTypeAp()->create($value);
            } elseif ($this->isSequential($value)) {
                // Si le tableau est séquentiel, applique la transformation à chaque élément
                return array_map(
                    fn ($item) => is_array($item) && isset($item["type"])
                        ? $this->getModelTypeAp()->create($item)
                        : $item,
                    $value
                );
            } else {
                // Retourne le tableau non modifié si associatif
                return $value;
            }
        } else {
            // Retourne la valeur non modifiée si elle n'est pas un tableau
            return $value;
        }
    }

    /**
     * Set the value of a property.
     *
     * @param string $name The name of the property.
     * @param mixed $value The value to set.
     * @return self Returns the instance of the object.
     */
    public function set(string $name, $value): self
    {
        if (property_exists($this, $name)) {
            $this->$name = $this->transform($value);
        } elseif ($name == "@context") {
            $this->_context = $value;
        } else {
            $this->_extend_props[$name] = $this->transform($value);
        }

        return $this;
    }

    /**
     * Set multiple values for the object.
     *
     * @param array $values The array of values to set.
     * @return void
     */
    public function setMultiple(array $values): void
    {
        foreach ($values as $name => $value) {
            $this->set((string) $name, $value);
        }
    }

    /**
     * Get the value of a property by name.
     *
     * @param string $name The name of the property.
     * @return mixed The value of the property.
     */
    public function get(string $name)
    {
        return property_exists($this, $name) ? $this->$name : null;
    }

    /**
     * Converts the object to an array representation.
     *
     * @return array The array representation of the object.
     */
    public function toArray(): array
    {
        $allProperties = array_merge(get_object_vars($this), $this->_extend_props);
        unset($allProperties["_extend_props"], $allProperties["_context"]);

        $output = [];
        if (! is_null($this->_context)) {
            $output["@context"] = $this->_context;
        }

        foreach ($allProperties as $name => $value) {
            $output[$name] = $this->processValue($value);
        }

        return $output;
    }

    /**
     * Process the given value.
     *
     * @param mixed $value The value to be processed.
     * @return mixed The processed value.
     */
    private function processValue($value)
    {
        if (is_null($value)) {
            return null;
        } elseif ($value instanceof self) {
            return $value->toArray();
        } elseif (is_array($value)) {
            return $this->processArray($value);
        } else {
            return $value;
        }
    }

    /**
     * Process an array value.
     *
     * @param array $value The array to be processed.
     * @return array The processed array.
     */
    private function processArray(array $value): array
    {
        if ($this->isSequential($value)) {
            return array_map([$this, 'processValue'], $value);
        }
        return $value;
    }

    /**
     * Check if an array is sequential.
     *
     * @param array $array The array to check.
     * @return bool Returns true if the array is sequential, false otherwise.
     */
    private function isSequential(array $array): bool
    {
        return array_keys($array) === range(0, count($array) - 1);
    }
}
