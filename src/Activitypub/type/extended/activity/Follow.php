<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Follow extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Follow';
}
