<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\IntransitiveActivity;

class Arrive extends IntransitiveActivity
{
    /**
     * @var string
     */
    protected $type = 'Arrive';
}
