<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

class Block extends Ignore
{
    /**
     * @var string
     */
    protected $type = 'Block';
}
