<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Update extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Update';
}
