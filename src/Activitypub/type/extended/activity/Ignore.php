<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Ignore extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Ignore';
}
