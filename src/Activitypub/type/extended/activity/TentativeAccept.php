<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

class TentativeAccept extends Accept
{
    /**
     * @var string
     */
    protected $type = 'TentativeAccept';
}
