<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\IntransitiveActivity;

class Travel extends IntransitiveActivity
{
    /**
     * @var string
     */
    protected $type = 'Travel';
}
