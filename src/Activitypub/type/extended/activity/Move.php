<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Move extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Move';
}
