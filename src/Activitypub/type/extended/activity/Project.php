<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Project extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Offer';
}
