<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Announce extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Announce';
}
