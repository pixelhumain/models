<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Undo extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Undo';
}
