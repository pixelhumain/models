<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Read extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Read';
}
