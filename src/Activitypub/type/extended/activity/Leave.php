<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Leave extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Leave';
}
