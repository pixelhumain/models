<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class View extends Activity
{
    /**
     * @var string
     */
    protected $type = 'View';
}
