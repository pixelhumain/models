<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

use PixelHumain\Models\Activitypub\type\core\Activity;

class Listen extends Activity
{
    /**
     * @var string
     */
    protected $type = 'Listen';
}
