<?php

namespace PixelHumain\Models\Activitypub\type\extended\activity;

class Invite extends Offer
{
    /**
     * @var string
     */
    protected $type = 'Invite';
}
