<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Note extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Note';
}
