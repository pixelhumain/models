<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Document extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Document';
}
