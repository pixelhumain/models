<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Tombstone extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Tombstone';

    /**
     * The type of the object that was deleted.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-formertype
     *
     * @var string|null
     */
    protected $formerType;

    /**
     * A timestamp for when the object was deleted.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-deleted
     *
     * @var string|null xsd:dateTime formatted
     */
    protected $deleted;

    public function setFormerType($value)
    {
        $this->set("formerType", $value);
    }

    public function setDeleted($value)
    {
        $this->set("deleted", $value);
    }
}
