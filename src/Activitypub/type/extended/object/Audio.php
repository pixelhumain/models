<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

class Audio extends Document
{
    /**
     * @var string
     */
    protected $type = 'Audio';
}
