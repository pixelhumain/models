<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\Link;

class Mention extends Link
{
    /**
     * @var string
     */
    protected $type = 'Mention';
}
