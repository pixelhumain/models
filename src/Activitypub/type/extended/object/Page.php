<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

class Page extends Document
{
    /**
     * @var string
     */
    protected $type = 'Page';
}
