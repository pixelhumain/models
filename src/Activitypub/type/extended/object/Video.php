<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

class Video extends Document
{
    /**
     * @var string
     */
    protected $type = 'Video';
}
