<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

class Image extends Document
{
    /**
     * @var string
     */
    protected $type = 'Image';
}
