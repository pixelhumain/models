<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Place extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Place';

    /**
     * Indicates the accuracy of position coordinates on a Place
     * objects. Expressed in properties of percentage.
     * e.g. "94.0" means "94.0% accurate".
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-accuracy
     *
     * @var float|null
     */
    protected $accuracy;

    /**
     * The altitude of a place.
     * The measurement units is indicated using the units property.
     * If units is not specified, the default is assumed to be "m"
     * indicating meters.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-altitude
     *
     * @var float|null
     */
    protected $altitude;

    /**
     * The latitude of a place.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-latitude
     *
     * @var float|int|null
     */
    protected $latitude;

    /**
     * The longitude of a place.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-longitude
     *
     * @var float|int|null
     */
    protected $longitude;

    /**
     * The radius from the given latitude and longitude for a Place.
     * The units is expressed by the units property.
     * If units is not specified, the default is assumed to be "m"
     * indicating "meters".
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-radius
     *
     * @var float|int|null
     */
    protected $radius;

    /**
     * Specifies the measurement units for the radius and altitude
     * properties on a Place object.
     * If not specified, the default is assumed to be "m" for "meters".
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-units
     *
     * "cm" | " feet" | " inches" | " km" | " m" | " miles" | xsd:anyURI
     *
     * @var string
     */
    protected $units;

    public function setAccuracy($value)
    {
        $this->set("accuracy", $value);
    }

    public function setAltitude($value)
    {
        $this->set("altitude", $value);
    }

    public function setLatitude($value)
    {
        $this->set("latitude", $value);
    }

    public function setRadius($value)
    {
        $this->set("radius", $value);
    }

    public function setUnits($value)
    {
        $this->set("units", $value);
    }
}
