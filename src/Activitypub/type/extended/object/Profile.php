<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Profile extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Profile';

    /**
     * Identify the object described by the Profile.
     *
     * @var \ActivityPhp\Type\Core\ObjectType
     */
    protected $describes;

    public function setDescribes($value)
    {
        $this->set("describes", $value);
    }
}
