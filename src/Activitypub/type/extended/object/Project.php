<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

/**
 * https://forgefed.org/ns#components
 */
class Project extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Project';
}
