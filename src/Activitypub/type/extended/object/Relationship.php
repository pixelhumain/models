<?php

namespace PixelHumain\Models\Activitypub\type\extended\object;

use PixelHumain\Models\Activitypub\type\core\ObjectType;

class Relationship extends ObjectType
{
    /**
     * @var string
     */
    protected $type = 'Relationship';

    /**
     * One of the connected individuals.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-subject
     *
     * @var  string
     *     | \ActivityPhp\Type\Core\ObjectType
     *     | \ActivityPhp\Type\Core\Link
     *     | null
     */
    protected $subject;

    /**
     * The entity to which the subject is related.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object-term
     *
     * @var string
     *    | \ActivityPhp\Type\Core\Object
     *    | \ActivityPhp\Type\Core\Link
     *    | null
     */
    protected $object;

    /**
     * Type of relationship
     *
     * @var string URL
     */
    protected $relationship;

    public function setSubject($value)
    {
        $this->set("subject", $value);
    }

    public function setObject($value)
    {
        $this->set("object", $value);
    }

    public function setRelationship($value)
    {
        $this->set("relationship", $value);
    }
}
