<?php

namespace PixelHumain\Models\Activitypub\type\extended;

use PixelHumain\Models\Activitypub\type\core\ObjectType;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;

class AbstractActor extends ObjectType implements AbstractActorInterface
{
    /**
     * A reference to an ActivityStreams OrderedCollection comprised of
     * all the messages received by the actor.
     *
     * @see https://www.w3.org/TR/activitypub/#inbox
     *
     * @var \ActivityPhp\Type\Core\OrderedCollection
     *    | \ActivityPhp\Type\Core\OrderedCollectionPage
     *    | null
     */
    protected $inbox;

    /**
     * A reference to an ActivityStreams OrderedCollection comprised of
     * all the messages produced by the actor.
     *
     * @see https://www.w3.org/TR/activitypub/#outbox
     *
     * @var \ActivityPhp\Type\Core\OrderedCollection
     *    | \ActivityPhp\Type\Core\OrderedCollectionPage
     *    | null
     */
    protected $outbox;

    /**
     * A link to an ActivityStreams collection of the actors that this
     * actor is following.
     *
     * @see https://www.w3.org/TR/activitypub/#following
     *
     * @var string
     */
    protected $following;

    /**
     * A link to an ActivityStreams collection of the actors that
     * follow this actor.
     *
     * @see https://www.w3.org/TR/activitypub/#followers
     *
     * @var string
     */
    protected $followers;

    /**
     * A link to an ActivityStreams collection of objects this actor has
     * liked.
     *
     * @see https://www.w3.org/TR/activitypub/#liked
     *
     * @var string
     */
    protected $liked;

    /**
     * A list of supplementary Collections which may be of interest.
     *
     * @see https://www.w3.org/TR/activitypub/#streams-property
     *
     * @var array
     */
    protected $streams = [];

    /**
     * A short username which may be used to refer to the actor, with no
     * uniqueness guarantees.
     *
     * @see https://www.w3.org/TR/activitypub/#preferredUsername
     *
     * @var string|null
     */
    protected $preferredUsername;

    /**
     * A JSON object which maps additional typically server/domain-wide
     * endpoints which may be useful either for this actor or someone
     * referencing this actor. This mapping may be nested inside the
     * actor document as the value or may be a link to a JSON-LD
     * document with these properties.
     *
     * @see https://www.w3.org/TR/activitypub/#endpoints
     *
     * @var string|array|null
     */
    protected $endpoints;

    /**
     * It's not part of the ActivityPub protocol but it's a quite common
     * practice to handle an actor public key with a publicKey array:
     * [
     *     'id' => 'https://my-example.com/actor#main-key'
     *     'owner' => 'https://my-example.com/actor',
     *     'publicKeyPem' => '-----BEGIN PUBLIC KEY-----
     *                       MIIBI [...]
     *                       DQIDAQAB
     *                       -----END PUBLIC KEY-----'
     * ]
     *
     * @see https://www.w3.org/wiki/SocialCG/ActivityPub/Authentication_Authorization#Signing_requests_using_HTTP_Signatures
     *
     * @var string|array|null
     */
    protected $publicKey;

    /**
     * Set the inbox of the actor.
     *
     * @param mixed $value The value of the inbox.
     * @return void
     */
    public function setInbox($value)
    {
        $this->set("inbox", $value);
    }

    /**
     * Set the outbox of the actor.
     *
     * @param mixed $value The value of the outbox.
     * @return void
     */
    public function setOutbox($value)
    {
        $this->set("outbox", $value);
    }

    /**
     * Set the following collection of the actor.
     *
     * @param mixed $value The value of the following collection.
     * @return void
     */
    public function setFollowing($value)
    {
        $this->set("following", $value);
    }

    /**
     * Set the followers collection of the actor.
     *
     * @param mixed $value The value of the followers collection.
     * @return void
     */
    public function setFollowers($value)
    {
        $this->set("followers", $value);
    }

    /**
     * Set the liked collection of the actor.
     *
     * @param mixed $value The value of the liked collection.
     * @return void
     */
    public function setLiked($value)
    {
        $this->set("liked", $value);
    }

    /**
     * Set the streams of the actor.
     *
     * @param mixed $value The value of the streams.
     * @return void
     */
    public function setStreams($value)
    {
        $this->set("streams", $value);
    }

    /**
     * Set the preferred username of the actor.
     *
     * @param mixed $value The value of the preferred username.
     * @return void
     */
    public function setPreferredUsername($value)
    {
        $this->set("preferredUsername", $value);
    }

    /**
     * Set the endpoints of the actor.
     *
     * @param mixed $value The value of the endpoints.
     * @return void
     */
    public function setEndpoints($value)
    {
        $this->set("endpoints", $value);
    }

    /**
     * Set the public key of the actor.
     *
     * @param mixed $value The value of the public key.
     * @return void
     */
    public function setPublicKey($value)
    {
        $this->set("publicKey", $value);
    }
}
