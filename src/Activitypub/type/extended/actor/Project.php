<?php

namespace PixelHumain\Models\Activitypub\type\extended\actor;

use PixelHumain\Models\Activitypub\type\extended\AbstractActor;

/**
 * https://forgefed.org/ns#components
 */
class Project extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Project';
}
