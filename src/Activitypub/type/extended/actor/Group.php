<?php

namespace PixelHumain\Models\Activitypub\type\extended\actor;

use PixelHumain\Models\Activitypub\type\extended\AbstractActor;

class Group extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Group';
}
