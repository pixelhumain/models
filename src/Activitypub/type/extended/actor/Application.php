<?php

namespace PixelHumain\Models\Activitypub\type\extended\actor;

use PixelHumain\Models\Activitypub\type\extended\AbstractActor;

class Application extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Application';
}
