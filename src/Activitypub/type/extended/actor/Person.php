<?php

namespace PixelHumain\Models\Activitypub\type\extended\actor;

use PixelHumain\Models\Activitypub\type\extended\AbstractActor;

class Person extends AbstractActor
{
    /**
     * @var string
     */
    protected $type = 'Person';
}
