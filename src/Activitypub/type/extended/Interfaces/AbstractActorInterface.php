<?php

namespace PixelHumain\Models\Activitypub\type\extended\Interfaces;

use PixelHumain\Models\Activitypub\type\core\Interfaces\ObjectTypeInterface;

interface AbstractActorInterface extends ObjectTypeInterface
{
    /**
     * Set the inbox of the actor.
     *
     * @param mixed $value The value of the inbox.
     * @return void
     */
    public function setInbox($value);

    /**
     * Set the outbox of the actor.
     *
     * @param mixed $value The value of the outbox.
     * @return void
     */
    public function setOutbox($value);

    /**
     * Set the following collection of the actor.
     *
     * @param mixed $value The value of the following collection.
     * @return void
     */
    public function setFollowing($value);

    /**
     * Set the followers collection of the actor.
     *
     * @param mixed $value The value of the followers collection.
     * @return void
     */
    public function setFollowers($value);

    /**
     * Set the liked collection of the actor.
     *
     * @param mixed $value The value of the liked collection.
     * @return void
     */
    public function setLiked($value);

    /**
     * Set the streams of the actor.
     *
     * @param mixed $value The value of the streams.
     * @return void
     */
    public function setStreams($value);

    /**
     * Set the preferred username of the actor.
     *
     * @param mixed $value The value of the preferred username.
     * @return void
     */
    public function setPreferredUsername($value);

    /**
     * Set the endpoints of the actor.
     *
     * @param mixed $value The value of the endpoints.
     * @return void
     */
    public function setEndpoints($value);

    /**
     * Set the public key of the actor.
     *
     * @param mixed $value The value of the public key.
     * @return void
     */
    public function setPublicKey($value);
}
