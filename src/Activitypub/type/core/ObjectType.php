<?php

namespace PixelHumain\Models\Activitypub\type\core;

use PixelHumain\Models\Activitypub\type\AbstractObject;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ObjectTypeInterface;

class ObjectType extends AbstractObject implements ObjectTypeInterface
{
    /**
     * The object's unique global identifier
     *
     * @see https://www.w3.org/TR/activitypub/#obj-id
     *
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $type = 'Object';

    /**
     * A resource attached or related to an object that potentially
     * requires special handling.
     * The intent is to provide a model that is at least semantically
     * similar to attachments in email.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attachment
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $attachment;

    /**
     * One or more entities to which this object is attributed.
     * The attributed entities might not be Actors. For instance, an
     * object might be attributed to the completion of another activity.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-attributedto
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $attributedTo;

    /**
     * One or more entities that represent the total population of
     * entities for which the object can considered to be relevant.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-audience
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $audience;

    /**
     * The content or textual representation of the Object encoded as a
     * JSON string. By default, the value of content is HTML.
     * The mediaType property can be used in the object to indicate a
     * different content type.
     *
     * The content MAY be expressed using multiple language-tagged
     * values.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-content
     *
     * @var string|null
     */
    protected $content;

    /**
     * The context within which the object exists or an activity was
     * performed.
     * The notion of "context" used is intentionally vague.
     * The intended function is to serve as a means of grouping objects
     * and activities that share a common originating context or
     * purpose. An example could be all activities relating to a common
     * project or event.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-context
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $context;

    /**
     * The content MAY be expressed using multiple language-tagged
     * values.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-content
     *
     * @var array|null
     */
    protected $contentMap;

    /**
     * A simple, human-readable, plain-text name for the object.
     * HTML markup MUST NOT be included.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
     *
     * @var string|null xsd:string
     */
    protected $name;

    /**
     * The name MAY be expressed using multiple language-tagged values.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
     *
     * @var array|null rdf:langString
     */
    protected $nameMap;

    /**
     * The date and time describing the actual or expected ending time
     * of the object.
     * When used with an Activity object, for instance, the endTime
     * property specifies the moment the activity concluded or
     * is expected to conclude.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-endtime
     *
     * @var string|null
     */
    protected $endTime;

    /**
     * The entity (e.g. an application) that generated the object.
     *
     * @var string|null
     */
    protected $generator;

    /**
     * An entity that describes an icon for this object.
     * The image should have an aspect ratio of one (horizontal)
     * to one (vertical) and should be suitable for presentation
     * at a small size.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-icon
     *
     * @var string
     *    | Image
     *    | Link
     *    | array<Image>
     *    | array<Link>
     *    | null
     */
    protected $icon;

    /**
     * An entity that describes an image for this object.
     * Unlike the icon property, there are no aspect ratio
     * or display size limitations assumed.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-image-term
     *
     * @var string
     *    | Image
     *    | Link
     *    | array<Image>
     *    | array<Link>
     *    | null
     */
    protected $image;

    /**
     * One or more entities for which this object is considered a
     * response.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-inreplyto
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $inReplyTo;

    /**
     * One or more physical or logical locations associated with the
     * object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-location
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $location;

    /**
     * An entity that provides a preview of this object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-preview
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $preview;

    /**
     * The date and time at which the object was published
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-published
     *
     * @var string|null xsd:dateTime
     */
    protected $published;

    /**
     * A Collection containing objects considered to be responses to
     * this object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-replies
     *
     * @var string
     *    | Collection
     *    | Link
     *    | null
     */
    protected $replies;

    /**
     * The date and time describing the actual or expected starting time
     * of the object.
     * When used with an Activity object, for instance, the startTime
     * property specifies the moment the activity began
     * or is scheduled to begin.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-starttime
     *
     * @var string|null xsd:dateTime
     */
    protected $startTime;

    /**
     * A natural language summarization of the object encoded as HTML.
     * Multiple language tagged summaries MAY be provided.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-summary
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $summary;

    /**
     * The content MAY be expressed using multiple language-tagged
     * values.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-summary
     *
     * @var array<string>|null
     */
    protected $summaryMap;

    /**
     * One or more "tags" that have been associated with an objects.
     * A tag can be any kind of Object.
     * The key difference between attachment and tag is that the former
     * implies association by inclusion, while the latter implies
     * associated by reference.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-tag
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $tag;

    /**
     * The date and time at which the object was updated
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-updated
     *
     * @var string|null xsd:dateTime
     */
    protected $updated;

    /**
     * One or more links to representations of the object.
     *
     * @var string
     *    | array<string>
     *    | Link
     *    | array<Link>
     *    | null
     */
    protected $url;

    /**
     * An entity considered to be part of the public primary audience
     * of an Object
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-to
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $to;

    /**
     * An Object that is part of the private primary audience of this
     * Object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bto
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $bto;

    /**
     * An Object that is part of the public secondary audience of this
     * Object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-cc
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $cc;

    /**
     * One or more Objects that are part of the private secondary
     * audience of this Object.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-bcc
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | array<ObjectType>
     *    | array<Link>
     *    | null
     */
    protected $bcc;

    /**
     * The MIME media type of the value of the content property.
     * If not specified, the content property is assumed to contain
     * text/html content.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-mediatype
     *
     * @var string|null
     */
    protected $mediaType;

    /**
     * When the object describes a time-bound resource, such as an audio
     * or video, a meeting, etc, the duration property indicates the
     * object's approximate duration.
     * The value MUST be expressed as an xsd:duration as defined by
     * xmlschema11-2, section 3.3.6 (e.g. a period of 5 seconds is
     * represented as "PT5S").
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-duration
     *
     * @var string|null
     */
    protected $duration;

    /**
     * Intended to convey some sort of source from which the content
     * markup was derived, as a form of provenance, or to support
     * future editing by clients.
     *
     * @see https://www.w3.org/TR/activitypub/#source-property
     *
     * @var ObjectType
     */
    protected $source;

    /**
     * Set the ID of the object.
     *
     * @param mixed $value The ID value.
     * @return void
     */
    public function setId($value)
    {
        $this->set("id", $value);
    }

    /**
     * Set the type of the object.
     *
     * @param mixed $value The type value.
     * @return void
     */
    public function setType($value)
    {
        $this->set("type", $value);
    }

    /**
     * Set the attachment of the object.
     *
     * @param mixed $value The attachment value.
     * @return void
     */
    public function setAttachment($value)
    {
        $this->set("attachment", $value);
    }

    /**
     * Set the attributedTo of the object.
     *
     * @param mixed $value The attributedTo value.
     * @return void
     */
    public function setAttributedTo($value)
    {
        $this->set("attributedTo", $value);
    }

    /**
     * Set the audience of the object.
     *
     * @param mixed $value The audience value.
     * @return void
     */
    public function setAudience($value)
    {
        $this->set("audience", $value);
    }

    /**
     * Set the content of the object.
     *
     * @param mixed $value The content value.
     * @return void
     */
    public function setContent($value)
    {
        $this->set("content", $value);
    }

    /**
     * Set the context of the object.
     *
     * @param mixed $value The context value.
     * @return void
     */
    public function setContext($value)
    {
        $this->set("context", $value);
    }

    /**
     * Set the contentMap of the object.
     *
     * @param mixed $value The contentMap value.
     * @return void
     */
    public function setContentMap($value)
    {
        $this->set("contentMap", $value);
    }

    /**
     * Set the name of the object.
     *
     * @param mixed $value The name value.
     * @return void
     */
    public function setName($value)
    {
        $this->set("name", $value);
    }

    /**
     * Set the nameMap of the object.
     *
     * @param mixed $value The nameMap value.
     * @return void
     */
    public function setNameMap($value)
    {
        $this->set("nameMap", $value);
    }

    /**
     * Set the endTime of the object.
     *
     * @param mixed $value The endTime value.
     * @return void
     */
    public function setEndTime($value)
    {
        $this->set("endTime", $value);
    }

    /**
     * Set the generator of the object.
     *
     * @param mixed $value The generator value.
     * @return void
     */
    public function setGenerator($value)
    {
        $this->set("generator", $value);
    }

    /**
     * Set the icon of the object.
     *
     * @param mixed $value The icon value.
     * @return void
     */
    public function setIcon($value)
    {
        $this->set("icon", $value);
    }

    /**
     * Set the image of the object.
     *
     * @param mixed $value The image value.
     * @return void
     */
    public function setImage($value)
    {
        $this->set("image", $value);
    }

    /**
     * Set the inReplyTo of the object.
     *
     * @param mixed $value The inReplyTo value.
     * @return void
     */
    public function setInReplyTo($value)
    {
        $this->set("inReplyTo", $value);
    }

    /**
     * Set the location of the object.
     *
     * @param mixed $value The location value.
     * @return void
     */
    public function setLocation($value)
    {
        $this->set("location", $value);
    }

    /**
     * Set the preview of the object.
     *
     * @param mixed $value The preview value.
     * @return void
     */
    public function setPreview($value)
    {
        $this->set("preview", $value);
    }

    /**
     * Set the published of the object.
     *
     * @param mixed $value The published value.
     * @return void
     */
    public function setPublished($value)
    {
        $this->set("published", $value);
    }

    /**
     * Set the replies of the object.
     *
     * @param mixed $value The replies value.
     * @return void
     */
    public function setReplies($value)
    {
        $this->set("replies", $value);
    }

    /**
     * Set the startTime of the object.
     *
     * @param mixed $value The startTime value.
     * @return void
     */
    public function setStartTime($value)
    {
        $this->set("startTime", $value);
    }

    /**
     * Set the summary of the object.
     *
     * @param mixed $value The summary value.
     * @return void
     */
    public function setSummary($value)
    {
        $this->set("summary", $value);
    }

    /**
     * Set the summaryMap of the object.
     *
     * @param mixed $value The summaryMap value.
     * @return void
     */
    public function setSummaryMap($value)
    {
        $this->set("summaryMap", $value);
    }

    /**
     * Set the tag of the object.
     *
     * @param mixed $value The tag value.
     * @return void
     */
    public function setTag($value)
    {
        $this->set("tag", $value);
    }

    /**
     * Set the URL of the object.
     *
     * @param mixed $value The URL value.
     * @return void
     */
    public function setUrl($value)
    {
        $this->set("url", $value);
    }

    /**
     * Set the to of the object.
     *
     * @param mixed $value The to value.
     * @return void
     */
    public function setTo($value)
    {
        $this->set("to", $value);
    }

    /**
     * Set the bto of the object.
     *
     * @param mixed $value The bto value.
     * @return void
     */
    public function setBto($value)
    {
        $this->set("bto", $value);
    }

    /**
     * Set the cc of the object.
     *
     * @param mixed $value The cc value.
     * @return void
     */
    public function setCc($value)
    {
        $this->set("cc", $value);
    }

    /**
     * Set the bcc of the object.
     *
     * @param mixed $value The bcc value.
     * @return void
     */
    public function setBcc($value)
    {
        $this->set("bcc", $value);
    }

    /**
     * Set the mediaType of the object.
     *
     * @param mixed $value The mediaType value.
     * @return void
     */
    public function setMediaType($value)
    {
        $this->set("mediaType", $value);
    }

    /**
     * Set the duration of the object.
     *
     * @param mixed $value The duration value.
     * @return void
     */
    public function setDuration($value)
    {
        $this->set("duration", $value);
    }

    /**
     * Set the source of the object.
     *
     * @param mixed $value The source value.
     * @return void
     */
    public function setSource($value)
    {
        $this->set("source", $value);
    }

    /**
     * Set the target of the object.
     *
     * @param mixed $value The target value.
     * @return void
     */
    public function setTarget($value)
    {
        $this->set("target", $value);
    }
}
