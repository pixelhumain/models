<?php

namespace PixelHumain\Models\Activitypub\type\core;

class OrderedCollectionPage extends CollectionPage
{
    /**
     * @var string
     */
    protected $type = 'OrderedCollectionPage';

    /**
     * A non-negative integer value identifying the relative position
     * within the logical view of a strictly ordered collection.
     *
     * @var int
     */
    protected $startIndex;

    /**
     * Set the start index of the ordered collection page.
     *
     * @param int $value The start index value.
     * @return void
     */
    public function setStartIndex($value)
    {
        $this->set("startIndex", $value);
    }
}
