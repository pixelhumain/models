<?php

namespace PixelHumain\Models\Activitypub\type\core;

class IntransitiveActivity extends AbstractActivity
{
    /**
     * @var string
     */
    protected $type = 'IntransitiveActivity';
}
