<?php

namespace PixelHumain\Models\Activitypub\type\core;

use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;

class Activity extends AbstractActivity implements ActivityInterface
{
    /**
     * Les traits activitypub
     */
    use TypeTrait;

    /**
     * @var string
     */
    protected $type = 'Activity';

    /**
     * Describes the direct object of the activity.
     * For instance, in the activity "John added a movie to his
     * wishlist", the object of the activity is the movie added.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object-term
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $object;

    /**
     * Set the object of the activity.
     *
     * @param mixed $value The value to set as the object.
     * @return void
     */
    public function setObject($value)
    {
        $this->set("object", $value);
    }

    /**
     * Get the object of the activity.
     *
     * @return AbstractObjectInterface The object of the activity.
     */
    public function getObject(): AbstractObjectInterface
    {
        return $this->getModelTypeAp()->createFromAnyValue($this->get("object"));
    }
}
