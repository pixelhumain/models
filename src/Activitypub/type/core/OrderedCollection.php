<?php

namespace PixelHumain\Models\Activitypub\type\core;

class OrderedCollection extends Collection
{
    /**
     * @var string
     */
    protected $type = 'OrderedCollection';
}
