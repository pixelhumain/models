<?php

namespace PixelHumain\Models\Activitypub\type\core;

use PixelHumain\Models\Activitypub\type\core\Interfaces\AbstractActivityInterface;

abstract class AbstractActivity extends ObjectType implements AbstractActivityInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * Describes one or more entities that either performed or are
     * expected to perform the activity.
     * Any single activity can have multiple actors.
     * The actor MAY be specified using an indirect Link.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-actor
     *
     * @var string
     *    | \ActivityPhp\Type\Extended\AbstractActorInterface
     *    | array<Actor>
     *    | array<Link>
     *    | Link
     */
    protected $actor;

    /**
     * The indirect object, or target, of the activity.
     * The precise meaning of the target is largely dependent on the
     * type of action being described but will often be the object of
     * the English preposition "to".
     * For instance, in the activity "John added a movie to his
     * wishlist", the target of the activity is John's wishlist.
     * An activity can have more than one target.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-target
     *
     * @var string
     *    | ObjectType
     *    | array<ObjectType>
     *    | Link
     *    | array<Link>
     */
    protected $target;

    /**
     * Describes the result of the activity.
     * For instance, if a particular action results in the creation of
     * a new resource, the result property can be used to describe
     * that new resource.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-result
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $result;

    /**
     * An indirect object of the activity from which the
     * activity is directed.
     * The precise meaning of the origin is the object of the English
     * preposition "from".
     * For instance, in the activity "John moved an item to List B
     * from List A", the origin of the activity is "List A".
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-origin
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $origin;

    /**
     * One or more objects used (or to be used) in the completion of an
     * Activity.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-instrument
     *
     * @var string
     *    | ObjectType
     *    | Link
     *    | null
     */
    protected $instrument;

    /**
     * Set the actor of the activity.
     *
     * @param mixed $value The actor value.
     * @return void
     */
    public function setActor($value)
    {
        $this->set("actor", $value);
    }

    /**
     * Set the target of the activity.
     *
     * @param mixed $value The target value.
     * @return void
     */
    public function setTarget($value)
    {
        $this->set("target", $value);
    }

    /**
     * Set the result of the activity.
     *
     * @param mixed $value The result value.
     * @return void
     */
    public function setResult($value)
    {
        $this->set("result", $value);
    }

    /**
     * Set the origin of the activity.
     *
     * @param mixed $value The origin value.
     * @return void
     */
    public function setOrigin($value)
    {
        $this->set("origin", $value);
    }

    /**
     * Set the instrument of the activity.
     *
     * @param mixed $value The instrument value.
     * @return void
     */
    public function setInstrument($value)
    {
        $this->set("instrument", $value);
    }

    /**
     * Set the summary of the activity.
     *
     * @param mixed $value The summary value.
     * @return void
     */
    public function setSummary($value)
    {
        $this->set('summary', $value);
    }
}
