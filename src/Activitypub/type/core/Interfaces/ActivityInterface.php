<?php

namespace PixelHumain\Models\Activitypub\type\core\Interfaces;

use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;

interface ActivityInterface extends AbstractActivityInterface
{
    /**
     * Set the object of the activity.
     *
     * @param mixed $value The value to set as the object.
     * @return void
     */
    public function setObject($value);

    /**
     * Get the object of the activity.
     *
     * @return AbstractObjectInterface The object of the activity.
     */
    public function getObject(): AbstractObjectInterface;
}
