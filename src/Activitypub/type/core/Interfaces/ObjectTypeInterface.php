<?php

namespace PixelHumain\Models\Activitypub\type\core\Interfaces;

use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;

interface ObjectTypeInterface extends AbstractObjectInterface
{
    /**
     * Set the ID of the object.
     *
     * @param mixed $value The ID value.
     * @return void
     */
    public function setId($value);

    /**
     * Set the type of the object.
     *
     * @param mixed $value The type value.
     * @return void
     */
    public function setType($value);

    /**
     * Set the attachment of the object.
     *
     * @param mixed $value The attachment value.
     * @return void
     */
    public function setAttachment($value);

    /**
     * Set the attributedTo of the object.
     *
     * @param mixed $value The attributedTo value.
     * @return void
     */
    public function setAttributedTo($value);

    /**
     * Set the audience of the object.
     *
     * @param mixed $value The audience value.
     * @return void
     */
    public function setAudience($value);

    /**
     * Set the content of the object.
     *
     * @param mixed $value The content value.
     * @return void
     */
    public function setContent($value);

    /**
     * Set the context of the object.
     *
     * @param mixed $value The context value.
     * @return void
     */
    public function setContext($value);

    /**
     * Set the contentMap of the object.
     *
     * @param mixed $value The contentMap value.
     * @return void
     */
    public function setContentMap($value);

    /**
     * Set the name of the object.
     *
     * @param mixed $value The name value.
     * @return void
     */
    public function setName($value);

    /**
     * Set the nameMap of the object.
     *
     * @param mixed $value The nameMap value.
     * @return void
     */
    public function setNameMap($value);

    /**
     * Set the endTime of the object.
     *
     * @param mixed $value The endTime value.
     * @return void
     */
    public function setEndTime($value);

    /**
     * Set the generator of the object.
     *
     * @param mixed $value The generator value.
     * @return void
     */
    public function setGenerator($value);

    /**
     * Set the icon of the object.
     *
     * @param mixed $value The icon value.
     * @return void
     */
    public function setIcon($value);

    /**
     * Set the image of the object.
     *
     * @param mixed $value The image value.
     * @return void
     */
    public function setImage($value);

    /**
     * Set the inReplyTo of the object.
     *
     * @param mixed $value The inReplyTo value.
     * @return void
     */
    public function setInReplyTo($value);

    /**
     * Set the location of the object.
     *
     * @param mixed $value The location value.
     * @return void
     */
    public function setLocation($value);

    /**
     * Set the preview of the object.
     *
     * @param mixed $value The preview value.
     * @return void
     */
    public function setPreview($value);

    /**
     * Set the published of the object.
     *
     * @param mixed $value The published value.
     * @return void
     */
    public function setPublished($value);

    /**
     * Set the replies of the object.
     *
     * @param mixed $value The replies value.
     * @return void
     */
    public function setReplies($value);

    /**
     * Set the startTime of the object.
     *
     * @param mixed $value The startTime value.
     * @return void
     */
    public function setStartTime($value);

    /**
     * Set the summary of the object.
     *
     * @param mixed $value The summary value.
     * @return void
     */
    public function setSummary($value);

    /**
     * Set the summaryMap of the object.
     *
     * @param mixed $value The summaryMap value.
     * @return void
     */
    public function setSummaryMap($value);

    /**
     * Set the tag of the object.
     *
     * @param mixed $value The tag value.
     * @return void
     */
    public function setTag($value);

    /**
     * Set the URL of the object.
     *
     * @param mixed $value The URL value.
     * @return void
     */
    public function setUrl($value);

    /**
     * Set the to of the object.
     *
     * @param mixed $value The to value.
     * @return void
     */
    public function setTo($value);

    /**
     * Set the bto of the object.
     *
     * @param mixed $value The bto value.
     * @return void
     */
    public function setBto($value);

    /**
     * Set the cc of the object.
     *
     * @param mixed $value The cc value.
     * @return void
     */
    public function setCc($value);

    /**
     * Set the bcc of the object.
     *
     * @param mixed $value The bcc value.
     * @return void
     */
    public function setBcc($value);

    /**
     * Set the mediaType of the object.
     *
     * @param mixed $value The mediaType value.
     * @return void
     */
    public function setMediaType($value);

    /**
     * Set the duration of the object.
     *
     * @param mixed $value The duration value.
     * @return void
     */
    public function setDuration($value);

    /**
     * Set the source of the object.
     *
     * @param mixed $value The source value.
     * @return void
     */
    public function setSource($value);

    /**
     * Set the target of the object.
     *
     * @param mixed $value The target value.
     * @return void
     */
    public function setTarget($value);
}
