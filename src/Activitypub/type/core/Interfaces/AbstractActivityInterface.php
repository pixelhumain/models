<?php

namespace PixelHumain\Models\Activitypub\type\core\Interfaces;

interface AbstractActivityInterface extends ObjectTypeInterface
{
    /**
     * Set the actor of the activity.
     *
     * @param mixed $value The actor value.
     * @return void
     */
    public function setActor($value);

    /**
     * Set the target of the activity.
     *
     * @param mixed $value The target value.
     * @return void
     */
    public function setTarget($value);

    /**
     * Set the result of the activity.
     *
     * @param mixed $value The result value.
     * @return void
     */
    public function setResult($value);

    /**
     * Set the origin of the activity.
     *
     * @param mixed $value The origin value.
     * @return void
     */
    public function setOrigin($value);

    /**
     * Set the instrument of the activity.
     *
     * @param mixed $value The instrument value.
     * @return void
     */
    public function setInstrument($value);

    /**
     * Set the summary of the activity.
     *
     * @param mixed $value The summary value.
     * @return void
     */
    public function setSummary($value);
}
