<?php

namespace PixelHumain\Models\Activitypub\type\core;

use PixelHumain\Models\Activitypub\type\AbstractObject;

class Link extends AbstractObject
{
    /**
     * @var string
     */
    protected $type = 'Link';

    /**
     * @var string
     */
    protected $id;

    /**
     * A simple, human-readable, plain-text name for the object.
     * HTML markup MUST NOT be included.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
     *
     * @var string|null xsd:string
     */
    protected $name;

    /**
     * The name MAY be expressed using multiple language-tagged values.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-name
     *
     * @var array<string,string>|null rdf:langString
     */
    protected $nameMap;

    /**
     * The target resource pointed to by a Link.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-href
     *
     * @var string|null
     */
    protected $href;

    /**
     * Hints as to the language used by the target resource.
     * Value MUST be a BCP47 Language-Tag.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-hreflang
     *
     * @var string|null
     */
    protected $hreflang;

    /**
     * The MIME media type of the referenced resource.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-mediatype
     *
     * @var string|null
     */
    protected $mediaType;

    /**
     * A link relation associated with a Link.
     * The value MUST conform to both the HTML5
     * and RFC5988 "link relation" definitions.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-rel
     *
     * @var string|array|null
     */
    protected $rel;

    /**
     * Specifies a hint as to the rendering height
     * in device-independentpixels of the linked resource
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-height
     *
     * @var int|null A non negative integer
     */
    protected $height;

    /**
     * An entity that provides a preview of this link.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-preview
     *
     * @var string
     *    | Object
     *    | Link
     *    | null
     */
    protected $preview;

    /**
     * On a Link, specifies a hint as to the rendering width in
     * device-independent pixels of the linked resource.
     *
     * @see https://www.w3.org/TR/activitystreams-vocabulary/#dfn-width
     *
     * @var int|null A non negative integer
     */
    protected $width;

    /**
     * Set the type of the link.
     *
     * @param string $value The type of the link.
     * @return void
     */
    public function setType($value)
    {
        $this->set("type", $value);
    }

    /**
     * Set the ID of the link.
     *
     * @param string $value The ID of the link.
     * @return void
     */
    public function setId($value)
    {
        $this->set("id", $value);
    }

    /**
     * Set the name of the link.
     *
     * @param string $value The name of the link.
     * @return void
     */
    public function setName($value)
    {
        $this->set("name", $value);
    }

    /**
     * Set the name map of the link.
     *
     * @param array $value The name map of the link.
     * @return void
     */
    public function setNameMap($value)
    {
        $this->set("nameMap", $value);
    }

    /**
     * Set the href of the link.
     *
     * @param string $value The href of the link.
     * @return void
     */
    public function setHref($value)
    {
        $this->set("href", $value);
    }

    /**
     * Set the hreflang of the link.
     *
     * @param string $value The hreflang of the link.
     * @return void
     */
    public function setHreflang($value)
    {
        $this->set("hreflang", $value);
    }

    /**
     * Set the media type of the link.
     *
     * @param string $value The media type of the link.
     * @return void
     */
    public function setMediaType($value)
    {
        $this->set("mediaType", $value);
    }

    /**
     * Set the rel of the link.
     *
     * @param string $value The rel of the link.
     * @return void
     */
    public function setRel($value)
    {
        $this->set("rel", $value);
    }

    /**
     * Set the height of the link.
     *
     * @param int $value The height of the link.
     * @return void
     */
    public function setHeight($value)
    {
        $this->set("height", $value);
    }

    /**
     * Set the preview of the link.
     *
     * @param string $value The preview of the link.
     * @return void
     */
    public function setPreview($value)
    {
        $this->set("preview", $value);
    }

    /**
     * Set the width of the link.
     *
     * @param int $value The width of the link.
     * @return void
     */
    public function setWidth($value)
    {
        $this->set("width", $value);
    }
}
