<?php

namespace PixelHumain\Models\Activitypub\type;

use PixelHumain\Models\Activitypub\type\Interfaces\TypeResolverInterface;

/**
 * The TypeResolver class is responsible for resolving the class name based on the given type.
 */
abstract class TypeResolver implements TypeResolverInterface
{
    /**
     * The core types supported by ActivityPub.
     *
     * @var array
     */
    public static array $coreTypes = [
        'Activity', 'Collection', 'CollectionPage',
        'IntransitiveActivity', 'Link', 'ObjectType',
        'OrderedCollection', 'OrderedCollectionPage',
        'Object',
    ];

    /**
     * The actor types supported by ActivityPub.
     *
     * @var array
     */
    public static array $actorTypes = [
        'Application', 'Group', 'Organization', 'Person', 'Service', 'Project',
    ];

    /**
     * The activity types supported by ActivityPub.
     *
     * @var array
     */
    public static array $activityTypes = [
        'Accept', 'Add', 'Announce', 'Arrive', 'Block',
        'Create', 'Delete', 'Dislike', 'Flag', 'Follow',
        'Ignore', 'Invite', 'Join', 'Leave', 'Like', 'Listen',
        'Move',  'Offer', 'Question', 'Read', 'Reject', 'Remove',
        'TentativeAccept', 'TentativeReject', 'Travel', 'Undo',
        'Update', 'View',
    ];

    /**
     * The object types supported by ActivityPub.
     *
     * @var array
     */
    public static array $objectTypes = [
        'Article', 'Audio', 'Document', 'Event', 'Image',
        'Mention', 'Note', 'Page', 'Place', 'Profile',
        'Relationship', 'Tombstone', 'Video',
    ];

    /**
     * Get the class name for the given type.
     *
     * @param string $type The type to resolve.
     * @return string The fully qualified class name.
     */
    public static function getClass(string $type): string
    {
        $namespace = __NAMESPACE__;

        if ($type == "Object") {
            $type .= "Type";
        }

        if (in_array($type, self::$coreTypes)) {
            $namespace .= "\core";
        } elseif (in_array($type, self::$activityTypes)) {
            $namespace .= '\extended\activity';
        } elseif (in_array($type, self::$actorTypes)) {
            $namespace .= '\extended\actor';
        } elseif (in_array($type, self::$objectTypes)) {
            $namespace .= '\extended\object';
        } else {
            $type = "AnonymousType";
            $namespace .= "\core";
            //throw new Exception("Undefined scope for type '$type'");
        }

        return $namespace . '\\' . $type;
    }

    /**
     * Check if the given type is an anonymous type.
     *
     * @param string $type The type to check.
     * @return bool True if the type is anonymous, false otherwise.
     */
    public static function isAnonymousType(string $type): bool
    {
        return ! (
            $type == "Object" ||
            in_array($type, array_merge(
                self::$coreTypes,
                self::$actorTypes,
                self::$activityTypes,
                self::$objectTypes
            ))
        );
    }
}
