<?php

namespace PixelHumain\Models\Activitypub\type;

use Exception;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubObjectTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubObjectTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\AbstractActivityInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\TypeInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\BaseModel;
use RuntimeException;

class Type extends BaseModel implements TypeInterface, ActivitypubObjectTraitInterface, ActivitypubActivityTraitInterface, RequestTraitInterface
{
    use ActivitypubObjectTrait;
    use ActivitypubActivityTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    /**
     * Create a new instance of the specified type with the given attributes.
     *
     * @param array|string $type The type of the object to create.
     * @param array $attributes The attributes to set on the object.
     * @return AbstractObjectInterface|AbstractActivityInterface|AbstractActorInterface The newly created object.
     */
    public function create($type, array $attributes = [])
    {
        if (! is_string($type) && ! is_array($type)) {
            throw new Exception('Type parameter must be a string or an array. Given=' . gettype($type));
        }

        if (is_array($type)) {
            if (! isset($type['type'])) {
                throw new Exception("Type parameter must have a 'type' key");
            }
            $attributes = $type;
        }

        $type = is_array($type) ? $type["type"] : $type;
        $className = TypeResolver::getClass($type);

        $object = new $className();

        if (! ($object instanceof AbstractObjectInterface) && ! ($object instanceof AbstractActivityInterface) && ! ($object instanceof AbstractActorInterface)) {
            throw new RuntimeException('Unexpected return type');
        }

        if (TypeResolver::isAnonymousType($type)) {
            $object->set("type", $type);
        }

        foreach ($attributes as $name => $value) {
            if ($name == "content") {
                $value = str_replace("\n", "</br>", $value);
            }
            $object->set($name, $value);
        }
        return $object;
    }

    /**
     * Creates an instance of Type from any value.
     *
     * @param mixed $value The value to create the Type from.
     * @return AbstractObjectInterface|AbstractActivityInterface|AbstractActorInterface The newly created object.
     */
    public function createFromAnyValue($value)
    {
        if ($value instanceof AbstractObjectInterface) {
            return $value;
        }

        if (is_array($value) && isset($value["type"])) {
            return $this->create($value["type"], $value);
        }

        if (is_string($value) && filter_var($value, FILTER_VALIDATE_URL)) {
            if ($localObject = $this->getModelActivitypubObject()->getObjectByIdAsArray($value)) {
                $typeArray = $localObject;
            } elseif ($localActivity = $this->getModelActivitypubActivity()->getByActivityId($value)) {
                $typeArray = $localActivity;
            } else {
                $typeArray = $this->getModelRequest()->get($value);
            }

            if (isset($typeArray["type"])) {
                return $this->create($typeArray["type"], $typeArray);
            }
        }

        throw new Exception("Invalid value.");
    }
}
