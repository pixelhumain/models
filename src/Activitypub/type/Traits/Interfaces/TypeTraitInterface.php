<?php

namespace PixelHumain\Models\Activitypub\type\Traits\Interfaces;

use PixelHumain\Models\Activitypub\type\Interfaces\TypeInterface;

interface TypeTraitInterface
{
    /**
     * Set the type activity pub.
     *
     * @param TypeInterface $typeAp The type object to set.
     * @return void
     */
    public function setModelTypeAp(TypeInterface $typeAp): void;

    /**
     * Gets the type activity pub.
     *
     * @return TypeInterface The type.
     */
    public function getModelTypeAp(): TypeInterface;
}
