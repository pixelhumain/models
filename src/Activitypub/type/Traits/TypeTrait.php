<?php

namespace PixelHumain\Models\Activitypub\type\Traits;

use Exception;
use PixelHumain\Models\Activitypub\type\Interfaces\TypeInterface;

trait TypeTrait
{
    protected ?TypeInterface $typeAp = null;

    /**
     * Set the type activity pub.
     *
     * @param TypeInterface $typeAp The type activity pub object to set.
     * @return void
     */
    public function setModelTypeAp(TypeInterface $typeAp): void
    {
        $this->typeAp = $typeAp;
    }

    /**
     * Gets the type activity pub.
     *
     * @return TypeInterface The type activity pub.
     */
    public function getModelTypeAp(): TypeInterface
    {
        if ($this->typeAp === null) {
            throw new Exception(TypeInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->typeAp;
    }
}
