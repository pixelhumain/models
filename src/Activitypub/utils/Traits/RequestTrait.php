<?php

namespace PixelHumain\Models\Activitypub\utils\Traits;

use Exception;
use PixelHumain\Models\Activitypub\utils\Interfaces\RequestInterface;

trait RequestTrait
{
    protected ?RequestInterface $request = null;

    /**
     * Set the request.
     *
     * @param RequestInterface $request The request object to set.
     * @return void
     */
    public function setModelRequest(RequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * Gets the request.
     *
     * @return RequestInterface The request.
     */
    public function getModelRequest(): RequestInterface
    {
        if ($this->request === null) {
            throw new Exception(RequestInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->request;
    }
}
