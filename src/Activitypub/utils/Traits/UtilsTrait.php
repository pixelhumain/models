<?php

namespace PixelHumain\Models\Activitypub\utils\Traits;

use Exception;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

trait UtilsTrait
{
    protected ?UtilsInterface $utils = null;

    /**
     * Set the utils.
     *
     * @param UtilsInterface $utils The utils object to set.
     * @return void
     */
    public function setModelUtils(UtilsInterface $utils): void
    {
        $this->utils = $utils;
    }

    /**
     * Gets the utils.
     *
     * @return UtilsInterface The utils.
     */
    public function getModelUtils(): UtilsInterface
    {
        if ($this->utils === null) {
            throw new Exception(UtilsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->utils;
    }
}
