<?php

namespace PixelHumain\Models\Activitypub\utils\Traits\Interfaces;

use PixelHumain\Models\Activitypub\utils\Interfaces\RequestInterface;

interface RequestTraitInterface
{
    /**
     * Set the request.
     *
     * @param RequestInterface $request The request object to set.
     * @return void
     */
    public function setModelRequest(RequestInterface $request): void;

    /**
     * Gets the request.
     *
     * @return RequestInterface The request.
     */
    public function getModelRequest(): RequestInterface;
}
