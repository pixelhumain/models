<?php

namespace PixelHumain\Models\Activitypub\utils\Traits\Interfaces;

use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

interface UtilsTraitInterface
{
    /**
     * Set the utils.
     *
     * @param UtilsInterface $utils The utils object to set.
     * @return void
     */
    public function setModelUtils(UtilsInterface $utils): void;

    /**
     * Gets the utils.
     *
     * @return UtilsInterface The utils.
     */
    public function getModelUtils(): UtilsInterface;
}
