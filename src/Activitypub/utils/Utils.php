<?php

namespace PixelHumain\Models\Activitypub\utils;

use DateTime;
use DateTimeZone;
use Exception;
use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\handlers\Handler;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityCreatorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubObjectTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubObjectTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\SlugTrait;

// TODO : Yii::getAlias
// TODO : Yii::get('module')->getModule('co2')->assetsUrl

class Utils implements UtilsInterface, PersonTraitInterface, DataValidatorTraitInterface, SlugTraitInterface, PreferenceTraitInterface, TypeTraitInterface, ActivitypubTranslatorTraitInterface, ActivitypubObjectTraitInterface, ActivitypubActorTraitInterface
{
    // Yii::getAlias('@app') . "/config/certificate/
    public string $basePathCertificate;

    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use DataValidatorTrait;
    use PreferenceTrait;
    use SlugTrait;

    /**
     * Les traits activitypub
     */
    use TypeTrait;
    use ActivitypubTranslatorTrait;
    use ActivitypubObjectTrait;
    use ActivitypubActivityCreatorTrait;
    use ActivitypubActorTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateParamsProperty();

        if ($this->params->get("basePathCertificate") != null) {
            $this->basePathCertificate = $this->params->get("basePathCertificate");
        }

        if (! is_string($this->basePathCertificate)) {
            throw new CTKException("The basePathCertificate property must be a string.");
        }
    }

    /**
     * Creates a URL using the provided endpoint.
     *
     * @param string $endpoint The endpoint to append to the URL.
     * @return string The complete URL.
     */
    public function createUrl(string $endpoint): string
    {
        return Config::SCHEMA . "://" . Config::HOST . "/" . $endpoint;
    }

    /**
     * Decodes a JSON string into an associative array.
     *
     * @param string $value The JSON string to decode.
     * @return mixed The decoded JSON as an associative array.
     * @throws Exception If the JSON decoding fails.
     */
    public function decodeJson(string $value)
    {
        $json = json_decode($value, true);

        if ($error = json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception(json_encode([
                "code" => $error,
                "message" => 'JSON decoding failed for string.',
                "value" => $value,
            ]));
        }

        return $json;
    }

    /**
     * Retrieves the PEM string based on the given type.
     *
     * @param string $type The type of PEM string to retrieve.
     * @return string|null The PEM string if found, null otherwise.
     */
    public function getPem(string $type): ?string
    {
        if ($type == "private") {
            return file_get_contents($this->basePathCertificate . "key.pem");
        } elseif ($type == "public") {
            return file_get_contents($this->basePathCertificate . "public.pem");
        } else {
            return null;
        }
    }

    /**
     * Returns the current UTC date in RFC7231 format.
     *
     * @return string The current UTC date.
     */
    public function getCurrentUTCDate(): string
    {
        $utcDate = new DateTime("now", new DateTimeZone("UTC"));
        return $utcDate->format(DateTime::RFC7231);
    }

    /**
     * Retrieves all the IDs from an array of objects that implement the AbstractObjectInterface.
     *
     * @param array $objects An array of objects.
     * @return array An array containing all the IDs.
     */
    public function retrieveAllIdsFromObjects(array $objects): array
    {
        $ids = [];
        foreach ($objects as $object) {
            if ($object instanceof AbstractObjectInterface && ($id = $object->get("id"))) {
                $ids[] = $id;
            }
        }
        return $ids;
    }

    /**
     * Checks if a text contains a URL.
     *
     * @param string $str The text to check.
     * @return bool Returns true if the text contains a URL, false otherwise.
     */
    public function isTextHasUrl(string $str): bool
    {
        $match = preg_match_all(self::URL_REGEX, $str);
        return $match > 0;
    }

    /**
     * Retrieves the URL of the website's favicon.
     *
     * @param string $url The URL of the website.
     * @return string The URL of the website's favicon.
     */
    public function getWebsiteFavicon(string $url): string
    {
        $elems = parse_url($url);
        $url = $elems['scheme'] . '://' . $elems['host'] . '/favicon.ico';
        return $url;
    }

    /**
     * Retrieves the base URL of the website.
     *
     * @param string $url The URL of the website.
     * @return string The base URL of the website.
     */
    public function getWebsiteBaseUrl(string $url): string
    {
        $result = parse_url($url);
        return $result['scheme'] . "://" . $result['host'];
    }

    /**
     * Retrieves the domain of the website.
     *
     * @param string $url The URL of the website.
     * @return string The domain of the website.
     */
    public function getWebsiteDomain(string $url): string
    {
        $result = parse_url($url);
        return $result['host'] ?? '';
    }

    /**
     * Wraps URLs in a text with HTML link tags.
     *
     * @param string $str The text containing URLs.
     * @return string The text with URLs wrapped in HTML link tags.
     */
    public function warpUrlIntoLinkTag(string $str): string
    {
        $text = preg_replace_callback(
            self::URL_REGEX,
            fn ($v) => '<a href="' . $v[0] . '" target="_blank">' . $v[0] . '</a>',
            explode(" ", $str)
        );
        return implode(" ", $text);
    }

    /**
     * Converts a string date to a MongoDB date.
     *
     * @param string $myDate The string date to convert.
     * @param string $label The label for the date.
     * @return mixed The converted MongoDB date.
     */
    public function stringDateToMongoDate(string $myDate, string $label)
    {
        $result = $this->getModelDataValidator()->getDateTimeFromString($myDate, $label);
        return $this->db->MongoDate($result->getTimestamp());
    }

    /**
     * Truncates a text to a specified number of characters and adds an optional ellipsis at the end.
     *
     * @param string $text The text to truncate.
     * @param int $maxchar The maximum number of characters.
     * @param string $end The optional ellipsis to add at the end. Default is '...'.
     * @return string The truncated text.
     */
    public function substrwords(string $text, int $maxchar, string $end = '...'): string
    {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i = 0;
            while (1) {
                $length = strlen($output) + strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                } else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } else {
            $output = $text;
        }
        return strip_tags($output);
    }

    /**
     * Returns the MIME type of an image based on its file path.
     *
     * @param string $image_path The path to the image file.
     * @return string The MIME type of the image.
     */
    public function get_image_mime_type(string $image_path): string
    {
        $mimes = [
            IMAGETYPE_GIF => "image/gif",
            IMAGETYPE_JPEG => "image/jpg",
            IMAGETYPE_PNG => "image/png",
            IMAGETYPE_BMP => "image/bmp",
        ];

        if (($image_type = exif_imagetype($image_path))
            && (array_key_exists($image_type, $mimes))
        ) {
            return $mimes[$image_type];
        } else {
            return $mimes[IMAGETYPE_JPEG];
        }
    }

    /**
     * Generates a UUID (Universally Unique Identifier).
     *
     * @return string The generated UUID.
     */
    public function genUuid(): string
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            random_int(0, 0x0fff) | 0x4000,
            random_int(0, 0x3fff) | 0x8000,
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            random_int(0, 0xffff)
        );
    }

    /**
     * Converts an object to an event data array.
     *
     * @param object $object The object to convert.
     * @param string|null $uuid The UUID of the object.
     * @return array The event data array.
     */
    public function parseToEvent($object, ?string $uuid)
    {
        $date = $this->db->MongoDate(time());
        $data = [
            "collection" => "events",
            "name" => $object->get('name'),
            "created" => strtotime($object->get('published')),
            "updated" => strtotime($object->get('updated')),
            "startDate" => $this->stringDateToMongoDate($object->get("startTime"), 'startTime'),
            "endDate" => $this->stringDateToMongoDate($object->get("endTime"), 'endTime'),
            "public" => true,
            "fromActivityPub" => true,
            "attributedTo" => $object->get("attributedTo"),
            "objectId" => $object->get("id"),
        ];

        if (@$uuid) {
            $data = array_merge($data, [
                'objectUUID' => $uuid,
            ]);
        }
        if ($object->get('_extend_props')) {
            $objectExtendProps = $object->get('_extend_props');
            if ($objectExtendProps['category']) {
                $data = array_merge($data, [
                    'type' => ucfirst(strtolower($objectExtendProps['category'])),
                ]);
            }
            if ($objectExtendProps['timezone']) {
                $data = array_merge($data, [
                    'timeZone' => $objectExtendProps['timezone'],
                ]);
            }
        }
        if (@$object->get('location') && $object->get('location') != null) {
            $location = $object->get('location');
            $locationExtendProps = $location->get('_extend_props');
            $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

            if (@$addressExtendProps && $addressExtendProps != null) {
                $geo = [
                    "@type" => "GeoCoordinates",
                    "latitude" => $location->get('latitude'),
                    "longitude" => $location->get('longitude'),
                ];
                $geoPosition = [
                    "type" => $location->get('type'),
                    "coordinates" => [$location->get('longitude'), $location->get('latitude')],
                ];

                $data = array_merge($data, [
                    "address" =>
                                    [
                                        '@type' => $addressExtendProps['type'],
                                        'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null ? @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] . " " . $addressExtendProps['addressRegion'] : $addressExtendProps['addressRegion'] : "",
                                        'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : "",
                                    ],
                    'addressLocality' => @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "",
                    "geo" => $geo,
                ]);
            }
        }
        return $data;
    }

    // TODO : $uuid n'est pas utilisé
    /**
     * Parses the given object into a project data array.
     *
     * @param object $object The object to parse.
     * @param string|null $uuid The UUID of the project.
     * @return array The parsed project data.
     */
    public function parseToProject($object, ?string $uuid)
    {
        $data = [
            "collection" => "projects",
            "name" => $object->get('name'),
            "slug" => $this->getModelSlug()->checkAndCreateSlug($object->get('name')),
            "created" => strtotime($object->get('published')),
            "updated" => strtotime($object->get('updated')),
            "public" => true,
            "fromActivityPub" => true,
            "attributedTo" => $object->get("attributedTo"),
            "objectId" => $object->get("id"),
        ];

        if (! empty($object->get('startTime'))) {
            $data["startDate"] = $object->get('startTime');
        }

        if (! empty($object->get('endTime'))) {
            $data["endDate"] = $object->get('endTime');
        }
        if (! empty($object->get('content'))) {
            $data["description"] = $object->get('content');
        }
        if (! empty($object->get('progress'))) {
            $data["properties"] = [
                'avancement' => $object->get('progress'),
            ];
        }

        if (! empty($object->get('tag'))) {
            $data["tags"] = $object->get('tag');
        }
        if (! empty($object->get('email'))) {
            $data["email"] = $object->get('email');
        }

        if (@$object->get('tag') && $object->get('tag') != null) {
            $tags = $object->get('tag');
            $tagList = [];
            foreach ($tags as $tagData) {
                $extendTagProps = $tagData->get('_extend_props');
                array_push($tagList, substr($extendTagProps['name'], 1));
            }
            $data = array_merge($data, [
                'tags' => $tagList,
            ]);
        }

        if (@$object->get('attachment') && $object->get('attachment') != null) {
            $attachements = $object->get('attachment');
            foreach ($attachements as $attachement) {
                $extendAttachementsProps = $attachement->get('_extend_props');
                if ($extendAttachementsProps == null) {
                    if ($attachement->get('name') == 'Banner') {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg = $attachement->get('url');
                            $data = array_merge($data, [
                                'profilBannerUrl' => $urlImg,
                                'profilRealUrl' => $urlImg,
                            ]);
                        }
                    } else {
                        if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            $urlImg = $attachement->get('url');
                            $data = array_merge($data, [
                                'profilImageUrl' => $urlImg,
                                'profilMediumImageUrl' => $urlImg,
                            ]);
                        }
                    }
                }
            }
        }

        $location = $object->get('location');
        if (isset($location) && $location != null) {
            $location = $object->get('location');
            $locationExtendProps = $location->get('_extend_props');
            $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

            if (isset($addressExtendProps) && $addressExtendProps != null) {
                $geo = [
                    "@type" => "GeoCoordinates",
                    "latitude" => $location->get('latitude'),
                    "longitude" => $location->get('longitude'),
                ];
                $geoPosition = [
                    "type" => $location->get('type'),
                    "coordinates" => [$location->get('longitude'), $location->get('latitude')],
                ];

                $data = array_merge($data, [
                    "address" =>
                                    [
                                        '@type' => $addressExtendProps['type'],
                                        'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null ? @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressRegion'] . " " . $addressExtendProps['addressLocality'] : $addressExtendProps['addressRegion'] : "",
                                        'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : "",
                                    ],
                    'addressLocality' => @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "",
                ]);
            }
            $data = array_merge($data, [
                'geo' => $geo,
            ]);
        }
        return $data;
    }

    /**
     * Returns an array of CC recipients based on the given parameters.
     *
     * @param string $elementType The type of the element.
     * @param array|string $linkType The type of the link.
     * @param AbstractActorInterface $subject The subject of the activity.
     * @param AbstractActorInterface $invitor The invitor of the activity.
     * @return array The array of CC recipients.
     */
    public function getCCRecipients(string $elementType, $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): array
    {
        $element = $this->db->findOne($elementType, [
            "objectId" => $subject->get("id"),
        ]);
        if (! $element) {
            throw new Exception("Link not found");
        }
        $links = [];
        if (is_string($linkType)) {
            foreach ($element["links"]["activitypub"][$linkType] as $el) {
                if ($el["invitorId"] == $invitor) {
                    $links[] = $el;
                    break;
                }
            }
        } else {
            if (is_countable($linkType) ? count($linkType) : 0) {
                foreach ($linkType as $lt) {
                    foreach ($element["links"]["activitypub"][$linkType] as $el) {
                        if ($el["invitorId"] == $invitor) {
                            $links[] = $el;
                            break;
                        }
                    }
                }
            }
        }
        return $links;
    }

    /**
     * Converts an ActivityPub object to an event.
     *
     * @param string $objectUUID The UUID of the object to convert.
     * @return array The converted event.
     */
    public function activitypubObjectToEvent(string $objectUUID): array
    {
        $connectedUserId = $this->session->getUserId();
        $connectedUser = $this->getModelPerson()->getById($connectedUserId);
        $actorConnected = $this->getModelActivitypubTranslator()->coPersonToActor($connectedUser);
        $activitypubEnable = $this->getModelPreference()->isActivitypubActivate(@$connectedUser["preferences"]);
        $data = [];

        if ($activitypubEnable) {
            $object = $this->getModelActivitypubObject()->getObjectByUUID($objectUUID);
            if ($object) {
                $organizer = $this->getModeltypeAp()->createFromAnyValue($object->get("attributedTo"));
                $res = $this->db->findOne(ActivitypubObjectInterface::COLLECTION, [
                    "uuid" => $objectUUID,
                ]);
                $date = $this->db->MongoDate(time());
                $participant = @$res['payload'] && is_array($res['payload']) ? $res['payload'] : [];
                $data = [
                    "collection" => "events",
                    "name" => strtoupper($object->get('name')),
                    "url" => $object->get('id'),
                    "created" => strtotime($object->get('published')),
                    "updated" => strtotime($object->get('updated')),
                    "shortDescription" => $object->get("content") != "" ? $this->substrwords($object->get("content"), 140) : "",
                    "providerInfo" => [
                        "name" => $this->getWebsiteDomain($object->get('id')),
                        "icon" => $this->getWebsiteFavicon($object->get('id')),
                        "url" => $this->getWebsiteBaseUrl($object->get('id')),
                    ],
                    "slug" => $this->getModelSlug()->checkAndCreateSlug($object->get('name')),
                    "startDate" => $this->stringDateToMongoDate($object->get("startTime"), 'startTime'),
                    "endDate" => $this->stringDateToMongoDate($object->get("endTime"), 'endTime'),
                    "description" => $object->get("content"),
                    "objectId" => $object->get("id"),
                    "hasParticipate" => in_array($actorConnected->get("id"), $participant),
                ];
                if (@$object->get('_extend_props')) {
                    $objectExtendProps = $object->get('_extend_props');
                    if (@$objectExtendProps['category']) {
                        $data = array_merge($data, [
                            'type' => ucfirst(strtolower($objectExtendProps['category'])),
                        ]);
                    }
                    if (@$objectExtendProps['timezone']) {
                        $data = array_merge($data, [
                            'timeZone' => $objectExtendProps['timezone'],
                        ]);
                    }
                    $data = array_merge($data, [
                        'participantCount' => count($participant),
                    ]);
                }
                if (@$object->get('attachment') && $object->get('attachment') != null) {
                    $attachements = $object->get('attachment');

                    foreach ($attachements as $attachement) {
                        $extendAttachementsProps = $attachement->get('_extend_props');
                        if ($extendAttachementsProps == null) {
                            //if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                            if ($attachement->get('category') == 'socialNetwork') {
                                $data = array_merge($data, [
                                    'socialNetwork' => [
                                        $attachement->get('name') => $attachement->get('url'),
                                    ],
                                ]);
                            } else {
                                $urlImg = $attachement->get('url');
                                $data = array_merge($data, [
                                    'profilImageUrl' => $urlImg,
                                    'profilMediumImageUrl' => $urlImg,
                                    'profilRealBannerUrl' => $urlImg,
                                ]);
                            }
                            //}
                        }
                    }
                }

                $location = $object->get('location');
                if (isset($location) && $location != null) {
                    $location = $object->get('location');
                    $locationExtendProps = $location->get('_extend_props');
                    $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

                    if (isset($addressExtendProps) && $addressExtendProps != null) {
                        $geo = [
                            "@type" => "GeoCoordinates",
                            "latitude" => $location->get('latitude'),
                            "longitude" => $location->get('longitude'),
                        ];
                        $geoPosition = [
                            "type" => $location->get('type'),
                            "coordinates" => [$location->get('longitude'), $location->get('latitude')],
                        ];

                        $data = array_merge($data, [
                            "address" =>
                                                    [
                                                        '@type' => $addressExtendProps['type'],
                                                        'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null ? @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressRegion'] . " " . $addressExtendProps['addressLocality'] : $addressExtendProps['addressRegion'] : "",
                                                        'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : "",
                                                    ],
                            'addressLocality' => @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "",
                        ]);
                    }
                    $data = array_merge($data, [
                        'geo' => $geo,
                    ]);
                    //$data = array_merge($data,array('geoPosition' => $geoPosition));
                }
                if (@$object->get('tag') && $object->get('tag') != null) {
                    $tags = $object->get('tag');
                    $tagList = [];
                    foreach ($tags as $tagData) {
                        $extendTagProps = $tagData->get('_extend_props');
                        //  if($tag->get('type')=='Hashtag'){
                        array_push($tagList, substr($extendTagProps['name'], 1));
                        //  }
                    }
                    $data = array_merge($data, [
                        'tags' => $tagList,
                    ]);
                }
                //$organizer->get('icon')->get('url')
                $data = array_merge(
                    $data,
                    [
                        'organizer' =>
                                            [
                                                'name' => $organizer->get('preferredUsername'),
                                                'url' => $object->get("attributedTo"),
                                                'profilThumbImageUrl' => $organizer->get('icon') ? $organizer->get('icon')->get('url') : Yii::get('module')->getModule('co2')->assetsUrl . "/images/avatar.jpg",
                                            ],
                    ]
                );
            }
        }
        return $data;
    }

    /**
     * Removes duplicate values from an array.
     *
     * @param array $array The array to deduplicate.
     * @return array The deduplicated array.
     */
    public function deduplicateArray(array $array): array
    {
        $uniqueArray = [];
        foreach ($array as $element) {
            if (! in_array($element, $uniqueArray)) {
                $uniqueArray[] = $element;
            }
        }
        return $uniqueArray;
    }

    /**
     * Adds or removes a value from an array.
     *
     * @param array $my_array The array to modify.
     * @param array $value The value to add or remove.
     * @return array The modified array.
     */
    public function addOrRemoveIntoArray(array $my_array, array $value): array
    {
        $result = [];
        if (in_array($value[0], $my_array)) {
            $result = array_diff($my_array, $value);
        } else {
            $result = array_merge($my_array, $value);
        }
        return $result;
    }

    /**
     * Adds the given value to the array if it is not already present.
     *
     * @param array $my_array The array to check and modify.
     * @param array $value The value to add to the array.
     * @return array The modified array.
     */
    public function addIfNotInArray(array $my_array, array $value): array
    {
        $result = [];
        if (! in_array($value[0], $my_array)) {
            $result = array_merge($my_array, $value);
        } else {
            $result = $my_array;
        }
        return $result;
    }

    /**
     * Removes the elements from the given array if they exist in the value array.
     *
     * @param array $my_array The array from which elements will be removed.
     * @param array $value The array containing elements to be removed.
     * @return array The modified array after removing the elements.
     */
    public function removeIfExistInArray(array $my_array, array $value): array
    {
        $result = [];
        if (in_array($value[0], $my_array)) {
            $result = array_diff($my_array, $value);
        } else {
            $result = $my_array;
        }
        return $result;
    }

    /**
     * Slugify a tag.
     *
     * @param string $text The tag text to slugify.
     * @param int|null $length The maximum length of the slug (optional).
     * @return string The slugified tag.
     */
    public function slugifyTag(string $text, ?int $length = null): string
    {
        $replacements = [
            '<' => '',
            '>' => '',
            '-' => ' ',
            '&' => '',
            '"' => '',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'Ae',
            'Ä' => 'A',
            'Å' => 'A',
            'Ā' => 'A',
            'Ą' => 'A',
            'Ă' => 'A',
            'Æ' => 'Ae',
            'Ç' => 'C',
            "'" => '',
            'Ć' => 'C',
            'Č' => 'C',
            'Ĉ' => 'C',
            'Ċ' => 'C',
            'Ď' => 'D',
            'Đ' => 'D',
            'Ð' => 'D',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ē' => 'E',
            'Ę' => 'E',
            'Ě' => 'E',
            'Ĕ' => 'E',
            'Ė' => 'E',
            'Ĝ' => 'G',
            'Ğ' => 'G',
            'Ġ' => 'G',
            'Ģ' => 'G',
            'Ĥ' => 'H',
            'Ħ' => 'H',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ī' => 'I',
            'Ĩ' => 'I',
            'Ĭ' => 'I',
            'Į' => 'I',
            'İ' => 'I',
            'Ĳ' => 'IJ',
            'Ĵ' => 'J',
            'Ķ' => 'K',
            'Ł' => 'L',
            'Ľ' => 'L',
            'Ĺ' => 'L',
            'Ļ' => 'L',
            'Ŀ' => 'L',
            'Ñ' => 'N',
            'Ń' => 'N',
            'Ň' => 'N',
            'Ņ' => 'N',
            'Ŋ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'Oe',
            'Ö' => 'Oe',
            'Ø' => 'O',
            'Ō' => 'O',
            'Ő' => 'O',
            'Ŏ' => 'O',
            'Œ' => 'OE',
            'Ŕ' => 'R',
            'Ř' => 'R',
            'Ŗ' => 'R',
            'Ś' => 'S',
            'Š' => 'S',
            'Ş' => 'S',
            'Ŝ' => 'S',
            'Ș' => 'S',
            'Ť' => 'T',
            'Ţ' => 'T',
            'Ŧ' => 'T',
            'Ț' => 'T',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'Ue',
            'Ū' => 'U',
            'Ü' => 'Ue',
            'Ů' => 'U',
            'Ű' => 'U',
            'Ŭ' => 'U',
            'Ũ' => 'U',
            'Ų' => 'U',
            'Ŵ' => 'W',
            'Ý' => 'Y',
            'Ŷ' => 'Y',
            'Ÿ' => 'Y',
            'Ź' => 'Z',
            'Ž' => 'Z',
            'Ż' => 'Z',
            'Þ' => 'T',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'ae',
            'ä' => 'ae',
            'å' => 'a',
            'ā' => 'a',
            'ą' => 'a',
            'ă' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'ć' => 'c',
            'č' => 'c',
            'ĉ' => 'c',
            'ċ' => 'c',
            'ď' => 'd',
            'đ' => 'd',
            'ð' => 'd',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ē' => 'e',
            'ę' => 'e',
            'ě' => 'e',
            'ĕ' => 'e',
            'ė' => 'e',
            'ƒ' => 'f',
            'ĝ' => 'g',
            'ğ' => 'g',
            'ġ' => 'g',
            'ģ' => 'g',
            'ĥ' => 'h',
            'ħ' => 'h',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ī' => 'i',
            'ĩ' => 'i',
            'ĭ' => 'i',
            'į' => 'i',
            'ı' => 'i',
            'ĳ' => 'ij',
            'ĵ' => 'j',
            'ķ' => 'k',
            'ĸ' => 'k',
            'ł' => 'l',
            'ľ' => 'l',
            'ĺ' => 'l',
            'ļ' => 'l',
            'ŀ' => 'l',
            'ñ' => 'n',
            'ń' => 'n',
            'ň' => 'n',
            'ņ' => 'n',
            'ŉ' => 'n',
            'ŋ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'oe',
            'ö' => 'oe',
            'ø' => 'o',
            'ō' => 'o',
            'ő' => 'o',
            'ŏ' => 'o',
            'œ' => 'oe',
            'ŕ' => 'r',
            'ř' => 'r',
            'ŗ' => 'r',
            'š' => 's',
            'ś' => 's',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'ue',
            'ū' => 'u',
            'ü' => 'ue',
            'ů' => 'u',
            'ű' => 'u',
            'ŭ' => 'u',
            'ũ' => 'u',
            'ų' => 'u',
            'ŵ' => 'w',
            'ý' => 'y',
            'ÿ' => 'y',
            'ŷ' => 'y',
            'ž' => 'z',
            'ż' => 'z',
            'ź' => 'z',
            'þ' => 't',
            'α' => 'a',
            'ß' => 'ss',
            'ẞ' => 'b',
            'ſ' => 'ss',
            'ый' => 'iy',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'YO',
            'Ж' => 'ZH',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'CH',
            'Ш' => 'SH',
            'Щ' => 'SCH',
            'Ъ' => '',
            'Ы' => 'Y',
            'Ь' => '',
            'Э' => 'E',
            'Ю' => 'YU',
            'Я' => 'YA',
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'yo',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sch',
            'ъ' => '',
            'ы' => 'y',
            'ь' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            '.' => '-',
            '€' => '-eur-',
            '$' => '-usd-',
        ];
        $text = strtr($text, $replacements);
        $text = preg_replace('~[^\pL\d.]+~u', '-', $text);
        $text = preg_replace('~[^\-\w.]+~', '-', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (isset($length) && $length < strlen($text)) {
            $text = rtrim(substr($text, 0, $length), '-');
        }
        return $text;
    }

    /**
     * Converts a coLocation array to an ActivityPub location array.
     *
     * @param array $location The coLocation array to convert.
     * @param string|null $uuid The UUID of the location.
     * @return array The converted ActivityPub location array.
     */
    public function coLocationToActivitypubLocation(array $location, ?string $uuid): array
    {
        $array = [];
        if (isset($location['address'])) {
            if (isset($uuid)) {
                $array['id'] = "https://somemobilizon.instance/address/" . $uuid;
            }
            if (isset($location['address']['addressCountry'])) {
                $array['address']['addressCountry'] = $location['address']['addressCountry'];
            }
            if (isset($location['address']['addressLocality'])) {
                $array['address']['addressLocality'] = $location['address']['addressLocality'];
            }
            if (isset($location['address']['level1Name'])) {
                $array['address']['addressRegion'] = $location['address']['level1Name'];
            }

            if (isset($location['address']['postalCode'])) {
                $array['address']['postalCode'] = $location['address']['postalCode'];
            }
            if (isset($location['address']['streetAddress'])) {
                $array['address']['streetAddress'] = $location['address']['streetAddress'];
            }
            if (isset($location['address']['@type'])) {
                $array['address']['type'] = $location['address']['@type'];
            }

            if (isset($location['geoPosition']['latitude'])) {
                $array['latitude'] = $location['geoPosition']['latitude'];
            }
            if (isset($location['geoPosition']['longitude'])) {
                $array['longitude'] = $location['geoPosition']['longitude'];
            }
            if (isset($location["addresses"])) {
                $array['name'] = $location["addresses"];
            }
            $array['type'] = "Place";
        }
        return $array;
    }

    /**
     * Converts a coTag with a specified type to an ActivityPub tag.
     *
     * @param array|null $tags The coTags to convert.
     * @param string $type The type of the coTags.
     * @return array The converted ActivityPub tags.
     */
    public function coTagWithTypeToActivitypubTag(?array $tags = null, string $type): array
    {
        $tagArray = [];
        if (isset($tags) && is_array($tags)) {
            foreach ($tags as $tag) {
                $tagArray[] = [
                    "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($tag),
                    "name" => "#" . $tag,
                    "type" => "Hashtag",
                    "category" => "tag",
                ];
            }
        }
        $tagArray[] = [
            "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($type),
            "name" => "#" . $type,
            "type" => "Hashtag",
            "category" => "type",
        ];
        return $tagArray;
    }

    /**
     * Converts a coTag to an Activitypub tag for editing.
     *
     * @param array|null $exist The existing tags.
     * @param array $tags The new tags.
     * @return array The converted Activitypub tags.
     */
    public function coTagToActivitypubTagEdit(?array $exist, array $tags): array
    {
        if ($exist == null) {
            $exist = [];
        }
        $tagArray = [];
        foreach ($tags as $tag) {
            $tagArray[] = [
                "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($tag),
                "name" => "#" . $tag,
                "type" => "Hashtag",
                "category" => "tag",
            ];
        }

        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        // Supprimer les tags existants qui ne sont plus présents dans $tags
        foreach ($exist as $key => $value) {
            $found = false;
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href'] && $v['category'] === 'tag' && $v['type'] === 'Hashtag') {
                    $found = true;
                    break;
                }
            }
            if (! $found && $value['type'] === 'Hashtag' && $value['category'] === 'tag') {
                unset($exist[$key]);
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        }
        return $this->deduplicateArray($exist);
    }

    /**
     * Combines the existing tags with a new tag for an ActivityPub edit operation.
     *
     * @param array|null $exist The existing tags.
     * @param string $type The new tag to be added.
     * @return array The combined tags.
     */
    public function coTypeCombineToActivitypubTagEdit(?array $exist, string $type): array
    {
        $tagArray = [];
        if ($exist == null) {
            $exist = [];
        }
        // Supprimer les tags existants avec une catégorie "type"
        foreach ($exist as $key => $value) {
            if (isset($value['category']) && $value['category'] === 'type') {
                unset($exist[$key]);
            }
        }
        $tagArray[] = [
            "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($type),
            "name" => "#" . $type,
            "type" => "Hashtag",
            "category" => "type",
        ];
        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        };

        return $this->deduplicateArray($exist);
    }

    /**
     * Converts a coTag with a given type to an ActivityPub tag for editing.
     *
     * @param array|null $exist The existing tags.
     * @param array $tags The new tags.
     * @param string $type The type of the tag.
     * @return array The converted ActivityPub tag.
     */
    public function coTagWithTypeToActivitypubTagEdit(?array $exist, array $tags, string $type): array
    {
        if ($exist == null) {
            $exist = [];
        }
        $tagArray = [];
        foreach ($tags as $tag) {
            $tagArray[] = [
                "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($tag),
                "name" => "#" . $tag,
                "type" => "Hashtag",
                "category" => "tag",
            ];
        }
        foreach ($exist as $key => $value) {
            if (@$value['category'] && $value['category'] === "type") {
                unset($exist[$key]);
            }
        }
        $tagArray[] = [
            "href" => "https://somemobilizon.instance/tags/" . $this->slugifyTag($type),
            "name" => "#" . $type,
            "type" => "Hashtag",
            "category" => "type",
        ];

        foreach ($exist as $key => $value) {
            foreach ($tagArray as $k => $v) {
                if (isset($v['href']) && $v['href'] === $value['href']) {
                    unset($exist[$key]);
                }
            }
        }
        foreach ($tagArray as $value) {
            array_push($exist, $value);
        }
        $tags = array_map('strtolower', $tags);
        foreach ($exist as $key => $value) {
            $existTagName = strtolower(substr($value['name'], 1));
            if ($existTagName != $type && ! in_array($existTagName, $tags)) {
                unset($exist[$key]);
            }
        }
        return $this->deduplicateArray($exist);
    }

    /**
     * Converts a social network array to an ActivityPub link array.
     *
     * @param array $socialNetwork The social network array to convert.
     * @return array The converted ActivityPub link array.
     */
    public function coSocialNetworkToActivitypubLink(array $socialNetwork): array
    {
        $array = [];
        if (@$socialNetwork["gitlab"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "gitlab",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["gitlab"],
            ];
        }
        if (@$socialNetwork["github"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "github",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["github"],
            ];
        }
        if (@$socialNetwork["diaspora"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "diaspora",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["diaspora"],
            ];
        }
        if (@$socialNetwork["mastodon"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "mastodon",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["mastodon"],
            ];
        }
        if (@$socialNetwork["telegram"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "telegram",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["telegram"],
            ];
        }
        if (@$socialNetwork["signal"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "signal",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["signal"],
            ];
        }
        if (@$socialNetwork["facebook"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "facebook",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["facebook"],
            ];
        }
        if (@$socialNetwork["twitter"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "twitter",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["twitter"],
            ];
        }
        if (@$socialNetwork["instagram"]) {
            $array[] = [
                "mediaType" => "text/html",
                "name" => "instagram",
                "type" => "Link",
                "category" => "socialNetwork",
                "url" => $socialNetwork["instagram"],
            ];
        }
        return $array;
    }

    /**
     * Converts a coAttachment to an Activitypub attachment.
     *
     * @param array $data The coAttachment data.
     * @param array|null $exist The existing attachment data (optional).
     * @return array The converted Activitypub attachment.
     */
    public function coAttachmentToActivitypubAttachment(array $data, ?array $exist = null): array
    {
        $url = [];
        $attachment = [];
        if (@$data['email']) {
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    if (isset($value['url']) && strpos($value['url'], 'mailto') !== false) {
                        unset($exist[$key]);
                    }
                }
            }
            array_push($attachment, [
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "mailto",
                "url" => "mailto:" . $data['email'],
            ]);
        }

        if (@$data['url']) {
            $url[] = [
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "url",
                "url" => $data['url'],
            ];
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    if (isset($url['url']) && $url['url'] === $value['url']) {
                        unset($exist[$key]);
                    }
                }
            }
            array_push($attachment, [
                "mediaType" => "text/html",
                "name" => "Website",
                "type" => "Link",
                "category" => "url",
                "url" => $data['url'],
            ]);
        }
        if (isset($data['profilBannerUrl'])) {
            array_push($attachment, [
                "mediaType" => $this->get_image_mime_type($data['profilBannerUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $data['profilBannerUrl'],
            ]);
        }
        if ((is_countable($this->coSocialNetworkToActivitypubLink($data)) ? count($this->coSocialNetworkToActivitypubLink($data)) : 0) > 0) {
            $socialNetwork = $this->coSocialNetworkToActivitypubLink($data);
            if (isset($exist) && is_array($exist)) {
                foreach ($exist as $key => $value) {
                    foreach ($socialNetwork as $k => $v) {
                        if (isset($v['url']) && $v['url'] === $value['url']) {
                            unset($exist[$key]);
                        }
                    }
                }
            }
            foreach ($socialNetwork as $sn) {
                array_push($attachment, $sn);
            }
        }
        if (isset($exist) && is_array($exist)) {
            return $this->mergeArrayByDomain($exist, $attachment);
        } else {
            return $attachment;
        }
    }

    /**
     * Converts a CoImage array to an ActivityPub Image array.
     *
     * @param array $res The CoImage array to convert.
     * @return array|null The converted ActivityPub Image array, or null if the conversion fails.
     */
    public function coImageToActivityPubImage(array $res): ?array
    {
        $arr = null;
        $attachment = [];
        if (isset($res['profilImageUrl'])) {
            $arr = array_merge($attachment, [
                "mediaType" => $this->get_image_mime_type($res['profilImageUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $res['profilImageUrl'],
            ]);
        } else {
            if (isset($res['docPath'])) {
                $imageUrl = $this->createUrl(substr($res['docPath'], 1));
                $arr = array_merge($attachment, [
                    "mediaType" => $this->get_image_mime_type($imageUrl),
                    "name" => "Banner",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl,
                ]);
            }
        }

        return $arr;
    }

    /**
     * Converts the coImageBanner to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param array $el The element array.
     * @return array The converted ActivityPub image.
     */
    public function coImageBannerToActivityPubImage(?array $objectArray, array $el): array
    {
        $attachment = ! empty($objectArray) ? $objectArray : [];
        // $imageUrl = $this->createUrl(substr($el, 1));
        // $arr = [];
        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Banner') {
                unset($attachment[$key]);
            }
        }
        if (isset($el['profilBannerUrl'])) {
            $attachment[] = [
                "mediaType" => $this->get_image_mime_type($el['profilBannerUrl']),
                "name" => "Banner",
                "type" => "Document",
                "category" => "image",
                "url" => $el['profilBannerUrl'],
            ];
        } else {
            if (isset($el['docPath'])) {
                $imageUrl = $this->createUrl(substr($el['docPath'], 1));
                $attachment[] = [
                    "mediaType" => $this->get_image_mime_type($imageUrl),
                    "name" => "Banner",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl,
                ];
            }
        }
        return $attachment;
    }

    /**
     * Converts a coImageProfil to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param array $el The element array.
     * @return array The converted ActivityPub image.
     */
    public function coImageProfilToActivityPubImage(?array $objectArray, array $el): array
    {
        $attachment = ! empty($objectArray) ? $objectArray : [];
        // $imageUrl = $this->createUrl(substr($el, 1));

        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Image') {
                unset($attachment[$key]);
            }
        }
        if (isset($el['profilImageUrl'])) {
            $attachment[] = [
                "mediaType" => $this->get_image_mime_type($el['profilImageUrl']),
                "name" => "Image",
                "type" => "Document",
                "category" => "image",
                "url" => $el['profilImageUrl'],
            ];
        } else {
            if (isset($el['docPath'])) {
                $imageUrl = $this->createUrl(substr($el['docPath'], 1));
                $attachment[] = [
                    "mediaType" => $this->get_image_mime_type($imageUrl),
                    "name" => "Image",
                    "type" => "Document",
                    "category" => "image",
                    "url" => $imageUrl,
                ];
            }
        }
        return $attachment;
    }

    /**
     * Converts a thumbnail to an ActivityPub image.
     *
     * @param array|null $objectArray The object array.
     * @param string $el The element.
     * @return array The converted image array.
     */
    public function coThumbToActivityPubImage(?array $objectArray, string $el): array
    {
        $attachment = ! empty($objectArray) ? $objectArray : [];
        $imageUrl = $this->createUrl(substr($el, 1));
        foreach ($attachment as $key => $value) {
            if (isset($value['name']) && $value['name'] == 'Banner') {
                unset($attachment[$key]);
            }
        }
        $arr = array_merge($attachment, [
            "mediaType" => $this->get_image_mime_type($el),
            "name" => "Banner",
            "type" => "Document",
            "category" => "image",
            "url" => $imageUrl,
        ]);
        return $arr;
    }

    /**
     * Merge two arrays by domain.
     *
     * @param array $exist The existing array.
     * @param array $newData The new data array.
     * @return array The merged array.
     */
    public function mergeArrayByDomain(array $exist, array $newData): array
    {
        $existDomains = array_map(function ($value) {
            $parsedUrl = parse_url($value['url']);
            return $parsedUrl['host'] ?? '';
        }, $exist);

        $mergedArray = [];

        foreach ($newData as $value) {
            $parsedUrl = parse_url($value['url']);
            $domain = $parsedUrl['host'] ?? '';

            $existingKey = array_search($domain, $existDomains);
            if ($existingKey !== false) {
                if ($exist[$existingKey]['url'] != $value['url']) {
                    $exist[$existingKey]['url'] = $value['url'];
                }
            } else {
                $existDomains[] = $domain;
                $exist[] = $value;
            }
        }

        return $exist;
    }

    /**
     * Check if the given instance is a Mobilizon instance.
     *
     * @param string $instance The instance to check.
     * @return bool Returns true if the instance is a Mobilizon instance, false otherwise.
     */
    public function isMobilizonInstance(string $instance): bool
    {
        $url = 'https://instances.joinmobilizon.org/api/v1/instances?start=0&count=10&sort=totalLocalGroups&search=' . $this->getWebsiteDomain($instance);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $res = curl_exec($curl);
        curl_close($curl);
        $json = $this->decodeJson($res);
        return $json['total'] > 0;
    }

    /**
     * This method takes an array of job parameters and returns an array of modified job parameters.
     *
     * @param array $job The job parameters to be modified.
     * @return array The modified job parameters.
     */
    public function jobToRunBeforePost(array $job): array
    {
        if ($this->isMobilizonInstance(preg_replace('/\/inbox$/', '', $job["receiver"]))) {
            $job["data"]["id"] = str_replace('somemobilizon.instance', $this->getWebsiteDomain($job["receiver"]), $job["data"]["id"]);
            $job["data"]["object"]["attributedTo"] = preg_replace('/\/inbox$/', '', $job["receiver"]);
            $job["data"]["object"]["id"] = str_replace('somemobilizon.instance', $this->getWebsiteDomain($job["receiver"]), $job["data"]["object"]["id"]);
            $job["data"]["object"]["url"] = str_replace('somemobilizon.instance', $this->getWebsiteDomain($job["receiver"]), $job["data"]["object"]["url"]);

            if (@$job["data"]["object"]["tag"]) {
                $tags = $job["data"]["object"]["tag"];
                foreach ($tags as $x => $val) {
                    $job["data"]["object"]["tag"][$x]["href"] = str_replace('somemobilizon.instance', $this->getWebsiteDomain($job["receiver"]), $job["data"]["object"]["tag"][$x]["href"]);
                }
            }
            if (@$job["data"]["object"]["location"] && (is_countable($job["data"]["object"]["location"]) ? count($job["data"]["object"]["location"]) : 0) > 0) {
                $job["data"]["object"]["location"]["id"] = str_replace('somemobilizon.instance', $this->getWebsiteDomain($job["receiver"]), $job["data"]["object"]["location"]["id"]);
            } else {
                unset($job["data"]["object"]["location"]);
            }
        }
        return $job;
    }

    /**
     * Federates an element.
     *
     * @param array $toSave The element to federate.
     * @return array|null The federated element or null if it couldn't be federated.
     */
    public function federateElement(array $toSave): ?array
    {
        $activity = null;
        $toSave['isEdit'] = false;
        if (isset($toSave['typeElement'])) {
            $toSave['collection'] = $toSave['typeElement'];
            $toSave['isEdit'] = true;
        }

        if ($toSave['collection'] == 'projects' && (isset($toSave['startDate']) || isset($toSave['endDate']))) {
            $toSave['isEdit'] = true;
        }

        switch ($toSave['collection']) {
            case "events":
                $activity = $this->getModelActivitypubActivityCreator()->createOrUpdateEventActivity($toSave);
                break;
            case "projects":
                $activity = $this->getModelActivitypubActivityCreator()->createOrUpdateProjectActivity($toSave);
                break;
            default:
                $activity = null;
                break;
        }

        if ($activity == null) {
            return null;
        }
        Handler::handle($this->getModelTypeAp(), $activity);
        return $activity->toArray();
    }

    /**
     * Returns an array of actor follows.
     *
     * @return array The list of actor follows.
     */
    public function actorFollowsList(): array
    {
        $follows = [];

        $userId = $this->session->getUserId();
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $userId);
        if (isset($user["links"]["activitypub"]["follows"])) {
            foreach ($user["links"]["activitypub"]["follows"] as $follow) {
                if (! isset($follow["pending"])) {
                    array_push($follows, $follow["invitorId"]);
                }
            }
        }
        $groups = $this->db->find(OrganizationInterface::COLLECTION, [
            "creator" => $userId,
        ]);
        foreach ($groups as $v) {
            if (isset($v["links"]["activitypub"]["follows"])) {
                foreach ($v["links"]["activitypub"]["follows"] as $follow) {
                    if (! isset($follow["pending"])) {
                        array_push($follows, $follow["invitorId"]);
                    }
                }
            }
        }

        return $follows;
    }

    /**
     * Check if ActivityPub is enabled.
     *
     * @return bool Returns true if ActivityPub is enabled, false otherwise.
     */
    public function isActivitypubEnabled(): bool
    {
        $connectedUserId = $this->session->getUserId();
        $connectedUser = $this->getModelPerson()->getById($connectedUserId);
        if (! isset($connectedUser)) {
            $activitypubEnable = false;
        }
        $activitypubEnable = $this->getModelPreference()->isActivitypubActivate(@$connectedUser["preferences"]);
        return $activitypubEnable;
    }

    /**
     * Check if the given actor is a local actor.
     *
     * @param AbstractActorInterface|null $actor The actor to check. Defaults to null.
     * @return bool Returns true if the actor is local, false otherwise.
     */
    public function isLocalActor(?AbstractActorInterface $actor = null): bool
    {
        if ($actor == null) {
            return $_SERVER['HTTP_HOST'] == Config::HOST;
        } else {
            $domain = parse_url($actor->get('id'), PHP_URL_HOST);
            return $domain == Config::HOST;
        }
    }

    /**
     * Returns a link to the address.
     *
     * @param string $url The URL of the address.
     * @return string|null The link to the address, or null if the URL is empty.
     */
    public static function getLinkToAdress(string $url): ?string
    {
        // Vérifier si l'URL utilise la méthode "u" ou le format "username@domain"
        if (strpos($url, '/u/') !== false) {
            $parts = explode('/u/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } elseif (strpos($url, '/p/') !== false) {
            $parts = explode('/p/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else {
            preg_match('/^https?:\/\/(.+)@(.+)$/', $url, $matches);
            if (count($matches) !== 3) {
                return null;
            }
            $username = $matches[1];
        }

        // Construire l'adresse ActivityPub en utilisant le nom d'utilisateur et le domaine
        $domain = parse_url($url, PHP_URL_HOST);
        return '@' . $username . '@' . $domain;
    }

    /**
     * Returns the link to the username based on the given URL.
     *
     * @param string $url The URL to extract the username from.
     * @return string|null The link to the username, or null if the URL is invalid.
     */
    public static function getLinkToUsername(string $url): ?string
    {
        // Vérifier si l'URL utilise la méthode "u" ou le format "username@domain"
        if (strpos($url, '/u/') !== false) {
            $parts = explode('/u/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } elseif (strpos($url, '/p/') !== false) {
            $parts = explode('/p/', $url);
            if (count($parts) !== 2) {
                return null;
            }
            $username = $parts[1];
        } else {
            preg_match('/^https?:\/\/(.+)@(.+)$/', $url, $matches);
            if (count($matches) !== 3) {
                return null;
            }
            $username = $matches[1];
        }
        return $username;
    }

    /**
     * Check if the given array of links contains a specific connection type.
     *
     * @param array $links The array of links to check.
     * @param string $connectType The connection type to search for.
     * @return bool Returns true if the array contains the connection type, false otherwise.
     */
    public function checkIsContainLinks(array $links, string $connectType): bool
    {
        $exists = true;

        if (@$links["activitypub"] && isset($links[$connectType][$this->session->getUserId()]) == false) {
            $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
            if (! $actor) {
                return false;
            }
            if (@$links["activitypub"][$connectType]) {
                $exists = array_reduce($links["activitypub"][$connectType], fn ($carry, $item) => $carry || @$item['invitorId'] == $actor->get('id'), false);
            }
        } else {
            $exists = isset($links[$connectType][$this->session->getUserId()]);
        }

        return $exists;
    }

    /**
     * Check if a link exists in an array of links based on the connection type and link type.
     *
     * @param array $links The array of links to check.
     * @param string $connectType The connection type to match.
     * @param string $linkType The link type to match.
     * @return bool Returns true if a link matching the connection type and link type is found, false otherwise.
     */
    public function checkByLinkType(array $links, string $connectType, string $linkType): bool
    {
        $exists = false;
        if (@$links["activitypub"] && $this->session->getUserId()) {
            $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());

            if (! $actor) {
                return false;
            }
            if (@$links["activitypub"][$connectType]) {
                foreach ($links["activitypub"][$connectType] as $key => $value) {
                    if (@$value['invitorId'] == $actor->get('id')) {
                        $exists = isset($value[$linkType]);
                    }
                }
            }
        } else {
            $exists = isset($links[$connectType][$this->session->getUserId()][$linkType]);
        }
        return $exists;
    }

    /**
     * Retrieves information about the links.
     *
     * @param array $links The array of links.
     * @param string $connectType The type of connection.
     * @return mixed The information about the links.
     */
    public function getLinksInfo(array $links, string $connectType)
    {
        $exists = null;
        if (@$links["activitypub"] && $this->session->getUserId()) {
            $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
            if (! $actor) {
                return false;
            }
            if (@$links["activitypub"][$connectType]) {
                foreach ($links["activitypub"][$connectType] as $key => $value) {
                    if (@$value['invitorId'] == $actor->get('id')) {
                        $exists = $value;
                    }
                }
            }
        } else {
            $exists = $links[$connectType][$this->session->getUserId()];
        }
        return $exists;
    }

    /**
     * Returns a list of admins for a given parent entity.
     *
     * @param string $parentId The ID of the parent entity.
     * @param string $parentType The type of the parent entity.
     * @return array|bool The list of admins.
     */
    public function listAdmins(string $parentId, string $parentType)
    {
        $project = [];
        $contributors = [];
        if (in_array($parentType, [ProjectInterface::COLLECTION])) {
            $project = $this->db->findOne($parentType, [
                '$or' => [
                    [
                        "_id" => $this->db->MongoId($parentId),
                    ],
                    [
                        "objectId" => $parentId,
                    ],
                ],
            ]);
        } else {
            return false;
        }
        if (! $project) {
            return [];
        }
        if (@$project["links"]["activitypub"]) {
            if (@$project["links"]["activitypub"]["contributors"]) {
                foreach ($project["links"]["activitypub"]["contributors"] as $key => $value) {
                    if (@$value['isAdmin'] == true) {
                        if (@$value["isAdminPending"] == null || @$value["isAdminPending"] == false) {
                            array_push($contributors, $value['invitorId']);
                        }
                    }
                }
            }
        }
        return $contributors;
    }

    /**
     * Check if the given parent is an admin.
     *
     * @param string $parentId The ID of the parent.
     * @param string $parentType The type of the parent.
     * @return bool Returns true if the parent is an admin, false otherwise.
     */
    public function checkIfIsAdmin(string $parentId, string $parentType): bool
    {
        if ($this->session->getUserId()) {
            $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
            $admins = $this->listAdmins($parentId, $parentType);
            return in_array($actor->get('id'), $admins);
        } else {
            return false;
        }
    }

    /**
     * Builds a URL from an array of parts.
     *
     * @param array $parts The array of parts to build the URL from.
     * @return string The built URL.
     */
    public function buildUrlFromParts(array $parts): string
    {
        $scheme = isset($parts['scheme']) ? $parts['scheme'] . '://' : '';
        $host = $parts['host'] ?? '';
        $port = isset($parts['port']) ? ':' . $parts['port'] : '';
        $user = $parts['user'] ?? '';
        $pass = isset($parts['pass']) ? ':' . $parts['pass'] : '';
        $pass = ($user || $pass) ? "$pass@" : '';
        $path = $parts['path'] ?? '';
        $query = isset($parts['query']) ? '?' . $parts['query'] : '';
        $fragment = isset($parts['fragment']) ? '#' . $parts['fragment'] : '';

        return "$scheme$user$pass$host$port$path$query$fragment";
    }

    /**
     * Check if the co-user is a follower of a person.
     *
     * @param mixed $person The person to check if the co-user is a follower of.
     * @return bool Returns true if the co-user is a follower, false otherwise.
     */
    public function checkIfCoUserIsFollower($person): bool
    {
        $exists = false;
        if ($this->session->getUserId()) {
            $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
            if (! $user) {
                return false;
            }
            $exists = array_reduce($user["links"]["activitypub"]["follows"], fn ($carry, $item) => $carry || @$item['invitorId'] == $person, false);
        } else {
            return false;
        }
        return $exists;
    }

    /**
     * Check if the given element is a local share in the Fediverse.
     *
     * @param array $element The element to check.
     * @return bool Returns true if the element is a local share, false otherwise.
     */
    public function isFediverseShareLocal(array $element): bool
    {
        if (isset($element['fromActivityPub'])) {
            if ($this->session->getUserId() && isset($element['attributedTo'])) {
                $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
                if ($element['attributedTo'] == $actor->get('id')) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Converts an ActivityPub object to an Element.
     *
     * @param mixed $objectId The ID of the ActivityPub object.
     * @return array The converted Element.
     */
    public function activitypubToElement($objectId): array
    {
        $parent = [];
        $connectedUserId = $this->session->getUserId();
        $connectedUser = $this->getModelPerson()->getById($connectedUserId);
        $activitypubEnable = $this->getModelPreference()->isActivitypubActivate(@$connectedUser["preferences"]);
        $data = [];
        $socialNetwork = [];
        if ($activitypubEnable) {
            $object = $this->getModeltypeAp()->createFromAnyValue($objectId);
            $data = [
                "collection" => $this->getCollection($object),
                "name" => strtoupper($object->get('name')),
                "created" => strtotime($object->get('published')),
                "updated" => strtotime($object->get('updated')),
                "shortDescription" => $object->get("content") != "" ? $this->substrwords($object->get("content"), 140) : "",
                "providerInfo" => [
                    "name" => $this->getWebsiteDomain($object->get('id')),
                    "icon" => $this->getWebsiteFavicon($object->get('id')),
                    "url" => $this->getWebsiteBaseUrl($object->get('id')),
                ],
                "slug" => $this->getModelSlug()->checkAndCreateSlug($object->get('name')),
                "description" => $object->get("content"),
                "objectId" => $objectId,

            ];
            if (! empty($object->get("attributedTo"))) {
                $collectionType = $this->getCollectionTypeByUrl($object->get("attributedTo"));
                $actor = $this->getModeltypeAp()->createFromAnyValue($object->get("attributedTo"));
                $parent['type'] = $actor->get('type');
                $parent['id'] = $object->get("attributedTo");
                if (! empty($actor->get('icon'))) {
                    $parent['profilThumbImageUrl'] = $actor->get('icon')->get('url');
                }
                $parent['name'] = $actor->get('name');
                $parent['activitypub'] = true;

                $data = array_merge($data, [
                    'parent' =>
                                    [
                                        $object->get("attributedTo") => $parent,
                                    ],
                ]);
            }
            if ($object->get('_extend_props')) {
                $objectExtendProps = $object->get('_extend_props');

                if (isset($objectExtendProps['category'])) {
                    $data = array_merge($data, [
                        'type' => ucfirst(strtolower($objectExtendProps['category'])),
                    ]);
                }
                if (isset($objectExtendProps['timezone'])) {
                    $data = array_merge($data, [
                        'timeZone' => $objectExtendProps['timezone'],
                    ]);
                }

                if (isset($objectExtendProps['progress'])) {
                    $data = array_merge($data, [
                        'properties' => [
                            'avancement' => $objectExtendProps['progress'],
                        ],
                    ]);
                }
                if (isset($objectExtendProps['email'])) {
                    $data = array_merge($data, [
                        'email' => $objectExtendProps['email'],
                    ]);
                }
            }
            if (! empty($object->get('startTime'))) {
                $data["startDate"] = $object->get('startTime');
            }

            if (! empty($object->get('endTime'))) {
                $data["endDate"] = $object->get('endTime');
            }
            if (! empty($object->get('content'))) {
                $data["description"] = $object->get('content');
            }
            if (@$object->get('location') && $object->get('location') != null) {
                $location = $object->get('location');
                $locationExtendProps = $location->get('_extend_props');
                $addressExtendProps = $locationExtendProps['address']->get('_extend_props');

                if (@$addressExtendProps && $addressExtendProps != null) {
                    $geo = [
                        "@type" => "GeoCoordinates",
                        "latitude" => $location->get('latitude'),
                        "longitude" => $location->get('longitude'),
                    ];
                    $geoPosition = [
                        "type" => $location->get('type'),
                        "coordinates" => [$location->get('longitude'), $location->get('latitude')],
                    ];

                    $data = array_merge($data, [
                        "address" =>
                                            [
                                                '@type' => $addressExtendProps['type'],
                                                'streetAddress' => $location->get('name') . " " . @$addressExtendProps['addressRegion'] && $addressExtendProps['addressRegion'] != null ? @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] . " " . $addressExtendProps['addressRegion'] : $addressExtendProps['addressRegion'] : "",
                                                'postalCode' => @$addressExtendProps['postalCode'] && $addressExtendProps['postalCode'] != null ? $addressExtendProps['postalCode'] : "",
                                            ],
                        'addressLocality' => @$addressExtendProps['addressLocality'] && $addressExtendProps['addressLocality'] != null ? $addressExtendProps['addressLocality'] : "",
                        "geo" => $geo,
                    ]);
                }
            }

            if (@$object->get('tag') && $object->get('tag') != null) {
                $tags = $object->get('tag');
                $tagList = [];
                foreach ($tags as $tagData) {
                    $extendTagProps = $tagData->get('_extend_props');
                    array_push($tagList, substr($extendTagProps['name'], 1));
                }
                $data = array_merge($data, [
                    'tags' => $tagList,
                ]);
            }
            if (! empty($object->get('attachment'))) {
                $attachements = $object->get('attachment');
                foreach ($attachements as $attachement) {
                    $extendAttachementsProps = $attachement->get('_extend_props');
                    if ($extendAttachementsProps != null) {
                        if ($extendAttachementsProps['category'] == 'url') {
                            $data['url'] = $extendAttachementsProps['url'];
                        } elseif ($extendAttachementsProps['category'] == 'socialNetwork') {
                            $socialNetwork[$attachement->get('name')] = $extendAttachementsProps['url'];
                            $data = array_merge($data, [
                                'socialNetwork' => $socialNetwork,
                            ]);
                        } else {
                            if ($attachement->get('name') == 'Banner') {
                                if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                                    $urlImg = $attachement->get('url');
                                    $data = array_merge($data, [
                                        'profilBannerUrl' => $urlImg,
                                        'profilRealUrl' => $urlImg,
                                    ]);
                                }
                            } else {
                                if ($attachement->get('mediaType') == "image/jpeg" || $attachement->get('mediaType') == "image/png" || $attachement->get('mediaType') == "image/webp" || $attachement->get('mediaType') == "image/jpg" || $attachement->get('mediaType') == "image/svg+xml") {
                                    $urlImg = $attachement->get('url');
                                    $data = array_merge($data, [
                                        'profilImageUrl' => $urlImg,
                                        'profilMediumImageUrl' => $urlImg,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Returns the collection of the given object.
     *
     * @param AbstractObjectInterface $object The object for which to retrieve the collection.
     * @return string The collection of the object.
     */
    public function getCollection(AbstractObjectInterface $object): string
    {
        switch ($object->get('type')) {
            case 'Project':
                return 'projects';
            case 'Event':
                return 'events';
            case 'Person':
                return 'person';
        }
        throw new Exception('Unknown object type: ' . (string) $object->get('type'));
    }

    /**
     * Retrieves the collection type based on the provided URL.
     *
     * @param string $url The URL of the collection.
     * @return array The collection type.
     */
    public function getCollectionTypeByUrl(string $url): array
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } elseif (strpos($url, 'projects') !== false) {
            return [
                "name" => $basename,
                "type" => "projects",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }

    /**
     * Filters the results for the ActivityPub flux.
     *
     * @param array $results The results to filter.
     * @return array The filtered results.
     */
    public function filterResultsForActivityPubFlux(array $results): array
    {
        if ($this->isActivitypubEnabled()) {
            foreach ($results["results"] as $key => $value) {
                if (isset($value['fromActivityPub']) && isset($value['attributedTo'])) {
                    if ($this->checkIfCoUserIsFollower($value['attributedTo'])) {
                        $results["results"][$key]["providerInfo"] = [
                            "name" => $this->getWebsiteDomain($results["results"][$key]["objectId"]),
                            "icon" => $this->getWebsiteFavicon($results["results"][$key]["objectId"]),
                            "url" => $this->getWebsiteBaseUrl($results["results"][$key]["objectId"]),
                        ];
                    } else {
                        unset($results["results"][$key]);
                    }
                }
            }
        } else {
            // si activitypub ne pas activé on retire tous les elements qui vient du fediverse
            foreach ($results["results"] as $key => $value) {
                if (isset($value['fromActivityPub'])) {
                    unset($results["results"][$key]);
                }
            }
        }
        return $results;
    }

    /**
     * Filters the search admin data for the ActivityPub flux.
     *
     * @param string $type The type of the data.
     * @param string $id The ID of the data.
     * @param array $post The post data.
     * @param array $results The search results.
     * @return array The filtered search admin data.
     */
    public function filterSearchAdminDataForActivityPubFlux(string $type, string $id, array $post, array $results): array
    {
        if ($this->isActivitypubEnabled()) {
            if ($type == 'projects') {
                $projectInfo = $this->db->findOneById(ProjectInterface::COLLECTION, $id);

                if (isset($projectInfo['links']['activitypub'])) {
                    if (isset($post['filters']) && isset($post['filters']['links.projects.' . $id])) {
                        if (isset($projectInfo['links']['activitypub']['contributors'])) {
                            $contributors = $projectInfo['links']['activitypub']['contributors'];
                            $results['count']['citoyens'] = is_countable($contributors) ? count($contributors) : 0;
                            foreach ($contributors as $key => $value) {
                                $results['results'][$value['invitorId']] = $this->getModelActivitypubTranslator()->actorTocoUser($value);
                                $results['results'][$value['invitorId']]['projectCreator'] = $projectInfo['attributedTo'];
                            }
                        }
                    }
                    if (isset($post['filters']) && isset($post['filters']['links.follows.' . $id])) {
                        if (isset($projectInfo['links']['activitypub']['followers'])) {
                            $followers = $projectInfo['links']['activitypub']['followers'];
                            $results['count']['citoyens'] = is_countable($followers) ? count($followers) : 0;
                            foreach ($followers as $key => $value) {
                                $results['results'][$value['invitorId']] = $this->getModelActivitypubTranslator()->actorTocoUser($value);
                                $results['results'][$value['invitorId']]['projectCreator'] = $projectInfo['attributedTo'];
                            }
                        }
                    }
                }
            }
        }
        return $results;
    }

    /**
     * Display the first 30 words of a given text.
     *
     * @param string $text The text to display.
     * @return string The first 30 words of the text.
     */
    public function displayFirst30Words(string $text): string
    {
        $text = trim($text);
        $words = preg_split('/\s+/', $text);
        $first30Words = array_slice($words, 0, 30);
        $displayText = implode(' ', $first30Words);
        return $displayText;
    }
}
