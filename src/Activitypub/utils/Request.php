<?php

namespace PixelHumain\Models\Activitypub\utils;

use InvalidArgumentException;
use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\Traits\ActivitypubCronTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubCronTraitInterface;
use PixelHumain\Models\Activitypub\utils\Interfaces\RequestInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\CacheHelperInterface;

class Request extends BaseModel implements RequestInterface, ActivitypubCronTraitInterface, UtilsTraitInterface
{
    use ActivitypubCronTrait;
    use UtilsTrait;

    protected CacheHelperInterface $cacheHelper;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        if (! $this->cacheHelper instanceof CacheHelperInterface) {
            throw new InvalidArgumentException("cacheHelper must be an instance of CacheHelperInterface");
        }
    }

    /**
     * Sends a POST request to the specified receiver.
     *
     * @param string $sender The sender of the request.
     * @param string $receiver The receiver of the request.
     * @param mixed $data The data to be sent in the request.
     * @param bool $addToCron (optional) Whether to add the request to the cron job queue.
     * @return bool|void Returns true if the request was successful, false otherwise. If $addToCron is true, returns void.
     */
    public function post(string $sender, string $receiver, $data, bool $addToCron = false)
    {
        if ($addToCron) {
            $this->getModelActivitypubCron()->save($sender, $receiver, $data);
        } else {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $receiver);
            curl_setopt($ch, CURLOPT_HTTPHEADER, HttpSignature::sign($this->getModelUtils(), $receiver, $data, $sender));
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $error = curl_error($ch); // récupérer l'erreur cURL
            curl_close($ch);
            if (! empty($error)) {
                return false;
            } else {
                return ($httpcode >= 200 && $httpcode <= 299);
            }
        }
    }

    /**
     * Sends a GET request to the specified URL.
     *
     * @param string $url The URL to send the request to.
     * @param string $accept The value of the Accept header. Defaults to self::HEADER_ACCEPT.
     * @return mixed The response from the GET request.
     */
    public function get(string $url, string $accept = RequestInterface::HEADER_ACCEPT)
    {
        $isLocalUrl = parse_url($url, PHP_URL_HOST) == Config::HOST;

        if ($output = $this->cacheHelper->get($url)) {
            return $output;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Accept: " . $accept]);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        if (! $output) {
            return null;
        }
        $output = $this->getModelUtils()->decodeJson($output);

        if (! $isLocalUrl) {
            $this->cacheHelper->set($url, $output);
        }

        return $output;
    }
}
