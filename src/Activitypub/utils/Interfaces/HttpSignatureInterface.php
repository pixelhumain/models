<?php

namespace PixelHumain\Models\Activitypub\utils\Interfaces;

interface HttpSignatureInterface
{
    /**
     * Signs the HTTP request with a digital signature.
     *
     * @param UtilsInterface $utils The utility class for HTTP requests.
     * @param string $target The target URL of the request.
     * @param mixed $data The data to be included in the request.
     * @param string $keyId The identifier of the key used for signing.
     * @return array The signed HTTP request.
     */
    public static function sign(UtilsInterface $utils, string $target, $data, string $keyId): array;
}
