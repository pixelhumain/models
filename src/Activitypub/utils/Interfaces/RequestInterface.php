<?php

namespace PixelHumain\Models\Activitypub\utils\Interfaces;

interface RequestInterface
{
    public const HEADER_ACCEPT = 'application/activity+json';

    /**
     * Sends a POST request to the specified receiver.
     *
     * @param string $sender The sender of the request.
     * @param string $receiver The receiver of the request.
     * @param mixed $data The data to be sent in the request.
     * @param bool $addToCron (optional) Whether to add the request to the cron job queue.
     * @return bool|void Returns true if the request was successful, false otherwise. If $addToCron is true, returns void.
     */
    public function post(string $sender, string $receiver, $data, bool $addToCron = false);

    /**
     * Sends a GET request to the specified URL.
     *
     * @param string $url The URL to send the request to.
     * @param string $accept The value of the Accept header. Defaults to self::HEADER_ACCEPT.
     * @return mixed The response from the GET request.
     */
    public function get(string $url, string $accept = self::HEADER_ACCEPT);
}
