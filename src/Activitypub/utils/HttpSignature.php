<?php

namespace PixelHumain\Models\Activitypub\utils;

use DateTime;
use DateTimeZone;
use PixelHumain\Models\Activitypub\utils\Interfaces\HttpSignatureInterface;
use PixelHumain\Models\Activitypub\utils\Interfaces\RequestInterface;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

class HttpSignature implements HttpSignatureInterface
{
    /**
     * Signs the HTTP request with a digital signature.
     *
     * @param UtilsInterface $utils The utility class for HTTP requests.
     * @param string $target The target URL of the request.
     * @param mixed $data The data to be included in the request.
     * @param string $keyId The identifier of the key used for signing.
     * @return array The signed HTTP request.
     */
    public static function sign(UtilsInterface $utils, string $target, $data, string $keyId): array
    {
        $targetDomain = parse_url($target, PHP_URL_HOST);
        $targetPath = parse_url($target, PHP_URL_PATH);

        //get current date
        $utcDate = new DateTime("now", new DateTimeZone("UTC"));
        $utcDateString = $utcDate->format(DateTime::RFC7231);

        //digest data
        $digest = base64_encode(hash("sha256", json_encode($data), true));

        //define content of signature
        $stringToSign = "(request-target): post $targetPath\nhost: $targetDomain\ndate: $utcDateString\ndigest: SHA-256=$digest";
        openssl_sign($stringToSign, $signature, (string) $utils->getPem("private"), "sha256");
        $signature_b64 = base64_encode($signature);

        //define the header signature
        $headerSignature = 'keyId="' . $keyId . '",headers="(request-target) host date digest",signature="' . $signature_b64 . '"';

        return [
            "Host: " . $targetDomain,
            "Date: " . $utcDateString,
            "Digest: SHA-256=" . $digest,
            "Signature: " . $headerSignature,
            "Content-Type: " . RequestInterface::HEADER_ACCEPT,
        ];
    }
}
