<?php

namespace PixelHumain\Models\Activitypub;

use Exception;
use PhpParser\Node\Expr\Cast\Object_;
use PixelHumain\Models\Activitypub\handlers\Webfinger;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActorInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProjectTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProjectTrait;

class ActivitypubActor extends BaseModel implements ActivitypubActorInterface, PersonTraitInterface, OrganizationTraitInterface, ProjectTraitInterface, ActivitypubTranslatorTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use OrganizationTrait;
    use ProjectTrait;

    /**
     * Les traits activitypub
     */
    use ActivitypubTranslatorTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function getLocalFollowersOfExternalUser($actorId)
    {
        $followers = [];
        try {
            $users = $this->db->find(PersonInterface::COLLECTION, [
                "links.activitypub.follows" => [
                    '$elemMatch' => [
                        "invitorId" => $actorId,
                    ],
                ],
            ]);
            foreach ($users as $user) {
                $followers[] = $this->getModelActivitypubTranslator()->coPersonToActor($user);
            }
        } catch (Exception $e) {
        }

        return $followers;
    }

    public function getCoPersonAsActor($username): AbstractActorInterface
    {
        $user = $this->db->findOne(PersonInterface::COLLECTION, [
            "username" => $username,
        ]);
        if (! $user) {
            throw new Exception("User not found");
        }
        return $this->getModelActivitypubTranslator()->coPersonToActor($user);
    }

    public function searchActorByRessourceAddress($address): AbstractActorInterface
    {
        $parts = Webfinger::parseResource($address);
        $res = file_get_contents("https://" . $parts["host"] . "/.well-known/webfinger?resource=acct:" . $parts["user"] . "@" . $parts["host"]);
        $res = json_decode($res, true);
        if (! $res || ! isset($res["links"])) {
            throw new Exception("Remote data could not be fetch");
        }
        $data = null;
        foreach ($res["links"] as $link) {
            if ($link["rel"] == "self") {
                $data = $this->getModelTypeAp()->createFromAnyValue($link["href"]);
                break;
            }
        }
        if (! $data) {
            throw new Exception("Remote data could not be fetch");
        }
        return $data;
    }

    public function getCoGroupAsActor($slug): AbstractActorInterface
    {
        $group = $this->db->findOne(OrganizationInterface::COLLECTION, [
            "slug" => $slug,
        ]);

        if (! $group) {
            throw new Exception("Group not found");
        }
        return $this->getModelActivitypubTranslator()->coGroupToActor($group);
    }

    public function getCoProjectAsObject($slug): AbstractActorInterface
    {
        $project = $this->db->findOne(ProjectInterface::COLLECTION, [
            "slug" => $slug,
        ]);

        if (! $project) {
            throw new Exception("Project not found");
        }
        return $this->getModelActivitypubTranslator()->coProjectAsActor($project);
    }

    public function getCoPersonAsActorByUserId($param): AbstractActorInterface
    {
        $user = $this->db->findOne(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($param),
            ]
        );
        if (! $user) {
            throw new Exception("User not found");
        }
        return $this->getModelActivitypubTranslator()->coPersonToActor($user);
    }
}
