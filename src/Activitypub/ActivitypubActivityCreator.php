<?php

namespace PixelHumain\Models\Activitypub;

use ActivityHub;
use ActivityStr;
use DateTime;
use Error;
use Exception;
use Parsedown;

use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityCreatorInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubObjectTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubObjectTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\activity\Accept;
use PixelHumain\Models\Activitypub\type\extended\activity\Invite;
use PixelHumain\Models\Activitypub\type\extended\activity\Offer;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\Activitypub\utils\Utils;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\EventTrait;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\EventTraitInterface;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProjectTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProjectTrait;
use PixelHumain\Models\Traits\SlugTrait;

// TODO : $_SESSION je crois que l'on utilise $_SESSION directement
// TODO : new Parsedown()
class ActivitypubActivityCreator extends BaseModel implements ActivitypubActivityCreatorInterface, PersonTraitInterface, OrganizationTraitInterface, DocumentTraitInterface, EventTraitInterface, ProjectTraitInterface, SlugTraitInterface, UtilsTraitInterface, ActivitypubTranslatorTraitInterface, ActivitypubObjectTraitInterface, ActivitypubActivityTraitInterface, ActivitypubActorTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use SessionTrait;
    use I18NTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use OrganizationTrait;
    use DocumentTrait;
    use EventTrait;
    use ProjectTrait;
    use SlugTrait;

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivitypubTranslatorTrait;
    use ActivitypubObjectTrait;
    use ActivitypubActivityTrait;
    use ActivitypubActorTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18NProperty();
    }

    /**
     * Transforms the content before setting it.
     *
     * @param array $params The parameters for the transformation.
     * @return string The transformed content.
     */
    private function transformContentBeforeSet(array $params): string
    {
        if (isset($params["text"])) {
            $parsedown = new Parsedown();
            if ($params['markdownActive'] == true) {
                return $parsedown->text($params["text"]);
            } else {
                return $this->getModelUtils()->isTextHasUrl($params["text"]) ? $this->getModelUtils()->warpUrlIntoLinkTag($params["text"]) : $params["text"];
            }
        } else {
            return "";
        }
    }

    /**
     * Creates a note activity.
     *
     * @param array $params The parameters for creating the note activity.
     * @return AbstractObjectInterface|null The created note activity object, or null if creation failed.
     */
    public function createNoteActivity(array $params): ?AbstractObjectInterface
    {
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);

        $noteParams = [
            "content" => $this->transformContentBeforeSet($params),
            "attributedTo" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];

        if (isset($params["mediaImg"]["images"])) {
            $noteParams["attachment"] = [];
            foreach ($params["mediaImg"]["images"] as $docId) {
                $document = $this->db->findOne(DocumentInterface::COLLECTION, [
                    "_id" => $this->db->MongoId($docId),
                ]);
                if ($document) {
                    $doxExt = array_reverse(explode(".", $document["name"]))[0];
                    $doxExt = ($doxExt == "jpg") ? "jpeg" : $doxExt;
                    $noteParams["attachment"][] = [
                        "type" => "Document",
                        "mediaType" => "image/" . $doxExt,
                        "url" => $this->getModelUtils()->createUrl("upload/communecter/" . $document["folder"] . "/" . $document["name"]),
                    ];
                }
            }
        }
        if (@$params['parentType'] == "projects" && isset($params['parentId'])) {
            $project = $this->db->findOne(ProjectInterface::COLLECTION, [
                "_id" => $this->db->MongoId($params['parentId']),
            ]);
            $params["target"] = [
                "type" => $params['parentType'],
                "id" => $project['objectId'],
            ];
            if (! $project) {
                return null;
            }
            $noteParams['providerInfo'] = [
                "title" => $project["slug"] . '@' . Config::HOST,
                "link" => [
                    "icon" => "https://www.fediverse.to/static/images/icon.png",
                ],
            ];
            if (isset($project["links"]["activitypub"]["followers"]) && sizeof($project["links"]["activitypub"]["followers"]) > 0) {
                foreach ($project["links"]["activitypub"]["followers"] as $follower) {
                    if ($follower["invitorId"] != $actor->get("id")) {
                        $noteParams["cc"][] = $follower["invitorId"];
                    }
                }
            }
            if (isset($project["links"]["activitypub"]["contributors"]) && sizeof($project["links"]["activitypub"]["contributors"]) > 0) {
                foreach ($project["links"]["activitypub"]["contributors"] as $follower) {
                    if ($follower["invitorId"] != $actor->get("id")) {
                        $noteParams["cc"][] = $follower["invitorId"];
                    }
                }
            }
            $summary = $this->language->t(
                "activitypub",
                "{who} has create new note in {where} :  '{what}'",
                [
                    "{who}" => $actor->get("name"),
                    "{what}" => $this->getModelUtils()->displayFirst30Words($noteParams['content']),
                    "{where}" => $project["name"],
                ]
            );
        } else {
            if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
                $noteParams["cc"] = [];
                foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                    $noteParams["cc"][] = $follower["invitorId"];
                }
            }
            $summary = $this->language->t(
                "activitypub",
                "{who} has create new note '{what}'",
                [
                    "{who}" => $actor->get("name"),
                    "{what}" => $noteParams['content'],
                ]
            );
        }

        $note = $this->getModelTypeAp()->create("Note", $noteParams);
        $activity = $this->getModelTypeAp()->create("Create", [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
        ]);
        if ($params['markdownActive']) {
            $activity->set("source", [
                "content" => $params["text"],
                "mediaType" => "text/markdown",
            ]);
        }
        if ($this->getModelUtils()->isTextHasUrl($params["text"])) {
            $activity->set("source", [
                "mediaType" => "text/html",
            ]);
        }
        if (isset($params['target']['id'])) {
            $activity->set("target", $params['target']['id']);
        }

        $activity->set("object", $note);
        $activity->set("to", $note->get("to"));
        $activity->set("cc", $note->get("cc"));
        $activity->set("summary", $summary);
        return $activity;
    }

    /**
     * Creates an update note activity.
     *
     * @param array $params The parameters for creating the activity.
     * @return AbstractObjectInterface|null The created activity object, or null if creation failed.
     */
    public function createUpdateNoteActivity(array $params): ?AbstractObjectInterface
    {
        $res = $this->db->findOne(ActivitypubObject::COLLECTION, [
            "payload.newsId" => $params["idNews"],
        ]);
        if ($res) {
            $object = $this->getModelTypeAp()->createFromAnyValue($res["object"]);

            if ($object) {
                $object->set("updated", (new DateTime())->format(DATE_ISO8601));
                $object->set("attachment", []);
                $object->set("content", $this->transformContentBeforeSet($params));
                if (isset($params["mediaImg"]["images"])) {
                    $attachments = [];
                    foreach ($params["mediaImg"]["images"] as $docId) {
                        $document = $this->db->findOne(DocumentInterface::COLLECTION, [
                            "_id" => $this->db->MongoId($docId),
                        ]);
                        if ($document) {
                            $doxExt = array_reverse(explode(".", $document["name"]))[0];
                            $doxExt = ($doxExt == "jpg") ? "jpeg" : $doxExt;
                            $attachments[] = $this->getModelTypeAp()->create("Document", [
                                "mediaType" => "image/" . $doxExt,
                                "url" => $this->getModelUtils()->createUrl("upload/communecter/" . $document["folder"] . "/" . $document["name"]),
                            ]);
                        }
                    }
                    $object->set("attachment", $attachments);
                }

                $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
                $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);

                $activity = $this->getModelTypeAp()->create("Update", [
                    "actor" => $actor->get("id"),
                    "published" => (new DateTime())->format(DATE_ISO8601),
                ]);
                if ($params['markdownActive'] == true) {
                    $activity->set("source", [
                        "content" => $params["text"],
                        "mediaType" => "text/markdown",
                    ]);
                }
                if ($this->getModelUtils()->isTextHasUrl($params["text"])) {
                    $activity->set("source", [
                        "mediaType" => "text/html",
                    ]);
                }
                $activity->set("object", $object);
                $activity->set("to", $object->get("to"));
                $activity->set("cc", $object->get("cc"));
                return $activity;
            }
        }

        return null;
    }

    /**
     * Creates a delete note activity.
     *
     * @param string $newsId The ID of the news to be deleted.
     * @return AbstractObjectInterface|null The delete note activity object, or null if the activity cannot be created.
     */
    public function createDeleteNoteActivity(string $newsId): ?AbstractObjectInterface
    {
        $object = $this->getModelActivitypubObject()->getObjectByNewsId($newsId);
        if (! $object) {
            return null;
        }

        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);

        return $this->getModelTypeAp()->create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $object->get("cc"),
            "object" => [
                "id" => $object->get("id"),
                "type" => "Tombstone",
            ],
            "published" => (new DateTime())->format(DATE_ISO8601),
        ]);
    }

    /**
     * Creates an update person activity.
     *
     * @param string $id The ID of the person.
     * @return AbstractObjectInterface The created activity.
     */
    public function createUpdatePersonActivity(string $id): AbstractObjectInterface
    {
        $person = $this->db->findOneById(PersonInterface::COLLECTION, $id);
        if (! $person) {
            throw new Error("Person not found.");
        }

        $cc = [];
        if (isset($person["links"]["activitypub"]["followers"])) {
            foreach ($person["links"]["activitypub"]["followers"] as $follower) {
                $cc[] = $follower["invitorId"];
            }
        }

        $object = $this->getModelActivitypubTranslator()->coPersonToActor($person);
        return $this->getModelTypeAp()->create("Update", [
            "actor" => $object->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $object,
            "to" => [UtilsInterface::PUBLIC_INBOX],
            "cc" => $cc,
        ]);
    }

    /**
     * Creates an event activity.
     *
     * @param array $params The parameters for creating the activity.
     * @return AbstractObjectInterface The created activity object.
     */
    public function createEventActivity(array $params): AbstractObjectInterface
    {
        $uuid = $this->getModelUtils()->genUuid();

        if (@$params['organizer']) {
            $collection = OrganizationInterface::COLLECTION;
            $keys = array_keys($params['organizer']);
            $first_key = $keys[0];
            $actorId = $first_key;
        } else {
            $collection = PersonInterface::COLLECTION;
            $actorId = $_SESSION["userId"];
        }
        $user = $this->db->findOneById($collection, $actorId);
        if (@$params['organizer']) {
            $actor = $this->getModelActivitypubTranslator()->coGroupToActor($user);
        } else {
            $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        }

        $event = [
            "name" => $params['name'],
            "actor" => $actor->get('id'),
            "startTime" => $params['startDate'],
            "endTime" => $params['endDate'],
            "content" => $params['shortDescription'] ?? "",
            "ical:status" => "CONFIRMED",
            "contacts" => [
                $actor->get('id'),
            ],
            "to" => [UtilsInterface::PUBLIC_INBOX],
            "uuid" => $uuid,
            "updated" => (new DateTime())->format(DATE_ISO8601),
            "attributedTo" => $actor->get("id"),
            "participantCount" => 0,
            "maximumAttendeeCapacity" => 0,
            "repliesModerationOption" => "allow_all",
            "timezone" => $params['timeZone'],
            "commentsEnabled" => true,
            "attachment" => [],
            "category" => $params['type'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "tag" => [],
            "url" => "https://somemobilizon.instance/events/" . $uuid . '/activity',
            "mediaType" => "text/html",
            "anonymousParticipationEnabled" => false,
            "draft" => false,
            "joinMode" => "free",
        ];
        $invitor = [];

        if (@$params['tags'] || @$params['type']) {
            if (! @$params['tags']) {
                $event['tag'] = $this->getModelUtils()->coTagWithTypeToActivitypubTag(null, $params['type']);
            } else {
                $event['tag'] = $this->getModelUtils()->coTagWithTypeToActivitypubTag($params['tags'], $params['type']);
            }
        }
        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $event["cc"][] = $follower["invitorId"];
                $invitor["cc"][] = $follower["invitorId"] . "/followers";
            }
        }
        $event['attachment'] = $this->getModelUtils()->coAttachmentToActivitypubAttachment($params) ?: null;
        if ($this->getModelUtils()->coLocationToActivitypubLocation($params, $uuid) > 0) {
            $event['location'] = $this->getModelUtils()->coLocationToActivitypubLocation($params, $uuid);
        }
        $createEventActivity = [
            "id" => "https://somemobilizon.instance/events/" . $uuid . '/activity',
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];
        $object = $this->getModelTypeAp()->create("Event", $event);
        if (@$event["cc"] && count($event["cc"]) > 0) {
            $object->set('cc', array_unique($event["cc"]));
        }
        $object->set("id", "https://somemobilizon.instance/events/" . $uuid);
        $activity = $this->getModelTypeAp()->create("Create", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to", $object->get('to'));
        $activity->set("cc", $object->get('cc'));

        return $activity;
    }

    /**
     * Updates the event activity.
     *
     * @param array $params The parameters for updating the activity.
     *
     * @return AbstractObjectInterface|null The updated activity object, or null if the update failed.
     */
    public function updateEventActivity(array $params): ?AbstractObjectInterface
    {
        $event = $this->db->findOneById(EventInterface::COLLECTION, $params['id']);
        if (! $event) {
            return null;
        }
        $object = $this->getModelTypeAp()->createFromAnyValue($event["objectId"]);
        $objectArray = $object->toArray();
        if (@$params['name']) {
            $object->set('name', $params['name']);
        }
        if (@$params['description']) {
            $object->set('content', $params['description']);
        }

        if (@$params['startDate']) {
            $object->set('startTime', $params['startDate']);
        }
        if (@$params['endDate']) {
            $object->set('endTime', $params['endDate']);
        }
        if (@$params['tags'] && @$params['type']) {
            $object->set('tag', $this->getModelUtils()->coTagWithTypeToActivitypubTagEdit($objectArray['tag'], $params['tags'], $params['type']));
        } else {
            if (@$params['tags']) {
                $object->set('tag', $this->getModelUtils()->coTagToActivitypubTagEdit($objectArray['tag'], $params['tags']));
            }
            if (@$params['type']) {
                $object->set('tag', $this->getModelUtils()->coTypeCombineToActivitypubTagEdit($objectArray['tag'], $params['type']));
            }
        }
        if (@$params['attachment']) {
            $arrayAttach = [];
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
            $arrayAttach[] = $this->getModelUtils()->coThumbToActivityPubImage($attachment, @$params['attachment']);
            $object->set('attachment', $arrayAttach);
        }
        //if(@$objectArray['attachment']){
        $attachmentArray = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
        if ((is_countable($this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray)) ? count($this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray)) : 0) > 0) {
            $arrayAttach = $this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray);
            $object->set('attachment', $arrayAttach);
        }
        //}
        if ($this->getModelUtils()->coLocationToActivitypubLocation($params, $objectArray['uuid'])) {
            $object->set('location', $this->getModelUtils()->coLocationToActivitypubLocation($params, $objectArray['uuid']));
        }
        $createEventActivity = [
            "id" => $event["objectId"] . '/activity',
            "actor" => $objectArray['actor'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "updated" => (new DateTime())->format(DATE_ISO8601),
        ];
        $cc = [];

        $activity = $this->getModelTypeAp()->create("Update", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to", $object->get("to"));
        $activity->set("cc", $object->get("cc"));
        return $activity;
    }

    /**
     * Creates a cancel event activity.
     *
     * @param array $params The parameters for creating the activity.
     * @return AbstractObjectInterface|null The created activity object, or null if creation failed.
     */
    public function cancelEventActivity(array $params): ?AbstractObjectInterface
    {
        $event = $this->db->findOneById(EventInterface::COLLECTION, $params['id']);
        if (! $event) {
            return null;
        }
        $object = $this->db->findOne(ActivitypubObject::COLLECTION, [
            "object.id" => $event["objectId"],
        ]);
        $object["ical:status"] = "CANCELLED";
        $createEventActivity = [
            "id" => $event["objectId"] . '/activity',
            "actor" => $object['actor'],
            "published" => (new DateTime())->format(DATE_ISO8601),
            "updated" => (new DateTime())->format(DATE_ISO8601),
        ];
        $activity = $this->getModelTypeAp()->create("Update", $createEventActivity);
        $activity->set("object", $object);
        $activity->set("to", $object['to']);
        $activity->set("cc", $object['cc']);
        return $activity;
    }

    /**
     * Creates a Join Event activity.
     *
     * @param string $activityId The ID of the activity.
     * @return AbstractObjectInterface|null The created activity object, or null if it couldn't be created.
     */
    public function createJoinEventActivity(string $activityId): ?AbstractObjectInterface
    {
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        $res = $this->getModelActivitypubActivity()->getByActivityByObjectId($activityId);
        if (! $res) {
            return null;
        }
        return $this->getModelTypeAp()->create("Join", [
            "actor" => $actor->get("id"),
            "object" => $res["object"],
            "attributedTo" => $res["attributedTo"],
            "id" => $res["id"],
        ]);
    }

    /**
     * Creates a Leave Event Activity.
     *
     * @param string $activityId The ID of the activity.
     * @return AbstractObjectInterface|null The created Leave Event Activity object, or null if it fails.
     */
    public function createLeaveEventActivity(string $activityId): ?AbstractObjectInterface
    {
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        $res = $this->getModelActivitypubActivity()->getByActivityByObjectId($activityId);
        if (! $res) {
            return null;
        }
        return $this->getModelTypeAp()->create("Leave", [
            "actor" => $actor->get("id"),
            "object" => $res["object"],
            "attributedTo" => $res["attributedTo"],
            "id" => $res["id"],
        ]);
    }

    /**
     * Creates an update upload activity.
     *
     * @param string $ownerId The ID of the owner.
     * @param array $res The resource array.
     * @param array $params The parameters array.
     * @return AbstractObjectInterface|null The created activity object or null if not created.
     */
    public function createUpdateUploadActivity(string $ownerId, array $res, array $params): ?AbstractObjectInterface
    {
        if ($params["type"] == PersonInterface::COLLECTION) {
            return $this->createUpdateNoteActivity($this->session->getUserId());
        } else {
            $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
            $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
            $data = $this->db->findOne($params['type'], [
                '_id' => $this->db->MongoId($ownerId),
            ]);
            if (! isset($data["objectId"])) {
                return null;
            }
            $object = $this->getModelTypeAp()->createFromAnyValue($data["objectId"]);
            if (isset($res['isBanner']) && $res['isBanner'] == false) {
                $params = [
                    'connectType' => 'updateAvatar',
                    'res' => $res,
                ];
            } else {
                $params = [
                    'connectType' => 'updateBanner',
                    'res' => $res,
                ];
            }
            return $this->updateProjectActivity($actor, $object, $params);
        }
    }

    // TODO : $res sert à quoi ?
    /**
     * Updates the upload activity for a given collection and ID.
     *
     * @param string $collection The collection name.
     * @param string $id The ID of the activity.
     * @param mixed $res The response data.
     * @return void
     */
    public function updateUploadActivity(string $collection, string $id, $res): void
    {
        $res = $this->db->findOne($collection, [
            '_id' => $this->db->MongoId($id),
        ]);

        if (! $res) {
            return null;
        }
        $this->db->update(ActivitypubObject::COLLECTION, [
            "object.id" => $res["objectId"],
        ], [
            '$push' => [
                'object.attachment' => $this->getModelUtils()->coImageToActivityPubImage($res),
            ],
        ]);
    }

    /**
     * Crée ou met à jour une activité d'événement.
     *
     * @param array $toSave Les données à enregistrer.
     *
     * @return AbstractObjectInterface|null L'objet d'activité créé ou mis à jour, ou null en cas d'erreur.
     */
    public function createOrUpdateEventActivity(array $toSave): ?AbstractObjectInterface
    {
        if (! $toSave['isEdit']) {
            $activity = $this->createEventActivity($toSave);
        } else {
            $activity = $this->updateEventActivity($toSave);
        }

        return $activity;
    }

    /**
     * Creates a project activity.
     *
     * @param array $params The parameters for creating the activity.
     * @return AbstractObjectInterface The created activity object.
     */
    public function createProjectActivity(array $params): AbstractObjectInterface
    {
        $uuid = $this->getModelUtils()->genUuid();
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        $slugActivity = $this->getModelSlug()->checkAndCreateSlug($params['name']);

        $projectParams = [
            "uuid" => $uuid,
            "name" => $params['name'],
            "slug" => $slugActivity,
            "attributedTo" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];
        if (isset($params["email"])) {
            $projectParams["email"] = $params["email"];
        }
        if (isset($params["tags"])) {
            $projectParams["tags"] = $params["tags"];
        }
        if (isset($params["shortDescription"])) {
            $projectParams["shortDescription"] = $params["shortDescription"];
        }
        if (isset($params["url"])) {
            $projectParams["url"] = $params["url"];
        }

        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            $projectParams["cc"] = [];
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $projectParams["cc"][] = $follower["invitorId"];
            }
        }

        $project = $this->getModelTypeAp()->create("Project", $projectParams);
        $activity = $this->getModelTypeAp()->create("Create", [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
        ]);
        $activity->set("object", $project);
        $activity->set("to", $project->get("to"));
        $activity->set("cc", $project->get("cc"));
        $summary = $this->language->t(
            "activitypub",
            "{who} has create project named {what}",
            [
                "{who}" => $actor->get("name"),
                "{what}" => $project->get('name'),
            ]
        );
        $activity->set("summary", $summary);
        return $activity;
    }

    /**
     * Updates the project activity.
     *
     * @param AbstractActorInterface $actor The actor performing the update.
     * @param AbstractObjectInterface $object The object to be updated.
     * @param array $params Additional parameters for the update.
     * @return AbstractObjectInterface|null The updated object, or null if the update failed.
     */
    public function updateProjectActivity(AbstractActorInterface $actor, AbstractObjectInterface $object, array $params): ?AbstractObjectInterface
    {
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        if (! $user) {
            return null;
        }
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];
        $project = $this->db->findOne(ProjectInterface::COLLECTION, [
            "objectId" => $object->get('id'),
        ]);
        $attributeUpdated = [];

        if (! $project) {
            return null;
        }
        if (isset($user["links"]["activitypub"]["followers"]) && sizeof($user["links"]["activitypub"]["followers"]) > 0) {
            $projectParams["cc"] = [];
            foreach ($user["links"]["activitypub"]["followers"] as $follower) {
                $projectParams["cc"][] = $follower["invitorId"];
            }
        }
        if ($params['connectType'] == 'updateBanner') {
            $objectArray = $object->toArray();
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
            $arr = $this->getModelUtils()->coImageBannerToActivityPubImage($attachment, $params['res']);
            $object->set("attachment", $arr);
            $attributeUpdated[] = 'banner';
        } elseif ($params['connectType'] == 'updateAvatar') {
            $objectArray = $object->toArray();
            $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
            $arr = $this->getModelUtils()->coImageProfilToActivityPubImage($attachment, $params['res']);
            $object->set("attachment", $arr);
            $attributeUpdated[] = 'avatar';
        } elseif ($params['connectType'] == 'update') {
            $objectArray = $object->toArray();
            if (@$params['name']) {
                $object->set('name', $params['name']);
                $attributeUpdated[] = 'nom';
            }

            if (@$params['description']) {
                $object->set('content', $params['description']);
                $attributeUpdated[] = 'description';
            }

            if (@$params['startDate']) {
                $object->set('startTime', $params['startDate']);
                $attributeUpdated[] = "date de debut";
            }
            if (@$params['endDate']) {
                $object->set('endTime', $params['endDate']);
                $attributeUpdated[] = "date fin";
            }
            if (@$params['email']) {
                $object->set('email', $params['email']);
                $attributeUpdated[] = "email";
            }

            if (@$params['avancement']) {
                $object->set('progress', $params['avancement']);
                $attributeUpdated[] = "progression";
            }

            if (@$params['tags']) {
                $object->set('tag', $this->getModelUtils()->coTagToActivitypubTagEdit(@$objectArray['tag'], $params['tags']));
                $attributeUpdated[] = "tags";
            }
            if (@$params['attachment']) {
                $arrayAttach = [];
                $attachment = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
                $arrayAttach[] = $this->getModelUtils()->coThumbToActivityPubImage($attachment, @$params['attachment']);
                $object->set('attachment', $arrayAttach);
                $attributeUpdated[] = "images";
            }
            if (@$params['address']) {
                $attributeUpdated[] = "localisation";
            }
            if (@$params['gitlab']) {
                $attributeUpdated[] = "le lien vers gitlab";
            }
            if (@$params['twitter']) {
                $attributeUpdated[] = "le lien vers twitter";
            }
            if (@$params['facebook']) {
                $attributeUpdated[] = "le lien vers facebook";
            }
            if (@$params['instagram']) {
                $attributeUpdated[] = "le lien vers instagram";
            }
            if (@$params['signal']) {
                $attributeUpdated[] = "le lien vers signal";
            }
            if (@$params['telegram']) {
                $attributeUpdated[] = "le lien vers telegram";
            }
            if (@$params['diaspora']) {
                $attributeUpdated[] = "le lien vers diaspora";
            }
            if (@$params['mastodon']) {
                $attributeUpdated[] = "le lien vers mastodon";
            }
            if (@$params['url']) {
                $attributeUpdated[] = "l'url";
            }

            $attachmentArray = @$objectArray['attachment'] ? $objectArray['attachment'] : [];
            if ((is_countable($this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray)) ? count($this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray)) : 0) > 0) {
                $arrayAttach = $this->getModelUtils()->coAttachmentToActivitypubAttachment($params, $attachmentArray);
                $object->set('attachment', $arrayAttach);
            }
            if ($this->getModelUtils()->coLocationToActivitypubLocation($params, @$objectArray['uuid'])) {
                $object->set('location', $this->getModelUtils()->coLocationToActivitypubLocation($params, @$objectArray['uuid']));
            }
        }

        $activity = $this->getModelTypeAp()->create("Update", $projectParams);
        $activity->set("object", $object);
        $activity->set("actor", $actor->get("id"));
        if (isset($projectParams["cc"])) {
            $activity->set("cc", $projectParams["cc"]);
        }
        $summary = $this->language->t(
            "activitypub",
            "{who} has updated info {what} of project {where}",
            [
                "{who}" => $actor->get("name"),
                "{what}" => implode(',', $attributeUpdated),
                "{where}" => $project['name'],
            ]
        );
        $activity->set("summary", $summary);

        return $activity;
    }

    /**
     * Creates or updates a project activity.
     *
     * @param array $toSave The data to save.
     *
     * @return AbstractObjectInterface|null The created or updated activity object, or null if an error occurred.
     */
    public function createOrUpdateProjectActivity(array $toSave): ?AbstractObjectInterface
    {
        $activity = null;
        if (! $toSave['isEdit']) {
            $activity = $this->createProjectActivity($toSave);
        } else {
            $project = $this->db->findOne(ProjectInterface::COLLECTION, [
                '_id' => $this->db->MongoId($toSave['id']),
            ]);
            if (! $project) {
                return null;
            }
            if (! isset($project["objectId"])) {
                return null;
            }
            $subject = $this->getModelTypeAp()->createFromAnyValue($project["objectId"]);
            $content = array_merge([
                'connectType' => 'update',
            ], $toSave);
            $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
            $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
            $activity = $this->updateProjectActivity($actor, $subject, $content);
        }

        return $activity;
    }

    /**
     * Creates a delete activity.
     *
     * @param string $collection The collection where the activity is being deleted from.
     * @param string $id         The ID of the activity being deleted.
     *
     * @return AbstractObjectInterface|null The created delete activity object, or null if an error occurred.
     */
    public function createDeleteActivity(string $collection, string $id): ?AbstractObjectInterface
    {
        $res = $this->db->findOne($collection, [
            '_id' => $this->db->MongoId($id),
        ]);
        if (! isset($res["objectId"])) {
            return null;
        }
        $object = $this->getModelTypeAp()->createFromAnyValue($res["objectId"]);

        if (! $object) {
            return null;
        }
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        $activity = $this->getModelTypeAp()->create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $object->get("cc"),
            "object" => [
                "id" => $object->get("id"),
                "type" => "Tombstone",
            ],
            "published" => (new DateTime())->format(DATE_ISO8601),
        ]);
        $summary = $this->language->t(
            "activitypub",
            "{who} has delete note named {what}",
            [
                "{who}" => $actor->get("id"),
                "{what}" => $object->get('name'),
            ]
        );
        $activity->set("summary", $summary);
        return $activity;
    }

    /**
     * @throws Exception
     */
    public function createContributionActivity($target, $instrument): AbstractObjectInterface
    {
        $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
        // send to admin the request
        $cc = $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $target, $actor);
        $joinParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $actor,
            "to" => [UtilsInterface::PUBLIC_INBOX],
            "cc" => $cc,
            "target" => $target->get('id'),
            "instrument" => $instrument,
        ];

        if ($instrument == 'contributor') {
            $summary = $this->language->t(
                "activitypub",
                "{who} request to join the project {what}  as contributor",
                [
                    "{who}" => $actor->get('name'),
                    "{what}" => $target->get('name'),
                ]
            );
            $joinParams['summary'] = $summary;
        }
        if ($instrument == 'admin') {
            $summary = $this->language->t(
                "activitypub",
                "{who} request to join the project {what}  as admin",
                [
                    "{who}" => $actor->get('name'),
                    "{what}" => $target->get('name'),
                ]
            );
            $joinParams['summary'] = $summary;
        }

        return $this->getModelTypeAp()->create("Join", $joinParams);
    }

    public function createFollowActivity($object): AbstractObjectInterface
    {
        $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
        $cc = $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $object, $actor);
        if (! isset($cc)) {
            $cc = [$object->get('actor')];
        }
        $target = $this->getModelTypeAp()->createFromAnyValue($object->get('id'));
        $object = $this->getModelTypeAp()->createFromAnyValue($actor->get('id'));
        $joinParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "object" => $object,
            "to" => [UtilsInterface::PUBLIC_INBOX],
            "cc" => $cc,
            "target" => $target->get('id'),
            "instrument" => "follow",
        ];
        $summary = $this->language->t(
            "activitypub",
            "{who} request to join the project {what}  as follower",
            [
                "{who}" => $object->get('name'),
                "{what}" => $target->get('name'),
            ]
        );
        $joinParams['summary'] = $summary;
        $activity = $this->getModelTypeAp()->create("Join", $joinParams);
        return $activity;
    }

    public function createInvitationProjectActivity(array $payload): Invite
    {
        $joinParams = [];
        $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
        $object = $this->getModelTypeAp()->createFromAnyValue($payload['objectId']);
        $invitor = $this->getModelTypeAp()->createFromAnyValue($payload['targetId']);
        $instrument = [
            'as' => 'contributor',
        ];
        if (isset($payload['inviteWithRoles'])) {
            $instrument = [
                'as' => 'inviteWithRules',
                'rules' => $payload['inviteWithRoles'],
            ];
        }
        if (isset($payload['inviteAsAdmin'])) {
            $instrument = 'becomeadmin';
        }
        if (isset($payload['inviteWithRoles']) && isset($payload['inviteAsAdmin'])) {
            $instrument = [
                'as' => 'adminandrole',
                'rules' => $payload['inviteWithRoles'],
            ];
        }
        $summary = "";
        $person = $this->getModelTypeAp()->createFromAnyValue($invitor);
        $target = $this->getModelTypeAp()->createFromAnyValue($object->get('id'));
        $activity = new Invite();
        $activity->setActor($actor->get("id"));
        $activity->setObject($person);
        $activity->setPublished((new DateTime())->format(DATE_ISO8601));
        $activity->setTarget($target->get('id'));
        $activity->setCc([
            $person->get('id'),
        ]);
        $activity->setTo([UtilsInterface::PUBLIC_INBOX]);
        $activity->setInstrument($instrument);
        if (is_string($instrument)) {
            if ($instrument == 'becomeadmin') {
                $summary = $this->language->t(
                    "activitypub",
                    "{who} invite to become admin in projet {what}",
                    [
                        "{who}" => $person->get('name'),
                        "{what}" => $target->get('name'),
                    ]
                );
            }
        } else {
            if ($instrument['as'] == 'adminandrole') {
                $summary = $this->language->t(
                    "activitypub",
                    "{who} invite you to manage with rules {rules} in projet {what}",
                    [
                        "{who}" => $person->get('name'),
                        "{rules}" => implode(',', $instrument['rules']),
                        "{what}" => $target->get('name'),
                    ]
                );
                $joinParams['summary'] = $summary;
            } elseif ($instrument['as'] == 'inviteWithRules') {
                $summary = $this->language->t(
                    "activitypub",
                    "{who} invite you with rules {rules} in projet {where}",
                    [
                        "{who}" => $person->get('name'),
                        "{what}" => $invitor->get('name'),
                        "{rules}" => implode(',', $instrument['rules']),
                        "{where}" => $target->get('name'),
                    ]
                );
                $joinParams['summary'] = $summary;
            } elseif ($instrument['as'] == 'contributor') {
                $summary = $this->language->t(
                    "activitypub",
                    "{who} invite you to contribute on projet {what}",
                    [
                        "{who}" => $person->get('name'),
                        "{what}" => $target->get('name'),
                    ]
                );
                $joinParams['summary'] = $summary;
            }
        }
        $activity->setSummary($summary);
        return $activity;
    }

    public function acceptContributionActivity($person, $objectId)
    {
        $object = $this->getModelTypeAp()->createFromAnyValue($objectId);
        $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
        $project = $this->db->findOne(ProjectInterface::COLLECTION, [
            "objectId" => $object->get('id'),
        ]);

        $target = $this->getModelTypeAp()->createFromAnyValue($person);
        $activity = $this->getModelActivitypubActivity()->getLinkActivity("contributors", $project['_id'], $target, 'project');

        $summary = $this->language->t("activitypub", "{who} accept as contributor  into {what}", [
            "{who}" => $target->get('preferredUsername'),
            "{what}" => $object->get('name'),
        ]);
        $cc = $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $object, $actor);
        $acceptactivity = new Accept();
        $acceptactivity->setObject($activity->get("id"));
        $acceptactivity->setActor($actor->get("id"));
        $acceptactivity->setTarget($activity->get("target"));
        $acceptactivity->setInstrument($activity->get("instrument"));
        $acceptactivity->setCc($cc);
        $acceptactivity->setTo($activity->get("to"));
        $acceptactivity->setSummary($summary);
        return $acceptactivity;
    }

    /**
     * Creates an Offer Project Activity.
     *
     * @param array $payload The payload data for the activity.
     * @return Offer The created Offer object.
     */
    public function createOfferProjectActivity(array $payload): Offer
    {
        $instrument = [];
        $actor = $this->getModelActivitypubActor()->getCoPersonAsActorByUserId($this->session->getUserId());
        $object = $this->getModelTypeAp()->createFromAnyValue($payload['objectId']);
        $invitor = $this->getModelTypeAp()->createFromAnyValue($payload['targetId']);
        if (isset($payload['connectType']) && $payload['connectType'] == 'addAsAdmin') {
            $instrument = 'asadmin';
        }
        if (isset($payload['connectType']) && $payload['connectType'] == 'withRules') {
            $instrument = [
                'as' => 'withRules',
                'rules' => $payload['rules'],
            ];
        }
        $summary = "";
        $activity = new Offer();
        $activity->setActor($actor->get("id"));
        $activity->setObject($invitor);
        $activity->setPublished((new DateTime())->format(DATE_ISO8601));
        $activity->setTarget($object->get('id'));
        $activity->setCc([
            $invitor->get('id'),
        ]);
        $activity->setTo([UtilsInterface::PUBLIC_INBOX]);
        $activity->setInstrument($instrument);
        if ($instrument['as'] == 'withRules') {
            $summary = $this->language->t(
                "activitypub",
                "{who} add {invitor} as contributor with rules {rules} in projet {what}",
                [
                    "{who}" => $actor->get('name'),
                    "{rules}" => implode(',', $instrument['rules']),
                    "{what}" => $object->get('name'),
                    "{invitor}" => $invitor->get('name'),
                ]
            );
            $activity->setSummary($summary);
        }
        if ($instrument == 'asadmin') {
            $summary = $this->language->t(
                "activitypub",
                "{who} add you as admin in projet {where} in projet {what}",
                [
                    "{who}" => $actor->get('name'),
                    "{what}" => $invitor->get('name'),
                    "{where}" => $object->get('name'),
                ]
            );
            $activity->setSummary($summary);
        }

        return $activity;
    }

    public function leaveContributionOnProjectActivity($actor, $object, $params)
    {
        $summary = $this->language->t(
            "activitypub",
            "{who} leave contribution in project {where}",
            [
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name'),
            ]
        );
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];
        $projectParams["cc"] = $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $object, $actor);

        $activity = $this->getModelTypeAp()->create("Leave", $projectParams);
        $activity->set("object", $object->get('id'));
        $activity->setInstrument('contributor');
        $activity->set("summary", $summary);
        return $activity;
    }

    public function leaveFollowInProjectActivity($actor, $object, $params)
    {
        $summary = $this->language->t(
            "activitypub",
            "{who} leave his follow in  {where}",
            [
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name'),
            ]
        );
        $projectParams = [
            "actor" => $actor->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
            "to" => [UtilsInterface::PUBLIC_INBOX],
        ];
        $projectParams["cc"] = $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $object, $actor);
        $activity = $this->getModelTypeAp()->create("Leave", $projectParams);
        $activity->set("object", $object->get('id'));
        $activity->setInstrument('unfollow');

        $activity->set("summary", $summary);
        return $activity;
    }

    public function createDeleteContributionOnProjectActivity($params)
    {
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);
        $object = $this->getModelTypeAp()->createFromAnyValue($params['objectId']);
        $summary = $this->language->t(
            "activitypub",
            "{who} delete contribution in project {where}",
            [
                "{who}" => $actor->get('name'),
                "{where}" => $object->get('name'),
            ]
        );
        $activity = $this->getModelTypeAp()->create("Delete", [
            "actor" => $actor->get("id"),
            "to" => $object->get("to"),
            "cc" => $this->getModelActivitypubActivity()->getAllCCRecipients(ProjectInterface::COLLECTION, "contributors", $object, $actor),
            "object" => $params['targetId'],
            "target" => $object->get("id"),
            "published" => (new DateTime())->format(DATE_ISO8601),
        ]);
        $activity->set("summary", $summary);
        return $activity;
    }
}
