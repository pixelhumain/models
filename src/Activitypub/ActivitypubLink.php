<?php

namespace PixelHumain\Models\Activitypub;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubLinkInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProjectTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ProjectTrait;

class ActivitypubLink extends BaseModel implements ActivitypubLinkInterface, PersonTraitInterface, OrganizationTraitInterface, ProjectTraitInterface, UtilsTraitInterface, ActivitypubTranslatorTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use OrganizationTrait;
    use ProjectTrait;

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivitypubTranslatorTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    //save link
    public function saveLink($link, $subject, AbstractActorInterface $actor, $activityId, $payload = null, $pending = true)
    {
        if ($payload["type"] == self::GROUP) {
            $collection = OrganizationInterface::COLLECTION;
            $query = [
                "slug" => $payload["name"],
            ];
        } else {
            $collection = PersonInterface::COLLECTION;
            $query = [
                "username" => $payload["name"],
            ];
        }

        if (self::isLinkAlreadyExist($link, $subject, $actor, $payload)) {
            return false;
        }
        $id = uniqid();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => $this->getModelActivitypubTranslator()->getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername"),
        ];

        if ($pending) {
            $data["pending"] = true;
        }

        $this->db->update($collection, $query, [
            '$push' => [
                'links.activitypub.' . $link => $data,
            ],
        ]);

        return $id;
    }

    //accept follow link
    public function approveLink($link, $username, AbstractActorInterface $invitor, $actorEntity)
    {
        $collection = ($actorEntity['type'] == "group") ? OrganizationInterface::COLLECTION : PersonInterface::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $actorEntity['name'],
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id"),
        ] : [
            "username" => $username,
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id"),
        ];
        $this->db->update(
            $collection,
            $data,
            [
                '$unset' => [
                    'links.activitypub.' . $link . ".$.pending" => "",
                ],
            ]
        );
    }

    //check if link is already exist
    public function isLinkAlreadyExist($link, AbstractActorInterface $subject, AbstractActorInterface $actor, $actorEntity)
    {
        $collection = PersonInterface::COLLECTION;
        $query = [
            'username' => $subject->get("preferredUsername"),
            'links.activitypub.' . $link =>
            [
                '$elemMatch' => [
                    "invitorId" => $actor->get("id"),
                ],
            ],
        ];
        switch ($actorEntity['type']) {
            case self::PROJECT:
                $collection = ProjectInterface::COLLECTION;
                $query = [
                    'objectId' => $subject->get("id"),
                    'links.activitypub.' . $link => [
                        '$elemMatch' => [
                            "invitorId" => $actor->get("id"),
                        ],
                    ],
                ];
                break;
            case self::GROUP:
                $collection = PersonInterface::COLLECTION;
                $query = [
                    'slug' => $subject->get("preferredUsername"),
                    'links.activitypub.' . $link => [
                        '$elemMatch' => [
                            "invitorId" => $actor->get("id"),
                        ],
                    ],
                ];
                break;
                break;
        }
        $entity = $this->db->findOne($collection, $query);
        return boolval($entity);
    }

    public function deleteLink($link, $username, AbstractActorInterface $invitor, $actorEntity)
    {
        $collection = $actorEntity['type'] == "group" ? OrganizationInterface::COLLECTION : PersonInterface::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $username,
        ] : [
            "username" => $username,
        ];
        $this->db->update(
            $collection,
            $data,
            [
                '$pull' => [
                    'links.activitypub.' . $link => [
                        "invitorId" => $invitor->get("id"),
                    ],
                ],
            ]
        );
    }

    public function pendingLink($link, $username, AbstractActorInterface $invitor, $actorEntity)
    {
        $collection = $actorEntity['type'] == "group" ? OrganizationInterface::COLLECTION : PersonInterface::COLLECTION;
        $data = $actorEntity['type'] == "group" ? [
            "slug" => $actorEntity['name'],
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id"),
        ] : [
            "username" => $username,
            "links.activitypub." . $link . ".invitorId" => $invitor->get("id"),
        ];

        $this->db->update(
            $collection,
            $data,
            [
                '$set' => [
                    'links.activitypub.' . $link . '.$.pending' => true,
                ],
            ]
        );
    }

    public function saveLinkProject($link, $subject, AbstractActorInterface $actor, $activityId, $payload = null, $pending = true)
    {
        $id = uniqid();
        $actorArray = $actor->toArray();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => $this->getModelActivitypubTranslator()->getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername"),
        ];
        $data["isAdmin"] = true;
        if (self::isLinkAlreadyExist($link, $subject, $actor, [
            'type' => 'project',
        ])) {
            $this->db->update(
                ProjectInterface::COLLECTION,
                [
                    'objectId' => $subject->get('id'),
                    "links.activitypub." . $link . ".invitorId" => $actor->get("id"),
                ],
                [
                    '$set' => [
                        'links.activitypub.' . $link . '.$' => $data,
                    ],
                ]
            );
        } else {
            $this->db->update(ProjectInterface::COLLECTION, [
                'objectId' => $subject->get('id'),
            ], [
                '$push' => [
                    'links.activitypub.' . $link => $data,
                ],
            ]);
        }

        return $id;
    }

    public function deleteFollowersProject($objectId, AbstractActorInterface $invitor)
    {
        $this->db->update(
            ProjectInterface::COLLECTION,
            [
                "objectId" => $objectId,
            ],
            [
                '$pull' => [
                    'links.activitypub.followers' => [
                        "invitorId" => $invitor->get("id"),
                    ],
                ],
            ]
        );
    }

    public function deleteProject($link, $subject, AbstractActorInterface $invitor)
    {
        $this->db->update(
            ProjectInterface::COLLECTION,
            [
                "objectId" => $subject->get('id'),
            ],
            [
                '$pull' => [
                    'links.activitypub.' . $link => [
                        "invitorId" => $invitor->get("id"),
                    ],
                ],
            ]
        );
    }

    public function saveLinkForProject($link, $subject, AbstractActorInterface $actor, $activityId, $instrument = null)
    {
        $id = uniqid();
        $data = [
            "activityId" => $activityId,
            "invitorId" => $actor->get("id"),
            "invitorAdress" => $this->getModelActivitypubTranslator()->getActorAdress($actor),
            "invitorName" => $actor->get("name"),
            "invitorUsername" => $actor->get("preferredUsername"),
        ];
        if ($instrument != 'follow' || $instrument != 'unfollow') {
            self::deleteFollowersProject($subject->get('id'), $actor);
        } else {
            self::deleteProjectLink('followers', $subject->get('id'), $actor);
        }
        if (is_string($instrument)) {
            switch ($instrument) {
                case self::CONTRIBUTION:
                    $data["toBeValidated"] = true;
                    break;
                case self::ADMIN:
                    $data["isAdmin"] = true;
                    $data["isAdminPending"] = true;
                    $data["toBeValidated"] = true;
                    break;
                case self::BECOME_ADMIN:
                    $data["isAdmin"] = true;
                    $data["isInviting"] = true;
                    break;
                case self::ADD_AS_ADMIN:
                    $data["isAdmin"] = true;
                    break;
            }
        } else {
            switch ($instrument['as']) {
                case self::ADMIN_AND_ROLE:
                    $data["isAdmin"] = true;
                    $data["roles"] = explode(',', $instrument['rules']);
                    $data["isInviting"] = true;
                    break;
                case self::INVITE_WITH_RULES:
                    $data["roles"] = explode(',', $instrument['rules']);
                    $data["isInviting"] = true;
                    break;
                case self::CONTRIBUTOR:
                    $data["isInviting"] = true;
                    break;
                case self::ADD_WITH_RULES:
                    $data["roles"] = explode(',', $instrument['rules']);
                    break;
            }
        }
        if (self::isLinkAlreadyExist($link, $subject, $actor, [
            'type' => 'project',
        ])) {
            $this->db->update(
                ProjectInterface::COLLECTION,
                [
                    'objectId' => $subject->get('id'),
                    "links.activitypub." . $link . ".invitorId" => $actor->get("id"),
                ],
                [
                    '$set' => [
                        'links.activitypub.' . $link . '.$' => $data,
                    ],
                ]
            );
        } else {
            $this->db->update(ProjectInterface::COLLECTION, [
                'objectId' => $subject->get('id'),
            ], [
                '$push' => [
                    'links.activitypub.' . $link => $data,
                ],
            ]);
        }

        return $id;
    }

    public function acceptProjectLink($link, $subject, $actor, $activityId, $instrument = null)
    {
        $query = [
            'objectId' => $subject->get('id'),
            "links.activitypub." . $link . ".invitorId" => $actor->get("id"),
        ];
        $project = $this->db->findOne(ProjectInterface::COLLECTION, $query);

        $links = $project["links"]["activitypub"][$link];

        // recuperer les attributs de links
        $data = array_reduce($links, function ($carry, $v) use ($actor) {
            if ($carry === null && $v["invitorId"] == $actor->get("id")) {
                $carry = $v;
            }
            return $carry;
        }, null);
        $data["activityId"] = $activityId;
        if (is_string($instrument)) {
            switch ($instrument) {
                case self::CONTRIBUTION:
                    unset($data["toBeValidated"]);
                    break;
                case self::ADMIN:
                    $data["isAdmin"] = true;
                    unset($data["isAdminPending"]);
                    unset($data["toBeValidated"]);
                    break;
                case self::BECOME_ADMIN:
                    $data["isAdmin"] = true;
                    unset($data["toBeValidated"]);
                    break;
                case self::ADD_AS_ADMIN:
                    $data["isAdmin"] = true;
                    break;
            }
        } else {
            switch ($instrument['as']) {
                case self::ADMIN_AND_ROLE:
                    $data["isAdmin"] = true;
                    $data["roles"] = explode(',', $instrument['rules']);
                    unset($data["isInviting"]);
                    break;
                case self::INVITE_WITH_RULES:
                    $data["roles"] = explode(',', $instrument['rules']);
                    unset($data["isInviting"]);
                    break;
                case self::CONTRIBUTION:
                    unset($data["isInviting"]);
                    break;
                case self::ADD_WITH_RULES:
                    $data["roles"] = $instrument['rules'];
                    break;
            }
        }
        $this->db->update(
            ProjectInterface::COLLECTION,
            $query,
            [
                '$set' => [
                    'links.activitypub.' . $link . '.$' => $data,
                ],
            ]
        );
    }

    public function deleteProjectLink($link, $subject, AbstractActorInterface $actor)
    {
        $this->db->update(
            ProjectInterface::COLLECTION,
            [
                'objectId' => $subject->get('id'),
                "links.activitypub." . $link . ".invitorId" => $actor->get("id"),
            ],
            [
                '$pull' => [
                    'links.activitypub.' . $link => [
                        "invitorId" => $actor->get("id"),
                    ],
                ],
            ]
        );
    }

    public function saveTags($payload, $actorObject): void
    {
        $this->db->update(
            PersonInterface::COLLECTION,
            [
                'username' => $actorObject['name'],
                "links.activitypub.follows.invitorId" => $actorObject['id'],
            ],
            [
                '$set' => [
                    'links.activitypub.follows.$.tags' => $payload,
                ],
            ]
        );
    }

    public function getActorTags($actor)
    {
        $tags = [];
        $query = [
            'links.activitypub.follows' => [
                '$elemMatch' => [
                    "invitorId" => $actor,
                ],
            ],
        ];
        $user = $this->db->findOne(PersonInterface::COLLECTION, $query);
        if (isset($user["links"]['activitypub']['follows'])) {
            foreach ($user["links"]['activitypub']['follows'] as $key => $value) {
                if (isset($value['invitorId']) && $value['invitorId'] == $actor) {
                    if (isset($value['tags'])) {
                        $tags = $value['tags'];
                    }
                }
            }
        }
        return $tags;
    }

    public function getAllTags(): array
    {
        $tags = [];
        if (isset($_SESSION["user"]["username"])) {
            $user = $this->db->findOne(PersonInterface::COLLECTION, [
                'username' => $_SESSION["user"]["username"],
            ]);
            if (isset($user["links"]['activitypub']['follows'])) {
                foreach ($user["links"]['activitypub']['follows'] as $key => $value) {
                    if (isset($value['tags'])) {
                        $tags = array_merge($tags, $value['tags']);
                    }
                }
            }
        }
        return $this->getModelUtils()->deduplicateArray($tags);
    }
}
