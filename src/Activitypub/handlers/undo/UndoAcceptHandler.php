<?php

namespace PixelHumain\Models\Activitypub\handlers\undo;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;

class UndoAcceptHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    public function __construct(array $config = [], ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("target"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $this->getModelActivitypubActivity()->save($this->activity, [$this->target]);

        $this->getModelActivitypubLink()->pendingLink(
            "followers",
            $this->actor->get("preferredUsername"),
            $this->target,
            $this->payload
        );

        $this->getModelRequest()->post(
            $this->actor->get("id"),
            $this->target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
