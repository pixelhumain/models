<?php

namespace PixelHumain\Models\Activitypub\handlers\undo;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;

class UndoFollowHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    public function __construct(array $config = [], ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $object = $this->getModeltypeAp()->createFromAnyValue($activity->get("object"));
        $this->target = $this->getModeltypeAp()->createFromAnyValue($object->get("object"));
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $this->getModelActivitypubActivity()->save($this->activity, [$this->target]);

        $this->getModelActivitypubLink()->deleteLink(
            "follows",
            $this->actor->get("preferredUsername"),
            $this->target,
            $this->payload
        );
        $this->getModelRequest()->post(
            $this->actor->get("id"),
            $this->target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {
        $activty = $this->activity->toArray();
        $object = $activty['object'];
        $objectValue = $object['object'];
        $mocked_payload = self::mockPayload($object['object']);
        $this->getModelActivitypubActivity()->save($this->activity, [$this->target]);
        $this->getModelActivitypubLink()->deleteLink(
            "followers",
            $this->target->get("preferredUsername"),
            $this->actor,
            $mocked_payload
        );
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }

    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }
}
