<?php

namespace PixelHumain\Models\Activitypub\handlers\invite;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\Activitypub\utils\Utils;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;

class InvitePersonHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivitypubActorTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivitypubActorTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use UtilsTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateI18NProperty();
    }

    public function __construct(array $config, ActivityInterface $activity)
    {
        parent::__construct($config, $activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc"))) {
            $targetIds = $this->activity->get("cc");
        }
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = $this->getModeltypeAp()->createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("target"));
        $actor = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $instrument = $this->activity->get('instrument');
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);
        $this->getModelActivitypubLink()->saveLinkForProject("contributors", $object, $actor, $UUIDS["activity"], $instrument);
        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = null;
        // check if invitation from mobilizon
        if ($this->getModelUtils()->isMobilizonInstance($this->activity->get("actor"))) {
            $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
            $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->activity->get("attributedTo"));
            if (empty($this->activity->get('summary'))) {
                $summary = $this->language->t(
                    "activitypub",
                    "{who} send invitation for become member of '{what}'",
                    [
                        "{who}" => $this->actor->get('preferredUsername'),
                        "{what}" => $target->get("name"),
                    ]
                );
                $this->activity->set('summary', $summary);
            }
            $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, [$target]);
            $mocked_payload = self::mockPayload($this->activity->get("target"));
            $this->getModelActivitypubLink()->saveLink("followers", $target, $target, $UUIDS["activity"], $mocked_payload);
        } else {
            $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("target"));
            $person = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
            $mocked_payload = self::mockPayload($this->activity->get("target"));

            $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, [$target]);
            //Todo: check type of element before save
            // if ($mocked_payload['type'] == "projects") {
            $this->getModelActivitypubLink()->saveLinkForProject("contributors", $target, $person, $UUIDS["activity"], $this->activity->get("instrument"));
            //} else {
            //  return;
            // }
        }
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor, $targets);
    }

    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } elseif (strpos($url, 'projects') !== false) {
            return [
                "name" => $basename,
                "type" => "projects",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }
}
