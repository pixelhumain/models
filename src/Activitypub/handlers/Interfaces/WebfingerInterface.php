<?php

namespace PixelHumain\Models\Activitypub\handlers\Interfaces;

interface WebfingerInterface
{
    /**
     * Parses the given resource and returns an array.
     *
     * @param string $resource The resource to parse.
     * @return array The parsed resource as an array.
     */
    public static function parseResource(string $resource): array;
}
