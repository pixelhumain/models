<?php

namespace PixelHumain\Models\Activitypub\handlers\follow;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;


use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;

class FollowPersonHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateI18NProperty();
    }

    public function __construct(array $config, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $actor = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("actor"));
        $target = $object;

        //save activity to inbox
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, [$target]);
        $pending = boolval($object->get("manuallyApprovesFollowers"));
        $this->getModelActivitypubLink()->saveLink("follows", $actor, $object, $UUIDS["activity"], $this->payload, $pending);
        //federate activity

        $this->getModelRequest()->post(
            $this->actor->get("id"),
            $target->get("inbox"),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {
        $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, [$target]);
        $mocked_payload = self::mockPayload($this->activity->get("object"));
        $this->getModelActivitypubLink()->saveLink("followers", $target, $this->actor, $UUIDS["activity"], $mocked_payload);
        // notify user
        if (empty($this->activity->get('summary'))) {
            $summary = $this->language->t(
                "activitypub",
                "{who} follow you",
                [
                    "{who}" => $this->actor->get('preferredUsername'),
                ]
            );
            $this->activity->set('summary', $summary);
        }
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }

    private function mockPayload($url): array
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }
}
