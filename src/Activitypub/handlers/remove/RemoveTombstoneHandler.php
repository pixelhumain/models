<?php

namespace PixelHumain\Models\Activitypub\handlers\remove;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubObjectTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubObjectTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class RemoveTombstoneHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubObjectTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubObjectTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function __construct(array $config, ActivityInterface $activity)
    {
        parent::__construct($config, $activity);
    }

    protected function handleActivityFromClient()
    {
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));

        $targets = [];
        $targetIds = [];
        if (is_array($this->activity->get("to"))) {
            $targetIds = array_merge($this->activity->get("to"));
        }
        if (is_array($this->activity->get("cc"))) {
            $targetIds = array_merge($this->activity->get("cc"));
        }

        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }

        $this->getModelActivitypubActivity()->save($this->activity, $targets);
        $this->getModelActivitypubObject()->removeByObjectId($object->get("id"));

        $this->activity->set("object", $object);

        //group targets by domain name
        $targetInboxes = [];
        foreach ($targets as $target) {
            if (is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"])) {
                if (! in_array($target->get("endpoints")["sharedInbox"], $targetInboxes)) {
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
                }
            } else {
                $targetInboxes[] = $target->get("inbox");
            }
        }

        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $res = $this->db->findOne(ActivitypubObjectInterface::COLLECTION, [
            "object.id" => $object->get("id"),
        ]);
        if ($res) {
            $localObject = $this->getModeltypeAp()->createFromAnyValue($res["object"]);
            if ($localObject->get("type") == "Note") {
                $this->db->remove(NewsInterface::COLLECTION, [
                    "objectId" => $localObject->get("id"),
                ]);
            } elseif ($localObject->get("type") == "Event") {
                $this->db->remove(EventInterface::COLLECTION, [
                    "objectId" => $localObject->get("id"),
                ]);
            }
            $this->db->remove($this->getModelActivitypubObject()->COLLECTION, [
                "object.id" => $localObject->get("id"),
            ]);
        }
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
