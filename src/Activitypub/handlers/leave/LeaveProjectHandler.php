<?php

namespace PixelHumain\Models\Activitypub\handlers\leave;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubTranslatorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubTranslatorTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class LeaveProjectHandler extends AbstractHandler implements ActivitypubTranslatorTraitInterface, ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivityNotificationTraitInterface, ActivitypubActorTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubTranslatorTrait;
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivityNotificationTrait;
    use ActivitypubActorTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function __construct(array $config, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);

        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $user = $this->db->findOneById(PersonInterface::COLLECTION, $_SESSION["userId"]);
        $actor = $this->getModelActivitypubTranslator()->coPersonToActor($user);

        if (is_array($this->activity->get("cc"))) {
            $targetIds = $this->activity->get("cc");
        }

        foreach ($targetIds as $id) {
            if ($id !== $actor->get('id')) {
                $x = $this->getModeltypeAp()->createFromAnyValue($id);
                $targets[] = $x;
            }
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));

        $targetInboxes = [];
        foreach ($targets as $t) {
            $targetInboxes[] = $t->get("inbox");
        }

        $this->getModelActivitypubActivity()->save($this->activity, []);
        $instrument = $this->activity->get("instrument");
        if ($instrument != 'unfollow') {
            $this->getModelActivitypubLink()->deleteProjectLink("contributors", $object, $actor);
        } else {
            $this->getModelActivitypubLink()->deleteProjectLink("followers", $object, $actor);
        }

        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->actor->get("id"));
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $actor = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("actor"));
        $this->getModelActivitypubActivity()->save($this->activity, $targets);
        $instrument = $this->activity->get("instrument");
        if ($instrument != 'unfollow') {
            $this->getModelActivitypubLink()->deleteProjectLink("contributors", $object, $actor);
        } else {
            $this->getModelActivitypubLink()->deleteProjectLink("followers", $object, $actor);
        }
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
