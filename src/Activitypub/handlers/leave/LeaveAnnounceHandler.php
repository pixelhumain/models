<?php

namespace PixelHumain\Models\Activitypub\handlers\leave;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class LeaveAnnounceHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubActorTraitInterface, UtilsTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubActorTrait;
    use UtilsTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function __construct(array $config, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("attributedTo"));
        $participant = [$this->activity->get("actor")];
        $this->getModelActivitypubActivity()->save($this->activity, [$target], $participant);
        $this->getModelRequest()->post(
            $this->activity->get("actor"),
            $target->get('inbox'),
            $this->activity->toArray(),
            true
        );
    }

    protected function handleActivityFromServer()
    {
        $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->actor->get("id"));
        $objectId = $this->activity->get("object");
        $res = $this->db->findOne(ActivitypubObjectInterface::COLLECTION, [
            "object.id" => $objectId,
        ]);
        $participant = $this->getModelUtils()->removeIfExistInArray($res["payload"], [$this->activity->get("actor")]);
        if (! $res) {
            return null;
        }
        $this->db->update(ActivitypubObjectInterface::COLLECTION, [
            "object.id" => $objectId,
        ], [
            '$set' => [
                //  "object.participateCount" => count($participant),
                "payload" => (is_countable($participant) ? count($participant) : 0) == 0 ? [] : $participant,
            ],
        ]);
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
