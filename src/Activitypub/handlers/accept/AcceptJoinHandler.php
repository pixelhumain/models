<?php

namespace PixelHumain\Models\Activitypub\handlers\accept;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubLinkTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubLinkTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class AcceptJoinHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivitypubLinkTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubLinkTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function __construct(array $config, ActivityInterface $activity)
    {
        parent::__construct($config, $activity);
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc"))) {
            $targetIds = $this->activity->get("cc");
        }
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = $this->getModeltypeAp()->createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }

        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $target = $this->getModeltypeAp()->createFromAnyValue($object->get("target"));
        $actor = $this->getModeltypeAp()->createFromAnyValue($object->get("actor"));
        $instrument = $this->activity->get('instrument');
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);

        $this->getModelActivitypubLink()->acceptProjectLink("contributors", $target, $actor, $UUIDS["activity"], $instrument);
        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        if ($this->getModelUtils()->isMobilizonInstance($this->activity->get("actor"))) {
            $targets = [];
            $object = $this->activity->get("object");
            $url = parse_url($object->get('object'));
            $host = $url['host'];
            $objectId = str_replace($host, 'somemobilizon.instance', $object->get("object"));
            $res = $this->db->findOne(ActivitypubObjectInterface::COLLECTION, [
                "object.id" => $objectId,
            ]);
            $participant = [];
            if (! isset($res["payload"])) {
                $participant = [$object->get("actor")];
                $this->db->update(ActivitypubObjectInterface::COLLECTION, [
                    "object.id" => $objectId,
                ], [
                    '$set' => [
                        "payload" => $participant,
                    ],
                    'upsert' => true,
                ]);
            } else {
                $participant = $this->getModelUtils()->addIfNotInArray($res["payload"], [$object->get("actor")]);
                $this->db->update(ActivitypubObjectInterface::COLLECTION, [
                    "object.id" => $objectId,
                ], [
                    '$set' => [
                        "payload" => $participant,
                    ],
                ]);
            }
            $this->getModelActivitypubActivity()->save($this->activity, $targets);
        } else {
            $acceptActivity = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
            $actor = $this->getModeltypeAp()->createFromAnyValue($acceptActivity->get("actor"));
            $target = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("target"));
            $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, []);
            $this->getModelActivitypubLink()->acceptProjectLink("contributors", $target, $actor, $UUIDS["activity"], $this->activity->get("instrument"));
            // notify user
            $this->getModelActivityNotification()->send($this->activity, $this->actor);
        }
    }

    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } elseif (strpos($url, 'projects') !== false) {
            return [
                "name" => $basename,
                "type" => "projects",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }
}
