<?php

namespace PixelHumain\Models\Activitypub\handlers;

use Exception;
use PixelHumain\Models\Activitypub\handlers\Interfaces\WebfingerInterface;

/**
 * Class Webfinger
 *
 * This class implements the WebfingerInterface and handles Webfinger requests.
 */
class Webfinger implements WebfingerInterface
{
    /**
    * @var string
    */
    private string $subject;

    /**
     * @var string[]
     */
    private array $aliases = [];

    private array $links = [];

    /**
         * Set subject property
         *
         * @param string $subject The subject to be set
         */
    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    /**
     * Set aliases property
     *
     * @param array $aliases An array of aliases
     * @throws Exception If any alias is not a string
     */
    public function setAliases(array $aliases): void
    {
        foreach ($aliases as $alias) {
            if (! is_string($alias)) {
                throw new Exception(
                    "WebFinger aliases must be an array of strings"
                );
            }

            $this->aliases[] = $alias;
        }
    }

    /**
     * Set links property
     *
     * @param array $links An array of links objects containing 'rel', 'type', 'href', and 'template' properties.
     * @throws Exception If the links parameter is not an array of objects or if any link object is missing the 'rel' property.
     */
    public function setLinks(array $links): void
    {
        foreach ($links as $link) {
            if (! is_array($link)) {
                throw new Exception(
                    "WebFinger links must be an array of objects"
                );
            }

            if (! isset($link['rel'])) {
                throw new Exception(
                    "WebFinger links object must contain 'rel' property"
                );
            }

            $tmp = [];
            $tmp['rel'] = $link['rel'];

            foreach (['type', 'href', 'template'] as $key) {
                if (isset($link[$key]) && is_string($link[$key])) {
                    $tmp[$key] = $link[$key];
                }
            }

            $this->links[] = $tmp;
        }
    }

    /**
     * Get aliases
     *
     * Returns an array of aliases.
     *
     * @return array The array of aliases.
     */
    public function getAliases(): array
    {
        return $this->aliases;
    }

    /**
     * Get links
     *
     * This method returns an array of links.
     *
     * @return array The array of links.
     */
    public function getLinks(): array
    {
        return $this->links;
    }

    /**
     * Get subject fetched from profile
     *
     * @return null|string Subject
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * Get WebFinger response as an array
     *
     * @return array Returns the WebFinger response as an array
     */
    public function toArray(): array
    {
        return [
            'subject' => $this->subject,
            'aliases' => $this->aliases,
            'links' => $this->links,
        ];
    }

    /**
     * Parses the given resource and returns an array.
     *
     * @param string $resource The resource to parse.
     * @return array The parsed resource as an array.
     */
    public static function parseResource(string $resource): array
    {
        $pattern = '/^@?(?P<user>[\w\-\.]+)@(?P<host>[\w\.\-]+)(?P<port>:[\d]+)?$/';
        if (! preg_match($pattern, $resource, $matches)) {
            throw new Exception("WebFinger handle is malformed '{$resource}'");
        }
        return $matches;
    }
}
