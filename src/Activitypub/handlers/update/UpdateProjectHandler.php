<?php

namespace PixelHumain\Models\Activitypub\handlers\update;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;

class UpdateProjectHandler extends AbstractHandler implements ActivitypubActivityTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    public function __construct(array $config = [], ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        $targetInboxes = [];
        if (is_array($this->activity->get("cc"))) {
            $targetIds = $this->activity->get("cc");
        }
        foreach ($targetIds as $id) {
            if (isset($id)) {
                $x = $this->getModeltypeAp()->createFromAnyValue($id);
                $targetInboxes[] = $x->get('inbox');
            }
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object")); // project as object
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);
        //federate activity
        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = [];
        $targetIds = [];
        if (is_array($this->activity->get("to"))) {
            $targetIds = array_merge($this->activity->get("to"));
        }
        if (is_array($this->activity->get("cc"))) {
            $targetIds = array_merge($this->activity->get("cc"));
        }

        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, []);
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }
}
