<?php

namespace PixelHumain\Models\Activitypub\handlers\update;

use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;

class UpdateEventHandler extends UpdateHandler
{
    public function __construct(array $config, ActivityInterface $activity)
    {
        parent::__construct($config, $activity);
    }
}
