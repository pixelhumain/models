<?php

namespace PixelHumain\Models\Activitypub\handlers\create;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\PreferenceTrait;

class AnnounceEventHandler extends AbstractHandler implements PreferenceTraitInterface, ActivitypubActorTraitInterface, ActivityNotificationTraitInterface, UtilsTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;
    use ParamsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PreferenceTrait;

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubActorTrait;
    use ActivityNotificationTrait;
    use UtilsTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18NProperty();
        $this->validateParamsProperty();
    }

    public function __construct(array $config, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, []);
    }

    protected function handleActivityFromServer()
    {
        $targets = [];
        $to = $this->activity->get("to");
        if ($this->getModelUtils()->isMobilizonInstance($this->actor->get("id"))) {
        }
        if (is_array($to) && in_array(UtilsInterface::PUBLIC_INBOX, $to)) {
            $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->actor->get("id"));
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $summary = $this->language->t(
            "activitypub",
            "{who} has create new event '{what}'",
            [
                "{who}" => $this->actor->get('preferredUsername'),
                "{what}" => $object->get('name'),
            ]
        );
        $this->activity->set('summary', $summary);
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);
        $data = $this->getModelUtils()->parseToEvent($object, $UUIDS["object"]);
        $this->db->insert(EventInterface::COLLECTION, $data);
        $this->pingTargets($targets);
        // notify user
        $this->getModelActivityNotification()->send($this->activity, $this->actor);
    }

    private function pingTargets($targets)
    {
        $targetIds = [];
        foreach ($targets as $target) {
            $user = $this->db->findOne(PersonInterface::COLLECTION, [
                "username" => $target->get("preferredUsername"),
            ]);
            if ($user && isset($user["preferences"]) && $this->getModelPreference()->isActivitypubActivate($user["preferences"])) {
                $targetIds[] = (string) $user["_id"];
            }
        }

        $coWs = $this->params->get('cows');
        $curl = curl_init($coWs["pingNewPostUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
            "users" => $targetIds,
        ]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_exec($curl);
    }
}
