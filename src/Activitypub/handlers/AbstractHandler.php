<?php

namespace PixelHumain\Models\Activitypub\handlers;

use Exception;
use PixelHumain\Models\Activitypub\Config;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;

/**
 * The AbstractHandler class is an abstract base class for handling ActivityPub activities.
 */
abstract class AbstractHandler implements TypeTraitInterface
{
    /**
     * The activity being handled.
     *
     * @var ActivityInterface
     */
    protected ActivityInterface $activity;

    /**
     * The actor associated with the activity.
     *
     * @var AbstractActorInterface
     */
    protected AbstractActorInterface $actor;

    /**
     * Indicates whether the activity is from a client.
     *
     * @var bool|null
     */
    protected ?bool $isFromClient;

    use TypeTrait;

    /**
     * Constructs a new AbstractHandler instance.
     *
     * @param array $config The configuration array.
     * @param ActivityInterface $activity The activity being handled.
     * @param bool|null $isFromClient Indicates whether the activity is from a client.
     * @throws Exception If the actor is not an instance of AbstractActorInterface.
     */
    public function __construct(array $config, ActivityInterface $activity, ?bool $isFromClient = null)
    {
        $this->configure($config);
        $this->activity = $activity;
        $actor = $this->getModeltypeAp()->createFromAnyValue($activity->get("actor"));
        if ($actor instanceof AbstractActorInterface) {
            $this->actor = $actor;
        } else {
            throw new Exception("The actor is not an actor");
        }
        $this->isFromClient = $isFromClient;
    }

    /**
     * Configures the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    abstract protected function configure(array $config): void;

    /**
     * Validates the properties of the model.
     *
     * @return void
     */
    abstract protected function validateProperties(): void;

    /**
     * Handles the activity.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->isFromClient === null) {
            $this->isFromClient = parse_url((string) $this->actor->get("id"), PHP_URL_HOST) == Config::HOST;
        }

        if ($this->isFromClient) {
            $this->handleActivityFromClient();
        } else {
            $this->handleActivityFromServer();
        }
    }

    /**
     * Handles the activity from a client.
     *
     * @return void
     */
    abstract protected function handleActivityFromClient();

    /**
     * Handles the activity from a server.
     *
     * @return void
     */
    abstract protected function handleActivityFromServer();
}
