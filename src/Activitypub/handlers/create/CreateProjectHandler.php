<?php

namespace PixelHumain\Models\Activitypub\handlers\create;

use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;

class CreateProjectHandler extends CreateHandler
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use ParamsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    public function __construct(array $config, ActivityInterface $activity)
    {
        $cows = $this->params->get('cows');
        parent::__construct($config, $activity, $cows["pingNewEventUrl"]);
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateParamsProperty();
    }
}
