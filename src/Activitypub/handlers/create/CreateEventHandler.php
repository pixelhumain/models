<?php

namespace PixelHumain\Models\Activitypub\handlers\create;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\EventTrait;
use PixelHumain\Models\Traits\Interfaces\EventTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\PreferenceTrait;

class CreateEventHandler extends AbstractHandler implements EventTraitInterface, PersonTraitInterface, PreferenceTraitInterface, ActivitypubActivityTraitInterface, ActivitypubActorTraitInterface, UtilsTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;
    use ParamsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use EventTrait;
    use PersonTrait;
    use PreferenceTrait;

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubActorTrait;
    use UtilsTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18NProperty();
        $this->validateParamsProperty();
    }

    public function __construct(array $config, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($config, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        if (is_array($this->activity->get("to"))) {
            $targetIds = array_merge($this->activity->get("to"));
        }
        if (is_array($this->activity->get("cc"))) {
            $targetIds = array_merge($this->activity->get("cc"));
        }

        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }

        $this->getModelActivitypubActivity()->save($this->activity, $targets, []);
        $targetInboxes = [];
        foreach ($targets as $target) {
            if (is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"]) && strpos($target->get("endpoints")["sharedInbox"], "@") !== false) {
                if (! in_array($target->get("endpoints")["sharedInbox"], $targetInboxes)) {
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
                }
            } else {
                $targetInboxes[] = $target->get("inbox");
            }
        }
        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get('id'),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = [];
        $to = $this->activity->get("to");

        if (is_array($to) && in_array(UtilsInterface::PUBLIC_INBOX, $to)) {
            $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->activity->get("attributedTo"));
        }
        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));
        $summary = $this->language->t(
            "activitypub",
            "{who} has create new event '{what}'",
            [
                "{who}" => $this->actor->get('preferredUsername'),
                "{what}" => $object->get('name'),
            ]
        );
        $this->activity->set('summary', $summary);
        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets, []);
        $data = $this->getModelUtils()->parseToEvent($object, $UUIDS["object"]);
        $this->db->insert(EventInterface::COLLECTION, $data);
        $this->pingTargets($targets);
        // notify user
        if ((is_countable($targets) ? count($targets) : 0) > 0) {
            $this->getModelActivityNotification()->send($this->activity, $this->actor, $targets);
        } else {
            $this->getModelActivityNotification()->send($this->activity, $this->actor);
        }
    }

    private function pingTargets($targets)
    {
        $targetIds = [];
        foreach ($targets as $target) {
            $user = $this->db->findOne(PersonInterface::COLLECTION, [
                "username" => $target->get("preferredUsername"),
            ]);
            if ($user && isset($user["preferences"]) && $this->getModelPreference()->isActivitypubActivate($user["preferences"])) {
                $targetIds[] = (string) $user["_id"];
            }
        }

        $coWs = $this->params->get('cows');
        $curl = curl_init($coWs["pingNewEventUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
            "users" => $targetIds,
        ]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_exec($curl);
    }
}
