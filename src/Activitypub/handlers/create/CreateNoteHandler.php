<?php

namespace PixelHumain\Models\Activitypub\handlers\create;

use PixelHumain\Models\Activitypub\handlers\AbstractHandler;
use PixelHumain\Models\Activitypub\notification\Traits\ActivityNotificationTrait;
use PixelHumain\Models\Activitypub\notification\Traits\Interfaces\ActivityNotificationTraitInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActivityTrait;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActivityTraitInterface;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Interfaces\UtilsInterface;

use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\PreferenceTrait;

class CreateNoteHandler extends AbstractHandler implements PreferenceTraitInterface, ActivitypubActivityTraitInterface, ActivitypubActorTraitInterface, ActivityNotificationTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    private $payload;

    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use ParamsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PreferenceTrait;

    /**
     * Les traits activitypub
     */
    use ActivitypubActivityTrait;
    use ActivitypubActorTrait;
    use ActivityNotificationTrait;
    use TypeTrait;
    use RequestTrait;

    private $pingUrl;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateParamsProperty();
    }

    public function __construct(array $congig, ActivityInterface $activity, $payload = null)
    {
        parent::__construct($congig, $activity);
        $this->payload = $payload;
    }

    protected function handleActivityFromClient()
    {
        $targets = [];
        $targetIds = [];
        if (is_array($this->activity->get("to"))) {
            $targetIds = array_merge($this->activity->get("to"));
        }
        if (is_array($this->activity->get("cc"))) {
            $targetIds = array_merge($this->activity->get("cc"));
        }
        foreach ($targetIds as $id) {
            if ($id !== UtilsInterface::PUBLIC_INBOX) {
                $targets[] = $this->getModeltypeAp()->createFromAnyValue($id);
            }
        }

        $this->getModelActivitypubActivity()->save($this->activity, $targets, $this->payload);

        //group targets by domain name
        $targetInboxes = [];
        foreach ($targets as $target) {
            if (is_array($target->get("endpoints")) && isset($target->get("endpoints")["sharedInbox"]) && strpos($target->get("endpoints")["sharedInbox"], "@") !== false) {
                if (! in_array($target->get("endpoints")["sharedInbox"], $targetInboxes)) {
                    $targetInboxes[] = $target->get("endpoints")["sharedInbox"];
                }
            } else {
                $targetInboxes[] = $target->get("inbox");
            }
        }
        foreach ($targetInboxes as $inbox) {
            $this->getModelRequest()->post(
                $this->actor->get("id"),
                $inbox,
                $this->activity->toArray(),
                true
            );
        }
    }

    protected function handleActivityFromServer()
    {
        $targets = [];

        $to = $this->activity->get("to");
        if (is_array($to) && in_array(UtilsInterface::PUBLIC_INBOX, $to)) {
            $targets = $this->getModelActivitypubActor()->getLocalFollowersOfExternalUser($this->actor->get("id"));
        }

        $object = $this->getModeltypeAp()->createFromAnyValue($this->activity->get("object"));

        $UUIDS = $this->getModelActivitypubActivity()->save($this->activity, $targets);
        $date = $this->db->MongoDate(time());
        $news = [
            "type" => "activitypub",
            "author" => $this->actor->get("id"),
            "created" => $date,
            "date" => $date,
            "sharedBy" => [
                "id" => $this->actor->get("id"),
                "updated" => $date,
            ],
            "updated" => $date,
            "objectUUID" => $UUIDS["object"],
            "objectId" => $object->get("id"),
            "scope" => [
                'type' => 'restricted',
            ],
        ];
        $news["fediTags"] = $this->getModelActivitypubLink()->getActorTags($this->activity->get('actor'));
        if (! empty($this->activity->get('target'))) {
            $obj = $this->getModeltypeAp()->createFromAnyValue($this->activity->get('target'));
            $project = $this->db->findOne(ProjectInterface::COLLECTION, [
                "objectId" => $obj->get('id'),
            ]);
            $news["target"] = [
                'id' => (string) $project['_id'],
                "type" => $obj->get('type'),
            ];
        }
        $isNotAreplyTo = ! $object->get("inReplyTo");
        if ($isNotAreplyTo) {
            $this->db->insert(NewsInterface::COLLECTION, $news);
            $this->pingTargets($targets);
            // notify user
            if ((is_countable($targets) ? count($targets) : 0) > 0) {
                $this->getModelActivityNotification()->send($this->activity, $this->actor, $targets);
            } else {
                $this->getModelActivityNotification()->send($this->activity, $this->actor);
            }
        }
    }

    protected function pingTargets($targets)
    {
        $targetIds = [];
        foreach ($targets as $target) {
            $user = $this->db->findOne(PersonInterface::COLLECTION, [
                "username" => $target->get("preferredUsername"),
            ]);
            if ($user && isset($user["preferences"]) && $this->getModelPreference()->isActivitypubActivate($user["preferences"])) {
                $targetIds[] = (string) $user["_id"];
            }
        }

        $coWs = $this->params->get('cows');
        $curl = curl_init($coWs["pingNewPostUrl"]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode([
            "users" => $targetIds,
        ]));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_exec($curl);
    }

    private function mockPayload($url)
    {
        $url_path = parse_url($url, PHP_URL_PATH);
        $basename = pathinfo($url_path, PATHINFO_BASENAME);
        if (strpos($url, 'groups') !== false) {
            return [
                "name" => $basename,
                "type" => "group",
            ];
        } elseif (strpos($url, 'projects') !== false) {
            return [
                "name" => $basename,
                "type" => "project",
            ];
        } else {
            return [
                "name" => $basename,
                "type" => "person",
            ];
        }
    }

    private function getTagByActivityActor($actor)
    {
    }
}
