<?php

namespace PixelHumain\Models\Activitypub\handlers;

use PixelHumain\Models\Activitypub\handlers\Interfaces\HandlerInterface;
use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\TypeInterface;

class Handler implements HandlerInterface
{
    /**
     * Handles the given activity for the specified type.
     *
     * @param TypeInterface $typeAp The type of the activity.
     * @param ActivityInterface $activity The activity to handle.
     * @param mixed $payload Additional payload data (optional).
     * @return void|bool Returns false if no handler was found, otherwise void.
     */
    public static function handle(TypeInterface $typeAp, ActivityInterface $activity, $payload = null)
    {
        $object = $typeAp->createFromAnyValue($activity->get("object"));

        $activityType = (string) $activity->get("type");

        $className = $activityType;
        if (in_array($object->get("type"), ["Person", "Group"])) {
            $className .= "Person";
        } else {
            $className .= (string) $object->get("type");
        }
        $className .= "Handler";
        $class = __NAMESPACE__ . "\\" . strtolower($activityType) . "\\" . $className;
        if (! class_exists($class)) {
            return false;
        }

        $handler = new $class($activity, $payload);
        $handler->handle();
    }
}
