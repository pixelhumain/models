<?php

namespace PixelHumain\Models\Activitypub;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubCronInterface;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\RequestTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\RequestTrait;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class ActivitypubCron extends BaseModel implements ActivitypubCronInterface, UtilsTraitInterface, TypeTraitInterface, RequestTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use TypeTrait;
    use RequestTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function save($sender, $receiver, $data)
    {
        $this->db->insert(
            self::COLLECTION,
            [
                "sender" => $sender,
                "receiver" => $receiver,
                "data" => $data,
                "nbAttempt" => 0,
                "status" => self::STATUS_PENDING,
                "createAt" => $this->db->MongoDate(),
            ]
        );
    }

    public function processCron()
    {
        $jobs = $this->db->findAndSort(self::COLLECTION, [
            "status" => self::STATUS_PENDING,
        ], [
            "createAt" => 1,
        ], self::EXEC_COUNT);

        foreach ($jobs as $job) {
            $nbAttempt = intval($job["nbAttempt"]) + 1;

            if (in_array($job["data"]["type"], ["Create", "Update", "Undo"])) {
                $object = $this->getModelTypeAp()->createFromAnyValue($job["data"]["object"]);
                $job["data"]["object"] = $object->toArray();
                $job = $this->getModelUtils()->jobToRunBeforePost($job);
            }
            $success = $this->getModelRequest()->post($job["sender"], $job["receiver"], $job["data"]);

            $data = [
                "updateAt" => $this->db->MongoDate(),
                "nbAttempt" => $nbAttempt,
            ];
            if ($success) {
                $data["status"] = self::STATUS_DONE;
            } else {
                $data["status"] = $nbAttempt > self::MAX_ATTEMPT ? self::STATUS_FAIL : self::STATUS_PENDING;
            }

            $this->db->update(self::COLLECTION, [
                "_id" => $job["_id"],
            ], [
                '$set' => $data,
            ]);
        }
    }
}
