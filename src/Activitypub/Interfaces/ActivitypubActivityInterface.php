<?php

namespace PixelHumain\Models\Activitypub\Interfaces;

use PixelHumain\Models\Activitypub\type\core\Interfaces\ActivityInterface;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;

interface ActivitypubActivityInterface
{
    public const COLLECTION = "activitypub_activity";

    /**
     * Saves an activity.
     *
     * @param ActivityInterface $activity The activity to be saved.
     * @param array $receivers The receivers of the activity.
     * @param mixed $payload The payload associated with the activity.
     * @return array The saved activity.
     */
    public function save(ActivityInterface $activity, array $receivers, $payload = null): array;

    /**
     * Retrieves an activity by its ID.
     *
     * @param string $id The ID of the activity.
     * @return array|bool The activity if it exists, false otherwise.
     */
    public function getByActivityId(string $id);

    /**
     * Retrieves an activity by its object ID.
     *
     * @param string $id The object ID of the activity.
     * @return array|bool The activity if it exists, false otherwise.
     */
    public function getByActivityByObjectId(string $id);

    /**
     * Retrieves an activity by its UUID.
     *
     * @param string $uuid The UUID of the activity.
     * @return ActivityInterface|null The activity object if found, null otherwise.
     */
    public function getByUUID(string $uuid): ?ActivityInterface;

    /**
     * Returns a link activity.
     *
     * @param string $linkType The type of link.
     * @param string $localUserId The ID of the local user.
     * @param AbstractActorInterface $invitor The invitor actor.
     * @param string $actorType The type of actor.
     * @return ActivityInterface The link activity.
     */
    public function getLinkActivity(string $linkType, string $localUserId, AbstractActorInterface $invitor, string $actorType): ActivityInterface;

    /**
     * Returns an activity for linking a project.
     *
     * @param string $linkType The type of link.
     * @param AbstractActorInterface $subject The subject of the activity.
     * @param AbstractActorInterface $invitor The invitor of the activity.
     * @return ActivityInterface The activity for linking a project.
     */
    public function getLinkActivityProject(string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): ActivityInterface;

    /**
     * Undocumented function
     *
     * @param string $elementType
     * @param string $linkTypes
     * @param AbstractActorInterface $subject
     * @param AbstractActorInterface $invitor
     * @return mixed
     */
    public function getElementLinkActivity(string $elementType, string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor): ActivityInterface;

    /**
     * Returns an array of all CC recipients for the given element type, link type, subject, invitor, and admin status.
     *
     * @param string $elementType The element type.
     * @param string $linkType The link type.
     * @param AbstractActorInterface $subject The subject.
     * @param AbstractActorInterface $invitor The invitor.
     * @param bool $isAdmin Whether the user is an admin or not.
     * @return array An array of all CC recipients.
     */
    public function getAllCCRecipients(string $elementType, string $linkType, AbstractActorInterface $subject, AbstractActorInterface $invitor, bool $isAdmin = false): array;

    /**
     * Retrieves activities within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain to filter activities by.
     * @param bool $isLocalActivity Whether to include only local activities.
     * @param string $type The type of activities to retrieve.
     * @return array An array of activities within the specified date range.
     */
    public function getActivityByDateRange(string $startDate, string $endDate, string $domain, bool $isLocalActivity, string $type): array;

    /**
     * Returns an array of instances interacting within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return array An array of instances interacting within the specified date range.
     */
    public function getInstanceInteractingByDateRange(string $startDate, string $endDate): array;

    /**
     * Retrieves interactions within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain of the interactions.
     * @param string $refdomain The reference domain for the interactions.
     * @return array An array of interactions within the specified date range.
     */
    public function getInteractionByDateRange(string $startDate, string $endDate, string $domain, string $refdomain): array;

    /**
     * Retrieves activities by type and date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain of the activities.
     * @param string $refdomain The reference domain of the activities.
     * @return array An array of activities matching the specified criteria.
     */
    public function getActivityByTypeAndDateRange(string $startDate, string $endDate, string $domain, string $refdomain): array;

    /**
     * Retrieves activities within a specified date range and of a specific type.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @param string $domain The domain to filter activities by.
     * @param string $type The type of activities to retrieve.
     * @param string|null $refDomain The reference domain to filter activities by (optional).
     * @return array An array of activities matching the specified criteria.
     */
    public function getActivityByDateRangeAndType(string $startDate, string $endDate, string $domain, string $type, ?string $refDomain = null): array;
}
