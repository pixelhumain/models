<?php

namespace PixelHumain\Models\Activitypub\Interfaces;

interface ActivitypubLinkInterface
{
    // public const for actor
    public const GROUP = 'group';

    public const PERSON = 'person';

    public const PROJECT = 'project';

    // constant for project
    public const CONTRIBUTION = 'contributor';

    public const ADMIN = 'admin';

    public const ADD_AS_ADMIN = 'asadmin';

    public const ADD_WITH_RULES = 'withRules';

    public const BECOME_ADMIN = 'becomeadmin';

    public const ADMIN_AND_ROLE = 'adminandrole';

    public const INVITE_WITH_RULES = 'inviteWithRules';

    public const CONTRIBUTOR = 'contributor';

    public const FOLLOW = 'follow';
}
