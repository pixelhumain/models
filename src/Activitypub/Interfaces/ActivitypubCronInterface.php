<?php

namespace PixelHumain\Models\Activitypub\Interfaces;

interface ActivitypubCronInterface
{
    public const COLLECTION = "activitypub_cron";

    public const EXEC_COUNT = 5;

    public const MAX_ATTEMPT = 5;

    public const STATUS_PENDING = "pending";

    public const STATUS_DONE = "done";

    public const STATUS_FAIL = "fail";
}
