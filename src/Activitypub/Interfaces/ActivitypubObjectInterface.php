<?php

namespace PixelHumain\Models\Activitypub\Interfaces;

use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\Interfaces\AbstractObjectInterface;

interface ActivitypubObjectInterface
{
    public const COLLECTION = "activitypub_object";

    /**
     * Saves the activitypub object.
     *
     * @param AbstractActorInterface $creator The creator of the object.
     * @param array $targets The targets of the object.
     * @param AbstractObjectInterface $object The object to be saved.
     * @param mixed $payload Additional payload data.
     * @return string|null The ID of the saved object, or null if saving failed.
     */
    public function save(AbstractActorInterface $creator, array $targets, AbstractObjectInterface $object, $payload = null): ?string;

    /**
     * Retrieves an object by its ID.
     *
     * @param string $id The ID of the object.
     * @return AbstractObjectInterface|null The object with the specified ID, or null if not found.
     */
    public function getObjectById(string $id): ?AbstractObjectInterface;

    /**
     * Retrieve an object by its ID and return it as an array.
     *
     * @param string $id The ID of the object.
     * @return array|null The object as an array or null if not found.
     */
    public function getObjectByIdAsArray(string $id): ?array;

    /**
     * Retrieves an object by its UUID.
     *
     * @param string $uuid The UUID of the object.
     * @return AbstractObjectInterface|null The object with the specified UUID, or null if not found.
     */
    public function getObjectByUUID(string $uuid): ?AbstractObjectInterface;

    /**
     * Retrieves the replies of a note.
     *
     * @param string $noteId The ID of the note.
     * @param int|null $index The index of the first reply to retrieve (optional).
     * @param int $limit The maximum number of replies to retrieve (default: 5).
     * @return array An array containing the replies of the note.
     */
    public function getRepliesOfNote(string $noteId, ?int $index = null, int $limit = 5): array;

    /**
     * Retrieves the comments for a news article.
     *
     * @param string $newsId The ID of the news article.
     * @return array An array of comments for the news article.
     */
    public function getNewsComments(string $newsId): array;

    /**
     * Retrieves an object by its news ID.
     *
     * @param string $newsId The news ID.
     * @return AbstractObjectInterface|null The object corresponding to the news ID, or null if not found.
     */
    public function getObjectByNewsId(string $newsId): ?AbstractObjectInterface;

    /**
     * Removes an object by its ID.
     *
     * @param string $objectId The ID of the object to be removed.
     * @return void
     */
    public function removeByObjectId(string $objectId): void;

    /**
     * Retrieves the notes associated with the activitypub object for a given user.
     *
     * @param string $userAdress The user's address.
     * @return array An array of notes.
     */
    public function getNotes(string $userAdress): array;

    /**
     * Returns an array of projects for a given user.
     *
     * @param string $user The username of the user.
     * @return array An array of projects.
     */
    public function getProjects(string $user): array;

    /**
     * Retrieves activities within a specified date range.
     *
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return array An array of activities within the specified date range.
     */
    public function getActivityByDateRange(string $startDate, string $endDate): array;
}
