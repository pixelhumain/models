<?php

namespace PixelHumain\Models\Activitypub\Interfaces;

interface ActivitypubActivityCreatorInterface
{
    public const CONTRIBUTORS = "contributors";
}
