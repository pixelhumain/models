<?php

namespace PixelHumain\Models\Activitypub;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubTranslatorInterface;
use PixelHumain\Models\Activitypub\Traits\ActivitypubActorTrait;
use PixelHumain\Models\Activitypub\Traits\Interfaces\ActivitypubActorTraitInterface;
use PixelHumain\Models\Activitypub\type\extended\actor\Group;
use PixelHumain\Models\Activitypub\type\extended\actor\Person;
use PixelHumain\Models\Activitypub\type\extended\Interfaces\AbstractActorInterface;
use PixelHumain\Models\Activitypub\type\extended\object\Image;
use PixelHumain\Models\Activitypub\type\extended\object\Note;
use PixelHumain\Models\Activitypub\type\Traits\Interfaces\TypeTraitInterface;
use PixelHumain\Models\Activitypub\type\Traits\TypeTrait;
use PixelHumain\Models\Activitypub\type\Type;
use PixelHumain\Models\Activitypub\utils\Traits\Interfaces\UtilsTraitInterface;
use PixelHumain\Models\Activitypub\utils\Traits\UtilsTrait;
use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\PixelHumain\modules\news\models\UrlExtractor;

// TODO : Yii::get('module')->getModule('co2')->assetsUrl
// TODO : UrlExtractor::extract

class ActivitypubTranslator extends BaseModel implements ActivitypubTranslatorInterface, UtilsTraitInterface, ActivitypubActorTraitInterface, TypeTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits activitypub
     */
    use UtilsTrait;
    use ActivitypubActorTrait;
    use TypeTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    //get actor adress (ex: actorname@domain.com) from Actor
    public function getActorAdress(AbstractActorInterface $actor, $withoutDot = false): string
    {
        $address = $actor->get("preferredUsername") . "@" . parse_url($actor->get("id"), PHP_URL_HOST);
        if ($withoutDot) {
            $address = str_replace(".", "_", $address);
        }
        return $address;
    }

    public function setActivityPubProperties($object, $type, $slug, $image_url = null)
    {
        $object->setPreferredUsername($slug);
        $object->setInbox($this->getModelUtils()->createUrl("api/activitypub/inbox/p/" . $slug));
        $object->set_extend_props("manuallyApprovesFollowers", true);
        $object->setPublicKey([
            "id" => $this->getModelUtils()->createUrl("api/activitypub/" . $type . "\/p/" . $slug . "#main-key"),
            "owner" => $this->getModelUtils()->createUrl("api/activitypub/" . $type . "/p/" . $slug),
            "publicKeyPem" => $this->getModelUtils()->getPem("public"),
        ]);
        $object->setEndpoints([
            "sharedInbox" => $this->getModelUtils()->createUrl("api/activitypub/inbox"),
        ]);

        if (isset($image_url)) {
            $fileExtension = pathinfo($image_url, PATHINFO_EXTENSION);
            $mediaType = ($fileExtension == "png") ? "image/png" : "image/jpeg";

            $icon = new Image();
            $icon->set("url", $this->getModelUtils()->createUrl($image_url));
            $icon->set("mediaType", $mediaType);
            $object->set("icon", $icon);
        }
    }

    //convert CO user to activitystream actor
    public function coPersonToActor($person): AbstractActorInterface
    {
        $actor = new Person();
        $actor->set_context([
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            [
                "manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
            ],
        ]);

        $actor->setId($this->getModelUtils()->createUrl("api/activitypub/users/u/" . $person["username"]));
        $actor->setName($person["name"]);
        $actor->setPreferredUsername($person["username"]);
        $actor->setInbox($this->getModelUtils()->createUrl("api/activitypub/inbox/u/" . $person["username"]));
        $actor->set_extend_props("manuallyApprovesFollowers", true);
        $actor->setPublicKey([
            "id" => $this->getModelUtils()->createUrl("api/activitypub/person/u/" . $person["username"] . "#main-key"),
            "owner" => $this->getModelUtils()->createUrl("api/activitypub/person/u/" . $person["username"]),
            "publicKeyPem" => $this->getModelUtils()->getPem("public"),
        ]);
        $actor->setEndpoints([
            "sharedInbox" => $this->getModelUtils()->createUrl("api/activitypub/inbox"),
            "followers" => $this->getModelUtils()->createUrl("api/activitypub/followers/u/" . $person["username"]),
            "projects" => $this->getModelUtils()->createUrl("api/activitypub/projects/p/" . $person["username"]),
        ]);

        return self::getIcon($actor);
    }

    public function noteToNewsMedia(Note $note)
    {
        $actor = $this->getModelTypeAp()->createFromAnyValue($note->get("attributedTo"));

        $media = [
            "id" => $note->get("id"),
            "content" => $note->get("content"),
            "attachments" => [],
            "nbReplies" => 0,
            "url" => $note->get("url") ?: $note->get("id"),
        ];

        if (is_array($note->get("attachment"))) {
            foreach ($note->get("attachment") as $attachment) {
                $isValidImageType = in_array($attachment->get("mediaType"), ['image/png', 'image/jpeg', 'image/jpg']);
                $isValidUrl = filter_var($attachment->get("url"), FILTER_VALIDATE_URL);

                if ($isValidImageType && $isValidUrl) {
                    $media["attachments"][] = $attachment->get("url");
                }
            }
        }

        $media["author"] = [
            "id" => $actor->get("id"),
            "name" => $actor->get("name"),
            "address" => self::getActorAdress($actor),
            "avatar" => Yii::get('module')->getModule('co2')->assetsUrl . "/images/avatar.jpg",
        ];
        if ($actor->get("icon") && $actor->get("icon")->get("type") == "Image") {
            $media["author"]["avatar"] = $actor->get("icon")->get("url");
        }

        //fetch provider info
        $actorIdParts = parse_url($actor->get("id"));
        $actorSite = $actorIdParts["scheme"] . "://" . $actorIdParts["host"];
        $noteArray = $note->toArray();
        if (isset($noteArray['providerInfo'])) {
            $providerInfo = $noteArray['providerInfo'];
        } else {
            $providerInfo = UrlExtractor::extract($actorSite);
        }

        $media["providerInfo"] = [
            "title" => $providerInfo["title"] ?? $actorIdParts["host"],
        ];
        if (isset($providerInfo["meta"]["description"])) {
            $media["providerInfo"]["description"] = $providerInfo["meta"]["description"];
        }
        if (isset($providerInfo["link"]["icon"])) {
            $media["providerInfo"]["icon"] = $providerInfo["link"]["icon"];
        }

        return $media;
    }

    //convert CO group to activitystream actor
    public function coGroupToActor($group): AbstractActorInterface
    {
        $gactor = new Group();
        $gactor->set_context([
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            [
                "manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
            ],
        ]);
        $gactor->setId($this->getModelUtils()->createUrl("api/activitypub/groups/g/" . $group["slug"]));
        $gactor->setId($this->getModelUtils()->createUrl("api/activitypub/groups/g/" . $group["slug"]));
        $gactor->setName($group["slug"]);
        $gactor->setPreferredUsername($group["slug"]);
        $gactor->setInbox($this->getModelUtils()->createUrl("api/activitypub/inbox/g/" . $group["slug"]));
        $gactor->set_extend_props("manuallyApprovesFollowers", true);
        $gactor->setPublicKey([
            "id" => $this->getModelUtils()->createUrl("api/activitypub/groups/g/" . $group["slug"] . "#main-key"),
            "owner" => $this->getModelUtils()->createUrl("api/activitypub/groups/g/" . $group["slug"]),
            "publicKeyPem" => $this->getModelUtils()->getPem("public"),
        ]);
        $gactor->setEndpoints([
            "sharedInbox" => $this->getModelUtils()->createUrl("api/activitypub/inbox"),
        ]);

        return self::getIcon($gactor);
    }

    public function actorTocoUser($value)
    {
        $date = $this->db->MongoDate(time());
        if (! isset($value["invitorId"]) || ! filter_var($value["invitorId"], FILTER_VALIDATE_URL)) {
            return;
        }
        $adress = $this->getModelUtils()->getLinkToAdress($value["invitorId"]);
        $actor = $this->getModelActivitypubActor()->searchActorByRessourceAddress($adress);
        $actorArray = $actor->toArray();
        $data = [
            "id" => $value['invitorId'],
            "rolesAndStatusLink" => [
                "roles" => $value['roles'] ?? [],
            ],
            "activitypub" => true,
            "collection" => "citoyens",
            "name" => $actorArray['name'],
            'created' => $date,
            'updated' => $date,
            "sorting" => $date,
            "preferences" => [
                "activitypub" => true,
            ],
        ];
        if (isset($value['isAdmin'])) {
            $data['rolesAndStatusLink']['isAdmin'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isInviting'])) {
            $data['rolesAndStatusLink']['isInviting'] = true;
        }
        if (isset($value['isAdminPending'])) {
            $data['rolesAndStatusLink']['isAdminPending'] = true;
        }
        if (isset($value['toBeValidated'])) {
            $data['rolesAndStatusLink']['toBeValidated'] = true;
        }
        return $data;
    }

    public function getIcon($actor)
    {
        $person = [];
        if (isset($person["profilImageUrl"])) {
            $fileExtension = pathinfo($person["profilImageUrl"], PATHINFO_EXTENSION);
            $mediaType = ($fileExtension == "png") ? "image/png" : "image/jpeg";

            $icon = new Image();
            $icon->set("url", $this->getModelUtils()->createUrl($person["profilImageUrl"]));
            $icon->set("mediaType", $mediaType);

            $actor->set("icon", $icon);
        }
        return $actor;
    }
}
