<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubDynamicObjectInterface;

trait ActivitypubDynamicObjectTrait
{
    protected ?ActivitypubDynamicObjectInterface $activitypubDynamicObject = null;

    /**
     * Set the activitypubDynamicObject.
     *
     * @param ActivitypubDynamicObjectInterface $activitypubDynamicObject The activitypubDynamicObject object to set.
     * @return void
     */
    public function setModelActivitypubDynamicObject(ActivitypubDynamicObjectInterface $activitypubDynamicObject): void
    {
        $this->activitypubDynamicObject = $activitypubDynamicObject;
    }

    /**
     * Gets the activitypubDynamicObject.
     *
     * @return ActivitypubDynamicObjectInterface The activitypubDynamicObject.
     */
    public function getModelActivitypubDynamicObject(): ActivitypubDynamicObjectInterface
    {
        if ($this->activitypubDynamicObject === null) {
            throw new Exception(ActivitypubDynamicObjectInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubDynamicObject;
    }
}
