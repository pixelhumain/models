<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubCronInterface;

interface ActivitypubCronTraitInterface
{
    /**
     * Set the activitypubCron.
     *
     * @param ActivitypubCronInterface $activitypubCron The activitypubCron object to set.
     * @return void
     */
    public function setModelActivitypubCron(ActivitypubCronInterface $activitypubCron): void;

    /**
     * Gets the activitypubCron.
     *
     * @return ActivitypubCronInterface The activitypubCron.
     */
    public function getModelActivitypubCron(): ActivitypubCronInterface;
}
