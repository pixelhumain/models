<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityCreatorInterface;

interface ActivitypubActivityCreatorTraitInterface
{
    /**
     * Set the activitypubActivityCreator.
     *
     * @param ActivitypubActivityCreatorInterface $activitypubActivityCreator The activitypubActivityCreator object to set.
     * @return void
     */
    public function setModelActivitypubActivityCreator(ActivitypubActivityCreatorInterface $activitypubActivityCreator): void;

    /**
     * Gets the activitypubActivityCreator.
     *
     * @return ActivitypubActivityCreatorInterface The activitypubActivityCreator.
     */
    public function getModelActivitypubActivityCreator(): ActivitypubActivityCreatorInterface;
}
