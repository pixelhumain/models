<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;

interface ActivitypubObjectTraitInterface
{
    /**
     * Set the activitypubObject.
     *
     * @param ActivitypubObjectInterface $activitypubObject The activitypubObject object to set.
     * @return void
     */
    public function setModelActivitypubObject(ActivitypubObjectInterface $activitypubObject): void;

    /**
     * Gets the activitypubObject.
     *
     * @return ActivitypubObjectInterface The activitypubObject.
     */
    public function getModelActivitypubObject(): ActivitypubObjectInterface;
}
