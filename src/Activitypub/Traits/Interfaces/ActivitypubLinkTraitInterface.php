<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubLinkInterface;

interface ActivitypubLinkTraitInterface
{
    /**
     * Set the activitypubLink.
     *
     * @param ActivitypubLinkInterface $activitypubLink The activitypubLink object to set.
     * @return void
     */
    public function setModelActivitypubLink(ActivitypubLinkInterface $activitypubLink): void;

    /**
     * Gets the activitypubLink.
     *
     * @return ActivitypubLinkInterface The activitypubLink.
     */
    public function getModelActivitypubLink(): ActivitypubLinkInterface;
}
