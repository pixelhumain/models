<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubDynamicObjectInterface;

interface ActivitypubDynamicObjectTraitInterface
{
    /**
     * Set the activitypubDynamicObject.
     *
     * @param ActivitypubDynamicObjectInterface $activitypubDynamicObject The activitypubDynamicObject object to set.
     * @return void
     */
    public function setModelActivitypubDynamicObject(ActivitypubDynamicObjectInterface $activitypubDynamicObject): void;

    /**
     * Gets the activitypubDynamicObject.
     *
     * @return ActivitypubDynamicObjectInterface The activitypubDynamicObject.
     */
    public function getModelActivitypubDynamicObject(): ActivitypubDynamicObjectInterface;
}
