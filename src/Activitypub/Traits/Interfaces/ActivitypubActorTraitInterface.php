<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActorInterface;

interface ActivitypubActorTraitInterface
{
    /**
     * Set the activitypubActor.
     *
     * @param ActivitypubActorInterface $activitypubActor The activitypubActor object to set.
     * @return void
     */
    public function setModelActivitypubActor(ActivitypubActorInterface $activitypubActor): void;

    /**
     * Gets the activitypubActor.
     *
     * @return ActivitypubActorInterface The activitypubActor.
     */
    public function getModelActivitypubActor(): ActivitypubActorInterface;
}
