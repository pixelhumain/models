<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityInterface;

interface ActivitypubActivityTraitInterface
{
    /**
     * Set the activitypubActivity.
     *
     * @param ActivitypubActivityInterface $activitypubActivity The activitypubActivity object to set.
     * @return void
     */
    public function setModelActivitypubActivity(ActivitypubActivityInterface $activitypubActivity): void;

    /**
     * Gets the activitypubActivity.
     *
     * @return ActivitypubActivityInterface The activitypubActivity.
     */
    public function getModelActivitypubActivity(): ActivitypubActivityInterface;
}
