<?php

namespace PixelHumain\Models\Activitypub\Traits\Interfaces;

use PixelHumain\Models\Activitypub\Interfaces\ActivitypubTranslatorInterface;

interface ActivitypubTranslatorTraitInterface
{
    /**
     * Set the activitypubTranslator.
     *
     * @param ActivitypubTranslatorInterface $activitypubTranslator The activitypubTranslator object to set.
     * @return void
     */
    public function setModelActivitypubTranslator(ActivitypubTranslatorInterface $activitypubTranslator): void;

    /**
     * Gets the activitypubTranslator.
     *
     * @return ActivitypubTranslatorInterface The activitypubTranslator.
     */
    public function getModelActivitypubTranslator(): ActivitypubTranslatorInterface;
}
