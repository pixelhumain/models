<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubCronInterface;

trait ActivitypubCronTrait
{
    protected ?ActivitypubCronInterface $activitypubCron = null;

    /**
     * Set the activitypubCron.
     *
     * @param ActivitypubCronInterface $activitypubCron The activitypubCron object to set.
     * @return void
     */
    public function setModelActivitypubCron(ActivitypubCronInterface $activitypubCron): void
    {
        $this->activitypubCron = $activitypubCron;
    }

    /**
     * Gets the activitypubCron.
     *
     * @return ActivitypubCronInterface The activitypubCron.
     */
    public function getModelActivitypubCron(): ActivitypubCronInterface
    {
        if ($this->activitypubCron === null) {
            throw new Exception(ActivitypubCronInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubCron;
    }
}
