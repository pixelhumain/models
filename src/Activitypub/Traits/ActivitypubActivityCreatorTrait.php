<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityCreatorInterface;

trait ActivitypubActivityCreatorTrait
{
    protected ?ActivitypubActivityCreatorInterface $activitypubActivityCreator = null;

    /**
     * Set the activitypubActivityCreator.
     *
     * @param ActivitypubActivityCreatorInterface $activitypubActivityCreator The activitypubActivityCreator object to set.
     * @return void
     */
    public function setModelActivitypubActivityCreator(ActivitypubActivityCreatorInterface $activitypubActivityCreator): void
    {
        $this->activitypubActivityCreator = $activitypubActivityCreator;
    }

    /**
     * Gets the activitypubActivityCreator.
     *
     * @return ActivitypubActivityCreatorInterface The activitypubActivityCreator.
     */
    public function getModelActivitypubActivityCreator(): ActivitypubActivityCreatorInterface
    {
        if ($this->activitypubActivityCreator === null) {
            throw new Exception(ActivitypubActivityCreatorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubActivityCreator;
    }
}
