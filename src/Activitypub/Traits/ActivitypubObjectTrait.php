<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubObjectInterface;

trait ActivitypubObjectTrait
{
    protected ?ActivitypubObjectInterface $activitypubObject = null;

    /**
     * Set the activitypubObject.
     *
     * @param ActivitypubObjectInterface $activitypubObject The activitypubObject object to set.
     * @return void
     */
    public function setModelActivitypubObject(ActivitypubObjectInterface $activitypubObject): void
    {
        $this->activitypubObject = $activitypubObject;
    }

    /**
     * Gets the activitypubObject.
     *
     * @return ActivitypubObjectInterface The activitypubObject.
     */
    public function getModelActivitypubObject(): ActivitypubObjectInterface
    {
        if ($this->activitypubObject === null) {
            throw new Exception(ActivitypubObjectInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubObject;
    }
}
