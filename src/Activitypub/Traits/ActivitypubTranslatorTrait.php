<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubTranslatorInterface;

trait ActivitypubTranslatorTrait
{
    protected ?ActivitypubTranslatorInterface $activitypubTranslator = null;

    /**
     * Set the activitypubTranslator.
     *
     * @param ActivitypubTranslatorInterface $activitypubTranslator The activitypubTranslator object to set.
     * @return void
     */
    public function setModelActivitypubTranslator(ActivitypubTranslatorInterface $activitypubTranslator): void
    {
        $this->activitypubTranslator = $activitypubTranslator;
    }

    /**
     * Gets the activitypubTranslator.
     *
     * @return ActivitypubTranslatorInterface The activitypubTranslator.
     */
    public function getModelActivitypubTranslator(): ActivitypubTranslatorInterface
    {
        if ($this->activitypubTranslator === null) {
            throw new Exception(ActivitypubTranslatorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubTranslator;
    }
}
