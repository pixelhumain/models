<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubLinkInterface;

trait ActivitypubLinkTrait
{
    protected ?ActivitypubLinkInterface $activitypubLink = null;

    /**
     * Set the activitypubLink.
     *
     * @param ActivitypubLinkInterface $activitypubLink The activitypubLink object to set.
     * @return void
     */
    public function setModelActivitypubLink(ActivitypubLinkInterface $activitypubLink): void
    {
        $this->activitypubLink = $activitypubLink;
    }

    /**
     * Gets the activitypubLink.
     *
     * @return ActivitypubLinkInterface The activitypubLink.
     */
    public function getModelActivitypubLink(): ActivitypubLinkInterface
    {
        if ($this->activitypubLink === null) {
            throw new Exception(ActivitypubLinkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubLink;
    }
}
