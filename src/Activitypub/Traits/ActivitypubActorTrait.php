<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActorInterface;

trait ActivitypubActorTrait
{
    protected ?ActivitypubActorInterface $activitypubActor = null;

    /**
     * Set the activitypubActor.
     *
     * @param ActivitypubActorInterface $activitypubActor The activitypubActor object to set.
     * @return void
     */
    public function setModelActivitypubActor(ActivitypubActorInterface $activitypubActor): void
    {
        $this->activitypubActor = $activitypubActor;
    }

    /**
     * Gets the activitypubActor.
     *
     * @return ActivitypubActorInterface The activitypubActor.
     */
    public function getModelActivitypubActor(): ActivitypubActorInterface
    {
        if ($this->activitypubActor === null) {
            throw new Exception(ActivitypubActorInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubActor;
    }
}
