<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ConfigInterface;

trait ConfigTrait
{
    protected ?ConfigInterface $config = null;

    /**
     * Set the config.
     *
     * @param ConfigInterface $config The config object to set.
     * @return void
     */
    public function setModelConfig(ConfigInterface $config): void
    {
        $this->config = $config;
    }

    /**
     * Gets the config.
     *
     * @return ConfigInterface The config.
     */
    public function getModelConfig(): ConfigInterface
    {
        if ($this->config === null) {
            throw new Exception(ConfigInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->config;
    }
}
