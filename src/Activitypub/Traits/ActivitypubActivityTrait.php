<?php

namespace PixelHumain\Models\Activitypub\Traits;

use Exception;
use PixelHumain\Models\Activitypub\Interfaces\ActivitypubActivityInterface;

trait ActivitypubActivityTrait
{
    protected ?ActivitypubActivityInterface $activitypubActivity = null;

    /**
     * Set the activitypubActivity.
     *
     * @param ActivitypubActivityInterface $activitypubActivity The activitypubActivity object to set.
     * @return void
     */
    public function setModelActivitypubActivity(ActivitypubActivityInterface $activitypubActivity): void
    {
        $this->activitypubActivity = $activitypubActivity;
    }

    /**
     * Gets the activitypubActivity.
     *
     * @return ActivitypubActivityInterface The activitypubActivity.
     */
    public function getModelActivitypubActivity(): ActivitypubActivityInterface
    {
        if ($this->activitypubActivity === null) {
            throw new Exception(ActivitypubActivityInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->activitypubActivity;
    }
}
