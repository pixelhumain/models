<?php

namespace PixelHumain\Models\Activitypub;

/**
 * Class Config
 *
 * This class represents the configuration settings for the Activitypub module.
 */
class Config
{
    /**
     * The schema used for Activitypub URLs.
     */
    public const SCHEMA = "https";

    /**
     * The host name for the Activitypub server.
     */
    public const HOST = "communecter.org";
}
