<?php

namespace PixelHumain\Models\Activitypub;

/**
 * Class ActivitypubDynamicObject
 *
 * Represents a dynamic object in the ActivityPub model.
 */
class ActivitypubDynamicObject
{
    private array $attributes = [];

    /**
     * Set the value of an attribute.
     *
     * @param string $key The attribute key.
     * @param mixed $value The attribute value.
     * @return void
     */
    public function set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    /**
     * Get the value of an attribute.
     *
     * @param string $key The attribute key.
     * @return mixed|null The attribute value, or null if the attribute does not exist.
     */
    public function get($key)
    {
        return $this->attributes[$key] ?? null;
    }

    /**
     * Convert the object to an associative array.
     *
     * @return array The object as an associative array.
     */
    public function toArray()
    {
        $result = [];
        foreach ($this->attributes as $key => $value) {
            if ($value instanceof ActivitypubDynamicObject) {
                $result[$key] = $value->toArray();
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}
