<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\PoiInterface;

use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\SearchNewTrait;

// CO2::getContextList(self::MODULE)

class Poi extends BaseModel implements PoiInterface, SearchNewTraitInterface, DocumentTraitInterface, CommentTraitInterface, AuthorisationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SearchNewTrait;
    use DocumentTrait;
    use CommentTrait;
    use AuthorisationTrait;

    //TODO Translate
    public static array $types = [
        "compostPickup" => "récolte de composte",
        "video" => "video",
        "sharedLibrary" => "bibliothèque partagée",
        "artPiece" => "oeuvres",
        "recoveryCenter" => "ressourcerie",
        "trash" => "poubelle",
        "history" => "histoire",
        "something2See" => "chose a voir",
        "streetArts" => "arts de rue",
        "openScene" => "scène ouverte",
        "stand" => "stand",
        "parking" => "Parking",
        "other" => "Autre",
        "cms" => "CMS",
        "doc" => "Documentation",
    ];

    //From Post/Form name to database field name
    public array $dataBinding = [
        "section" => [
            "name" => "section",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "category" => [
            "name" => "category",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "scope" => [
            "name" => "scope",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "geo" => [
            "name" => "geo",
        ],
        "geoPosition" => [
            "name" => "geoPosition",
        ],
        "description" => [
            "name" => "description",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "structags" => [
            "name" => "structags",
        ],
        "level" => [
            "name" => "level",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "rank" => [
            "name" => "rank",
        ],
        "descriptionHTML" => [
            "name" => "descriptionHTML",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "source" => [
            "name" => "source",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "status" => [
            "name" => "status",
        ],
        "accompanimentType" => [
            "name" => "accompanimentType",
        ],
        "thematic" => [
            "name" => "thematic",
        ],
        "sizeInColonne" => [
            "name" => "sizeInColonne",
        ],
    ];

    // Todo n'est pas utilisé
    public array $collectionsList = ["Où sont les femmes", "Passeur d'images", "MHQM", "MIAA", "Portrait citoyens", "Parcours d'engagement"];

    public static array $genresList = ["Documentaire", "Fiction", "Docu-fiction", "Films outils", "Films de commande"];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public function getConfig(): array
    {
        return [
            "collection" => PoiInterface::COLLECTION,
            "controller" => PoiInterface::CONTROLLER,
            "categories" => CO2::getContextList(PoiInterface::MODULE),
            "lbhp" => true,
        ];
    }

    // Todo : n'est pas utilisé
    public function getPoiByIdAndTypeOfParent(string $id, string $type, array $orderBy = []): array
    {
        $pois = $this->db->findAndSort(PoiInterface::COLLECTION, [
            "parentId" => $id,
            "parentType" => $type,
        ], $orderBy);
        return $pois;
    }

    //TODO corriger ou supprimer -> DEPRECIATED use getPoiByTagWhereSortAndLimit
    public function getPoiByTagsAndLimit($limitMin = 0, $indexStep = 15, $searchByTags = [], $where = [], $fields = [])
    {
        if (empty($where)) {
            $where = [
                "name" => [
                    '$exists' => 1,
                ],
            ];
        } else {
            $where["name"] = [
                '$exists' => 1,
            ];
        }
        if (@$searchByTags && ! empty($searchByTags)) {
            $queryTag = [];
            foreach ($searchByTags as $key => $tag) {
                if ($tag != "") {
                    $queryTag[] = $this->db->MongoRegex("/" . $tag . "/i");
                }
            }
            if (! empty($queryTag)) {
                $where["tags"] = [
                    '$in' => $queryTag,
                ];
            }
        }

        $pois = $this->db->findAndSort(PoiInterface::COLLECTION, $where, [
            "updated" => -1,
        ], $limitMin, $fields);
        return $pois;
    }

    /**
     * Retrieves a list of POIs based on the specified conditions, sorted and limited.
     *
     * @param array $where An array of conditions to filter the POIs.
     * @param array $orderBy An array specifying the sorting order of the POIs. The keys represent the fields to sort by, and the values represent the sort order (1 for ascending, -1 for descending).
     * @param int $indexStep The number of POIs to retrieve per page.
     * @param int $indexMin The starting index of the first POI to retrieve.
     * @return array An array containing the retrieved POIs.
     */
    public function getPoiByWhereSortAndLimit(array $where = [], array $orderBy = [
        "updated" => -1,
    ], int $indexStep = 10, int $indexMin = 0): array
    {
        $pois = $this->db->findAndSortAndLimitAndIndex(PoiInterface::COLLECTION, $this->getModelSearchNew()->prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
        foreach ($pois as $key => $val) {
            $pois[$key]["typePoi"] = @$val["type"];
            $pois[$key]["type"] = PoiInterface::COLLECTION;
        }
        return $pois;
    }

    /**
     * Retrieves a Point of Interest (POI) by its structure.
     *
     * @param array $l The array containing the POIs.
     * @param string $val The value to search for.
     * @param string $field The field to search in (default: "tags").
     * @return array The array of matching POIs.
     */
    public function getPoiByStruct(array $l, string $val, string $field = "tags"): array
    {
        $res = [];

        foreach ($l as $k => $v) {
            if (isset($v[$field]) && is_string($v[$field]) && $val == $v[$field]) {
                $res[] = $v;
            } elseif (isset($v[$field]) && is_array($v[$field]) && in_array($val, $v[$field])) {
                $res[] = $v;
            }
        }
        return $res;
    }

    /**
     * Retrieves a Point of Interest by its ID.
     *
     * @param string $id The ID of the Point of Interest.
     * @return array|null The Point of Interest data as an array, or null if not found.
     */
    public function getById(string $id): ?array
    {
        $poi = $this->db->findOneById(PoiInterface::COLLECTION, $id);

        // Use case notragora
        if (@$poi["type"]) {
            $poi["typeSig"] = PoiInterface::COLLECTION . "." . $poi["type"];
        } else {
            $poi["typeSig"] = PoiInterface::COLLECTION;
        }
        if (@$poi["type"]) {
            $poi = $this->mergeWithImages($id, $poi);
        }

        $poi["images"] = $this->listImages($id);
        $poi["files"] = $this->listFiles($id);
        return $poi;
    }

    /**
     * Deletes a Poi.
     *
     * @param string $id The ID of the Poi to delete.
     * @param string|null $userId The ID of the user performing the deletion (optional).
     * @return array The result of the deletion operation.
     */
    public function delete(string $id, ?string $userId): array
    {
        if (! @$userId) {
            return [
                "result" => false,
                "msg" => "You must be loggued to delete something",
            ];
        }

        $poi = $this->getById($id);
        if (! $this->canDeletePoi($userId, $id, $poi)) {
            return [
                "result" => false,
                "msg" => "You are not authorized to delete this poi.",
            ];
        }

        //Delete the comments
        $resComments = $this->getModelComment()->deleteAllContextComments($id, PoiInterface::COLLECTION, $userId);
        if (@$resComments["result"]) {
            $this->db->remove(PoiInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            $resDocs = $this->getModelDocument()->removeDocumentByFolder(PoiInterface::COLLECTION . "/" . $id);
        } else {
            return $resComments;
        }

        return [
            "result" => true,
            "msg" => "The element has been deleted succesfully",
            "resDocs" => $resDocs,
        ];
    }

    /**
     * Determines if a user can delete a point of interest.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the point of interest.
     * @param array|null $poi The point of interest data (optional).
     * @return bool Returns true if the user can delete the point of interest, false otherwise.
     */
    public function canDeletePoi(string $userId, string $id, ?array $poi = null): bool
    {
        if ($poi == null) {
            $poi = $this->getById($id);
        }
        //To Delete POI, the user should be creator or can delete the parent of the POI
        if ($userId == @$poi['creator'] || $this->getModelAuthorisation()->canDeleteElement(@$poi["parentId"], @$poi["parentType"], $userId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns the data binding for the Poi class.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array
    {
        return $this->dataBinding;
    }

    /**
     * Returns the hierarchy of a list of points of interest based on a specified field.
     *
     * @param array $pList The list of points of interest.
     * @param string $field The field used to determine the hierarchy (default: "tags").
     * @return array The hierarchy of the points of interest.
     */
    public function hierarchy(array $pList, string $field = "tags"): array
    {
        $poiList = [];
        $poiParents = [];
        foreach ($pList as $key => $value) {
            //
            if (isset($value[$field]) && is_string($value[$field])) {
                $poiTree = explode(".", $value[$field]);
                //this is a child
                if (count($poiTree) > 1) {
                    $parentTag = $poiTree[0];
                    if (! isset($poiParents[$parentTag])) {
                        $poiParents[$parentTag] = [];
                    }

                    $poiParents[$parentTag][] = $value;
                }
                //this is a parent
                else {
                    $poiList[$value[$field]] = $value;
                }
            }
            //use case if strutags is an array
            elseif (isset($value[$field]) && is_array($value[$field]) && count($value[$field])) {
                foreach ($value[$field] as $tk => $tv) {
                    $poiTree = explode(".", $tv);
                    if (count($poiTree) > 1) {
                        $parentTag = $poiTree[0];
                        if (! isset($poiParents[$parentTag])) {
                            $poiParents[$parentTag] = [];
                        }

                        $poiParents[$parentTag][] = $value;
                    } else {
                        $poiList[$tv] = $value;
                    }
                }
            } else {
                $poiList[] = $value;
            }
        }
        return [
            "orphans" => $poiList,
            "parentTree" => $poiParents,
        ];
    }
}
