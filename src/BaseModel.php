<?php

namespace PixelHumain\Models;

/**
 * Class BaseModel
 *
 * This class serves as the base model for all citizen models.
 */
abstract class BaseModel
{
    /**
     * base constructor.
     *
     * @param array $config The configuration array.
     */
    public function __construct(array $config)
    {
        $this->configure($config);
    }

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    abstract protected function configure(array $config): void;

    /**
     * Validates the properties of the model.
     */
    abstract protected function validateProperties(): void;
}
