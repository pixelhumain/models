<?php

namespace PixelHumain\Models\Adapter\Interfaces;

interface InflectorHelperInterface
{
    /**
     * Converts a string into a slug.
     *
     * @param string $text The text to be slugified.
     * @return string The slugified string.
     */
    public static function slugify(string $text): string;
}
