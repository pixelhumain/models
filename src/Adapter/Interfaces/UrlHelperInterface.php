<?php

namespace PixelHumain\Models\Adapter\Interfaces;

interface UrlHelperInterface
{
    /**
     * Returns the base URL of the current request.
     * @param bool|string $scheme the URI scheme to use in the returned base URL:
     *
     * - `false` (default): returning the base URL without host info.
     * - `true`: returning an absolute base URL whose scheme is the same as that in [[\yii\web\UrlManager::$hostInfo]].
     * - string: returning an absolute base URL with the specified scheme (either `http`, `https` or empty string
     *   for protocol-relative URL).
     * @return string
     */
    public function base($scheme = false): string;

    /**
     * Returns the base URL.
     *
     * @return string The base URL.
     */
    public function getBaseUrl(): string;

    /**
     * Sets the base URL for the adapter.
     *
     * @param string $baseUrl The base URL to set.
     * @return void
     */
    public function setBaseUrl(string $baseUrl): void;

    /**
     * Creates a URL based on the given path.
     *
     * @param string $path The path to be used in the URL.
     * @return string The generated URL.
     */
    public function createUrl(string $path): string;
}
