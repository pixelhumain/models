<?php

namespace PixelHumain\Models\Adapter;

use PixelHumain\Models\Adapter\Interfaces\UrlHelperInterface;

class UrlHelperAdapter implements UrlHelperInterface
{
    /**
     * @var string The base URL.
     */
    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * Returns the base URL.
     *
     * @return string The base URL.
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * Sets the base URL for the adapter.
     *
     * @param string $baseUrl The base URL to set.
     * @return void
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }

    public function base($scheme = false): string
    {
        // TODO: Implement base() method static.
        // \yii\helpers\Url::class
        // \yii\helpers\Url::base($scheme);
        // get('url')::base($scheme);
    }

    /**
     * Creates a URL based on the given path.
     *
     * @param string $path The path to be used in the URL.
     * @return string The generated URL.
     */
    public function createUrl(string $path): string
    {
        /**
         * @psalm-suppress PossiblyNullArgument
         */
        return rtrim($this->baseUrl, '/') . '/' . ltrim($path, '/');
    }
}
