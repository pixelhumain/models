<?php

namespace PixelHumain\Models\Adapter;

use PixelHumain\Models\Adapter\Interfaces\InflectorHelperInterface;

class InflectorHelperAdapter implements InflectorHelperInterface
{
    /**
     * Converts a string into a slug.
     *
     * @param string $text The text to be slugified.
     * @return string The slugified string.
     */
    public static function slugify(string $text): string
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^\-\w]+~', '', $text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
