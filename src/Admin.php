<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\AdminInterface;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;

use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

class Admin extends BaseModel implements AdminInterface, ElementTraitInterface, CostumTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;
    use CostumTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Adds a source in an element.
     *
     * @param string|null $id The ID of the element.
     * @param string|null $type The type of the element.
     * @param string|null $keySource The key source.
     * @param string|null $setKey The set key.
     * @param string $origin The origin of the source (default: "costum").
     * @return array|null The updated element.
     */
    public function addSourceInElement(?string $id = null, ?string $type = null, ?string $keySource = null, ?string $setKey = null, string $origin = "costum"): ?array
    {
        $fields = [$setKey];
        $elt = $this->getModelElement()->getElementById($id, $type, null, $fields);

        if ($setKey == "source") {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "You can't add existed element as sourceKey but you can reference it"),
            ];
        }
        if (! empty($elt[$setKey]) && is_array($elt[$setKey])) {
            $set = $elt[$setKey];
            if (@$set[$origin]) {
                array_push($set[$origin], $keySource);
            } else {
                $set[$origin] = [$keySource];
            }
        } else {
            $set = [
                $origin => [$keySource],
            ];
        }
        $this->db->update(
            $type,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    $setKey => $set,
                ],
            ]
        );
        return [
            "result" => true,
            "msg" => $this->language->t("common", "The element is well refering"),
        ];
    }

    /**
     * Removes a source from an element.
     *
     * @param string|null $id The ID of the element.
     * @param string|null $type The type of the element.
     * @param string|null $keySource The key of the source to remove.
     * @param string|null $setKey The key of the set to remove the source from.
     * @param string $origin The origin of the removal (default: "costum").
     * @return array|null An array containing the updated element, or null if the element was not found.
     */
    public function removeSourceFromElement(?string $id = null, ?string $type = null, ?string $keySource = null, ?string $setKey = null, string $origin = "costum"): ?array
    {
        $fields = [$setKey];
        $elt = $this->getModelElement()->getElementById($id, $type, null, $fields);
        if (! empty($elt[$setKey])) {
            if ($setKey == "source") {
                if ((is_countable($elt[$setKey]["keys"]) ? count($elt[$setKey]["keys"]) : 0) == 1) {
                    $update = [
                        '$unset' => [
                            $setKey => 1,
                        ],
                    ];
                } else {
                    $getNewKey = false;
                    if ($elt[$setKey]["key"] == $keySource) {
                        $getNewKey = true;
                    }
                    foreach ($elt[$setKey]["keys"] as $k => $v) {
                        if ($v == $keySource) {
                            unset($elt[$setKey]["keys"][$k]);
                        } elseif ($getNewKey) {
                            $getNewKey = $v;
                        }
                    }
                    if (! empty($getNewKey)) {
                        $elt[$setKey]["key"] = $getNewKey;
                    }
                    $update = [
                        '$set' => [
                            $setKey => $elt[$setKey],
                        ],
                    ];
                }
                $msg = "Element is well removed from sources";
            } else {
                if ((is_countable($elt[$setKey][$origin]) ? count($elt[$setKey][$origin]) : 0) == 1) {
                    if ((is_countable($elt[$setKey]) ? count($elt[$setKey]) : 0) == 1) {
                        $update = [
                            '$unset' => [
                                $setKey => 1,
                            ],
                        ];
                    } else {
                        $update = [
                            '$unset' => [
                                $setKey . "." . $origin => 1,
                            ],
                        ];
                    }
                } else {
                    foreach ($elt[$setKey][$origin] as $k => $v) {
                        if ($v == $keySource) {
                            unset($elt[$setKey][$origin][$k]);
                        }
                    }
                    $update = [
                        '$set' => [
                            $setKey => $elt[$setKey],
                        ],
                    ];
                }
                if ($this->getModelCostum()->isSameFunction("removeSourceFromElement")) {
                    $params = [
                        "id" => $id,
                        "type" => $type,
                    ];
                    $this->getModelCostum()->sameFunction("removeSourceFromElement", $params);
                }
                $msg = "Element is well removed from reference";
            }
            $this->db->update(
                $type,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                $update
            );
            return [
                "result" => true,
                "msg" => $this->language->t("common", $msg),
            ];
        }
    }
}
