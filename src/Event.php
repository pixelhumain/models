<?php

namespace PixelHumain\Models;

use CTKException;
use DateTime;
use DateTimeZone;

use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\PersonInterface;

use PixelHumain\Models\Traits\BaseModel\DataHandlerTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\ImportTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ImportTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;

use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\SearchNewTrait;

// TODO : Yii::get('application')->controller->module->id
// Yii::get('module')->params['idOpenAgenda']

class Event extends BaseModel implements EventInterface, DocumentTraitInterface, ElementTraitInterface, SearchNewTraitInterface, DataValidatorTraitInterface, ImportTraitInterface, LinkTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;

    use DataHandlerTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DocumentTrait;
    use ElementTrait;
    use SearchNewTrait;
    use DataValidatorTrait;
    use ImportTrait;
    use LinkTrait;

    public array $types = [
        "competition" => "Competition",
        "concert" => "Concert",
        "contest" => "Contest",
        "exhibition" => "Exhibition",
        "festival" => "Festival",
        "getTogether" => "Get together",
        "market" => "Market",
        "meeting" => "Meeting",
        "course" => "Course",
        "workshop" => "Workshop",
        "conference" => "Conference",
        "debate" => "Debate",
        "film" => "Film",
        "stand" => "Stand",
        "crowdfunding" => "Crowdfunding",
        "internship" => "Internship",
        "spectacle" => "Spectacle",
        "others" => "Others",
        "protest" => "Protest",
        "fair" => "Fair",
    ];

    //From Post/Form name to database field name
    public static array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "slug" => [
            "name" => "slug",
            "rules" => ["checkSlug"],
        ],
        "email" => [
            "name" => "email",
            "rules" => ["email"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "type" => [
            "name" => "type",
        ],
        "public" => [
            "name" => "public",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "organizerId" => [
            "name" => "organizerId",
        ],
        "organizerType" => [
            "name" => "organizerType",
        ],
        "organizer" => [
            "name" => "organizer",
            "rules" => ["validOrganizer"],
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
            "rules" => ["geoValid"],
        ],
        "geoPosition" => [
            "name" => "geoPosition",
            "rules" => ["geoPositionValid"],
        ],
        "description" => [
            "name" => "description",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "allDay" => [
            "name" => "allDay",
            "rules" => ["boolean"],
        ],
        "modules" => [
            "name" => "modules",
        ],
        "recurrency" => [
            "name" => "recurency",
        ],
        "openingHours" => [
            "name" => "openingHours",
        ],
        "startDate" => [
            "name" => "startDate",
            "rules" => ["eventStartDate"],
        ],
        "endDate" => [
            "name" => "endDate",
            "rules" => ["eventEndDate"],
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "warnings" => [
            "name" => "warnings",
        ],
        "source" => [
            "name" => "source",
        ],
        "badges" => [
            "name" => "badges",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "url" => [
            "name" => "url",
        ],
        "contacts" => [
            "name" => "contacts",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "locality" => [
            "name" => "address",
        ],
        "descriptionHTML" => [
            "name" => "descriptionHTML",
        ],
        "facebook" => [
            "name" => "socialNetwork.facebook",
        ],
        "twitter" => [
            "name" => "socialNetwork.twitter",
        ],
        "gpplus" => [
            "name" => "socialNetwork.googleplus",
        ],
        "github" => [
            "name" => "socialNetwork.github",
        ],
        "gitlab" => [
            "name" => "socialNetwork.gitlab",
        ],
        "diaspora" => [
            "name" => "socialNetwork.diaspora",
        ],
        "mastodon" => [
            "name" => "socialNetwork.mastodon",
        ],
        "signal" => [
            "name" => "socialNetwork.signal",
        ],
        "telegram" => [
            "name" => "socialNetwork.telegram",
        ],
        "onepageEdition" => [
            "name" => "onepageEdition",
        ],
        "instagram" => [
            "name" => "socialNetwork.instagram",
        ],
        "timeZone" => [
            "name" => "timeZone",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Retrieves an event by its ID.
     *
     * @param string $id The ID of the event.
     * @return array The event data.
     */
    public function getById(string $id): array
    {
        $event = $this->getDataById($id);

        if ($event === null) {
            return $this->getModelElement()->getGhost(EventInterface::COLLECTION);
        }

        return $event;
    }

    /**
     * Get events by array of IDs.
     *
     * @param array $arrayId The array of event IDs.
     * @param array $fields The optional array of fields to retrieve.
     * @param bool $simply Whether to simplify the result or not.
     * @return array The array of events.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array
    {
        return $this->getDataByArrayId($arrayId, $fields, $simply);
    }

    /**
     * Format the enrich data.
     *
     * @param array $data The data to be enriched.
     * @return array The formatted enriched data.
     */
    protected function formatEnrichData(array $data): array
    {
        $data = $this->formatDates($data);
        return $data;
    }

    /**
     * Format the date for rendering.
     *
     * @param array $params The parameters for formatting the date.
     * @return void
     */
    public function formatDateRender(array $params): array
    {
        if (isset($params["startDate"]) && isset($params["endDate"])) {
            $this->formatDates($params);
        }
        return $params;
    }

    /**
     * Format the dates in the given project array.
     *
     * @param array $data The project array to format dates for.
     * @return array The formatted project array.
     */
    private function formatDates(array $data): array
    {
        date_default_timezone_set('UTC'); // Définir le fuseau horaire sur UTC

        $yester2day = mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"));
        $yesterday = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));

        if (! empty($data["startDate"]) && ! empty($data["endDate"])) {
            $data["startDate"] = $this->formatMongoDate($data["startDate"], $yester2day, DateTime::ISO8601);
            $data["endDate"] = $this->formatMongoDate($data["endDate"], $yesterday, DateTime::ISO8601);
        }
        return $data;
    }

    /**
     * Returns the fields of the project.
     *
     * @return array The fields of the project.
     */
    protected function getDataFields(): array
    {
        return [
            "id" => 1,
            "name" => 1,
            "creator" => 1,
            "shortDescription" => 1,
            "description" => 1,
            "address" => 1,
            "geo" => 1,
            "tags" => 1,
            "links" => 1,
            "profilImageUrl" => 1,
            "profilThumbImageUrl" => 1,
            "profilMarkerImageUrl" => 1,
            "profilMediumImageUrl" => 1,
            "recurrency" => 1,
            "openingHours" => 1,
            "startDate" => 1,
            "endDate" => 1,
            "addresses" => 1,
            "preferences" => 1,
            "collection" => 1,
            "type" => 1,
            "slug" => 1,
        ];
    }

    /**
     * Returns an array of link types.
     *
     * @return array The link types.
     */
    protected function getlinkTypes(): array
    {
        return ["attendees", "followers"];
    }

    /**
     * Retrieves a simple project by its ID.
     *
     * @param string $id The ID of the project.
     * @param array|null $event Optional event data.
     * @return array The simple project data.
     */
    public function getSimpleProjectById(string $id, ?array $event = null): array
    {
        return $this->getSimpleDataById($id, $event);
    }

    /**
     * Get the simple data for a specific event.
     *
     * @param int $id The ID of the event.
     * @param array $data The data array.
     * @return array The simple data array.
     */
    protected function getDataSimple($id, array $data): array
    {
        return [
            "id" => $id,
            "_id" => $data["_id"],
            "name" => $data["name"] ?? null,
            "recurrency" => $data["recurrency"] ?? null,
            "openingHours" => $data["openingHours"] ?? null,
            "geo" => $data["geo"] ?? null,
            "tags" => $data["tags"] ?? null,
            "preferences" => $data["preferences"] ?? null,
            "shortDescription" => $data["shortDescription"] ?? null,
            "description" => $data["description"] ?? null,
            "addresses" => $data["addresses"] ?? null,
            "creator" => $data["creator"] ?? null,
            "slug" => $data["slug"] ?? null,
            "address" => $data["address"] ?? [
                "addressLocality" => $this->language->t("common", "Unknown Locality"),
            ],
            "collection" => EventInterface::COLLECTION,
        ];
    }

    /**
     * Get the events that match the given parameters.
     *
     * @param array $params The parameters to filter the events.
     * @return array The array of events that match the given parameters.
     */
    public function getWhere(array $params): array
    {
        $events = $this->db->findAndSort(EventInterface::COLLECTION, $params, ["created"]);
        return $this->addInfoEvents($events);
    }

    /**
     * Retrieves events based on specified conditions, sorting, and limit.
     *
     * @param array $where An array of conditions to filter the events.
     * @param array $orderBy An array specifying the sorting order of the events.
     * @param int $indexStep The number of events to skip before returning results.
     * @param int $indexMin The minimum index of events to retrieve.
     * @return array An array of events matching the specified conditions.
     */
    public function getEventByWhereSortAndLimit(array $where, array $orderBy, int $indexStep, int $indexMin): array
    {
        $events = $this->db->findAndSortAndLimitAndIndex(EventInterface::COLLECTION, $this->getModelSearchNew()->prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
        return $this->addInfoEvents($events);
    }

    /**
     * Format the event data before saving.
     *
     * @param array $params The event parameters.
     * @return array The formatted event data.
     */
    public function formatBeforeSaving(array $params): array
    {
        if (@$params["startDate"] && ! is_a($params["startDate"], 'MongoDate')) {
            $startDate = $this->getModelDataValidator()->getDateTimeFromString($params['startDate'], "start date");
            $endDate = $this->getModelDataValidator()->getDateTimeFromString($params['endDate'], "end date");

            if (@$params["allDay"] && $params["allDay"]) {
                $startDate = date_time_set($startDate, 00, 00);
                $endDate = date_time_set($endDate, 23, 59);
            }

            $params["startDate"] = $this->db->MongoDate($startDate->getTimestamp());
            $params["endDate"] = $this->db->MongoDate($endDate->getTimestamp());
        }
        return [
            'result' => true,
            "params" => $params,
        ];
    }

    /**
     * Adds additional information to the events.
     *
     * @param array $events The array of events.
     * @return array The updated array of events.
     */
    public function addInfoEvents(array $events): array
    {
        foreach ($events as $key => $event) {
            // Mise à jour de l'événement avec les dates formatées
            $events[$key] = $this->formatDateRender($event);

            // Initialisation de l'organisateur
            $organizers = [];
            if (isset($event["links"]["organizer"]) && is_array($event["links"]["organizer"])) {
                foreach ($event["links"]["organizer"] as $organizerId => $val) {
                    if (isset($val["type"])) {
                        $organization = $this->getModelElement()->getInfos($val["type"], $organizerId);
                        if (isset($organization["name"])) {
                            $organizers[] = $organization["name"];
                        }
                    }
                }
            }
            $events[$key]["organizer"] = implode(', ', $organizers);
            // Fusion avec les images
            $events[$key] = $this->mergeWithImages($key, $events[$key]);
        }
        return $events;
    }

    /**
     * Retrieves the events from OpenAgenda.
     *
     * @param string $eventsIdOpenAgenda The ID of the events in OpenAgenda.
     * @return array The array of events from OpenAgenda.
     */
    public function getEventsOpenAgenda(string $eventsIdOpenAgenda): array
    {
        $where = [
            "source.id" => $eventsIdOpenAgenda,
        ];
        $event = $this->db->find(EventInterface::COLLECTION, $where);
        return $event;
    }

    /**
     * Returns the state of events from OpenAgenda.
     *
     * @param string $eventsIdOpenAgenda The ID of the events from OpenAgenda.
     * @param string $dateUpdate The date of the last update.
     * @param array $endDateOpenAgenda The array of end dates for the events.
     * @return string The state of the events.
     */
    public function getStateEventsOpenAgenda(string $eventsIdOpenAgenda, string $dateUpdate, array $endDateOpenAgenda): string
    {
        $state = "";
        $events = $this->getEventsOpenAgenda($eventsIdOpenAgenda);

        if (empty($events)) {
            return "Add"; // Aucun événement trouvé, donc état "Add"
        }

        foreach ($events as $event) {
            if (! empty($endDateOpenAgenda[0]["dates"])) {
                $lastUpdateTimestamp = ! empty($event["modified"]->sec) ? $event["modified"]->sec : $event["created"]->sec;

                if (strtotime($dateUpdate) > $lastUpdateTimestamp) {
                    return "Update"; // Date de mise à jour plus récente que la dernière modification de l'événement
                }
            }
            // Supprimez la ligne suivante si vous devez traiter tous les événements
            break;
        }

        return $state;
    }

    /**
     * Crée des événements à partir des données d'OpenAgenda.
     *
     * @param array $eventOpenAgenda Les données d'OpenAgenda pour créer les événements.
     * @return array Les événements créés.
     */
    public function createEventsFromOpenAgenda(array $eventOpenAgenda): array
    {
        $newEvents = [];
        $geo = [];
        $address = [];
        $newEvents["name"] = empty($eventOpenAgenda["title"]["fr"]) ? "" : $eventOpenAgenda["title"]["fr"];
        $newEvents["description"] = empty($eventOpenAgenda["description"]["fr"]) ? "" : $eventOpenAgenda["description"]["fr"];
        $newEvents["shortDescription"] = empty($eventOpenAgenda["freeText"]["fr"]) ? "" : $eventOpenAgenda["freeText"]["fr"];
        $newEvents["image"] = empty($eventOpenAgenda["image"]) ? "" : $eventOpenAgenda["image"];
        $newEvents["organizerId"] = $this->params->get('idOpenAgenda');
        $newEvents["organizerType"] = PersonInterface::COLLECTION;

        if (! empty($eventOpenAgenda["locations"][0]["dates"])) {
            $nbDates = is_countable($eventOpenAgenda["locations"][0]["dates"]) ? count($eventOpenAgenda["locations"][0]["dates"]) : 0;
            if (! empty($eventOpenAgenda["locations"][0]["dates"][0]["timeStart"]) && ! empty($eventOpenAgenda["locations"][0]["dates"][$nbDates - 1]["timeEnd"])) {
                $arrayTimeStart = explode(":", $eventOpenAgenda["locations"][0]["dates"][0]["timeStart"]);
                $arrayTimeEnd = explode(":", $eventOpenAgenda["locations"][0]["dates"][$nbDates - 1]["timeEnd"]);

                $arrayDateStart = explode("-", $eventOpenAgenda["locations"][0]["dates"][0]["date"]);
                $arrayDateEnd = explode("-", $eventOpenAgenda["locations"][0]["dates"][$nbDates - 1]["date"]);

                $start = mktime($arrayTimeStart[0], $arrayTimeStart[1], $arrayTimeStart[2], $arrayDateStart[1], $arrayDateStart[2], $arrayDateStart[0]);
                $end = mktime($arrayTimeEnd[0], $arrayTimeEnd[1], $arrayTimeEnd[2], $arrayDateEnd[1], $arrayDateEnd[2], $arrayDateEnd[0]);

                $newEvents["startDate"] = date('Y-m-d H:i:s', $start);
                $newEvents["endDate"] = date('Y-m-d H:i:s', $end);
            }

            $newEvents["dates"] = $eventOpenAgenda["locations"][0]["dates"];
        }

        $geo["latitude"] = (empty($eventOpenAgenda["locations"][0]["latitude"]) ? "" : $eventOpenAgenda["locations"][0]["latitude"]);
        $geo["longitude"] = (empty($eventOpenAgenda["locations"][0]["longitude"]) ? "" : $eventOpenAgenda["locations"][0]["longitude"]);
        $address['postalCode'] = (empty($eventOpenAgenda["locations"][0]["postalCode"]) ? "" : $eventOpenAgenda["locations"][0]["postalCode"]);
        $address['streetAddress'] = (empty($eventOpenAgenda["locations"][0]["address"]) ? "" : $eventOpenAgenda["locations"][0]["address"]);
        $address['addressLocality'] = (empty($eventOpenAgenda["locations"][0]["city"]) ? "" : $eventOpenAgenda["locations"][0]["city"]);

        $details = $this->getModelImport()->getAndCheckAddressForEntity($address, $geo);
        $newEvents['address'] = $details['address'];

        if (! empty($details['geo'])) {
            $newEvents['geo'] = $details['geo'];
        }

        if (! empty($details['geoPosition'])) {
            $newEvents['geoPosition'] = $details['geoPosition'];
        }

        $newEvents["tags"] = empty($eventOpenAgenda["tags"]["fr"]) ? "" : explode(",", $eventOpenAgenda["tags"]["fr"]);

        $newEvents["creator"] = $this->params->get('idOpenAgenda');
        $newEvents["type"] = "other";
        $newEvents["public"] = true;
        $newEvents['allDay'] = true;

        $newEvents['source']["id"] = $eventOpenAgenda["uid"];
        $newEvents['source']["url"] = $eventOpenAgenda["link"];
        $newEvents['source']["key"] = "openagenda";

        return $newEvents;
    }

    /**
     * Saves an event from OpenAgenda.
     *
     * @param array $params The parameters for the event.
     * @param string $moduleId The module ID.
     * @return array The saved event.
     */
    public function saveEventFromOpenAgenda(array $params, string $moduleId): array
    {
        $pathFolderImage = null;
        $newEvent = $this->getAndCheckEventOpenAgenda($params);

        if (! empty($newEvent["image"])) {
            $arrrayNameImage = explode("/", $newEvent["image"]);
            $nameImage = $arrrayNameImage[count($arrrayNameImage) - 1];
            $pathFolderImage = "https://cibul.s3.amazonaws.com/";
            unset($newEvent["image"]);
        }

        $newEvent = $this->db->insert(EventInterface::COLLECTION, $newEvent);
        if (isset($newEvent["_id"])) {
            $newEventId = (string) $newEvent["_id"];
        } else {
            throw new CTKException("Problem inserting the new event");
        }

        $creator = true;
        $isAdmin = true;
        $this->getModelLink()->attendee($newEvent["_id"], $this->params->get('idOpenAgenda'), $isAdmin, $creator);
        $this->getModelLink()->addOrganizer($params["organizerId"], $params["organizerType"], $newEvent["_id"], $this->params->get('idOpenAgenda'));

        $msgErrorImage = "";
        if (! empty($nameImage)) {
            try {
                $res = $this->getModelDocument()->uploadDocument($moduleId, EventInterface::COLLECTION, $newEventId, "avatar", false, $pathFolderImage, $nameImage);
                if (! empty($res["result"]) && $res["result"] == true) {
                    $params = [];
                    $params['id'] = $newEventId;
                    $params['type'] = EventInterface::COLLECTION;
                    $params['moduleId'] = $moduleId;
                    $params['folder'] = EventInterface::COLLECTION . "/" . $newEventId;
                    $params['name'] = $res['name'];
                    $params['author'] = $this->session->getUserId();
                    $params['size'] = $res["size"];
                    $params["contentKey"] = "profil";
                    $res2 = $this->getModelDocument()->save($params);
                    if ($res2["result"] == false) {
                        throw new CTKException("Impossible de d'enregistrer le fichier.");
                    }
                } else {
                    $msgErrorImage = "Impossible uploader le document.";
                }
            } catch (CTKException $e) {
                throw new CTKException($e);
            }
        }

        return [
            "result" => true,
            "msg" => $this->language->t("event", "Your event has been connected.") . " " . $msgErrorImage,
            "id" => $newEvent["_id"],
            "event" => $newEvent,
        ];
    }

    /**
     * Retrieves and checks the event from OpenAgenda.
     *
     * @param array $event The event details.
     * @return array The event details after checking.
     */
    public function getAndCheckEventOpenAgenda(array $event): array
    {
        $organization = [];
        $newEvent = [];

        if (empty($event['name'])) {
            throw new CTKException($this->language->t("import", "001", null, Yii::get('application')->controller->module->id));
        } else {
            $newEvent['name'] = $event['name'];
        }

        $newEvent['created'] = $this->db->MongoDate(time());

        if (! empty($event['email'])) {
            if (! preg_match('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#', $organization['email'])) {
                throw new CTKException($this->language->t("import", "205", null, Yii::get('application')->controller->module->id));
            }
            $newEvent["email"] = $event['email'];
        }

        if (empty($event['type'])) {
            throw new CTKException($this->language->t("import", "208", null, Yii::get('application')->controller->module->id));
        } else {
            $newEvent["type"] = $event['type'];
        }

        if (! empty($event['address'])) {
            if (empty($event['address']['postalCode'])) {
                throw new CTKException($this->language->t("import", "101", null, Yii::get('application')->controller->module->id));
            }
            if (empty($event['address']['codeInsee'])) {
                throw new CTKException($this->language->t("import", "102", null, Yii::get('application')->controller->module->id));
            }
            if (empty($event['address']['addressCountry'])) {
                throw new CTKException($this->language->t("import", "104", null, Yii::get('application')->controller->module->id));
            }
            if (empty($event['address']['addressLocality'])) {
                throw new CTKException($this->language->t("import", "105", null, Yii::get('application')->controller->module->id));
            }

            $newEvent['address'] = $event['address'];
        } else {
            throw new CTKException($this->language->t("import", "100", null, Yii::get('application')->controller->module->id));
        }

        if (! empty($event['geo']) && ! empty($event["geoPosition"])) {
            $newEvent["geo"] = $event['geo'];
            $newEvent["geoPosition"] = $event['geoPosition'];
        } elseif (! empty($event["geo"]['latitude']) && ! empty($event["geo"]["longitude"])) {
            $newEvent["geo"] = [
                "@type" => "GeoCoordinates",
                "latitude" => $event["geo"]['latitude'],
                "longitude" => $event["geo"]["longitude"],
            ];

            $newEvent["geoPosition"] = [
                "type" => "Point",
                "coordinates" => [floatval($event["geo"]['latitude']), floatval($event["geo"]['longitude'])],
            ];
        } else {
            throw new CTKException($this->language->t("import", "150", null, Yii::get('application')->controller->module->id));
        }

        if (isset($event['tags'])) {
            if (gettype($event['tags']) == "array") {
                $tags = $event['tags'];
            } elseif (gettype($event['tags']) == "string") {
                $tags = explode(",", $event['tags']);
            }
            $newEvent["tags"] = $tags;
        }

        if (! empty($event['description'])) {
            $newEvent["description"] = $event['description'];
        }

        if (! empty($event['shortDescription'])) {
            $newEvent["shortDescription"] = $event['shortDescription'];
        }

        if (! empty($event['creator'])) {
            $newEvent["creator"] = $event['creator'];
        }

        if (! empty($event['source'])) {
            $newEvent["source"] = $event['source'];
        }

        //url by ImportData
        if (! empty($event['url'])) {
            $newEvent["url"] = $event['url'];
        }

        if (! empty($event['allDay'])) {
            $newEvent["allDay"] = $event['allDay'];
        }

        if (! empty($event['startDate'])) {
            $m = $this->db->MongoDate(strtotime($event['startDate']));
            $newEvent['startDate'] = $m;
        }
        if (! empty($event['image'])) {
            $newEvent["image"] = $event['image'];
        }

        if (! empty($event['endDate'])) {
            $m = $this->db->MongoDate(strtotime($event['endDate']));
            $newEvent['endDate'] = $m;
        }

        return $newEvent;
    }

    /**
     * Returns the data binding for the event.
     *
     * @return array The data binding for the event.
     */
    public function getDataBinding(): array
    {
        return self::$dataBinding;
    }
}
