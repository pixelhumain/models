<?php

namespace PixelHumain\Models\Builders\Interfaces;

interface QueryBuilderInterface
{
    public function select(string $table, array $fields): QueryBuilderInterface;

    /**
     * Définit une clause WHERE dans la requête.
     *
     * @param string $field Le champ sur lequel appliquer la condition.
     * @param mixed $value La valeur à comparer.
     * @return QueryBuilderInterface L'instance de QueryBuilderInterface.
     */
    public function where(string $field, $value): QueryBuilderInterface;

    public function andWhere(array $conditions): QueryBuilderInterface;

    public function orWhere(array $conditions): QueryBuilderInterface;

    public function orderBy(string $field, string $direction = 'ASC'): QueryBuilderInterface;

    public function limit(int $limit): QueryBuilderInterface;

    public function offset(int $offset): QueryBuilderInterface;

    public function getQuery(): array;
}
