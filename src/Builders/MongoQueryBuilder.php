<?php

namespace PixelHumain\Models\Builders;

use PixelHumain\Models\Builders\Interfaces\QueryBuilderInterface;
use stdClass;

/**
 * The MongoQueryBuilder class is responsible for building MongoDB queries.
 */
class MongoQueryBuilder implements QueryBuilderInterface
{
    /**
     * @var stdClass The query object.
     */
    private stdClass $query;

    /**
     * Create a new MongoQueryBuilder instance.
     */
    public function __construct()
    {
        $this->query = new stdClass();
        $this->query->filter = [];
        $this->query->options = [];
    }

    /**
     * Reset the query object.
     */
    protected function reset(): void
    {
        $this->query = new stdClass();
        $this->query->filter = [];
        $this->query->options = [];
    }

    /**
     * Add a "where" condition to the query.
     *
     * @param string $field The field to filter on.
     * @param mixed $value The value to filter on.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function where(string $field, $value): QueryBuilderInterface
    {
        $this->query->filter[$field] = $value;
        return $this;
    }

    /**
     * Add an "and where" condition to the query.
     *
     * @param array $conditions The conditions to add.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function andWhere(array $conditions): QueryBuilderInterface
    {
        if (! isset($this->query->filter['$and'])) {
            $this->query->filter['$and'] = [];
        }
        foreach ($conditions as $condition) {
            $this->query->filter['$and'][] = $condition;
        }
        return $this;
    }

    /**
     * Add an "or where" condition to the query.
     *
     * @param array $conditions The conditions to add.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function orWhere(array $conditions): QueryBuilderInterface
    {
        if (! isset($this->query->filter['$or'])) {
            $this->query->filter['$or'] = [];
        }
        foreach ($conditions as $condition) {
            $this->query->filter['$or'][] = $condition;
        }
        return $this;
    }

    /**
     * Set the limit for the query.
     *
     * @param int $limit The limit value.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function limit(int $limit): QueryBuilderInterface
    {
        $this->query->options['limit'] = $limit;
        return $this;
    }

    /**
     * Set the skip for the query.
     *
     * @param int $skip The skip value.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function skip(int $skip): QueryBuilderInterface
    {
        $this->query->options['skip'] = $skip;
        return $this;
    }

    /**
     * Set the sort criteria for the query.
     *
     * @param array $sortCriteria The sort criteria.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function sort(array $sortCriteria): QueryBuilderInterface
    {
        $this->query->options['sort'] = $sortCriteria;
        return $this;
    }

    /**
     * Set the field and direction to order the query results by.
     *
     * @param string $field The field to order by.
     * @param string $direction The direction to order by (default: 'ASC').
     * @return QueryBuilderInterface The updated query builder instance.
     */
    public function orderBy(string $field, string $direction = 'ASC'): QueryBuilderInterface
    {
        $this->query->options['sort'][$field] = $direction === 'ASC' ? 1 : -1;
        return $this;
    }

    /**
     * Set the offset for the query.
     *
     * @param int $offset The offset value.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function offset(int $offset): QueryBuilderInterface
    {
        $this->query->options['offset'] = $offset;
        return $this;
    }

    /**
     * Set the fields to select for the query.
     *
     * @param string $table The table to select from.
     * @param array $fields The fields to select.
     * @return QueryBuilderInterface The current instance of the query builder.
     */
    public function select(string $table, array $fields): QueryBuilderInterface
    {
        $this->query->table = $table;
        $this->query->type = 'find';
        $this->reset();
        $this->query->options['projection'] = array_fill_keys($fields, 1);
        return $this;
    }

    /**
     * Returns the query as an array.
     *
     * @return array The query as an array.
     */
    public function getQuery(): array
    {
        return [
            'filter' => $this->query->filter,
            'options' => $this->query->options,
        ];
    }
}
