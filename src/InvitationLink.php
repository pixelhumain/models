<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\InvitationLinkInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

// TODO : Link::$linksTypes doit peut être dans l'interface LinkInterface ?

class InvitationLink extends BaseModel implements InvitationLinkInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Retrieves the links associated with a target.
     *
     * @param string $targetType The type of the target.
     * @param string $targetId   The ID of the target.
     * @return array An array of links associated with the target.
     */
    public function getLinks(string $targetType, string $targetId): array
    {
        $invitationLinks = $this->db->find(InvitationLinkInterface::COLLECTION, [
            "targetType" => $targetType,
            "targetId" => $targetId,
        ]);

        foreach ($invitationLinks as $id => $invitationLink) {
            $invitationLinks[$id]["joined"] = [];
        }

        $element = $this->getModelElement()->getElementById($targetId, $targetType);
        $elementLinks = [];

        if (isset($element["links"]) && isset(Link::$linksTypes[$targetType][PersonInterface::COLLECTION]) && isset($element["links"][Link::$linksTypes[$targetType][PersonInterface::COLLECTION]])) {
            $elementLinks = $element["links"][Link::$linksTypes[$targetType][PersonInterface::COLLECTION]];
        }

        foreach ($elementLinks as $personId => $elementLink) {
            if (isset($elementLink["inviteByLink"]) && $elementLink["inviteByLink"]) {
                $inviteLinkId = (string) $elementLink["inviteLinkId"];
                if (isset($invitationLinks[$inviteLinkId])) {
                    $invitationLinks[$inviteLinkId]["joined"][] = $personId;
                }
            }
        }

        return $invitationLinks;
    }

    /**
     * Deletes an invitation link.
     *
     * @param string $ref The reference of the invitation link to delete.
     * @return mixed The result of the deletion.
     */
    public function delete(string $ref)
    {
        return $this->db->remove(InvitationLinkInterface::COLLECTION, [
            "ref" => $ref,
        ]);
    }
}
