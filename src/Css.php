<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CssInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class Css extends BaseModel implements CssInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Creates or updates the CSS for a given context.
     *
     * @param string $contextId The ID of the context.
     * @param mixed $css The CSS content to be created or updated.
     * @return void
     */
    public function createORupdateCss(string $contextId, $css): void
    {
        $cssFile = $this->db->find(CssInterface::COLLECTION, [
            "contextId" => $contextId,
        ]);

        if (empty($cssFile)) {
            $params = [
                "contextId" => $contextId,
                "css" => $css,
                "collection" => "cssFile",
                "created" => time(),
                "updated" => time(),
            ];
            $params = $this->db->insert("cssFile", $params);
        } else {
            $this->db->update("cssFile", [
                "contextId" => $contextId,
            ], [
                '$set' => [
                    "css" => $css,
                    "updated" => time(),
                ],
            ]);
        }
    }

    /**
     * Retrieves CSS by custom ID.
     *
     * @param string $contextId The custom ID of the CSS.
     * @return array The CSS data.
     */
    public function getCssByCostumId(string $contextId): array
    {
        return $this->db->find(CssInterface::COLLECTION, [
            "contextId" => $contextId,
        ]);
    }
}
