<?php

namespace PixelHumain\Models;

use DateTime;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\AnswerInterface;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\BookmarkInterface;
use PixelHumain\Models\Interfaces\CircuitInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\ClassifiedInterface;
use PixelHumain\Models\Interfaces\CostumInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\LinkInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PoiInterface;
use PixelHumain\Models\Interfaces\ProductInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Interfaces\SearchInterface;
use PixelHumain\Models\Interfaces\ServiceInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;
use PixelHumain\Models\Interfaces\ZoneInterface;
use PixelHumain\Models\Traits\AdresseDataGouvTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\AdresseDataGouvTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTranslatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NominatimTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TranslateTraitInterface;
use PixelHumain\Models\Traits\NewsTranslatorTrait;
use PixelHumain\Models\Traits\NominatimTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\SlugTrait;
use PixelHumain\Models\Traits\TranslateTrait;

// TODO : Yii::get('stringHelper')::wd_remove_accents

class Search extends BaseModel implements SearchInterface, CostumTraitInterface, ElementTraitInterface, TranslateTraitInterface, SlugTraitInterface, CityTraitInterface, NewsTranslatorTraitInterface, PersonTraitInterface, SIGTraitInterface, AdresseDataGouvTraitInterface, NominatimTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use ElementTrait;
    use TranslateTrait;
    use SlugTrait;
    use CityTrait;
    use NewsTranslatorTrait;
    use PersonTrait;
    use SIGTrait;
    use AdresseDataGouvTrait;
    use NominatimTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
    }

    /**
     * Find records in a collection based on given criteria.
     *
     * @param string $collection The name of the collection to search in.
     * @param array $criterias An array of criteria to filter the records.
     * @param string $sortOnField The field to sort the records on. (optional)
     * @param int $nbResultMax The maximum number of results to return. (optional)
     * @return array An array of matching records.
     */
    public function findByCriterias(string $collection, array $criterias, string $sortOnField = "", int $nbResultMax = 10): array
    {
        $seprator = '$or';
        $query = [];

        //Add the criterias
        foreach ($criterias as $field => $value) {
            $aCriteria = [];
            $aCriteria[$field] = $this->db->MongoRegex("/$value/i");
            array_push($query, $aCriteria);
        }

        if (count($criterias) > 1) {
            $where = [
                $seprator => $query,
            ];
        } else {
            $where = $query;
        }

        $res = $this->db->findAndSort($collection, $where, [
            $sortOnField => 1,
        ], $nbResultMax);

        return $res;
    }

    /**
     * Converts accented characters in a string to their corresponding regular expression patterns.
     *
     * @param string $text The text to convert.
     * @return string The converted text with accented characters replaced by regular expression patterns.
     */
    public function accentToRegex(string $text): string
    {
        $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ---\''));
        $to = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy    '));
        $text = utf8_decode($text);
        $regex = [];

        foreach ($to as $key => $value) {
            if (isset($regex[$value])) {
                $regex[$value] .= $from[$key];
            } else {
                $regex[$value] = $value;
            }
        }

        foreach ($regex as $rg_key => $rg) {
            $text = preg_replace("/[$rg]/", "_{$rg_key}_", $text);
        }

        foreach ($regex as $rg_key => $rg) {
            $text = preg_replace("/_{$rg_key}_/", "[$rg]", $text);
        }
        return utf8_encode($text);
    }

    /**
     * Converts accented characters in a string to their corresponding regular expression pattern.
     *
     * @param string $text The input text.
     * @return string The converted text with accented characters replaced by their regex pattern.
     */
    public function accentToRegexSimply(string $text): string
    {
        $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'));
        $to = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'));
        $text = utf8_decode($text);

        foreach ($from as $key => $value) {
            $text = str_replace($value, $to[$key], $text);
        }

        return utf8_encode($text);
    }

    // TODO : $filter not used
    // TODO : pas de retour de la fonction
    // TODO : est ce que cette fonction est utilisée ?

    /**
     * Auto-completes the network based on the given parameters.
     *
     * @param array $post The input parameters for the auto-complete.
     * @param mixed $filter The optional filter to apply to the auto-complete results.
     * @return void
     */
    public function networkAutoComplete(array $post, $filter = null): void
    {
        $search = isset($post['name']) ? trim(urldecode($post['name'])) : null;
        $locality = isset($post['locality']) ? trim(urldecode($post['locality'])) : null;
        $searchType = $post['searchType'] ?? null;
        $searchTags = $post['searchTag'] ?? null;
        $searchPrefTag = $post['searchPrefTag'] ?? null;
        $searchBy = $post['searchBy'] ?? "INSEE";
        $indexMin = $post['indexMin'] ?? 0;
        $indexMax = $post['indexMax'] ?? 100;
        $country = $post['country'] ?? "";
        $sourceKey = $post['sourceKey'] ?? null;
        $mainTag = $post['mainTag'] ?? null;
        $paramsFiltre = $post['paramsFiltre'] ?? null;
        $countType = $post['countType'] ?? null;
        $indexStep = $indexMax - $indexMin;

        $searchTypeOrga = ""; /* used in CO2 to find different organisation type */

        if (sizeOf($searchType) == 1 &&
            @$searchType[0] == OrganizationInterface::TYPE_NGO ||
            @$searchType[0] == OrganizationInterface::TYPE_BUSINESS ||
            @$searchType[0] == OrganizationInterface::TYPE_GROUP ||
            @$searchType[0] == OrganizationInterface::TYPE_GOV ||
            @$searchType[0] == OrganizationInterface::TYPE_COOP) {
            $searchTypeOrga = $searchType[0];
            $searchType = [OrganizationInterface::COLLECTION];
        }

        $query = [];
        $query = $this->searchString($search, $query);

        $verbTag = ((! empty($paramsFiltre) && '$all' == $paramsFiltre) ? '$all' : '$in');
        $queryTags = $this->searchTags($searchTags, $verbTag);

        if (! empty($queryTags)) {
            $query = [
                '$and' => [$query, $queryTags],
            ];
        }

        $query = [
            '$and' => [
                $query, [
                    "state" => [
                        '$ne' => "uncomplete",
                    ],
                ]],
        ];

        if (! empty($mainTag)) {
            $verbMainTag = ((! empty($searchPrefTag) && '$or' == $searchPrefTag) ? '$or' : '$and');
            $queryTags = $this->searchTags($mainTag, $verbMainTag);
            if (! empty($queryTags)) {
                $query = [
                    '$and' => [$query, $queryTags],
                ];
            }
        }

        $query = $this->searchSourceKey($sourceKey, $query);

        $allRes = [];
    }

    // TODO : $api not used
    /**
     * Performs a global autocomplete search.
     *
     * @param array $post The search parameters.
     * @param string|null $filter The filter to apply to the search.
     * @param bool $api Determines if the search is performed via API.
     * @return array The search results.
     */
    public function globalAutoComplete(array $post, string $filter = null, $api = false): array
    {
        $results = [];
        $costum = $this->getModelCostum()->getCostum();
        $search = (@$post['name']) ? trim(urldecode($post['name'])) : "";

        $searchLocality = $post['locality'] ?? null;
        $searchType = $post['searchType'] ?? null;
        $searchTags = $post['searchTags'] ?? null;
        $country = $post['country'] ?? "";
        $priceMin = $_POST['priceMin'] ?? null;
        $priceMax = $_POST['priceMax'] ?? null;
        $devise = $_POST['devise'] ?? null;
        $latest = $_POST['latest'] ?? null;
        $startDate = $_POST['startDate'] ?? null;
        $startDateUTC = $_POST['startDateUTC'] ?? null;
        $endDate = $_POST['endDate'] ?? null;
        $type = ! empty($post['type']) ? $post['type'] : "";
        $sourceType = ! empty($post['sourceType']) ? $post['sourceType'] : null;
        $subType = ! empty($post['subType']) ? $post['subType'] : "";
        $category = ! empty($post['category']) ? $post['category'] : "";
        $section = ! empty($post['section']) ? $post['section'] : "";
        $sourceKey = ! empty($post['sourceKey']) ? $post['sourceKey'] : "";
        $countResult = (@$post["count"]) ? true : false;
        $initType = $_POST['initType'] ?? null;
        $searchOnAll = (@$post['initType'] && $post['initType'] == "all") ? true : false;
        $ranges = @$post['ranges'] ? $post['ranges'] : null;
        $countType = $post['countType'] ?? null;
        $indexMin = $post['indexMin'] ?? 0;
        $indexStep = $post['indexStep'] ?? 30;
        $lastTimes = (! empty($post["lastTimes"])) ? $post["lastTimes"] : false;
        $onlyCount = isset($post['onlyCount']) ? true : false;
        $searchGeo = (@$post["geoSearch"]) ? $post["geoSearch"] : null;
        $filterBy = (! empty($post["filterBy"])) ? $post["filterBy"] : null;
        $subParams = (! empty($post["subParams"])) ? $post["subParams"] : null;
        $private = (! empty($post["private"])) ? $post["private"] : null;
        $links = (! empty($post["links"])) ? $post["links"] : null;
        $filters = (! empty($post["filters"])) ? $post["filters"] : null;
        $community = ! empty($post['community']) ? $post['community'] : null;
        $options = (! empty($post['options'])) ? $post['options'] : null;
        if (isset($post["sortBy"]) && ! empty($post["sortBy"])) {
            $sortBy = [
                $post["sortBy"][0] => 1,
            ];
        } else {
            $sortBy = [
                "updated" => -1,
            ];
        }

        if (isset($post["sort"]) && ! empty($post["sort"])) {
            $sortBy = $post["sort"];
            foreach ($sortBy as $key => $value) {
                $sortBy[$key] = (int) $value;
            }
        }

        $searchTypeOrga = []; /* used in CO2 to find different organisation type */
        $addTypeOrga = false;
        if (! empty($searchType)) {
            if (is_string($searchType)) {
                $searchType = [$searchType];
            }
            foreach ($searchType as $v) {
                if (in_array($v, [OrganizationInterface::TYPE_NGO, OrganizationInterface::TYPE_BUSINESS, OrganizationInterface::TYPE_GROUP, OrganizationInterface::TYPE_GOV, OrganizationInterface::TYPE_COOP])) {
                    array_push($searchTypeOrga, $v);
                    $addTypeOrga = true;
                }
            }
            // var_dump($searchType);
        }
        if ($addTypeOrga) {
            array_push($searchType, OrganizationInterface::COLLECTION);
        }
        //*********************************  DEFINE GLOBAL QUERY   ******************************************
        $arrayIds = [];
        if ($this->session->getUserId()) {
            $user = $this->getModelElement()->getElementSimpleById($this->session->getUserId(), PersonInterface::COLLECTION, null, ["links"]);
            $arrayIds = [$this->session->getUserId()];
            if (@$user["links"]["memberOf"] && ! empty($user["links"]["memberOf"])) {
                foreach ($user["links"]["memberOf"] as $key => $data) {
                    if (! @$data[LinkInterface::TO_BE_VALIDATED]) {
                        array_push($arrayIds, $key);
                    }
                }
            }
            if (@$user["links"]["projects"] && ! empty($user["links"]["projects"])) {
                foreach ($user["links"]["projects"] as $key => $data) {
                    if (! @$data[LinkInterface::TO_BE_VALIDATED]) {
                        array_push($arrayIds, $key);
                    }
                }
            }
            if (@$user["links"]["events"] && ! empty($user["links"]["events"])) {
                foreach ($user["links"]["events"] as $key => $data) {
                    if (! @$data[LinkInterface::TO_BE_VALIDATED]) {
                        array_push($arrayIds, $key);
                    }
                }
            }
        }
        $query = [];
        $queryNews = [];
        $query = $this->searchString($search, $query);

        if (! empty($private) && ($private === true || $private == "true")) {
            $query = [
                '$and' =>
                                          [
                                              $query, [
                                                  "state" => [
                                                      '$nin' => ["uncomplete", "deleted"],
                                                  ],
                                              ], [
                                                  "status" => [
                                                      '$nin' => ["uncomplete", "deleted", "deletePending"],
                                                  ],
                                              ]],
            ];
        } else {
            $query = [
                '$and' =>
                                          [
                                              $query, [
                                                  "state" => [
                                                      '$nin' => ["uncomplete", "deleted"],
                                                  ],
                                                  "status" => [
                                                      '$nin' => ["uncomplete", "deleted", "deletePending"],
                                                  ],
                                                  '$or' => [[
                                                      'preferences.private' => [
                                                          '$exists' => false,
                                                      ],
                                                  ], [
                                                      'preferences.private' => false,
                                                  ]],
                                              ]],
            ];
        }

        if (! empty($links)) {
            $or = [];
            foreach ($links as $v) {
                array_push($or, [
                    "links." . $v["type"] . "." . $v["id"] => [
                        '$exists' => true,
                    ],
                ]);
            }

            if (! empty($or)) {
                $query = [
                    '$and' =>
                                                  [
                                                      $query, [
                                                          '$or' => $or,
                                                      ]],
                ];
            }
        }

        $query = $this->searchSourceKey($sourceKey, $query);
        if (! empty($costum["slug"])) {
            $query = [
                '$and' =>
                                            [
                                                $query, [
                                                    "source.toBeValidated." . $costum["slug"] => [
                                                        '$exists' => false,
                                                    ],
                                                ]],
            ];
        }

        if ($latest) {
            $query = [
                '$and' => [
                    $query, [
                        "updated" => [
                            '$exists' => 1,
                        ],
                    ]],
            ];
        }

        if (! empty($lastTimes)) {
            $query = [
                '$and' => [
                    $query, [
                        "updated" => [
                            '$gt' => $lastTimes,
                        ],
                    ]],
            ];
        }

        //*********************************  TAGS   ******************************************

        if ((is_countable($searchTags) ? count($searchTags) : 0) > 1 || (is_countable($searchTags) ? count($searchTags) : 0) == 1 && $searchTags[0] != "") {
            $operator = (! empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
            if (strcmp($filter, ClassifiedInterface::COLLECTION) != 0 && in_array(ClassifiedInterface::COLLECTION, $searchType)) {
                $queryTags = $this->searchTags($searchTags, $operator);
            } else {
                $queryTags = $this->searchTags($searchTags, $operator);
            }

            if (! empty($queryTags)) {
                $query = [
                    '$and' => [$query, $queryTags],
                ];
            }
        }
        if (isset($type) && $type != "") {
            array_push($query['$and'], [
                "type" => $type,
            ]);
        }

        if (! empty($filters)) {
            $query = $this->searchFilters($filters, $query);
        }

        $queryPersons = $query;
        //*********************************  DEFINE LOCALITY QUERY   ****************************************
        if (! empty($searchLocality) || ! empty($searchGeo)) {
            $query = $this->searchLocality($searchLocality, $query, $searchGeo);
            $queryPersons = $query;
            array_push($queryPersons['$and'], [
                "preferences.publicFields" => [
                    '$in' => ["locality"],
                ],
            ]);
        }

        if ($community != false) {
            $queryCommunity = $this->searchCommunity($query, $community);
        }

        if (! empty($queryCommunity)) {
            $query = [
                '$or' => [$query, $queryCommunity],
            ];
        }

        $queryEvents = $query;
        $queryOrga = $this->getQueryOrganizations($query, $category);
        $queryClassifieds = $this->getQueryClassifieds($query, $section, $category, $subType, @$priceMin, @$priceMax, @$devise);
        $queryBookmark = $this->getQueryBookmark($query, $category);
        $queryPoi = $this->getQueryPoi($query, $category);
        $queryProposals = $query;

        $allRes = [];
        if (! $onlyCount) {
            //*********************************  CITIES   ******************************************
            if (! empty($search) /*&& !empty($locality) */) {
                if (strcmp($filter, CityInterface::COLLECTION) != 0 && in_array(CityInterface::COLLECTION, $searchType)) {
                    $allCitiesRes = $this->getModelCity()->searchCity($country, $search, false, false, (! empty($subParams[CityInterface::COLLECTION]) ? $subParams[CityInterface::COLLECTION] : null));
                }

                if (count($allRes) < $indexStep) {
                    if (isset($allCitiesRes)) {
                        $allRes = array_merge($allRes, $allCitiesRes);
                    }
                }
            }
            //*********************************  Badge   ******************************************
            if (strcmp($filter, BadgeInterface::COLLECTION) != 0 && in_array(BadgeInterface::COLLECTION, $searchType)) {
                $prefLocality = (! empty($searchLocality) ? true : false);
                if (@$ranges) {
                    $indexMin = $ranges[PersonInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[PersonInterface::COLLECTION]["indexMax"] - $ranges[PersonInterface::COLLECTION]["indexMin"];
                }
                $allRes = array_merge($allRes, $this->searchBadges($queryPersons, $indexStep, $indexMin));
            }

            //*********************************  PERSONS   ******************************************
            if (strcmp($filter, PersonInterface::COLLECTION) != 0 && in_array(PersonInterface::COLLECTION, $searchType)) {
                $prefLocality = (! empty($searchLocality) ? true : false);
                if (@$ranges) {
                    $indexMin = $ranges[PersonInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[PersonInterface::COLLECTION]["indexMax"] - $ranges[PersonInterface::COLLECTION]["indexMin"];
                }
                $allRes = array_merge($allRes, $this->searchPersons($queryPersons, $indexStep, $indexMin, $prefLocality, $community));
            }

            //*********************************  ORGANISATIONS   ******************************************
            if (strcmp($filter, OrganizationInterface::COLLECTION) != 0 && in_array(OrganizationInterface::COLLECTION, $searchType)) {
                if (! empty($searchTypeOrga)) {
                    foreach ($searchTypeOrga as $v) {
                        if (@$ranges) {
                            $indexMin = $ranges[$v]["indexMin"];
                            $indexStep = $ranges[$v]["indexMax"] - $ranges[$v]["indexMin"];
                        }

                        $allRes = array_merge($allRes, $this->searchOrganizations($queryOrga, $indexStep, $indexMin, $v, $sortBy));
                    }
                } else {
                    $allRes = array_merge($allRes, $this->searchOrganizations($queryOrga, $indexStep, $indexMin, null, $sortBy));
                }
            }

            //*********************************  EVENT   ******************************************
            if (strcmp($filter, EventInterface::COLLECTION) != 0 && in_array(EventInterface::COLLECTION, $searchType)) {
                $searchAll = false;
                if (@$ranges) {
                    $indexMin = $ranges[EventInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[EventInterface::COLLECTION]["indexMax"] - $ranges[EventInterface::COLLECTION]["indexMin"];
                }

                if ($search != "") {
                    $searchAll = true;
                }
                $allEvents = $this->searchEvents($queryEvents, $indexStep, $indexMin, $searchOnAll, $searchAll, $sortBy, $startDate, $startDateUTC);

                $allRes = array_merge($allRes, $allEvents);
            }
            //*********************************  PROJECTS   ******************************************
            if (strcmp($filter, ProjectInterface::COLLECTION) != 0 && in_array(ProjectInterface::COLLECTION, $searchType)) {
                if (@$ranges) {
                    $indexMin = $ranges[ProjectInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[ProjectInterface::COLLECTION]["indexMax"] - $ranges[ProjectInterface::COLLECTION]["indexMin"];
                }
                if (@$category && ! empty($category)) {
                    $query = [
                        '$and' =>
                                                    [
                                                        $query, [
                                                            "category" => $category,
                                                        ]],
                    ];
                }
                $allRes = array_merge($allRes, $this->searchProject($query, $indexStep, $indexMin, $sortBy));
            }
            //*********************************  CLASSIFIED   ******************************************
            if (strcmp($filter, ClassifiedInterface::COLLECTION) != 0 && in_array(ClassifiedInterface::COLLECTION, $searchType)) {
                //var_dump($query) ; exit;
                if (@$ranges) {
                    $indexMin = $ranges[ClassifiedInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[ClassifiedInterface::COLLECTION]["indexMax"] - $ranges[ClassifiedInterface::COLLECTION]["indexMin"];
                }
                if (! empty($searchTags) && in_array("favorites", $searchTags)) {
                    $allRes = array_merge($allRes, $this->searchFavorites(ClassifiedInterface::COLLECTION));
                } else {
                    $allRes = array_merge($allRes, $this->searchClassified($queryClassifieds, $indexStep, $indexMin, @$priceMin, @$priceMax, @$devise));
                }
            }
            //*********************************  POI   ******************************************
            if (strcmp($filter, PoiInterface::COLLECTION) != 0 && in_array(PoiInterface::COLLECTION, $searchType)) {
                //Rest::json($queryPoi); exit;
                if (@$ranges) {
                    $indexMin = $ranges[PoiInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[PoiInterface::COLLECTION]["indexMax"] - $ranges[PoiInterface::COLLECTION]["indexMin"];
                }
                $allRes = array_merge($allRes, $this->searchPoi($queryPoi, $indexStep, $indexMin, $sortBy));
            }

            //*********************************  POI   ******************************************
            if (strcmp($filter, AnswerInterface::COLLECTION) != 0 && in_array(AnswerInterface::COLLECTION, $searchType)) {
                //Rest::json($queryPoi); exit;
                if (@$ranges) {
                    $indexMin = $ranges[AnswerInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[AnswerInterface::COLLECTION]["indexMax"] - $ranges[AnswerInterface::COLLECTION]["indexMin"];
                }
                $allRes = array_merge($allRes, $this->searchAnswers($query, $indexStep, $indexMin));
            }

            //*********************************  PRODUCT  ******************************************
            /* if(strcmp($filter, Product::COLLECTION) != 0 && in_array(Product::COLLECTION, $searchType)){
                 $allRes = array_merge($allRes, $this->searchProduct($query, $indexStep, $indexMin));
               }
               //*********************************  SERVICE  ******************************************
             if(strcmp($filter, Service::COLLECTION) != 0 && in_array(Service::COLLECTION, $searchType)){
                 $allRes = array_merge($allRes, $this->searchService($query, $indexStep, $indexMin));
               }
               //*********************************  SERVICE  ******************************************
             if(strcmp($filter, Circuit::COLLECTION) != 0 && in_array(Circuit::COLLECTION, $searchType)){
                 $allRes = array_merge($allRes, $this->searchCircuit($query, $indexStep, $indexMin));
               }*/

            //*********************************  NEWS   ******************************************
            if (strcmp($filter, NewsInterface::COLLECTION) != 0 && in_array(NewsInterface::COLLECTION, $searchType)) {
                if (@$ranges) {
                    $indexMin = $ranges[NewsInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[NewsInterface::COLLECTION]["indexMax"] - $ranges[NewsInterface::COLLECTION]["indexMin"];
                }
                $allRes = array_merge($allRes, $this->searchNews($queryNews, $indexStep, $indexMin));
            }

            // array_push( $queryProposals[ '$and' ], array( "parentType" => "citoyens" ) );
            $queryProposals = [
                '$and' => [
                    $queryProposals, [
                        "parentType" => "citoyens",
                    ]],
            ];
            //*********************************  Proposals   ******************************************
            if (strcmp($filter, ProposalInterface::COLLECTION) != 0 && in_array(ProposalInterface::COLLECTION, $searchType)) {
                $allRes = array_merge($allRes, $this->searchProposals($queryProposals, $indexStep, $indexMin, $searchLocality));
            }

            //*********************************  DDA   ******************************************
            /*if(strcmp($filter, ActionRoom::COLLECTION) != 0 && $this->typeWanted(ActionRoom::COLLECTION, $type)){
                $allRes = array_merge($allRes, $this->searchDDA($query, $indexMax));
              }
              if(strcmp($filter, ActionRoom::COLLECTION) != 0 && $this->typeWanted(ActionRoom::COLLECTION, $type)){
                $allRes = array_merge($allRes, $this->searchDDA($query, $indexMax));
              }*/

            //***************************** COSTUM  *****************************************/
            //$queryBookmark = $query;
            // Rest::json($allRes); exit;
            if (strcmp($filter, CostumInterface::COLLECTION) != 0 && in_array(CostumInterface::COLLECTION, $searchType)) {
                if (@$ranges) {
                    // Rest::json($query);exit;
                    $indexMin = $ranges[CostumInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[CostumInterface::COLLECTION]["indexMax"] - $ranges[CostumInterface::COLLECTION]["indexMin"];
                }
                //$resultat = $this->searchCostum($queryCostum, $indexStep, $indexMin, $type, null);
                $resultat = $this->searchCostum($query, $indexStep, $indexMin, $type);
                //Rest::json($allRes);exit;
                $allRes = array_merge($allRes, $resultat);
                // Rest::json($allRes);exit;
            }

            //***************************** BOOKMARK  *****************************************/
            //$queryBookmark = $query;
            // Rest::json($allRes); exit;
            if (strcmp($filter, BookmarkInterface::COLLECTION) != 0 && in_array(BookmarkInterface::COLLECTION, $searchType)) {
                if (@$ranges) {
                    // Rest::json($query);exit;
                    $indexMin = $ranges[BookmarkInterface::COLLECTION]["indexMin"];
                    $indexStep = $ranges[BookmarkInterface::COLLECTION]["indexMax"] - $ranges[BookmarkInterface::COLLECTION]["indexMin"];
                }
                $resultat = $this->searchBookmark($queryBookmark, $indexStep, $indexMin, $type, null);
                //Rest::json($allRes);exit;
                $allRes = array_merge($allRes, $resultat);
                //Rest::json($allRes);exit;
            }

            //*********************************  VOTES / propositions   ******************************************
            //error_log(print_r($searchType));
            //error_log("filter : ".$filter);
            if ($this->session->getUserId() &&
                    in_array(ActionRoomInterface::TYPE_VOTE, $searchType) ||
                    in_array(ActionRoomInterface::TYPE_ACTIONS, $searchType)
            ) {
                $allRes = array_merge($allRes, $this->searchVotes($query, $indexStep, $indexMin, $type));
            }

            if (@$post['tpl'] == "/pod/nowList") {
                // TODO : verfier les usort ailleurs
                usort($allRes, 'self::mySortByUpdated');
            }

            foreach ($allRes as $key => $value) {
                if ($searchOnAll) {
                    if (! empty($value["type"]) && $value["type"] == NewsInterface::COLLECTION) {
                        if (@$value["updated"]) {
                            $allRes[$key]["sorting"] = @$value["updated"]->sec;
                        } elseif (@$value["created"]) {
                            $allRes[$key]["sorting"] = @$value["created"]->sec;
                        }
                    } else {
                        if (is_object(@$value["updated"])) {
                            $allRes[$key]["sorting"] = @$value["updated"]->sec;
                        } else {
                            $allRes[$key]["sorting"] = @$value["updated"];
                        }
                    }
                }
                if (@$value["updated"]) {
                    if ($initType == "agenda") {
                        $allRes[$key]["updatedLbl"] = $this->getModelTranslate()->pastTime(@$value["startDate"], "date");
                    } elseif (! empty($value["type"]) && $value["type"] == NewsInterface::COLLECTION) {
                        if (@$value["updated"]) {
                            $allRes[$key]["updatedLbl"] = $this->getModelTranslate()->pastTime(@$value["updated"]->sec, "timestamp");
                        } elseif (@$value["created"]) {
                            $allRes[$key]["updatedLbl"] = $this->getModelTranslate()->pastTime(@$value["created"]->sec, "timestamp");
                        }
                    } else {
                        $allRes[$key]["updatedLbl"] = $this->getModelTranslate()->pastTime(@$value["updated"], "timestamp");
                    }
                }

                if (! empty($filterBy)) {
                    if ($allRes[$key][$filterBy]) {
                        $newElt = $allRes[$key];

                        foreach ($allRes[$key][$filterBy] as $keyF => $valueF) {
                            unset($newElt[$filterBy]);
                            $newElt[$filterBy] = $valueF;
                            $newElt["name"] = $allRes[$key]["name"] . " : " . $valueF["label"];
                            $allRes[$key . "." . $keyF] = $newElt;
                        }
                        unset($allRes[$key]);
                    }
                }
            }
        }

        $results["results"] = $allRes;

        if ($countResult && ! empty($countType)) {
            if (! empty($startDateUTC)) {
                $date1 = new DateTime($startDateUTC);
                $date2 = new DateTime($startDateUTC);
                $startD = $date1->getTimestamp();
                $queryDate = [
                    '$or' => [[
                        "openingHours.dayOfWeek" => [
                            '$exists' => 1,
                        ],
                    ], [
                        "startDate" => [
                            '$gte' => $this->db->MongoDate((float) $startD),
                        ],
                    ], [
                        "startDate" => [
                            '$lte' => $this->db->MongoDate((float) $startD),
                        ],
                        "endDate" => [
                            '$gte' => $this->db->MongoDate((float) $startD),
                        ],
                    ]],
                ];
                $queryEvents = [
                    '$and' => [$queryEvents, $queryDate],
                ];
            }

            $results["count"] = $this->countResultsByCollection($countType, $query, $queryPersons, $queryNews, $queryEvents, $queryClassifieds, $queryProposals, $queryPoi);

            if (! empty($searchType) && is_array($searchType) &&
                        in_array(OrganizationInterface::COLLECTION, $searchType) &&

                ! in_array("NGO", $searchType) &&
                            ! in_array("LocalBusiness", $searchType) &&
                            ! in_array("GovernmentOrganization", $searchType) &&
                            ! in_array("Group", $searchType) &&
                            ! in_array("Cooperative", $searchType) &&
                            (
                                ! empty($results["count"]["NGO"]) ||
                                ! empty($results["count"]["LocalBusiness"]) ||
                                ! empty($results["count"]["GovernmentOrganization"]) ||
                                ! empty($results["count"]["Group"]) ||
                                ! empty($results["count"]["Cooperative"])
                            )

            ) {
                $results["count"][OrganizationInterface::COLLECTION] = 0;
                foreach (["Group", "NGO", "LocalBusiness", "GovernmentOrganization", "Cooperative"] as $value) {
                    if (! empty($results["count"][$value]) && ! empty($results["count"])) {
                        $results["count"][OrganizationInterface::COLLECTION] += $results["count"][$value];
                    }
                }
            }
        }

        return $results;
    }

    //*********************** Count search results********************************************************//
    // params countTYpe is an array defining collection searching in modules context
    // params query is array of condition general
    // params queryPersons is array of condition specific for people
    // params queryNews is array of condition specific for news
    // params queryEvents is array of condition specific for events
    /**
     * Counts the results by collection.
     *
     * @param array $countType The count types.
     * @param array $query The query.
     * @param array $queryPersons The query for persons.
     * @param array $queryNews The query for news.
     * @param array $queryEvents The query for events.
     * @param array $queryClassifieds The query for classifieds.
     * @param array $queryProposals The query for proposals.
     * @param array $queryPoi The query for points of interest.
     * @return array The count results.
     */
    public function countResultsByCollection(array $countType, array $query, array $queryPersons, array $queryNews, array $queryEvents, array $queryClassifieds, array $queryProposals, array $queryPoi): array
    {
        $count = [];
        foreach ($countType as $value) {
            $countQuery = $query;
            $col = (string) $value;
            if ($value == PersonInterface::COLLECTION) {
                $countQuery = [
                    '$and' => [
                        $queryPersons, [
                            "roles.tobeactivated" => [
                                '$exists' => 0,
                            ],
                        ]],
                ];
            } elseif ($value == NewsInterface::COLLECTION) {
                $countQuery = $queryNews;
            } elseif ($value == PoiInterface::COLLECTION) {
                $countQuery = $queryPoi;
            } elseif ($value == EventInterface::COLLECTION) {
                $countQuery = $queryEvents;
            } elseif ($value == ClassifiedInterface::COLLECTION) {
                $countQuery = $queryClassifieds;
            } elseif ($value == ProposalInterface::COLLECTION) {
                $countQuery = $queryProposals;
            } elseif (in_array($value, ["Group", "NGO", "LocalBusiness", "GovernmentOrganization", "Cooperative"])) {
                $countQuery = [
                    '$and' => [
                        $countQuery, [
                            "type" => $value,
                        ]],
                ];

                $col = OrganizationInterface::COLLECTION;
            }
            $count[(string) $value] = $this->db->count($col, $countQuery);
        }

        return $count;
    }

    //*********************************  Search   ******************************************
    /**
     * Search for a string in an array of queries.
     *
     * @param string $search The string to search for.
     * @param array $query The array of queries to search in.
     * @return array The array of search results.
     */
    public function searchString(string $search, array $query): array
    {
        if (strpos($search, "#") > -1) {
            $searchTagText = substr($search, 1, strlen($search));
            $tags = explode("#", $searchTagText);
            $query = $this->searchTags($tags);
        } else {
            $searchRegExp = $this->accentToRegex($search);
            $query = [
                '$or' => [[
                    "name" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "title" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "slug" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "sigle" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "siret" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "shortDescription" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ], [
                    "description" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                ]],
            ];
            $explodeSearchRegExp = explode(" ", $searchRegExp);
            if (count($explodeSearchRegExp) > 1) {
                $andArray = [];
                foreach ($explodeSearchRegExp as $data) {
                    array_push($andArray, [
                        "name" => $this->db->MongoRegex("/.*{$data}.*/i"),
                    ]);
                }
                $query = [
                    '$or' => [
                        $query, [
                            '$and' => $andArray,
                        ]],
                ];
            }
        }
        return $query;
    }

    /**
     * Search for filters in the given query.
     *
     * @param array|null $filters The filters to search for.
     * @param array $query The query to search in.
     * @return array The filtered results.
     */
    public function searchFilters(?array $filters, array $query): array
    {
        if (! empty($filters)) {
            foreach ($filters as $key => $value) {
                if (! empty($value['$exists'])) {
                    $query = [
                        '$and' => [
                            $query, [
                                $key => $value,
                            ]],
                    ];
                } elseif (is_array($value)) {
                    $query = [
                        '$and' => [
                            $query, [
                                $key => [
                                    '$in' => $value,
                                ],
                            ]],
                    ];
                } elseif (is_string($value)) {
                    $query = [
                        '$and' => [
                            $query, [
                                $key => $value,
                            ]],
                    ];
                }
            }
        }

        return $query;
    }

    //*********************************  Search  in news ******************************************
    /**
     * Search for news based on a search string and query parameters.
     *
     * @param string $search The search string.
     * @param array $query The query parameters.
     * @return array The search results.
     */
    public function searchNewsString(string $search, array $query): array
    {
        if (strpos($search, "#") > -1) {
            $searchTagText = substr($search, 1, strlen($search));
            $query = $this->searchTags([$searchTagText]);
        } else {
            $searchRegExp = $this->accentToRegex($search);
            $query = [
                "text" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
            ];
            $explodeSearchRegExp = explode(" ", $searchRegExp);
            if (count($explodeSearchRegExp) > 1) {
                $andArray = [];
                foreach ($explodeSearchRegExp as $data) {
                    array_push(
                        $andArray,
                        [
                            '$or' => [[
                                "text" => $this->db->MongoRegex("/.*{$data}.*/i"),
                            ], [
                                "media.name" => $this->db->MongoRegex("/.*{$data}.*/i"),
                            ]],
                        ]
                    );
                }
                $query = [
                    '$or' => [
                        $query, [
                            '$and' => $andArray,
                        ]],
                ];
            }
        }

        return $query;
    }

    //*********************************  TAGS   ******************************************
    /**
     * Search for tags.
     *
     * @param array|string|null $searchTags The tags to search for.
     * @param string $verb The comparison operator for the search. Defaults to '$in'.
     * @return array The search results.
     */
    public function searchTags($searchTags, string $verb = '$in'): array
    {
        $isString = false;
        $tmpTags = [];
        $query = [];
        if (! empty($searchTags)) {
            if (is_array($searchTags)) {
                foreach ($searchTags as $value) {
                    if (trim((string) $value) != "") {
                        $value = rtrim(rtrim((string) $value), " ,");
                        $tmpTags[] = $this->db->MongoRegex("/^" . $this->accentToRegex($value) . "$/i");
                    }
                }
            } else {
                $tmpTags[] = $this->db->MongoRegex("/^" . $this->accentToRegex($searchTags) . "$/i");
                $isString = true;
            }

            if (count($tmpTags)) {
                $allverb = ['$in', '$all'];
                if (! in_array($verb, $allverb)) {
                    $verb = '$in';
                }
                $query = [
                    "tags" => [
                        $verb => $tmpTags,
                    ],
                ];
            }
        }

        if ($isString && count($tmpTags)) {
            $query = [
                '$and' => [
                    $query, [
                        "tags" => [
                            '$in' => $tmpTags,
                        ],
                    ]],
            ];
        }

        return $query;
    }

    /**
     * Concatenates two query arrays using the specified verb.
     *
     * @param array|null $q1 The first query array.
     * @param array $q2 The second query array.
     * @param string $verb The verb used to concatenate the arrays.
     * @return array The concatenated query array.
     */
    public function concatQuery(?array $q1, array $q2, string $verb): array
    {
        if (empty($q1)) {
            $query = $q2;
        } else {
            $query = [
                $verb => [$q1, $q2],
            ];
        }
        return $query;
    }

    /**
     * Search for a specific source key.
     *
     * @param string|array|null $sourceKey The source key to search for.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchSourceKey($sourceKey, array $query): array
    {
        $tmpSourceKey = [];
        if ($sourceKey != null && $sourceKey != "") {
            //Several Sourcekey
            if (is_array($sourceKey)) {
                foreach ($sourceKey as $value) {
                    $tmpSourceKey[] = $value;
                }
            }//One Sourcekey
            else {
                $tmpSourceKey[] = $sourceKey;
            }

            $costum = $this->getModelCostum()->getCostum();
            $origin = (@$costum) ? "costum" : "network";
            $query = [
                '$and' => [
                    $query, [
                        '$or' => [[
                            "source.keys" => [
                                '$in' => $tmpSourceKey,
                            ],
                        ], [
                            "reference." . $origin => [
                                '$in' => $tmpSourceKey,
                            ],
                        ]],
                    ]],
            ];

            unset($tmpSourceKey);
        }
        return $query;
    }

    //*********************************  Zones   ******************************************
    /**
     * Search for zones based on localities.
     *
     * @param array $localities The array of localities to search for.
     * @return array The array of zones matching the localities.
     */
    public function searchZones(array $localities): array
    {
        $query = [];
        foreach ($localities as $key => $locality) {
            $zone = $this->db->findOne("zones", [
                "_id" => $this->db->MongoId($locality["id"]),
            ]);
            $queryLocality = [
                "address.codeInsee" => $zone["insee"],
                'geoPosition' => [
                    '$geoWithin' => [
                        '$polygon' => $zone["geoShape"]["coordinates"][0],
                    ],
                ],
            ];
            if (empty($query)) {
                $query = $queryLocality;
            } elseif (! empty($queryLocality)) {
                $query = [
                    '$or' => [$query, $queryLocality],
                ];
            }
        }
        return $query;
    }

    //****************************DEFINE LOCALITY QUERY   ***************************************
    //*********************************  ZONES   *************************************************
    //************************ LOCALITY QUERY FOR ELEMENT ****************************************
    // TODO : $geo regarder le type
    /**
     * Search for localities based on the given query and optional geographical coordinates.
     *
     * @param array|null $localities An array of localities to search in.
     * @param array $query The search query.
     * @param mixed|null $geo The geographical coordinates for the search.
     * @return array An array of search results.
     */
    public function searchLocality(?array $localities, array $query, $geo = null): array
    {
        $allQueryLocality = [];
        if (! empty($localities) && empty($geo)) {
            foreach ($localities as $key => $locality) {
                if (! empty($locality)) {
                    if (@$locality["type"] == CityInterface::COLLECTION) {
                        $queryLocality = [
                            "address.localityId" => @$locality["id"],
                        ];
                        if (! empty($locality["postalCode"])) {
                            $queryLocality = array_merge($queryLocality, [
                                "address.postalCode" => $this->db->MongoRegex("/^" . $locality["postalCode"] . "/i"),
                            ]);
                        }
                    } elseif (@$locality["type"] == "cp") {
                        $queryLocality = [
                            "address.postalCode" => $this->db->MongoRegex("/^" . $locality["name"] . "/i"),
                        ];
                        if (! empty($locality["countryCode"])) {
                            $queryLocality = array_merge($queryLocality, [
                                "address.addressCountry" => $locality["countryCode"],
                            ]);
                        }
                    } elseif (@$locality["type"] == "country") {
                        $queryLocality = [
                            "address.addressCountry" => $locality["countryCode"],
                        ];
                    } else {
                        $queryLocality = [
                            "address." . $locality["type"] => @$locality["id"],
                        ];
                    }

                    $scope = [
                        "scope." . $key => [
                            '$exists' => 1,
                        ],
                    ];
                    $queryLocality = [
                        '$or' => [$queryLocality, $scope],
                    ];

                    if (empty($allQueryLocality)) {
                        $allQueryLocality = $queryLocality;
                    } elseif (! empty($queryLocality)) {
                        $allQueryLocality = [
                            '$or' => [$allQueryLocality, $queryLocality],
                        ];
                    }
                }
            }
        } elseif (! empty($geo)) {
            $allQueryLocality = $this->getModelSIG()->getGeoQuery($geo, "geoPosition.coordinates");
        }

        //modifié le 21/10/2017 by Tango, en espérant que ça ne casse aucun autre process
        //(la query originale était perdu => pb pour les tags)
        if (! empty($allQueryLocality)) {
            if (! empty($query)) {
                $query = [
                    '$and' => [$query, $allQueryLocality],
                ];
            } else {
                $query = [
                    '$and' => [$allQueryLocality],
                ];
            }
        }
        return $query;
    }

    /**
     * Search for a specific scope.
     *
     * @param array|null $localities The localities to search within.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchScope(?array $localities, array $query): array
    {
        $allQueryLocality = [];
        if (! empty($localities)) {
            foreach ($localities as $key => $locality) {
                if (! empty($locality)) {
                    $queryLocality = [
                        "scope.key" . $key => [
                            '$exists' => 1,
                        ],
                    ];

                    if (empty($allQueryLocality)) {
                        $allQueryLocality = $queryLocality;
                    } elseif (! empty($queryLocality)) {
                        $allQueryLocality = [
                            '$or' => [$allQueryLocality, $queryLocality],
                        ];
                    }
                }
            }
        }

        if (! empty($allQueryLocality)) {
            if (! empty($query)) {
                $query = [
                    '$and' => [$query, $allQueryLocality],
                ];
            } else {
                $query = [
                    '$and' => [$allQueryLocality],
                ];
            }
        }
        return $query;
    }

    //****************************DEFINE LOCALITY QUERY   ***************************************
    //*********************************  ZONES   *************************************************
    //************************ LOCALITY QUERY FOR ELEMENT ****************************************
    /**
     * Search for locality network.
     *
     * @param array|null $localities The localities to search in.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchLocalityNetwork(?array $localities, array $query): array
    {
        $allQueryLocality = [];
        if (! empty($localities)) {
            foreach ($localities as $key => $locality) {
                if (! empty($locality)) {
                    if ($locality["type"] == CityInterface::COLLECTION) {
                        $queryLocality = [
                            "address.localityId" => $locality["id"],
                        ];
                        if (! empty($locality["postalCode"])) {
                            $queryLocality = array_merge($queryLocality, [
                                "address.postalCode" => $this->db->MongoRegex("/^" . $locality["postalCode"] . "/i"),
                            ]);
                        }
                    } elseif ($locality["type"] == "cp") {
                        $queryLocality = [
                            "address.postalCode" => $this->db->MongoRegex("/^" . $locality["name"] . "/i"),
                        ];
                        if (! empty($locality["countryCode"])) {
                            $queryLocality = array_merge($queryLocality, [
                                "address.addressCountry" => $locality["countryCode"],
                            ]);
                        }
                    } else {
                        $queryLocality = [
                            "address." . $locality["type"] => $locality["id"],
                        ];
                    }

                    if (empty($allQueryLocality)) {
                        $allQueryLocality = $queryLocality;
                    } elseif (! empty($queryLocality)) {
                        $allQueryLocality = [
                            '$or' => [$allQueryLocality, $queryLocality],
                        ];
                    }
                }
            }
        }

        //modifié le 21/10/2017 by Tango, en espérant que ça ne casse aucun autre process
        //(la query originale était perdu => pb pour les tags)
        if (! empty($allQueryLocality)) {
            if (! empty($query)) {
                $query = [
                    '$and' => [$query, $allQueryLocality],
                ];
            } else {
                $query = [
                    '$and' => [$allQueryLocality],
                ];
            }
        }
        return $query;
    }

    //***************************** LOCALITY QUERY FOR NEWS********************************************
    /**
     * Search for news in specific localities.
     *
     * @param array|null $localities The localities to search in.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchLocalityNews(?array $localities, array $query): array
    {
        $allQueryLocality = [];
        if (! empty($localities)) {
            foreach ($localities as $key => $locality) {
                if (! empty($locality)) {
                    if ($locality["type"] == CityInterface::COLLECTION) {
                        $queryLocality = [
                            "scope.localities.parentId" => $locality["id"],
                            "scope.localities.parentType" => CityInterface::COLLECTION,
                        ];
                        if (! empty($locality["postalCode"])) {
                            $queryLocality = array_merge($queryLocality, [
                                "scope.localities.postalCode" => $this->db->MongoRegex("/^" . $locality["postalCode"] . "/i"),
                            ]);
                        }
                    } elseif ($locality["type"] == "cp") {
                        $queryLocality = [
                            "scope.localities.postalCode" => $this->db->MongoRegex("/^" . $locality["name"] . "/i"),
                        ];
                    } else {
                        $queryLocality = [
                            '$or' => [[
                                "scope.localities.parentId" => $locality["id"],
                            ], [
                                "scope.localities." . $locality["type"] => $locality["id"],
                            ]],
                        ];
                    }

                    if (empty($allQueryLocality)) {
                        $allQueryLocality = $queryLocality;
                    } elseif (! empty($queryLocality)) {
                        $allQueryLocality = [
                            '$or' => [$allQueryLocality, $queryLocality],
                        ];
                    }
                }
            }
        }
        //modifié le 21/10/2017 by Tango, en espérant que ça ne casse aucun autre process
        //(la query originale était perdu => pb pour les tags)
        if (! empty($allQueryLocality)) {
            if (! empty($query)) {
                $query = [
                    '$and' => [$query, $allQueryLocality],
                ];
            } else {
                $query = [
                    '$and' => [$allQueryLocality],
                ];
            }
        }
        return $query;
    }
    //*********************************  END DEFINE LOCALITY QUERY   ****************************************

    //*********************************  Specific queries   ****************************************
    // TODO : verfier comment $startDate, $endDate sont utilisés et quel type ils ont
    /**
     * Retrieves the query events based on the given parameters.
     *
     * @param array $queryEvent The query event array.
     * @param mixed $startDate The start date of the events.
     * @param mixed $endDate The end date of the events.
     * @return array The array of query events.
     */
    public function getQueryEvents(array $queryEvent, $startDate, $endDate): array
    {
        $rangeDate = [];
        if ($startDate != null) {
            if ($endDate != null) {
                $rangeDate = [
                    '$or' => [[
                        "startDate" => [
                            '$gte' => $this->db->MongoDate((float) $startDate),
                            '$lte' => $this->db->MongoDate((float) $endDate),
                        ],
                    ], [
                        "startDate" => [
                            '$lte' => $this->db->MongoDate((float) $startDate),
                        ],
                        "endDate" => [
                            '$gte' => $this->db->MongoDate((float) $startDate),
                        ],
                    ]],
                ];
            } else {
                $rangeDate = [
                    '$or' => [[
                        "startDate" => [
                            '$gte' => $this->db->MongoDate((float) $startDate),
                        ],
                    ], [
                        "endDate" => [[
                            '$exists' => true,
                        ], [
                            '$gte' => $this->db->MongoDate((float) $startDate),
                        ]],
                    ]],
                ];
            }
        } elseif ($endDate != null) {
            $rangeDate = [
                "endDate" => [
                    '$lte' => $this->db->MongoDate((float) $endDate),
                ],
            ];
        }

        if (! empty($rangeDate)) {
            $queryEvent = [
                '$and' => [$queryEvent, $rangeDate],
            ];
        }

        return $queryEvent;
    }

    /**
     * Retrieves organizations based on the provided query and category.
     *
     * @param array $query The search query parameters.
     * @param string|null $category The category to filter the organizations by.
     * @return array The array of organizations matching the query and category.
     */
    public function getQueryOrganizations(array $query, ?string $category): array
    {
        if (isset($category) && $category != "") {
            array_push($query['$and'], [
                "category" => $category,
            ]);
        }

        return $query;
    }

    /**
     * Get the query with a custom costum value.
     *
     * @param array $query The query array.
     * @param string|null $costum The custom costum value.
     * @return array The modified query array.
     */
    public function getQueryCostum(array $query, ?string $costum): array
    {
        if (isset($costum) && $costum != "") {
            array_push($query['$and'], [
                "costum" => $costum,
            ]);
        }

        return $query;
    }

    /**
     * Retrieves the bookmark query based on the given parameters.
     *
     * @param array $query The search query.
     * @param string|null $category The category to filter the search results.
     * @return array The bookmark query.
     */
    public function getQueryBookmark(array $query, ?string $category): array
    {
        if (isset($category) && $category != "") {
            array_push($query['$and'], [
                "category" => $category,
            ]);
        }

        return $query;
    }

    /**
     * Retrieves classifieds based on the provided query parameters.
     *
     * @param array $query The search query parameters.
     * @param string $section The section of the classifieds.
     * @param string $category The category of the classifieds.
     * @param string $subType The subtype of the classifieds.
     * @param int $priceMin The minimum price of the classifieds.
     * @param int $priceMax The maximum price of the classifieds.
     * @param string $devise The currency of the classifieds.
     * @return array The classifieds matching the query parameters.
     */
    public function getQueryClassifieds(array $query, string $section, string $category, string $subType, int $priceMin, int $priceMax, string $devise): array
    {
        $queryPrice = [
            '$and' => [[
                'devise' => $devise,
            ]],
        ];

        if (@$priceMax) {
            array_push($query['$and'], [
                'price' => [
                    '$lte' => (int) $priceMax,
                ],
            ]);
        }
        if (@$priceMin) {
            array_push($query['$and'], [
                'price' => [
                    '$gte' => (int) $priceMin,
                ],
            ]);
        }
        if (@$devise) {
            array_push($query['$and'], [
                'devise' => $devise,
            ]);
        }

        if (isset($subType) && $subType != "") {
            array_push($query['$and'], [
                "subtype" => $subType,
            ]);
        }
        if (isset($category) && $category != "") {
            array_push($query['$and'], [
                "category" => $category,
            ]);
        }
        if (isset($section) && $section != "") {
            array_push($query['$and'], [
                "section" => $section,
            ]);
        }
        return $query;
    }

    /**
     * Retrieves the query points of interest.
     *
     * @param array $query The search query.
     * @param string|null $category The category of the points of interest.
     * @return array The array of query points of interest.
     */
    public function getQueryPoi(array $query, ?string $category): array
    {
        if (isset($category) && $category != "") {
            array_push($query['$and'], [
                "type" => $category,
            ]);
        }
        return $query;
    }

    //*********************************  END Specific squeries   ****************************************
    //trie les éléments dans l'ordre alphabetique par name
    /**
     * Sorts an array of elements by name.
     *
     * @param array $a The first element to compare.
     * @param array $b The second element to compare.
     * @return bool Returns true if $a should be placed before $b, false otherwise.
     */
    public function mySortByName(array $a, array $b): bool
    {
        if (isset($a["_id"]) && isset($b["name"])) {
            return (strtolower((string) $b["name"]) < strtolower((string) $a["name"]));
        } else {
            return false;
        }
    }

    //trie les éléments dans l'ordre alphabetique par updated
    /**
     * Sorts the given arrays by their "updated" value.
     *
     * @param array $a The first array to compare.
     * @param array $b The second array to compare.
     * @return bool Returns true if $a is considered less than $b, false otherwise.
     */
    public function mySortByUpdated(array $a, array $b): bool
    {
        if (isset($a["updated"]) && isset($b["updated"])) {
            return (strtolower((string) $b["updated"]) > strtolower((string) $a["updated"]));
        } else {
            return false;
        }
    }

    /**
     * Returns the type of localisation based on the given location string.
     *
     * @param string $locStr The location string.
     * @return string The type of localisation.
     */
    public function getTypeOfLocalisation(string $locStr): string
    {
        //le cas des localisation intégralement numérique (code postal, insee, departement)
        if (intval($locStr) > 0) {
            if (strlen($locStr) <= 3) {
                return "DEPARTEMENT";
            }
            if (strlen($locStr) == 4 || strlen($locStr) == 5) {
                return "CODE_POSTAL_INSEE";
            }
            return "UNDEFINED";
        } else {
            //le cas où le lieu est demandé en toute lettre
            return "NAME";
        }
    }

    /**
     * Check if the given type matches the wanted type.
     *
     * @param string $type The type to check.
     * @param array|null $searchType The wanted type to compare against. Default is null.
     * @return bool Returns true if the given type matches the wanted type, false otherwise.
     */
    public function typeWanted(string $type, ?array $searchType = null): bool
    {
        if ($searchType == null) {
            return false;
        }
        if ($searchType[0] == "all") {
            return true;
        }
        return in_array($type, $searchType);
    }

    // TODO : $_POST : attention utilisation dans le model pas bonne
    // TODO : $parentObj regarder comment il est utilisé j'ai mis en ?array pour le moment
    /**
     * Check if the given parent object is valid for the scope.
     *
     * @param array|null $parentObj The parent object to check.
     * @return bool Returns true if the parent object is valid for the scope, false otherwise.
     */
    public function checkScopeParent(?array $parentObj): bool
    {
        $localityReferences = [];

        $localityReferences['CITYKEY'] = "";
        $localityReferences['CODE_POSTAL'] = "address.postalCode";
        $localityReferences['DEPARTEMENT'] = "address.postalCode";
        $localityReferences['REGION'] = ""; //Spécifique

        $countScope = 0;
        foreach ($localityReferences as $key => $value) {
            if (isset($_POST["searchLocality" . $key]) && (is_countable($_POST["searchLocality" . $key]) ? count($_POST["searchLocality" . $key]) : 0) > 0 && $_POST["searchLocality" . $key][0] != "") {
                $countScope++;
            }
        }
        if ($countScope == 0) { //error_log("return true EMPTY");
            return true;
        }

        foreach ($localityReferences as $key => $value) {
            if (isset($_POST["searchLocality" . $key])
                && is_array($_POST["searchLocality" . $key])
                && count($_POST["searchLocality" . $key]) > 0) {
                foreach ($_POST["searchLocality" . $key] as $localityRef) {
                    if (isset($localityRef) && $localityRef != "") {
                        //OneRegion
                        if ($key == "CITYKEY") {
                            $city = $this->getModelCity()->getByUnikey($localityRef);
                            if (empty($city["cp"])) {
                                if (@$parentObj["address"]["addressCountry"] == $city["country"] &&
                                   @$parentObj["address"]["codeInsee"] == $city["insee"]) {
                                    return true;
                                }
                            } else {
                                if (@$parentObj["address"]["addressCountry"] == $city["country"] &&
                                   @$parentObj["address"]["codeInsee"] == $city["insee"] &&
                                   @$parentObj["address"]["postalCode"] == $city["cp"]) {
                                    return true;
                                }
                            }
                        } elseif ($key == "CODE_POSTAL") {
                            if (@$parentObj["address"]["postalCode"] == $localityRef) {
                                return true;
                            }
                        } elseif ($key == "DEPARTEMENT") {
                            $dep = $this->db->findOne(CityInterface::COLLECTION, [
                                "depName" => $localityRef,
                            ], ["dep"]);
                            if (preg_match("/^{$dep['dep']}/i", @$parentObj["address"]["postalCode"])) {
                                return true;
                            }
                        } elseif ($key == "REGION") {
                            $deps = $this->db->find(CityInterface::COLLECTION, [
                                "regionName" => $localityRef,
                            ], ["dep"]);
                            $departements = [];
                            $inQuest = [];
                            if (is_array($deps)) {
                                foreach ($deps as $index => $value) {
                                    if (! in_array($value["dep"], $departements)) {
                                        $departements[] = $value["dep"];
                                        if (preg_match("/^{$value['dep']}/i", @$parentObj["address"]["postalCode"])) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    // TODO : $parentObj $tags regarder comment ils sont utilisé j'ai mis en ?array pour le moment
    /**
     * Check if the given parent object has the specified tags.
     *
     * @param array|null $parentObj The parent object to check.
     * @param array|null $tags The tags to check.
     * @return bool Returns true if the parent object has the tags, false otherwise.
     */
    public function checkTagsParent(?array $parentObj, ?array $tags): bool
    {
        if ((is_countable($tags) ? count($tags) : 0) <= 0) {
            return true;
        }
        foreach ($tags as $key => $tag) {
            error_log("checkTagsParent tag : " . $tag);
            if (@$parentObj["tags"]) {
                foreach ($parentObj["tags"] as $key => $parentTag) {
                    error_log("checkTagsParent parentTag : " . $parentTag);
                    if (preg_match("/.*{$tag}.*/i", $parentTag)) {
                        error_log("checkTagsParent return true");
                        return true;
                    }
                }
            }
        }

        error_log("checkTagsParent return false");

        return false;
    }

    //********************************* COSTUM  ******************************************/

    // TODO : $searchCostum is not used
    /**
     * Search for custom items based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for indexing.
     * @param int $indexMin The minimum index value.
     * @param mixed $searchCostum The custom search parameter.
     * @return array The search results as an array.
     */
    public function searchCostum(array $query, int $indexStep, int $indexMin, $searchCostum): array
    {
        $res = [];

        array_push($query['$and'], [
            "costum" => [
                '$exists' => 1,
            ],
        ]);

        $orga = $this->searchOrganizations($query, $indexStep, $indexMin, null, [
            "updated" => -1,
        ]);
        $project = $this->searchProject($query, $indexStep, $indexMin, [
            "updated" => -1,
        ]);

        $res = array_merge($orga, $project);

        return $res;
    }

    //********************************* BOOKMARK  ******************************************/

    // TODO : $indexStep, $indexMin, $type, $searchBook are not used
    /**
     * Search for bookmarks based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step value for pagination.
     * @param int $indexMin The minimum index value for pagination.
     * @param string $type The type of bookmark to search for.
     * @param bool $searchBook Whether to search for books or not.
     * @return array The search results as an array.
     */
    public function searchBookmark(array $query, $indexStep, $indexMin, $type, $searchBook): array
    {
        $res = [];

        $queryTest = $query;

        $allBookmark = $this->db->find(BookmarkInterface::COLLECTION, $query, null);

        foreach ($allBookmark as $key => $value) {
            if (! empty($value)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }

    //********************************* EVENTS COMMUNITY  ******************************************/

    /**
     * Search for a community based on the given query parameters.
     *
     * @param array $query The search query parameters.
     * @param array|null $community The community to search within (optional).
     * @return array The search results as an array.
     */
    public function searchCommunity(array $query, ?array $community): array
    {
        $arrayIds = [];
        if (! empty($community) && ! empty($community["sourceKey"])) {
            if (is_array($community["sourceKey"])) {
                foreach ($community["sourceKey"] as $keyC => $valueC) {
                    $sourceCostum = $this->getModelSlug()->getElementBySlug($valueC, ["name"]);
                    $arrayIds[] = [
                        "type" => $sourceCostum["type"],
                        "id" => $sourceCostum["id"],
                    ];
                }
            } else {
                $sourceCostum = $this->getModelSlug()->getElementBySlug($community["sourceKey"], ["name"]);
                $arrayIds[] = [
                    "type" => $sourceCostum["type"],
                    "id" => $sourceCostum["id"],
                ];
            }
        } else {
            $costum = $this->getModelCostum()->getCostum();
            // WARNING
            $arrayIds[] = [
                "type" => $costum["contextType"],
                "id" => $costum["contextId"],
            ];
        }

        $queryCommunity = [];
        // var_dump($arrayIds);exit;
        if (! empty($arrayIds)) {
            foreach ($arrayIds as $keyelt => $valelt) {
                $allCommunity = $this->getModelElement()->getByTypeAndId($valelt["type"], $valelt["id"]);

                // TO OPTIMIZE
                if (! empty($community["roles"])) {
                    // var_dump($community["roles"]);exit;
                    array_push($queryCommunity, [
                        "links.organizer." . $valelt["id"] . ".roles" => $community["roles"],
                    ]);
                    array_push($queryCommunity, [
                        "links.memberOf." . $valelt["id"] . ".roles" => $community["roles"],
                    ]);
                    array_push($queryCommunity, [
                        "links.members." . $valelt["id"] . ".roles" => $community["roles"],
                    ]);
                    array_push($queryCommunity, [
                        "links.projects." . $valelt["id"] . ".roles" => $community["roles"],
                    ]);
                } else {
                    array_push($queryCommunity, [
                        "links.organizer." . $valelt["id"] => [
                            '$exists' => 1,
                        ],
                    ]);
                    array_push($queryCommunity, [
                        "links.members." . $valelt["id"] => [
                            '$exists' => 1,
                        ],
                    ]);
                    array_push($queryCommunity, [
                        "links.memberOf." . $valelt["id"] => [
                            '$exists' => 1,
                        ],
                    ]);
                    array_push($queryCommunity, [
                        "links.projects." . $valelt["id"] => [
                            '$exists' => 1,
                        ],
                    ]);
                }

                if (isset($community["sourceKey"]) && $community["children"] = true) {
                    foreach ($allCommunity as $key => $value) {
                        if (! empty($value)) {
                            array_push($queryCommunity, [
                                "links.organizer." . $key => [
                                    '$exists' => 1,
                                ],
                            ]);
                            array_push($queryCommunity, [
                                "links.memberOf." . $key => [
                                    '$exists' => 1,
                                ],
                            ]);
                        }
                    }
                }
            }
        }

        if (! empty($queryCommunity)) {
            $queryFinal = [
                '$and' => [
                    $query, [
                        '$or' => $queryCommunity,
                    ]],
            ];
        } else {
            $queryFinal = $query;
        }

        return $queryFinal;
    }

    //*********************************  BADGES   ******************************************
    /**
     * Search for badges based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The array of badges matching the search query.
     */
    public function searchBadges(array $query, int $indexStep, int $indexMin): array
    {
        $res = [];
        $allBadges = $this->db->findAndSortAndLimitAndIndex(
            BadgeInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );

        foreach ($allBadges as $key => $value) {
            $value["type"] = BadgeInterface::COLLECTION;
            $res[$key] = $value;
        }
        return $res;
    }

    //*********************************  PERSONS   ******************************************
    /**
     * Search for persons based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param bool $prefLocality Whether to prioritize locality in the search.
     * @param array|null $community The community to filter the search results.
     * @return array The array of search results.
     */
    public function searchPersons(array $query, int $indexStep, int $indexMin, bool $prefLocality = false, ?array $community): array
    {
        $OrgaId = [];
        $res = [];
        $allPersons = [];
        $query = [
            '$and' => [
                $query, [
                    "roles.tobeactivated" => [
                        '$exists' => 0,
                    ],
                ]],
        ];

        $allCitoyen = $this->db->findAndSortAndLimitAndIndex(
            PersonInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );

        // MODIF HERE
        if (! empty($community["memberOf"]) == true) {
            $costum = $this->getModelCostum()->getCostum();
            // WARNING
            $OrgaId[] = [
                "type" => $costum["contextType"],
                "id" => $costum["contextId"],
            ];
        }

        foreach ($allCitoyen as $key => $value) {
            if (! empty($community["memberOf"]) && $community["memberOf"] == true) {
                $person = $this->getModelPerson()->getById($key);
            } else {
                $person = $this->getModelPerson()->getSimpleUserById($key, $value);
            }
            $person["collection"] = PersonInterface::COLLECTION;
            $person["type"] = PersonInterface::COLLECTION;
            $person["typeSig"] = "citoyens";
            if (@$value["links"]["followers"][$this->session->getUserId()]) {
                $person["isFollowed"] = true;
            }
            $res[$key] = $person;
        }
        return $res;
    }

    //*********************************  ORGANIZATIONS   ******************************************
    /**
     * Search organizations based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The number of results to return per page.
     * @param int $indexMin The minimum index of the results to return.
     * @param string $searchTypeOrga The type of organization to search for.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchOrganizations(array $query, int $indexStep, int $indexMin, string $searchTypeOrga, array $sort = [
        "updated" => -1,
    ]): array
    {
        $res = [];
        $queryOrganization = $query;
        if (! isset($queryOrganization['$and'])) {
            $queryOrganization['$and'] = [];
        }
        array_push($queryOrganization['$and'], [
            "disabled" => [
                '$exists' => false,
            ],
        ]);
        if (! empty($searchTypeOrga)) {
            $queryTypeOrga = [
                '$or' => [],
            ];
            array_push($queryOrganization['$and'], [
                "type" => $searchTypeOrga,
            ]);
        }
        $allOrganizations = $this->db->findAndSortAndLimitAndIndex(OrganizationInterface::COLLECTION, $queryOrganization, $sort, $indexStep, $indexMin);

        foreach ($allOrganizations as $key => $value) {
            if (! empty($value)) {
                if (@$value["links"]["followers"][$this->session->getUserId()]) {
                    $value["isFollowed"] = true;
                }

                $res[$key] = $value;
            }
        }

        return $res;
    }

    //*********************************  NEWS   ******************************************
    /**
     * Search for news based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchNews(array $query, int $indexStep, int $indexMin): array
    {
        $res = [];
        $allNews = $this->db->findAndSortAndLimitAndIndex(
            NewsInterface::COLLECTION,
            $query,
            [
                "sharedBy.updated" => -1,
            ],
            $indexStep,
            $indexMin
        );

        foreach ($allNews as $key => $value) {
            if (! empty($value)) {
                $value = $this->getModelNewsTranslator()->convertParamsForNews($value, false);

                $res[$key] = $value;
            }
        }
        return $res;
    }

    /**
     * Search for answers based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for indexing.
     * @param int $indexMin The minimum index value.
     * @return array The array of search results.
     */
    public function searchAnswers(array $query, int $indexStep, int $indexMin): array
    {
        $res = [];
        $allanswers = $this->db->findAndSortAndLimitAndIndex(
            AnswerInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );
        if ($this->getModelCostum()->isSameFunction("afterSearchAnswers")) {
            $allanswers = $this->getModelCostum()->sameFunction("afterSearchAnswers", $allanswers);
        }

        return $allanswers;
    }

    //*********************************  EVENT   ******************************************
    // public function searchEventsOld($query, $indexStep, $indexMin, $searchOnAll = null, $all = null, $sort = [
    //     "startDate" => -1,
    // ])
    // {
    //     //date_default_timezone_set('UTC');
    //     $queryEvent = $query;
    //     // if(!empty($sort))
    //     // 	$sort=$sort;
    //     //   	else if($searchOnAll)
    //     //   		$sort=array("updated" => -1);
    //     //else if(@$all && $all)
    //     //	$sort=array("startDate" => -1);
    //     //else
    //     $sort = [
    //         "startDate" => 1,
    //     ];

    //     //var_dump($sort);
    //     $allEvents = $this->db->findAndSortAndLimitAndIndex(
    //         PHType::TYPE_EVENTS,
    //         $queryEvent,
    //         $sort,
    //         $indexStep,
    //         $indexMin
    //     );
    //     foreach ($allEvents as $key => $value) {
    //         $allEvents[$key]["typeEvent"] = @$allEvents[$key]["type"];
    //         $allEvents[$key]["type"] = "events";
    //         $allEvents[$key]["typeSig"] = EventInterface::COLLECTION;
    //         if (@$value["links"]["attendees"][$this->session->getUserId()]) {
    //             $allEvents[$key]["isFollowed"] = true;
    //         }
    //         if (@$allEvents[$key]["startDate"] && @$allEvents[$key]["startDate"]->sec) {
    //             $allEvents[$key]["startDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
    //             $allEvents[$key]["startDate"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
    //         }
    //         if (@$allEvents[$key]["endDate"] && @$allEvents[$key]["endDate"]->sec) {
    //             $allEvents[$key]["endDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
    //             $allEvents[$key]["endDate"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
    //         }
    //         if (! empty($value["organizer"])) {
    //             foreach ($value["organizer"] as $k => $v) {
    //                 $elt = $this->getElement()->getElementSimpleById($k, $v["type"], null, ["slug", "profilThumbImageUrl", "name"]);
    //                 if (! empty($elt)) {
    //                     $allEvents[$key]["organizer"][$k]["name"] = $elt["name"];
    //                     $allEvents[$key]["organizer"][$k]["profilThumbImageUrl"] = (! empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "");
    //                     $allEvents[$key]["organizer"][$k]["slug"] = @$elt["slug"];
    //                 }
    //             }
    //         }
    //         $el = $value;
    //         if (@$el["links"]) {
    //             foreach (["attendees"] as $k) {
    //                 if (@$value["links"][$k]) {
    //                     $allEvents[$key]["counts"][$k] = is_countable(@$value["links"][$k]) ? count(@$value["links"][$k]) : 0;
    //                 }
    //             }
    //         }
    //     }
    //     return $allEvents;
    // }

    // TODO : $searchOnAll $all not used and $startD is defined in fonction
    /**
     * Search events based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param mixed|null $searchOnAll Flag to indicate whether to search on all fields.
     * @param mixed|null $all Flag to indicate whether to search on all fields.
     * @param array $sort The sorting criteria for the search results.
     * @param mixed $startD The start date for the search.
     * @param string $startDateUTC The start date in UTC format.
     * @return array The search results as an array.
     */
    public function searchEvents(array $query, int $indexStep, int $indexMin, $searchOnAll = null, $all = null, $sort = [
        "startDate" => -1,
    ], $startD, string $startDateUTC): array
    {
        date_default_timezone_set('UTC');

        if (! empty($startDateUTC)) {
            $parseDateUTC = [];
            $date1 = new DateTime($startDateUTC);
            $date2 = new DateTime($startDateUTC);

            $startD = $date1->getTimestamp();

            $date2->setTime(23, 59, 59);

            $dayStr = substr($date1->format('D'), 0, 2);

            $endDay = $date2->getTimestamp();

            $parseDateUTC = [
                "dayStr" => $dayStr,
                "startD" => $startD,
                "endDay" => $endDay,
                "year" => $date1->format('Y'),
                "mon" => $date1->format('m'),
                "day" => $date1->format('d'),
            ];

            $queryEvent = [
                '$or' => [[
                    "openingHours.dayOfWeek" => $dayStr,
                ], [
                    "startDate" => [
                        '$gte' => $this->db->MongoDate((float) $startD),
                        '$lte' => $this->db->MongoDate($endDay),
                    ],
                ], [
                    "startDate" => [
                        '$lte' => $this->db->MongoDate((float) $startD),
                    ],
                    "endDate" => [
                        '$gte' => $this->db->MongoDate((float) $startD),
                    ],
                ]],
            ];

            $queryFinal = [
                '$and' => [$query, $queryEvent],
            ];

            $allEvents = $this->db->findAndSortAndLimitAndIndex(
                PHType::TYPE_EVENTS,
                $queryFinal,
                $sort
            );
        } else {
            $sort = [
                "updated" => -1,
            ];
            $queryFinal = $query;

            $allEvents = $this->db->findAndSortAndLimitAndIndex(
                PHType::TYPE_EVENTS,
                $queryFinal,
                $sort,
                $indexStep,
                $indexMin
            );
        }

        if (! empty($allEvents)) {
            foreach ($allEvents as $key => $value) {
                if (@$value["links"]["attendees"][$this->session->getUserId()]) {
                    $allEvents[$key]["isFollowed"] = true;
                }

                if (@$allEvents[$key]["startDate"] && @$allEvents[$key]["startDate"]->sec) {
                    $testS = $allEvents[$key]["startDate"];
                    $allEvents[$key]["startDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
                    $allEvents[$key]["startDate"] = date(DateTime::ISO8601, $allEvents[$key]["startDate"]->sec);
                    if (! empty($startDateUTC) && ! empty($allEvents[$key]["timeZone"])) {
                        $timezoneEvent = new DateTimeZone($allEvents[$key]["timeZone"]);
                        $date = new DateTime($allEvents[$key]["startDate"]);
                        $date->setTimezone($timezoneEvent);
                        $date->setDate(intval($parseDateUTC["year"]), intval($parseDateUTC["mon"]), intval($parseDateUTC["day"]));
                        $allEvents[$key]["startDateSort"] = $date;
                    }
                }

                if (@$allEvents[$key]["endDate"] && @$allEvents[$key]["endDate"]->sec) {
                    $allEvents[$key]["endDateTime"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
                    $allEvents[$key]["endDate"] = date(DateTime::ISO8601, $allEvents[$key]["endDate"]->sec);
                }

                if (! empty($startDateUTC)) {
                    if (! empty($allEvents[$key]["openingHours"]) && ! empty($allEvents[$key]["timeZone"])) {
                        foreach ($allEvents[$key]["openingHours"] as $keyO => $valO) {
                            if (! empty($valO) && ! empty($valO["dayOfWeek"]) && $valO["dayOfWeek"] == $dayStr && isset($valO["hours"])) {
                                $starHourstring = $valO["hours"][0]["opens"];
                                $hourArray = explode(":", $starHourstring);
                                $startHour = mktime($hourArray[0], $hourArray[1], 00, $parseDateUTC["mon"], $parseDateUTC["day"], $parseDateUTC["year"]);
                                $elelele = date(DateTime::ISO8601, $startHour);
                                $timezoneEvent = new DateTimeZone($allEvents[$key]["timeZone"]);
                                $created_at = new DateTime($elelele);
                                $created_at->setTimezone($timezoneEvent);
                                $created_at->setTime($hourArray[0], $hourArray[1], 00);
                                $allEvents[$key]["startDateSort"] = $created_at;
                                $allEvents[$key]["startDateSortFormat"] = $created_at->format(DateTime::ISO8601);
                            }
                        }
                    }
                }

                if (! empty($value["organizer"])) {
                    foreach ($value["organizer"] as $k => $v) {
                        $elt = $this->getModelElement()->getElementSimpleById($k, $v["type"], null, ["slug", "profilThumbImageUrl", "name"]);
                        if (! empty($elt)) {
                            $allEvents[$key]["organizer"][$k]["name"] = $elt["name"];
                            $allEvents[$key]["organizer"][$k]["profilThumbImageUrl"] = (! empty($elt["profilThumbImageUrl"]) ? $elt["profilThumbImageUrl"] : "");
                            $allEvents[$key]["organizer"][$k]["slug"] = @$elt["slug"];
                        }
                    }
                }
                $el = $value;
                if (@$el["links"]) {
                    foreach (["attendees"] as $k) {
                        if (@$value["links"][$k]) {
                            $allEvents[$key]["counts"][$k] = is_countable(@$value["links"][$k]) ? count(@$value["links"][$k]) : 0;
                        }
                    }
                }
            }
        }

        function mySort($a, $b)
        {
            if (isset($a['startDateSort']) && isset($b['startDateSort'])) {
                return $a["startDateSort"]->getTimestamp() - $b["startDateSort"]->getTimestamp();
            } else {
                return false;
            }
        }

        if (! empty($startDateUTC)) {
            usort($allEvents, "mySort");
        }

        return $allEvents;
    }

    //*********************************  PROJECTS   ******************************************
    /**
     * Search for projects based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The number of results to return per page.
     * @param int $indexMin The minimum index of the results to return.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchProject(array $query, int $indexStep, int $indexMin, array $sort = [
        "updated" => -1,
    ]): array
    {
        date_default_timezone_set('UTC');

        $allProject = $this->db->findAndSortAndLimitAndIndex(
            ProjectInterface::COLLECTION,
            $query,
            $sort,
            $indexStep,
            $indexMin
        );

        foreach ($allProject as $key => $value) {
            if (@$project["links"]["followers"][$this->session->getUserId()]) {
                $allProject[$key]["isFollowed"] = true;
            }

            if (@$allProject[$key]["startDate"]) {
                $allProject[$key]["startDate"] = date('Y-m-d H:i:s', @$allProject[$key]["startDate"]->sec);
            }
            if (@$allProject[$key]["endDate"]) {
                $allProject[$key]["endDate"] = date('Y-m-d H:i:s', @$allProject[$key]["endDate"]->sec);
            }

            $el = $value;
            if (@$el["links"]) {
                foreach (["contributors", "followers"] as $k) {
                    if (@$el["links"][$k]) {
                        $allProject[$key]["counts"][$k] = is_countable($el["links"][$k]) ? count($el["links"][$k]) : 0;
                    }
                }
            }
        }
        return $allProject;
    }

    //*********************************  CLASSIFIED   ******************************************
    // TODO : $priceMin, $priceMax, $devise is not used
    /**
     * Recherche les annonces classées en fonction des critères spécifiés.
     *
     * @param array $query Les critères de recherche.
     * @param int $indexStep Le nombre d'annonces à récupérer par étape.
     * @param int $indexMin L'index minimum des annonces à récupérer.
     * @param mixed $priceMin Le prix minimum des annonces à récupérer.
     * @param mixed $priceMax Le prix maximum des annonces à récupérer.
     * @param string $devise La devise utilisée pour les prix.
     * @return array Les annonces classées correspondantes aux critères de recherche.
     */
    public function searchClassified(array $query, int $indexStep, int $indexMin, $priceMin, $priceMax, $devise)
    {
        $allClassified = $this->db->findAndSortAndLimitAndIndex(
            ClassifiedInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );

        foreach ($allClassified as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } else {
                $parent = [];
            }
            $allClassified[$key]["parent"] = $parent;

            $allClassified[$key]["gallery"] = $this->getModelDocument()->listMyDocumentByIdAndType(@$key, "classifieds");
        }
        return $allClassified;
    }

    //*********************************  CLASSIFIED   ******************************************
    /**
     * Search for favorites of a specific type.
     *
     * @param string $type The type of favorites to search for.
     * @return array An array containing the search results.
     */
    public function searchFavorites(string $type): array
    {
        $person = $this->getModelPerson()->getById($this->session->getUserId());
        $res = [];

        if (@$person["collections"] && @$person["collections"]["favorites"] && @$person["collections"]["favorites"][$type]) {
            foreach ($person["collections"]["favorites"][$type] as $key => $value) {
                $el = $this->db->findOne($type, [
                    "_id" => $this->db->MongoId($key),
                ]);
                $el["type"] = $type;
                $el["typeSig"] = $type;
                $res[$key] = $el;
            }
        }
        return $res;
    }

    //*********************************  POI   ******************************************
    /**
     * Search for points of interest (POI) based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchPoi(array $query, int $indexStep, int $indexMin, array $sort = [
        "updated" => -1,
    ]): array
    {
        $allPoi = $this->db->findAndSortAndLimitAndIndex(
            PoiInterface::COLLECTION,
            $query,
            $sort,
            $indexStep,
            $indexMin
        );
        foreach ($allPoi as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } elseif (! empty($value["parent"])) {
                $parent = $value["parent"];
            } else {
                $parent = [];
            }
            $allPoi[$key]["parent"] = $parent;
        }

        return $allPoi;
    }

    //*********************************  PRODUCT   ******************************************
    /**
     * Search for a product based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchProduct(array $query, int $indexStep, int $indexMin): array
    {
        $allProduct = $this->db->findAndSortAndLimitAndIndex(
            ProductInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );
        foreach ($allProduct as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } else {
                $parent = [];
            }
            $allProduct[$key]["parent"] = $parent;
        }
        return $allProduct;
    }

    //*********************************  SERVICE   ******************************************
    /**
     * Search for a service based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchService(array $query, int $indexStep, int $indexMin): array
    {
        $allService = $this->db->findAndSortAndLimitAndIndex(
            ServiceInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );

        foreach ($allService as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } else {
                $parent = [];
            }
            $allService[$key]["parent"] = $parent;
        }
        return $allService;
    }

    //*********************************  CIRCUIT   ******************************************

    /**
    * Searches for a circuit based on the specified criteria.
    *
    * @param array $query The search criteria.
    * @param int $indexStep The starting index for pagination.
    * @param int $indexMin The minimum index of the results to return.
    * @return array The search results.
    */
    public function searchCircuit(array $query, int $indexStep, int $indexMin): array
    {
        $allCircuit = $this->db->findAndSortAndLimitAndIndex(
            CircuitInterface::COLLECTION,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );
        foreach ($allCircuit as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } else {
                $parent = [];
            }
            $allCircuit[$key]["parent"] = $parent;
        }
        return $allCircuit;
    }

    //*********************************  Generic Search   ******************************************
    /**
     * Search for any item in a collection based on a query.
     *
     * @param string $collection The name of the collection to search in.
     * @param array $query The query parameters to filter the search results.
     * @param int $indexStep The step size for pagination of search results.
     * @param int $indexMin The minimum index for pagination of search results.
     * @return array The search results as an array.
     */
    public function searchAny(string $collection, array $query, int $indexStep, int $indexMin): array
    {
        $allPlace = $this->db->findAndSortAndLimitAndIndex(
            $collection,
            $query,
            [
                "updated" => -1,
            ],
            $indexStep,
            $indexMin
        );
        foreach ($allPlace as $key => $value) {
            if (@$value["parentId"] && @$value["parentType"]) {
                $parent = $this->getModelElement()->getElementSimpleById(@$value["parentId"], @$value["parentType"]);
            } else {
                $parent = [];
            }
            $allPlace[$key]["parent"] = $parent;
        }
        return $allPlace;
    }

    //*********************************  DDA   ******************************************
    /**
     * Search for DDA (Data Driven Application) using the given query.
     *
     * @param array $query The search query.
     * @param int $indexMax The maximum number of results to return.
     * @return array The search results.
     */
    public function searchDDA(array $query, int $indexMax): array
    {
        $allRes = [];
        $queryDiscuss = $query;
        if (! isset($queryDiscuss['$and'])) {
            $queryDiscuss['$and'] = [];
        }
        array_push($queryDiscuss['$and'], [
            "type" => ActionRoomInterface::TYPE_DISCUSS,
        ]);
        $allFound = $this->db->findAndSort(ActionRoomInterface::COLLECTION, $queryDiscuss, [
            "updated" => -1,
        ], $indexMax);
        foreach ($allFound as $key => $value) {
            $allFound[$key]["type"] = $value["type"];
            $allFound[$key]["typeSig"] = ActionRoomInterface::COLLECTION;
        }
        $allRes = array_merge($allRes, $allFound);

        $allFound = $this->db->findAndSort(ActionRoomInterface::COLLECTION_ACTIONS, $query, [
            "updated" => -1,
        ], $indexMax);
        foreach ($allFound as $key => $value) {
            $allFound[$key]["type"] = @$value["type"];
            $allFound[$key]["typeSig"] = ActionRoomInterface::COLLECTION_ACTIONS;
        }
        $allRes = array_merge($allRes, $allFound);

        $allFound = $this->db->findAndSort(SurveyInterface::COLLECTION, $query, [
            "updated" => -1,
        ], $indexMax);
        foreach ($allFound as $key => $value) {
            $allFound[$key]["type"] = $value["type"];
            $allFound[$key]["typeSig"] = SurveyInterface::CONTROLLER;
        }
        $allRes = array_merge($allRes, $allFound);
        return $allRes;
    }

    //*********************************  VOTES / propositions   ******************************************
    // TODO : $localities is not used
    /**
     * Search proposals based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param mixed $localities The localities to filter the search results.
     * @return array The array of search results.
     */
    public function searchProposals(array $query, int $indexStep, int $indexMin, $localities): array
    {
        $allFound = [];
        $collection = ProposalInterface::COLLECTION;
        array_push($query['$and'], [
            '$or' => [[
                "preferences.private" => false,
            ], [
                "preferences.private" => [
                    '$exists' => false,
                ],
            ]],
        ]);

        array_push($query['$and'], [
            "status" => [
                '$ne' => "abuse",
            ],
        ]);
        $allFound = $this->db->findAndSortAndLimitAndIndex($collection, $query, [
            "updated" => -1,
        ], $indexStep, $indexMin);

        //pour chaque resultat, on ajoute les infos du parentRoom
        foreach ($allFound as $keyS => $survey) {
            if (@$allFound[$keyS]["dateEnd"]) {
                $allFound[$keyS]["dateEnd"] = date("Y-m-d H:i:s", $allFound[$keyS]["dateEnd"]);
            }
            if (@$allFound[$keyS]["endDate"]) {
                $allFound[$keyS]["endDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["endDate"]);
            }
            if (@$allFound[$keyS]["startDate"]) {
                $allFound[$keyS]["startDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["startDate"]);
            }
            if (@$allFound[$keyS]["created"]) {
                $allFound[$keyS]["created"] = date("Y-m-d H:i:s", $allFound[$keyS]["created"]);
            }
            $allFound[$keyS]["type"] = $collection;
            $allFound[$keyS]["voteRes"] = $this->getProposal()->getAllVoteRes($allFound[$keyS]);
            $allFound[$keyS]["hasVote"] = @$allFound[$keyS]["votes"] ? $this->getCooperation()->userHasVoted(@$this->session->getUserId(), $allFound[$keyS]["votes"]) : false;
            if ($allFound[$keyS]["parentType"] && @$allFound[$keyS]["parentType"] != null &&
               @$allFound[$keyS]["parentId"] && @$allFound[$keyS]["parentId"] != null) {
                $allFound[$keyS]["auth"] = $this->getAuthorisation()->canParticipate(@$this->session->getUserId(), @$allFound[$keyS]["parentType"], @$allFound[$keyS]["parentId"]);
            }

            if (! empty($allFound[$keyS]["producer"])) {
                $arrayKey = [];
                foreach ($allFound[$keyS]["producer"] as $k => $v) {
                    $elt = $this->getModelElement()->getElementById($k, $v["type"], null, ["name", "slug", "profilThumbImageUrl"]);
                    if (! empty($elt)) {
                        $allFound[$keyS]["producer"][$k] = array_merge($allFound[$keyS]["producer"][$k], $elt);
                    }
                }
            }
        }
        return $allFound;
    }

    /**
     * Search for votes based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param string $searchType The type of search to perform.
     * @return array The array of search results.
     */
    public function searchVotes(array $query, int $indexStep, int $indexMin, string $searchType): array
    {
        $collection = null;
        $allFound = [];
        if (! empty($this->session->getUserId())) {
            if (in_array(ActionRoomInterface::TYPE_VOTE, $searchType)) {
                $collection = ProposalInterface::COLLECTION;
                $parentRow = "survey";
            } elseif (in_array(ActionRoomInterface::TYPE_ACTIONS, $searchType)) {
                $collection = ActionInterface::NODE_ACTIONS;
                $parentRow = "room";
            }

            $allFound = $this->db->findAndSortAndLimitAndIndex($collection, $query, [
                "updated" => -1,
            ], $indexStep, $indexMin);

            //pour chaque resultat, on ajoute les infos du parentRoom
            foreach ($allFound as $keyS => $survey) {
                if (@$allFound[$keyS]["dateEnd"]) {
                    $allFound[$keyS]["dateEnd"] = date("Y-m-d H:i:s", $allFound[$keyS]["dateEnd"]);
                }
                if (@$allFound[$keyS]["endDate"]) {
                    $allFound[$keyS]["endDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["endDate"]);
                }
                if (@$allFound[$keyS]["startDate"]) {
                    $allFound[$keyS]["startDate"] = date("Y-m-d H:i:s", $allFound[$keyS]["startDate"]);
                }
                if (@$allFound[$keyS]["created"]) {
                    $allFound[$keyS]["created"] = date("Y-m-d H:i:s", $allFound[$keyS]["created"]);
                }
                $allFound[$keyS]["type"] = $collection;
            }
        }
        return $allFound;
    }

    //*********************************  CITIES   ******************************************
    /**
     * Search for cities based on the given parameters.
     *
     * @param string $search The search query.
     * @param string|null $locality The locality to filter the search results by.
     * @param string|null $country The country to filter the search results by.
     * @return array The array of search results.
     */
    public function searchCities(string $search, ?string $locality, ?string $country): array
    {
        $query = [
            "name" => $this->db->MongoRegex("/" . Yii::get('stringHelper')::wd_remove_accents($search) . "/i"),
        ];

        //*********************************  DEFINE LOCALITY QUERY   ******************************************
        if ($locality == null || $locality == "") {
            $locality = $search;
        }

        $type = $this->getTypeOfLocalisation($locality);

        if ($type == "NAME") {
            $query = [
                '$or' => [[
                    "name" => $this->db->MongoRegex("/" . Yii::get('stringHelper')::wd_remove_accents($locality) . "/i"),
                ], [
                    "alternateName" => $this->db->MongoRegex("/" . Yii::get('stringHelper')::wd_remove_accents($locality) . "/i"),
                ], [
                    "postalCodes.name" => [
                        '$in' => [$this->db->MongoRegex("/" . Yii::get('stringHelper')::wd_remove_accents($locality) . "/i")],
                    ],
                ]],
            ];
        }
        if ($type == "CODE_POSTAL_INSEE") {
            $query = [
                "postalCodes.postalCode" => [
                    '$in' => [$locality],
                ],
            ];
        }
        if ($type == "DEPARTEMENT") {
            $query = [
                "dep" => $locality,
            ];
        }
        if ($type == "INSEE") {
            $query = [
                "insee" => $locality,
            ];
        }

        if ($country != "") {
            $query["country"] = $country;
        }

        $allCities = $this->db->find(CityInterface::COLLECTION, $query);
        $allCitiesRes = [];
        $nbMaxCities = 20;
        $nbCities = 0;
        foreach ($allCities as $data) {
            if (! empty($data["postalCodes"])) {
                $countPostalCodeByInsee = is_countable($data["postalCodes"]) ? count($data["postalCodes"]) : 0;
                foreach ($data["postalCodes"] as $val) {
                    if ($nbCities < $nbMaxCities) {
                        $newCity = [];
                        $newCity = [
                            "_id" => $data["_id"],
                            "id" => (string) $data["_id"],
                            "insee" => $data["insee"],
                            "level1" => $data["level1"] ?? "",
                            "level1Name" => $data["level1Name"] ?? "",
                            "country" => $data["country"],
                            "geoShape" => $data["geoShape"] ?? "",
                            "cp" => $val["postalCode"],
                            "postalCodes" => $data["postalCodes"],
                            "geo" => $val["geo"],
                            "geoPosition" => $val["geoPosition"],
                            "name" => ucwords(strtolower($val["name"])),
                            "cityName" => $val["name"],
                            "alternateName" => ucwords(strtolower($val["name"])),
                            "type" => "city",
                            "typeSig" => "city",
                        ];

                        if (! empty($newCity["postalCodes"]) && (is_countable($newCity["postalCodes"]) ? count($newCity["postalCodes"]) : 0) > 1) {
                            foreach ($newCity["postalCodes"] as $key => $v) {
                                $citiesNames[] = $v["name"];
                            }
                            $newCity["cities"] = $citiesNames;
                        } elseif (is_string($newCity["cp"]) && strlen($newCity["cp"]) > 0) {
                            if ($newCity["cp"]) {
                                $where = [
                                    "postalCodes.postalCode" => $this->db->MongoRegex("/^" . $newCity["cp"] . "/i"),
                                    "country" => $newCity["country"],
                                ];
                                $citiesResult = $this->db->find(CityInterface::COLLECTION, $where, ["_id"]);
                                $citiesNames = [];
                                foreach ($citiesResult as $key => $v) {
                                    $citiesNames[] = $this->getModelCity()->getNameCity($key);
                                }
                                $newCity["cities"] = $citiesNames;
                            }
                        }

                        if (! empty($data["level4"])) {
                            $newCity["level4"] = $data["level4"];
                            $newCity["level4Name"] = $data["level4Name"];
                        }

                        if (! empty($data["level3"])) {
                            $newCity["level3"] = $data["level3"];
                            $newCity["level3Name"] = $data["level3Name"];
                        }

                        if (! empty($data["level2"])) {
                            $newCity["level2"] = $data["level2"];
                            $newCity["level2Name"] = $data["level2Name"];
                        }

                        if ($countPostalCodeByInsee > 1) {
                            $newCity["countCpByInsee"] = $countPostalCodeByInsee;
                            $newCity["cityInsee"] = ucwords(strtolower($data["alternateName"]));
                        }
                        $allCitiesRes[(string) $data["_id"]] = $newCity;
                    } $nbCities++;
                }
            } else {
                $data["type"] = "city";
                $data["typeSig"] = "city";
                $allCitiesRes[(string) $data["_id"]] = $data;
            }
        }

        if (empty($allCitiesRes)) {
            $query = [
                "cp" => $search,
            ];
            $allCities = $this->db->find(CityInterface::COLLECTION, $query, ["name", "cp", "insee", "geo", "geoShape"]);
            $nbCities = 0;
            foreach ($allCities as $key => $value) {
                if ($nbCities < $nbMaxCities) {
                    $city = $this->getModelCity()->getSimpleCityById($key);
                    $city["type"] = "city";
                    $city["typeSig"] = "city";
                    $allCitiesRes[(string) $city["_id"]] = $city;
                } $nbCities++;
            }
        }

        return $allCitiesRes;
    }

    /**
     * Removes empty words from the given search string.
     *
     * @param string $search The search string to remove empty words from.
     * @return string The modified search string without empty words.
     */
    public function removeEmptyWords(string $search): string
    {
        $stopwords = [" ", "", "-", "?", "!", ",", ".", "/", "le", "la", "les", "un", "une", "des", "mon", "ton", "son", "pour", "à", "a", "d", "d'", "de", "notre", "votre", "leur", "leurs", "mes", "tes", "ses", "du"];

        $arraySearch = explode(" ", $search);
        $resArraySearch = [];
        foreach ($arraySearch as $key => $word) {
            if (! in_array($word, $stopwords)) {
                $resArraySearch[] = $word;
            }
        }

        $resStr = "";
        foreach ($resArraySearch as $key => $word) {
            if ($resStr != "") {
                $resStr .= " ";
            }
            $resStr .= $word;
        }
        return $resStr;
    }

    // TODO : searchLocalityNetworkOld est ce que c'est encore utilisé ?
    /***********************************  DEFINE LOCALITY QUERY   ***************************************/
    public function searchLocalityNetworkOld(array $query, array $post): array
    {
        $localityReferences = [];
        $localityReferences['NAME'] = "address.addressLocality";
        $localityReferences['CODE_POSTAL_INSEE'] = "address.postalCode";
        $localityReferences['DEPARTEMENT'] = "address.postalCode";
        $localityReferences['REGION'] = ""; //Spécifique
        $localityReferences['INSEE'] = "address.codeInsee";

        foreach ($localityReferences as $key => $value) {
            if (isset($post["searchLocality" . $key]) && is_array($post["searchLocality" . $key])) {
                foreach ($post["searchLocality" . $key] as $locality) {
                    //OneRegion

                    if ($key == "REGION") {
                        if ($locality == "La Réunion") {
                            $locality = "Réunion";
                        }
                        $dep = $this->db->findOne(ZoneInterface::COLLECTION, [
                            "name" => $locality,
                        ], ["name"]);
                        if (! empty($dep)) {
                            $queryLocality = [
                                "address.level3" => (string) $dep["_id"],
                            ];
                        }
                    } elseif ($key == "DEPARTEMENT") {
                        $dep = $this->db->findOne(ZoneInterface::COLLECTION, [
                            "name" => $locality,
                        ], ["name"]);
                        if (! empty($dep)) {
                            $queryLocality = [
                                "address.level4" => (string) $dep["_id"],
                            ];
                        }
                    }//OneLocality
                    else {
                        $queryLocality = [
                            $value => $this->db->MongoRegex("/" . $locality . "/i"),
                        ];
                    }

                    //Consolidate Queries
                    if (! empty($queryLocality)) {
                        if (isset($allQueryLocality)) {
                            $allQueryLocality = [
                                '$or' => [$allQueryLocality, $queryLocality],
                            ];
                        } else {
                            $allQueryLocality = $queryLocality;
                        }
                    }
                    unset($queryLocality);
                }
            }
        }
        if (isset($allQueryLocality) && is_array($allQueryLocality)) {
            $query = [
                '$and' => [$query, $allQueryLocality],
            ];
        }
        return $query;
    }

    /**
     * Search for addresses based on the given parameters.
     *
     * @param array $params The search parameters.
     *
     * @return array|null The array of addresses matching the search parameters, or null if no addresses are found.
     */
    public function address(array $params): ?array
    {
        $res = null;
        if (! empty($params["countryCode"]) &&
            in_array($params["countryCode"], ["FR", "RE", "GP", "GF", "MQ", "YT", "NC" /*, "PM" */])) {
            $data = $this->getModelAdresseDataGouv()->getAddressByDataGouv($params);
            $res = $this->getModelAdresseDataGouv()->parseAddress($data, $params);
        }

        if (empty($res)) {
            $data = $this->getModelNominatim()->getAddressByNominatim($params);
            $res = $this->getModelNominatim()->parseAddress($data, $params);
        }
        return $res;
    }
}
