<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;

use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\ElementTrait;

use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\NotificationTrait;

class Badge extends BaseModel implements BadgeInterface, ElementTraitInterface, AuthorisationTraitInterface, NotificationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;
    use AuthorisationTrait;
    use NotificationTrait;

    public array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "icon" => [
            "name" => "icon",
        ],
        "color" => [
            "name" => "color",
        ],
        "tags" => [
            "name" => "tags",
            "rules" => ["required"],
        ],
        "img" => [
            "name" => "img",
        ],
        "isParcours" => [
            "name" => "isParcours",
            "rules" => ["required"],
        ],
        "parent" => [
            "name" => "parent",
        ],
        "description" => [
            "name" => "description",
        ],
        "criteria" => [
            "name" => "criteria",
        ],
        "synergie" => [
            "name" => "synergie",
        ],
        "structags" => [
            "name" => "structags",
        ],
        "source" => [
            "name" => "source",
        ],
        "category" => [
            "name" => "category",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "isOpenData" => [
            "name" => "isOpenData",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "issuer" => [
            "name" => "issuer",
        ],
        "assertion" => [
            "name" => "assertion",
        ],
        "badgeClass" => [
            "name" => "badgeClass",
        ],
        "type" => [
            "name" => "type",
        ],
        "input" => [
            "name" => "input",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    public function getById($id)
    {
        $badge = $this->db->findOne(BadgeInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);

        if (! empty($badge)) {
            $badge["typeSig"] = "badges";
        }
        return $badge;
    }

    public function getBagdes($idItem, $typeItem)
    {
        $badges = [];
        $account = $this->db->findOneById($typeItem, $idItem, ["badges"]);
        if (! empty($account["badges"])) {
            $badges = $account["badges"];
        }
        return $badges;
    }

    public function getByWhere($where, $img = false)
    {
        return $this->db->find(BadgeInterface::COLLECTION, $where);
    }

    public function getOneByWhere($where, $img = false)
    {
        return $this->db->findOne(BadgeInterface::COLLECTION, $where);
    }

    public function getBadgesByLinks($links)
    {
        $badges = [];
        foreach ($links as $sec => $l) {
            foreach ($l as $id => $v) {
                if (@$v["type"]) {
                    $el = $this->getModelElement()->getByTypeAndId($v["type"], $id);
                    if (@$el["preferences"]["badge"]) {
                        $b = [
                            "name" => $el["name"],
                            "type" => $v["type"],
                        ];
                        if (! empty($el["profilThumbImageUrl"])) {
                            $b["img"] = $el["profilThumbImageUrl"];
                        }
                        $badges[] = $b;
                    }
                }
            }
        }

        return $badges;
    }

    public function checkBadgeInListBadges($badge, $badges)
    {
        $res = false;
        return (@$badges[$badge]) ? true : false;
    }

    public function addBadgeInListBadges($badge, $badges)
    {
        $newBadge = [];
        $res = [
            "result" => false,
            "badges" => $badges,
            "msg" => $this->language->t("import", "Le badge est déjà dans la liste"),
        ];
        if (is_array($badge)) {
            $newListBadges = [];
            foreach ($badge as $key => $value) {
                if (! $this->checkBadgeInListBadges((empty($value["name"]) ? $value : $value["name"]), $badges)) {
                    $newBadge["name"] = (empty($value["name"]) ? $value : $value["name"]);
                    $newBadge["date"] = (empty($value["date"]) ? $this->db->MongoDate(time()) : $value["date"]);
                    $newListBadges[] = $newBadge;
                }
            }
            $badges = array_merge($badges, $newListBadges);
            $res = [
                "result" => true,
                "badges" => $badges,
            ];
        } elseif (is_string($badge)) {
            if (! $this->checkBadgeInListBadges($badge, $badges)) {
                $newBadge["name"] = $badge;
                $newBadge["date"] = $this->db->MongoDate(time());
                $badges[] = $newBadge;
                $res = [
                    "result" => true,
                    "badges" => $badges,
                ];
            }
        }
        return $res;
    }

    public function updateBadges($badges, $idItem, $typeItem)
    {
        return $this->getModelElement()->updatePathValue($typeItem, $idItem, "badges", $badges);
    }

    public function addAndUpdateBadges($nameBadge, $idItem, $typeItem)
    {
        $badges = $this->getBagdes($idItem, $typeItem);
        if (empty($badges)) {
            $badges = [];
        }

        $resAddBadge = $this->addBadgeInListBadges($nameBadge, $badges);
        if ($resAddBadge["result"] == true) {
            $res = $this->updateBadges($resAddBadge["badges"], $idItem, $typeItem);
        } else {
            $res = [
                "result" => false,
                "msg" => $resAddBadge["msg"],
            ];
        }

        return $res;
    }

    public function conformeBadges($badges)
    {
        $newBadge = [];
        $newListBadges = [];
        if (is_array($badges)) {
            foreach ($badges as $key => $value) {
                $newBadge["name"] = (empty($value["name"]) ? $value : $value["name"]);
                $newBadge["date"] = (empty($value["date"]) ? $this->db->MongoDate(time()) : $value["date"]);
                $newListBadges[] = $newBadge;
            }
        } elseif (is_string($badges)) {
            $newBadge["name"] = $badges;
            $newBadge["date"] = $this->db->MongoDate(time());
            $newListBadges[] = $newBadge;
        }
        return $newListBadges;
    }

    public function delete($nameBadge, $idItem, $typeItem)
    {
        $badges = $this->getBagdes($idItem, $typeItem);
        $newBadges = [];
        foreach ($badges as $key => $badge) {
            if ($badge["name"] != $nameBadge) {
                $newBadges[] = $badge;
            }
        }
        $res = $this->updateBadges($newBadges, $idItem, $typeItem);
        return $res;
    }

    public function getBadgeChildrenTree($idBadge, $badgeAssigned = null)
    {
        $badge = $this->getById((string) $idBadge);
        if ($badge != null) {
            $badge["id"] = (string) $badge["_id"];
            $badge = self::dfsChildrenTree($badge, $badgeAssigned);
            return $badge;
        } else {
            return [];
        }
    }

    private function dfsChildrenTree($badge, $badgeAssigned = null)
    {
        if (! empty($badge)) {
            $children = $this->db->find(BadgeInterface::COLLECTION, [
                '$and' => [[
                    'parent.' . ((string) $badge["_id"]) => [
                        "\$exists" => true,
                    ],
                ], [
                    "_id" => [
                        '$ne' => $this->db->MongoId((string) $badge["_id"]),
                    ],
                ]],
            ]);
            if (! empty($children)) {
                $count = 0;
                $childrenLocked = 0;
                foreach ($children as $id => $child) {
                    $child["id"] = (string) $id;
                    $children[$id] = static::dfsChildrenTree($child, $badgeAssigned);
                    $count += $children[$id]["childrenCount"] + 1;
                    $childrenLocked += $children[$id]["childrenLocked"];
                    if (! empty($badgeAssigned)) {
                        if (! in_array((string) $id, $badgeAssigned)) {
                            $children[$id]["locked"] = true;
                            $childrenLocked++;
                        }
                    }
                }
                $badge["children"] = array_values($children);
                $badge["childrenCount"] = $count;
                $badge["childrenLocked"] = $childrenLocked;
            } else {
                $badge["childrenCount"] = 0;
                $badge["childrenLocked"] = 0;
            }
        }
        return $badge;
    }

    public function categorizeBadge($badges, $category)
    {
        $badgesOption = [];
        foreach ($badges as $kbg => $vbg) {
            if (isset($vbg["parent"])) {
                $parentId = array_keys($vbg["parent"])[0];
                $parent = $this->db->findOneById(BadgeInterface::COLLECTION, $parentId);
            } elseif (! isset($vbg["parent"])) {
                $parentId = $kbg;
                $parent = $vbg;
            }
            $bdg = $this->db->find(BadgeInterface::COLLECTION, [
                "parent." . $parentId => [
                    '$exists' => true,
                ],
                "category" => $category,
            ], ["name"]);
            $badgeName = [];
            foreach ($bdg as $kbdg => $vbdg) {
                $badgeName[] = $vbdg["name"];
            }
            $badgesOption[@$parent["name"]] = $badgeName;
        }
        return $badgesOption;
    }

    public function canEdit($id, $userId = null)
    {
        $userId ??= $this->session->getUserId();
        if (empty($userId)) {
            return false;
        }
        $badge = $this->db->findOne(BadgeInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ], ["issuer"]);
        if (empty($badge)) {
            return false;
        }
        $typeIssuer = null;
        $idIssuer = null;
        foreach ($badge['issuer'] as $key => $issuer) {
            $typeIssuer = $issuer['type'];
            $idIssuer = $key;
        }
        return $this->getModelAuthorisation()->canEditItem($userId, $typeIssuer, $idIssuer);
    }

    public function afterSave($badge)
    {
        $target = [];

        if (isset($badge["issuer"])) {
            foreach ($badge["issuer"] as $id => $issuer) {
                $target = [
                    "id" => $id,
                    "type" => $issuer["type"],
                ];
                if (isset($issuer["name"])) {
                    $target["name"] = $issuer["name"];
                }
            }
        }

        $this->getModelNotification()->constructNotification(
            ActStrInterface::VERB_ADD,
            [
                "id" => $this->session->getUserId(),
                "name" => $this->session->getUserName(),
                "type" => PersonInterface::COLLECTION,
            ],
            $target,
            [
                "type" => BadgeInterface::COLLECTION,
                "id" => (string) $badge["_id"],
                "name" => $badge["name"],
            ],
            BadgeInterface::COLLECTION
        );
    }

    public function cleanAllElementsLinks($id)
    {
        //REMOVE ASSIGNATION
        $this->db->update(OrganizationInterface::COLLECTION, [
            'badges.' . $id => [
                '$exists' => true,
            ],
        ], [
            '$unset' => [
                'badges.' . $id => true,
            ],
        ]);
        $this->db->update(PersonInterface::COLLECTION, [
            'badges.' . $id => [
                '$exists' => true,
            ],
        ], [
            '$unset' => [
                'badges.' . $id => true,
            ],
        ]);
        $this->db->update(ProjectInterface::COLLECTION, [
            'badges.' . $id => [
                '$exists' => true,
            ],
        ], [
            '$unset' => [
                'badges.' . $id => true,
            ],
        ]);
        $this->db->update(BadgeInterface::COLLECTION, [
            'parent.' . $id => [
                '$exists' => true,
            ],
        ], [
            '$unset' => [
                'parent.' . $id => true,
            ],
        ]);
        //TODO: CLEAN ENDORSSEMENT
    }
}
