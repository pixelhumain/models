<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ExportInterface;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

// TODO : ArrayHelper::getAllPathJson
// TODO : Rest::json ne doit pas avoir dans les modeles

class Export extends BaseModel implements ExportInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    /**
     * Converts an array of data to a CSV string and outputs it to the file system.
     *
     * @param array $data The data to be converted.
     * @param string $separ The separator character for the CSV fields. Default is ";".
     * @param string $separText The separator character for text values within the CSV fields. Default is " ".
     * @param bool $notPath Whether to exclude file paths from the CSV. Default is false.
     * @param bool $niv1 Whether to include the first level of the array in the CSV. Default is true.
     * @param mixed $type The type of data to be exported. Default is null.
     * @param array $order The order in which the data should be exported. Default is an empty array.
     * @return void
     */
    public function toCSV(array $data, string $separ = ";", string $separText = " ", bool $notPath = false, bool $niv1 = true, $type = null, array $order = []): void
    {
        $data = $this->getMongoParam($data);

        $allPath = [];
        $ind = [];
        foreach ($data as $key1 => $value1) {
            $value1 = json_encode($value1);
            $ind[] = $key1;
            if (substr($value1, 0, 1) == "{") {
                $allPath = ArrayHelper::getAllPathJson($value1, $allPath, $niv1);
            } else {
                if (empty($allPath)) {
                    $allPath = [];
                }
                foreach (json_decode($value1, true) as $key => $value) {
                    if ($value != null) {
                        $allPath = ArrayHelper::getAllPathJson(json_encode($value), $allPath, $niv1);
                    }
                }
            }
        }

        if (! empty($order)) {
            $newAllPath = [];
            foreach ($order as $keyO => $valO) {
                foreach ($allPath as $keyPath => $valPath) {
                    $o = explode(".", $valPath);

                    if ($o[0] == $valO) {
                        $newAllPath[] = $valPath;
                    }
                }
            }

            $allPath = $newAllPath;
        }

        $res = [];

        foreach ($allPath as $key2 => $value2) {
            foreach ($data as $key1 => $value1) {
                $i = array_search($key1, $ind);
                if (empty($res[$i])) {
                    $res[$i] = [];
                }
                $valPath = ArrayHelper::getValueByDotPath($value1, $value2);

                if ($valPath != null) {
                    if (gettype($valPath) == "array") {
                        array_push($res[$i], implode(";", $valPath));
                    } else {
                        $valPath = str_replace(";", ".", $valPath);
                        $valPath = str_replace('"', "'", $valPath);
                        array_push($res[$i], $valPath);
                    }
                } else {
                    array_push($res[$i], " ");
                }
            }
        }

        $fp = fopen('php://output', 'w');
        fputcsv($fp, $allPath);
        foreach ($res as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    /**
     * Retrieves the member of a specific type for a given ID.
     *
     * @param string $id The ID of the member.
     * @param string $type The type of the member.
     * @return array The array of member data.
     */
    public function getMemberOf(string $id, string $type): array
    {
        $data = $this->getModelElement()->getByTypeAndId($type, $id);

        $list_orga = [];

        foreach ($data["links"]["memberOf"] as $key => $value) {
            $orga = $this->getModelElement()->getByTypeAndId($value['type'], $key);
            $list_orga[] = $orga;
        }
        return $list_orga;
    }

    /**
     * Reformats the data fields mongo to be exported.
     *
     * @param array $data The data to be reformatted.
     * @return array The reformatted data.
     */
    public function getMongoParam(array $data): array
    {
        foreach ($data as $key => $value) {
            if (isset($value["_id"])) {
                $data[$key]['_id'] = (string) $value["_id"];
            }

            if (isset($value['modified'])) {
                $data[$key]['modified'] = (string) $value['modified'];
            }
        }

        return $data;
    }

    /**
     * Converts an array of data to a CSV string.
     *
     * @param array $data The data to be converted.
     * @param string $separ The separator character for the CSV fields. Default is ";".
     * @param string $separText The separator character for text values within the CSV fields. Default is " ".
     * @param bool $notPath Indicates whether to exclude file paths from the CSV. Default is false.
     * @param bool $niv1 Indicates whether to include the first level of the array in the CSV. Default is true.
     * @param mixed $type The type of data to be exported. Default is null.
     * @param array $order The order in which the data should be exported. Default is an empty array.
     * @return mixed The CSV.
     */
    public function toCSV2(array $data, string $separ = ";", string $separText = " ", bool $notPath = false, bool $niv1 = true, $type = null, array $order = [])
    {
        ini_set('max_execution_time', 10_000_000);

        $data = $this->getMongoParam($data);

        $allPath = [];
        $ind = [];
        foreach ($data as $key1 => $value1) {
            $value1 = json_encode($value1);
            $ind[] = $key1;
            if (substr($value1, 0, 1) == "{") {
                $allPath = ArrayHelper::getAllPathJson($value1, $allPath, $niv1);
            } else {
                if (empty($allPath)) {
                    $allPath = [];
                }
                foreach (json_decode($value1, true) as $key => $value) {
                    if ($value != null) {
                        $allPath = ArrayHelper::getAllPathJson(json_encode($value), $allPath, $niv1);
                    }
                }
            }
        }

        if (! empty($order)) {
            $newAllPath = [];
            foreach ($order as $keyO => $valO) {
                foreach ($allPath as $keyPath => $valPath) {
                    $o = explode(".", $valPath);

                    if ($o[0] == $valO) {
                        $newAllPath[] = $valPath;
                    }
                }
            }

            $allPath = $newAllPath;
        }

        $res = [];

        foreach ($allPath as $key2 => $value2) {
            foreach ($data as $key1 => $value1) {
                $i = array_search($key1, $ind);
                if (empty($res[$i])) {
                    $res[$i] = [];
                }
                $valPath = ArrayHelper::getValueByDotPath($value1, $value2);

                if ($valPath != null) {
                    if (gettype($valPath) == "array") {
                        array_push($res[$i], implode(";", $valPath));
                    } else {
                        $valPath = str_replace(";", ".", $valPath);
                        $valPath = str_replace('"', "'", $valPath);
                        array_push($res[$i], $valPath);
                    }
                } else {
                    array_push($res[$i], " ");
                }
            }
        }
        return Rest::json($res);
    }
}
