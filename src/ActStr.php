<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\PersonInterface;

use PixelHumain\Models\Traits\ActivityStreamTrait;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\Interfaces\ActivityStreamTraitInterface;

// Todo est ce que j'ai besoin de BaseModel dessus cette classe ? car je n'utilise pas les fonctions de cette classe
class ActStr extends BaseModel implements ActStrInterface, ActivityStreamTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ActivityStreamTrait;

    //All events taht can be loggued into the activity stream

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
    }

    // Todo une methode avec le même nom existe dans le model ActivityStream, est ce qu'il faudrait la rapatrier dans ce model en la renommant ?
    /**
     * Builds an entry based on the given parameters.
     *
     * @param array $params The parameters used to build the entry.
     * @return array The built entry.
     */
    public function buildEntry(array $params): array
    {
        $action = [
            "type" => $params["type"],
            "verb" => $params["verb"],
            "author" => $this->session->getUserId(),
            "created" => $this->db->MongoDate(time()),
            "updated" => $this->db->MongoDate(time()),
        ];
        if (@$params["author"]) {
            if (! @$params["author"]["id"]) {
                $action["author"] = $params["author"];
            } elseif (@$params["author"]["name"]) {
                $action["author"] = [
                    $params["author"]["id"] => [
                        "name" => $params["author"]["name"],
                    ],
                ];
            }
        }
        if (isset($params["ip"])) {
            $action["ip"] = $params["ip"];
        }

        if (isset($params["object"])) {
            $action["object"] = $params["object"];
        }

        if (isset($params["target"])) {
            $action["target"] = [
                "type" => $params["target"]['type'],
                "id" => $params["target"]['id'],
            ];
            if (@$params["target"]["parent"]) {
                $action["target"]["parent"] = $params["target"]["parent"];
            }
        }

        if ($params["type"] == ActivityStreamInterface::COLLECTION) {
            $action["scope.type"] = "public";
            if (isset($params["cities"])) {
                $action["scope"]["cities"] = $params["cities"];
            }
            if (isset($params["geo"])) {
                $action["scope"]["geo"] = $params["geo"];
            }
        }
        if (isset($params["label"])) {
            $action["object"]["displayName"] = $params["label"];
        }
        if (isset($params["tags"])) {
            $action["tags"] = $params["tags"];
        }
        return $action;
    }

    // Todo c'est lier à ActivityStream peut être qu'il faudrait le déplacer dans ce model (utilise la fonction buildEntry)
    /**
     * Affiche une page à partir d'une URL.
     *
     * @param string $url L'URL de la page à afficher.
     * @return void
     */
    public function viewPage(string $url): void
    {
        $asParam = [
            "type" => ActStrInterface::VIEW_PAGE,
            "verb" => ActStrInterface::VERB_VIEW,
            "actorType" => PersonInterface::COLLECTION,
            "object" => [
                "type" => ActStrInterface::TYPE_URL,
                "id" => $url,
            ],
            "ip" => $_SERVER['REMOTE_ADDR'],
        ];
        $action = $this->buildEntry($asParam);
        $this->getModelActivityStream()->addEntry($action);
    }


    // // Todo function to be removed
    // public function getParamsByVerb($verb,$ctrl,$target,$currentUser)
    // {
    //     $res = false;
    //     $verbParams = array(
    //         ActStr::VERB_CLOSE => array("label" => $target["name"]." ".$this->language->t("common","has been disabled by")." ".$currentUser['name'],
    //                                     "url"   => $ctrl.'/detail/id/'.$target["id"]),
    //         ActStr::VERB_POST => array("label"  => $target["name"]." : ".$this->language->t("common","new post by")." ".$currentUser['name'],
    //                                     "url"   => 'news/index/type/'.$target["type"].'/id/'.$target["id"]),
    //         ActStr::VERB_FOLLOW => array("label" => $currentUser['name'],
    //                                     "url"   => Person::CONTROLLER.'/detail/id/'.$this->session->getUserId()),
    //        ActStr::VERB_FOLLOW_AP => array("label" => $currentUser['name'],
    //                                     "url"   => Person::CONTROLLER.'/detail/id/'.$this->session->getUserId()),
    //         ActStr::VERB_WAIT => array("label" => $currentUser['name']." ".$this->language->t("common","wants to join")." ".$target["name"],
    //                                     "url"   => $ctrl.'/directory/id/'.$target["id"].'?tpl=directory2'),
    //         ActStr::VERB_AUTHORIZE => array("label" => $currentUser['name']." ".$this->language->t("common","wants to administrate")." ".$target["name"],
    //                                     "url"   => $ctrl.'/directory/id/'.$target["id"].'?tpl=directory2'),
    //         ActStr::VERB_JOIN => array("label" => $currentUser['name']." ".$this->language->t("common","participates to the event")." ".$target["name"],
    //                                     "url"   => 'news/detail/id/'.$target["id"]),
    //         ActStr::VERB_COMMENT => array("label" => $currentUser['name']." ".$this->language->t("common","has commented your post"),
    //                                         "url"   => $ctrl.'/detail/id/'.$target["id"])
    //     );

    //     if( isset( $verbParams[$verb]) )
    //     {
    //         $res = $verbParams[$verb];
    //         if( $verb == ActStr::VERB_FOLLOW || $verb == ActStr::VERB_FOLLOW_AP )
    //         {
    //             $specificLab = $this->language->t("common","is following")." ".$target["name"];
    //             if($target["type"]==Person::COLLECTION)
    //                 $specificLab = $this->language->t("common","is following you");

    //             $res["label"] = $res["label"]." ".$specificLab;
    //         }
    //     }

    //     return $res;
    // }
}
