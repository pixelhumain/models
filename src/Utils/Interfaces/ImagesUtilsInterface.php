<?php

namespace PixelHumain\Models\Utils\Interfaces;

interface ImagesUtilsInterface
{
    /**
     * Display method.
     *
     * @return void
     */
    public function display(): void;

    /**
     * Saves an image to the specified destination path with the given quality.
     *
     * @param string $destImagePath The destination path where the image will be saved.
     * @param string $quality The quality of the saved image (default is 100).
     * @return void
     */
    public function save(string $destImagePath, string $quality = "100"): void;

    /**
     * Saves an image as a PNG file.
     *
     * @param string $destImagePath The path where the PNG file will be saved.
     * @param string $quality The quality of the PNG file (optional, default is "100").
     * @return void
     */
    public function savePng(string $destImagePath, string $quality = "100"): void;

    /**
     * Resize the image to the specified dimensions.
     *
     * @param int $newwidth The new width of the image.
     * @param int $newheight The new height of the image.
     * @return ImagesUtilsInterface The resized image.
     */
    public function resizeImage(int $newwidth, int $newheight): ImagesUtilsInterface;

    /**
     * Resizes and crops an image.
     *
     * @param int $newwidth The new width of the image.
     * @param int $newheight The new height of the image.
     * @param array $crop The crop parameters for the image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizeAndCropImage(int $newwidth = 1300, int $newheight = 400, array $crop): ImagesUtilsInterface;

    /**
     * Crop the image to the specified dimensions.
     *
     * @param int $new_width The width of the cropped image.
     * @param int $new_height The height of the cropped image.
     * @param int $x The x-coordinate of the starting point for cropping. Default is 0.
     * @param int $y The y-coordinate of the starting point for cropping. Default is 0.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function imagecropping(int $new_width, int $new_height, int $x = 0, int $y = 0): ImagesUtilsInterface;

    /**
     * Resize an image proportionally.
     *
     * @param int $newwidth The desired width of the resized image.
     * @param int $newheight The desired height of the resized image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizePropertionalyImage(int $newwidth, int $newheight): ImagesUtilsInterface;

    /**
     * Creates a circle image with the specified dimensions.
     *
     * @param int $newwidth The width of the new image.
     * @param int $newheight The height of the new image.
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function createCircleImage(int $newwidth, int $newheight): ImagesUtilsInterface;

    /**
     * Crée un marqueur à partir d'une image.
     *
     * @param string $srcEmptyMarker Le chemin de l'image du marqueur vide.
     * @return ImagesUtilsInterface Une instance de l'interface ImagesUtilsInterface.
     */
    public function createMarkerFromImage(string $srcEmptyMarker): ImagesUtilsInterface;

    /**
     * Transforms transparency in the image to white.
     *
     * @return void
     */
    public function transformTransparencyToWhite(): void;

    /**
     * Resizes the image into a badge.
     *
     * @return ImagesUtilsInterface The instance of the ImagesUtilsInterface.
     */
    public function resizeIntoBadge(): ImagesUtilsInterface;
}
