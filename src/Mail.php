<?php

namespace PixelHumain\Models;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CronInterface;
use PixelHumain\Models\Interfaces\ElementInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\MailInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;

use PixelHumain\Models\Services\Interfaces\MailerServiceInterface;
use PixelHumain\Models\Services\Interfaces\ViewServiceInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\BaseModel\UrlHelperTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\CronTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CronTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsLetterTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NewsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\RoleTraitInterface;

use PixelHumain\Models\Traits\NewsLetterTrait;
use PixelHumain\Models\Traits\NewsTrait;
use PixelHumain\Models\Traits\NotificationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\RoleTrait;

class Mail extends BaseModel implements MailInterface, CostumTraitInterface, NewsLetterTraitInterface, CronTraitInterface, ElementTraitInterface, PersonTraitInterface, DataValidatorTraitInterface, NewsTraitInterface, NotificationTraitInterface, RoleTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;
    use UrlHelperTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use NewsLetterTrait;
    use CronTrait;
    use ElementTrait;
    use PersonTrait;
    use DataValidatorTrait;
    use NewsTrait;
    use NotificationTrait;
    use RoleTrait;

    protected MailerServiceInterface $mailer;

    protected ViewServiceInterface $view;

    public string $templatePathEmail = "@app/views/emails/";

    public string $applicationName = "Communecter";

    public string $applicationLogo = "/images/logoLTxt.jpg";

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
        $this->validateUrlHelperProperty();

        if (! $this->mailer instanceof MailerServiceInterface) {
            throw new InvalidArgumentException("mailer must be an instance of " . MailerServiceInterface::class . " (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        if (! $this->view instanceof ViewServiceInterface) {
            throw new InvalidArgumentException("view must be an instance of " . ViewServiceInterface::class . " (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }

        if ($this->params->get('applicationName')) {
            $this->applicationName = $this->params->get('applicationName');
        }
        if ($this->params->get('templatePathEmail')) {
            $this->templatePathEmail = $this->params->get('templatePathEmail');
        }
        if ($this->params->get('applicationLogo')) {
            $this->applicationLogo = $this->params->get('applicationLogo');
        }
    }

    /**
     * Crée et envoie un email.
     *
     * @param array $params Les paramètres de l'email.
     * @return void
     */
    public function createAndSend(array $params): void
    {
        $costum = $this->getModelCostum()->getCostum();
        if (empty($params["tplMail"]) && is_array($costum) && ! empty($costum["admin"]["email"])) {
            $params["tplMail"] = $costum["admin"]["email"];
        }
        if (! empty($params["tplMail"])) {
            if (is_string($params["tplMail"]) && strpos($params["tplMail"], ",") != false) {
                $params["tplMail"] = explode(",", $params["tplMail"]);
            }

            if (is_array($params["tplMail"])) {
                foreach ($params["tplMail"] as $v) {
                    $params["tplMail"] = $v;
                    $this->createAndSendSingle($params);
                }
            } else {
                $this->createAndSendSingle($params);
            }
        } else {
            throw new CTKException($this->language->t("common", "Missing email!"));
        }
    }

    /**
     * Crée et envoie un seul e-mail.
     *
     * @param array $params Les paramètres de l'e-mail.
     * @return void
     */
    public function createAndSendSingle(array $params): void
    {
        if (! empty($params["tplMail"])) {
            $fromMail = (isset($params["fromMail"]) && ! empty($params["fromMail"])) ? $params["fromMail"] : null;
            $res = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => $params["tpl"],
                "subject" => $params["tplObject"],
                "from" => $this->params->get('adminEmail'),
                "to" => $params["tplMail"],
                "tplParams" => $this->initTplParams($params),
            ];
            $res = $this->getCustomMail($res, $fromMail);
            $this->schedule($res);
        } else {
            throw new CTKException($this->language->t("common", "Missing email!"));
        }
    }

    /**
     * Initializes the template parameters for the Mail class.
     *
     * @param array $params The array of parameters to initialize.
     * @return array The initialized array of parameters.
     */
    public function initTplParams(array $params): array
    {
        $tplP = [
            "logo" => $this->params->get("logoUrl"),
            "logo2" => $this->params->get("logoUrl2"),
            "baseUrl" => $this->baseUrl . "/",
        ];

        $res = array_merge($tplP, $params);
        return $res;
    }

    /**
     * Sends an email.
     *
     * @param array $params The parameters for the email.
     * @param bool $force (optional) Whether to force sending the email even if it's disabled.
     * @return bool|array Whether the email was sent or not.
     */
    public function send(array $params, bool $force = false)
    {
        $account = null;

        if (! empty($params['to'])) {
            if ($params['to'] != $this->params->get('adminEmail')) {
                $valid = $this->authorizationMail($params['to']);

                if ($valid == false && $params['tpl'] != "askdata") {
                    return [
                        "result" => false,
                        "msg" => $this->language->t("common", "You cannot enter this email address : ") . $params['to'],
                    ];
                } else {
                    $listTpl = ["referenceEmailInElement", "basic"];
                    if (! empty($params['tpl']) && in_array($params['tpl'], $listTpl)) {
                        $nameTo = ((! empty($params['tplParams']) && ! empty($params['tplParams']["name"])) ? $params['tplParams']["name"] : $params['to']);
                    } else {
                        $tpl = ["removeData", "askdata"];
                        if (! in_array($params['tpl'], $tpl)) {
                            $account = $this->db->findOne(PersonInterface::COLLECTION, [
                                "email" => $params['to'],
                            ]);
                            if (! empty($account)) {
                                if (@$account["isNotValidEmail"]) {
                                    $msg = "Try to send an email to a not valid email user : " . $params['to'];
                                    return [
                                        "result" => false,
                                        "msg" => $msg,
                                    ];
                                } elseif (@$account["status"] == "deleted") {
                                    $msg = "Try to send an email to a deleted user : " . $params['to'];
                                    return [
                                        "result" => false,
                                        "msg" => $msg,
                                    ];
                                }
                            } else {
                                $msg = "Try to send an email to an unknown email user : " . $params['to'];
                                return [
                                    "result" => false,
                                    "msg" => $msg,
                                ];
                            }
                        } else {
                            $nameTo = $params['to'];
                        }
                    }
                }
            } else {
                $nameTo = "Admin Communecter";
            }
        } else {
            return false;
        }

        if ($account) {
            $nameTo = $account["name"];
        }

        if ($this->notlocalServer() || $force) {
            $message = $this->mailer->compose();
            $params['to'] = $this->params->get('mailTrapTo') ?: $params['to'];
            $expeditor = $params["expeditor"] ?? "Communecter";
            $to = (isset($nameTo)) ? [
                $params['to'] => $nameTo,
            ] : $params['to'];

            $message->setFrom([
                $params['from'] => $expeditor,
            ]);
            $message->setTo($to);
            $message->setSubject($params['subject']);

            if (! empty($params['output'])) {
                $output = $params['output'];
            } else {
                $output = $this->renderTemplate($params);
            }

            $message->setHtmlBody($output);

            // TODO : integrer le changement de transport si present source.key avec dans la collection configSmtp

            try {
                return $this->mailer->send($message);
            } catch (Exception $exception) {
                // TODO : log
                // Yii::getLogger()->log($exception->getMessage(), \yii\log\Logger::LEVEL_ERROR, __METHOD__);
                return [
                    "result" => false,
                    "msg" => "Problem sending Email : " . $exception->getMessage(),
                ];
            }
        } else {
            return false;
        }
    }

    /**
     * Renders the template with the given parameters.
     *
     * @param array $params The parameters to be passed to the template.
     * @return string The rendered template.
     */
    public function renderTemplate(array $params): string
    {
        if ($params["tpl"] === "newsletter") {
            $output = $this->getModelNewsLetter()->getNewsLetterPage($params["tplParams"]["newsletter"]);
        } else {
            $objectView = $this->view->createView();
            $objectView->setModule($this->params->get("module")["parent"]);
            $file = $objectView->findViewFile($this->templatePathEmail . $params['tpl']);
            // verifie si le fichier existe pas et que le nom du fichier contient un point
            // si oui on sait que c'est la version de nommage de l'ancien systeme Yii1
            if (! file_exists($file) && strpos($params['tpl'], '.')) {
                $output = $objectView->renderView($params['tpl'], $params['tplParams']);
            } else {
                $output = $objectView->renderView($this->templatePathEmail . $params['tpl'], $params['tplParams']);
            }
        }
        return $output;
    }

    public function notlocalServer()
    {
        return (stripos($_SERVER['SERVER_NAME'], "127.0.0.1") === false && stripos($_SERVER['SERVER_NAME'], "localhost:8080") === false);
    }

    /**
     * Schedule a mail.
     *
     * @param array $params The parameters for the mail.
     * @param mixed $update The update parameter (optional).
     * @return void
     */
    public function schedule(array $params, $update = null)
    {
        $this->cron->save($params, $update);
    }

    // TODO : je crois que c'est plus utilisé, elle est commentée partout dans le code
    /**
     * Sends a notification email to the admin about a new user.
     *
     * @param array $person The details of the new user.
     * @return void
     */
    public function notifAdminNewUser(array $person): void
    {
        $data = [];
        $mail = $this->getMailUpdate($this->params->get('adminEmail'), 'notifAdminNewUser');

        if (! empty($mail)) {
            $mail["tplParams"]["data"][] = $person;
            $this->db->update(
                CronInterface::COLLECTION,
                [
                    "_id" => $mail["_id"],
                ],
                [
                    '$set' => [
                        "tplParams" => $mail["tplParams"],
                    ],
                ]
            );
        } else {
            $data[] = $person;
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_notifAdminNewUser,
                "subject" => $this->language->t("mail", 'New user on {website}', [
                    "{website}" => $this->getAppName(),
                ]),
                "from" => $this->params->get('adminEmail'),
                "to" => $this->params->get('adminEmail'),
                "tplParams" => [
                    "data" => $data,
                    "title" => $this->applicationName,
                    "logo" => $this->applicationLogo,
                ],
            ];
            $params = $this->getCustomMail($params);
            $this->schedule($params, true);
        }
    }

    /**
        * References the email in a specified element.
        *
        * @param string $collection The name of the collection.
        * @param string $id The identifier of the element.
        * @param string $email The email to reference.
        * @return void
        */
    public function referenceEmailInElement(string $collection, string $id, string $email): void
    {
        $element = $this->element->getElementSimpleById($id, $collection, null, ["name"]);
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_referenceEmailInElement,
            "subject" => $this->language->t("mail", "{who} added your contact in {where} on {website}", [
                "{who}" => $this->session->getUserName(),
                "{where}" => $this->language->t("common", "the " . $this->element->getControlerByCollection($collection)) . " " . $element["name"],
                "{website}" => $this->getAppName(),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $email,
            "tplParams" => [
                "collection" => $collection,
                "id" => $id,
                "name" => $element["name"],
                "invitorId" => $this->session->getUserId(),
                "invitorName" => $this->session->getUserName(),
                "title" => $this->applicationName,
                "language" => $this->language->get(),
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Sends a notification email to the admin about a new professional.
     *
     * @param array $person The details of the new professional.
     * @return void
     */
    public function notifAdminNewPro(array $person): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_notifAdminNewPro,
            "subject" => $this->language->t("mail", 'New professional account on {website}', [
                "{website}" => $this->getAppName(),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $this->params->get('adminEmail'),
            "tplParams" => [
                "person" => $person,
                "title" => $this->applicationName,
                "logo" => $this->applicationLogo,
            ],
        ];
        $this->schedule($params);
    }

    /**
     * Sends a notification to the admin about a new reservation.
     *
     * @param array $params The parameters for the notification.
     * @return void
     */
    public function notifAdminNewReservation(array $params): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_reservations,
            "subject" => $this->language->t("terla", 'Réservation de ' . $this->session->getUserName() . ' pour le circuit ' . $params["name"]),
            "from" => $this->params->get('adminEmail'),
            "to" => $this->params->get('adminEmail'),
            "tplParams" => [
                "order" => $params,
                "title" => $this->language->t("terla", 'Réservation de ' . $this->session->getUserName() . ' pour le circuit ' . $params["name"]),
                "logo" => $this->params->get("logoUrl"),
                "logo2" => $this->params->get("logoUrl2"),
            ],
        ];
        $this->schedule($params);
    }

    /**
     * Invite a person via email.
     *
     * @param array $person The person to invite.
     * @param string|null $msg The invitation message (optional).
     * @param string|null $nameInvitor The name of the invitor (optional).
     * @param string|null $invitorUrl The URL of the invitor (optional).
     * @param string|null $subject The subject of the invitation email (optional).
     * @return void
     */
    public function invitePerson(array $person, ?string $msg = null, ?string $nameInvitor = null, ?string $invitorUrl = null, ?string $subject = null): void
    {
        $invitor = [];
        if (isset($person["invitedBy"])) {
            $invitor = $this->person->getSimpleUserById($person["invitedBy"]);
        } elseif (isset($nameInvitor)) {
            $invitor["name"] = $nameInvitor;
        }

        if (empty($subject)) {
            if (@$invitor && empty(@$invitor["name"])) {
                $subject = $this->language->t("mail", "{who} is waiting for you on {what}", [
                    "{who}" => $invitor["name"],
                    "{what}" => $this->getAppName(),
                ]);
            } else {
                $subject = $this->language->t("mail", "{what} is waiting for you", [
                    "{what}" => $this->getAppName(),
                ]);
            }
        }

        if (! @$person["email"] || empty($person["email"])) {
            $getEmail = $this->person->getEmailById((string) $person["_id"]);
            $person["email"] = $getEmail["email"];
        }

        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_invitation,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "invitorName" => $invitor["name"],
                "title" => $this->getAppName(),
                "invitorLogo" => @$invitor["profilThumbImageUrl"],
                "invitedUserId" => $person["_id"],
                "message" => $msg,
                "language" => $this->language->get(),
            ],
        ];

        $params = $this->getCustomMail($params);
        if (! empty($invitorUrl)) {
            $params["tplParams"]["invitorUrl"] = $invitorUrl;
        }

        $this->schedule($params);
    }

    /**
     * Relaunches an invitation to a person.
     *
     * @param array $person The person to invite.
     * @param string|null $nameInvitor The name of the invitor (optional).
     * @param string|null $invitorUrl The URL of the invitor (optional).
     * @param string|null $subject The subject of the invitation (optional).
     * @return void
     */
    public function relaunchInvitePerson(array $person, ?string $nameInvitor = null, ?string $invitorUrl = null, ?string $subject = null): void
    {
        $invitor = [];
        if (isset($person["invitedBy"])) {
            $invitor = $this->person->getSimpleUserById($person["invitedBy"]);
        } elseif (isset($nameInvitor)) {
            $invitor["name"] = $nameInvitor;
        }

        if (@$invitor && empty(@$invitor["name"])) {
            $subject = $this->language->t("mail", "{who} is waiting for you on {what}", [
                "{who}" => $invitor["name"],
                "{what}" => $this->getAppName(),
            ]);
        } else {
            $subject = $this->language->t("mail", "{what} is waiting for you", [
                "{what}" => $this->getAppName(),
            ]);
        }

        if (! @$person["email"] || empty($person["email"])) {
            $getEmail = $this->person->getEmailById((string) $person["_id"]);
            $person["email"] = $getEmail["email"];
        }

        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_relaunchInvitation,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "invitorName" => @$invitor["name"],
                "title" => $this->getAppName(),
                "invitedUserId" => $person["_id"],
                "language" => (! empty($person["language"]) ? $person["language"] : "fr"),
            ],
        ];

        $params = $this->getCustomMail($params);
        if (! empty($invitorUrl)) {
            $params["tplParams"]["invitorUrl"] = $invitorUrl;
        }

        $this->schedule($params);
    }

    // TODO : ça date de kissKissBankBank, n'est plus utilisé, à supprimer ?
    /**
     * Invite a person to KKBB.
     *
     * @param array $person The person to invite.
     * @param mixed $isInvited Whether the person is invited or not.
     * @return void
     */
    public function inviteKKBB(array $person, $isInvited): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_inviteKKBB,
            "subject" => '[' . $this->getAppName() . '] - Venez rejoindre le réseau social citoyen',
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "title" => $this->getAppName(),
                "logo" => "/images/logo-communecter.png",
                "logo2" => $this->applicationLogo,
                "invitedUserId" => $person["_id"],
                "isInvited" => $isInvited,
            ],
        ];
        $this->schedule($params);
    }

    //TODO SBAR - Do the template
    // public function newConnection(string $name, $mail, $newConnectionUserId)
    // {
    //     $params = [
    //         "type" => Cron::TYPE_MAIL,
    //         "tpl" => self::TPL_invitation,
    //         "subject" => 'Invited to ' . $this->getAppName() . ' by ' . $name,
    //         "from" => $this->params['adminEmail'],
    //         "to" => $this->params['adminEmail'],
    //         "tplParams" => [
    //             "sponsorName" => $name,
    //             "title" => $this->applicationName,
    //             "logo" => "/images/logo.png",
    //         ],
    //     ];
    //     $this->schedule($params);
    // }

    /**
     * Sends a password retrieval email to the specified email address.
     *
     * @param string $email The email address of the user.
     * @param string $pwd The password to be retrieved.
     * @return void
     */
    public function passwordRetreive(string $email, string $pwd): void
    {
        $user = $this->db->findOne("citoyens", [
            "email" => $email,
        ]);
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_passwordRetreive,
            "subject" => $this->language->t("mail", "Retreive your password on {website}", [
                "{website}" => $this->getAppName(),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $email,
            "tplParams" => [
                "pwd" => $pwd,
                "title" => $this->getAppName(),
                "userId" => (string) @$user["_id"],
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Validates a person.
     *
     * @param array $person The person to validate.
     * @return void
     */
    public function validatePerson(array $person): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_validation,
            //TODO validation should be Controller driven boolea $this->userAccountValidation
            "subject" => $this->language->t("mail", "Confirm your account on {website}", [
                "{website}" => $this->getAppName(),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "user" => $person["_id"],
                "title" => $this->getAppName(),
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Validates a person with a new password.
     *
     * @param array $person The person data.
     * @param string $pwd The new password.
     * @param string|null $formTitle The title of the form (optional).
     * @param string $redirectUrl The URL to redirect to after validation.
     * @return void
     */
    public function validatePersonWithNewPwd(array $person, string $pwd, ?string $formTitle = null, string $redirectUrl = ""): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_validationWithNewPwd,
            "subject" => $this->language->t("mail", "Confirm your account on {website}", [
                "{website}" => $this->getAppName(),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "user" => $person["_id"],
                "pwd" => $pwd,
                "urlToRedirect" => $redirectUrl,
                "formTitle" => $formTitle,
                "title" => $this->getAppName(),
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    //TODO QUESTION BOUBOULE => TO DELETE ...
    public function newEvent(array $creator, array $newEvent): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_newEvent,
            "subject" => $this->language->t("common", 'New Event created on ') . $this->getAppName(),
            "from" => $this->params->get('adminEmail'),
            "to" => $creator['email'],
            "tplParams" => [
                "user" => $creator['_id'],
                "title" => $newEvent['name'],
                "creatorName" => $creator['name'],
                "url" => "#event.detail.id." . $newEvent["_id"],
            ],
        ];
        $this->schedule($params);
    }

    //TODO QUESTION BOUBOULE => TO DELETE ...
    public function newProject(array $creator, array $newProject): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_newProject,
            "subject" => $this->language->t("common", 'New Project created on ') . $this->getAppName(),
            "from" => $this->params->get('adminEmail'),
            "to" => $creator['email'],
            "tplParams" => [
                "user" => $creator['_id'],
                "title" => $newProject['name'],
                "creatorName" => $creator['name'],
                "url" => "#project.detail.id." . $newProject["_id"],
            ],
        ];
        $this->schedule($params);
    }

    //TODO QUESTION BOUBOULE => TO DELETE ...
    public function newOrganization(array $creator, array $newOrganization): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_newOrganization,
            "subject" => $this->language->t("common", 'New Organization created on ') . $this->getAppName(),
            "from" => $this->params->get('adminEmail'),
            "to" => $newOrganization["email"],
            "tplParams" => [
                "user" => $creator['_id'],
                "title" => $newOrganization['name'],
                "creatorName" => $creator['name'],
                "url" => "#organization.dashboard.id." . $newOrganization["_id"],
            ],
        ];
        $this->schedule($params);
    }

    //TODO QUESTION BOUBOULE => TO DELETE ...
    public function inviteContact($mailContact, $user)
    {
        $person = [];
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_invitation,
            "subject" => $this->language->t("mail", 'You have been invited to {website} by {who}', [
                "{website}" => $this->getAppName(),
                "{who}" => $user["name"],
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $person["email"],
            "tplParams" => [
                "invitorName" => $user["name"],
                "title" => $this->getAppName(),
                "logo" => "/images/logo.png",
                "invitedUserId" => $person["_id"],
            ],
        ];
        $this->schedule($params);
    }

    //TODO QUESTION BOUBOULE : TO DELETE OR IMPROVE PROCESS
    public function notifAdminBugMessage(string $text): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_helpAndDebugNews,
            "subject" => 'You received a new post on help and debug stream by ' . $this->getAppName(),
            "from" => $this->session->get("userEmail"),
            "to" => $this->params->get('adminEmail'),
            "tplParams" => [
                "title" => $this->session->get("userName"),
                "logo" => "/images/logo.png",
                "content" => $text,
            ],
        ];
        $this->schedule($params);
    }

    //TODO QUESTION THOMAS : je vois pas cette fonction utiliser quelque part
    /**
     * Sends an email notification when someone demands to become an admin.
     *
     * @param mixed $parent The parent object.
     * @param mixed $parentType The type of the parent object.
     * @param mixed $newPendingAdmin The email address of the new pending admin.
     * @param array $listofAdminsEmail The list of email addresses of the current admins.
     * @param mixed $typeOfDemand The type of demand.
     * @return void
     */
    public function someoneDemandToBecome($parent, $parentType, $newPendingAdmin, array $listofAdminsEmail, $typeOfDemand): void
    {
        foreach ($listofAdminsEmail as $currentAdminEmail) {
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_askToBecomeAdmin,
                "subject" => "[" . $this->getAppName() . "] " . $this->language->t(
                    "mail",
                    "A citizen ask to become {what} of {where}",
                    [
                        "{what}" => $typeOfDemand,
                        "{where}" => $parent["name"],
                    ]
                ),
                "from" => $this->params->get('adminEmail'),
                "to" => $currentAdminEmail,
                "tplParams" => [
                    "newPendingAdmin" => $newPendingAdmin,
                    "title" => $this->getAppName(),
                    "parent" => $parent,
                    "parentType" => $parentType,
                    "typeOfDemand" => $typeOfDemand,
                ],
            ];
            $params = $this->getCustomMail($params);
            $this->schedule($params);
        }
    }

    /**
     * Follows an element of a specific type by sending mail notifications.
     *
     * @param array $element The element to follow.
     * @param string $elementType The type of the element.
     * @param array|null $listOfMail The list of mail recipients (optional).
     * @return void
     */
    public function follow(array $element, string $elementType, ?array $listOfMail = null): void
    {
        $params = [];
        if ($elementType == PersonInterface::COLLECTION) {
            $childMail = $this->person->getEmailById((string) $element["_id"]);
            $listOfMail = [$childMail["email"]];
            $title = $this->language->t("mail", "You have a new follower");
        } else {
            $title = $element["name"] . $this->language->t("mail", "has a new follower");
        }
        foreach ($listOfMail as $mail) {
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_follow,
                "subject" => "[" . $this->getAppName() . "] " . $title,
                "from" => $this->params->get('adminEmail'),
                "to" => $mail,
                "tplParams" => [
                    "title" => $this->getAppName(),
                    "authorName" => $this->session->getUserName(),
                    "authorId" => $this->session->getUserId(),
                    "parent" => $element,
                    "parentType" => $elementType,
                ],
            ];
            $params = $this->getCustomMail($params);
        }
        $this->schedule($params);
    }

    /**
     * Sends an email from the contact form.
     *
     * @param string $emailSender The email address of the sender.
     * @param mixed $names The names of the sender.
     * @param string $subject The subject of the email.
     * @param string $contentMsg The content of the email.
     * @return void
     */
    public function sendMailFormContact(string $emailSender, string $names, string $subject, string $contentMsg): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_contactForm,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $this->params->get('adminEmail'),
            "tplParams" => [
                "title" => $this->language->t("mail", "New message from {who}", [
                    "{who}" => $names,
                ]),
                "subject" => $subject,
                "message" => $contentMsg,
                "emailSender" => $emailSender,
            ],
        ];

        $this->schedule($params);
    }

    /**
     * Sends a private contact form email.
     *
     * @param string $emailSender The email address of the sender.
     * @param string $names The names of the sender.
     * @param string $subject The subject of the email.
     * @param string $contentMsg The content of the email.
     * @param string $emailReceiver The email address of the receiver.
     * @return void
     */
    public function sendMailFormContactPrivate(string $emailSender, string $names, string $subject, string $contentMsg, string $emailReceiver): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_contactForm,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $emailReceiver,
            "tplParams" => [
                "title" => $this->language->t("mail", "New message from {who}", [
                    "{who}" => $names,
                ]),
                "subject" => $subject,
                "message" => $contentMsg,
                "emailSender" => $emailSender,
            ],
        ];

        $this->schedule($params);
    }

    /**
     * Sends a confirmation email for deleting an element.
     *
     * @param string $elementType The type of the element being deleted.
     * @param string $elementId The ID of the element being deleted.
     * @param string $reason The reason for deleting the element.
     * @param array $admins An array of admin email addresses.
     * @param string $userId The ID of the user initiating the deletion.
     * @return void
     */
    public function confirmDeleteElement(string $elementType, string $elementId, string $reason, array $admins, string $userId): void
    {
        $element = $this->element->getElementSimpleById($elementId, $elementType);
        $user = $this->person->getSimpleUserById($userId);
        $url = "#" . $this->element->getControlerByCollection($elementType) . ".detail.id." . $element["_id"];
        $nbDayBeforeDelete = ElementInterface::NB_DAY_BEFORE_DELETE;
        foreach ($admins as $id) {
            $aPerson = $this->person->getById($id, false);
            if (! empty($aPerson["email"])) {
                $params = [
                    "type" => CronInterface::TYPE_MAIL,
                    "tpl" => MailInterface::TPL_confirmDeleteElement,
                    "subject" => "[" . $this->getAppName() . "] - Suppression de " . @$element["name"],
                    "from" => $this->params->get('adminEmail'),
                    "to" => $aPerson["email"],
                    "tplParams" => [
                        "elementType" => @$elementType,
                        "elementName" => @$element["name"],
                        "userName" => @$user["name"],
                        "logo" => $this->params->get("logoUrl"),
                        "logo2" => $this->params->get("logoUrl2"),
                        "reason" => $reason,
                        "nbDayBeforeDelete" => $nbDayBeforeDelete,
                        "url" => $this->baseUrl . "/" . $url,
                    ],
                ];

                $this->schedule($params);
            }
        }
    }

    // TODO $admins pas utilisé dans la fonction
    /**
     * Propose an interop source.
     *
     * @param string $url_source The URL of the source.
     * @param mixed $admins The admins of the source.
     * @param string $userID The ID of the user.
     * @param string $description The description of the source.
     * @return void
     */
    public function proposeInteropSource(string $url_source, $admins, string $userID, string $description)
    {
        $user = $this->person->getSimpleUserById($userID);

        $aPerson = $this->person->getById("5880b24a8fe7a1a65b8b456b", false);
        if (! empty($aPerson["email"])) {
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_proposeInteropSource,
                "subject" => "[" . $this->getAppName() . "] - Proposition de " . @$user["name"],
                "from" => $this->params->get('adminEmail'),
                "to" => $aPerson["email"],
                "tplParams" => [
                    "userName" => @$user["name"],
                    "description" => $description,
                    "source_de_donnees" => $url_source,
                    "logo" => $this->params->get("logoUrl"),
                    "logo2" => $this->params->get("logoUrl2"),
                ],
            ];

            $this->schedule($params);
        }
    }

    /**
     * Validates the proposed interop.
     *
     * @param string $url_source The URL source.
     * @param string $userID The user ID.
     * @param string $adminID The admin ID.
     * @param string $description The description.
     * @return void
     */
    public function validateProposedInterop(string $url_source, string $userID, string $adminID, string $description): void
    {
        $user = $this->person->getSimpleUserById($userID);
        $aPerson = $this->person->getSimpleUserById($adminID);

        if (! empty($aPerson["email"])) {
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_validateInteropSource,
                "subject" => "[" . $this->getAppName() . "] - Validation de votre proposition pour une nouvelle interopérabilité, " . @$user["name"],
                "from" => $this->params->get('adminEmail'),
                "to" => $user["email"],
                "tplParams" => [
                    "userName" => @$user["name"],
                    "url_source" => $url_source,
                    "description" => $description,
                    "logo" => $this->params->get("logoUrl"),
                    "logo2" => $this->params->get("logoUrl2"),
                ],
            ];

            $this->schedule($params);
        }
    }

    /**
     * Rejects a proposed interop.
     *
     * @param string $url_source The source URL.
     * @param string $userID The user ID.
     * @param string $adminID The admin ID.
     * @param string $description The rejection description.
     * @return void
     */
    public function rejectProposedInterop(string $url_source, string $userID, string $adminID, string $description): void
    {
        $user = $this->person->getSimpleUserById($userID);
        $aPerson = $this->person->getSimpleUserById($adminID);

        if (! empty($aPerson["email"])) {
            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_rejectInteropSource,
                "subject" => "[" . $this->getAppName() . "] - Rejet de votre proposition pour une nouvelle interopérabilité, " . @$user["name"],
                "from" => $this->params->get('adminEmail'),
                "to" => $user["email"],
                "tplParams" => [
                    "userName" => @$user["name"],
                    "url_source" => $url_source,
                    "description" => $description,
                    "logo" => $this->params->get("logoUrl"),
                    "logo2" => $this->params->get("logoUrl2"),
                ],
            ];

            $this->schedule($params);
        }
    }

    /**
     * Returns the name of the application.
     *
     * @return string The name of the application.
     */
    public function getAppName(): string
    {
        $costum = $this->getModelCostum()->getCostum();
        if (@$costum && @$costum["title"]) {
            $appName = $costum["title"];
        } elseif (@$this->params->get("name")) {
            $appName = $this->params->get("name");
        } else {
            $appName = $this->applicationName;
        }
        return $appName;
    }

    // TODO THOMAS - je crois pas que ça soit utilisé, car ça utilise encore echo, utilisé la actionMailMaj - TO DELETE
    public function mailMaj()
    {
        $persons = $this->db->find(PersonInterface::COLLECTION, [
            "pending" => [
                '$exists' => 0,
            ],
            "email" => [
                '$exists' => 1,
            ],
        ], ["name", "language", "email"]);
        $i = 0;
        $v = 0;
        $languageUser = $this->language->get();
        $res = [];
        foreach ($persons as $key => $value) {
            if (! empty($value["email"]) && $this->dataValidator->email($value["email"]) == "" && ! empty($value["language"])) {
                echo $key . " : " . $value["name"] . " : " . $value["language"] . " <br/> ";
                $subject = $this->language->t("mail", "New Update");
                $params = [
                    "type" => CronInterface::TYPE_MAIL,
                    "tpl" => MailInterface::TPL_update,
                    "subject" => $subject,
                    "from" => $this->params->get('adminEmail'),
                    "to" => $value["email"],
                    "tplParams" => [
                        "title" => $this->getAppName(),
                        "logo" => $this->params->get("logoUrl"),
                        "logo2" => $this->params->get("logoUrl2"),
                        "invitedUserId" => $value["_id"],
                        "language" => (! empty($value["language"]) ? $value["language"] : "fr"),
                    ],
                ];
                $this->schedule($params);
                $i++;
            } else {
                $v++;
            }
        }
        echo $i . " mails envoyé pour relancer l'inscription<br>";
        echo $v . " utilisateur non inscrit (validé) qui ont un mail de marde<br>";

        $this->language->set($languageUser);
    }

    /**
     * Sends a confirmation email to the user after saving a survey.
     *
     * @param array $user The user information.
     * @param array $survey The survey information.
     * @return void
     */
    public function confirmSavingSurvey(array $user, array $survey): void
    {
        $subject = $this->language->t("surveys", "{what} Your application package is well submited", [
            "{what}" => "[" . $survey["title"] . "]",
        ]);
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_surveysubmissionSuccess,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $user["email"],
            "tplParams" => [
                "title" => $survey["title"],
                "logo" => $this->urlHelper->createUrl('/' . $survey["urlLogo"]),
                "logo2" => $this->urlHelper->createUrl('/' . $survey["urlLogo"]),
                "user" => $user,
                "language" => $this->language->get(),
                "survey" => $survey,
            ],
        ];
        $this->schedule($params);
    }

    /**
     * Sends a new answer notification to the admin.
     *
     * @param string $email The email address of the admin.
     * @param array $user The user information.
     * @param array $survey The survey information.
     * @return void
     */
    public function sendNewAnswerToAdmin(string $email, array $user, array $survey): void
    {
        $subject = $this->language->t("surveys", "{what} A new application package is added", [
            "{what}" => "[" . $survey["title"] . "]",
        ]);
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_surveynewSubmission,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $email,
            "tplParams" => [
                "title" => $survey["title"],
                "logo" => $this->urlHelper->createUrl('/' . $survey["urlLogo"]),
                "logo2" => $this->urlHelper->createUrl('/' . $survey["urlLogo"]),
                "user" => $user,
                "language" => $this->language->get(),
                "survey" => $survey,
            ],
        ];
        $this->schedule($params);
    }

    /**
     * Retrieves the mail update.
     *
     * @param string $mail The mail address.
     * @param string $tpl The template.
     * @return array|null The mail update, or null if not found.
     */
    private function getMailUpdate(string $mail, string $tpl): ?array
    {
        $costum = $this->getModelCostum()->getCostum();
        $where = [
            "tpl" => $tpl,
            "to" => $mail,
            "status" => CronInterface::STATUS_UPDATE,
        ];
        if (! empty($costum) && ! empty($costum["slug"])) {
            $where['source.key'] = $costum["slug"];
        } else {
            $where['source'] = [
                '$exists' => 0,
            ];
        }

        $res = $this->db->findOne(CronInterface::COLLECTION, $where);
        return $res;
    }

    /**
     * Retrieves a custom mail.
     *
     * @param array $params The parameters for the custom mail.
     * @param string|null $from The sender of the custom mail. Defaults to null.
     * @return array The custom mail.
     */
    public function getCustomMail(array $params, ?string $from = null): array
    {
        $costum = $this->getModelCostum()->getCostum();
        if (isset($costum) && isset($costum["slug"])) {
            $params["source"] = [];
            $params["source"]["key"] = $costum["contextSlug"] ?? $costum["slug"];
        }
        if (@$costum && @$costum["logo"]) {
            $params["tplParams"]["logo"] = $costum["logo"];
            $params["tplParams"]["logo2"] = "";
            $params["tplParams"]["logoHeader"] = $costum["mailsConfig"]["logo"] ?? $costum["logo"];
            if (! empty($params["tplParams"]["noLogoHeader"]) && $params["tplParams"]["noLogoHeader"] == true) {
                unset($params["tplParams"]["logoHeader"]);
                unset($params["tplParams"]["noLogoHeader"]);
            }
        } else {
            $params["tplParams"]["logo"] = $this->params->get("logoUrl");
            $params["tplParams"]["logo2"] = $this->params->get("logoUrl2");
        }
        if (@$costum && @$costum["title"]) {
            $params["tplParams"]["title"] = $costum["title"];
            $params["expeditor"] = $costum["title"];
        }
        if (isset($costum) && isset($costum["mailsConfig"])) {
            if (isset($costum["mailsConfig"][$params["tpl"]]) && isset($costum["mailsConfig"][$params["tpl"]]["tpl"])) {
                $params["tplParams"]["costumTpl"] = $costum["mailsConfig"][$params["tpl"]]["tpl"];
            }
            if (isset($costum["mailsConfig"]["footerTpl"])) {
                $params["tplParams"]["footerTpl"] = $costum["mailsConfig"]["footerTpl"];
            }
            if (isset($costum["mailsConfig"]["headerTpl"])) {
                $params["tplParams"]["headerTpl"] = $costum["mailsConfig"]["headerTpl"];
            }
            if (isset($costum["mailsConfig"]["expeditor"])) {
                $params["expeditor"] = $costum["mailsConfig"]["expeditor"];
            }
        }

        if (! empty($from)) {
            $params["from"] = $from;
        } elseif (@$costum && @$costum["admin"] && @$costum["admin"]["email"]) {
            $params["from"] = $costum["admin"]["email"];
        }

        $params["tplParams"]["baseUrl"] = $this->baseUrl;
        if (isset($costum) && isset($costum["host"])) {
            $params["tplParams"]["baseUrl"] = "https://" . $costum["host"];
            $params["tplParams"]["url"] = $costum["url"];
        } elseif (isset($costum) && isset($costum["url"]) && empty($costum["isTemplate"])) {
            $costumUrl = str_replace($costum["assetsSlug"], $costum["slug"], $costum["url"]);
            $params["tplParams"]["baseUrl"] = $params["tplParams"]["baseUrl"] . $costumUrl;
            $params["tplParams"]["url"] = $costumUrl;
        } elseif (isset($costum) && isset($costum["isTemplate"]) && $costum["isTemplate"] && isset($costum["contextSlug"])) {
            $params["tplParams"]["baseUrl"] = $params["tplParams"]["baseUrl"] . "/costum/co/index/slug/" . $costum["contextSlug"];
            $params["tplParams"]["url"] = (isset($params["tplParams"]["url"]) && ! empty($params["tplParams"]["url"])) ? $params["tplParams"]["url"] : "/costum/co/index/slug/" . $costum["contextSlug"];
        }

        if (@$costum &&
            @$costum["mail"] &&
            @$costum["mail"][$params["tpl"]]) {
            $params["tplParams"] = array_merge($params["tplParams"], $costum["mail"][$params["tpl"]]);
        }
        return $params;
    }

    /**
    * Requests to become something.
    *
    * @param array $construct The construction data.
    * @param array $val The values.
    * @return void
    */
    public function askToBecome(array $construct, array $val): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_askToBecome,
            "subject" => "[" . $this->getAppName() . "] " . $this->language->t(
                "mail",
                "A citizen ask to become {what} of {where}",
                [
                    "{what}" => $this->language->t("common", @$construct["value"]["typeOfDemand"]),
                    "{where}" => $construct["target"]["name"],
                ]
            ),
            "from" => $this->params->get('adminEmail'),
            "to" => $val["email"],
            "tplParams" => [
                "newPendingAdmin" => $construct["author"],
                "title" => $this->getAppName(),
                "parent" => $construct["target"],
                "parentType" => $construct["target"]["type"],
                "typeOfDemand" => @$construct["value"]["typeOfDemand"],
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Sends an invitation email.
     *
     * @param array $construct The construction parameters.
     * @param array $val The values to be used in the email.
     * @return void
     */
    public function invitation(array $construct, array $val): void
    {
        $msg = null;

        $invitor = $construct["author"];

        if (@$invitor && ! empty($invitor["name"])) {
            $subject = $this->language->t("mail", "{who} is waiting for you on {what}", [
                "{who}" => $invitor["name"],
                "{what}" => $this->getAppName(),
            ]);
        } else {
            $subject = $this->language->t("mail", "{what} is waiting for you", [
                "{what}" => $this->getAppName(),
            ]);
        }

        if (! @$val["email"] || empty($val["email"])) {
            $getEmail = $this->person->getEmailById($val["id"]);
            $val["email"] = $getEmail["email"];
        }

        $target = (! empty($construct["target"]) ? $construct["target"] : null);

        $value = (! empty($construct["value"]) ? $construct["value"] : null);

        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_invitation,
            "subject" => $subject,
            "from" => $this->params->get('adminEmail'),
            "to" => $val["email"],
            "tplParams" => [
                "invitorName" => $invitor["name"],
                "title" => $this->getAppName(),
                "invitorLogo" => @$invitor["profilThumbImageUrl"],
                "invitedUserId" => $val["id"],
                "message" => @$msg,
                "target" => $target,
                "language" => $val["language"],
                "value" => $value,
            ],
        ];

        $params = $this->getCustomMail($params);
        if (! empty($invitorUrl)) {
            $params["tplParams"]["invitorUrl"] = $invitorUrl;
        }

        $this->schedule($params);
    }

    /**
     * Invite someone to something.
     *
     * @param array $construct The construction parameters.
     * @param array $val The values to be used in the invitation.
     * @return void
     */
    public function inviteYouTo(array $construct, array $val): void
    {
        $person = $this->element->getElementById($val["id"], PersonInterface::COLLECTION, null, ["roles"]);

        if (! empty($person["roles"]) && ! empty($person["roles"]["tobeactivated"]) && $person["roles"]["tobeactivated"] == true) {
            $this->invitation($construct, $val);
        } else {
            $invitor = $construct["author"];
            $target = (! empty($construct["target"]) ? $construct["target"] : null);
            $value = (! empty($construct["value"]) ? $construct["value"] : null);

            if ($value["typeOfDemand"] == "admin") {
                $verb = "administrate";
            } else {
                if ($target["type"] == EventInterface::COLLECTION) {
                    $verb = "participate to";
                } elseif ($target["type"] == ProjectInterface::COLLECTION) {
                    $verb = "contribute to";
                } else {
                    $verb = "join";
                }
            }

            $params = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_inviteYouTo,
                "subject" => "[" . $this->getAppName() . "] " . $this->language->t("mail", "Invitation to {what} {where}", [
                    "{what}" => $this->language->t("mail", $verb),
                    "{where}" => $target["name"],
                ]),
                "from" => $this->params->get('adminEmail'),
                "to" => $val["email"],
                "tplParams" => [
                    "title" => $this->getAppName(),
                    "invitorName" => $invitor["name"],
                    "invitorLogo" => @$invitor["profilThumbImageUrl"],
                    "invitedUserId" => $val["id"],
                    "target" => $target,
                    "value" => $value,
                ],
            ];
            $params = $this->getCustomMail($params);
            $this->schedule($params);
        }
    }

    /**
     * Demande à devenir ami.
     *
     * @param array $construct Les informations de construction.
     * @param array $val Les valeurs.
     * @return void
     */
    public function askToBecomeFriend(array $construct, array $val): void
    {
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_inviteYouTo,
            "subject" => "[" . $this->getAppName() . "] " . $this->language->t("mail", "{who} asks to be friend whith you", [
                "{who}" => ucfirst($construct["author"]["name"]),
            ]),
            "from" => $this->params->get('adminEmail'),
            "to" => $val["email"],
            "tplParams" => [
                "title" => $this->getAppName(),
                "invitor" => $construct["author"],
                "target" => $construct["target"],
                "language" => $val["language"] ?? "FR",
                "value" => $val,
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Creates a notification.
     *
     * @param array $construct The notification data.
     * @param string|null $tpl The template to use for the notification.
     * @return void
     */
    public function createNotification(array $construct, ?string $tpl = null): void
    {
        foreach ($construct["community"]["mails"] as $key => $value) {
            $person = $this->person->getById($key, false);
            $mailIsNotDesactivated = ! (isset($person["preferences"]["mails"]) && $person["preferences"]["mails"] == "desactivated");

            if ($key != $this->session->getUserId() && $mailIsNotDesactivated) {
                if (! empty($tpl)) {
                    if (method_exists("Mail", $tpl) || function_exists($tpl)) {
                        $this->$tpl($construct, $value);
                    }
                } else {
                    if (isset($value["email"])) {
                        $mail = $this->getMailUpdate($value["email"], 'notification');
                    }

                    if (! empty($mail)) {
                        $paramTpl = $this->createParamsTpl($construct, $mail["tplParams"]["data"]);
                        $mail["tplParams"]["data"] = $paramTpl;
                        $this->db->update(
                            CronInterface::COLLECTION,
                            [
                                "_id" => $mail["_id"],
                            ],
                            [
                                '$set' => [
                                    "tplParams" => $mail["tplParams"],
                                ],
                            ]
                        );
                    } else {
                        $language = (@$value["language"]) ? $value["language"] : "fr";
                        $paramTpl = $this->createParamsTpl($construct);
                        $params = [
                            "type" => CronInterface::TYPE_MAIL,
                            "tpl" => MailInterface::TPL_notification,
                            "subject" => "[" . $this->getAppName() . "] - " . $this->language->t("mail", "We got news for you.", [], null, $language),
                            "from" => $this->params->get('adminEmail'),
                            "to" => $value["email"],
                            "language" => $language,
                            "tplParams" => [
                                "logo" => $this->params->get("logoUrl"),
                                "logo2" => $this->params->get("logoUrl2"),
                                "data" => $paramTpl,
                            ],
                        ];
                        $params = $this->getCustomMail($params);
                        $this->schedule($params, true);
                    }
                }
            }
        }
    }

    /**
     * Sends a confirmation email to someone who confirmed you.
     *
     * @param array $parent The parent data.
     * @param string $parentType The type of the parent.
     * @param array $child The child data.
     * @param string $typeOfDemand The type of demand.
     * @return void
     */
    public function someoneConfirmYouTo(array $parent, string $parentType, array $child, string $typeOfDemand): void
    {
        if ($typeOfDemand == "admin") {
            $verb = "administrate";
        } else {
            if ($parentType == EventInterface::COLLECTION) {
                $verb = "participate to";
            } elseif ($parentType == ProjectInterface::COLLECTION) {
                $verb = "contribute to";
            } else {
                $verb = "join";
            }
        }
        $childMail = $this->person->getEmailById((string) $child["_id"]);
        $params = [
            "type" => CronInterface::TYPE_MAIL,
            "tpl" => MailInterface::TPL_confirmYouTo,
            "subject" => "[" . $this->getAppName() . "] " . $this->language->t(
                "mail",
                "Confirmation to {what} {where}",
                [
                    "{what}" => $this->language->t("mail", $verb),
                    "{where}" => $parent["name"],
                ]
            ),
            "from" => $this->params->get('adminEmail'),
            "to" => $childMail["email"],
            "tplParams" => [
                "newChild" => $child,
                "title" => $this->getAppName(),
                "authorName" => $this->session->getUserName(),
                "authorId" => $this->session->getUserId(),
                "parent" => $parent,
                "parentType" => $parentType,
                "typeOfDemand" => $typeOfDemand,
                "verb" => $verb,
            ],
        ];
        $params = $this->getCustomMail($params);
        $this->schedule($params);
    }

    /**
     * Creates the parameters template.
     *
     * @param array $construct The construct array.
     * @param array $paramTpl The parameter template array.
     * @return array The created parameters template.
     */
    private function createParamsTpl(array $construct, array $paramTpl = []): array
    {
        $targetType = $construct["target"]["type"];
        $targetId = $construct["target"]["id"];
        $verb = $construct["verb"];
        $repeat = false;
        $repeatKey = null;
        $sameAuthor = null;
        $labelArray = [];
        $myParam = null;

        if ($targetType == NewsInterface::COLLECTION) {
            $news = $this->news->getById($targetId);
            $construct["target"] = $news["target"];
        }

        if (empty($paramTpl["countData"])) {
            $paramTpl["countData"] = 0;
        }

        if (empty($paramTpl["data"])) {
            $paramTpl["data"] = [];
        } else {
            foreach ($paramTpl["data"] as $keyD => $valD) {
                if ($valD["verb"] == $verb &&
                    $valD["targetType"] == $targetType &&
                    $valD["targetId"] == $targetId &&
                    ((! empty($construct["object"]) &&
                            ! empty($valD["object"]) &&
                            $valD["object"]["type"] == $construct["object"]["type"]) ||
                        empty($construct["object"]))) {
                    $myParam = $valD;
                    $repeatKey = $keyD;

                    if (! empty($valD["labelArray"]) &&
                        ! empty($valD["labelArray"]["{author}"]) &&
                        ! empty($valD["labelArray"]["{author}"]) &&
                        ! empty($construct["author"]) &&
                        ! empty($construct["author"]["id"])) {
                        foreach ($valD["labelArray"]["{author}"] as $keyA => $valA) {
                            if ($construct["author"]["id"] == $valA["id"]) {
                                $sameAuthor = true;
                            }
                        }
                    }

                    break;
                }
            }
        }

        if ($myParam == null) {
            $myParam = [
                "targetType" => $targetType,
                "targetId" => $targetId,
                "verb" => $verb,
                "repeat" => "Mail",

                "url" => $this->urlMailNotif($targetType, $targetId, $construct),
                "name" => @$construct["target"]["name"],
            ];
        } else {
            $myParam["repeat"] = "RepeatMail";
            $repeat = true;
            $labelArray = $myParam["labelArray"];
        }

        if (! empty($construct["value"]) && $repeat == false) {
            $myParam["value"] = $construct["value"];
        } elseif (! empty($myParam["value"]) && $repeat == true) {
            unset($myParam["value"]);
        }

        if (! empty($construct["object"])) {
            $myParam["object"] = $construct["object"];
        }

        foreach ($construct["labelArray"] as $key => $value) {
            $str = [
                "name" => "",
                "url" => "",
            ];
            if ("who" == $value) {
                if (! empty($construct["target"]) &&
                    ! empty($construct["target"]["targetIsAuthor"]) &&
                    $construct["target"]["targetIsAuthor"] == true) {
                    $str["name"] = @$construct["target"]["name"];
                    $str["type"] = @$construct["target"]["type"];
                    $str["img"] = @$construct["target"]["profilThumbImageUrl"];

                    $str["url"] = $this->urlMailNotif($targetType, $targetId, $construct);
                    $str["id"] = @$targetId;
                } elseif (! empty($construct["author"])) {
                    $str["name"] = @$construct["author"]["name"];
                    $str["type"] = PersonInterface::COLLECTION;
                    $str["img"] = @$construct["author"]["profilThumbImageUrl"];

                    $str["url"] = $this->urlMailNotif(PersonInterface::COLLECTION, $construct["author"]["id"], $construct);
                    $str["id"] = @$construct["author"]["id"];
                }
            } elseif ("where" == $value && ! empty($construct["target"])) {
                $str["name"] = (! empty($construct["target"]["name"]) ? @$construct["target"]["name"] : @$construct["target"]["title"]);
                $str["type"] = @$construct["target"]["type"];
                $str["id"] = @$construct["target"]["id"];

                $str["url"] = $this->urlMailNotif($construct["target"]["type"], $construct["target"]["id"], $construct);
            } elseif ("what" == $value && ! empty($construct["object"])) {
                $str["name"] = (! empty($construct["object"]["name"]) ? @$construct["object"]["name"] : @$construct["object"]["title"]);
                $str["type"] = @$construct["object"]["type"];
                $str["id"] = @$construct["object"]["id"];

                $str["url"] = $this->urlMailNotif($construct["object"]["type"], $construct["object"]["id"], $construct);
            } elseif ("author" == $value && ! empty($construct["author"])) {
                $str["name"] = @$construct["author"]["name"];
                $str["type"] = PersonInterface::COLLECTION;
                $str["img"] = @$construct["author"]["profilThumbImageUrl"];

                $str["url"] = $this->urlMailNotif(PersonInterface::COLLECTION, $construct["author"]["id"], $construct);
                $str["id"] = $construct["author"]["id"];
            }

            if (! empty($str)) {
                $find = false;
                if (! empty($labelArray["{" . $value . "}"])) {
                    foreach ($labelArray["{" . $value . "}"] as $key2 => $value2) {
                        if ($value2 == $str) {
                            $find = true;
                        }
                    }
                }

                if ($find == false) {
                    $labelArray["{" . $value . "}"][] = $str;
                }
            }
        }

        $myParam["label"] = $this->notification->getLabelNotification($construct, null, 1, null, $myParam["repeat"], @$sameAuthor);
        $myParam["labelArray"] = $labelArray;

        if ($repeat === true) {
            $paramTpl["data"][$repeatKey] = $myParam;
        } elseif ($paramTpl["countData"] < 3) {
            $paramTpl["data"][] = $myParam;
        }

        if ($repeat == null) {
            $paramTpl["countData"]++;
        }

        return $paramTpl;
    }

    /**
     * Generates the URL for a mail notification.
     *
     * @param string $type The type of the mail notification.
     * @param string $id The ID of the mail notification.
     * @param array|null $construct Additional parameters for constructing the URL.
     * @return string The generated URL for the mail notification.
     */
    public function urlMailNotif(string $type, string $id, ?array $construct = null): string
    {
        $costum = $this->getModelCostum()->getCostum();
        $baseUrl = $this->baseUrl;
        if (! empty($costum)) {
            if (isset($costum["url"]) && ! empty($costum["url"])) {
                $baseUrl .= $costum["url"];
            } elseif (isset($costum["host"]) && ! empty($costum["host"])) {
                $baseUrl = $costum["host"];
            }
        }
        if ($type == ProposalInterface::COLLECTION) {
            $url = $baseUrl . "/#dda?" . $type . "." . $id;
        } elseif ($type == FormInterface::COLLECTION) {
            $baseUrl = $this->baseUrl;
            $parentSlug = $construct["target"]["slug"];
            if (isset($construct["value"]["id"])) {
                $url = $baseUrl . "/#@" . $parentSlug . ".view.forms.dir.answer." . ((string) $construct["value"]["id"]);
            } else {
                $url = $baseUrl . "/#@" . $parentSlug . ".view.forms.dir.observatory." . $id;
            }
        } else {
            $url = $baseUrl . "/#page.type." . $type . ".id." . $id;
        }

        return $url;
    }

    /**
     * Translates the label of a mail.
     *
     * @param array $mail The mail data.
     * @return string The translated label.
     */
    public function translateLabel(array $mail): string
    {
        $resArray = [];
        if (! empty($mail["labelArray"])) {
            if (! empty($mail["labelArray"]["{who}"])) {
                $who = "";
                $i = 0;
                $countEntry = is_countable($mail["labelArray"]["{who}"]) ? count($mail["labelArray"]["{who}"]) : 0;
                foreach ($mail["labelArray"]["{who}"] as $key => $value) {
                    if ($i == 1 && $countEntry == 2) {
                        $who .= " " . $this->language->t("common", "and") . " ";
                    } elseif ($i > 0) {
                        $who .= ", ";
                    }

                    if ($i >= 2) {
                        $s = "";
                        if ($countEntry > 3) {
                            $s = "s";
                        }
                        $typeMore = "person";

                        $who .= " " . $this->language->t("common", "and") . " " . ($countEntry - 2) . " " . $this->language->t("common", $typeMore . $s);
                    } else {
                        $color = "#ea0040";
                        $img = "";
                        if (! empty($value["img"])) {
                            $img = '<img id="menu-thumb-profil" src="' . $this->baseUrl . $value["img"] . '" alt="image" width="35" height="35" style="display: inline; vertical-align: middle; border-radius:100%;">';
                        }
                        $who .= "<a href='" . $value["url"] . "' target='_blank' style='color:" . $color . ";font-weight:800;font-variant:small-caps;'>" . $img . " <span style=''>" . $value["name"] . "</span>" . "</a>";
                    }
                    $i++;
                }

                $resArray["{who}"] = $who;
            }

            if (! empty($mail["labelArray"]["{author}"])) {
                $who = "";
                $i = 0;
                $countEntry = is_countable($mail["labelArray"]["{author}"]) ? count($mail["labelArray"]["{author}"]) : 0;
                foreach ($mail["labelArray"]["{author}"] as $key => $value) {
                    if ($i == 1 && $countEntry == 2) {
                        $who .= " " . $this->language->t("common", "and") . " ";
                    } elseif ($i > 0) {
                        $who .= ", ";
                    }

                    if ($i >= 2) {
                        $s = "";
                        if ($countEntry > 3) {
                            $s = "s";
                        }
                        $typeMore = "person";

                        $who .= " " . $this->language->t("common", "and") . " " . ($countEntry - 2) . " " . $this->language->t("common", $typeMore . $s);
                    } else {
                        $color = "#ea0040";
                        $img = "";
                        if (! empty($value["img"])) {
                            $img = '<img id="menu-thumb-profil" src="' . $this->baseUrl . $value["img"] . '" alt="image" width="35" height="35" style="display: inline; vertical-align: middle; border-radius:100%;">';
                        }
                        $who .= "<a href='" . $value["url"] . "' target='_blank' style='color:" . $color . ";font-weight:800;font-variant:small-caps;'>" . $img . " <span style=''>" . $value["name"] . "</span>" . "</a>";
                    }
                    $i++;
                }

                $resArray["{author}"] = $who;
            }

            if (! empty($mail["labelArray"]["{what}"])) {
                $what = "";
                $i = 0;
                $names = [];
                foreach ($mail["labelArray"]["{what}"] as $data) {
                    if ($i > 0) {
                        $what .= " ";
                    }

                    $color = "#ea0040";

                    if (! in_array($data["name"], $names)) {
                        $what .= "<a href='" . $data["url"] . "' target='_blank' style='color:" . $color . ";font-weight:800;font-variant:small-caps;'>" . $this->language->t("notification", $data["name"]) . "</a>";
                    }

                    $names[] = $data["name"];
                    $i++;
                }
                $resArray["{what}"] = $what;
            }

            if (! empty($mail["labelArray"]["{where}"])) {
                $where = "";
                $i = 0;
                foreach ($mail["labelArray"]["{where}"] as $data) {
                    if ($i > 0) {
                        $where .= " ";
                    }

                    $color = "#ea0040";
                    $where .= "<a href='" . $data["url"] . "' target='_blank' style='color:" . $color . ";font-weight:800;font-variant:small-caps;'>" . $this->language->t("notification", $data["name"]) . "</a>";
                    $i++;
                }
                $resArray["{where}"] = $where;
            }
        }

        if ($this->costum->isSameFunction("translateLabel")) {
            $mail = $this->costum->sameFunction("translateLabel", $mail);
        }

        return $this->language->t("mail", $mail["label"], $resArray);
    }

    // TODO est ce que c'est utilisé ?
    /**
     * Sends a bookmark notification email.
     *
     * @param array $params The parameters for the email.
     * @param string $userID The ID of the user to send the email to.
     * @param array|null $mailParams Additional parameters for the email (optional).
     * @return void
     */
    public function bookmarkNotif(array $params, string $userID, ?array $mailParams = null): void
    {
        $user = $this->person->getSimpleUserById($userID);
        if (! empty($user["email"])) {
            $subTit = (@$mailParams["title"]) ? $mailParams["title"] : $this->getAppName();
            $mailConstruct = [
                "type" => CronInterface::TYPE_MAIL,
                "tpl" => MailInterface::TPL_bookmarkNotif,
                "subject" => "[" . $subTit . "] - Nouvelles annonces, " . @$user["name"],
                "from" => $this->params->get('adminEmail'),
                "to" => $user["email"],
                "tplParams" => [
                    "userName" => @$user["name"],
                    "logo" => $this->params->get("logoUrl"),
                    "logo2" => $this->params->get("logoUrl2"),
                    "params" => $params,
                    "baseUrl" => $this->baseUrl . "/",
                ],
            ];
            if (@$mailParams && ! empty($mailParams)) {
                if (@$mailParams["logo"]) {
                    $mailConstruct["tplParams"]["logo"] = $mailParams["logo"];
                    $mailConstruct["tplParams"]["logo2"] = $mailParams["logo"];
                    $mailConstruct["tplParams"]["logoHeader"] = $mailParams["logo"];
                }
                if (@$mailParams["title"]) {
                    $mailConstruct["tplParams"]["title"] = $mailParams["title"];
                }
                if (@$mailParams["url"]) {
                    $mailConstruct["tplParams"]["url"] = $this->baseUrl . $mailParams["url"];
                }
            }
            $this->schedule($mailConstruct);
        }
    }

    /**
     * Sends a feedback email.
     *
     * @param array $params The parameters for the email.
     * @return void
     */
    public function mailFeedBack(array $params): void
    {
        $costum = $this->getModelCostum()->getCostum();
        if (empty($params["tplMail"]) && isset($costum["admin"]["email"])) {
            $params["tplMail"] = $costum["admin"]["email"];
        }

        if (! empty($params["tplMail"])) {
            foreach ($params["tplMail"] as $v) {
                $params["tplMail"] = $v;
                $fromMail = (isset($params["fromMail"]) && ! empty($params["fromMail"])) ? $params["fromMail"] : null;
                $res = [
                    "type" => CronInterface::TYPE_MAIL,
                    "tpl" => $params["tpl"],
                    "logo" => $params["logo"],
                    "subject" => $params["tplObject"],
                    "from" => $this->params->get('adminEmail'),
                    "to" => $params["tplMail"],
                    "tplParams" => $this->initTplParams($params),
                ];
                $res = $this->getCustomMail($res, $fromMail);
                if (@$params["logo"] === "banner") {
                    $res["tplParams"]["logo"] = $costum["banner"];
                    $res["tplParams"]["sign"] = $params["sign"];
                }
                $this->schedule($res);
            }
        } else {
            throw new CTKException($this->language->t("common", "Missing email!"));
        }
    }

    // TODO vraiment trés bizzare comme fonction, je comprend pas bien le but de cette fonction
    public function authorizationMail(string $mail): bool
    {
        $res = false;
        if (! empty($mail)) {
            // Vérifie si l'utisateur ne veut plus recevoir de mail de la plateforme
            $r = $this->db->find(ActivityStreamInterface::COLLECTION, [
                "target.email" => $mail,
                "verb" => ActStrInterface::VERB_NOSENDING,
            ]);
            if (empty($r)) {
                // Il n'y a pas de demande
                $res = true;
            }
        }
        return $res;
    }

    /**
        * Relaunches an invitation.
        *
        * @param string $id The identifier of the invitation.
        * @return void
        */
    public function relaunchInvitation(string $id): void
    {
        if ($this->role->isSuperAdmin($this->role->getRolesUserId($this->session->getUserId()))) {
            $user = $this->db->findOneById(PersonInterface::COLLECTION, $id, ["name", "language", "invitedBy", "email"]);
            $languageUser = $this->language->get();
            if ($this->dataValidator->email($user["email"]) == "") {
                if (! empty($user["language"])) {
                    $this->language->set($user["language"]);
                }

                $this->relaunchInvitePerson($user);
            }
            $this->language->set($languageUser);
        }
    }
}
