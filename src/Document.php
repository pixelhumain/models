<?php

namespace PixelHumain\Models;

use Exception;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\ClassifiedInterface;
use PixelHumain\Models\Interfaces\CmsInterface;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\NetworkInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PoiInterface;
use PixelHumain\Models\Interfaces\ProductInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Interfaces\RessourceInterface;
use PixelHumain\Models\Interfaces\ServiceInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;


use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\FsTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\FolderTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\FolderTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SurveyTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\SurveyTrait;
use PixelHumain\Models\Utils\ImagesUtils;

// TODO : Yii::get('url')::base()
// TODO : $this->params->get('uploadDir')
// TODO : Yii::get('application')->controller->module->assetsUrl
// TODO : new PNGMetaDataHandler

class Document extends BaseModel implements DocumentInterface, SurveyTraitInterface, ElementTraitInterface, AuthorisationTraitInterface, LinkTraitInterface, CostumTraitInterface, DocumentTraitInterface, PreferenceTraitInterface, FolderTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;
    use FsTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SurveyTrait;
    use ElementTrait;
    use AuthorisationTrait;
    use LinkTrait;
    use CostumTrait;
    use DocumentTrait;
    use PreferenceTrait;
    use FolderTrait;

    public static array $dataBinding = [
        "id" => [
            "name" => "id",
        ],
        "type" => [
            "name" => "type",
        ],
        "folder" => [
            "name" => "folder",
        ],
        "moduleId" => [
            "name" => "moduleId",
        ],
        "author" => [
            "name" => "author",
        ],
        "name" => [
            "name" => "name",
        ],
        "size" => [
            "name" => "size",
        ],
        "contentKey" => [
            "name" => "contentKey",
        ],
        "doctype" => [
            "name" => "doctype",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "created" => [
            "name" => "created",
        ],
        "subKey" => [
            "name" => "subKey",
        ],
        "source" => [
            "name" => "source",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "modified" => [
            "name" => "modified",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
        $this->validateFsProperty();
    }

    /**
     * Retrieves a document by its ID.
     *
     * @param string $id The ID of the document.
     * @return array|null The document data if found, null otherwise.
     */
    public function getById(string $id): ?array
    {
        try {
            return $this->db->findOne(DocumentInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
        } catch (Exception $e) {
            error_log((string) $e);
        }
    }

    /**
     * Get the documents that match the given parameters.
     *
     * @param array $params The parameters to filter the documents.
     * @return array The array of documents that match the given parameters.
     */
    public function getWhere(array $params): array
    {
        return $this->db->find(DocumentInterface::COLLECTION, $params);
    }

    /**
     * List documents by type for a specific user.
     *
     * @param string $userId The ID of the user.
     * @param string $type The type of the document.
     * @param string $contentKey The content key of the document.
     * @param array $sort The sorting criteria for the documents.
     * @return array The list of documents.
     */
    protected function listMyDocumentByType(string $userId, string $type, string $contentKey, array $sort): array
    {
        $params = [
            "id" => $userId,
            "type" => $type,
            "contentKey" => $contentKey,
        ];
        $listDocuments = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort);
        return $listDocuments;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    // TODO BOUBOULE - TO DELETE ONLY ONE DEPENDENCE WITH getListDocumentsByContentKey
    /**
     * List documents by content key for a specific user.
     *
     * @param string $userId The ID of the user.
     * @param string $contentKey The content key.
     * @param string|null $docType The document type (optional).
     * @param array $sort The sorting criteria.
     * @return array The list of documents.
     */
    protected function listMyDocumentByContentKey(string $userId, string $contentKey, ?string $docType = null, array $sort): array
    {
        $params = [
            "id" => $userId,
            "contentKey" => $contentKey,
        ];

        if (! empty($docType)) {
            $params["doctype"] = $docType;
        }

        $listDocuments = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort);
        return $listDocuments;
    }

    // TODO : n'est pas utilisé
    // public function listDocumentByCategory($collectionId, $type, $category, $sort = null)
    // {
    //     $params = [
    //         "id" => $collectionId,
    //         "type" => $type,
    //         "category" => $this->db->MongoRegex("/" . $category . "/i"),
    //     ];
    //     $listDocuments = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort);
    //     return $listDocuments;
    // }

    /**
     * Saves the document with the given parameters.
     *
     * @param array $params The parameters for saving the document.
     * @return array The saved document.
     */
    public function save(array $params): array
    {
        $src = null;
        //check content key
        if (! @$params["doctype"] && ! in_array(@$params["contentKey"], [DocumentInterface::IMG_BANNER, DocumentInterface::IMG_PROFIL, DocumentInterface::IMG_LOGO, DocumentInterface::IMG_SLIDER, DocumentInterface::IMG_MEDIA])) {
            throw new CTKException("Unknown contentKey " . $params["contentKey"] . " for the document !");
        }
        error_log("save xxxxxxxxxxxxxxxx");
        $new = [
            "id" => $params['id'],
            "type" => $params['type'],
            "folder" => $params['folder'],
            "moduleId" => $params['moduleId'],
            "author" => $params['author'],
            "name" => $params['name'],
            "size" => (int) $params['size'],
            "contentKey" => @$params["contentKey"],
            "doctype" => $params["doctype"],
            "collection" => DocumentInterface::COLLECTION,
            'created' => time(),
        ];
        if ($params["doctype"] == DocumentInterface::DOC_TYPE_FILE) {
            $new["contentKey"] = $this->getFileContentKey($params['name']);
        }
        if (@$params["folderId"]) {
            $new["folderId"] = $params["folderId"];
        }
        if (@$params["crop"]) {
            $new["crop"] = $params["crop"];
        }
        if (@$params["surveyId"]) {
            $new["surveyId"] = $params["surveyId"];
        }
        if (@$params["subKey"]) {
            $new["subKey"] = $params["subKey"];
        }
        if (@$params["restricted"]) {
            $new["restricted"] = $params["restricted"];
        }
        if (@$params["cryptage"]) {
            $new["cryptage"] = $params["cryptage"];
        }

        if (in_array($params["type"], [SurveyInterface::COLLECTION, SurveyInterface::CONTROLLER, ActionRoomInterface::COLLECTION, ActionRoomInterface::COLLECTION_ACTIONS])) {
            if ($params['type'] == SurveyInterface::COLLECTION || $params['type'] == SurveyInterface::CONTROLLER) {
                $elem = $this->getModelSurvey()->getById($params["id"]);
                $room = $this->getModelElement()->getByTypeAndId(ActionRoomInterface::COLLECTION, $elem["survey"]);
                $params["parentId"] = $room["parentId"];
                $params["parentType"] = $room["parentType"];
            }

            if ($params['type'] == ActionRoomInterface::TYPE_ACTION) {
                $params['type'] = ActionRoomInterface::COLLECTION_ACTIONS;
            }

            if (@$params["parentType"] == null || @$params["parentId"] == null) {
                $elem = $this->getModelElement()->getByTypeAndId($params['type'], $params["id"]);
                $params["parentId"] = $elem["parentId"];
                $params["parentType"] = $elem["parentType"];
            }

            if (! $this->getModelAuthorisation()->canEditItem($params['author'], $params['type'], $params['id'], @$params["parentType"], @$params["parentId"]) && ! $this->getModelAuthorisation()->canParticipate($params['author'], $params["type"], $params["id"])) {
                return [
                    "result" => false,
                    "type" => $params['type'],
                    "id" => @$params["id"],
                    "parentType" => @$params["parentType"],
                    "parentId" => @$params["parentId"],
                    "msg" => $this->language->t('document', "You are not allowed to modify the document of this item !"),
                ];
            }
        } elseif ($params["type"] != FormInterface::ANSWER_COLLECTION) {
            if (empty($params["forcedUnloggued"]) && ! $this->getModelAuthorisation()->canEditItem($params['author'], $params['type'], $params['id'])
                && ! $this->getModelAuthorisation()->isOpenEdition($params['id'], $params['type'])
                && ! $this->getModelAuthorisation()->canParticipate($params['author'], $params["type"], $params["id"])
                && (! @$params["formOrigin"] || ! $this->getModelLink()->isLinked($params['id'], $params['type'], $params['author']))) {
                if (@$params["formOrigin"] && $params["formOrigin"] == "news") {
                    return [
                        "result" => false,
                        "msg" => $this->language->t('document', "You have no rights upload document on this item, just write a message !"),
                    ];
                } else {
                    return [
                        "result" => false,
                        "msg" => $this->language->t('document', "You are not allowed to modify the document of this item !"),
                        "params" => $params,
                    ];
                }
            }
        }

        if (isset($params["category"]) && ! empty($params["category"])) {
            $new["category"] = $params["category"];
        }

        if ($new["contentKey"] == DocumentInterface::IMG_BANNER || $new["contentKey"] == DocumentInterface::IMG_PROFIL) {
            $this->db->update(
                DocumentInterface::COLLECTION,
                [
                    "id" => $new["id"],
                    "type" => $new["type"],
                    "contentKey" => $new["contentKey"],
                    "current" => [
                        '$exists' => true,
                    ],
                ],
                [
                    '$unset' => [
                        "current" => true,
                    ],
                ]
            );
            $new["current"] = true;
            if ($new["type"] == 'badges') {
                $id = $new["id"];
                $badgeElement = $this->db->findOneById(BadgeInterface::COLLECTION, $id);
                $baseDir = getcwd();
                $all = [];
                $all = array_merge($all, $this->db->find(OrganizationInterface::COLLECTION, [
                    'badges.' . $id => [
                        '$exists' => true,
                    ],
                ]));
                $all = array_merge($all, $this->db->find(PersonInterface::COLLECTION, [
                    'badges.' . $id => [
                        '$exists' => true,
                    ],
                ]));
                $all = array_merge($all, $this->db->find(ProjectInterface::COLLECTION, [
                    'badges.' . $id => [
                        '$exists' => true,
                    ],
                ]));
                if (isset($badgeElement["profilMediumImageUrl"])) {
                    foreach ($all as $award => $value) {
                        $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
                        $image = file_get_contents($baseDir . $badgeElement["profilMediumImageUrl"]);
                        $meta = new PNGMetaDataHandler($image);
                        $image_with_meta = $meta->add_chunks('iTXt', 'openbadges', $baseUrl . "/co2/badges/assertions/badge/" . (string) $badgeElement["_id"] . "/award/" . $award);
                        $first = substr($badgeElement["profilMediumImageUrl"], 0, strripos($badgeElement["profilMediumImageUrl"], "/"));
                        $saveDir = $baseDir . substr($first, 0, strripos($first, "/")) . "/" . "assertions/";
                        if (! is_dir($saveDir)) {
                            mkdir($saveDir, 0755, true);
                        }
                        file_put_contents($saveDir . $award . ".png", $image_with_meta);
                    }
                }
            }
        }
        $costum = $this->getModelCostum()->getCostum();
        if (empty($new["source"]) && ! empty($costum) && ! empty($costum["slug"])) {
            $new['source'] = [
                "key" => $costum["slug"],
                "insertOrign" => "costum",
            ];
        }

        $new = $this->db->insert(DocumentInterface::COLLECTION, $new);
        if ($new["doctype"] == DocumentInterface::DOC_TYPE_IMAGE) {
            if ($new["contentKey"] == DocumentInterface::IMG_BANNER) {
                // get banner image resize and crop
                $src = $this->generateBannerImages($new);
                // get normal image resize
                $this->generateAlbumImages($new);
                // get album image resize
                $this->generateAlbumImages($new, DocumentInterface::GENERATED_IMAGES_FOLDER);

                $typeNotif = "bannerImage";
            } else {
                //Generate small image

                $this->generateAlbumImages($new);
                //Generate image profil if necessary
                if ($new["contentKey"] == DocumentInterface::IMG_PROFIL) {
                    $src = $this->generateProfilImages($new);
                    $typeNotif = "profilImage";
                }
                if ($new["contentKey"] == DocumentInterface::IMG_SLIDER) {
                    $this->generateAlbumImages($new, DocumentInterface::GENERATED_MEDIUM_FOLDER);

                    $this->generateAlbumImages($new, DocumentInterface::GENERATED_IMAGES_FOLDER);
                    $typeNotif = "albumImage";
                }
            }
        }

        $survey = (@$new["surveyId"]) ? true : false;
        return [
            "result" => true,
            "msg" => $this->language->t('document', 'Document saved successfully'),
            "id" => $new["_id"],
            "name" => $new["name"],
            "src" => @$src,
            "docPath" => $this->getDocumentPath($new, true),
            "survey" => $survey,
        ];
    }

    /**
     * Get the doctype for the given name.
     *
     * @param string $strname The name of the document.
     * @return string The doctype for the given name.
     */
    public function getDoctype(string $strname): string
    {
        $supported_image = ['gif', 'jpg', 'jpeg', 'png'];

        $doctype = "";
        $ext = strtolower(pathinfo($strname, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
        if (in_array($ext, $supported_image)) {
            $doctype = "image";
        } else {
            $doctype = $ext;
        }
        return $doctype;
    }

    // TODO : $presentation_ext n'est pas utilisé
    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Returns the file content key for the given name.
     *
     * @param string $strname The name of the file.
     * @return string The file content key.
     */
    private function getFileContentKey(string $strname): string
    {
        $pdf_ext = ["pdf"];
        $spreadsheet_ext = ["xls", "xlsx", "ods"];
        $text_ext = ["doc", "docx", "odt"];
        $contentKey = "";
        $ext = strtolower(pathinfo($strname, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
        if (in_array($ext, $pdf_ext)) {
            $contentKey = "pdf";
        } elseif (in_array($ext, $spreadsheet_ext)) {
            $contentKey = "spreadsheet";
        } elseif (in_array($ext, $text_ext)) {
            $contentKey = "text";
        } else {
            $contentKey = "presentation";
        }
        return $contentKey;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /** TODO BOUBOULE
    *	TO DELETE --- NOT CORRECT BECAUSE OF CONTENTKEY WHICH IS A COMPLEX SEARCH WHEN IT COULD SIMPLE
    * 	Still present in city/detailAction, and survey/entryAction then impact on the rest of documents !!!
    * END TODO
     */
    /**
     * Retrieves a list of documents by content key.
     *
     * @param string $id The ID of the document.
     * @param string $contentKey The content key to search for.
     * @param string|null $docType The type of document (optional).
     * @param int|null $limit The maximum number of documents to retrieve (optional).
     * @return array The list of documents matching the content key.
     */
    private function getListDocumentsByContentKey(string $id, string $contentKey, ?string $docType = null, ?int $limit = null): array
    {
        $listDocuments = [];
        $sort = [
            'created' => -1,
        ];
        $explodeContentKey = explode(".", $contentKey);
        $listDocumentsofType = $this->listMyDocumentByContentKey($id, $explodeContentKey[0], $docType, $sort);
        foreach ($listDocumentsofType as $key => $value) {
            $toPush = false;
            if (isset($value["contentKey"]) && $value["contentKey"] != "") {
                $explodeValueContentKey = explode(".", $value["contentKey"]);
                $currentType = (string) $explodeValueContentKey[2];
                if (isset($explodeContentKey[1])) {
                    if ($explodeContentKey[1] == $explodeValueContentKey[1]) {
                        if (! isset($limit)) {
                            $toPush = true;
                        } else {
                            if (isset($limit[$currentType])) {
                                $limitByType = $limit[$currentType];
                                $actuelNbCurrentType = isset($listDocuments[$currentType]) ? count($listDocuments[$currentType]) : 0;
                                if ($actuelNbCurrentType < $limitByType) {
                                    $toPush = true;
                                }
                            } else {
                                $toPush = true;
                            }
                        }
                    }
                } else {
                    $toPush = true;
                }
            }
            if ($toPush) {
                $imageUrl = $this->getDocumentUrl($value);
                if (! isset($listDocuments[$currentType])) {
                    $listDocuments[$currentType] = [];
                }
                $value['imageUrl'] = $imageUrl;
                array_push($listDocuments[$currentType], $value);
            }
        }

        return $listDocuments;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Retrieves a simple document by the given conditions.
     *
     * @param array $where The conditions to filter the document.
     * @param array|null $fields The fields to include in the document (optional).
     * @return array The retrieved document.
     */
    private function getDocumentSimpleByWhere(array $where, ?array $fields = null): array
    {
        if (empty($fields)) {
            $fields = ["_id"];
        }
        $documents = $this->db->find(DocumentInterface::COLLECTION, $where, $fields);
        return $documents;
    }

    // TODO : n'est pas utilisé
    // /**
    //  * Updates the collection name of a document.
    //  *
    //  * @param string $targetId The ID of the target.
    //  * @param string $targetType The type of the target.
    //  * @param string $name The new name of the document.
    //  * @param string $docType The type of the document.
    //  * @param string|null $oldName The old name of the document (optional).
    //  * @return array The updated document.
    //  */
    // public function updateCollectionNameDocument(string $targetId, string $targetType, string $name, string $docType, ?string $oldName = null): array
    // {
    //     return $this->db->update(
    //         DocumentInterface::COLLECTION,
    //         [
    //             "id" => $targetId,
    //             "type" => $targetType,
    //             "docType" => $docType,
    //             "collection" => $oldName,
    //         ],
    //         [
    //             '$set' => [
    //                 "collection" => $name,
    //             ],
    //         ]
    //     );
    // }

    /**
     * Move a document to a collection.
     *
     * @param string $id The ID of the document.
     * @param string|null $name The name of the collection (optional).
     * @return array The updated document.
     */
    public function moveDocumentToCollection(string $id, ?string $name = null): array
    {
        return $this->db->update(
            DocumentInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    "collection" => $name,
                ],
            ]
        );
    }

    /**
     * Removes all documents of a specific type from a target.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $collection The name of the collection.
     * @param string $docType The type of the document (default: "image").
     * @return void
     */
    public function removeAllDocument(string $targetId, string $targetType, string $collection, string $docType = "image"): void
    {
        $where = [
            "type" => $targetType,
            "id" => $targetId,
            "docType" => $docType,
            "collection" => $collection,
        ];
        $docsToDelete = $this->getDocumentSimpleByWhere($where);
        foreach ($docsToDelete as $data) {
            $this->removeDocumentById((string) $data["_id"]);
        }
    }

    /**
     * Removes all documents associated with a specific element.
     *
     * @param string $targetId The ID of the target element.
     * @param string $targetType The type of the target element.
     * @return array The list of removed documents.
     */
    public function removeAllForElement(string $targetId, string $targetType): array
    {
        if ($this->getModelAuthorisation()->canEditItem($this->session->getUserId(), $targetType, $targetId)) {
            $remove_path_dir = (string) $this->params->get('uploadDir') . DocumentInterface::UPLOAD_DIR . "/" . $targetType . "/" . $targetId;
            $this->deleteDir($remove_path_dir);
            $this->db->remove(DocumentInterface::COLLECTION, [
                "type" => $targetType,
                "id" => $targetId,
            ]);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "All files concerning {what} has been successfully removed", [
                    "{what}" => $this->language->t("common", "the " . $this->getModelElement()->getControlerByCollection($targetType)),
                ]),
            ];
        }
        return [
            "resut" => false,
            "msg" => $this->language->t("common", "Can not update the element : you are not authorized to update that element !"),
        ];
    }

    /**
     * Deletes a directory and all its contents recursively.
     *
     * @param string $dirPath The path of the directory to delete.
     * @return void
     */
    private function deleteDir(string $dirPath): void
    {
        if ($this->fs->has($dirPath)) {
            $this->fs->delete($dirPath);
        }
    }

    // TODO : Yii::get('url')::base()
    /**
     * Lists documents by ID and type.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param array|string $contentKey The content key of the document (optional).
     * @param string|null $docType The document type (optional).
     * @param array $sort The sorting criteria for the documents (optional).
     * @return array An array of documents matching the given criteria.
     */
    public function listMyDocumentByIdAndType(string $id, string $type, $contentKey = null, ?string $docType = null, array $sort = []): array
    {
        $params = [
            "id" => $id,
            "type" => $type,
        ];
        if (! empty($contentKey)) {
            if (is_array($contentKey)) {
                $params["contentKey"] = [
                    '$in' => $contentKey,
                ];
            } else {
                $params["contentKey"] = $contentKey;
            }
        }

        if (! empty($docType)) {
            $params["doctype"] = $docType;
        }

        $listDocuments = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort);
        foreach ($listDocuments as $k => $img) {
            $imagePath = Yii::get('url')::base() . "/" . $this->params->get('uploadUrl') . $img["moduleId"] . "/" . $img["folder"];
            $listDocuments[$k]["path"] = $imagePath . "/" . $img["name"];
        }
        return $listDocuments;
    }

    /**
     * Get a list of documents based on specified conditions.
     *
     * @param array $where An array of conditions to filter the documents.
     * @param string|null $docType The type of document to filter by (optional).
     * @param int|null $limit The maximum number of documents to retrieve (optional).
     * @param int $index The starting index for pagination (default: 0).
     * @return array An array of documents matching the specified conditions.
     */
    public function getListDocumentsWhere(array $where, ?string $docType = null, ?int $limit = null, int $index = 0): array
    {
        $docs = $this->db->findAndSortAndLimitAndIndex(DocumentInterface::COLLECTION, $where, [
            'created' => -1,
        ], $limit, $index);
        if ($docType == "image") {
            $docs = $this->getListOfImage($docs);
        } elseif ($docType == "file") {
            foreach ($docs as $k => $file) {
                $docs[$k]["docPath"] = $this->getDocumentPath($file, true);
            }
        }
        return $docs;
    }

    // TODO : Yii::get('application')->controller->module->assetsUrl
    /**
     * Returns the last thumbnail for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string $contentKey The content key of the document.
     * @param string|null $collection The collection of the document (optional).
     * @return string The URL of the last thumbnail.
     */
    public function getLastThumb(string $id, string $type, string $contentKey, ?string $collection = null): string
    {
        $where = [
            "id" => $id,
            "type" => $type,
            "contentKey" => $contentKey,
        ];
        if (! empty($collection)) {
            $where["folderId"] = $collection;
        } elseif ($contentKey == DocumentInterface::IMG_SLIDER) {
            $where["folderId"] = [
                '$exists' => false,
            ];
        }
        $doc = $this->db->findOne(DocumentInterface::COLLECTION, $where);
        if (! empty($doc)) {
            $doc = $this->getListOfImage([$doc]);
            $url = ! empty($doc[0]["contentKey"]) && $doc[0]["contentKey"] == "profil" ? $doc[0]["imagePath"] : $doc[0]["imageThumbPath"];
        } else {
            $url = Yii::get('application')->controller->module->assetsUrl . '/images/thumbnail-default.jpg';
        }
        return $url;
    }

    /**
     * Counts the number of documents that match the specified criteria.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string|null $contentKey The content key of the document (optional).
     * @param string|null $col The column of the document (optional).
     * @param string $docType The type of the document (default: "image").
     * @return int The number of documents that match the criteria.
     */
    public function countByWhere(string $id, string $type, ?string $contentKey = null, ?string $col = null, string $docType = "image"): int
    {
        $where = [
            "id" => $id,
            "type" => $type,
            "doctype" => $docType,
        ];
        if (! empty($contentKey)) {
            $where["contentKey"] = $contentKey;
        }
        if (! empty($col)) {
            $where["folderId"] = $col;
        }
        return $this->db->count(DocumentInterface::COLLECTION, $where);
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Get the list of images from the given array of documents of a specific type.
     *
     * @param array $listDocumentsofType The array of documents of a specific type.
     * @return array The list of images.
     */
    public function getListOfImage(array $listDocumentsofType): array
    {
        $listDocuments = [];
        foreach ($listDocumentsofType as $key => $value) {
            $toPush = false;
            if (isset($value["contentKey"]) && $value["contentKey"] != "") {
                $currentContentKey = $value["contentKey"];
                if (! isset($limit)) {
                    $toPush = true;
                } else {
                    if (isset($limit[$currentContentKey])) {
                        $limitByType = $limit[$currentContentKey];
                        $actuelNbCurrentType = isset($listDocuments[$currentContentKey]) ? count($listDocuments[$currentContentKey]) : 0;
                        if ($actuelNbCurrentType < $limitByType) {
                            $toPush = true;
                        }
                    } else {
                        $toPush = true;
                    }
                }
            } else {
                $toPush = true;
            }
            if ($toPush) {
                $pushImage = [];
                if ($value["moduleId"] == "communevent") {
                    $pushImage['id'] = $value["objId"];
                    $imagePath = $this->params->get('communeventBaseUrl') . "/" . $value["folder"] . "/" . $value["name"];
                    $imageThumbPath = $imagePath . "?store=photosLarge";
                    $imageMediumPath = "";
                } else {
                    $pushImage['id'] = (string) $value["_id"];
                    $pushImage['_id'] = $value["_id"];
                    $imagePath = $this->getDocumentPath($value, true);
                    $imageThumbPath = ($value["contentKey"] == "profil") ? $this->getDocumentPath($value, true, DocumentInterface::GENERATED_MEDIUM_FOLDER . "/") : $this->getDocumentPath($value, true, DocumentInterface::GENERATED_IMAGES_FOLDER . "/");
                    $imageMediumPath = (file_exists($this->getDocumentPath($value, false, DocumentInterface::GENERATED_MEDIUM_FOLDER . "/"))) ? $this->getDocumentPath($value, true, DocumentInterface::GENERATED_MEDIUM_FOLDER . "/") : "";

                    $subKey = $value["subKey"] ?? "";
                }
                $pushImage['moduleId'] = $value["moduleId"];
                $pushImage['contentKey'] = $value["contentKey"];
                $pushImage['imagePath'] = $imagePath;
                $pushImage['imageThumbPath'] = $imageThumbPath;
                $pushImage['imageMediumPath'] = $imageMediumPath;
                $pushImage['subKey'] = $subKey;
                $pushImage['name'] = $value["name"];
                $pushImage['title'] = @$value["title"];
                $pushImage['size'] = $this->getHumanFileSize($value["size"]);
                array_push($listDocuments, $pushImage);
            }
        }
        return $listDocuments;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Returns the storage space occupied by a document with the given ID, type, and document type.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string $docType The document type.
     * @return int The storage space occupied by the document.
     */
    private function storageSpaceByIdAndType(string $id, string $type, string $docType): int
    {
        $params = [
            "id" => $id,
            "type" => $type,
        ];
        if (! empty($docType)) {
            $params["doctype"] = $docType;
        }

        $opts = [[
            '$match' => $params,
        ], [
            '$group' => [
                '_id' => $params,
                'sumDocSpace' => [
                    '$sum' => '$size',
                ],
            ],
        ]];
        $result = $this->db->aggregate(DocumentInterface::COLLECTION, $opts);
        $spaceUsed = 0;
        if (! empty($result) && ! empty($result["result"]) && ! empty($result["result"][0]) && ! empty($result["result"][0]["sumDocSpace"])) {
            $spaceUsed = (int) $result["result"][0]["sumDocSpace"];
        }
        return $spaceUsed;
    }

    // TODO : $docType is not used
    /**
     * Check if the user is authorized to stock a document.
     *
     * @param string $id The ID of the document.
     * @param string|null $type The type of the document.
     * @param string|null $docType The document type.
     * @return bool Returns true if the user is authorized to stock the document, false otherwise.
     */
    public function authorizedToStock(string $id, ?string $type, ?string $docType): bool
    {
        $authorizedToStock = false;
        $userId = $this->session->getUserId();
        if ($userId) {
            if (empty($type) || $type == "city") {
                $id = $userId;
                $type = PersonInterface::COLLECTION;
            }
            $storageSpace = $this->storageSpaceByIdAndType($id, $type, DocumentInterface::DOC_TYPE_IMAGE);
            $preferences = $this->getModelPreference()->getPreferencesByTypeId($id, $type);
            $initAuthorizedSpace = 20;
            if (! empty($preferences) && is_array($preferences) && ! empty($preferences["authorizedSpace"])) {
                $initAuthorizedSpace = (int) $preferences["authorizedSpace"];
            }
            $authorizedToStock = true;
            if ($storageSpace > ($initAuthorizedSpace * 1_048_576)) {
                $authorizedToStock = false;
            }
        }
        return $authorizedToStock;
    }

    // TODO : n'est pas utilisé
    // public function getListDocumentsURLByContentKey($id, $contentKey, $docType = null, $limit = null)
    // {
    //     $res = [];
    //     $listDocuments = $this->getListDocumentsByContentKey($id, $contentKey, $docType, $limit);
    //     foreach ($listDocuments as $contentKey => $documents) {
    //         foreach ($documents as $document) {
    //             if (! isset($res[$contentKey])) {
    //                 $res[$contentKey] = [];
    //             }
    //             array_push($res[$contentKey], $document["imageUrl"]);
    //         }
    //     }
    //     return $res;
    // }

    /**
     * Removes a document by its ID.
     *
     * @param string $id The ID of the document to remove.
     * @param bool $canDelete Whether the document can be deleted.
     * @return array The result of the removal operation.
     */
    public function removeDocumentById(string $id, bool $canDelete = false): array
    {
        $doc = $this->getById($id);
        if (! empty($doc) && ! empty($doc["id"]) && ! empty($doc["type"])) {
            if ($canDelete || $this->getModelAuthorisation()->canEditItem($this->session->getUserId(), (string) $doc["type"], (string) $doc["id"])) {
                // IF EXIST FILEPATH IN DOCUMENT STOCK, DELETE DOCUMENT IN UPLOAD FOLDER ELSE SIMPLY DELETE DOCUMENT IN DB
                $filepath = $this->getDocumentPath($doc);
                if (in_array($doc["contentKey"], [DocumentInterface::IMG_SLIDER, DocumentInterface::IMG_BANNER])) {
                    $filePathThumb = $this->getDocumentFolderPath($doc) . DocumentInterface::GENERATED_IMAGES_FOLDER . "/" . (string) $doc["name"];
                    $filePathMedium = $this->getDocumentFolderPath($doc) . DocumentInterface::GENERATED_MEDIUM_FOLDER . "/" . (string) $doc["name"];
                } elseif ($doc["contentKey"] == DocumentInterface::IMG_PROFIL) {
                    $filePathThumb = $this->getDocumentFolderPath($doc) . DocumentInterface::GENERATED_MEDIUM_FOLDER . "/" . (string) $doc["name"];
                }
                if ($this->fs->has($filepath)) {
                    if ($this->fs->delete($filepath)) {
                        if (isset($filePathThumb) && $this->fs->has($filePathThumb)) {
                            $this->fs->delete($filePathThumb);
                        }
                        if (isset($filePathMedium) && $this->fs->has($filePathMedium)) {
                            $this->fs->delete($filePathMedium);
                        }
                        if (@$doc["current"]) {
                            // iF GET CURRENT DELETE PATH IN COLLECTION OF ELEMENT
                            if ($doc["contentKey"] == DocumentInterface::IMG_PROFIL) {
                                $unset = [
                                    "profilImageUrl" => true,
                                    "profilThumbImageUrl" => true,
                                    "profilMarkerImageUrl" => true,
                                    "profilMediumImageUrl" => true,
                                ];
                            } elseif ($doc["contentKey"] == DocumentInterface::IMG_BANNER) {
                                $unset = [
                                    "profilBannerUrl" => true,
                                    "profilRealBannerUrl" => true,
                                ];
                            }
                            $this->db->update(
                                $doc["type"],
                                [
                                    "_id" => $this->db->MongoId($doc["id"]),
                                ],
                                [
                                    '$unset' => $unset,
                                ]
                            );
                        }
                        $this->db->remove(DocumentInterface::COLLECTION, [
                            "_id" => $this->db->MongoId($id),
                        ]);
                        $res = [
                            'result' => true,
                            "msg" => $this->language->t("document", "Document deleted"),
                            "id" => $id,
                        ];
                    } else {
                        $res = [
                            'result' => false,
                            'msg' => $this->language->t("common", "Something went wrong!"),
                            "filepath" => $filepath,
                        ];
                    }
                } else {
                    //even if The file does not exists on the filesystem : we try to delete the document on mongo
                    $this->db->remove(DocumentInterface::COLLECTION, [
                        "_id" => $this->db->MongoId($id),
                    ]);
                    $res = [
                        'result' => true,
                        "msg" => $this->language->t("document", "Document deleted"),
                        "id" => $id,
                    ];
                }
            } else {
                $res = [
                    'result' => false,
                    'error' => $this->language->t("common", "You are not allowed to delete this document"),
                    "id" => $id,
                ];
            }
        } else {
            $res = [
                'result' => false,
                'error' => $this->language->t("common", "Something went wrong!"),
                "id" => $id,
            ];
        }

        return $res;
    }

    // TODO : $results is not used
    /**
     * Removes a document by folder.
     *
     * @param string $folder The folder of the document.
     * @return array An array containing the removed document.
     */
    public function removeDocumentByFolder(string $folder): array
    {
        //TODO SBAR - Generate new thumbs if the image is the current image
        $docs = $this->getWhere([
            "folder" => $folder,
        ]);
        if (! empty($docs)) {
            $results = [];
            //delete all entries in DB
            foreach ($docs as $key => $doc) {
                $this->db->remove(DocumentInterface::COLLECTION, [
                    "_id" => $key,
                ]);
                $results[$key] = [
                    'result' => true,
                    "entry" => "deleted",
                ];
            }

            $folder = $this->getDocumentFolderPath((array) $doc);
            //delete folder from disk recursively
            if ($this->fs->has($folder)) {
                $this->fs->delete($folder);
                $results["folder"] = "deleted";
            } else {
                $results["folder"] = "something went wrong";
            }

            $res = [
                'result' => true,
                "msg" => $this->language->t("document", "Document deleted"),
                "results" => $results,
            ];
        } else {
            $res = [
                'result' => true,
                "msg" => $this->language->t("document", "no Documents associated"),
            ];
        }

        return $res;
    }

    /**
     * Removes a document communevent by its object ID.
     *
     * @param string $id The object ID of the document communevent.
     * @param string $userId The user ID.
     * @return array|null The removed document communevent, or null if not found.
     */
    public function removeDocumentCommuneventByObjId(string $id, string $userId): ?array
    {
        $res = null;
        $doc = $this->getById($id);
        if (! empty($doc) && ! empty($doc["id"]) && ! empty($doc["type"])) {
            if ($this->getModelAuthorisation()->canEditItem($userId, (string) $doc["type"], (string) $doc["id"])) {
                //Suppression de l'image dans la collection cfs.photosimg.filerecord
                $this->db->remove("cfs.photosimg.filerecord", [
                    "_id" => $id,
                ]);
                //Suppression du document
                $this->db->remove(DocumentInterface::COLLECTION, [
                    "objId" => $id,
                ]);
                $res = [
                    'result' => true,
                    "msg" => $this->language->t("document", "Document deleted"),
                    "id" => $id,
                ];
            } else {
                $res = [
                    'result' => false,
                    "msg" => $this->language->t("document", "You are not allowed to delete this document !"),
                    "id" => $id,
                ];
            }
        }
        return $res;
    }

    // TODO : n'est pas utilisé
    // public function setImagePath(string $itemId, string $itemType, string $path, string $contentKey): array
    // {
    //     $tabImage = explode('.', $contentKey);

    //     if (in_array(DocumentInterface::IMG_PROFIL, $tabImage)) {
    //         return $this->db->update(
    //             $itemType,
    //             [
    //                 "_id" => $this->db->MongoId($itemId),
    //             ],
    //             [
    //                 '$set' => [
    //                     "imagePath" => $path,
    //                 ],
    //             ]
    //         );
    //     }
    // }

    /**
     * Update a document.
     *
     * @param string $id The ID of the document to update.
     * @param array|null $update The data to update the document with.
     * @return bool Returns true if the document was successfully updated, false otherwise.
     */
    public function update(string $id, ?array $update): bool
    {
        $set = [];
        if (! empty($update["title"])) {
            $set["title"] = $update["title"];
        }
        $this->db->update(
            DocumentInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => $set,
            ]
        );
        return true;
    }

    /**
     * Retrieves images by key.
     *
     * @param string $itemId The ID of the item.
     * @param string $itemType The type of the item.
     * @param array $limit The limit of images to retrieve.
     * @return array The array of images.
     */
    public function getImagesByKey(string $itemId, string $itemType, array $limit): array
    {
        $imageUrl = "";
        $res = [];

        foreach ($limit as $key => $aLimit) {
            $sort = [
                'created' => -1,
            ];
            $params = [
                "id" => $itemId,
                "type" => $itemType,
                "contentKey" => $key,
            ];
            $listImagesofType = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort, (int) $aLimit);

            $arrayOfImagesPath = [];
            foreach ($listImagesofType as $id => $document) {
                $imageUrl = $this->getDocumentUrl((array) $document);
                array_push($arrayOfImagesPath, $imageUrl);
            }
            $res[(string) $key] = $arrayOfImagesPath;
        }

        return $res;
    }

    /**
     * Retrieves the last image URL associated with a specific key and subkey (optional) for an item.
     *
     * @param string $itemId The ID of the item.
     * @param string $itemType The type of the item.
     * @param string $key The key associated with the image.
     * @param string|null $subKey The subkey associated with the image (optional).
     * @return string The URL of the last image.
     */
    public function getLastImageByKey(string $itemId, string $itemType, string $key, ?string $subKey = null): string
    {
        $imageUrl = "";
        $sort = [
            'created' => -1,
        ];
        $params = [
            "id" => $itemId,
            "type" => $itemType,
            "contentKey" => $key,
        ];
        if (! empty($subKey)) {
            $params["subKey"] = $subKey;
        }

        $listImagesofType = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort, 1);

        foreach ($listImagesofType as $key => $value) {
            $imageUrl = $this->getDocumentUrl((array) $value);
        }
        return $imageUrl;
    }

    // TODO : $sort n'est pas utilisé, j'ai enlevé
    /**
     * Returns the available categories for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @return array The available categories for the document.
     */
    public function getAvailableCategories(string $id, string $type): array
    {
        $params = [
            "id" => $id,
            "type" => $type,
        ];
        $listCategory = $this->db->distinct(DocumentInterface::COLLECTION, "category", $params);

        return $listCategory;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Returns the human-readable file size for the given number of bytes.
     *
     * @param int $bytes The number of bytes.
     * @param int $decimals The number of decimal places to round the result to (optional, default is 2).
     * @return string The human-readable file size.
     */
    private function getHumanFileSize(int $bytes, int $decimals = 2): string
    {
        if ($bytes < 1024) {
            return $bytes . ' B';
        }

        $units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'];
        $i = 0;

        while ($bytes >= 1024 && $i < count($units) - 1) {
            $bytes /= 1024;
            $i++;
        }

        return sprintf("%.{$decimals}f ", $bytes) . $units[$i];
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Cleans the given string.
     *
     * @param string $string The string to be cleaned.
     * @return string The cleaned string.
     */
    private function clean(string $string): string
    {
        $string = preg_replace('/  */', '-', $string);
        $string = strtr($string, 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    /**
     * Returns the URL of the document.
     *
     * @param array $document The document data.
     * @return string The URL of the document.
     */
    public function getDocumentUrl(array $document): string
    {
        return $this->getDocumentFolderUrl($document) . "/" . (string) $document["name"];
    }

    /**
     * Returns the URL of the document folder.
     *
     * @param array $document The document data.
     * @return string The URL of the document folder.
     */
    public function getDocumentFolderUrl(array $document): string
    {
        if ($document["moduleId"] == "communevent") {
            $folderUrl = $this->params->get('communeventUrl');
        } else {
            $folderUrl = "/" . (string) $this->params->get('uploadUrl') . (string) $document["moduleId"];
        }
        $folderUrl .= "/" . (string) $document["folder"];

        if (! empty($document["folderId"])) {
            $folderUrl .= "/" . (string) $document["folderId"];
        }

        return $folderUrl;
    }

    /**
     * Returns the path of a document.
     *
     * @param array $document The document data.
     * @param bool $imgPath Whether to include the image path.
     * @param string $thumb The thumbnail size.
     * @return string The path of the document.
     */
    public function getDocumentPath(array $document, bool $imgPath = false, string $thumb = ""): string
    {
        return $this->getDocumentFolderPath($document, $imgPath) . $thumb . (string) $document["name"];
    }

    // TODO : Yii::get('url')::base()
    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Returns the document folder path for a given document.
     *
     * @param array $document The document data.
     * @param bool $imgPath Whether to include the image path in the folder path.
     * @return string The document folder path.
     */
    private function getDocumentFolderPath(array $document, bool $imgPath = false): string
    {
        $path = ($imgPath) ? Yii::get('url')::base() . "/" . (string) $this->params->get('uploadUrl') : $this->params->get('uploadDir');
        $path .= (string) $document["moduleId"] . "/" . (string) $document["folder"] . "/";

        if (isset($document["folderId"])) {
            $path .= $this->getModelFolder()->getParentFoldersPath((string) $document["folderId"]);
        }
        return $path;
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Generates profile images based on the given document.
     *
     * @param array $document The document data.
     * @return array The generated profile images.
     */
    private function generateProfilImages(array $document): array
    {
        $changes = [];
        $dir = $document["moduleId"];
        $folder = $document["folder"];

        $upload_dir = $dir . '/' . $folder . '/' . DocumentInterface::GENERATED_IMAGES_FOLDER;
        $upload_dir_medium = $dir . '/' . $folder . '/' . DocumentInterface::GENERATED_MEDIUM_FOLDER;
        if ($this->fs->has($upload_dir)) {
            if ($this->fs->has($upload_dir . "bck")) {
                $this->fs->delete($upload_dir . "bck");

                $this->fs->move($upload_dir, $upload_dir . "bck");
            }
        }

        $this->fs->createDirectory($upload_dir);
        // Medium Image
        if (! $this->fs->has($upload_dir_medium)) {
            $this->fs->createDirectory($upload_dir_medium);
        }
        //GET THUMB IMAGE
        $profilUrl = $this->getDocumentUrl($document);
        $profilPath = $this->getDocumentPath($document);
        $imageUtils = new ImagesUtils($this->fs, $profilPath);
        $destPathThumb = $upload_dir . "/" . DocumentInterface::FILENAME_PROFIL_RESIZED;
        $profilThumbUrl = $this->getDocumentFolderUrl($document) . "/" . DocumentInterface::GENERATED_IMAGES_FOLDER . "/" . DocumentInterface::FILENAME_PROFIL_RESIZED;
        $imageUtils->resizeImage(50, 50)->savePng($destPathThumb);
        //GET MEDIUM IMAGE
        $imageMediumUtils = new ImagesUtils($this->fs, $profilPath);
        $destPathMedium = $upload_dir_medium . "/" . $document["name"];
        $profilMediumUrl = $this->getDocumentFolderUrl($document) . "/" . DocumentInterface::GENERATED_MEDIUM_FOLDER . "/" . $document["name"];
        $query = 'badges';
        if (substr($folder, 0, strlen($query)) === $query) {
            $imageMediumUtils->resizeIntoBadge()->save($destPathMedium, 100);
        } else {
            $imageMediumUtils->resizePropertionalyImage(400, 400)->save($destPathMedium, 100);
        }

        $destPathMarker = $upload_dir . "/" . DocumentInterface::FILENAME_PROFIL_MARKER;
        $profilMarkerImageUrl = $this->getDocumentFolderUrl($document) . "/" . DocumentInterface::GENERATED_IMAGES_FOLDER . "/" . DocumentInterface::FILENAME_PROFIL_MARKER;
        $markerFileName = $this->getEmptyMarkerFileName(@$document["type"], @$document["subType"]);
        if ($markerFileName) {
            $srcEmptyMarker = $this->getPathToMarkersAsset() . $markerFileName;
            $imageUtils->createMarkerFromImage($srcEmptyMarker)->save($destPathMarker);
        }

        //Update the entity collection to store the path of the profil images
        $allowedElements = [PersonInterface::COLLECTION, OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION, ProductInterface::COLLECTION, ServiceInterface::COLLECTION, EventInterface::COLLECTION, SurveyInterface::COLLECTION, FormInterface::ANSWER_COLLECTION, ActionRoomInterface::COLLECTION, ProposalInterface::COLLECTION, ActionRoomInterface::COLLECTION_ACTIONS, PoiInterface::COLLECTION, CmsInterface::COLLECTION, ClassifiedInterface::COLLECTION, NetworkInterface::COLLECTION, BadgeInterface::COLLECTION];

        if (isset($profilUrl) && in_array($document["type"], $allowedElements)) {
            $changes = [];
            if (isset($profilUrl)) {
                $changes["profilImageUrl"] = $profilUrl;
            }
            if (isset($profilMediumUrl)) {
                $changes["profilMediumImageUrl"] = $profilMediumUrl;
            }
            if (isset($profilThumbUrl)) {
                $changes["profilThumbImageUrl"] = $profilThumbUrl . "?t=" . time();
            }
            if (isset($profilMarkerImageUrl)) {
                $changes["profilMarkerImageUrl"] = $profilMarkerImageUrl;
            }
            $this->db->update($document["type"], [
                "_id" => $this->db->MongoId($document["id"]),
            ], [
                '$set' => $changes,
            ]);

            error_log("The entity " . $document["type"] . " and id " . $document["id"] . " has been updated with the URL of the profil images.");
        }

        if ($this->fs->has($upload_dir . "bck")) {
            $this->fs->delete($upload_dir . "bck");
        }
        return [
            "result" => true,
            "msg" => "Thumb and markers have been generated",
            "changes" => @$changes,
        ];
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Generates album images for a given document.
     *
     * @param array $document The document data.
     * @param string|null $folderAlbum The folder for the album images (optional).
     * @return void
     */
    private function generateAlbumImages(array $document, ?string $folderAlbum = null): void
    {
        $dir = (string) $document["moduleId"];
        $folder = (string) $document["folder"];
        $folderPath = "";
        if (@$document["folderId"]) {
            $folderPath = substr("/" . $this->getModelFolder()->getParentFoldersPath((string) $document["folderId"]), 0, -1);
        }
        if ($folderAlbum == DocumentInterface::GENERATED_IMAGES_FOLDER) {
            $destination = '/' . DocumentInterface::GENERATED_IMAGES_FOLDER;
            $maxWidth = 200;
            $maxHeight = 200;
            $quality = 100;
        } elseif ($folderAlbum == DocumentInterface::GENERATED_MEDIUM_FOLDER) {
            $destination = '/' . DocumentInterface::GENERATED_MEDIUM_FOLDER;
            $maxWidth = 500;
            $maxHeight = 500;
            $quality = 100;
        } else {
            $destination = "";
            $maxWidth = 1100;
            $maxHeight = 700;
            $quality = 80;
        }
        //The images will be stored in the /uploadDir/moduleId/ownerType/ownerId/thumb (ex : /upload/communecter/citoyen/1242354235435/thumb)
        $upload_dir = $dir . '/' . $folder . $folderPath . $destination;

        if (! $this->fs->has($upload_dir)) {
            $this->fs->createDirectory($upload_dir);
        }

        $path = $this->getDocumentPath($document);
        $imageUtils = new ImagesUtils($this->fs, $path);
        if ($imageUtils->source_width > $maxWidth || $imageUtils->source_height > $maxHeight) {
            $destPathThumb = $upload_dir . "/" . (string) $document["name"];
            if (in_array($folderAlbum, [DocumentInterface::GENERATED_IMAGES_FOLDER])) {
                $imageUtils->resizeImage($maxWidth, $maxHeight)->save($destPathThumb);
            } else {
                $imageUtils->resizePropertionalyImage($maxWidth, $maxHeight)->save($destPathThumb, strval($quality));
            }
        }
    }

    // TODO : n'est utilisé que dans la class changer la visibilité en private
    /**
     * Generates banner images for a given document.
     *
     * @param array $document The document data.
     * @return array|void
     */
    private function generateBannerImages(array $document)
    {
        $dir = (string) $document["moduleId"];
        $folder = (string) $document["folder"];

        $upload_dir = $dir . '/' . $folder . "/resized";

        if ($this->fs->has($upload_dir)) {
            if ($this->fs->has($upload_dir . "bck")) {
                $this->fs->delete($upload_dir . "bck");
                $this->fs->move($upload_dir, $upload_dir . "bck");
            }
        } else {
            $this->fs->createDirectory($upload_dir);
        }
        $path = $this->getDocumentPath($document);
        $profilBannerUrl = $this->getDocumentFolderUrl($document) . "/resized/" . DocumentInterface::FILENAME_PROFIL_BANNER . "?t=" . time();

        $imageUtils = new ImagesUtils($this->fs, $path);
        $destPathThumb = $upload_dir . "/" . DocumentInterface::FILENAME_PROFIL_BANNER;

        $crop = $document["crop"];
        $imageUtils->imagecropping($crop["cropW"], $crop["cropH"], $crop["cropX"], $crop["cropY"])->save($destPathThumb, 100);

        $allowedElements = [PersonInterface::COLLECTION, OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION, EventInterface::COLLECTION, PoiInterface::COLLECTION, CmsInterface::COLLECTION, ClassifiedInterface::COLLECTION, FormInterface::COLLECTION];
        if (! empty($profilBannerUrl) && in_array($document["type"], $allowedElements)) {
            $changes = [];
            $changes["profilBannerUrl"] = $profilBannerUrl;
            $changes["profilRealBannerUrl"] = $this->getDocumentUrl($document);
            $this->db->update($document["type"], [
                "_id" => $this->db->MongoId($document["id"]),
            ], [
                '$set' => $changes,
            ]);

            error_log("The entity " . $document["type"] . " and id " . $document["id"] . " has been updated with the URL of the profil images.");
            if ($this->fs->has($upload_dir . "bck")) {
                $this->fs->delete($upload_dir . "bck");
            }
            return $changes;
        }
    }

    // TODO : Yii::get('application')->controller->module->assetsUrl
    /**
     * Returns the URL of the generated image for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string $generatedImageType The type of the generated image.
     * @param string|null $subType The subtype of the document (optional).
     * @return string|null The URL of the generated image, or null if it doesn't exist.
     */
    public function getGeneratedImageUrl(string $id, string $type, string $generatedImageType, ?string $subType = null): ?string
    {
        $res = null;
        $sort = [
            'created' => -1,
        ];
        $params = [
            "id" => $id,
            "type" => $type,
            "contentKey" => DocumentInterface::IMG_PROFIL,
        ];
        $listDocuments = $this->db->findAndSort(DocumentInterface::COLLECTION, $params, $sort, 1);

        $generatedImageExist = false;
        if ($lastProfilImage = reset($listDocuments)) {
            $documentPath = $this->getDocumentFolderPath($lastProfilImage);
            if ($generatedImageType == DocumentInterface::GENERATED_THUMB_PROFIL) {
                $documentPath = $documentPath . '/' . DocumentInterface::GENERATED_THUMB_PROFIL . '/' . DocumentInterface::FILENAME_PROFIL_RESIZED . "?t=" . time();
            } elseif ($generatedImageType == DocumentInterface::GENERATED_MARKER) {
                $documentPath = $documentPath . '/' . DocumentInterface::GENERATED_THUMB_PROFIL . '/' . DocumentInterface::FILENAME_PROFIL_MARKER;
            } elseif ($generatedImageType == DocumentInterface::GENERATED_MEDIUM_FOLDER) {
                $documentPath = $documentPath . '/' . DocumentInterface::GENERATED_MEDIUM_FOLDER . '/' . $lastProfilImage["name"];
            }
            $generatedImageExist = $this->fs->has($documentPath);
        }

        //If there is an existing profil image
        if ($generatedImageExist) {
            $documentUrl = $this->getDocumentFolderUrl($lastProfilImage);
            $documentThumb = $documentUrl . '/thumb/';
            $documentMedium = $documentUrl . '/' . DocumentInterface::GENERATED_MEDIUM_FOLDER . '/';
            if ($generatedImageType == DocumentInterface::GENERATED_THUMB_PROFIL) {
                $res = $documentThumb . DocumentInterface::FILENAME_PROFIL_RESIZED;
            } elseif ($generatedImageType == DocumentInterface::GENERATED_MARKER) {
                $res = $documentThumb . DocumentInterface::FILENAME_PROFIL_MARKER;
            } elseif ($generatedImageType == DocumentInterface::GENERATED_MEDIUM_FOLDER) {
                $res = $documentMedium . $lastProfilImage["name"];
            }

            //Else the default image is returned
        } else {
            if ($generatedImageType == DocumentInterface::GENERATED_MARKER) {
                $markerDefaultName = str_replace("empty", "default", $this->getEmptyMarkerFileName($type, $subType));

                $res = "/" . Yii::get('application')->controller->module->assetsUrl . "/images/sig/markers/icons_carto/" . $markerDefaultName;
            } else {
                $res = "";
            }
        }
        return $res;
    }

    /**
     * Returns the file name for an empty marker based on the given type and subtype.
     *
     * @param string $type The type of the marker.
     * @param string|null $subType The subtype of the marker (optional).
     * @return string The file name for the empty marker.
     */
    private function getEmptyMarkerFileName(string $type, ?string $subType = null): string
    {
        $markerFileName = "";

        switch ($type) {
            case PersonInterface::COLLECTION:
                $markerFileName = "citizen-marker-empty.png";
                break;
            case OrganizationInterface::COLLECTION:
                if ($subType == "NGO") {
                    $markerFileName = "ngo-marker-empty.png";
                } elseif ($subType == "LocalBusiness") {
                    $markerFileName = "business-marker-empty.png";
                } else {
                    $markerFileName = "ngo-marker-empty.png";
                }
                break;
            case EventInterface::COLLECTION:
                $markerFileName = "event-marker-empty.png";
                break;
            case RessourceInterface::COLLECTION:
                $markerFileName = "ressource-marker-empty.png";
                break;
            case ClassifiedInterface::COLLECTION:
                $markerFileName = "classified-marker-empty.png";
                break;
            case ProjectInterface::COLLECTION:
                $markerFileName = "project-marker-empty.png";
                break;
            case CityInterface::COLLECTION:
                $markerFileName = "city-marker-empty.png";
                break;
            case PoiInterface::COLLECTION:
                if ($subType != null) {
                    $markerFileName = "poi-" . $subType . "-marker-empty.png";
                } else {
                    $markerFileName = "poi-marker-empty.png";
                }
                break;
            case CmsInterface::COLLECTION:
                $markerFileName = "poi-marker-empty.png";
                break;
        }

        return $markerFileName;
    }

    // TODO : Yii::get('application')->controller->module->id
    /**
     * Returns the path to the markers asset.
     *
     * @return string The path to the markers asset.
     */
    private function getPathToMarkersAsset(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." .
                DIRECTORY_SEPARATOR . Yii::get('application')->controller->module->id . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR .
                "images" . DIRECTORY_SEPARATOR . "sig" . DIRECTORY_SEPARATOR . "markers" . DIRECTORY_SEPARATOR .
                "icons_carto" . DIRECTORY_SEPARATOR;
    }

    /**
     * Retrieve all images URL for a given ID, type, and entity.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param array|null $entity The entity associated with the document (optional).
     * @return array An array of URLs for the images.
     */
    public function retrieveAllImagesUrl(string $id, string $type, ?array $entity = null): array
    {
        $res = [];

        if (isset($entity["profilImageUrl"])) {
            if (! empty($entity["profilImageUrl"])) {
                $res["profilImageUrl"] = $entity["profilImageUrl"];
            }
            if (! empty($entity["profilThumbImageUrl"])) {
                $res["profilThumbImageUrl"] = $entity["profilThumbImageUrl"];
            }
            if (! empty($entity["profilMarkerImageUrl"])) {
                $res["profilMarkerImageUrl"] = $entity["profilMarkerImageUrl"];
            }
            if (! empty($entity["profilBannerUrl"])) {
                $res["profilBannerUrl"] = $entity["profilBannerUrl"];
            }
            if (! empty($entity["profilRealBannerUrl"])) {
                $res["profilRealBannerUrl"] = $entity["profilRealBannerUrl"];
            }
            if (! empty($entity["profilMediumImageUrl"])) {
                $res["profilMediumImageUrl"] = (string) $entity["profilMediumImageUrl"] . "?_=" . time();
            }

            //If empty than retrieve the URLs from document and store them in the entity for next time
        } else {
            $profil = $this->getLastImageByKey($id, $type, DocumentInterface::IMG_PROFIL);

            if (! empty($profil)) {
                $profilThumb = $this->getGeneratedImageUrl($id, $type, DocumentInterface::GENERATED_THUMB_PROFIL);
                $profilMedium = $this->getGeneratedImageUrl($id, $type, DocumentInterface::GENERATED_MEDIUM_FOLDER);
                if ($profil != "") {
                    $marker = $this->getGeneratedImageUrl($id, $type, DocumentInterface::GENERATED_MARKER);
                } else {
                    $marker = "";
                }

                $this->db->update(
                    $type,
                    [
                        "_id" => $this->db->MongoId($id),
                    ],
                    [
                        '$set' => [
                            "profilImageUrl" => $profil,
                            "profilThumbImageUrl" => $profilThumb,
                            "profilMarkerImageUrl" => $marker,
                            "profilMediumImageUrl" => $profilMedium,
                        ],
                    ]
                );
                error_log("Add Profil image url for the " . $type . " with the id " . $id);
            }

            $res["profilImageUrl"] = $profil;
            //Add a time to force relaod of generated images
            $res["profilThumbImageUrl"] = ! empty($profilThumb) ? $profilThumb . "?_=" . time() : "";
            $res["profilMarkerImageUrl"] = ! empty($marker) ? $marker . "?_=" . time() : "";
            $res["profilMediumImageUrl"] = ! empty($profilMedium) ? $profilMedium . "?_=" . time() : "";
        }
        return $res;
    }

    // TODO : n'est pas utilisé
    // public function getImageByUrl(string $urlImage, $path, $nameImage)
    // {
    //     // Ouvre un fichier pour lire un contenu existant
    //     $current = file_get_contents($urlImage);
    //     // Écrit le résultat dans le fichier
    //     $file = "../../modules/cityData/" . $nameImage;
    //     file_put_contents($file, $current);
    // }

    /**
     * Retrieves the contents of a URL and returns them as an array.
     *
     * @param string $url The URL to retrieve the contents from.
     * @return array The contents of the URL as an array.
     */
    public function urlGetContents(string $url): array
    {
        $output = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output["file"] = curl_exec($ch);
        $output["size"] = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
        curl_close($ch);
        return $output;
    }

    /**
     * Check if a URL exists.
     *
     * @param string $url The URL to check.
     * @return bool Returns true if the URL exists, false otherwise.
     */
    public function urlExists(string $url): bool
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ($code == 200);
    }

    // TODO : $contentKey n'est pas utilisé je l'ai enlevé
    // TODO : notEmpty la fonction n'existe pas donc je suis pas sur que cette fonction fonctionne
    // TODO : $extraParams n'est pas utilisé donc j'enlève
    /**
     * Uploads a document from a URL.
     *
     * @param string $dir The directory where the document will be saved.
     * @param string $folder The folder where the document will be stored.
     * @param string $ownerId The ID of the document owner.
     * @param string $input The input data for the document.
     * @param bool $rename Whether to rename the document or not.
     * @param string $urlFile The URL of the document to be uploaded.
     * @param string $nameFile The name of the document.
     * @return array An array containing the result of the upload process.
     */
    public function uploadDocumentFromURL(string $dir, string $folder, string $ownerId, string $input, bool $rename = false, string $urlFile, string $nameFile): array
    {
        $file_headers = @get_headers($urlFile);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $exists = false;
        } else {
            $exists = true;
        }
        if ($exists) {
            $file = $this->urlGetContents($urlFile);
            $sendParams = [
                "dir" => $dir,
                "typeEltFolder" => $folder,
                "idEltFolder" => $ownerId,
                "nameFile" => $nameFile,
                "sizeUrl" => $file["size"],
            ];
            $res = $this->checkFileRequirements($file["file"], $input, $sendParams);
            if ($res["result"]) {
                $res = $this->uploadDocument($file, $res["uploadDir"], $input, $rename, $nameFile, $file["size"]);
            }
            return $res;
        } else {
            return [
                "resultUpload" => false,
                "msg" => "File does not exists",
            ];
        }
    }

    // TODO : $input, $sizeUrl n'est pas utilisé
    /**
     * Uploads a document.
     *
     * @param array $file The file to upload.
     * @param string $uploadDir The directory where the file will be uploaded.
     * @param mixed $input The input data.
     * @param bool $rename Whether to rename the file or not. Default is false.
     * @param string|null $nameUrl The URL of the file name. Default is null.
     * @param string|null $sizeUrl The URL of the file size. Default is null.
     * @param mixed|null $forcedUnloggued The forced unlogged value. Default is null.
     * @param bool $cryptage Whether to encrypt the file or not. Default is false.
     * @return array The result of the upload.
     */
    public function uploadDocument(array $file, string $uploadDir, $input, bool $rename = false, ?string $nameUrl = null, ?string $sizeUrl = null, $forcedUnloggued = null, bool $cryptage = false)
    {
        $ext = null;
        $uploadedFile = (! empty($file['tmp_name']) ? true : false);
        $nameFile = (! empty($nameUrl) ? $nameUrl : $file["name"]);

        $cleanfileName = $this->clean(pathinfo($nameFile, PATHINFO_FILENAME)) . "." . pathinfo($nameFile, PATHINFO_EXTENSION);
        $name = ($rename) ? $this->session->getUserId() . '.' . $ext : $cleanfileName;
        if ($this->fs->has($uploadDir . $name)) {
            $name = time() . "_" . $name;
        }

        if (($this->session->getUserId() || $forcedUnloggued) && $name) {
            if ($uploadedFile) {
                $stream = fopen($file['tmp_name'], 'r+');
                $this->fs->writeStream($uploadDir . $name, $stream, [
                    'visibility' => 'public',
                ]);

                if (is_resource($stream)) {
                    fclose($stream);
                }

                $size = (int) $this->fs->fileSize($uploadDir . $name);
                if ($cryptage) {
                    $name = $this->encryptFile($uploadDir . $name, $uploadDir . $name);
                }
            } else {
                $this->fs->write($uploadDir . $name, $file, [
                    'visibility' => 'public',
                ]);
                $size = (int) $this->fs->fileSize($uploadDir . $name);
            }
            return [
                'result' => true,
                "success" => true,
                'name' => $name,
                'uploadDir' => $uploadDir,
                'size' => $size,
            ];
        }

        return [
            'result' => false,
            'error' => $this->language->t("document", "Something went wrong with your upload!"),
        ];
    }

    // TODO : la fonction est utilisé que dans la class, donc à mettre en private
    /**
     * Encrypts a file.
     *
     * @param string $source The path to the source file.
     * @param string $dest The path to the destination file.
     * @return bool|string The result of the encryption.
     */
    public function encryptFile(string $source, string $dest)
    {
        $key = substr(sha1($this->params->get('encrypt_key'), true), 0, 16);
        $iv = openssl_random_pseudo_bytes(16);

        $error = false;
        if ($fpOut = fopen($dest, 'w')) {
            // Put the initialzation vector to the beginning of the file
            fwrite($fpOut, $iv);
            if ($fpIn = fopen($source, 'rb')) {
                while (! feof($fpIn)) {
                    $plaintext = fread($fpIn, 16 * DocumentInterface::FILE_ENCRYPTION_BLOCKS);
                    $ciphertext = openssl_encrypt($plaintext, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $iv);
                    // Use the first 16 bytes of the ciphertext as the next initialization vector
                    $iv = substr($ciphertext, 0, 16);
                    fwrite($fpOut, $ciphertext);
                }
                fclose($fpIn);
            } else {
                $error = true;
            }
            fclose($fpOut);
        } else {
            $error = true;
        }

        return $error ? false : $dest;
    }

    /**
     * Check the file requirements.
     *
     * @param array $file The file to check.
     * @param string $input The input string.
     * @param array $pathFolder The path folder.
     * @return array The result of the file requirements check.
     */
    public function checkFileRequirements(array $file, string $input, array $pathFolder): array
    {
        if (empty($file)) {
            return [
                'result' => false,
                'msg' => 'File parameter should not be empty',
            ];
        }
        //TODO SBAR
        //$dir devrait être calculé : sinon on peut facilement enregistrer des fichiers n'importe où
        $upload_dir = $this->params->get('uploadDir');

        $upload_dir = $this->params->get('uploadDir') . $pathFolder["dir"] . '/';
        if (! $this->fs->has($upload_dir)) {
            $this->fs->createDirectory($upload_dir);
        }

        //ex: upload/communecter/person
        if (isset($pathFolder["typeEltFolder"])) {
            $upload_dir .= $pathFolder["typeEltFolder"] . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }

        //ex: upload/communecter/person/userId
        if (isset($pathFolder["idEltFolder"])) {
            $upload_dir .= $pathFolder["idEltFolder"] . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }

        if (isset($pathFolder["restricted"])) {
            $upload_dir .= DocumentInterface::RESTRICTED_FOLDER . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }

        if (isset($pathFolder["docType"]) && $pathFolder["docType"] == DocumentInterface::DOC_TYPE_FILE) {
            $upload_dir .= DocumentInterface::GENERATED_FILE_FOLDER . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }
        if (isset($pathFolder["subDir"])) {
            $arraySub = explode(".", $pathFolder["subDir"]);
            foreach ($arraySub as $sub) {
                $upload_dir .= $sub . '/';
                if (! $this->fs->has($upload_dir)) {
                    $this->fs->createDirectory($upload_dir);
                }
            }
        }
        if (@$input == "newsImage" || (isset($pathFolder["contentKey"]) && $pathFolder["contentKey"] == DocumentInterface::IMG_SLIDER)) {
            $upload_dir .= DocumentInterface::GENERATED_ALBUM_FOLDER . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }
        if (isset($pathFolder["contentKey"]) && $pathFolder["contentKey"] == DocumentInterface::IMG_BANNER) {
            $upload_dir .= DocumentInterface::GENERATED_BANNER_FOLDER . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }
        if (isset($pathFolder["folderId"])) {
            $upload_dir .= $this->getModelFolder()->getParentFoldersPath($pathFolder["folderId"]);
        }
        //Check extension
        $allowed_ext = ['jpg', 'jpeg', 'png', 'gif', "pdf", "xls", "xlsx", "doc", "docx", "ppt", "pptx", "odt", "ods", "odp", "csv", "ttf"];

        $nameFile = (isset($pathFolder["nameUrl"]) && ! empty($pathFolder["nameUrl"])) ? $pathFolder["nameUrl"] : @$file["name"];

        $ext = strtolower(pathinfo($nameFile, PATHINFO_EXTENSION));
        if (! in_array($ext, $allowed_ext)) {
            return [
                'result' => false,
                'error' => $this->language->t("document", "Only") . implode(',', $allowed_ext) . $this->language->t("document", "files are allowed!"),
            ];
        }

        //Check size
        $size = (! empty($pathFolder["sizeUrl"]) ? $pathFolder["sizeUrl"] : $file["size"]);
        if ($size > 15_000_000) {
            return [
                'result' => false,
                'error' => "The file size should not be over 15 Mo",
            ];
        }

        return [
            'result' => true,
            'msg' => 'Files requirements meet',
            'uploadDir' => $upload_dir,
        ];
    }

    // TODO : n'est pas utilisé
    // /**
    //  * Retrieves the file at the specified path.
    //  *
    //  * @param string $path The path of the file to retrieve.
    //  * @return string|bool The file contents, or false on failure.
    //  */
    // public function get_file(string $path)
    // {
    //     $fileToGet = null;
    //     if (file_exists($path)) {
    //         header('Content-Type: image/png');
    //         header('Content-Length: ' . filesize($fileToGet));
    //         return file_get_contents($fileToGet);
    //     }
    // }
}
