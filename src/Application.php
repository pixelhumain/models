<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ApplicationInterface;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;

class Application extends BaseModel implements ApplicationInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use ParamsTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateParamsProperty();
        $this->validateI18nProperty();
    }

    /**
     * Loads the database application configuration.
     *
     * @param string|null $key The key of the configuration to load. If null, all configurations will be loaded.
     * @return void
     */
    public function loadDBAppConfig(?string $key = null): void
    {
        if (! $key) {
            $key = "devParams";
            if (stripos($_SERVER['SERVER_NAME'] ?? '', "communecter74-dev") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "communecter56-dev") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "communecter-dev") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "127.0.0.1") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "localhost") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "::1") === false &&
                stripos($_SERVER['SERVER_NAME'] ?? '', "localhost:8080") === false &&
                strpos($_SERVER['SERVER_NAME'] ?? '', "local.") !== 0 &&
                strpos($_SERVER['SERVER_NAME'] ?? '', "dev.") !== 0) {
                $key = "prodParams";
            } // PROD
        }
        $params = $this->db->findOne(ApplicationInterface::COLLECTION, [
            "key" => $key,
        ]);
        if (! $params) {
            throw new CTKException(403, $this->language->t('error', 'Missing Configs db.applications.key == ' . $key . ' doesnt exists'));
        } else {
            if (! empty($params["mangoPay"]) && is_array($params["mangoPay"])) {
                $this->params->set("mangoPay", [
                    "ClientId" => $params["mangoPay"]["ClientId"],
                    "ClientPassword" => $params["mangoPay"]["ClientPassword"],
                    "TemporaryFolder" => $params["mangoPay"]["TemporaryFolder"],
                ]);
            }
        }
    }

    /**
     * Retrieves the token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @return array|null The token for the specified API, or null if it doesn't exist.
     */
    public function getToken(string $apiName): ?array
    {
        $params = $this->db->findOne(
            ApplicationInterface::COLLECTION,
            [
                "key" => "devParams",
            ],
            ["api"]
        );
        $res = null;
        if (! empty($params["api"]) && ! empty($params["api"][$apiName]) && ! empty($params["api"][$apiName]["token"])) {
            $res = (array) $params["api"][$apiName];
        }
        return $res;
    }

    /**
     * Saves a token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @param array $token The token to be saved.
     * @return array The updated token.
     */
    public function saveToken(string $apiName, array $token): array
    {
        $params = $this->db->findOne(
            ApplicationInterface::COLLECTION,
            [
                "key" => "devParams",
            ],
            ["api"]
        );
        if (empty($params["api"]) || ! is_array($params["api"])) {
            $params["api"] = [];
        }

        if (empty($params["api"][$apiName]) || ! is_array($params["api"][$apiName])) {
            $params["api"][$apiName] = [];
        }

        $res = [
            "result" => false,
        ];
        if (! empty($token["expires_in"] ?? null)) {
            $params["api"][$apiName]["token"] = $token;

            $params["api"][$apiName]["expireToken"] = time() + (int) $token["expires_in"];
            $this->params->set("token" . $apiName, $token);
            $res = $this->db->update(
                ApplicationInterface::COLLECTION,
                [
                    "key" => "devParams",
                ],
                [
                    '$set' => [
                        "api" => $params["api"],
                    ],
                ]
            );
        }

        return $res;
    }
}
