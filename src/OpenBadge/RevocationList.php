<?php

namespace PixelHumain\Models\OpenBadge;

class RevocationList extends AbstractOpenBadge
{
    public $type = 'RevocationList';

    public $id;

    public $issuer;

    public $revokedAssertions;
}
