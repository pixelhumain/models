<?php

namespace PixelHumain\Models\OpenBadge;

class EndorsementClass extends AbstractOpenBadge
{
    public $id;

    public $type;

    public $claim;

    public $issuer;

    public $issuedOn;

    public $verification;

    public $revoked;

    public $revocationReason;

    public $endorsementComment;
}
