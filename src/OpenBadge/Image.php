<?php

namespace PixelHumain\Models\OpenBadge;

/**
 * Represents an image.
 */
class Image extends AbstractOpenBadge
{
    /**
     * The type of the image.
     *
     * @var string
     */
    public string $type;

    /**
     * The ID of the image.
     *
     * @var string
     */
    public string $id;

    /**
     * The caption of the image.
     *
     * @var string
     */
    public string $caption;

    /**
     * The author of the image.
     *
     * @var string
     */
    public string $author;
}
