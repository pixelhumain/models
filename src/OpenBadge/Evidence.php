<?php

namespace PixelHumain\Models\OpenBadge;

class Evidence extends AbstractOpenBadge
{
    public $type;

    public $id;

    public $narrative;

    public $name;

    public $description;

    public $genre;

    public $audience;
}
