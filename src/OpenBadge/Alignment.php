<?php

namespace PixelHumain\Models\OpenBadge;

class Alignment extends AbstractOpenBadge
{
    public $targetName;

    public $targetUrl;

    public $targetDescription;

    public $targetFramework;

    public $targetCode;
}
