<?php

namespace PixelHumain\Models\OpenBadge;

class Verification extends AbstractOpenBadge
{
    public $type;

    public $verificationProperty;

    public $startsWith;

    public $allowedOrigins;

    public $creator;
}
