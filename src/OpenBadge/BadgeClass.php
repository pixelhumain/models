<?php

namespace PixelHumain\Models\OpenBadge;

use PixelHumain\Models\Interfaces\BadgeInterface;

class BadgeClass extends AbstractOpenBadge
{
    protected array $mapVarClass = [
        'alignment' => [Alignment::class],
    ];

    public $context = BadgeInterface::CONTEXT;

    public $type = 'BadgeClass';

    public $id;

    public $name;

    public $description;

    public $image;

    public $criteria;

    public $tags;

    public $issuer;

    public $alignment;

    public function fromBadgeElement($badgeElement)
    {
        $baseUrl = empty($_SERVER['HTTPS']) ? "http://" . $_SERVER['SERVER_NAME'] : "https://" . $_SERVER['SERVER_NAME'];
        $cleaned = [
            "@context" => "https://w3id.org/openbadges/v2",
            "id" => $baseUrl . "/co2/badges/badges/id/" . (string) $badgeElement["_id"],
            "type" => "BadgeClass",
            "name" => $badgeElement["name"],
            "issuer" => $baseUrl . "/co2/badges/issuers/id/" . (string) $badgeElement["creator"],
            "image" => @$badgeElement["profilMediumImageUrl"] ? $baseUrl . $badgeElement["profilMediumImageUrl"] : null,
            "description" => $badgeElement["description"] ?? $badgeElement["collection"] . " : " . $badgeElement["_id"],
            "tags" => $badgeElement["tags"],
            "criteria" => $badgeElement["criteria"] ?? [
                "narrative" => "",
            ],
        ];
        $badgeClassInstance = new BadgeClass();
        $badgeClassInstance->setFromAssociativeArray($cleaned);
        return $badgeClassInstance;
    }
}
