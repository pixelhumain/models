<?php

namespace PixelHumain\Models\OpenBadge;

class CryptographicKey extends AbstractOpenBadge
{
    public $type;

    public $id;

    public $owner;

    public $publicKeyPem;
}
