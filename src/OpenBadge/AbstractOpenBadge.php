<?php

namespace PixelHumain\Models\OpenBadge;

use Exception;
use PixelHumain\Models\Interfaces\BadgeInterface;

//Image::class

/**
 * Represents an abstract OpenBadge.
 */
abstract class AbstractOpenBadge
{
    /**
     * This variable is an array that contains the keys to skip.
     *
     * @var array $SKIP_KEY
     */
    protected array $SKIP_KEY = [
        '@context', 'context', 'mapVarClass', 'SKIP_KEY',
    ];

    /**
     * This property holds the mapping between variable names and their corresponding class names.
     *
     * @var array $mapVarClass
     */
    protected array $mapVarClass = [
        'image' => Image::class,
    ];

    /**
     * Constructor for the AbstractOpenBadge class.
     */
    public function __construct()
    {
        $parent_vars = get_class_vars(self::class);
        $this->mapVarClass = array_merge((array) $parent_vars['mapVarClass'], $this->mapVarClass);
        $this->SKIP_KEY = array_merge((array) $parent_vars['SKIP_KEY'], $this->SKIP_KEY);
    }

    /**
     * Serializes the object into a string representation.
     *
     * @return string The serialized object.
     */
    public function serialize(): string
    {
        return json_encode($this->getAsAssociativeArray());
    }

    /**
     * Deserialize the given JSON string into the object.
     *
     * @param string $json The JSON string to deserialize.
     * @return void
     */
    public function deserialize(string $json): void
    {
        $json = preg_replace('/\s+/', '', $json);
        $values = json_decode($json, true);
        $this->setFromAssociativeArray((array) $values);
    }

    /**
     * Set the object properties from an associative array.
     *
     * @param array $array The associative array containing the object properties.
     * @return void
     */
    public function setFromAssociativeArray(array $array)
    {
        $var_names = array_keys(get_class_vars(get_class($this)));
        foreach ($var_names as $name) {
            if (in_array($name, $this->SKIP_KEY)) {
                continue;
            }
            if (isset($array[$name])) {
                if (is_array($array[$name]) && key_exists($name, (array) $this->mapVarClass)) {
                    if (is_array($this->mapVarClass[$name])) {
                        $arrayRes = [];
                        foreach ($array[$name] as $key => $value) {
                            $tmp = new $this->mapVarClass[$name][0]();
                            $tmp->setFromAssociativeArray($value);
                            $arrayRes[$key] = $tmp;
                        }
                        $this->$name = $arrayRes;
                    } else {
                        $this->$name = new $this->mapVarClass[$name]();
                        $this->$name->setFromAssociativeArray($array[$name]);
                    }
                } else {
                    $this->$name = $array[$name];
                }
            }
        }
    }

    /**
     * Returns the object as an associative array.
     *
     * @return array The object represented as an associative array.
     */
    public function getAsAssociativeArray()
    {
        $array = [];
        $var_names = array_keys(get_class_vars(get_class($this)));
        foreach ($var_names as $name) {
            if (in_array($name, $this->SKIP_KEY)) {
                continue;
            }
            $value = $this->getItemArray($this->$name);
            if (isset($value)) {
                $array[$name] = $value;
            }
        }
        if (in_array("context", $var_names)) {
            $array['@context'] = $this->context;
            unset($array['context']);
        }
        return $array;
    }

    /**
     * Returns an array representation of the given item.
     *
     * @param mixed $item The item to convert to an array.
     * @return array The array representation of the item.
     */
    private function getItemArray($item)
    {
        if (isset($item)) {
            if ($item instanceof AbstractOpenBadge) {
                return $item->getAsAssociativeArray();
            } elseif (is_array($item)) {
                $array = [];
                foreach ($item as $key => $value) {
                    $array[$key] = $this->getItemArray($value);
                }
                return $array;
            } else {
                return $item;
            }
        }
        return null;
    }

    /**
     * Check the context of the OpenBadge.
     *
     * @return void
     */
    public function checkContext()
    {
        if ($this->context != BadgeInterface::CONTEXT) {
            throw new Exception("Cannot support this version of OPEN BADGES");
        }
    }
}
