<?php

namespace PixelHumain\Models\OpenBadge;

class Recipient extends AbstractOpenBadge
{
    public $hashed;

    public $identity;

    public $type;

    public $salt;
}
