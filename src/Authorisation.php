<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\AnswerInterface;
use PixelHumain\Models\Interfaces\AuthorisationInterface;
use PixelHumain\Models\Interfaces\BadgeInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\ClassifiedInterface;
use PixelHumain\Models\Interfaces\CmsInterface;
use PixelHumain\Models\Interfaces\CrowdfundingInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\NetworkInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PoiInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;
use PixelHumain\Models\Interfaces\RoleInterface;
use PixelHumain\Models\Interfaces\RoomInterface;

use PixelHumain\Models\Traits\AnswerTrait;
use PixelHumain\Models\Traits\BadgeTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\FormTrait;
use PixelHumain\Models\Traits\Interfaces\AnswerTraitInterface;
use PixelHumain\Models\Traits\Interfaces\BadgeTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\FormTraitInterface;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PreferenceTraitInterface;

use PixelHumain\Models\Traits\Interfaces\RoleTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\PreferenceTrait;
use PixelHumain\Models\Traits\RoleTrait;
use PixelHumain\Models\Traits\SlugTrait;

// Room
// Action
// Proposal
// Event
// Project
// Organization
// Person
// City
// Cms
// Poi
// Classified
// Network
// Crowdfunding
// Answer
// Form
// Badge

class Authorisation extends BaseModel implements AuthorisationInterface, AnswerTraitInterface, BadgeTraitInterface, CostumTraitInterface, ElementTraitInterface, FormTraitInterface, PreferenceTraitInterface, RoleTraitInterface, PersonTraitInterface, OrganizationTraitInterface, SlugTraitInterface, CityTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;
    use ParamsTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AnswerTrait;
    use BadgeTrait;
    use CostumTrait;
    use ElementTrait;
    use FormTrait;
    use PreferenceTrait;
    use RoleTrait;
    use PersonTrait;
    use OrganizationTrait;
    use SlugTrait;
    use CityTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateParamsProperty();
    }

    /**
     * Check if a user can edit an item.
     *
     * @param string|null $userId The user ID.
     * @param string $type The type of the item.
     * @param string $itemId The ID of the item.
     * @param string|null $parentType The type of the parent item (optional).
     * @param string|null $parentId The ID of the parent item (optional).
     * @param bool $deleteProcess Flag indicating if it is a delete process (optional).
     * @return bool Returns true if the user can edit the item, false otherwise.
     */
    public function canEditItem(?string $userId, string $type, string $itemId, ?string $parentType = null, ?string $parentId = null, bool $deleteProcess = false): bool
    {
        $isDDA = null;
        $res = false;
        $check = false;

        if (! empty($userId)) {
            //DDA
            if ($type == RoomInterface::COLLECTION || $type == RoomInterface::CONTROLLER ||

                $type == ActionInterface::COLLECTION || $type == ActionInterface::CONTROLLER ||
                $type == ProposalInterface::COLLECTION || $type == ProposalInterface::CONTROLLER) {
                if ($parentType == null || $parentId == null) {
                    $elem = $this->getModelElement()->getByTypeAndId($type, $itemId);

                    if (empty($elem["preferences"]) &&
                        empty($elem["preferences"]["private"])) {
                        $parentId = $elem["parentId"];
                        $parentType = $elem["parentType"];
                    }
                }
                $isDDA = true;
                $type = (! empty($parentType) ? $parentType : $type);
                $itemId = (! empty($parentId) ? $parentId : $itemId);
                $check = true;
            }
            //Super Admin can do anything
            if ($this->isInterfaceAdmin()) {
                return true;
            }

            if (in_array($type, [EventInterface::COLLECTION, ProjectInterface::COLLECTION, OrganizationInterface::COLLECTION])) {
                //Check if delete pending => can not edit

                $isStatusDeletePending = $this->getModelElement()->isElementStatusDeletePending($type, $itemId);
                if ($isStatusDeletePending && $deleteProcess === false) {
                    return false;
                }
                //Element admin ?
                elseif ($this->isElementAdmin($itemId, $type, $userId)) {
                    return true;
                    //Source admin ?
                } elseif ($this->isSourceAdmin($itemId, $type, $userId)) {
                    return true;
                } elseif (@$isDDA == true) {
                    return $this->canParticipate($userId, $type, $itemId);
                }
            } elseif ($type == PersonInterface::COLLECTION) {
                if ($userId == $itemId) {
                    $res = true;
                }
            } elseif ($type == CityInterface::COLLECTION) {
                if ($check) {
                    $res = $this->isLocalCitizen($userId, ($parentType == CityInterface::CONTROLLER) ? $parentId : $itemId);
                } else {
                    $res = true;
                }
            } elseif (in_array($type, [CmsInterface::COLLECTION, PoiInterface::COLLECTION, ClassifiedInterface::COLLECTION, NetworkInterface::COLLECTION, CrowdfundingInterface::COLLECTION])) {
                $res = $this->canEdit($userId, $itemId, $type);
            } elseif ($type == ProposalInterface::COLLECTION) {
                $res = $this->canParticipate($userId, $type, $itemId);
            } elseif ($type == ActionInterface::COLLECTION) {
                $res = $this->canParticipate($userId, $type, $itemId);
            } elseif ($type == AnswerInterface::COLLECTION) {
                $res = $this->getModelAnswer()->canEdit($itemId);
            } elseif ($type == FormInterface::COLLECTION) {
                $res = $this->getModelForm()->canAdmin($itemId);
            } elseif ($type == BadgeInterface::COLLECTION) {
                $res = $this->getModelBadge()->canEdit($itemId, $userId);
            }
        }
        return $res;
    }

    /**
     * Check if a user can edit an item or open an edition.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param string|null $userId The ID of the user.
     * @param string|null $parentType The type of the parent entity (optional).
     * @param string|null $parentId The ID of the parent entity (optional).
     * @return bool Returns true if the user can edit the item or open an edition, false otherwise.
     */
    public function canEditItemOrOpenEdition(string $idEntity, string $typeEntity, ?string $userId, ?string $parentType = null, ?string $parentId = null): bool
    {
        $res = false;
        $res = $this->isOpenEdition($idEntity, $typeEntity);
        if ($res != true) {
            $res = $this->canEditItem($userId, $typeEntity, $idEntity, $parentType, $parentId);
        }
        return $res;
    }

    /**
     * Check if the entity is open for edition.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param array|null $preferences The preferences for the entity (optional).
     * @return bool Returns true if the entity is open for edition, false otherwise.
     */
    public function isOpenEdition(string $idEntity, string $typeEntity, ?array $preferences = null): bool
    {
        $res = false;
        if (empty($preferences)) {
            $entity = $this->db->findOne($typeEntity, [
                "_id" => $this->db->MongoId($idEntity),
            ], ['preferences']);
            $preferences = $entity["preferences"] ?? null;
        }
        if (! empty($preferences)) {
            $res = $this->getModelPreference()->isOpenEdition($preferences);
        }
        return $res;
    }

    /**
     * Check if a user has admin access to an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param bool $checkParent (optional) Whether to check the parent element for admin access. Default is false.
     * @param bool $considerSuperAdmin (optional) Whether to consider the user as a super admin. Default is true.
     * @return bool Returns true if the user has admin access to the element, false otherwise.
     */
    public function isElementAdmin(string $elementId, string $elementType, string $userId, bool $checkParent = false, bool $considerSuperAdmin = true): bool
    {
        $res = false;
        if ($this->isInterfaceAdmin() && $considerSuperAdmin) {
            return true;
        } elseif (empty($res) && in_array($elementType, [EventInterface::COLLECTION, ProjectInterface::COLLECTION, OrganizationInterface::COLLECTION])) {
            $eltAdmin = $this->getModelElement()->getElementById($elementId, $elementType, null, ["name", "links", "parent", "organizer"]);

            if (! empty($eltAdmin)) {
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if (isset($eltAdmin["links"])
                    && ! empty(Link::$linksTypes[$elementType])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId])
                    && isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId]["isAdmin"])
                    && ! isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId]["isAdminPending"])
                    && ! isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId]["toBeValidated"])
                    && ! isset($eltAdmin["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId]["isInviting"])
                ) {
                    return true;
                    // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                } elseif (isset($eltAdmin["parent"]) && ! empty($eltAdmin["parent"]) && empty($checkParent)) {
                    foreach ($eltAdmin["parent"] as $k => $v) {
                        if ($v["type"] == PersonInterface::COLLECTION) {
                            if ($k == $this->session->getUserId()) {
                                $res = true;
                            }
                        } else {
                            $res = $this->isElementAdmin($k, $v["type"], $this->session->getUserId(), true);
                        }
                        if (! empty($res)) {
                            return $res;
                        }
                    }
                } elseif (isset($eltAdmin["organizer"]) && ! empty($eltAdmin["organizer"]) && empty($checkParent)) {
                    foreach ($eltAdmin["organizer"] as $k => $v) {
                        if ($v["type"] == PersonInterface::COLLECTION) {
                            if ($k == $this->session->getUserId()) {
                                $res = true;
                            }
                        } else {
                            $res = $this->isElementAdmin($k, $v["type"], $this->session->getUserId(), true);
                        }
                        if (! empty($res)) {
                            return $res;
                        }
                    }
                }
            }
        } elseif ($elementType == FormInterface::COLLECTION) {
            $res = true;
        }

        if (empty($checkParent)) {
            if ($this->getModelCostum()->isSameFunction("isElementAdmin")) {
                $res = $this->getModelCostum()->sameFunction("isElementAdmin", [
                    "elementId" => $elementId,
                    "elementType" => $elementType,
                    "userId" => $userId,
                ]);
            }
        }
        return $res;
    }

    /**
     * Check if the user is an interface admin.
     *
     * @param string|null $id The user ID.
     * @param string|null $type The user type.
     * @return bool Returns true if the user is an interface admin, false otherwise.
     */
    public function isInterfaceAdmin(?string $id = null, ?string $type = null): bool
    {
        if ($this->isUserSuperAdmin($this->session->getUserId())) {
            return true;
        }
        if ($this->isCostumAdmin()) {
            return true;
        }
        if (! empty($id) && ! empty($type) && $this->isSourceAdmin($id, $type, $this->session->getUserId())) {
            return true;
        }
        //TODO add check if parent exist and has parent authorization
        return false;
    }

    /**
     * Check if the user is a super admin.
     *
     * @param string|null $userId The ID of the user.
     * @return bool Returns true if the user is a super admin, false otherwise.
     */
    public function isUserSuperAdmin(?string $userId): bool
    {
        $res = false;
        if (! empty($userId)) {
            $account = $this->getModelElement()->getElementById($userId, PersonInterface::COLLECTION, null, ["roles"]);
            $res = $this->getModelRole()->isUserSuperAdmin(@$account["roles"]);
        }
        return $res;
    }

    /**
     * Check if the user is a custom admin.
     *
     * @return bool Returns true if the user is a custom admin, false otherwise.
     */
    public function isCostumAdmin(): bool
    {
        $costum = $this->getModelCostum()->getCostum();
        $sessionCostum = $this->session->get('costum');
        if (isset($sessionCostum) && ! empty($sessionCostum) && ! is_bool($costum) && ! empty($sessionCostum[$costum["slug"]])) {
            if (isset($sessionCostum[$costum["slug"]]['isCostumAdmin']) && ! empty($sessionCostum[$costum["slug"]])) {
                return $sessionCostum[$costum["slug"]]['isCostumAdmin'];
            }
        }
        return false;
    }

    /**
     * Check if the user is an admin for a specific source entity.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param string $idUser The ID of the user.
     * @return bool Returns true if the user is an admin for the source entity, false otherwise.
     */
    public function isSourceAdmin(string $idEntity, string $typeEntity, string $idUser): bool
    {
        $res = false;
        $entity = $this->db->findOne($typeEntity, [
            "_id" => $this->db->MongoId($idEntity),
        ]);
        if (! empty($entity["source"]["sourceKey"])) {
            $user = $this->db->findOne(PersonInterface::COLLECTION, [
                "_id" => $this->db->MongoId($idUser),
                "sourceAdmin" => $entity["source"]["sourceKey"],
            ]);
        }
        if (! empty($user)) {
            $res = true;
        }
        return $res;
    }

    // ------------------------------------------------- //

    /**
     * Check if a user has the specified roles.
     *
     * @param string|null $userId The ID of the user.
     * @param array $roles The roles to check.
     * @return bool Returns true if the user has the specified roles, false otherwise.
     */
    public function isUser(?string $userId, array $roles): bool
    {
        $res = false;
        if (! empty($userId)) {
            $account = $this->getModelElement()->getElementById($userId, PersonInterface::COLLECTION, null, ["roles"]);
            if (! empty($account) && ! empty($account["roles"]) && is_array($account["roles"])) {
                $res = $this->getModelRole()->isUser($account["roles"], $roles);
            }
        }
        return $res;
    }

    /**
     * Check if the user is connected to the Meteor server.
     *
     * @param string $token The user's token.
     * @param string $accountId The user's account ID.
     * @param string $tokenName The name of the token.
     * @param mixed $test (optional) A test parameter.
     * @return bool Returns true if the user is connected to the Meteor server, false otherwise.
     */
    public function isMeteorConnected(string $token, string $accountId, string $tokenName, $test = null): bool
    {
        $result = false;

        $hashedToken = hash_hmac('sha256', $token, $accountId, true);
        $hashedToken = base64_encode($hashedToken);

        if ($account = $this->db->findOne(PersonInterface::COLLECTION, [
            "_id" => $this->db->MongoId($accountId),
            "loginTokens.hashedToken" => $hashedToken,
            'loginTokens.name' => $tokenName,
        ])) {
            $this->getModelPerson()->saveUserSessionData($account);

            $result = true;
        }
        return $result;
    }

    /**
     * Check if the user is valid.
     *
     * @param string $user The username.
     * @param string $pwd The password.
     * @return bool Returns true if the user is valid, false otherwise.
     */
    public function isValidUser(string $user, string $pwd): bool
    {
        $result = false;
        $this->getModelPerson()->clearUserSessionData();
        $account = $this->db->findOne(PersonInterface::COLLECTION, [
            '$or' => [[
                "email" => $this->db->MongoRegex('/^' . preg_quote(trim($user)) . '$/i'),
            ], [
                "username" => $user,
            ]],
        ]);
        if (! empty($account)) {
            if ($this->getModelPerson()->checkPassword($pwd, $account)) {
                $this->getModelPerson()->saveUserSessionData($account);
                $this->getModelPerson()->updateLoginHistory((string) $account["_id"]);
                $result = true;
            }
        }
        return $result;
    }

    /**
     * Check if the user can edit members data for a specific organization.
     *
     * @param string $organizationId The ID of the organization.
     * @return bool Returns true if the user can edit members data, false otherwise.
     */
    public function canEditMembersData(string $organizationId): bool
    {
        $res = false;
        if ($this->params->get('isParentOrganizationAdmin')) {
            $organization = $this->getModelOrganization()->getById($organizationId);
            if (isset($organization["canEditMember"]) && $organization["canEditMember"]) {
                $res = true;
            }
        }
        return $res;
    }

    /**
     * Check if a user can participate in an item.
     *
     * @param string $userId The ID of the user.
     * @param string $itemType The type of the item.
     * @param string $itemId The ID of the item.
     * @param bool $openEdition Whether the item is in open edition or not.
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, string $itemType, string $itemId, bool $openEdition = true): bool
    {
        $res = false;

        if ($userId) {
            if ($openEdition) {
                $res = $this->getModelPreference()->isOpenEdition($this->getModelPreference()->getPreferencesByTypeId($itemId, $itemType));
            }
            //var_dump($res);
            if ($res != true) {
                if ($itemType == PersonInterface::COLLECTION && $itemId == $userId) {
                    $res = true;
                }
                if (in_array($itemType, [OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION, EventInterface::COLLECTION])) {
                    $res = $this->isElementMember($itemId, $itemType, $userId);
                }
                if ($itemType == CityInterface::COLLECTION) {
                    $res = $this->isLocalCitizen($userId, $itemId);
                }
                if ($itemType == NewsInterface::COLLECTION) {
                    $res = true;
                }
                if ($itemType == ClassifiedInterface::COLLECTION) {
                    $res = true;
                }
                if ($itemType == ProposalInterface::COLLECTION) {
                    $el = $this->getModelElement()->getElementById($itemId, $itemType, null, ["preferences", "parent", "parentId", "parentType"]);
                    if (! empty($el["preferences"]) &&
                                isset($el["preferences"]["private"]) &&
                                $el["preferences"]["private"] == false) {
                        $res = true;
                    } elseif (! empty($el["parentType"]) && ! empty($el["parentId"])) {
                        $res = $this->canParticipate($this->session->getUserId(), $el["parentType"], $el["parentId"]);
                    } elseif (! empty($parent)) {
                        foreach ($parent as $k => $v) {
                            $res = $this->canParticipate($this->session->getUserId(), $v["type"], $k);
                        }
                    }
                }
            }
        }
        return $res;
    }

    /**
     * Determines if the user can see a specific context.
     *
     * @param string|null $contextType The type of the context.
     * @param string|null $contextId   The ID of the context.
     * @param bool        $openEdition Whether open edition is allowed.
     *
     * @return bool Returns true if the user can see the context, false otherwise.
     */
    public function canSee(?string $contextType = null, ?string $contextId = null, bool $openEdition = true): bool
    {
        $params = null;
        $res = false;

        if ($this->session->getUserId()) {
            $res = $this->canEditItem($this->session->getUserId(), $contextType, $contextId);
            if ($res == false) {
                $res = $this->canParticipate($this->session->getUserId(), $contextType, $contextId);
            }
            if ($res == false) {
                $res = $this->getModelCostum()->sameFunction("canSee", $params);
            }
        }
        return (bool) $res;
    }

    /**
     * Check if the user is a super admin for the CMS.
     *
     * @return bool Returns true if the user is a super admin for the CMS, false otherwise.
     */
    public function isUserSuperAdminCms(): bool
    {
        $res = false;
        $userId = $this->session->getUserId();
        if (! empty($userId)) {
            $account = $this->getModelElement()->getElementById($userId, PersonInterface::COLLECTION, null, ["roles"]);
            if (! empty($account) && ! empty($account["roles"]) && is_array($account["roles"])) {
                $res = $this->getModelRole()->isUserSuperAdminCms($account["roles"]);
            }
        }
        return $res;
    }

    /**
     * Check if the entity is open data.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param array|null $preferences The preferences for the entity (optional).
     * @return bool Returns true if the entity is open data, false otherwise.
     */
    public function isOpenData(string $idEntity, string $typeEntity, ?array $preferences = null): bool
    {
        $res = false;
        if (empty($preferences)) {
            $entity = $this->db->findOne($typeEntity, [
                "_id" => $this->db->MongoId($idEntity),
            ], ['preferences']);
            if (! empty($entity) && ! empty($entity["preferences"])) {
                $preferences = $entity["preferences"];
            }
        }
        if (! empty($preferences)) {
            $res = $this->getModelPreference()->isOpenData($preferences);
        }

        return $res;
    }

    /**
     * Check if a user can delete an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param array|null $elt The element data (optional).
     * @return bool Returns true if the user can delete the element, false otherwise.
     */
    public function canDeleteElement(string $elementId, string $elementType, string $userId, ?array $elt = null): bool
    {
        //If open Edition : the element can be deleted
        if ($elementType == PersonInterface::COLLECTION && $elementId == $userId) {
            $res = true;
        } else {
            $res = $this->isOpenEdition($elementId, $elementType);
            if ($res != true) {
                //check if the user is super admin
                $res = $this->isUser($userId, [RoleInterface::SUPERADMIN, RoleInterface::COEDITOR]);
                if ($res != true) {
                    // check if the user can edit the element (admin of the element)
                    $res = $this->canEditItem($userId, $elementType, $elementId, null, null, true);

                    if ($res != true && ! empty($elt) &&
                           ! empty($elt["source"]) &&
                           ! empty($elt["source"]["key"])) {
                        $el = $this->getModelSlug()->getElementBySlug($elt["source"]["key"], []);
                        if (! empty($el)) {
                            $res = $this->isElementAdmin($el["id"], $el["type"], $userId);
                        }
                    }
                }
            }
        }

        return $res;
    }

    /**
     * Determines if a user can see a private element.
     *
     * @param array|null $links The links associated with the element.
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @param string|null $creator The creator of the element.
     * @param string|null $parentType The type of the parent element.
     * @param string|null $parentId The ID of the parent element.
     * @return bool Returns true if the user can see the private element, false otherwise.
     */
    public function canSeePrivateElement(?array $links, string $type, string $id, ?string $creator, ?string $parentType = null, ?string $parentId = null): bool
    {
        $userId = $this->session->getUserId();

        if (empty($userId)) {
            return false;
        } else {
            // SuperAdmin Or costum Admin Or source admin
            if ($this->isInterfaceAdmin($id, $type)) {
                return true;
            }
            //creator access to his creativity
            if (! empty($creator) && $userId == $creator) {
                return true;
            }
            // attendees and contributors directly access and see the element
            if (! empty($links) &&
                @$links[Link::$linksTypes[$type][PersonInterface::COLLECTION]] &&
                @$links[Link::$linksTypes[$type][PersonInterface::COLLECTION]][$userId] &&
                ! @$links[Link::$linksTypes[$type][PersonInterface::COLLECTION]][$userId]["toBeValidated"]) {
                return true;
            }
            if (! empty($parentType) && ! empty($parentId)) {
                return $this->canParticipate($userId, $parentType, $parentId, false);
            }
            if ($this->isElementMember($id, $type, $userId)) {
                return true;
            }

            return false;
        }
    }

    /**
     * Check if the element with the given slug is admin for the specified user.
     *
     * @param string $slug The slug of the element.
     * @param string $userId The ID of the user.
     * @param bool $checkParent Optional. Whether to check the parent element as well. Default is false.
     * @return bool Returns true if the element is admin for the user, false otherwise.
     */
    public function isElementAdminBySlug(string $slug, string $userId, bool $checkParent = false): bool
    {
        $el = $this->getModelSlug()->getElementBySlug($slug);
        if (! empty($el) && is_array($el) && ! empty($el["id"]) && ! empty($el["type"])) {
            return $this->isElementAdmin((string) $el["id"], (string) $el["type"], $userId, $checkParent = false);
        }
        return false;
    }

    /**
     * Check if a user is a member of an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @return bool Returns true if the user is a member of the element, false otherwise.
     */
    public function isElementMember(string $elementId, string $elementType, string $userId): bool
    {
        $res = false;

        //Get the members of the organization : if there is no member then it's a new organization
        //We are in a creation process
        if (in_array($elementType, [EventInterface::COLLECTION, ProjectInterface::COLLECTION, OrganizationInterface::COLLECTION])) {
            if ($this->isElementAdmin($elementId, $elementType, $userId)) {
                return true;
            }
            $elt = $this->getModelElement()->getElementById($elementId, $elementType, null, ["links", "parent"]);
            if (! empty($elt)) {
                // CHECK FIRST IN ELEMENT DIRECTLY IF USER IS ADMIN
                if (isset($elt["links"])
                    && isset($elt["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]])
                    && isset($elt["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId])
                    && ! isset($elt["links"][Link::$linksTypes[$elementType][PersonInterface::COLLECTION]][$userId]["toBeValidated"])
                ) {
                    $res = true;
                }
                // CHECK THEN IF USER IS ADMIN OF PARENT OF THIS ELEMENT
                elseif (empty($res) && isset($elt["parent"]) && ! empty($elt["parent"])) {
                    foreach ($elt["parent"] as $k => $v) {
                        if ($v["type"] == PersonInterface::COLLECTION) {
                            if ($k == $this->session->getUserId()) {
                                $res = true;
                            }
                        } else {
                            $res = $this->isElementAdmin($k, $v["type"], $this->session->getUserId());
                        }
                        if (! empty($res)) {
                            return true;
                        }
                    }
                }
            }
        }
        if ($this->getModelCostum()->isSameFunction("isElementMember")) {
            $res = $this->getModelCostum()->sameFunction("isElementMember", [
                "elementId" => $elementId,
                "elementType" => $elementType,
                "userId" => $userId,
            ]);
        }
        return $res;
    }

    /**
     * Check if a user can edit a specific item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the item.
     * @param string $type The type of the item.
     * @return bool Returns true if the user can edit the item, false otherwise.
     */
    public function canEdit(string $userId, string $id, string $type): bool
    {
        $elem = $this->getModelElement()->getElementById($id, $type, null, ["parent", "creator"]);
        if (! empty($elem) && ! empty($userId)) {
            if ($this->isInterfaceAdmin()) {
                return true;
            } elseif ($userId == @$elem["creator"]) {
                return true;
            } elseif (! empty($elem["parent"])) {
                foreach ($elem["parent"] as $k => $v) {
                    if ($v["type"] == PersonInterface::COLLECTION && $k == $userId) {
                        return true;
                    } elseif ($this->canEditItem($userId, $v["type"], $k)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Check if the user is the owner of a specific resource.
     *
     * @param string $userId The ID of the user.
     * @param string $type The type of the resource.
     * @param string $id The ID of the resource.
     * @return bool Returns true if the user is the owner, false otherwise.
     */
    public function userOwner(string $userId, string $type, string $id): bool
    {
        $el = $this->getModelElement()->getElementById($id, $type, null, ["creator"]);
        if (! empty($el) && is_array($el) && ! empty($el["creator"]) && ! empty($userId) && $userId == $el["creator"]) {
            return true;
        }

        return false;
    }

    /**
     * Returns an array of admins for the specified parent entity.
     *
     * @param string $parentId The ID of the parent entity.
     * @param string $parentType The type of the parent entity.
     * @param bool $pending (optional) Whether to include pending admins. Default is false.
     * @return array An array of admins.
     */
    public function listAdmins(string $parentId, string $parentType, bool $pending = false): array
    {
        $parent = [];
        $link = null;
        $res = [];

        if (in_array($parentType, [OrganizationInterface::COLLECTION, EventInterface::COLLECTION, ProjectInterface::COLLECTION])) {
            $parent = $this->getModelElement()->getElementById($parentId, $parentType, null, ["links"]);
            $link = Link::$linksTypes[$parentType][PersonInterface::COLLECTION];
        } elseif ($parentType == FormInterface::COLLECTION) {
            $parent = $this->getModelForm()->getLinksById($parentId);
            $link = "survey";
        }

        if ($users = @$parent["links"][$link]) {
            foreach ($users as $personId => $linkDetail) {
                if (@$linkDetail["isAdmin"] == true) {
                    $userActivated = $this->getModelRole()->isUserActivated($personId);
                    if ($userActivated) {
                        if ($pending) {
                            array_push($res, $personId);
                        } elseif (@$linkDetail["isAdminPending"] == null || @$linkDetail["isAdminPending"] == false) {
                            array_push($res, $personId);
                        }
                    }
                }
            }
        }

        return $res;
    }

    /**
     * Vérifie si le captcha est valide.
     *
     * @param int|float $captcha Le captcha à vérifier.
     * @param string $reponse La réponse attendue du captcha.
     * @return bool Renvoie true si le captcha est valide, sinon renvoie false.
     */
    public function verifCaptcha($captcha, string $reponse): bool
    {
        function rpHash(string $value)
        {
            $hash = 5381;
            $value = strtoupper($value);
            for ($i = 0; $i < strlen($value); $i++) {
                $hash = (leftShift32($hash, 5) + $hash) + ord(substr($value, $i));
            }
            return $hash;
        }

        // Perform a 32bit left shift
        function leftShift32(int $number, int $steps)
        {
            // convert to binary (string)
            $binary = decbin($number);
            // left-pad with 0's if necessary
            $binary = str_pad($binary, 32, "0", STR_PAD_LEFT);
            // left shift manually
            $binary = $binary . str_repeat("0", $steps);
            // get the last 32 bits
            $binary = substr($binary, strlen($binary) - 32);
            // if it's a positive number return it
            // otherwise return the 2's complement
            return ($binary[0] == "0" ? bindec($binary) :
                -(2 ** 31 - bindec(substr($binary, 1))));
        }

        if (rpHash($reponse) == $captcha) {
            return true;
        } else {
            return false;
        }
    }

    // CHANGE SESSION COSTUM ROLES USER
    /**
     * Check if the user has access to a menu button.
     *
     * @param array $value The value to check.
     * @return bool Returns true if the user has access, false otherwise.
     */
    public function accessMenuButton(array $value): bool
    {
        $res = true;
        $userId = $this->session->getUserId() ?? null;

        if (isset($value["restricted"])) {
            $restricted = $value["restricted"];
            $costum = $this->getModelCostum()->getCostum();
            $contextId = ! empty($costum) && is_array($costum) && ! empty($costum["contextId"]) ? (string) $costum["contextId"] : "";
            $contextType = ! empty($costum) && is_array($costum) && ! empty($costum["contextType"]) ? (string) $costum["contextType"] : "";
            $slug = ! empty($costum) && is_array($costum) && ! empty($costum["slug"]) ? (string) $costum["slug"] : null;

            //WARNING peut être test $costum["slug"] exist en dessous
            if ($this->isInterfaceAdmin()) {
                return true;
            } elseif (isset($restricted["admins"]) && $restricted["admins"] == true) {
                return false;
            } elseif (isset($restricted["connected"]) && $restricted["connected"] && ! $userId) {
                return false;
            } elseif (isset($restricted["members"]) && $restricted["members"] == true && (empty($userId) || ! $this->isElementMember($contextId, $contextType, $userId))) {
                $res = false;
            } elseif (! empty($restricted["roles"]) && is_array($restricted["roles"])) {
                $costumSession = $this->session->get('costum');
                $costumSlugArray = ! empty($costumSession) && is_array($costumSession) && ! empty($slug) && ! empty($costumSession[$slug]) ? $costumSession[$slug] : null;
                $hasRoles = ! empty($costumSlugArray) && is_array($costumSlugArray) && ! empty($costumSlugArray["hasRoles"]) && is_array($costumSlugArray["hasRoles"]) ? (array) $costumSlugArray["hasRoles"] : null;
                if (! empty($hasRoles) && ! empty(array_intersect($hasRoles, $restricted["roles"]))) {
                    $res = true;
                } elseif ($this->hasRoles($restricted)) {
                    $res = true;
                } else {
                    $res = false;
                }
            }
        }
        return $res;
    }

    /**
     * Check if a specific condition is met.
     *
     * @param string $collection The collection name.
     * @param string $id The ID of the item.
     * @param mixed|null $tplGenerator The template generator.
     * @param string|null $path The path.
     * @return bool Returns true if the specific condition is met, false otherwise.
     */
    public function specificCondition(string $collection, string $id, $tplGenerator, ?string $path): bool
    {
        $userId = $this->session->getUserId();
        $res = false;
        if (! empty($userId) && $tplGenerator) {
            if ($this->isElementAdmin($id, $collection, $userId) || $this->isInterfaceAdmin()) {
                $costum = $this->getModelCostum()->getCostum();
                $tplPath = (! empty($costum) && is_array($costum) && ! empty($costum["contextId"])) ? "tplsUser." . (string) $costum["contextId"] . ".welcome" : "allToRoot";

                $validPath = [
                    "tplsUser." . $userId . ".welcome",
                    "allToRoot",
                    "costum.colors",
                    "costum.slug",
                    "costum.loaded",
                    "costum.dashboard",
                    "costum.reference.themes",
                    $tplPath,
                ];

                if (in_array($path, $validPath) == true) {
                    $res = true;
                }
            }
        }

        $costumSession = $this->session->get('costum');
        if (! empty($costumSession) &&
            ! empty($costumSession["form"]) &&
            ! empty($costumSession["form"]["anyOnewithLinkCanAnswer"])) {
            $res = true;
        } elseif ($collection == FormInterface::ANSWER_COLLECTION) {
            $ell = $this->db->findOneById($collection, $id);

            if (! empty($ell) && ! empty($ell["form"])) {
                $ellform = $this->db->findOneById(FormInterface::COLLECTION, (string) $ell["form"]);
                if (! empty($ellform) && ! empty($ellform["temporarymembercanreply"]) && $ellform["temporarymembercanreply"] == "true") {
                    $res = true;
                }
            }
        }
        return $res;
    }

    // TODO : $userId is not used
    /**
     * Check if a user is a local citizen of a specific city.
     *
     * @param string|null $userId The ID of the user.
     * @param string $cityId The ID of the city.
     * @return bool Returns true if the user is a local citizen of the city, false otherwise.
     */
    public function isLocalCitizen(?string $userId, string $cityId): bool
    {
        $cityMap = $this->getModelCity()->getUnikeyMap($cityId);
        if (empty($cityMap) || empty($cityMap["insee"])) {
            return false;
        }
        $userSession = $this->session->get('user');
        return (! empty($userSession["codeInsee"]) && $userSession["codeInsee"] == $cityMap["insee"]) ? true : false;
    }

    // TODO : $userId is not used
    /**
     * Check if the user with the given ID and type is a parent admin.
     *
     * @param string $id The ID of the user.
     * @param string $type The type of the user.
     * @param string|null $userId The ID of the parent user.
     * @param array|null $elt The additional element to consider.
     * @return bool Returns true if the user is a parent admin, false otherwise.
     */
    public function isParentAdmin(string $id, string $type, ?string $userId = null, ?array $elt = null): bool
    {
        $res = false;
        $userId = $this->session->getUserId() ?? "";
        if (empty($elt)) {
            $elt = $this->getModelElement()->getElementById($id, $type, null, ["name", "parent"]);
        }
        if (isset($elt["parent"]) && ! empty($elt["parent"])) {
            foreach ($elt["parent"] as $k => $v) {
                if (is_array($v) && $v["type"] == PersonInterface::COLLECTION) {
                    if ($k == $this->session->getUserId()) {
                        $res = true;
                    }
                } else {
                    if (is_string($k) && is_array($v) && ! empty($v["type"]) && is_string($v["type"])) {
                        $res = $this->isElementAdmin($k, $v["type"], $userId, true);
                    }
                }
            }
        }
        return $res;
    }

    // TODO : $userId is not used
    /**
     * Check if an element is a child of an admin.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @param string|null $userId The ID of the user (optional).
     * @param array|null $elt The element data (optional).
     * @param string|null $formParentId The ID of the parent form (optional).
     * @return bool Returns true if the element is a child of an admin, false otherwise.
     */
    public function isElementChildAdmin(string $id, string $type, ?string $userId = null, ?array $elt = null, ?string $formParentId = null): bool
    {
        $res = false;
        if (empty($elt)) {
            $elt = $this->getModelElement()->getElementById($id, $type, null, ["name", "type", "id", "step"]);
        }
        $formParent = $this->getModelElement()->getElementById($formParentId, FormInterface::COLLECTION, null, ["name", "parent", "subForms"]);
        if (isset($elt["type"]) && (isset($elt["id"]) || isset($elt["step"])) && ($elt["type"] == "openForm" || $elt["type"] == "inputs") && isset($formParent["subForms"]) && (in_array($elt["id"], $formParent["subForms"]) || in_array($elt["step"], $formParent["subForms"]))) {
            if (isset($formParent["parent"]) && ! empty($formParent["parent"]) && empty($checkParent)) {
                foreach ($formParent["parent"] as $k => $v) {
                    if ($v["type"] == PersonInterface::COLLECTION) {
                        if ($k == $this->session->getUserId()) {
                            $res = true;
                        }
                    } else {
                        $res = $this->isElementAdmin($k, $v["type"], $this->session->getUserId(), true);
                    }
                    if (! empty($res)) {
                        return $res;
                    }
                }
            }
        }
        return $res;
    }

    /**
     * Check if the user can assign a badge.
     *
     * @param string $badgeId The ID of the badge.
     * @param array|null $badgeElement The badge element.
     * @return bool Returns true if the user can assign the badge, false otherwise.
     */
    public function canAssignBadge(string $badgeId, ?array $badgeElement = null): bool
    {
        return $this->isPublicBadge($badgeId, $badgeElement) || $this->badgeIsAdminEmetteur($badgeId, $badgeElement);
    }

    /**
     * Vérifie si le badge de l'émetteur est un badge d'administrateur.
     *
     * @param string $badgeId L'identifiant du badge.
     * @param array|null $badgeElement Les éléments du badge (optionnel).
     * @return bool Retourne true si le badge est un badge d'administrateur, sinon false.
     */
    public function badgeIsAdminEmetteur(string $badgeId, ?array $badgeElement = null): bool
    {
        if (! isset($badgeElement)) {
            $badgeElement = $this->db->findOneById(BadgeInterface::COLLECTION, $badgeId);
        }
        if (! isset($badgeElement['issuer'])) {
            return $badgeElement['creator'] == $this->session->getUserId();
        }
        if (! is_array($badgeElement['issuer'])) {
            return false;
        }
        foreach ($badgeElement['issuer'] as $idEmetteur => $value) {
            if ($value['type'] == PersonInterface::COLLECTION) {
                $id = ((string) $idEmetteur);
                if ($this->session->getUserId() == $id) {
                    return true;
                }
            } elseif ($this->isElementAdmin($idEmetteur, $value['type'], $this->session->getUserId(), false, false)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a badge is public.
     *
     * @param string $badgeId The ID of the badge.
     * @param array|null $badgeElement The badge element.
     * @return bool Returns true if the badge is public, false otherwise.
     */
    public function isPublicBadge(string $badgeId, ?array $badgeElement = null): bool
    {
        if (! isset($badgeElement)) {
            $badgeElement = $this->db->findOneById(BadgeInterface::COLLECTION, $badgeId);
        }
        return isset($badgeElement) && isset($badgeElement['preferences']) && isset($badgeElement['preferences']['private']) && ! $badgeElement['preferences']['private'];
    }

    /**
     * Check if the user has the specified roles.
     *
     * @param array|null $authRoles The roles to check.
     * @return bool Returns true if the user has the specified roles, false otherwise.
     */
    public function hasRoles(?array $authRoles): bool
    {
        $costum = $this->getModelCostum()->getCostum();
        $element = $this->getModelSlug()->getElementBySlug($costum["slug"], ["links"]);
        $userId = $this->session->getUserId();
        $hasIt = false;
        if (isset($element["el"]) && isset($element["el"]["links"]) && isset($element["el"]["links"]["members"]) && isset($element["el"]["links"]["members"][$userId]) && isset($userId)) {
            $userIfMember = $element["el"]["links"]["members"][$this->session->getUserId()];

            if (isset($userIfMember["isAdmin"]) && $userIfMember["isAdmin"]) {
                $hasIt = true;
            } else {
                $userRoles = [];
                if (isset($userIfMember) && isset($userIfMember["roles"])) {
                    $userRoles = $userIfMember["roles"];
                }

                if (isset($authRoles) && isset($authRoles["roles"]) && (is_countable($userRoles) ? count($userRoles) : 0) != 0) {
                    $userRoles = array_map('strtolower', $userRoles);
                    foreach ($authRoles["roles"] as $k => $v) {
                        if (in_array(strtolower($v), $userRoles)) {
                            $hasIt = true;
                        }
                    }
                }
            }
        }

        return $hasIt;
    }
}
