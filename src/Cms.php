<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CmsInterface;

use PixelHumain\Models\Traits\AuthorisationTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CmsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;

use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\SearchNewTrait;

// CO2::getContextList(self::MODULE)
// Yii::get('module')->getModule('costum')->assetsUrl
// Yii::get('application')->controller->module->assetsUrl
// Rest::json

class Cms extends BaseModel implements CmsInterface, ElementTraitInterface, SearchNewTraitInterface, DocumentTraitInterface, AuthorisationTraitInterface, CostumTraitInterface, CommentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    use ElementTrait;
    use SearchNewTrait;
    use DocumentTrait;
    use AuthorisationTrait;
    use CostumTrait;
    use CommentTrait;

    //TODO Translate
    public array $types = [
        "cms" => "CMS",
        "doc" => "Documentation",
    ];

    public array $option = [
        "tpls.blockCms.events.blockevent" => "Le bloc d'évènements",
        "tpls.blockCms.events.blockeventcommunity" => "Les évènements de la communauté",
        "tpls.blockCms.events.eventcarousel" => "Les évènements en carousel",
        "tpls.blockCms.events.blockeventsmarterritoire" => "Les évènements smarterritoire",
        "tpls.blockCms.events.blockeventslide" => "Les évènements en slide",
        "tpls.blockCms.article.blockarticles" => "Les articles en bloc",
        "tpls.blockCms.article.blockarticlescommunity" => "Les articles de la communauté",
        "tpls.blockCms.article.blockarticlescarousel" => "Les articles sous forme de carousel",
        "tpls.blockCms.map.basicmaps" => "La carte basic",
        "tpls.blockCms.map.communitymaps" => "La carte de la communauté",
        "tpls.blockCms.community.communitycarousel" => "La Communauté en carousel",
        "tpls.blockCms.community.communityblock" => "Un block de la communautée",
        "tpls.blockCms.textImg.blockwithimg" => "Un bloc avec un texte et une image",
        "tpls.blockCms.projects.blockcarousel" => "Les projets sous forme de carousel",
        "tpls.blockCms.news.basicnews" => "L'actualité",
    ];

    public array $icones = [
        "fa-newspaper-o" => "News (atualité)",
        "fa-calendar " => "Calendrier (évènement)",
        "fa-lightbulb-o " => "Idée (projets)",
        "fa-map-marker" => "Carte",
        "fa-bullhorn" => "Annonces",
        "fa-user" => "Personne",
        "fa-gavel" => "Sondages",
        "fa-rocket" => "rocket",
    ];

    //From Post/Form name to database field name
    public array $dataBinding = [
        "id" => [
            "name" => "id",
        ],
        "section" => [
            "name" => "section",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "category" => [
            "name" => "category",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "name" => [
            "name" => "name",
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "geo" => [
            "name" => "geo",
        ],
        "geoPosition" => [
            "name" => "geoPosition",
        ],
        "description" => [
            "name" => "description",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "position" => [
            "name" => "position",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "structags" => [
            "name" => "structags",
        ],
        "level" => [
            "name" => "level",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "rank" => [
            "name" => "rank",
        ],
        "path" => [
            "name" => "path",
        ],
        "haveTpl" => [
            "name" => "haveTpl",
        ],
        "page" => [
            "name" => "page",
        ],
        "allPage" => [
            "name" => "choosePage",
        ],
        "gitlab" => [
            "name" => "gitlab",
        ],
        "share" => [
            "name" => "share",
        ],
        "tplParent" => [
            "name" => "tplParent",
        ],
        "userCounter" => [
            "name" => "userCounter",
        ],
        "blockParent" => [
            "name" => "blockParent",
        ],
        "order" => [
            "name" => "order",
        ],
        "tplsUser" => [
            "name" => "tplsUser",
        ],
        "cmsList" => [
            "name" => "cmsList",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "source" => [
            "name" => "source",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "color" => [
            "name" => "color",
        ],
        "siteParams" => [
            "name" => "siteParams",
        ],
        "dontRender" => [
            "name" => "dontRender",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
    }

    public function insertMultiple(array $cms): array
    {
        $res = $this->db->batchInsert(CmsInterface::COLLECTION, $cms);
        return $res;
    }

    // TODO : attention conflit possible avec Cms trait
    public function getCms($post)
    {
        $cms = $this->getModelElement()->getElementById($post["ider"], CmsInterface::COLLECTION);
        return $cms;
    }

    public function getCmsExist($post)
    {
        $res = [];
        $cmsList = $this->db->find(
            CmsInterface::COLLECTION,
            [
                "parent." . $post["contextId"] => [
                    '$exists' => 1,
                ],
            ]
        );
        $res["element"] = [];
        foreach ($cmsList as $key => $value) {
            array_push($res["element"], [
                "id" => (string) $value["_id"],
            ]);
        }
        return array_merge($res);
    }

    public function getProperties($post)
    {
        $cmsListe = $this->db->find(
            CmsInterface::COLLECTION,
            [
                "parent." . $post["contextId"] => [
                    '$exists' => 1,
                ],
            ]
        );
        return $cmsListe;
    }

    public function getTemplateByCategory($post)
    {
        $tplList = $this->db->find(
            CmsInterface::COLLECTION,
            [
                "tplsUser." . $post["contextId"] . "." . $post["page"] => "backup",
            ]
        );
        return $tplList;
    }

    // Todo CO2::getContextList(self::MODULE) ne doit pas être dans modele
    public function getConfig()
    {
        return [
            "collection" => CmsInterface::COLLECTION,
            "controller" => CmsInterface::CONTROLLER,
            "categories" => CO2::getContextList(CmsInterface::MODULE),
            "lbhp" => true,
        ];
    }

    public function getPoiByIdAndTypeOfParent($id, $type, $orderBy = null)
    {
        $cmses = $this->db->findAndSort(CmsInterface::COLLECTION, [
            "parentId" => $id,
            "parentType" => $type,
        ], $orderBy);
        return $cmses;
    }

    //TODO corriger ou supprimer -> DEPRECIATED use getPoiByTagWhereSortAndLimit
    public function getCmsByTagsAndLimit($limitMin = 0, $indexStep = 15, $searchByTags = [], $where = [], $fields = [])
    {
        if (empty($where)) {
            $where = [
                "name" => [
                    '$exists' => 1,
                ],
            ];
        } else {
            $where["name"] = [
                '$exists' => 1,
            ];
        }
        if (@$searchByTags && ! empty($searchByTags)) {
            $queryTag = [];
            foreach ($searchByTags as $key => $tag) {
                if ($tag != "") {
                    $queryTag[] = $this->db->MongoRegex("/" . $tag . "/i");
                }
            }
            if (! empty($queryTag)) {
                $where["tags"] = [
                    '$in' => $queryTag,
                ];
            }
        }

        $cmses = $this->db->findAndSort(CmsInterface::COLLECTION, $where, [
            "updated" => -1,
        ], $limitMin, $fields);
        return $cmses;
    }

    public function getCmsByWhereSortAndLimit($where = [], $orderBy = [
        "updated" => -1,
    ], $indexStep = 10, $indexMin = 0)
    {
        $cmses = $this->db->findAndSortAndLimitAndIndex(CmsInterface::COLLECTION, $this->getModelSearchNew()->prepareTagsRequete($where), $orderBy, $indexStep, $indexMin);
        return $cmses;
    }

    //dans une liste de cmses retourne tout ceux qui ont une $val donnée
    //$l is an array of cmses
    //$val is a string
    // work for tags and structags
    public function getCmsByStruct($l, $val, $field = "tags")
    {
        $res = [];

        foreach ($l as $k => $v) {
            if (isset($v[$field]) && is_string($v[$field]) && $val == $v[$field]) {
                $res[] = $v;
            } elseif (isset($v[$field]) && is_array($v[$field]) && in_array($val, $v[$field])) {
                $res[] = $v;
            }
        }
        return $res;
    }

    public function getById($id)
    {
        $cms = $this->db->findOneById(CmsInterface::COLLECTION, $id);
        $cms["images"] = $this->listImages($id);
        $cms["files"] = $this->listFiles($id);
        return $cms;
    }

    // Todo : Rest::json ne doit pas être dans un modele
    public function deleteCms($id)
    {
        $imgValue = [];
        $cms = $this->getModelElement()->getElementById($id, CmsInterface::COLLECTION);
        $cmsChild = $this->db->find(CmsInterface::COLLECTION, [
            "blockParent" => $id,
        ]);
        $parentId = null;
        $parentType = null;
        if (isset($cms["type"]) && $cms["type"] != "blockCms") {
            $parentId = array_keys($cms["parent"])[0];
            $parentType = $cms["parent"][$parentId]["type"];
        }

        $sessionUser = $this->session->get('user');
        if (isset($cms["type"]) && $cms["type"] == "blockCms" && ! $this->getModelAuthorisation()->isUserSuperAdmin($this->session->getUserId()) && ! in_array($sessionUser["slug"], ["mirana", "gova", "ifals", "devchriston", "nicoss", "Jaona", "anatolerakotoson", "yorre"]) && $imgValue["yorre"] !== "") {
            return Rest::json([
                "result" => false,
                "error" => "403",
                "msg" => "Forbidden : Only allowed for the super administrator",
            ]);
        } elseif (isset($cms["type"]) && $cms["type"] != "blockCms" && ! $this->getModelAuthorisation()->canDeleteElement($parentId, $parentType, $this->session->getUserId())) {
            return Rest::json([
                "result" => false,
                "error" => "403",
                "msg" => "Forbidden : Only allowed for the administrator",
            ]);
        } elseif (! empty($cmsChild)) {
            $this->deleteChild($cmsChild);
            $this->db->remove(CmsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            //REMOVE TABS CMS ENTRY
            $this->db->update(CmsInterface::COLLECTION, [
                "tabs." . $id => [
                    "\$exists" => true,
                ],
            ], [
                "\$unset" => [
                    "tabs." . $id => "",
                ],
            ]);
            return Rest::json([
                "result" => true,
                "msg" => "The element has been deleted succesfully",
            ]);
            // return Rest::json(array("result"=> false, "msg" => "<b>Can't be deleted :</b><br> This container is not empty. Try to delete their content first!"));
        } else {
            $this->db->remove(CmsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            //REMOVE TABS CMS ENTRY
            $this->db->update(CmsInterface::COLLECTION, [
                "tabs." . $id => [
                    "\$exists" => true,
                ],
            ], [
                "\$unset" => [
                    "tabs." . $id => "",
                ],
            ]);
            return Rest::json([
                "result" => true,
                "msg" => "The element has been deleted succesfully",
            ]);
        }
    }

    public function deleteChild($cmsList)
    {
        foreach ($cmsList as $cms) {
            $cmsChild = $this->db->find(CmsInterface::COLLECTION, [
                "blockParent" => (string) $cms['_id'],
            ]);
            if (! empty($cmsChild)) {
                $this->deleteChild($cmsChild);
            }
            $this->db->remove(CmsInterface::COLLECTION, [
                "_id" => $cms['_id'],
            ]);
            //REMOVE TABS CMS ENTRY
            $this->db->update(CmsInterface::COLLECTION, [
                "tabs." . (string) $cms['_id'] => [
                    "\$exists" => true,
                ],
            ], [
                "\$unset" => [
                    "tabs." . (string) $cms['_id'] => "",
                ],
            ]);
        }
    }

    public function delete($id, $userId)
    {
        if (! @$userId) {
            return [
                "result" => false,
                "msg" => "You must be loggued to delete something",
            ];
        }

        $cms = self::getById($id);
        if (! self::canDeleteCms($userId, $id, $cms)) {
            return [
                "result" => false,
                "msg" => "You are not authorized to delete this cms.",
            ];
        }

        //Delete the comments
        $resComments = $this->getModelComment()->deleteAllContextComments($id, CmsInterface::COLLECTION, $userId);
        if (@$resComments["result"]) {
            $this->db->remove(CmsInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            $resDocs = $this->getModelDocument()->removeDocumentByFolder(CmsInterface::COLLECTION . "/" . $id);
        } else {
            return $resComments;
        }

        return [
            "result" => true,
            "msg" => "The element has been deleted succesfully",
            "resDocs" => $resDocs,
        ];
    }

    public function canDeleteCms($userId, $id, $cms = null)
    {
        if ($cms == null) {
            $cms = self::getById($id);
        }
        //To Delete cms, the user should be creator or can delete the parent of the cms
        if ($userId == @$cms['creator'] || $this->getModelAuthorisation()->canDeleteElement(@$cms["parentId"], @$cms["parentType"], $userId)) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataBinding(): array
    {
        return self::$dataBinding;
    }

    //based on a list opis
    //hierarchy can be defined with tags
    //the parent carries tag #test, and all his children cayy #test.chichi1, #test.chichi2 ,
    //$field is the tag based field hierarchy works on...
    public function hierarchy($pList, $field = "tags")
    {
        $cmsList = [];
        $blockParents = [];
        foreach ($pList as $key => $value) {
            //
            if (isset($value[$field]) && is_string($value[$field])) {
                $cmsTree = explode(".", $value[$field]);
                //this is a child
                if (count($cmsTree) > 1) {
                    $parentTag = $cmsTree[0];
                    if (! isset($blockParents[$parentTag])) {
                        $blockParents[$parentTag] = [];
                    }

                    $blockParents[$parentTag][] = $value;
                }
                //this is a parent
                else {
                    $cmsList[$value[$field]] = $value;
                }
            }
            //use case if strutags is an array
            elseif (isset($value[$field]) && is_array($value[$field]) && count($value[$field])) {
                foreach ($value[$field] as $tk => $tv) {
                    $cmsTree = explode(".", $tv);
                    if (count($cmsTree) > 1) {
                        $parentTag = $cmsTree[0];
                        if (! isset($blockParents[$parentTag])) {
                            $blockParents[$parentTag] = [];
                        }

                        $blockParents[$parentTag][] = $value;
                    } else {
                        $cmsList[$tv] = $value;
                    }
                }
            } else {
                $cmsList[] = $value;
            }
        }
        return [
            "orphans" => $cmsList,
            "parentTree" => $blockParents,
        ];
    }

    public function getCmsByType($type)
    {
        return $this->db->find(CmsInterface::COLLECTION, [
            "type" => $type,
        ]);
    }

    public function getCmsByWhere($where = [], $sortCriteria = null)
    {
        if ($sortCriteria != null) {
            return $this->db->findAndSort(CmsInterface::COLLECTION, $where, $sortCriteria, 500);
        } else {
            return $this->db->find(CmsInterface::COLLECTION, $where);
        }
    }

    public function getCmsWithChildren($costumId, $page, $sortCriteria = null, $footer = null)
    {
        $parents = $this->getCmsByWhere(
            [
                '$and' => [[
                    '$or' => [[
                        "advanced.persistent" => [
                            '$exists' => true,
                        ],
                    ], [
                        "page" => $page,
                    ]],
                ], [
                    "blockParent" => [
                        '$exists' => false,
                    ],
                ], [
                    "parent." . $costumId => [
                        '$exists' => 1,
                    ],
                ]],
            ],
            $sortCriteria
        );

        $blockChildren = $this->getCmsByWhere(
            [
                //TRY REMOVE and Have TPL TYPE////
                //"type" => "blockChild",
                '$and' => [
                    //TRY REMOVE and Have TPL TYPE////
                    [
                        '$or' => [[
                            "advanced.persistent" => [
                                '$exists' => true,
                            ],
                        ], [
                            "page" => $page,
                        ]],
                    ],
                    [
                        "blockParent" => [
                            '$exists' => 1,
                        ],
                    ],
                    [
                        "parent." . $costumId => [
                            '$exists' => 1,
                        ],
                    ],
                ],
                // //END TRYYYY/////
                // "parent.".$costumId => array('$exists'=>1)
            ],
            [
                "position" => 1,
            ]
        );

        $countChildren = $this->db->count(CmsInterface::COLLECTION, [
            "type" => "blockChild",
            "parent." . $costumId => [
                '$exists' => 1,
            ],
            "page" => $page,
        ]);
        if ($countChildren > 400) {
            $str = "<div class='container' style='width:100%; text-align: center;background-color: lightgray;height: 190px;display: flex;
			justify-content: center;'><div style='background-color: white;width: 500px;margin: auto;padding: 25px;'><h1 style='color:#e6344d'>Oups</h1><p>Chargement incomplet!!</p>";
            if ($this->getModelAuthorisation()->isUserSuperAdmin($this->session->getUserId())) {
                $str .= "<font>Impossible de charger " . $countChildren . " des block CMS</font>";
            };
            $str .= "</div></div>";
            echo $str;
        }

        $addChildren = function ($parent) use (&$addChildren, &$blockChildren) {
            $parent["blockChildren"] = [];
            foreach ($blockChildren as $childKey => $child) {
                if (isset($child["blockParent"]) && ($child["blockParent"] == $parent["_id"]->{'$id'})) {
                    $parent["blockChildren"][$childKey] = $addChildren($child);
                }
            }

            return $parent;
        };

        foreach ($parents as $id => $parent) {
            $parents[$id] = $addChildren($parent);
        }

        return $parents;
    }

    public function getCmsFooter($costumId)
    {
        $footer = self::getCmsWithChildren($costumId, "allPages", [
            "position" => 1,
        ]);

        return $footer;
    }

    public function getByIdWithChildreen($id, $nofile = false)
    {
        $cms = $this->db->findOneById(CmsInterface::COLLECTION, $id);

        $addChildren = function ($parent) use (&$addChildren) {
            $parent["blockChildren"] = [];
            $parentId = "";
            if (isset($parent["parent"])) {
                $parentId = array_keys($parent["parent"])[0];
            }

            $blockChildren = $this->db->find(
                CmsInterface::COLLECTION,
                [
                    "blockParent" => @$parent["_id"]->{'$id'},
                    "parent." . $parentId => [
                        '$exists' => 1,
                    ],
                ]
            );
            foreach ($blockChildren as $childKey => $child) {
                if (isset($child["blockParent"]) && ($child["blockParent"] == $parent["_id"]->{'$id'})) {
                    $parent["blockChildren"][$childKey] = $addChildren($child);
                }
            }

            return $parent;
        };

        $cms = $addChildren($cms);

        // Use case notragora
        if (empty($nofile)) {
            $cms["images"] = $this->listImages($id);
            $cms["files"] = $this->listFiles($id);
        }
        return $cms;
    }

    public function getTplByWhere($where = [], $sortCriteria = null)
    {
        return $this->db->find("cms", [
            "type" => "template",
            "category" => "Sample",
        ]);
    }

    public function listPage($pList, $field = "page")
    {
        $listPage = [];
        $pages = [];
        foreach ($pList as $key => $value) {
            if (isset($value[$field]) && is_string($value[$field])) {
                $pages[] = $value[$field];
            }
        }
        foreach (array_unique($pages) as $filter) {
            $listPage[] = $filter;
        }
        return ($listPage);
    }

    public function duplicateDocumentFromTplBlock()
    {
        /* Just check if image OR BackgroundUrl to duplicate in good costum*/ //"be carefull to duplicate document";
    }

    public function recursiveDuplicateBlock($blockTODuplicate, $context, $blockParent = null)
    {
        $duplicateBlock = $blockTODuplicate;
        if (! in_array($context["parentSlug"], array_keys(@$blockTODuplicate["parent"]))) {
            self::duplicateDocumentFromTplBlock();
        }

        if (! empty($duplicateBlock["blockChildren"])) {
            $duplicateChildren = $duplicateBlock["blockChildren"];
        }

        if (! isset($duplicateBlock["blockName"]) && isset($duplicateBlock["name"])) {
            $duplicateBlock["blockName"] = $duplicateBlock["name"];
        }
        $removeDataUnuseful = ["_id", "profilImageUrl", "profilMediumImageUrl", "profilThumbImageUrl", "profilMarkerImageUrl", "blockChildren", "haveTpl", "type", "name"];
        foreach ($removeDataUnuseful as $v) {
            if (isset($duplicateBlock[$v]) && ($v != "name" || @$blockTODuplicate["type"] != "blockCms")) {
                unset($duplicateBlock[$v]);
            }
        }
        /** The first condition is for two useCase (and potentially the third case to develop in front)
            1 -- Duplicate the children inside this recursive loop after the first parent
            2 -- Case of using BLOCKCMS (template) where destination is defined (cms/useCmsAction.php)
            3 -- [TO DEVELOP] : Case when we want to copy, cut or duplicate a block to another destination
        **/
        if (! empty($blockParent)) {
            $duplicateBlock["blockParent"] = $blockParent;
        }
        //Or case of duplicate other then section like title or colunm inside directly
        elseif (! empty($blockTODuplicate["blockParent"])) {
            $duplicateBlock["blockParent"] = $blockTODuplicate["blockParent"];
        }
        $duplicateBlock["parent"] = [
            $context["parentId"] => [
                "type" => $context["parentType"],
                "name" => $context["parentSlug"],
            ],
        ];
        $duplicateBlock["modified"] = $this->db->MongoDate(time());
        $duplicateBlock["updated"] = time();
        $duplicateBlock["creator"] = $this->session->getUserId();
        $duplicateBlock["created"] = time();
        $duplicateBlock["source"] = [
            "insertOrign" => "costum",
            "key" => $context["parentSlug"],
            "keys" => [$context["parentSlug"]],
        ];
        if (! empty($context["page"])) {
            $duplicateBlock["page"] = trim($context["page"], "#");
        }

        // add type  if context type setted and blockParent not set ( can save section only as blockCms)
        // remove page and order
        // add name for new blockCms
        if (isset($context["type"]) && isset($context["type"]) == "blockCms" && ! isset($duplicateBlock["blockParent"])) {
            $duplicateBlock["_id"] = $this->db->MongoId($context["id"]);
            $duplicateBlock["type"] = $context["type"];
            $duplicateBlock["name"] = $context["name"];
            $duplicateBlock["css"]["width"] = "100%";
            unset($duplicateBlock["page"]);
            unset($duplicateBlock["order"]);
        }

        $duplicateBlock = $this->db->insert(CmsInterface::COLLECTION, $duplicateBlock);

        if (! empty($duplicateChildren)) {
            $duplicateBlock["blockChildren"] = [];
            foreach ($duplicateChildren as $key => $v) {
                $child = self::recursiveDuplicateBlock($v, $context, (string) $duplicateBlock["_id"]);
                $duplicateBlock["blockChildren"][(string) $child["_id"]] = $child;
            }
        }
        return $duplicateBlock;
    }

    public function duplicateBlock($idCms, $context, $blockParent = null)
    {
        $cmsInsertWithChildren = [];

        foreach ($idCms as $key) {
            // GET THE BLOCK WHITH ALL CHILDREN TO DUPLICATE
            $blockTODuplicate = self::getByIdWithChildreen($key, true);
            if (! empty($blockTODuplicate)) {
                $cmsInsertWithChildren = self::recursiveDuplicateBlock($blockTODuplicate, $context, $blockParent);
            }
        }

        return $cmsInsertWithChildren;
    }

    public function upPersistentChild($blockTOPersist, $value)
    {
        $persistBlock = $blockTOPersist;

        if (! empty($persistBlock["blockChildren"])) {
            $persistChildren = $persistBlock["blockChildren"];
        }

        if (! empty($persistChildren)) {
            $persistBlock["blockChildren"] = [];
            foreach ($persistChildren as $key => $v) {
                $this->db->update(CmsInterface::COLLECTION, [
                    "_id" => $this->db->MongoId($key),
                ], [
                    '$set' => [
                        "advanced.persistent" => $value,
                    ],
                ]);

                $child = self::upPersistentChild($v, $value);
            }
        }

        return $persistBlock;
    }

    public function updatePersistent($idCms, $value)
    {
        $cms = self::getByIdWithChildreen($idCms, true);

        self::upPersistentChild($cms, $value);
    }

    public function findCmsOrphans($idCms, $page)
    {
        $cmsOrphans = [];
        $childBlock = $this->db->find(CmsInterface::COLLECTION, [
            "blockParent" => $idCms,
        ], ["page"]);
        $childBlockID = array_keys($childBlock);
        if (! empty($childBlock)) {
            foreach ($childBlock as $key => $value) {
                $cmsOrphans[] = [
                    "id" => $value["_id"]->{'$id'},
                    "page" => $value["page"],
                ];
            }
        }
        return $cmsOrphans;
    }

    // Todo Yii::get('application')->controller->module->assetsUrl ne doit pas être dans un modele
    public function getParamsView($params, $clienturi = "", $el = [])
    {
        $costum = $this->getModelCostum()->getCostumCache();
        $convertToview = [
            "cmsList" => [],
            "blockKey" => (string) $params["_id"],
            "v" => $params,
            "page" => $params['page'] ?? "",
            "canEdit" => $costum["editMode"],
            "type" => $params["path"],
            "kunik" => (string) $params["_id"],
            "content" => "",
            "el" => $el,
            "i" => 0,
            'costumData' => $costum,
            'blockName' => @$params["name"],
            "clienturi" => $clienturi,
            // 'range'     => $i,
            "defaultImg" => Yii::get('application')->controller->module->assetsUrl . "/images/thumbnail-default.jpg",
        ];
        return $convertToview;
    }

    // Todo Yii::get('module')->getModule('costum')->assetsUrl ne doit pas être dans un modele
    public function getParamsBlock($params, $clienturi = "", $el = [])
    {
        $costum = [];
        $path = $params["path"];
        // Todo $assetsUrl n'est pas utilisé donc je le commente
        // $assetsUrl = Yii::get('module')->getModule('costum')->assetsUrl;
        $pathExplode = explode('.', $params["path"]);
        $count = count($pathExplode);
        $content = $params['content'] ?? [];
        $kunik = $pathExplode[$count - 1] . $params["_id"];
        $blockKey = (string) $params["_id"];
        $blockName = (string) @$params["name"];

        $convertToview = [
            "cmsList" => $params,
            "blockKey" => $blockKey,
            "blockCms" => $params,
            "page" => $params["page"],
            // Todo $costum["editMode"] n'existe pas
            "canEdit" => @$costum["editMode"],
            "type" => $path,
            "kunik" => $kunik,
            "content" => $content,
            "blockName" => $blockName,
            "costum" => $this->getModelCostum()->getCostumCache(),
            "el" => [],
            'range' => 0,
            "clienturi" => $clienturi,
        ];
        return $convertToview;
    }

    // create or update cssFile
    public function createORupdateCss($contextId, $css)
    {
        $params = [
            "contextId" => $contextId,
            "css" => $css,
            "collection" => "cssFile",
            "created" => time(),
            "updated" => time(),
        ];

        $cssFile = $this->db->find("cssFile", [
            "contextId" => $contextId,
        ]);

        if (empty($cssFile)) {
            $params = $this->db->insert("cssFile", $params);
        } else {
            $this->db->update("cssFile", [
                "contextId" => $contextId,
            ], [
                '$set' => [
                    "css" => $css,
                    "updated" => time(),
                ],
            ]);
        }
    }

    public function getCssByCostumId($contextId)
    {
        return $cssFile = $this->db->find("cssFile", [
            "contextId" => $contextId,
        ]);
    }

    public function getNewPageName($page, $costum)
    {
        $pagename = str_replace(preg_replace('/[^0-9]/', '', $page), "", $page);
        $menu = array_keys($costum['costum']['app']);
        $pageNumber = (intval(preg_replace('/[^0-9]/', '', max(array_filter($menu, function ($v, $k) use ($pagename) {
            if (strpos($v, (string) $pagename) > -1) {
                return $v;
            }
        }, ARRAY_FILTER_USE_BOTH)))) + 1);
        $newPageName = "#$pagename$pageNumber";
        $newSubdomainName = ucfirst($pagename) . " $pageNumber";
        return [
            "pagename" => $newPageName,
            "subdomainName" => $newSubdomainName,
        ];
    }
}
