<?php

namespace PixelHumain\Models\Interfaces;

interface UrlInterface
{
    public const COLLECTION = "url";
}
