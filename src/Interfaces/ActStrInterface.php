<?php

namespace PixelHumain\Models\Interfaces;

interface ActStrInterface
{
    public const TEST = "test";

    public const ICON_QUESTION = "fa-question";

    public const ICON_SHARE = "fa-share-alt";

    public const ICON_COMMENT = "fa-comment";

    public const ICON_CLOSE = "fa-times";

    public const ICON_ADD = "fa-plus";

    public const ICON_VOTE = "fa-gavel";

    public const ICON_RSS = "fa-rss";

    public const VIEW_PAGE = "viewPage";

    public const VERB_VIEW = "view";

    public const VERB_ADD = "add";

    public const VERB_UPDATE = "update";

    public const VERB_CREATE = "create";

    public const VERB_DELETE = "delete";

    public const VERB_PUBLISH = "publish";

    public const VERB_JOIN = "join";

    public const VERB_WAIT = "wait";

    public const VERB_ASK = "ask";

    public const VERB_LEAVE = "leave";

    public const VERB_INVITE = "invite";

    public const VERB_INVITE_ALL = "invite_all";

    public const VERB_ACCEPT = "accept";

    public const VERB_CLOSE = "close";

    public const VERB_SIGNIN = "signin";

    public const VERB_HOST = "host";

    public const VERB_FOLLOW = "follow";

    public const VERB_CONFIRM = "confirm";

    public const VERB_AUTHORIZE = "authorize";

    public const VERB_ATTEND = "attend";

    public const VERB_COMMENT = "comment";

    public const VERB_REACT = "react";

    public const VERB_LIKE = "like";

    public const VERB_UNLIKE = "unlike";

    public const VERB_MENTION = "mention";

    public const VERB_REFUSE = "refuse";

    public const VERB_ADDROOM = "addactionroom";

    public const VERB_ADD_PROPOSAL = "addproposal";

    public const VERB_ADD_RESOLUTION = "addresolution";

    public const VERB_MODERATE = "moderate";

    public const VERB_AMEND = "ammend";

    public const VERB_ADD_ACTION = "addaction";

    public const VERB_VOTE = "vote";

    public const VERB_POST = "post";

    public const VERB_RETURN = "return";

    public const VERB_NOSENDING = "nosending";
    //public const VERB_NOREFERENCE = "noreferencing";

    public const VERB_SHARE = "share";

    public const VERB_ASSIGN = "assign";

    public const VERB_EMETTEUR_CONFIRM = "emetteurconfirm";

    public const VERB_RECEPTEUR_CONFIRM = "recepteurconfirm";

    public const VERB_REVOKE = "revoke";

    //BADGE
    public const VERB_BADGE_ASK = "badge_ask";

    public const ASK_ASSIGN_EMITER = "confirm_assign_emiter";

    public const ASK_ASSIGN_RECEIVER = "confirm_assign_receiver";

    public const SELF_ASSIGN = "self_asign";

    public const VERB_BADGE_CONFIRM = "badge_confirm";

    public const TYPE_URL = "url";

    public const TYPE_ACTIVITY_HISTORY = "history";

    public const TYPE_ACTIVITY_SHARE = "share";

    public const VERB_FOLLOW_AP = "follow_ap";

    /**
     * Builds an entry based on the given parameters.
     *
     * @param array $params The parameters used to build the entry.
     * @return array The built entry.
     */
    public function buildEntry(array $params): array;

    /**
     * Affiche une page à partir d'une URL.
     *
     * @param string $url L'URL de la page à afficher.
     * @return void
     */
    public function viewPage(string $url): void;
}
