<?php

namespace PixelHumain\Models\Interfaces;

interface I18NInterface
{
    /**
     * Translates a message in the specified language.
     *
     * @param string $category The category of the message.
     * @param string $message The message to be translated.
     * @param array $params The parameters to be replaced in the message.
     * @param string|null $language The language in which the message should be translated.
     * @return string The translated message.
     */
    public function translate(string $category, string $message, array $params = [], ?string $language);

    /**
     * Format a message with parameters in a specific language.
     *
     * @param string $message The message to format.
     * @param array $params The parameters to replace in the message.
     * @param string $language The language to use for formatting.
     * @return string The formatted message.
     */
    public function format($message, $params, $language);

    /**
     * Returns the message source for the specified category.
     *
     * @param string $category The category of the message source.
     * @return mixed The message source for the specified category.
     */
    public function getMessageSource($category);

    /**
     * Set the message formatter.
     *
     * @param mixed $value The value to set as the message formatter.
     * @return void
     */
    public function setMessageFormatter($value);
}
