<?php

namespace PixelHumain\Models\Interfaces;

interface AdminInterface
{
    /**
     * Adds a source in an element.
     *
     * @param string|null $id The ID of the element.
     * @param string|null $type The type of the element.
     * @param string|null $keySource The key source.
     * @param string|null $setKey The set key.
     * @param string $origin The origin of the source (default: "costum").
     * @return array|null The updated element.
     */
    public function addSourceInElement(?string $id = null, ?string $type = null, ?string $keySource = null, ?string $setKey = null, string $origin = 'costum'): ?array;

    /**
     * Removes a source from an element.
     *
     * @param string|null $id The ID of the element.
     * @param string|null $type The type of the element.
     * @param string|null $keySource The key of the source to remove.
     * @param string|null $setKey The key of the set to remove the source from.
     * @param string $origin The origin of the removal (default: "costum").
     * @return array|null An array containing the updated element, or null if the element was not found.
     */
    public function removeSourceFromElement(?string $id = null, ?string $type = null, ?string $keySource = null, ?string $setKey = null, string $origin = 'costum'): ?array;
}
