<?php

namespace PixelHumain\Models\Interfaces;

interface ClassifiedInterface
{
    public const COLLECTION = "classifieds";

    public const TYPE_RESSOURCES = "ressources";

    public const TYPE_JOBS = "jobs";

    public const CONTROLLER = "classified";

    public const TYPE_RESSOURCES_CONTROLLER = "ressource";

    public const TYPE_JOBS_CONTROLLER = "job";

    public const MODULE = "eco";

    public const ICON = "fa-bullhorn";

    public const ICON_RESSOURCES = "fa-cubes";

    public const ICON_JOBS = "fa-briefcase";

    /**
     * Get the configuration for the Classified class.
     *
     * @param string|null $context The context for the configuration. Default is null.
     * @return array The configuration array.
     */
    public function getConfig(?string $context = null): array;

    /**
     * Get a classified by its ID.
     *
     * @param string $id The ID of the classified.
     * @return array The classified data.
     */
    public function getById(string $id): ?array;

    /**
     * Counts the number of classifieds by type and section.
     *
     * @param string $type The type of classifieds.
     * @param mixed $section The section of classifieds (optional).
     * @return int The number of classifieds.
     */
    public function countBy(string $type, $section = null): int;

    /**
     * Retrieves a classified by its creator.
     *
     * @param string $id The ID of the classified.
     * @return array The classified data.
     */
    public function getClassifiedByCreator(string $id): array;
}
