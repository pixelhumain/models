<?php

namespace PixelHumain\Models\Interfaces;

interface AuthorisationInterface
{
    /**
     * Check if a user can edit an item.
     *
     * @param string|null $userId The user ID.
     * @param string $type The type of the item.
     * @param string $itemId The ID of the item.
     * @param string|null $parentType The type of the parent item (optional).
     * @param string|null $parentId The ID of the parent item (optional).
     * @param bool $deleteProcess Flag indicating if it is a delete process (optional).
     * @return bool Returns true if the user can edit the item, false otherwise.
     */
    public function canEditItem(?string $userId, string $type, string $itemId, ?string $parentType = null, ?string $parentId = null, bool $deleteProcess = false): bool;

    /**
     * Check if a user can edit an item or open an edition.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param string|null $userId The ID of the user.
     * @param string|null $parentType The type of the parent entity (optional).
     * @param string|null $parentId The ID of the parent entity (optional).
     * @return bool Returns true if the user can edit the item or open an edition, false otherwise.
     */
    public function canEditItemOrOpenEdition(string $idEntity, string $typeEntity, ?string $userId, ?string $parentType = null, ?string $parentId = null): bool;

    /**
     * Check if the entity is open for edition.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param array|null $preferences The preferences for the entity (optional).
     * @return bool Returns true if the entity is open for edition, false otherwise.
     */
    public function isOpenEdition(string $idEntity, string $typeEntity, ?array $preferences = null): bool;

    /**
     * Check if the user is an interface admin.
     *
     * @param string|null $id The user ID.
     * @param string|null $type The user type.
     * @return bool Returns true if the user is an interface admin, false otherwise.
     */
    public function isInterfaceAdmin(?string $id = null, ?string $type = null): bool;

    /**
     * Check if the user is a super admin.
     *
     * @param string|null $userId The ID of the user.
     * @return bool Returns true if the user is a super admin, false otherwise.
     */
    public function isUserSuperAdmin(?string $userId): bool;

    /**
     * Check if the user is a custom admin.
     *
     * @return bool Returns true if the user is a custom admin, false otherwise.
     */
    public function isCostumAdmin(): bool;

    /**
     * Check if the user is an admin for a specific source entity.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param string $idUser The ID of the user.
     * @return bool Returns true if the user is an admin for the source entity, false otherwise.
     */
    public function isSourceAdmin(string $idEntity, string $typeEntity, string $idUser): bool;

    /**
     * Check if a user has admin access to an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param bool $checkParent (optional) Whether to check the parent element for admin access. Default is false.
     * @param bool $considerSuperAdmin (optional) Whether to consider the user as a super admin. Default is true.
     * @return bool Returns true if the user has admin access to the element, false otherwise.
     */
    public function isElementAdmin(string $elementId, string $elementType, string $userId, bool $checkParent = false, bool $considerSuperAdmin = true): bool;

    /**
     * Check if a user has the specified roles.
     *
     * @param string|null $userId The ID of the user.
     * @param array $roles The roles to check.
     * @return bool Returns true if the user has the specified roles, false otherwise.
     */
    public function isUser(?string $userId, array $roles): bool;

    /**
     * Check if the user is connected to the Meteor server.
     *
     * @param string $token The user's token.
     * @param string $accountId The user's account ID.
     * @param string $tokenName The name of the token.
     * @param mixed $test (optional) A test parameter.
     * @return bool Returns true if the user is connected to the Meteor server, false otherwise.
     */
    public function isMeteorConnected(string $token, string $accountId, string $tokenName, $test = null): bool;

    /**
     * Check if the user is valid.
     *
     * @param string $user The username.
     * @param string $pwd The password.
     * @return bool Returns true if the user is valid, false otherwise.
     */
    public function isValidUser(string $user, string $pwd): bool;

    /**
     * Check if the user can edit members data for a specific organization.
     *
     * @param string $organizationId The ID of the organization.
     * @return bool Returns true if the user can edit members data, false otherwise.
     */
    public function canEditMembersData(string $organizationId): bool;

    /**
     * Check if a user can participate in an item.
     *
     * @param string $userId The ID of the user.
     * @param string $itemType The type of the item.
     * @param string $itemId The ID of the item.
     * @param bool $openEdition Whether the item is in open edition or not.
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, string $itemType, string $itemId, bool $openEdition = true): bool;

    /**
     * Determines if the user can see a specific context.
     *
     * @param string|null $contextType The type of the context.
     * @param string|null $contextId   The ID of the context.
     * @param bool        $openEdition Whether open edition is allowed.
     *
     * @return bool Returns true if the user can see the context, false otherwise.
     */
    public function canSee(?string $contextType = null, ?string $contextId = null, bool $openEdition = true): bool;

    /**
     * Check if the user is a super admin for the CMS.
     *
     * @return bool Returns true if the user is a super admin for the CMS, false otherwise.
     */
    public function isUserSuperAdminCms(): bool;

    /**
     * Check if the entity is open data.
     *
     * @param string $idEntity The ID of the entity.
     * @param string $typeEntity The type of the entity.
     * @param array|null $preferences The preferences for the entity (optional).
     * @return bool Returns true if the entity is open data, false otherwise.
     */
    public function isOpenData(string $idEntity, string $typeEntity, ?array $preferences = null): bool;

    /**
     * Check if a user can delete an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param array|null $elt The element data (optional).
     * @return bool Returns true if the user can delete the element, false otherwise.
     */
    public function canDeleteElement(string $elementId, string $elementType, string $userId, ?array $elt = null): bool;

    /**
     * Determines if a user can see a private element.
     *
     * @param array|null $links The links associated with the element.
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @param string|null $creator The creator of the element.
     * @param string|null $parentType The type of the parent element.
     * @param string|null $parentId The ID of the parent element.
     * @return bool Returns true if the user can see the private element, false otherwise.
     */
    public function canSeePrivateElement(?array $links, string $type, string $id, ?string $creator, ?string $parentType = null, ?string $parentId = null): bool;

    /**
     * Check if the element with the given slug is admin for the specified user.
     *
     * @param string $slug The slug of the element.
     * @param string $userId The ID of the user.
     * @param bool $checkParent Optional. Whether to check the parent element as well. Default is false.
     * @return bool Returns true if the element is admin for the user, false otherwise.
     */
    public function isElementAdminBySlug(string $slug, string $userId, bool $checkParent = false): bool;

    /**
     * Check if a user is a member of an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @return bool Returns true if the user is a member of the element, false otherwise.
     */
    public function isElementMember(string $elementId, string $elementType, string $userId): bool;

    /**
     * Check if a user can edit a specific item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the item.
     * @param string $type The type of the item.
     * @return bool Returns true if the user can edit the item, false otherwise.
     */
    public function canEdit(string $userId, string $id, string $type): bool;

    /**
     * Check if the user is the owner of a specific resource.
     *
     * @param string $userId The ID of the user.
     * @param string $type The type of the resource.
     * @param string $id The ID of the resource.
     * @return bool Returns true if the user is the owner, false otherwise.
     */
    public function userOwner(string $userId, string $type, string $id): bool;

    /**
     * Returns an array of admins for the specified parent entity.
     *
     * @param string $parentId The ID of the parent entity.
     * @param string $parentType The type of the parent entity.
     * @param bool $pending (optional) Whether to include pending admins. Default is false.
     * @return array An array of admins.
     */
    public function listAdmins(string $parentId, string $parentType, bool $pending = false): array;

    /**
     * Vérifie si le captcha est valide.
     *
     * @param int|float $captcha Le captcha à vérifier.
     * @param string $reponse La réponse attendue du captcha.
     * @return bool Renvoie true si le captcha est valide, sinon renvoie false.
     */
    public function verifCaptcha($captcha, string $reponse): bool;

    /**
     * Check if the user has access to a menu button.
     *
     * @param array $value The value to check.
     * @return bool Returns true if the user has access, false otherwise.
     */
    public function accessMenuButton(array $value): bool;

    /**
     * Check if a specific condition is met.
     *
     * @param string $collection The collection name.
     * @param string $id The ID of the item.
     * @param mixed|null $tplGenerator The template generator.
     * @param string|null $path The path.
     * @return bool Returns true if the specific condition is met, false otherwise.
     */
    public function specificCondition(string $collection, string $id, $tplGenerator, ?string $path): bool;

    /**
     * Check if a user is a local citizen of a specific city.
     *
     * @param string|null $userId The ID of the user.
     * @param string $cityId The ID of the city.
     * @return bool Returns true if the user is a local citizen of the city, false otherwise.
     */
    public function isLocalCitizen(?string $userId, string $cityId): bool;

    /**
     * Check if the user with the given ID and type is a parent admin.
     *
     * @param string $id The ID of the user.
     * @param string $type The type of the user.
     * @param string|null $userId The ID of the parent user.
     * @param array|null $elt The additional element to consider.
     * @return bool Returns true if the user is a parent admin, false otherwise.
     */
    public function isParentAdmin(string $id, string $type, ?string $userId = null, ?array $elt = null): bool;

    /**
     * Check if an element is a child of an admin.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @param string|null $userId The ID of the user (optional).
     * @param array|null $elt The element data (optional).
     * @param string|null $formParentId The ID of the parent form (optional).
     * @return bool Returns true if the element is a child of an admin, false otherwise.
     */
    public function isElementChildAdmin(string $id, string $type, ?string $userId = null, ?array $elt = null, ?string $formParentId = null): bool;

    /**
     * Check if the user can assign a badge.
     *
     * @param string $badgeId The ID of the badge.
     * @param array|null $badgeElement The badge element.
     * @return bool Returns true if the user can assign the badge, false otherwise.
     */
    public function canAssignBadge(string $badgeId, ?array $badgeElement = null): bool;

    /**
     * Vérifie si le badge de l'émetteur est un badge d'administrateur.
     *
     * @param string $badgeId L'identifiant du badge.
     * @param array|null $badgeElement Les éléments du badge (optionnel).
     * @return bool Retourne true si le badge est un badge d'administrateur, sinon false.
     */
    public function badgeIsAdminEmetteur(string $badgeId, ?array $badgeElement = null): bool;

    /**
     * Check if a badge is public.
     *
     * @param string $badgeId The ID of the badge.
     * @param array|null $badgeElement The badge element.
     * @return bool Returns true if the badge is public, false otherwise.
     */
    public function isPublicBadge(string $badgeId, ?array $badgeElement = null): bool;

    /**
     * Check if the user has the specified roles.
     *
     * @param array|null $authRoles The roles to check.
     * @return bool Returns true if the user has the specified roles, false otherwise.
     */
    public function hasRoles(?array $authRoles): bool;
}
