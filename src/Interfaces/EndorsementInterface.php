<?php

namespace PixelHumain\Models\Interfaces;

interface EndorsementInterface
{
    public const COLLECTION = "endorsements";

    public const CONTROLLER = "endorsement";

    public const CONTEXT = "https://w3id.org/openbadges/v2";
}
