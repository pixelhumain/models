<?php

namespace PixelHumain\Models\Interfaces;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Regex;
use MongoDB\BSON\UTCDateTime;

interface PhdbDiInterface
{
    public function find(string $collection, array $where = [], ?array $fields = null): array;

    public function findByIds(string $collection, array $ids, ?array $fields = null): array;

    public function findAndSort(string $collection, array $where = [], array $sortCriteria, int $limit = 0, ?array $fields = null): array;

    public function distinct(string $collection, string $key, array $where = []): array;

    public function count(string $collection, array $where = []): int;

    public function countWFields(string $collection, array $where = [], array $fields = []): int;

    public function findOne(string $collection, array $where, ?array $fields = null): ?array;

    public function findOneById(string $collection, string $id, ?array $fields = null): ?array;

    public function update(string $collection, array $where, array $action): array;

    public function updateWithOptions(string $collection, array $where, array $action, array $options): array;

    /**
     * Inserts data into a collection.
     *
     * @param string $collection The name of the collection.
     * @param array $info The data to be inserted.
     * @return mixed
     */
    public function insert(string $collection, array $info);

    /**
     * Removes documents from a collection based on a given condition.
     *
     * @param string $collection The name of the collection to remove documents from.
     * @param array $where The condition to match documents for removal.
     * @return mixed
     */
    public function remove(string $collection, array $where);

    public function batchInsert(string $collection, array $rows): array;

    /**
     * Check if the given value is a valid MongoDB ObjectId.
     *
     * @param mixed $id The value to check.
     * @return bool True if the value is a valid MongoDB ObjectId, false otherwise.
     */
    public function isValidMongoId($id): bool;

    /**
     * Returns a MongoDB ObjectId based on the provided string ID.
     *
     * @param string|null $id The string ID to convert to ObjectId.
    * @return ObjectId The converted ObjectId.
     */
    public function MongoId(?string $id = null): ObjectId;

    /**
     * Converts a value to a MongoDB UTCDateTime object.
     *
     * @param mixed $value The value to convert.
     * @param bool $toBSONType Whether to convert the value to a BSON type.
     * @return UTCDateTime The converted UTCDateTime object.
     */
    public function MongoDate($value = null, bool $toBSONType = true): UTCDateTime;

    /**
     * Returns a MongoRegex object based on the given value.
     *
     * @param mixed $value The value to create the MongoRegex object from.
     * @return Regex The created MongoRegex object.
     */
    public function MongoRegex($value): Regex;

    public function getIdFromUpsertResult(array $result): string;

    public function findAndLimitAndIndex(string $collection, array $where = [], int $limit = 0, int $index = 0): array;

    public function findAndSortAndLimitAndIndex(string $collection, array $where = [], array $sortCriteria = [], int $limit = 0, int $index = 0): array;

    public function findAndFieldsAndSortAndLimitAndIndex(string $collection, array $where = [], array $fields = ["name", "collection"], array $sortCriteria = [], int $limit = 0, int $index = 30): array;

    public function aggregate(string $collection, array $aggregate = []): array;
}
