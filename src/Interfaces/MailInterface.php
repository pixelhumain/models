<?php

namespace PixelHumain\Models\Interfaces;

interface MailInterface
{
    public const COLLECTION = "mails";

    public const TPL_passwordRetreive = 'passwordRetreive';

    public const TPL_notifAdminNewUser = "notifAdminNewUser";

    public const TPL_notifAdminNewPro = "notifAdminNewPro";

    public const TPL_reservations = "reservations";

    public const TPL_relaunchInvitation = "relaunchInvitation";

    public const TPL_invitation = "invitation";

    public const TPL_inviteKKBB = "inviteKKBB";

    public const TPL_validation = "validation";

    public const TPL_validationWithNewPwd = "validationWithNewPwd";

    public const TPL_newEvent = "newEvent";

    public const TPL_newProject = "newProject";

    public const TPL_newOrganization = "newOrganization";

    public const TPL_helpAndDebugNews = "helpAndDebugNews";

    public const TPL_inviteYouTo = "inviteYouTo";

    public const TPL_follow = "follow";

    public const TPL_contactForm = "contactForm";

    public const TPL_confirmDeleteElement = "confirmDeleteElement";

    public const TPL_proposeInteropSource = "proposeInteropSource";

    public const TPL_validateInteropSource = "validateInteropSource";

    public const TPL_rejectInteropSource = "rejectInteropSource";

    public const TPL_update = "update";

    public const TPL_surveysubmissionSuccess = "survey.submissionSuccess";

    public const TPL_surveynewSubmission = "survey.newSubmission";

    public const TPL_askToBecome = "askToBecome";

    public const TPL_askToBecomeAdmin = "askToBecomeAdmin";

    public const TPL_notification = "notification";

    public const TPL_confirmYouTo = "confirmYouTo";

    public const TPL_bookmarkNotif = "bookmarkNotif";

    public const TPL_referenceEmailInElement = "referenceEmailInElement";

    /**
     * Crée et envoie un email.
     *
     * @param array $params Les paramètres de l'email.
     * @return void
     */
    public function createAndSend(array $params): void;

    /**
     * Crée et envoie un seul e-mail.
     *
     * @param array $params Les paramètres de l'e-mail.
     * @return void
     */
    public function createAndSendSingle(array $params): void;

    /**
     * Initializes the template parameters for the Mail class.
     *
     * @param array $params The array of parameters to initialize.
     * @return array The initialized array of parameters.
     */
    public function initTplParams(array $params): array;

    /**
     * Sends an email.
     *
     * @param array $params The parameters for the email.
     * @param bool $force (optional) Whether to force sending the email even if it's disabled.
     * @return bool|array Whether the email was sent or not.
     */
    public function send(array $params, bool $force = false);

    /**
     * Renders the template with the given parameters.
     *
     * @param array $params The parameters to be passed to the template.
     * @return string The rendered template.
     */
    public function renderTemplate(array $params): string;

    /**
     * Schedule a mail.
     *
     * @param array $params The parameters for the mail.
     * @param mixed $update The update parameter (optional).
     * @return void
     */
    public function schedule(array $params, $update = null);

    /**
     * Sends a notification email to the admin about a new user.
     *
     * @param array $person The details of the new user.
     * @return void
     */
    public function notifAdminNewUser(array $person): void;

    /**
        * References the email in a specified element.
        *
        * @param string $collection The name of the collection.
        * @param string $id The identifier of the element.
        * @param string $email The email to reference.
        * @return void
        */
    public function referenceEmailInElement(string $collection, string $id, string $email): void;

    /**
     * Sends a notification email to the admin about a new professional.
     *
     * @param array $person The details of the new professional.
     * @return void
     */
    public function notifAdminNewPro(array $person): void;

    /**
     * Sends a notification to the admin about a new reservation.
     *
     * @param array $params The parameters for the notification.
     * @return void
     */
    public function notifAdminNewReservation(array $params): void;

    /**
     * Invite a person via email.
     *
     * @param array $person The person to invite.
     * @param string|null $msg The invitation message (optional).
     * @param string|null $nameInvitor The name of the invitor (optional).
     * @param string|null $invitorUrl The URL of the invitor (optional).
     * @param string|null $subject The subject of the invitation email (optional).
     * @return void
     */
    public function invitePerson(array $person, string $msg = null, string $nameInvitor = null, string $invitorUrl = null, string $subject = null): void;

    /**
     * Relaunches an invitation to a person.
     *
     * @param array $person The person to invite.
     * @param string|null $nameInvitor The name of the invitor (optional).
     * @param string|null $invitorUrl The URL of the invitor (optional).
     * @param string|null $subject The subject of the invitation (optional).
     * @return void
     */
    public function relaunchInvitePerson(array $person, string $nameInvitor = null, string $invitorUrl = null, string $subject = null): void;

    /**
     * Invite a person to KKBB.
     *
     * @param array $person The person to invite.
     * @param mixed $isInvited Whether the person is invited or not.
     * @return void
     */
    public function inviteKKBB(array $person, $isInvited): void;

    /**
     * Sends a password retrieval email to the specified email address.
     *
     * @param string $email The email address of the user.
     * @param string $pwd The password to be retrieved.
     * @return void
     */
    public function passwordRetreive(string $email, string $pwd): void;

    /**
     * Validates a person.
     *
     * @param array $person The person to validate.
     * @return void
     */
    public function validatePerson(array $person): void;

    /**
     * Validates a person with a new password.
     *
     * @param array $person The person data.
     * @param string $pwd The new password.
     * @param string|null $formTitle The title of the form (optional).
     * @param string $redirectUrl The URL to redirect to after validation.
     * @return void
     */
    public function validatePersonWithNewPwd(array $person, string $pwd, string $formTitle = null, string $redirectUrl = ''): void;

    /**
     * Sends an email notification when someone demands to become an admin.
     *
     * @param mixed $parent The parent object.
     * @param mixed $parentType The type of the parent object.
     * @param mixed $newPendingAdmin The email address of the new pending admin.
     * @param array $listofAdminsEmail The list of email addresses of the current admins.
     * @param mixed $typeOfDemand The type of demand.
     * @return void
     */
    public function someoneDemandToBecome($parent, $parentType, $newPendingAdmin, array $listofAdminsEmail, $typeOfDemand): void;

    /**
     * Follows an element of a specific type by sending mail notifications.
     *
     * @param array $element The element to follow.
     * @param string $elementType The type of the element.
     * @param array|null $listOfMail The list of mail recipients (optional).
     * @return void
     */
    public function follow(array $element, string $elementType, array $listOfMail = null): void;

    /**
     * Sends an email from the contact form.
     *
     * @param string $emailSender The email address of the sender.
     * @param mixed $names The names of the sender.
     * @param string $subject The subject of the email.
     * @param string $contentMsg The content of the email.
     * @return void
     */
    public function sendMailFormContact(string $emailSender, string $names, string $subject, string $contentMsg): void;

    /**
     * Sends a private contact form email.
     *
     * @param string $emailSender The email address of the sender.
     * @param string $names The names of the sender.
     * @param string $subject The subject of the email.
     * @param string $contentMsg The content of the email.
     * @param string $emailReceiver The email address of the receiver.
     * @return void
     */
    public function sendMailFormContactPrivate(string $emailSender, string $names, string $subject, string $contentMsg, string $emailReceiver): void;

    /**
     * Sends a confirmation email for deleting an element.
     *
     * @param string $elementType The type of the element being deleted.
     * @param string $elementId The ID of the element being deleted.
     * @param string $reason The reason for deleting the element.
     * @param array $admins An array of admin email addresses.
     * @param string $userId The ID of the user initiating the deletion.
     * @return void
     */
    public function confirmDeleteElement(string $elementType, string $elementId, string $reason, array $admins, string $userId): void;

    /**
     * Propose an interop source.
     *
     * @param string $url_source The URL of the source.
     * @param mixed $admins The admins of the source.
     * @param string $userID The ID of the user.
     * @param string $description The description of the source.
     * @return void
     */
    public function proposeInteropSource(string $url_source, $admins, string $userID, string $description);

    /**
     * Validates the proposed interop.
     *
     * @param string $url_source The URL source.
     * @param string $userID The user ID.
     * @param string $adminID The admin ID.
     * @param string $description The description.
     * @return void
     */
    public function validateProposedInterop(string $url_source, string $userID, string $adminID, string $description): void;

    /**
     * Rejects a proposed interop.
     *
     * @param string $url_source The source URL.
     * @param string $userID The user ID.
     * @param string $adminID The admin ID.
     * @param string $description The rejection description.
     * @return void
     */
    public function rejectProposedInterop(string $url_source, string $userID, string $adminID, string $description): void;

    /**
     * Returns the name of the application.
     *
     * @return string The name of the application.
     */
    public function getAppName(): string;

    /**
     * Sends a confirmation email to the user after saving a survey.
     *
     * @param array $user The user information.
     * @param array $survey The survey information.
     * @return void
     */
    public function confirmSavingSurvey(array $user, array $survey): void;

    /**
     * Sends a new answer notification to the admin.
     *
     * @param string $email The email address of the admin.
     * @param array $user The user information.
     * @param array $survey The survey information.
     * @return void
     */
    public function sendNewAnswerToAdmin(string $email, array $user, array $survey): void;

    /**
     * Retrieves a custom mail.
     *
     * @param array $params The parameters for the custom mail.
     * @param string|null $from The sender of the custom mail. Defaults to null.
     * @return array The custom mail.
     */
    public function getCustomMail(array $params, string $from = null): array;

    /**
    * Requests to become something.
    *
    * @param array $construct The construction data.
    * @param array $val The values.
    * @return void
    */
    public function askToBecome(array $construct, array $val): void;

    /**
     * Sends an invitation email.
     *
     * @param array $construct The construction parameters.
     * @param array $val The values to be used in the email.
     * @return void
     */
    public function invitation(array $construct, array $val): void;

    /**
     * Invite someone to something.
     *
     * @param array $construct The construction parameters.
     * @param array $val The values to be used in the invitation.
     * @return void
     */
    public function inviteYouTo(array $construct, array $val): void;

    /**
     * Demande à devenir ami.
     *
     * @param array $construct Les informations de construction.
     * @param array $val Les valeurs.
     * @return void
     */
    public function askToBecomeFriend(array $construct, array $val): void;

    /**
     * Creates a notification.
     *
     * @param array $construct The notification data.
     * @param string|null $tpl The template to use for the notification.
     * @return void
     */
    public function createNotification(array $construct, string $tpl = null): void;

    /**
     * Sends a confirmation email to someone who confirmed you.
     *
     * @param array $parent The parent data.
     * @param string $parentType The type of the parent.
     * @param array $child The child data.
     * @param string $typeOfDemand The type of demand.
     * @return void
     */
    public function someoneConfirmYouTo(array $parent, string $parentType, array $child, string $typeOfDemand): void;

    /**
     * Generates the URL for a mail notification.
     *
     * @param string $type The type of the mail notification.
     * @param string $id The ID of the mail notification.
     * @param array|null $construct Additional parameters for constructing the URL.
     * @return string The generated URL for the mail notification.
     */
    public function urlMailNotif(string $type, string $id, array $construct = null): string;

    /**
     * Translates the label of a mail.
     *
     * @param array $mail The mail data.
     * @return string The translated label.
     */
    public function translateLabel(array $mail): string;

    /**
     * Sends a bookmark notification email.
     *
     * @param array $params The parameters for the email.
     * @param string $userID The ID of the user to send the email to.
     * @param array|null $mailParams Additional parameters for the email (optional).
     * @return void
     */
    public function bookmarkNotif(array $params, string $userID, array $mailParams = null): void;

    /**
     * Sends a feedback email.
     *
     * @param array $params The parameters for the email.
     * @return void
     */
    public function mailFeedBack(array $params): void;

    /**
        * Relaunches an invitation.
        *
        * @param string $id The identifier of the invitation.
        * @return void
        */
    public function relaunchInvitation(string $id): void;
}
