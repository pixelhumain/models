<?php

namespace PixelHumain\Models\Interfaces;

interface ResolutionInterface
{
    public const COLLECTION = "resolutions";

    public const CONTROLLER = "resolution";

    public const STATUS_AMENDABLE = "amendable";

    public const STATUS_TOVOTE = "tovote";

    public const STATUS_CLOSED = "closed";

    public const STATUS_ARCHIVED = "archived";
}
