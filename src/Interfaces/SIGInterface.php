<?php

namespace PixelHumain\Models\Interfaces;

interface SIGInterface
{
    /**
     * Retrieves the address schema that matches the given INSEE code.
     *
     * @param string $codeInsee The INSEE code.
     * @param string|null $postalCode The postal code (optional).
     * @param string|null $cityName The city name (optional).
     * @return array The array of address schemas.
     */
    public function getAdressSchemaLikeByCodeInsee($codeInsee, ?string $postalCode = null, ?string $cityName = null): array;

    /**
     * Retrieves the city information based on the INSEE code.
     *
     * @param string $codeInsee The INSEE code of the city.
     * @return array The city information.
     */
    public function getCityByCodeInsee(string $codeInsee): array;

    /**
     * Retrieves the geographical position by INSEE code.
     *
     * @param string $inseeCode The INSEE code of the city.
     * @param string|null $postalCode Optional postal code to filter the location.
     * @param string|null $cityName Optional city name to filter the location.
     * @return array The geographical coordinates.
     */
    public function getGeoPositionByInseeCode(string $inseeCode, ?string $postalCode = null, ?string $cityName = null): array;

    /**
     * Updates the geoposition of an entity.
     *
     * @param string $entityType The type of the entity.
     * @param string $entityId The ID of the entity.
     * @param string|null $latitude The latitude of the entity's geoposition.
     * @param string|null $longitude The longitude of the entity's geoposition.
     * @param mixed|null $addressIndex The index of the address.
     * @return void
     */
    public function updateEntityGeoposition(string $entityType, string $entityId, ?string $latitude, ?string $longitude, $addressIndex = null): void;
}
