<?php

namespace PixelHumain\Models\Interfaces;

interface CircuitInterface
{
    public const COLLECTION = "circuits";

    public const CONTROLLER = "circuit";

    /**
     * Retrieves a circuit by its ID.
     *
     * @param string $id The ID of the circuit.
     * @return array|null The circuit data as an array or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Get a list of circuits based on the given conditions.
     *
     * @param array $where The conditions to filter the circuits.
     * @return array The list of circuits that match the conditions.
     */
    public function getListBy(array $where): array;

    /**
     * Insère un circuit dans la base de données.
     *
     * @param array $circuit Les données du circuit à insérer.
     * @param string $userId L'identifiant de l'utilisateur.
     * @return array Les données du circuit inséré.
     */
    public function insert(array $circuit, string $userId): array;

    // /**
    //  * Get a list of circuits by user.
    //  *
    //  * @param array $where The conditions to filter the circuits.
    //  * @return array The list of circuits.
    //  */
    // public function getListByUser(array $where): array;

    // /**
    //  * Retrieves an order item by its ID.
    //  *
    //  * @param string $id The ID of the order item.
    //  * @return array The order item data.
    //  */
    // public function getOrderItemById(string $id): array;
}
