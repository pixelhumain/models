<?php

namespace PixelHumain\Models\Interfaces;

interface PoiInterface
{
    public const COLLECTION = "poi";

    public const CONTROLLER = "poi";

    public const MODULE = "poi";

    public const ICON = "fa-map-marker";

    /**
     * Retrieves a list of POIs based on the specified conditions, sorted and limited.
     *
     * @param array $where An array of conditions to filter the POIs.
     * @param array $orderBy An array specifying the sorting order of the POIs. The keys represent the fields to sort by, and the values represent the sort order (1 for ascending, -1 for descending).
     * @param int $indexStep The number of POIs to retrieve per page.
     * @param int $indexMin The starting index of the first POI to retrieve.
     * @return array An array containing the retrieved POIs.
     */
    public function getPoiByWhereSortAndLimit(array $where = [], array $orderBy = [
        'updated' => -1,
    ], int $indexStep = 10, int $indexMin = 0): array;

    /**
     * Retrieves a Point of Interest (POI) by its structure.
     *
     * @param array $l The array containing the POIs.
     * @param string $val The value to search for.
     * @param string $field The field to search in (default: "tags").
     * @return array The array of matching POIs.
     */
    public function getPoiByStruct(array $l, string $val, string $field = 'tags'): array;

    /**
     * Retrieves a Point of Interest by its ID.
     *
     * @param string $id The ID of the Point of Interest.
     * @return array|null The Point of Interest data as an array, or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Deletes a Poi.
     *
     * @param string $id The ID of the Poi to delete.
     * @param string|null $userId The ID of the user performing the deletion (optional).
     * @return array The result of the deletion operation.
     */
    public function delete(string $id, ?string $userId): array;

    /**
     * Determines if a user can delete a point of interest.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the point of interest.
     * @param array|null $poi The point of interest data (optional).
     * @return bool Returns true if the user can delete the point of interest, false otherwise.
     */
    public function canDeletePoi(string $userId, string $id, ?array $poi = null): bool;

    /**
     * Returns the data binding for the Poi class.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array;

    /**
     * Returns the hierarchy of a list of points of interest based on a specified field.
     *
     * @param array $pList The list of points of interest.
     * @param string $field The field used to determine the hierarchy (default: "tags").
     * @return array The hierarchy of the points of interest.
     */
    public function hierarchy(array $pList, string $field = 'tags'): array;
}
