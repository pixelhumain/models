<?php

namespace PixelHumain\Models\Interfaces;

interface ActivityStreamReferenceInterface
{
    public const COLLECTION = "activityStreamReference";

    public const NOTIFICATION_STATES = ["isUnseen", "isUnread"];

    /**
     * Adds an activity stream reference.
     *
     * @param array|null $activityStream The activity stream reference to add.
     * @return void
     */
    public function add(?array $activityStream): void;

    /**
     * Update the state of the activity stream reference by user.
     *
     * @param string $stateName The name of the state.
     * @param mixed $stateValue The value of the state.
     * @return void
     */
    public function updateStateByUser(string $stateName, $stateValue): void;

    /**
     * Updates the state of the activity stream reference by notification ID.
     *
     * @param string $notificationId The ID of the notification.
     * @param string $stateName The name of the state to update.
     * @param mixed $stateValue The new value of the state.
     * @return void
     */
    public function updateStateByNotificationId(string $notificationId, string $stateName, $stateValue): void;

    /**
     * Removes activity stream references by user ID.
     *
     * @param string $userId The ID of the user.
     * @return void
     */
    public function removeByUserId(string $userId): void;

    /**
     * Removes an activity stream reference by notification ID.
     *
     * @param string $notificationId The ID of the notification.
     * @return void
     */
    public function removeByNotificationId(string $notificationId): void;

    /**
     * Initializes the state of the activity stream reference.
     *
     * @param string $notificationId The ID of the notification.
     * @return void
     */
    public function initializeState(string $notificationId): void;

    /**
     * Retrieves a notification by constructing an array of parameters.
     *
     * @param array $construct The array of parameters used to construct the notification.
     * @return array|null The notification matching the given parameters, or null if not found.
     */
    public function getNotificationByConstruct(array $construct): ?array;

    /**
     * Retrieves notifications based on a time limit.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting criteria for the query. Default is ["updated" => -1].
     * @return array The array of notifications.
     */
    public function getNotificationsByTimeLimit(array $param, array $sort = [
        "updated" => -1,
    ]): array;

    /**
     * Retrieves notifications based on the given parameters.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotificationsByStep(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array;

    /**
     * Counts the number of unseen notifications for a given user, element type, and element ID.
     *
     * @param string $userId The ID of the user.
     * @param string|null $elementType The type of the element (optional).
     * @param string|null $elementId The ID of the element (optional).
     * @return int The number of unseen notifications.
     */
    public function countUnseenNotifications(string $userId, ?string $elementType, ?string $elementId);
}
