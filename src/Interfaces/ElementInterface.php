<?php

namespace PixelHumain\Models\Interfaces;

interface ElementInterface
{
    public const NB_DAY_BEFORE_DELETE = 5;

    public const STATUS_DELETE_PEDING = "deletePending";

    public const ERROR_DELETING = "errorTryingToDelete";

    /**
     * Retrieves a simple element by its ID from a specified collection.
     *
     * @param string $id The ID of the element to retrieve.
     * @param string $collection The name of the collection to search in.
     * @param array|null $where An optional array of conditions to filter the search.
     * @param array|null $fields An optional array of fields to include in the result.
     * @return array|null The retrieved element as an associative array, or null if not found.
     */
    public function getElementSimpleById(string $id, string $collection, ?array $where = null, ?array $fields = null): ?array;

    /**
     * Retrieves list of simple elements by their IDs from a specified collection.
     *
     * @param array $ids The IDs of the element.
     * @param string $collection The collection name.
     * @param array|null $where Optional. Additional conditions to filter the elements.
     * @param array|null $fields Optional. The fields to include in the result.
     * @return array The list of elements as an associative array.
     */
    public function getElementSimpleByIds(array $ids, string $collection, ?array $where = null, ?array $fields = null): array;

    /**
     * Retrieves an element by its ID from a specified collection.
     *
     * @param string|null $id The ID of the element to retrieve.
     * @param string $collection The name of the collection to search in.
     * @param array|null $where An optional array of conditions to filter the search.
     * @param array|null $fields An optional array of fields to include in the result.
     * @return array|null The retrieved element, or null if not found.
     */
    public function getElementById(?string $id, string $collection, ?array $where = [], ?array $fields = null): ?array;

    /**
     * Retrieves the controller based on the collection type.
     *
     * @param string|null $type The collection type.
     * @return mixed The controller associated with the collection type.
     */
    public function getControlerByCollection(?string $type = null);

    /**
     * Retrieves the collection by controller type.
     *
     * @param string $type The controller type.
     * @return string|null The collection corresponding to the controller type, or null if not found.
     */
    public function getCollectionByControler(string $type): ?string;

    /**
     * Retrieves the Font Awesome icon for the specified type.
     *
     * @param string $type The type of the element.
     * @return string|bool The Font Awesome icon, or false if not found.
     */
    public function getFaIcon(string $type);

    /**
     * Get the color icon for the specified type.
     *
     * @param string $type The type of the element.
     * @return string|bool The color icon for the specified type.
     */
    public function getColorIcon(string $type);

    /**
     * Retrieves the element specifications based on the given type.
     *
     * @param string $type The type of the element.
     * @return array|false The specifications of the element or false if not found.
     */
    public function getElementSpecsByType(string $type);

    /**
     * Check the ID and type of an element.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @param string|null $actionType The action type of the element (optional).
     * @return array An array containing the result of the check.
     */
    public function checkIdAndType(string $id, string $type, ?string $actionType = null): array;

    /**
     * Returns the link for the specified type and ID.
     *
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @param mixed $hashOnly (optional) If set to true, only the hash part of the link will be returned.
     * @return string The link for the specified type and ID.
     */
    public function getLink(string $type, string $id, $hashOnly = null): string;

    /**
     * Retrieves an element by its type and ID.
     *
     * @param string $type The type of the element.
     * @param array|string $id The ID of the element.
     * @param array|null $what The optional array of fields to retrieve.
     * @param mixed $update The optional update parameter.
     * @return array The retrieved element.
     */
    public function getByTypeAndId(string $type, $id, ?array $what = null, $update = null): array;

    /**
     * Get a simple element by type and ID.
     *
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @param array|null $what The optional fields to retrieve for the element.
     * @return array The simple element.
     */
    public function getSimpleByTypeAndId(string $type, string $id, ?array $what = null): array;

    /**
     * Retrieves the ghost element of the specified type.
     *
     * @param string $type The type of the ghost element.
     * @return array The ghost element.
     */
    public function getGhost(string $type): array;

    /**
     * Retrieves an element by its ID and the type of its parent.
     *
     * @param string $collection The name of the collection.
     * @param string $id The ID of the element.
     * @param string $type The type of the parent.
     * @param array $orderBy The order in which the elements should be sorted.
     * @param array|null $where An optional array of conditions to filter the elements.
     *
     * @return array The retrieved element(s).
     */
    public function getByIdAndTypeOfParent(string $collection, string $id, string $type, array $orderBy, ?array $where = null): array;

    /**
     * Get the information for a specific element.
     *
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @param mixed $loadByHashOnly Optional parameter to load the element by hash only.
     * @return array The information of the element.
     */
    public function getInfos(string $type, string $id, $loadByHashOnly = null): array;

    /**
     * Updates the value of a specific path in the element.
     *
     * @param string $collection The collection name.
     * @param string $id The element ID.
     * @param string $path The path to update.
     * @param mixed $value The new value.
     * @param mixed|null $arrayForm The array form.
     * @param mixed|null $edit The edit value.
     * @param mixed|null $pull The pull value.
     * @param string|null $formParentId The parent ID of the form.
     * @param bool|null $updatePartial Whether to update partially or not.
     * @param mixed|null $tplGenerator The template generator.
     * @param mixed|null $renameKey The key to rename.
     * @return array The updated element.
     */
    public function updatePathValue(string $collection, string $id, string $path, $value, $arrayForm = null, $edit = null, $pull = null, ?string $formParentId = null, ?bool $updatePartial = null, $tplGenerator = null, $renameKey = null): array;

    /**
     * Update a field in the specified collection and element.
     *
     * @param string $collection The name of the collection.
     * @param string $id The ID of the element.
     * @param string $fieldName The name of the field to update.
     * @param mixed $fieldValue The new value for the field.
     * @param bool|null $allDay (optional) Whether the field represents an all-day event.
     *
     * @return array The updated element.
     */
    public function updateField(string $collection, string $id, string $fieldName, $fieldValue, ?bool $allDay = null): array;

    /**
     * Returns the profile image URL for a person.
     *
     * @param array|null $person The person's data.
     * @param string $imgName The name of the image.
     * @param string $assetUrl The base URL for the assets.
     * @return string The profile image URL.
     */
    public function getImgProfil(?array $person, string $imgName, string $assetUrl): string;

    /**
     * Retrieves all links of a specific type and ID.
     *
     * @param array $links An array of links.
     * @param string $type The type of link to retrieve.
     * @param string $id The ID of the link to retrieve.
     * @return array An array containing all the links of the specified type and ID.
     */
    public function getAllLinks(array $links, string $type, string $id): array;

    /**
     * Get the active elements of a specific type.
     *
     * @param string $type The type of elements to retrieve.
     * @return array The array of active elements.
     */
    public function getActive(string $type): array;

    /**
     * Update the time element by label.
     *
     * @param string $elementType The type of the element.
     * @param string $elementId The ID of the element.
     * @param string $label The label of the element.
     * @return true Returns true if the update was successful, false otherwise.
     */
    public function updateTimeElementByLabel(string $elementType, string $elementId, string $label): bool;

    /**
     * Retrieves the community by parent type and ID.
     *
     * @param array $parent The parent type and ID.
     * @return array The community data.
     */
    public function getCommunityByParentTypeAndId(array $parent): array;

    /**
     * Retrieves the community based on the given type and ID.
     *
     * @param string $type The type of the community.
     * @param string $id The ID of the community.
     * @param string $typeCommunity The type of the community to retrieve. Default is "all".
     * @param string|null $attribute The attribute of the community to filter by. Default is null.
     * @param string|null $role The role of the community to filter by. Default is null.
     * @param array|null $settings Additional settings for the community retrieval. Default is null.
     * @param array|null $fields The fields to include in the retrieved community. Default is null.
     * @return array The retrieved community.
     */
    public function getCommunityByTypeAndId(string $type, string $id, string $typeCommunity = 'all', ?string $attribute = null, ?string $role = null, ?array $settings = null, ?array $fields = null): array;

    /**
     * Demande la suppression d'un élément.
     *
     * @param string $elementType Le type de l'élément.
     * @param string $elementId L'identifiant de l'élément.
     * @param string $reason La raison de la demande de suppression.
     * @param string $userId L'identifiant de l'utilisateur effectuant la demande.
     * @param array|null $elemTypes Les types d'éléments associés à la demande de suppression.
     * @return array Les informations de la demande de suppression.
     */
    public function askToDelete(string $elementType, string $elementId, string $reason, string $userId, ?array $elemTypes): array;

    /**
     * Deletes an element.
     *
     * @param string $elementType The type of the element.
     * @param string $elementId The ID of the element.
     * @param string $reason The reason for deleting the element.
     * @param string $userId The ID of the user performing the deletion.
     * @param array|null $elt The element data (optional).
     * @return array The response after deleting the element.
     */
    public function deleteElement(string $elementType, string $elementId, string $reason, string $userId, ?array $elt = null): array;

    /**
     * Deletes a simple element.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @param string|null $userId The ID of the user performing the deletion (optional).
     * @return array The result of the deletion operation.
     */
    public function deleteSimple(string $id, string $type, ?string $userId): array;

    /**
     * Stops the deletion of an element.
     *
     * @param string $elementType The type of the element.
     * @param string $elementId The ID of the element.
     * @param string|null $userId The ID of the user performing the action.
     * @return array The result of the operation.
     */
    public function stopToDelete(string $elementType, string $elementId, ?string $userId): array;

    /**
     * Checks if the element status is pending for deletion.
     *
     * @param string $elementType The type of the element.
     * @param string $elementId   The ID of the element.
     *
     * @return bool Returns true if the element status is pending for deletion, false otherwise.
     */
    public function isElementStatusDeletePending(string $elementType, string $elementId): bool;

    /**
     * Saves the element with the given parameters.
     *
     * @param array $params The parameters for saving the element.
     * @return array The saved element.
     */
    public function save(array $params): array;

    /**
     * Executes after saving an element.
     *
     * @param string $id The ID of the element.
     * @param string $collection The collection name.
     * @param array $params The parameters.
     * @param array|null $postParams The post parameters.
     * @param mixed|null $paramsImport The imported parameters.
     * @return array The updated array of parameters.
     */
    public function afterSave(string $id, string $collection, array $params, ?array $postParams, $paramsImport = null): array;

    /**
     * Preps the data by applying the given parameters.
     *
     * @param array $params The parameters to apply.
     * @return array The prepped data.
     */
    public function prepData(array $params): array;

    /**
     * Check if an element already exists in a collection.
     *
     * @param array $params The parameters to check.
     * @param string $collection The name of the collection to check in.
     * @return array The existing elements that match the given parameters.
     */
    public function alreadyExists(array $params, string $collection): array;

    /**
     * Retrieves an element by its name from a specified collection.
     *
     * @param string $name The name of the element to retrieve.
     * @param string $collection The name of the collection to search in.
     * @param string $source The source of the element (optional).
     * @return array The retrieved element as an array.
     */
    public function getElementByName(string $name, string $collection, string $source = ''): array;

    /**
     * Retrieves an element from a collection based on the specified conditions.
     *
     * @param string $collection The name of the collection to search in.
     * @param array $where The conditions to match the element against.
     * @param array|null $fields The fields to include in the returned element. If null, all fields are included.
     * @return array The matching element(s) from the collection.
     */
    public function getElementByWhere(string $collection, array $where, ?array $fields = null): array;

    /**
     * Follows a person.
     *
     * @param array $params The parameters for following a person.
     * @param string|null $gmail The Gmail of the person to follow (optional).
     * @return array The response after following the person.
     */
    public function followPerson(array $params, ?string $gmail = null): array;

    /**
     * Follows a person by a list of emails.
     *
     * @param array $listMails The list of emails of the person to follow.
     * @param string|null $msgEmail The optional message to include in the email.
     * @param string|null $gmail The optional Gmail account to use for sending the email.
     * @return array The result of the operation.
     */
    public function followPersonByListMails(array $listMails, ?string $msgEmail = null, ?string $gmail = null): array;

    /**
     * Saves a chart.
     *
     * @param string $type The type of the chart.
     * @param string $id The ID of the chart.
     * @param mixed $properties The properties of the chart.
     * @param string $label The label of the chart.
     * @return true Returns true if the chart is saved successfully, false otherwise.
     */
    public function saveChart(string $type, string $id, $properties, string $label): bool;

    /**
     * Removes a chart from the element.
     *
     * @param string $type The type of the chart.
     * @param string $id The ID of the chart.
     * @param string $label The label of the chart.
     * @return true Returns true if the chart was successfully removed, false otherwise.
     */
    public function removeChart(string $type, string $id, string $label): bool;

    /**
     * Saves a contact.
     *
     * @param array $params The parameters for saving the contact.
     * @return array The saved contact.
     */
    public function saveContact(array $params): array;

    /**
     * Saves the URL of the element.
     *
     * @param array $params The parameters for saving the URL.
     * @return array The updated array of parameters.
     */
    public function saveUrl(array $params): array;

    /**
     * Retrieves and checks the URL.
     *
     * @param string $url The URL to retrieve and check.
     * @return string The retrieved and checked URL.
     */
    public function getAndCheckUrl(string $url): string;

    /**
     * Get the URLs for a specific element.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @return array An array of URLs.
     */
    public function getUrls(string $id, string $type): array;

    /**
     * Get the curriculum for a specific ID and type.
     *
     * @param string $id The ID of the curriculum.
     * @param string $type The type of the curriculum.
     * @return array The curriculum data.
     */
    public function getCuriculum(string $id, string $type): array;

    /**
     * Retrieves the contacts for a given ID and type.
     *
     * @param string $id The ID of the element.
     * @param string $type The type of the element.
     * @return array An array containing the contacts.
     */
    public function getContacts(string $id, string $type): array;

    /**
     * Retrieves contacts based on a list of emails.
     *
     * @param array $listMails An array of email addresses.
     * @return array An array of contacts.
     */
    public function getContactsByMails(array $listMails): array;

    /**
     * Update a block.
     *
     * @param array $params The parameters for the update.
     * @return array The updated block.
     */
    public function updateBlock(array $params): array;

    /**
     * Get the detailed information of an element.
     *
     * @param array $params The parameters for the request.
     * @param array $element The element data.
     * @param string $type The type of the element.
     * @param string $id The ID of the element.
     * @return array The detailed information of the element.
     */
    public function getInfoDetail(array $params, array $element, string $type, string $id): array;

    /**
     * Returns the element for JavaScript.
     *
     * @param array $element The element data.
     * @param string|null $type The element type.
     * @return array The element data for JavaScript.
     */
    public function getElementForJS(array $element, ?string $type = null): array;

    /**
     * Retrieves the network associated with the specified ID and type.
     *
     * @param string $id The ID of the network.
     * @param string $type The type of the network.
     * @return array The network information.
     */
    public function myNetwork(string $id, string $type): array;

    /**
     * Returns the hash of the given element.
     *
     * @param string $element The element to calculate the hash for.
     * @return string The hash of the element.
     */
    public function getHash(string $element): string;

    /**
     * Get data by ask.
     *
     * @param string|null $id    The ID.
     * @param string|null $email The email.
     *
     * @return array The data.
     */
    public function getDataByAsk(?string $id = null, ?string $email = null): array;

    /**
     * Retrieves the sub-event of an element.
     *
     * @param array $element The element to retrieve the sub-event from.
     * @param int $sub The index of the sub-event to retrieve.
     * @param int $maxSub The maximum number of sub-events.
     * @param mixed $context The context of the sub-event.
     * @param array $arrayIdEvents An optional array of event IDs.
     * @return array The sub-event as an array.
     */
    public function getSubEvent(array $element, int $sub, int $maxSub, $context, array $arrayIdEvents = []): array;

    /**
     * Récupère les derniers événements d'un élément.
     *
     * @param string $col Le nom de la collection.
     * @param string $id L'identifiant de l'élément.
     * @param int $nbEvent Le nombre d'événements à récupérer.
     * @param string $startDateUTC La date de début des événements au format UTC.
     * @param array|null $tags Les tags des événements (optionnel).
     * @return array Les derniers événements de l'élément.
     */
    public function getLastEvents(string $col, string $id, int $nbEvent, string $startDateUTC, ?array $tags): array;

    /**
     * Check if there are any duplicates in a collection.
     *
     * @param string $collection The name of the collection to check.
     * @param string $name (optional) The name of the element to check for duplicates.
     * @param string $source (optional) The source of the element to check for duplicates.
     * @return array An array containing the duplicates found in the collection.
     */
    public function checkDoublonInCollection(string $collection, string $name = '', string $source = ''): array;

    /**
     * Check for all duplicates in the element.
     *
     * @param string $name The name to check for duplicates.
     * @param string $source The source to check for duplicates.
     * @return array An array containing the duplicates found.
     */
    public function checkAllDoublon(string $name = '', string $source = ''): array;

    /**
     * Returns an array of all elements that have duplicates.
     *
     * @return array An array of elements.
     */
    public function getAllElementToDoublons(): array;

    /**
     * Merge the given arrays into a new array.
     *
     * @param string $elementType The type of element.
     * @param array $arrayToMerge The array to merge.
     * @param array $arrayToDelete The array to delete.
     * @return array The merged array.
     */
    public function merge(string $elementType, array $arrayToMerge, array $arrayToDelete): array;
}
