<?php

namespace PixelHumain\Models\Interfaces;

interface ProposalInterface
{
    public const COLLECTION = "proposals";

    public const CONTROLLER = "proposal";
}
