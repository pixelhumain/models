<?php

namespace PixelHumain\Models\Interfaces;

/**
 * Interface CacheHelperInterface
 *
 * This interface defines the methods for a cache helper class.
 */
interface CacheHelperInterface
{
    /**
     * Get the value associated with the given key from the cache.
     *
     * @param string $key The key to retrieve the value for.
     * @return mixed The value associated with the key, or null if not found.
     */
    public function get(string $key);

    /**
     * Set the value associated with the given key in the cache.
     *
     * @param string $id The identifier for the value.
     * @param mixed $value The value to be stored in the cache.
     * @param int $expire The expiration time for the value in seconds. Default is 0 (no expiration).
     * @return bool
     */
    public function set(string $id, $value, int $expire = 0): bool;

    /**
     * Set the value associated with the given key in the cache with specified tags.
     *
     * @param string $id The identifier for the value.
     * @param mixed $value The value to be stored in the cache.
     * @param array $tags The tags to associate with the value.
     * @return bool True if the value was set, false otherwise.
     */
    public function setTags(string $id, $value, array $tags): bool;

    /**
     * Invalidate all cache entries associated with the given tags.
     *
     * @param array|string $tags The tags to invalidate.
     * @return void
     */
    public function invalidateTags($tags);

    /**
     * Delete the value associated with the given key from the cache.
     *
     * @param string $id The identifier for the value to delete.
     * @return bool True if the value was deleted, false if the value was not found or could not be deleted.
     */
    public function delete(string $id): bool;

    /**
     * Flush all cache entries.
     *
     * @return bool True if the cache entries were flushed, false otherwise.
     */
    public function flush(): bool;
}
