<?php

namespace PixelHumain\Models\Interfaces;

interface SmartDataInterface
{
    public const SCALAIR_COLLECTION = "scalair";

    /**
     * Retrieves scalar data from the SmartData object.
     *
     * @param int $indexStep The step size for retrieving the data. Default is 10.
     * @param int $indexMin The minimum index value for retrieving the data. Default is 0.
     * @return array An array containing the scalar data.
     */
    public function getScalairData(int $indexStep = 10, int $indexMin = 0): array;
}
