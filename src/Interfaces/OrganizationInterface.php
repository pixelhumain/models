<?php

namespace PixelHumain\Models\Interfaces;

interface OrganizationInterface
{
    public const COLLECTION = "organizations";

    public const CONTROLLER = "organization";

    public const ICON = "fa-users";

    public const ICON_BIZ = "fa-industry";

    public const ICON_GROUP = "fa-circle-o";

    public const ICON_GOV = "fa-university";

    public const COLOR = "#93C020";

    public const TYPE_NGO = "NGO";

    public const TYPE_BUSINESS = "LocalBusiness";

    public const TYPE_GROUP = "Group";

    public const TYPE_GOV = "GovernmentOrganization";

    public const TYPE_COOP = "Cooperative";

    /**
     * Retrieves organizations based on the specified parameters.
     *
     * @param array $params The parameters used to filter the organizations.
     * @return array The array of organizations matching the specified parameters.
     */
    public function getWhere(array $params): array;

    /**
     * Returns the data binding for the Organization model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array;

    /**
     * Translates the given type into a string representation.
     *
     * @param string $type The type to be translated.
     * @return string The translated type as a string.
     */
    public function translateType(string $type): string;

    /**
     * Executes after saving an organization.
     *
     * @param array $organization The organization data.
     * @param string $creatorId The ID of the creator.
     * @param array|null $paramsImport Additional import parameters (optional).
     * @return array The modified organization data.
     */
    public function afterSave(array $organization, string $creatorId, ?array $paramsImport = null): array;

    /**
     * Retrieves an organization by its ID.
     *
     * @param string $id The ID of the organization.
     * @return array The organization data.
     */
    public function getById(string $id): array;

    /**
     * Retrieves an organization by an array of IDs.
     *
     * @param array $arrayId The array of organization IDs.
     * @param array $fields The optional array of fields to retrieve.
     * @param bool $simply Whether to simplify the result or not.
     * @return array The retrieved organization(s).
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array;

    /**
     * Retrieves a simple organization by its ID.
     *
     * @param string $id The ID of the organization.
     * @param array|null $orga Optional organization data.
     * @return array The simple organization data.
     */
    public function getSimpleOrganizationById(string $id, ?array $orga = null): array;
}
