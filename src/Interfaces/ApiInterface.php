<?php

namespace PixelHumain\Models\Interfaces;

interface ApiInterface
{
    /**
     * Retrieves a new format link.
     *
     * @param array $link The link to be formatted.
     * @return array The formatted link.
     */
    public function getNewFormatLink(array $link): array;

    /**
     * Retrieves the URL of an image based on the provided data and type.
     *
     * @param array $data The data used to retrieve the image URL.
     * @param string $type The type of the image.
     * @return array The URL of the image.
     */
    public function getUrlImage(array $data, string $type): array;

    /**
     * Retrieves data based on search criteria.
     *
     * @param string $search The search term.
     * @param string|null $type The type of data to retrieve.
     * @param string|null $tags The tags associated with the data.
     * @param int|null $index The starting index of the data.
     * @param int|null $limit The maximum number of data to retrieve.
     * @return array The retrieved data.
     */
    public function getDataBySearch(string $search, ?string $type, ?string $tags, ?int $index, ?int $limit): array;

    /**
    * Returns a string with accent to REGEX expression to find any combinations
    * in accent insentive way
    *
    * @param string $text The text.
    * @return string The REGEX text.
    */
    public function accentToRegex($text);

    /**
     * Makes a rating based on the given rate and best value.
     *
     * @param mixed $rate The rate value.
     * @param int $bestvalue The best value for rating (default is 5).
     * @return string The rating string.
     */
    public function makeRating($rate, int $bestvalue = 5): string;

    /**
     * Check if a given value is a valid MongoDB ObjectId.
     *
     * @param mixed $id The value to check.
     * @return bool True if the value is a valid MongoDB ObjectId, false otherwise.
     */
    public function isValidMongoId($id);

    /**
     * Checks if a string starts with a specific substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle The substring to search for.
     * @param bool $case (optional) Whether to perform a case-sensitive search. Default is true.
     * @return int|bool True if the string starts with the specified substring, false otherwise.
     */
    public function stringStartsWith(string $haystack, string $needle, bool $case = true);

    /**
     * Checks if a string ends with a specific substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle The substring to search for.
     * @param bool $case (optional) Whether to perform a case-sensitive search. Default is true.
     * @return int|bool True if the string ends with the specified substring, false otherwise.
     */
    public function stringEndsWith(string $haystack, string $needle, bool $case = true);

    /**
     * Check if something is an associative array
     *
     * Note: this method only checks the key of the first value in the array.
     *
     * @param mixed $param The variable to check
     * @return bool true if the variable is an associative array
     */
    public function isAssociativeArray($param);

    /**
     * Check the status of a date within a given range.
     *
     * @param string $targetDate The date to check the status of.
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return string The status of the target date within the range.
     */
    public function checkDateStatus(string $targetDate, string $startDate, string $endDate): string;

    /**
     * Checks if a value is a valid timestamp.
     *
     * @param mixed $value The value to check.
     * @return bool Returns true if the value is a valid timestamp, false otherwise.
     */
    public function is_timestamp($value): bool;

    /**
     * Converts a timestamp to the format "d/m/Y H:i".
     *
     * @param string $default The default format to use if the timestamp is empty.
     * @param string $timestamp The timestamp to convert.
     * @return string The converted timestamp in the format "d/m/Y H:i".
     */
    public function convert_to_dd_mm_YYYY_H_i(string $default = 'd/m/Y H:i', string $timestamp): string;

    /**
     * Converts a timestamp to a formatted date and time string.
     *
     * @param string $target The desired format for the date and time string. Default is 'd/m/Y H:i'.
     * @param int $timestamp The timestamp to convert.
     * @return string The formatted date and time string.
     */
    public function convert_timestamp_to_dd_mm_YYYY_H_i(string $target = 'd/m/Y H:i', int $timestamp): string;

    /**
     * Retrieves the value from an array based on the given path.
     *
     * @param array $array The array to search in.
     * @param string $path The path to the desired value, using dot notation.
     * @return array|null The value found at the given path, or null if not found.
     */
    public function getArrayValueByPath(array $array, string $path): ?array;

    /**
     * Get the value from an array using a path.
     *
     * This method retrieves the value from an array using a path. The path is a string that represents the keys
     * separated by a delimiter (default is dot). If the path is not found in the array, null is returned.
     *
     * @param array &$array The array to search in.
     * @param string|array $path The path to the value.
     * @param string $glue The delimiter used in the path (default is dot).
     * @return array|null The value found in the array or null if not found.
     */
    public function path_get_value(array &$array, $path, string $glue = '.'): ?array;
}
