<?php

namespace PixelHumain\Models\Interfaces;

interface OrderItemInterface
{
    public const COLLECTION = "orderItems";

    public const CONTROLLER = "orderItems";

    /**
     * Retrieves an order item by its ID.
     *
     * @param string $id The ID of the order item.
     * @return array|null The order item data as an array, or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Retrieves order items by array of IDs.
     *
     * @param array $arrayId The array of IDs to retrieve order items for.
     * @param array $fields  Optional. The fields to include in the retrieved order items. Default is an empty array.
     *
     * @return array The retrieved order items.
     */
    public function getByArrayId(array $arrayId, array $fields = []): array;

    /**
     * Get a list of OrderItems based on the given conditions.
     *
     * @param array $where The conditions to filter the OrderItems.
     * @return array The list of OrderItems that match the conditions.
     */
    public function getListBy(array $where): array;

    /**
     * Get a list of order items by user.
     *
     * @param array $where The conditions to filter the order items.
     * @return array The list of order items.
     */
    public function getListByUser(array $where): array;

    /**
     * Inserts a new order item into the database.
     *
     * @param string $orderedItemId The ID of the ordered item.
     * @param array $orderedItemData The data of the ordered item.
     * @param string $userId The ID of the user.
     * @return array The inserted order item data.
     */
    public function insert(string $orderedItemId, array $orderedItemData, string $userId): array;

    /**
     * Action to rate an order item.
     *
     * @param array $params The parameters for rating.
     * @param string $commentId The ID of the comment to be rated.
     * @return void
     */
    public function actionRating(array $params, string $commentId): void;
}
