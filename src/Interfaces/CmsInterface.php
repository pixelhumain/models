<?php

namespace PixelHumain\Models\Interfaces;

interface CmsInterface
{
    public const COLLECTION = "cms";

    public const CONTROLLER = "cms";

    public const MODULE = "cms";

    public const ICON = "fa-file";
}
