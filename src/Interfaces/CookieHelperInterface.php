<?php

namespace PixelHumain\Models\Interfaces;

interface CookieHelperInterface
{
    public function hasCookie($name);

    public function getCookie($name);

    public function setCookie($name, $value);

    public function removeCookie($name);
}
