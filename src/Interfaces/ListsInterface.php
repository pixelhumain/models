<?php

namespace PixelHumain\Models\Interfaces;

interface ListsInterface
{
    public const COLLECTION = "lists";

    /**
     * Ajoute une nouvelle entrée à la liste spécifiée.
     *
     * @param string $listName Le nom de la liste.
     * @param string $entryKey La clé de l'entrée.
     * @param array $extra Les données supplémentaires de l'entrée (facultatif).
     * @return void
     */
    public function newEntry(string $listName, string $entryKey, array $extra = []): void;

    /**
     * Retrieves the specified lists.
     *
     * @param array $listNames The names of the lists to retrieve.
     * @return array The retrieved lists.
     */
    public function get(array $listNames): array;

    /**
     * Retrieves a list by its name.
     *
     * @param string $name The name of the list.
     * @return array The list matching the given name.
     */
    public function getListByName(string $name): array;

    /**
     * Retrieves the labels for a given list name and IDs.
     *
     * @param array $listName The name of the list.
     * @param array $ids The IDs of the items.
     * @return array The labels corresponding to the given list name and IDs.
     */
    public function getLabels(array $listName, array $ids): array;
}
