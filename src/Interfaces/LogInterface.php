<?php

namespace PixelHumain\Models\Interfaces;

interface LogInterface
{
    public const COLLECTION = "logs";

    /**
     * Saves a log entry.
     *
     * @param array $log The log entry to be saved.
     * @return void
     */
    public function save(array $log): void;
}
