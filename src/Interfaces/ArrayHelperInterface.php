<?php

namespace PixelHumain\Models\Interfaces;

interface ArrayHelperInterface
{
    public function getValueByDotPath($assocArray, $path);
}
