<?php

namespace PixelHumain\Models\Interfaces;

interface CleanTagsInterface
{
    /**
     * Cleans all tags in a given collection.
     *
     * @param string $collection The name of the collection.
     * @param array $doc The document containing the tags to be cleaned.
     * @param string $key The key of the tags in the document.
     * @param string $key3 The third key of the tags in the document.
     * @return void
     */
    public function cleanAllTags(string $collection, array $doc, string $key, string $key3): void;
}
