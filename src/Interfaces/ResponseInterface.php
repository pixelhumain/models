<?php

namespace PixelHumain\Models\Interfaces;

interface ResponseInterface
{
    public function getCookies();
}
