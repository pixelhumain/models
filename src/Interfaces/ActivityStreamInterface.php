<?php

namespace PixelHumain\Models\Interfaces;

interface ActivityStreamInterface
{
    public const COLLECTION = "activityStream";

    /**
     * Adds an entry to the activity stream.
     *
     * @param array $param The parameters for the entry.
     * @return void
     */
    public function addEntry(array $param): void;

    /**
     * Retrieves the activity stream records based on the specified parameters.
     *
     * @param array $params The parameters used to filter the activity stream records.
     * @return array The filtered activity stream records.
     */
    public function getWhere(array $params): array;

    /**
     * Retrieves notifications from the activity stream.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotifications(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array;

    /**
     * Retrieves notifications based on the specified parameters.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotificationsByStep(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array;

    /**
     * Retrieves notifications from the activity stream based on a time limit.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param array $sort The sorting criteria for the notifications. Default is sorting by "updated" in descending order.
     * @return array The array of notifications that match the specified criteria.
     */
    public function getNotificationsByTimeLimit(array $param, array $sort = [
        "updated" => -1,
    ]): array;

    /**
     * Counts the number of unseen notifications for a given user, element type, and element ID.
     *
     * @param string $userId The ID of the user.
     * @param string|null $elementType The type of the element (optional).
     * @param string|null $elementId The ID of the element (optional).
     * @return int The number of unseen notifications.
     */
    public function countUnseenNotifications(string $userId, ?string $elementType, ?string $elementId): int;

    /**
     * Retrieves the activity stream for a given object ID.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting options for the query. Default is descending by timestamp.
     * @return array The activity stream for the object ID.
     */
    public function getActivtyForObjectId(array $param, array $sort = [
        "timestamp" => -1,
    ]): array;

    /**
     * Returns the activity history for a given ID and type.
     *
     * @param string $id The ID of the citizen.
     * @param string $type The type of activity.
     * @return array The activity history.
     */
    public function activityHistory(string $id, string $type): array;

    /**
     * Removes an activity history entry.
     *
     * @param string $id The ID of the activity history entry to remove.
     * @param string $type The type of the activity history entry to remove.
     * @return mixed The result of the removal.
     */
    public function removeActivityHistory(string $id, string $type);

    /**
     * Removes an element from the activity stream.
     *
     * @param string $id The ID of the element to remove.
     * @param string $type The type of the element to remove.
     * @return array The updated activity stream.
     */
    public function removeElementActivityStream(string $id, string $type): array;

    /**
     * Saves the activity history.
     *
     * @param string $verb The verb of the activity.
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string|null $activityName The name of the activity (optional).
     * @param string|null $activityValue The value of the activity (optional).
     * @return void
     */
    public function saveActivityHistory(string $verb, string $targetId, string $targetType, ?string $activityName = null, ?string $activityValue = null): void;

    /**
     * Removes notifications for a specific ID.
     *
     * @param string $id The ID of the notification to be removed.
     * @return array An array containing the removed notifications.
     */
    public function removeNotifications(string $id): array;

    /**
     * Update the notification by its ID.
     *
     * @param string $id The ID of the notification.
     * @param string $action The action to be performed on the notification.
     * @return array The updated notification.
     */
    public function updateNotificationById(string $id, string $action): array;

    /**
     * Updates the notifications for a user based on the specified action.
     *
     * @param string $action The action to update the notifications for.
     * @return array The updated notifications.
     */
    public function updateNotificationsByUser(string $action): array;

    /**
     * Removes notifications for a specific user.
     *
     * @param string $userId The ID of the user.
     * @return array The removed notifications.
     */
    public function removeNotificationsByUser(string $userId): array;

    /**
     * Adds a notification to the activity stream.
     *
     * @param array $params The parameters for the notification.
     * @return array The updated activity stream.
     */
    public function addNotification(array $params): array;

    // todo : elle est utilisé que dans une action RemoveMemberAction qui n'a pas l'air d'être utilisé
    /**
     * Removes an object from the activity stream.
     *
     * @param string $objectId The ID of the object to be removed.
     * @param string $type The type of the object.
     * @param string|null $targetId The ID of the target object (optional).
     * @param string|null $targetType The type of the target object (optional).
     * @param string|null $verb The verb associated with the action (optional).
     * @return mixed The result of the removal.
     */
    public function removeObject(string $objectId, string $type, ?string $targetId = null, ?string $targetType = null, ?string $verb = null);

    /**
     * Builds an entry for the activity stream.
     *
     * @param array $params The parameters for building the entry.
     * @return array The built entry.
     */
    public function buildEntry(array $params): array;

    /**
     * Saves the activity stream without sending an email notification.
     *
     * @param string $mail The email address associated with the activity stream.
     * @return array An array containing the saved activity stream data.
     */
    public function saveNotSendMail(string $mail): array;
}
