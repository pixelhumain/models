<?php

namespace PixelHumain\Models\Interfaces;

interface CommentInterface
{
    public const COLLECTION = "comments";

    //Options of the comment
    public const COMMENT_ON_TREE = "tree";

    public const COMMENT_ANONYMOUS = "anonymous";

    public const ONE_COMMENT_ONLY = "oneCommentOnly";

    //Comment status
    public const STATUS_POSTED = "posted";

    public const STATUS_DECLARED_ABUSED = "declaredAbused";

    public const STATUS_DELETED = "deleted";

    public const STATUS_ACCEPTED = "accepted";

    /**
     * Retrieves a comment by its ID.
     *
     * @param string $id The ID of the comment.
     * @return array|null The comment data as an array or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Counts the number of comments from a specific source.
     *
     * @param mixed|null $from The source from which to count the comments. Defaults to null.
     * @param string $type The type of the source.
     * @param string $id The ID of the source.
     * @param string|null $path The path of the source. Defaults to null.
     * @return int The number of comments from the specified source.
     */
    public function countFrom($from = null, string $type, string $id, ?string $path = null): int;

    /**
     * Retrieves comments based on specified parameters, sorts them, and limits the result.
     *
     * @param array $params The parameters used to filter the comments.
     * @param array $sort The sorting criteria for the comments.
     * @param int $limit The maximum number of comments to retrieve (default is 1).
     * @return array The array of comments that match the specified parameters, sorted and limited.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array;

    public function insert(array $comment, string $userId): array;

    /**
     * Builds the comments tree for a given context.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $userId The ID of the user.
     * @param array|null $filters Optional filters to apply.
     * @param mixed $path Optional path parameter.
     * @return array The comments tree.
     */
    public function buildCommentsTree(string $contextId, string $contextType, string $userId, ?array $filters = null, $path = null): array;

    /**
     * Récupère les commentaires sélectionnés de la communauté.
     *
     * @param string $contextId L'identifiant du contexte.
     * @param string $contextType Le type de contexte.
     * @param array|null $options Les options de récupération des commentaires.
     * @return array Les commentaires sélectionnés de la communauté.
     */
    public function getCommunitySelectedComments(string $contextId, string $contextType, ?array $options): array;

    /**
     * Retrieves the abused comments based on the given context ID, context type, and options.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param array|null $options Additional options for retrieving the abused comments.
     * @return array The array of abused comments.
     */
    public function getAbusedComments(string $contextId, string $contextType, ?array $options): array;

    /**
     * Retrieves the comment options for a given ID and type.
     *
     * @param string $id The ID of the comment.
     * @param string $type The type of the comment.
     * @return array The comment options.
     */
    public function getCommentOptions(string $id, string $type): array;

    /**
     * Saves the comment options.
     *
     * @param string $id The comment ID.
     * @param string $type The comment type.
     * @param array|null $options The comment options.
     * @return array The updated comment options.
     */
    public function saveCommentOptions(string $id, string $type, ?array $options): array;


    //------------------------------------------------------------//
    //---------------------- Abuse Process -----------------------//
    //------------------------------------------------------------//

    /**
     * Signale un abus sur un commentaire.
     *
     * @param string $userId L'identifiant de l'utilisateur signalant l'abus.
     * @param string $commentId L'identifiant du commentaire signalé.
     * @param string $reason La raison du signalement.
     * @return array Les informations sur le signalement effectué.
     */
    public function reportAbuse(string $userId, string $commentId, string $reason): array;

    /**
     * Change the status of a comment.
     *
     * @param string $userId The ID of the user performing the action.
     * @param string $commentId The ID of the comment to change the status of.
     * @param string $action The action to perform on the comment status.
     * @return array The updated comment data.
     */
    public function changeStatus(string $userId, string $commentId, string $action): array;

    /**
     * Update a field in the comment.
     *
     * @param string $commentId The ID of the comment.
     * @param string $name The name of the field to update.
     * @param mixed $value The new value for the field.
     * @param string|null $userId The ID of the user performing the update (optional).
     * @return array The updated comment.
     */
    public function updateField(string $commentId, string $name, $value, ?string $userId = null): array;

    /**
     * Update a comment.
     *
     * @param string $id The ID of the comment to update.
     * @param array $params The parameters to update the comment with.
     * @return array The updated comment.
     */
    public function update(string $id, array $params): array;

    /**
     * Deletes a comment.
     *
     * @param string $id The ID of the comment to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function delete(string $id, string $userId): array;

    /**
     * Deletes all comments associated with a specific context.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $userId The ID of the user.
     * @param bool $deleteProcess Whether to delete the associated process or not. Default is false.
     * @return array The deleted comments.
     */
    public function deleteAllContextComments(string $contextId, string $contextType, string $userId, bool $deleteProcess = false): array;

    /**
     * Retrieves an array of comments to be moderated.
     *
     * @param array|null $whereAdditional Additional conditions to filter the comments (optional).
     * @param int $limit The maximum number of comments to retrieve (optional).
     * @return array An array of comments to be moderated.
     */
    public function getCommentsToModerate(?array $whereAdditional = null, int $limit = 0): array;
}
