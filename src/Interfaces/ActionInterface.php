<?php

namespace PixelHumain\Models\Interfaces;

interface ActionInterface
{
    public const NODE_ACTIONS = "actions";

    public const COLLECTION = "actions";

    public const CONTROLLER = "action";

    public const ACTION_ROOMS = "actionRooms";

    public const ACTION_ROOMS_TYPE_SURVEY = "survey";

    public const ACTION_MODERATE = "moderate";

    public const ACTION_VOTE_UP = "voteUp";

    public const ACTION_VOTE = "vote";

    public const ACTION_VOTE_ABSTAIN = "voteAbstain";

    public const ACTION_VOTE_UNCLEAR = "voteUnclear";

    public const ACTION_VOTE_MOREINFO = "voteMoreInfo";

    public const ACTION_VOTE_DOWN = "voteDown";

    public const ACTION_FUND = "fund";

    public const ACTION_PURCHASE = "purchase";

    public const ACTION_COMMENT = "comment";

    public const ACTION_REPORT_ABUSE = "reportAbuse";

    public const ACTION_FOLLOW = "follow";

    /**
     * Returns the data binding for the Action model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array;

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @param array|null $fields The fields to retrieve. If null, all fields will be retrieved.
     * @return array|null The action object if found, null otherwise.
     */
    public function getById(string $id, ?array $fields = null): ?array;

    /**
     * Retrieves the simple specification of an action by its ID.
     *
     * @param string $id The ID of the action.
     * @param array|null $where Additional condition to filter the results (optional).
     * @param array|null $fields Specific fields to retrieve (optional).
     * @return array|null The simple specification of the action.
     */
    public function getSimpleSpecById(string $id, ?array $where = null, ?array $fields = null): ?array;

    /**
     * Adds an action to the collection.
     *
     * @param string $userId The user ID.
     * @param string $id The ID of the action.
     * @param string $collection The name of the collection.
     * @param string $action The action to be added.
     * @param bool $unset (optional) Whether to unset the action.
     * @param bool $multiple (optional) Whether to allow multiple actions.
     * @param array|null $details (optional) Additional details for the action.
     * @param string|null $path (optional) The path for the action.
     * @return array The updated collection.
     */
    public function addAction(string $userId, string $id, string $collection, string $action, bool $unset = false, bool $multiple = false, ?array $details = null, ?string $path = null): array;

    /**
     * Get a list of actions.
     *
     * @param string $type The type of the action.
     * @param string $id The ID of the action.
     * @param string $actionType The type of the action.
     * @param int $indexStep The index step.
     * @return array|null The list of actions or null if not found.
     */
    public function getList(string $type, string $id, string $actionType, int $indexStep = 0): ?array;

    /**
     * Adds an action history entry.
     *
     * @param string|null $userId The ID of the user performing the action.
     * @param mixed|null $id The ID of the action.
     * @param string|null $collection The name of the collection.
     * @param string|null $action The action performed.
     * @return void
     */
    public function addActionHistory(?string $userId = null, $id = null, string $collection = null, $action = null): void;

    /**
     * Updates the parent of the action.
     *
     * @param string|null $id The ID of the action.
     * @param string|null $collection The collection of the action.
     * @return void
     */
    public function updateParent(?string $id = null, ?string $collection = null): void;

    /**
     * Check if a user is following a specific action type.
     *
     * @param mixed $value The value to check.
     * @param string $actionType The action type to check against.
     * @return bool Returns true if the user is following the action type, false otherwise.
     */
    public function isUserFollowing($value, string $actionType): bool;

    /**
     * Vote for links and infos.
     *
     * @param bool $logguedAndValid Indicates if the user is logged in and valid.
     * @param array $value The value to be voted on.
     * @return array The updated array after voting.
     */
    public function voteLinksAndInfos(bool $logguedAndValid, array $value): array;

    /**
     * Format the parameters before saving.
     *
     * @param array $params The parameters to be formatted.
     * @return array The formatted parameters.
     */
    public function formatBeforeSaving(array $params): array;
}
