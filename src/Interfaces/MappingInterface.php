<?php

namespace PixelHumain\Models\Interfaces;

interface MappingInterface
{
    public const COLLECTION = "mappings";

    /**
     * Insert a new mapping into the database.
     *
     * @param array $mapping The mapping data to be inserted.
     * @param string $creatorId The ID of the creator.
     * @return array The inserted mapping data.
     */
    public function insert(array $mapping, string $creatorId): array;

    /**
     * Deletes a record from the database.
     *
     * @param array $post The post data.
     * @param string|null $creatorId The ID of the creator.
     * @return array The updated array after deletion.
     */
    public function delete(array $post, ?string $creatorId): array;

    /**
     * Update the mapping with the given post data and creator ID.
     *
     * @param array $post The post data to update the mapping.
     * @param string|null $creatorId The ID of the creator. Can be null.
     * @return array The updated mapping.
     */
    public function update(array $post, ?string $creatorId): array;

    /**
     * Replaces dots in the keys of the given array with the provided new field.
     *
     * @param array $newField The new field to replace the dots with.
     * @return array The modified array with dots replaced.
     */
    public function replaceDot(array $newField): array;

    /**
     * Replaces the keys in the array with dot notation.
     *
     * @param array $newField The array with the new keys.
     * @return array The modified array with dot notation keys.
     */
    public function replaceByRealDot(array $newField): array;
}
