<?php

namespace PixelHumain\Models\Interfaces;

interface ImportInterface
{
    public const MAPPINGS = "mappings";

    /**
     * Retrieves and checks the address for an entity.
     *
     * @param array|null $address The address information.
     * @param array|null $geo The geographic information.
     * @return array The updated address information.
     */
    public function getAndCheckAddressForEntity(?array $address = null, ?array $geo = null): array;
}
