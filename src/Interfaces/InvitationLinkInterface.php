<?php

namespace PixelHumain\Models\Interfaces;

interface InvitationLinkInterface
{
    public const COLLECTION = "invitationLinks";
}
