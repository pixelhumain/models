<?php

namespace PixelHumain\Models\Interfaces;

interface FormInterface
{
    public const COLLECTION = "forms";

    public const CONTROLLER = "forms";

    public const ANSWER_COLLECTION = "answers";

    public const INPUTS_COLLECTION = "inputs";

    public const ANSWER_CONTROLLER = "answer";

    public const ICON = "fa-list-alt";

    public const ICON_ANSWER = "fa-calendar-check-o";
}
