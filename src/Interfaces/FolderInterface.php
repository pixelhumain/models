<?php

namespace PixelHumain\Models\Interfaces;

interface FolderInterface
{
    public const COLLECTION = "folders";

    public const CONTROLLER = "folder";

    /**
     * Retrieves a folder by its ID.
     *
     * @param string $id The ID of the folder.
     * @return array|null The folder data if found, null otherwise.
     */
    public function getById(string $id): ?array;

    /**
     * Create a new folder.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $name The name of the folder.
     * @param string|null $docType The type of the document.
     * @param string|null $parentId The ID of the parent folder.
     * @return array The created folder.
     */
    public function create(string $contextId, string $contextType, string $name, string $docType = null, string $parentId = null): array;

    /**
     * Update a folder.
     *
     * @param string $id The ID of the folder.
     * @param string $name The new name of the folder.
     * @param bool $del Whether to delete the folder or not.
     * @return array The updated folder data.
     */
    public function update(string $id, string $name, bool $del = false): array;

    /**
     * Retrieves an array of subfolders by their ID.
     *
     * @param string $id The ID of the folder.
     * @return array An array of subfolders.
     */
    public function getSubfoldersById(string $id): array;

    /**
     * Retrieves an array of subfolders based on the given context ID, context type, and document type.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $docType The type of the document.
     * @return array An array of subfolders.
     */
    public function getSubfoldersByContext(string $contextId, string $contextType, string $docType): array;

    /**
     * Deletes a folder.
     *
     * @param string $id The ID of the folder to delete.
     * @param bool $removeDir Whether to remove the directory associated with the folder.
     * @return array An array containing the result of the deletion operation.
     */
    public function delete(string $id, bool $removeDir = true): array;

    /**
     * Retrieves the parent folders of a folder by its ID.
     *
     * @param string $id The ID of the folder.
     * @return array|null An array of parent folders or null if no parent folders are found.
     */
    public function getParentsFoldersById(string $id): ?array;

    /**
     * Returns the path of the parent folders for a given folder ID.
     *
     * @param string $id The ID of the folder.
     * @return string The path of the parent folders.
     */
    public function getParentFoldersPath(string $id): string;

    /**
     * Returns the folder path for a given folder.
     *
     * @param array $folder The folder data.
     * @param bool $thumb Whether to include the thumbnail path or not. Default is false.
     * @return string|false The folder path.
     */
    public function getFolderPath(array $folder, bool $thumb = false);

    /**
     * Move the specified documents to a new folder.
     *
     * @param array $ids The IDs of the documents to move.
     * @param string|null $idFolder The ID of the destination folder. If null, the documents will be moved to the root folder.
     * @param string $type The type of the documents to move. Defaults to DocumentInterface::COLLECTION.
     * @return array The updated documents.
     */
    public function moveToFolder(array $ids, string $idFolder = null, string $type = 'documents'): array;

    /**
     * Counts the number of subfolders for a given folder ID.
     *
     * @param string $id The ID of the folder.
     * @return int The number of subfolders.
     */
    public function countSubfolders(string $id): int;

    /**
     * Counts the number of subfolders by context.
     *
     * @param string $id The ID of the folder.
     * @param string $type The type of the folder.
     * @param string $docType The document type.
     * @return int The number of subfolders.
     */
    public function countSubfoldersByContext(string $id, string $type, string $docType): int;
}
