<?php

namespace PixelHumain\Models\Interfaces;

interface TemplateInterface
{
    public const COLLECTION = "templates";

    public const CONTROLLER = "template";

    public const MODULE = "template";

    public const ICON = "fa-file";

    /**
     * Get a template by its ID.
     *
     * @param string $id The ID of the template.
     * @return array The template data.
     */
    public function getById(string $id): array;
}
