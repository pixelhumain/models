<?php

namespace PixelHumain\Models\Interfaces;

interface ButtonCtkInterface
{
    /**
     * Generates a search bar HTML element.
     *
     * @param array $params The parameters for the search bar.
     * @return string The generated search bar HTML element.
     */
    public function searchBar(array $params): string;

    /**
     * Méthode pour gérer l'application du bouton.
     *
     * @param array $params Les paramètres de l'application.
     * @return string Le résultat de l'application du bouton.
     */
    public function app(array $params): string;

    /**
     * Generates a button with tooltips.
     *
     * @param string $pos The position of the tooltips.
     * @param string $label The label of the button.
     * @return string The generated button with tooltips.
     */
    public function buttonTooltips(string $pos, string $label): string;
}
