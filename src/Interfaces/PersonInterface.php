<?php

namespace PixelHumain\Models\Interfaces;

use MongoDB\BSON\ObjectId;

interface PersonInterface
{
    public const COLLECTION = "citoyens";

    public const CONTROLLER = "person";

    public const ICON = "fa-user";

    public const COLOR = "#F5E740";

    public const REGISTER_MODE_MINIMAL = "minimal";

    public const REGISTER_MODE_NORMAL = "normal";

    public const REGISTER_MODE_TWO_STEPS = "two_steps_register";

    public const REGISTER_MODE_SERVICE = "service";

    /**
     * Checks if the person is logged in and their account is valid.
     *
     * @return bool Returns true if the person is logged in and their account is valid, false otherwise.
     */
    public function logguedAndValid(): bool;

    /**
     * Check if the person is logged in and authorized.
     *
     * @return bool Returns true if the person is logged in and authorized, false otherwise.
     */
    public function logguedAndAuthorized(): bool;

    /**
     * Saves the user session data.
     *
     * @param array {
     *     slug?: string, // Identifiant unique pour l'utilisateur, facultatif.
     *     cp?: string, // Code postal, facultatif.
     *     address?: mixed, // Adresse, facultatif.
     *     roles?: array<mixed>, // Rôles de l'utilisateur, facultatif.
     *     preferences?: array<mixed>, // Préférences de l'utilisateur, facultatif.
     *     lastLoginDate?: int, // Timestamp de la dernière connexion, facultatif.
     *     _id: string|ObjectId, // Identifiant MongoDB de l'utilisateur.
     *     serviceName?: ?string, // Nom du service, facultatif.
     *     services?: array<string, mixed>,
     *     name?: string, // Nom de l'utilisateur, facultatif.
     *     email?: string, // Email de l'utilisateur, facultatif.
     *     username?: string, // Nom d'utilisateur de l'utilisateur, facultatif.
     *     profilThumbImageUrl?: string, // URL de l'image de profil de l'utilisateur, facultatif.
     * } $account Tableau associatif contenant les données utilisateur avec une structure détaillée.
     * @param bool $isRegisterProcess Indicates if the process is a registration process.
     * @param string|null $pwd The password.
     * @return void
     */
    public function saveUserSessionData(array $account, bool $isRegisterProcess = false, ?string $pwd = null): void;

    /**
     * Clears the user session data.
     *
     * @return void
     */
    public function clearUserSessionData(): void;

    /**
     * get a Person By Id
     * @param string $id is the mongoId of the person
     * @param boolean $clearAttribute by default true. Will clear the confidential attributes
     * @return array|null person document as in db
     */
    public function getById(string $id, bool $clearAttribute = true): ?array;

    /**
     * Clear attributes based on the confidentiality level of the entity.
     *
     * @param array|null $entity The entity to check the confidentiality level for.
     * @return array|null The entity with the attributes cleared, or null if the entity is not valid.
     */
    public function clearAttributesByConfidentiality(?array $entity = null): ?array;

    /**
     * Retrieves a person by their array ID.
     *
     * @param array $arrayId The array ID of the person.
     * @param array $fields The fields to retrieve. Default is an empty array.
     * @param bool $clearAttribute Whether to clear the attributes of the person. Default is true.
     * @param bool $simpleUser Whether the person is a simple user. Default is false.
     * @return array The array of persons.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $clearAttribute = true, bool $simpleUser = false): array;

    /**
     * Retrieves a simple user by their ID.
     *
     * @param string $id The ID of the user.
     * @param array|null $person The person object.
     * @return array The simple user.
     */
    public function getSimpleUserById(string $id, ?array $person = null): ?array;

    /**
     * Retrieves the minimal user information by their ID.
     *
     * @param string $id The ID of the user.
     * @param array|null $person The person.
     * @return array The minimal user information.
     */
    public function getMinimalUserById(string $id, ?array $person = null): array;

    /**
     * Get a list of persons by condition.
     *
     * @param array $params The parameters to use to retrieve the persons.
     * @return array The list of persons.
     */
    public function getWhere(array $params): array;

    /**
     * Retrieves the organizations associated with a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array The organizations associated with the person.
     */
    public function getOrganizationsById(string $id): array;

    /**
     * Retrieves the person links associated with a given person ID.
     *
     * @param string $id The ID of the person.
     * @return array The person links associated with the given person ID.
     */
    public function getPersonLinksByPersonId(string $id): array;

    /**
     * Creates a new person and sends an invitation.
     *
     * @param array $param The parameters for creating the person.
     * @param string|null $msg The message to include in the invitation. DEPRECATED
     * @param string|null $gmail The Gmail address to send the invitation to. DEPRECATED
     * @return array The created person and invitation details.
     */
    public function createAndInvite(array $param, ?string $msg = null, ?string $gmail = null): array;

    /**
     * Insert a person into the database.
     *
     * @param array $person The person data to be inserted.
     * @param string $mode The registration mode. Defaults to self::REGISTER_MODE_NORMAL.
     * REGISTER_MODE_MINIMAL : a person can be created using only name and email.
     * REGISTER_MODE_NORMAL : name, email, username, password, postalCode, city
     * REGISTER_MODE_TWO_STEPS : name, username, email, password
     * @param string|null $inviteCode The invite code. Defaults to null.
     * @param array|null $forced The forced data. Defaults to null.
     * @return array The inserted person data.
     */
    public function insert(array $person, string $mode = 'normal', ?string $inviteCode = null, ?array $forced = null): array;

    /**
     * Retrieves a person by their email address.
     *
     * @param string $email The email address of the person.
     * @return array|null An array containing the person's information if found, or null if not found.
     */
    public function getPersonByEmail(string $email): ?array;

    /**
     * Check if the given username is unique.
     *
     * @param string $username The username to check.
     * @return bool Returns true if the username is unique, false otherwise.
     */
    public function isUniqueUsername(string $username): bool;

    /**
     * Update a specific field of a person.
     *
     * @param string $personId The ID of the person.
     * @param string $personFieldName The name of the field to update.
     * @param mixed $personFieldValue The new value for the field.
     * @param string $userId The ID of the user performing the update.
     * @return array Result status and message.
     */
    public function updatePersonField(string $personId, string $personFieldName, $personFieldValue, string $userId): array;

    /**
     * Retrieves the public data of a person.
     *
     * @param string $id The ID of the person.
     * @return array|null The public data of the person, or null if the person is not found.
     */
    public function getPublicData(string $id): ?array;

    /**
     * Logs in a person.
     *
     * @param string|null $emailOrUsername The email or username of the person.
     * @param string|null $pwd The password of the person.
     * @param bool $isRegisterProcess (optional) Whether the login is part of a registration process.
     * @param bool $firstConnection (optional) Whether it is the person's first connection.
     * @return array The result of the login method.
     */
    public function login(?string $emailOrUsername, ?string $pwd, bool $isRegisterProcess = false, bool $firstConnection = false): array;

    /**
     * Check if the provided password matches the password stored in the account.
     *
     * @param string $pwd The password to check.
     * @param array $account The account information containing the stored password.
     * @return bool Returns true if the password matches, false otherwise.
     */
    public function checkPassword(string $pwd, array $account): bool;

    /**
     * Updates the login history for a person.
     *
     * @param string $accountId The account ID of the person.
     * @return array The updated login history.
     */
    public function updateLoginHistory(string $accountId): array;

    /**
     * Update the cookie communexion for a person.
     *
     * @param string|null $userId The user ID.
     * @param array|null $address The address array.
     * @return array The updated cookie communexion.
     */
    public function updateCookieCommunexion(?string $userId, ?array $address): array;

    /**
     * Logs in a user and create authentication token.
     *
     * @param string|null $emailOrUsername The email or username of the user.
     * @param string|null $pwd The password of the user.
     * @param string $tokenName The name of the authentication token.
     * @param bool $isRegisterProcess Indicates if the login is part of a registration process.
     * @return array The result of the login method.
     */
    public function loginAuthToken(?string $emailOrUsername, ?string $pwd, string $tokenName = "comobi", bool $isRegisterProcess = false): array;

    /**
     * Adds a token to the person's account.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token.
     * @param bool $regenerate Whether to regenerate the token if it already exists.
     * @return string|null The token if it was added, false else.
     */
    public function addToken(string $accountId, string $tokenName, bool $regenerate = false): ?string;

    /**
     * Removes a token from the person's account.
     *
     * @param string $accountId The ID of the account.
     * @param string $tokenName The name of the token to remove.
     * @return bool Returns true if the token was successfully removed, false otherwise.
     */
    public function removeToken(string $accountId, string $tokenName): bool;

    /**
     * Service Oauth.
     *
     * This method is used to perform OAuth service authentication.
     *
     * @param array|null $serviceDecrypt The decrypted service data.
     * @param string|null $tokenName The name of the token.
     * @return array The result of the OAuth service authentication.
     */
    public function serviceOauth(?array $serviceDecrypt, ?string $tokenName = null): array;

    /**
     * Logs in a user using the authentication service token.
     *
     * @param string $service The authentication service details.
     * @param string|null $tokenName The name of the authentication token.
     * @return array The user information.
     */
    public function loginAuthServiceToken(string $service, ?string $tokenName = null): array;

    /**
     * Encrypts a value using the CryptoJS AES algorithm.
     *
     * @param string $passphrase The passphrase used for encryption.
     * @param mixed $value The value to be encrypted.
     * @return string The encrypted value.
     */
    public function cryptoJsAesEncrypt(string $passphrase, $value): string;

    /**
     * Change the password for a user.
     *
     * @param string $oldPassword The old password.
     * @param string $newPassword The new password.
     * @param string $userId The user ID.
     * @return array The result of the password change.
     */
    public function changePassword(string $oldPassword, string $newPassword, string $userId): array;

    /**
     * Validates the email account with the given account ID and validation key.
     *
     * @param string $accountId The ID of the email account to validate.
     * @param string $validationKey The validation key for the email account.
     * @return array An array containing the validation result.
     */
    public function validateEmailAccount(string $accountId, string $validationKey): array;

    /**
     * Check if the provided validation key is correct for the given account ID.
     *
     * @param string $accountId The account ID to check the validation key for.
     * @param string $validationKey The validation key to check.
     * @return bool Returns true if the validation key is correct, false otherwise.
     */
    public function isRightValidationKey(string $accountId, string $validationKey): bool;

    /**
     * Validates a user.
     *
     * @param string $accountId The account ID of the user.
     * @param bool $admin (optional) Whether the user is an admin or not. Default is false.
     * @return array The result of the validation method.
     */
    public function validateUser(string $accountId, bool $admin = false): array;

    /**
     * Returns the validation key check for a given account ID.
     *
     * @param string $accountId The account ID to check the validation key for.
     * @return string The validation key check.
     */
    public function getValidationKeyCheck(string $accountId): string;

    /**
     * Retrieves the email of a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array|null The email of the person, or null if not found.
     */
    public function getEmailById(string $id): ?array;

    /**
     * Updates the minimal data of a person.
     *
     * @param string $personId The ID of the person.
     * @param array $person The updated person data.
     * @return array The updated person data.
     */
    public function updateMinimalData(string $personId, array $person): array;

    /**
     * Check if the given email is unique.
     *
     * @param string $email The email to check.
     * @return bool Returns true if the email is unique, false otherwise.
     */
    public function isUniqueEmail(string $email): bool;

    /**
     * Check if the person is the first citizen with the given INSEE number.
     *
     * @param string $insee The INSEE number to check.
     * @return bool Returns true if the person is the first citizen, false otherwise.
     */
    public function isFirstCitizen(string $insee): bool;

    /**
     * Retrieves the person ID associated with the given email.
     *
     * @param string|null $email The email address of the person.
     * @return mixed The person ID if found, false or null otherwise.
     */
    public function getPersonIdByEmail(?string $email);

    /**
     * Retrieves the roles of a person by their ID.
     *
     * @param string $id The ID of the person.
     * @return array|null The roles of the person.
     */
    public function getRolesById(string $id): ?array;

    /**
     * Update the role of a person.
     *
     * @param string $userId The ID of the user.
     * @param string $mongoAction The action to perform on the MongoDB.
     * @param string $role The role to update.
     * @param mixed $roleValue The new value for the role.
     * @return array The result of the update.
     */
    public function updatePersonRole(string $userId, string $mongoAction, string $role, $roleValue): array;

    /**
     * Retrieves a pending user by email.
     *
     * @param string|null $email The email of the pending user.
     * @return string|null The ID of the pending user, or null if not found.
     */
    public function getPendingUserByEmail(?string $email): ?string;

    /**
     * Returns the data binding model.
     *
     * @return array The data binding configuration.
     */
    public function getDataBinding(): array;
}
