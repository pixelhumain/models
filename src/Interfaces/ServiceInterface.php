<?php

namespace PixelHumain\Models\Interfaces;

interface ServiceInterface
{
    public const COLLECTION = "services";

    public const CONTROLLER = "service";
}
