<?php

namespace PixelHumain\Models\Interfaces;

interface CrowdfundingInterface
{
    public const COLLECTION = "crowdfunding";

    public const CONTROLLER = "crowdfunding";

    public const MODULE = "crowdfunding";

    public const ICON = "fa-map-money";

    public const TYPE_CAMPAIGN = "campaign";

    public const TYPE_DONATION = "donation";

    public const TYPE_PLEDGE = "pledge";
}
