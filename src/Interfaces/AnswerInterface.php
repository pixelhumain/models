<?php

namespace PixelHumain\Models\Interfaces;

interface AnswerInterface
{
    public const COLLECTION = "answers";

    public const CONTROLLER = "answer";

    public const ICON = "fa-calendar-check-o";

    /**
     * Retrieves an answer by its ID.
     *
     * @param string $id The ID of the answer.
     * @param array $fields The optional fields to retrieve.
     * @return array|null The answer data as an associative array, or null if not found.
     */
    public function getById(string $id, array $fields = []): ?array;

    /**
     * Generates an answer based on the given form.
     *
     * @param array $form The form data.
     * @param bool $draft Indicates if the answer is a draft (optional, default: false).
     * @param array|null $context The context for generating the answer (optional).
     * @return array The generated answer.
     */
    public function generateAnswer(array $form, bool $draft = false, ?array $context = null): array;

    /**
     * Deletes an answer by its ID.
     *
     * @param string $id The ID of the answer to delete.
     * @return array The result of the deletion operation.
     */
    public function delete(string $id): array;

    /**
     * Removes links from the given answer.
     *
     * @param array $answer The answer to remove links from.
     * @return bool Returns true if the links were successfully removed, false otherwise.
     */
    public function removeLinks(array $answer): bool;

    /**
     * Check if the user can administer the answer.
     *
     * @param array $ans The answer data.
     * @param array|null $form The form data (optional).
     * @return bool Returns true if the user can administer the answer, false otherwise.
     */
    public function canAdmin(array $ans, ?array $form = null): bool;

    /**
     * Check if the answer can be edited.
     *
     * @param string|array $ans The answer ID or data.
     * @param array|null $form The form data (optional).
     * @param string|null $userId The user ID (optional).
     * @param mixed $parent The parent object (optional).
     * @return bool Returns true if the answer can be edited, false otherwise.
     */
    public function canEdit($ans, ?array $form = null, ?string $userId = null, ?array $parent = null): bool;

    /**
     * Determines if a user can access the answer.
     *
     * @param string|array $answer The answer ID or data.
     * @param array|null $form The form associated with the answer (optional).
     * @param string|null $userId The ID of the user (optional).
     * @param array|null $parentForm The parent form of the answer (optional).
     * @return bool Returns true if the user can access the answer, false otherwise.
     */
    public function canAccess($answer, ?array $form = null, ?string $userId = null, ?array $parentForm = null): bool;

    /**
     * Get a list of answers based on optional parameters.
     *
     * @param string|null $form The form parameter.
     * @param string|null $costum The costum parameter.
     * @param string|null $userId The user ID parameter.
     * @param array|null $cond The condition parameter.
     * @return array The list of answers.
     */
    public function getListBy(?string $form = null, ?string $costum = null, ?string $userId = null, ?array $cond = null): array;

    /**
     * Global autocomplete function.
     *
     * @param array $form The form data.
     * @param array $searchParams The search parameters.
     * @return array The autocomplete results.
     */
    public function globalAutocomplete(array $form, array $searchParams): array;

    /**
     * Returns an array of mapping values based on the given mapping and answer.
     *
     * @param array $mapping The mapping array.
     * @param array $ans The answer array.
     * @return array The array of mapping values.
     */
    public function getMappingValues(array $mapping, array $ans): array;

    /**
     * Get the value from an array by accessing it using a given path.
     *
     * @param array|string $indexes The path to access the value in the array.
     * @param array $arrayToAccess The array to access the value from.
     * @return mixed The value from the array at the specified path.
     */
    public function getValueByPath($indexes, array $arrayToAccess);

    /**
     * Récupère les données des réponses.
     *
     * @param array $answerList La liste des réponses.
     * @param array $subForms Les sous-formulaires.
     * @param array $form Le formulaire.
     * @return array Les données des réponses.
     */
    public function getDataAnswers(array $answerList, array $subForms, array $form): array;

    /**
     * Process the mail.
     *
     * @param string $id The ID of the mail.
     * @param string $tpl The template to use for the mail.
     * @param string|null $step The step of the mail process.
     * @param array $infos Additional information for the mail.
     * @return void
     */
    public function mailProcess(string $id, string $tpl, ?string $step, array $infos): void;

    /**
     * Validates the given parameters.
     *
     * @param array $params The parameters to validate.
     * @return array The validated parameters.
     */
    public function validate(array $params): array;

    /**
     * Check if a value is true.
     *
     * @param string|bool|int $val The value to check.
     * @param bool $return_null Whether to return null if the value is not true. Default is false.
     * @return bool Returns true if the value is true, false otherwise.
     */
    public function is_true($val, bool $return_null = false): bool;

    /**
     * Generates a project from an answer.
     *
     * @param string $name The name of the project.
     * @param string $descr The description of the project.
     * @param array $links The links related to the project (optional).
     * @param array $financer The financers of the project (optional).
     * @param array $parent The parent project (required).
     * @param array $action The actions related to the project (optional).
     * @param string $answerId The ID of the answer.
     * @param array|null $images The images related to the project (optional).
     *
     * @return array The generated project.
     */
    public function GenerateProjectFromAnswer(string $name, string $descr, array $links, array $financer, array $parent, array $action, string $answerId, ?array $images = null): array;

    /**
     * Calcule le résultat d'un vote majoritaire.
     *
     * @param array $parentForm Le tableau contenant les données du formulaire parent.
     * @param string $formId L'identifiant du formulaire.
     * @param mixed $key La clé utilisée pour récupérer les données du formulaire.
     * @param array $paramsData Les données supplémentaires utilisées dans le calcul.
     * @return string Le résultat du vote majoritaire.
     */
    public function ResultVoteMajoritaire(array $parentForm, string $formId, $key, array $paramsData = []): string;
}
