<?php

namespace PixelHumain\Models\Interfaces;

interface EventInterface
{
    public const COLLECTION = "events";

    public const CONTROLLER = "event";

    public const ICON = "fa-calendar";

    public const COLOR = "#F9B21A";

    public const NO_ORGANISER = "dontKnow";

    /**
     * Retrieves an event by its ID.
     *
     * @param string $id The ID of the event.
     * @return array The event data.
     */
    public function getById(string $id): array;

    /**
     * Get events by array of IDs.
     *
     * @param array $arrayId The array of event IDs.
     * @param array $fields The optional array of fields to retrieve.
     * @param bool $simply Whether to simplify the result or not.
     * @return array The array of events.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array;

    /**
     * Format the date for rendering.
     *
     * @param array $params The parameters for formatting the date.
     * @return array The formatted date.
     */
    public function formatDateRender(array $params): array;

    /**
     * Retrieves a simple project by its ID.
     *
     * @param string $id The ID of the project.
     * @param array|null $event Optional event data.
     * @return array The simple project data.
     */
    public function getSimpleProjectById(string $id, ?array $event = null): array;

    /**
     * Get the events that match the given parameters.
     *
     * @param array $params The parameters to filter the events.
     * @return array The array of events that match the given parameters.
     */
    public function getWhere(array $params): array;

    /**
     * Retrieves events based on specified conditions, sorting, and limit.
     *
     * @param array $where An array of conditions to filter the events.
     * @param array $orderBy An array specifying the sorting order of the events.
     * @param int $indexStep The number of events to skip before returning results.
     * @param int $indexMin The minimum index of events to retrieve.
     * @return array An array of events matching the specified conditions.
     */
    public function getEventByWhereSortAndLimit(array $where, array $orderBy, int $indexStep, int $indexMin): array;

    /**
     * Format the event data before saving.
     *
     * @param array $params The event parameters.
     * @return array The formatted event data.
     */
    public function formatBeforeSaving(array $params): array;

    /**
     * Adds additional information to the events.
     *
     * @param array $events The array of events.
     * @return array The updated array of events.
     */
    public function addInfoEvents(array $events): array;

    /**
     * Retrieves the events from OpenAgenda.
     *
     * @param string $eventsIdOpenAgenda The ID of the events in OpenAgenda.
     * @return array The array of events from OpenAgenda.
     */
    public function getEventsOpenAgenda(string $eventsIdOpenAgenda): array;

    /**
     * Returns the state of events from OpenAgenda.
     *
     * @param string $eventsIdOpenAgenda The ID of the events from OpenAgenda.
     * @param string $dateUpdate The date of the last update.
     * @param array $endDateOpenAgenda The array of end dates for the events.
     * @return string The state of the events.
     */
    public function getStateEventsOpenAgenda(string $eventsIdOpenAgenda, string $dateUpdate, array $endDateOpenAgenda): string;

    /**
     * Crée des événements à partir des données d'OpenAgenda.
     *
     * @param array $eventOpenAgenda Les données d'OpenAgenda pour créer les événements.
     * @return array Les événements créés.
     */
    public function createEventsFromOpenAgenda(array $eventOpenAgenda): array;

    /**
     * Saves an event from OpenAgenda.
     *
     * @param array $params The parameters for the event.
     * @param string $moduleId The module ID.
     * @return array The saved event.
     */
    public function saveEventFromOpenAgenda(array $params, string $moduleId): array;

    /**
     * Retrieves and checks the event from OpenAgenda.
     *
     * @param array $event The event details.
     * @return array The event details after checking.
     */
    public function getAndCheckEventOpenAgenda(array $event): array;

    /**
     * Returns the data binding for the event.
     *
     * @return array The data binding for the event.
     */
    public function getDataBinding(): array;
}
