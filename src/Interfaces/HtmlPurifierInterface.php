<?php

namespace PixelHumain\Models\Interfaces;

interface HtmlPurifierInterface
{
    /**
     * Process the given content using HTMLPurifier.
     *
     * @param string $content The content to be processed.
     * @param array|null $config Optional configuration settings for HTMLPurifier.
     * @return string The processed content.
     */
    public static function process($content, $config = null);
}
