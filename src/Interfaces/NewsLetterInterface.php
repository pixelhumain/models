<?php

namespace PixelHumain\Models\Interfaces;

use Exception;

interface NewsLetterInterface
{
    public const COLLECTION = "newsletter";

    public const TYPE_TEMPLATE = "template";

    public const TYPE_MAIL = "mail";

    public const URL_API_MJML = "https://api2.communecter.org/mjml/tohtml";

    /**
     * Sends a newsletter email.
     *
     * @param string $authorId The ID of the author sending the newsletter.
     * @param string $contextType The type of context for the newsletter (e.g., "article", "event").
     * @param string $contextId The ID of the context for the newsletter.
     * @param array $mail The email details (e.g., subject, body, recipients).
     * @return string The result of the email sending process.
     */
    public function send(string $authorId, string $contextType, string $contextId, array $mail): string;

    /**
     * Saves a template for a newsletter.
     *
     * @param string $authorId The ID of the author.
     * @param string $contextType The type of the context.
     * @param string $contextId The ID of the context.
     * @param array $template The template to be saved.
     * @return string The ID of the saved template.
     * @throws Exception If the author or the context is not found or if the template is invalid.
     */
    public function saveTemplate(string $authorId, string $contextType, string $contextId, array $template): string;

    /**
     * Validates the template parameter.
     *
     * @param array $template The template parameter to validate.
     * @return void
     */
    public function validateTemplateParameter(array $template): void;

    /**
     * Retrieves the newsletter page based on the provided UUID.
     *
     * @param string $uuid The UUID of the newsletter page.
     * @return string The newsletter page content.
     */
    public function getNewsLetterPage(string $uuid): string;

    /**
     * Retrieves the template for the newsletter.
     *
     * @param string|null $uuid The UUID of the template (optional).
     * @return array The template data.
     */
    public function getTemplate(?string $uuid = null): array;

    /**
     * Updates the template for a newsletter.
     *
     * @param string $userId The ID of the user.
     * @param string $templateId The ID of the template.
     * @param array $data The data to update the template with.
     * @return string The updated template.
     * @throws Exception If the template is not found or the user is not authorized to update it.
     */
    public function updateTemplate(string $userId, string $templateId, array $data): string;

    /**
     * Deletes a template from the newsletter.
     *
     * @param string $userId The ID of the user deleting the template.
     * @param string $templateId The ID of the template to be deleted.
     * @return string The ID of the deleted template.
     * @throws Exception If the template is not found or the user is not authorized to delete it.
     */
    public function deleteTemplate(string $userId, string $templateId): string;
}
