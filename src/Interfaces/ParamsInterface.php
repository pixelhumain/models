<?php

namespace PixelHumain\Models\Interfaces;

use ArrayAccess;

/**
 * Interface ParamsInterface
 *
 * This interface represents a contract for classes that implement parameter storage functionality.
 * It extends the ArrayAccess interface, allowing the object to be accessed like an array.
 */
interface ParamsInterface extends ArrayAccess
{
    /**
     * Get the value associated with the specified key.
     *
     * @param string $key The key of the value to retrieve.
     * @return mixed The value associated with the specified key.
     */
    public function get(string $key);

    /**
     * Set the value associated with the specified key.
     *
     * @param string $key The key of the value to set.
     * @param mixed $value The value to set.
     * @return void
     */
    public function set(string $key, $value): void;

    /**
     * Check if the specified key exists.
     *
     * @param string $key The key to check.
     * @return bool True if the key exists, false otherwise.
     */
    public function has(string $key): bool;
}
