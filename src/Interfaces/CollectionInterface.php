<?php

namespace PixelHumain\Models\Interfaces;

interface CollectionInterface
{
    /**
     * Create a new collection with the given name.
     *
     * @param string $name The name of the collection.
     * @return array The created collection.
     */
    public function create(string $name): array;

    /**
     * Update a collection item.
     *
     * @param string $name The name of the item to update.
     * @param string $newName The new name for the item.
     * @param bool $del Whether to delete the item after updating.
     * @return array The updated collection.
     */
    public function update(string $name, string $newName, bool $del = false): array;

    /**
     * Adds an item to the collection.
     *
     * @param string $targetId The ID of the target item.
     * @param string $targetType The type of the target item.
     * @param string $collection The name of the collection (default: "favorites").
     * @return array The updated collection.
     */
    public function add(string $targetId, string $targetType, string $collection = 'favorites'): array;

    /**
     * Retrieves a collection of items.
     *
     * @param string|null $userId The user ID. Defaults to null.
     * @param string|null $type The type of items. Defaults to null.
     * @param string $collection The name of the collection. Defaults to "favorites".
     * @return array The collection of items.
     */
    public function get(string $userId = null, string $type = null, string $collection = 'favorites'): array;

    /**
     * Creates a new document in the collection.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $name The name of the document.
     * @param string $colType The type of the collection (default: "collections").
     * @param string|null $docType The type of the document (nullable).
     * @param array $subDir The subdirectories of the document.
     * @return array The created document.
     */
    public function createDocument(string $targetId, string $targetType, string $name, string $colType = 'collections', string $docType = null, array $subDir = []): array;

    /**
     * Updates the name of a document in a collection.
     *
     * @param string $targetId The ID of the target document.
     * @param string $targetType The type of the target document.
     * @param string $name The current name of the document.
     * @param string $newName The new name for the document.
     * @param string $colType The type of the collection. Default value is "collections".
     * @param string|null $docType The type of the document. Can be null.
     * @return array An array containing the updated collection.
     */
    public function updateCollectionNameDocument(string $targetId, string $targetType, string $name, string $newName, string $colType = 'collections', string $docType = null): array;

    /**
     * Deletes a document from the collection.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $name The name of the document.
     * @param string $colType The type of the collection. Default value is "collections".
     * @param string|null $docType The type of the document. Nullable.
     * @param string|null $subtype The subtype of the document. Nullable.
     * @return array The result of the deletion operation.
     */
    public function deleteDocument(string $targetId, string $targetType, string $name, string $colType = 'collections', string $docType = null, string $subtype = null): array;

    /**
     * Deletes an item from a collection.
     *
     * @param string $targetId The ID of the target item.
     * @param string $targetType The type of the target item.
     * @param string $fileId The ID of the file to be deleted.
     * @param string $colType The type of the collection (default: "collections").
     * @param string|null $docType The type of the document (optional).
     * @param array $nameCol The name of the collection (optional).
     * @return array The updated collection.
     */
    public function deleteFromCollection(string $targetId, string $targetType, string $fileId, string $colType = 'collections', string $docType = null, array $nameCol = []): array;

    /**
     * Adds a document to the collection.
     *
     * @param array $ids The IDs of the documents to add.
     * @param string|null $nameCol The name of the collection. Defaults to null.
     * @return array The updated collection.
     */
    public function addDocumentToColection(array $ids, string $nameCol = null): array;
}
