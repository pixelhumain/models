<?php

namespace PixelHumain\Models\Interfaces;

interface TranslateInterface
{
    public const FORMAT_SCHEMA = "schema";

    public const FORMAT_PLP = "plp";

    public const FORMAT_AS = "activityStream";

    public const FORMAT_COMMUNECTER = "communecter";

    public const FORMAT_RSS = "rss";

    public const FORMAT_KML = "kml";

    public const FORMAT_GEOJSON = "geojson";

    public const FORMAT_JSONFEED = "jsonfeed";

    public const FORMAT_GOGO = "gogocarto";

    public const FORMAT_MD = "md";

    public const FORMAT_TREE = "tree";

    public const FORMAT_FINDER = "finder";

    public const FORMAT_FTL = "ftl";

    /**
     * Converts the given data using the provided bind map.
     *
     * @param array $data The data to be converted.
     * @param array $bindMap The bind map used for conversion.
     * @return array The converted data.
     */
    public function convert(array $data, array $bindMap): array;

    /**
     * Converts a GeoJSON array using a bind map.
     *
     * @param array $data The GeoJSON array to convert.
     * @param array $bindMap The bind map to use for conversion.
     * @return array The converted GeoJSON array.
     */
    public function convert_geojson(array $data, array $bindMap): array;

    /**
     * Converts the answer data using the provided bind map.
     *
     * @param array $data The answer data to be converted.
     * @param array $bindMap The bind map used for conversion.
     * @return array The converted answer data.
     */
    public function convert_answer(array $data, array $bindMap): array;

    /**
     * Calculates the difference in days between a given date and the current date.
     *
     * @param string $date The date to calculate the difference from.
     * @param string $type The type of difference to calculate (e.g., "days", "months", "years").
     * @param string|null $timezone The timezone to use for the calculation (optional).
     * @return int|float|string The difference in days.
     */
    public function dayDifference($date, string $type, ?string $timezone = null);

    /**
     * Converts a string to a clickable link.
     *
     * @param string $str The input string to convert.
     * @return string The converted string with clickable links.
     */
    public function strToClickable(string $str): string;
}
