<?php

namespace PixelHumain\Models\Interfaces;

interface NetworkInterface
{
    public const COLLECTION = "network";

    public const CONTROLLER = "network";

    /**
     * Retrieves the network JSON data based on the provided network parameters.
     *
     * @param string|null $networkParams The network parameters.
     * @return array|null The network JSON data as an array, or null if no data is found.
     */
    public function getNetworkJson(?string $networkParams): ?array;

    /**
     * Preps the data.
     *
     * @param array $params The parameters.
     * @return array The prepped data.
     */
    public function prepData(array $params): array;

    /**
     * Retrieves a network by its ID.
     *
     * @param string $id The ID of the network.
     * @param array|null $fields The optional fields to include in the network data.
     * @return array|null The network data as an array, or null if the network is not found.
     */
    public function getById(string $id, array $fields = null): ?array;

    /**
     * Retrieves the network information for a given user ID.
     *
     * @param string $id The user ID.
     * @param array|null $fields Optional. An array of fields to retrieve. Default is null.
     * @return array The network information.
     */
    public function getNetworkByUserId(string $id, array $fields = null): array;

    /**
     * Get the list of networks by user ID.
     *
     * @param string $id The user ID.
     * @return array The list of networks.
     */
    public function getListNetworkByUserId(string $id): array;

    /**
     * Retrieves a network based on its ID and type.
     *
     * @param string $id The ID of the network.
     * @param string $type The type of the network.
     * @return array The network data.
     */
    public function getNetwork(string $id, string $type): array;

    /**
     * Executes after saving a network.
     *
     * @param array $network The network data.
     * @param string $creatorId The ID of the creator.
     * @return array The updated network data.
     */
    public function afterSave(array $network, string $creatorId): array;
}
