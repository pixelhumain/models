<?php

namespace PixelHumain\Models\Interfaces;

interface ActivityLogInterface
{
    public const COLLECTION = "ActivityLogs";

    public const CONTROLLER = "activitylog";

    public const MODULE = "activitylog";

    public const ICON = "fa-file";

    /**
     * Get an activity log by its ID.
     *
     * @param string $id The ID of the activity log.
     * @return array|null The activity log object if found, null otherwise.
     */
    public function getById(string $id): ?array;
}
