<?php

namespace PixelHumain\Models\Interfaces;

interface CronInterface
{
    public const COLLECTION = "cron";

    public const ASK_COLLECTION = "ask";

    public const TYPE_MAIL = "mail";

    public const TYPE_BADGE = "badge";

    public const STATUS_PENDING = "pending";

    public const STATUS_PROCESSED = "processed";

    public const STATUS_FAIL = "fail";

    public const STATUS_DONE = "done";

    public const STATUS_UPDATE = "update";

    public const STATUS_SEND = "send";

    public const STATUS_FUTURE = "future";

    public const EXEC_COUNT = 10;

    /**
     * Saves the cron data.
     *
     * @param array $params The cron parameters.
     * @param bool|null $update Whether to update an existing cron.
     * @return void
     */
    public function save(array $params, ?bool $update = null): void;

    /**
     * Process the mail.
     *
     * @param array $params The parameters for processing the mail.
     * @return mixed The result of the mail processing.
     */
    public function processMail(array $params);

    /**
     * Process an entry with the given parameters.
     *
     * @param array $params The parameters for processing the entry.
     * @return void
     */
    public function processEntry(array $params): void;

    /**
     * Process the cron job.
     *
     * @param int $count The number of times to process the cron job (default: 5).
     * @return void
     */
    public function processCron(int $count = 5): void;

    /**
     * Process the update to pending.
     *
     * @return void
     */
    public function processUpdateToPending(): void;

    /**
     * Retrieves the cron data based on the given conditions.
     *
     * @param array $where The conditions to filter the cron data.
     * @return array The cron data that matches the given conditions.
     */
    public function getCron(array $where = []): array;

    /**
     * Process future tasks to pending status.
     *
     * @return void
     */
    public function processFutureToPending(): void;
}
