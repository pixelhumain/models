<?php

namespace PixelHumain\Models\Interfaces;

interface CssInterface
{
    public const COLLECTION = "cssFile";

    public const CONTROLLER = "cssFile";

    /**
     * Creates or updates the CSS for a given context.
     *
     * @param string $contextId The ID of the context.
     * @param mixed $css The CSS content to be created or updated.
     * @return void
     */
    public function createORupdateCss(string $contextId, $css): void;

    /**
     * Retrieves CSS by custom ID.
     *
     * @param string $contextId The custom ID of the CSS.
     * @return array The CSS data.
     */
    public function getCssByCostumId(string $contextId): array;
}
