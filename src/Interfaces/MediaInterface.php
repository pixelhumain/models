<?php

namespace PixelHumain\Models\Interfaces;

interface MediaInterface
{
    public const COLLECTION = "media";

    //const CONTROLLER = "media";
    public const ICON = "fa-rss";

    public const COLOR = "#F9B21A";

    /**
     * Get media by ID.
     *
     * @param string $id The ID of the media.
     * @return array|null The media data or null if not found.
     */
    public function getById(string $id): ?array;
}
