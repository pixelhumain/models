<?php

namespace PixelHumain\Models\Interfaces;

interface CooperationInterface
{
    /**
     * Returns the icon for a cooperation based on the given key.
     *
     * @param string $key The key of the cooperation.
     * @return string The icon for the cooperation.
     */
    public function getIconCoop(string $key): string;

    /**
     * Returns the color of the cooperation based on the given key.
     *
     * @param string $key The key used to determine the color of the cooperation.
     * @return string The color of the cooperation.
     */
    public function getColorCoop(string $key): string;

    /**
     * Returns the color voted by the user.
     *
     * @param string $voted The vote casted by the user.
     * @return string The color voted by the user.
     */
    public function getColorVoted(string $voted): string;

    /**
     * Retrieves cooperation data based on the provided parameters.
     *
     * @param string $parentType The type of the parent entity.
     * @param string $parentId The ID of the parent entity.
     * @param string $type The type of the cooperation entity.
     * @param string|null $status The status of the cooperation entity (optional).
     * @param string|null $dataId The ID of the data associated with the cooperation entity (optional).
     * @return array The cooperation data.
     */
    public function getCoopData(string $parentType, string $parentId, string $type, string $status = null, string $dataId = null): array;

    /**
     * Check role access for a given resource.
     *
     * @param array $res The resource to check access for.
     * @return array The result of the role access check.
     */
    public function checkRoleAccess(array $res): array;

    /**
     * Check the role access in the news list.
     *
     * @param array $newsList The list of news.
     * @return array The updated news list with role access information.
     */
    public function checkRoleAccessInNews(array $newsList): array;

    /**
     * Check if a user has voted.
     *
     * @param string $userId The ID of the user.
     * @param array $obj The object to check for votes.
     * @return bool|string Returns the vote if the user has voted, false otherwise.
     */
    public function userHasVoted(string $userId, array $obj);

    /**
     * Get the count of all items.
     *
     * @param string $parentType The type of the parent.
     * @param string $parentId   The ID of the parent.
     *
     * @return array The count of all items.
     */
    public function getAllCount(string $parentType, string $parentId): array;

    /**
     * Update the status of a proposal.
     *
     * @param string $parentType The type of the parent entity.
     * @param string $parentId   The ID of the parent entity.
     *
     * @return array The updated proposal status.
     */
    public function updateStatusProposal(string $parentType, string $parentId): array;

    /**
     * Formats the date before saving.
     *
     * @param string $date The date to be formatted.
     * @return mixed The formatted date.
     */
    public function formatDateBeforeSaving(string $date);

    /**
     * Executes after saving the cooperation model.
     *
     * @param array $params The parameters passed to the method.
     * @param string $type The type of save operation (create or update).
     * @return void
     */
    public function afterSave(array $params, string $type): void;

    /**
     * Returns the count of notifications.
     *
     * @return int The count of notifications.
     */
    public function getCountNotif(): int;
}
