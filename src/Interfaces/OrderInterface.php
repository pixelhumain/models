<?php

namespace PixelHumain\Models\Interfaces;

interface OrderInterface
{
    public const COLLECTION = "orders";

    public const CONTROLLER = "order";

    /**
     * Retrieves an order by its ID.
     *
     * @param string $id The ID of the order.
     * @return array|null The order data as an array, or null if the order is not found.
     */
    public function getById(string $id): ?array;

    /**
     * Get a list of orders based on the given conditions.
     *
     * @param array $where The conditions to filter the orders.
     * @return array The list of orders that match the conditions.
     */
    public function getListBy(array $where): array;

    /**
     * Insert a new order into the database.
     *
     * @param array $order The order data.
     * @param string $userId The ID of the user placing the order.
     * @return array The inserted order data.
     */
    public function insert(array $order, string $userId): array;

    /**
     * Get a list of orders by user.
     *
     * @param array $where The conditions to filter the orders.
     * @return array The list of orders.
     */
    public function getListByUser(array $where): array;

    /**
     * Retrieves an order item by its ID.
     *
     * @param string $id The ID of the order item.
     * @return array The order item data.
     */
    public function getOrderItemById(string $id): array;

    /**
     * Retrieves the order item for invoice by user ID.
     *
     * @param string $id The user ID.
     * @return array The order item for invoice.
     */
    public function getOrderItemForInvoiceByIdUser(string $id): array;
}
