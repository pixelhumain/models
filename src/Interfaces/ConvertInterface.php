<?php

namespace PixelHumain\Models\Interfaces;

interface ConvertInterface
{
    /**
     * Retrieves the primary parameter from the given map.
     *
     * @param array $map The map containing the parameters.
     * @return array The primary parameter.
     */
    public function getPrimaryParam(array $map): array;

    /**
     * Returns the correct URL for ODS and Datanova.
     *
     * @return string The correct URL for ODS and Datanova.
     */
    public function getCorrectUrlForOdsAndDatanova(): string;

    /**
     * Converts an ODS file to PH format.
     *
     * @param string $url The URL of the ODS file.
     * @return array An array containing the converted data.
     */
    public function convertOdsToPh(string $url): array;

    /**
     * Converts the EducMembre object to a PixelHumain object.
     *
     * @param string $url The URL of the object.
     * @return array The converted object as an array.
     */
    public function convertEducMembreToPh(string $url): array;

    /**
     * Converts the given EducEcole URL to a PH URL.
     *
     * @param string $url The EducEcole URL to convert.
     * @return array The converted PH URL.
     */
    public function convertEducEcoleToPh(string $url): array;

    /**
     * Converts the EducEtab data to PixelHumain format.
     *
     * @param string $url The URL of the EducEtab data.
     * @return array The converted data in PixelHumain format.
     */
    public function convertEducEtabToPh(string $url): array;

    /**
     * Converts the educational structure to the PH format.
     *
     * @param string $url The URL of the educational structure.
     * @return array The converted educational structure in the PH format.
     */
    public function convertEducStructToPh(string $url): array;

    /**
     * Converts Datanova data to PH format.
     *
     * @param string $url The URL of the Datanova data.
     * @return array The converted data in PH format.
     */
    public function convertDatanovaToPh(string $url): array;

    /**
     * Converts an OSM file to PH format.
     *
     * @param string $url The URL of the OSM file.
     * @return array The converted data in PH format.
     */
    public function convertOsmToPh(string $url): array;

    /**
     * Converts the data from Datagouv to Pixelhumain format.
     *
     * @param string $url The URL of the data to be converted.
     * @return array The converted data in Pixelhumain format.
     */
    public function convertDatagouvToPh(string $url): array;

    /**
     * Converts a Wiki page to a PH page.
     *
     * @param string $url The URL of the Wiki page.
     * @param string|null $text_filter The text filter to apply during the conversion.
     * @return array The converted PH page.
     */
    public function convertWikiToPh(string $url, ?string $text_filter): array;

    /**
     * Convert class.
     *
     * This class provides methods for converting data.
     */
    public function poleEmploi(string $url): array;

    /**
     * Retrieves the token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @return string|null The token for the API, or null if not found.
     */
    public function getToken(string $apiName): ?string;

    /**
     * ConvertValueFlowsToPh method converts value flows to PH.
     *
     * @param string $url The URL to convert.
     * @return array The converted value flows.
     */
    public function ConvertValueFlowsToPh(string $url): array;

    /**
     * Converts the Organcity URL to PH format.
     *
     * @param string $url The Organcity URL to convert.
     * @return array The converted URL in PH format.
     */
    public function ConvertOrgancityToPh(string $url): array;

    /**
     * Retrieves the latitude and longitude of a Wikidata item.
     *
     * @param string $wikidata_item The Wikidata item to retrieve the latitude and longitude from.
     * @return array An array containing the latitude and longitude values.
     */
    public function getLatLongWikidataItem($wikidata_item): array;

    /**
     * Converts a GogoCarto URL to an array.
     *
     * @param string $url The GogoCarto URL to convert.
     * @param mixed $text_filter The text filter to apply.
     * @return array The converted array.
     */
    public function convertGogoCarto(string $url, $text_filter): array;

    /**
     * Converts the given FTL file to an array.
     *
     * @param string $url The URL of the FTL file.
     * @param mixed $text_filter The text filter to apply during the conversion.
     * @return array The converted FTL file as an array.
     */
    public function convertFtl(string $url, $text_filter): array;

    /**
     * Converts WikiMedia data to PH format.
     *
     * @param array $data The WikiMedia data to convert.
     * @param string $categorie The category of the data.
     * @param string $wikiName The name of the Wiki.
     * @return array The converted data in PH format.
     */
    public function convertWikiMediaToPh(array $data, string $categorie, string $wikiName): array;

    /**
     * Converts OpenAgenda data to Ph format.
     *
     * @param array $parameters The parameters for the conversion.
     * @return array The converted data in Ph format.
     */
    public function ConvertOpenAgendaToPh(array $parameters = []): array;

    /**
     * ConvertOpenAgendaEventToPh converts an OpenAgenda event to a PixelHumain event.
     *
     * @param string $agenda The agenda to convert.
     * @param string $key The key to use for the conversion.
     * @return string The converted PixelHumain event.
     */
    public function ConvertOpenAgendaEventToPh(string $agenda, string $key): string;

    /**
     * Converts an iCalendar string to a Ph string.
     *
     * @param string $http The iCalendar string to convert.
     * @return string The converted Ph string.
     */
    public function ConvertICalendarToPh(string $http): string;
}
