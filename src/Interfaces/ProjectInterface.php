<?php

namespace PixelHumain\Models\Interfaces;

interface ProjectInterface
{
    public const COLLECTION = "projects";

    public const CONTROLLER = "project";

    public const ICON = "fa-lightbulb-o";

    public const COLOR = "#8C5AA1";

    /**
     * Retrieves a project by its ID.
     *
     * @param string $id The ID of the project.
     * @return array The project details.
     */
    public function getById(string $id): array;

    /**
     * Returns the data binding for the project.
     *
     * @return array The data binding.
     */
    public function getDataBinding(): array;

    /**
     * Retrieves a project by an array of IDs.
     *
     * @param array $arrayId The array of IDs to search for.
     * @param array $fields The optional fields to retrieve for each project.
     * @param bool $simply Whether to return a simplified version of the project.
     * @return array The array of projects matching the given IDs.
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array;

    /**
     * Retrieves a simple project by its ID.
     *
     * @param string $id The ID of the project.
     * @param array|null $project Optional project data to be passed.
     * @return array The simple project data.
     */
    public function getSimpleProjectById(string $id, ?array $project = null): array;

    /**
     * Retrieves records from the database based on the specified parameters.
     *
     * @param array $params The parameters used to filter the records.
     * @return array The retrieved records.
     */
    public function getWhere(array $params): array;

    /**
     * Retrieves the public data of a project.
     *
     * @param string $id The ID of the project.
     * @return array The public data of the project.
     */
    public function getPublicData(string $id): array;

    /**
     * Format the date for rendering.
     *
     * @param array $project The project data.
     * @return array The formatted project data.
     */
    public function formatDateRender(array $project): array;
}
