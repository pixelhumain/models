<?php

namespace PixelHumain\Models\Interfaces;

interface RoomInterface
{
    public const COLLECTION = "rooms";

    public const CONTROLLER = "room";

    public const TYPE_SURVEY = "survey"; //sondage à la Google Forms

    public const TYPE_DISCUSS = "discuss"; // systeme de discussioin voir avec dialoguea

    public const TYPE_FRAMAPAD = "framapad"; // systeme de discussioin voir avec dialoguea

    public const TYPE_BRAINSTORM = "proposals"; //systeme de rpopositions pour prendre des décision

    public const TYPE_VOTE = "vote"; //vote

    public const TYPE_ENTRY = "entry"; //vote

    public const TYPE_DISTRIBUTE = "distribute"; //vote par distribution sur des proposition

    public const STATE_ARCHIVED = "archived";

    public const TYPE_ACTIONS = "actions"; //things to do

    public const TYPE_ACTION = "action"; //things to do

    public const COLLECTION_ACTIONS = "actions";

    public const ACTIONS_PARENT = "rooms";

    //ACTION STATES
    public const ACTION_TODO = "todo";

    public const ACTION_INPROGRESS = "inprogress";

    public const ACTION_LATE = "late";

    public const ACTION_CLOSED = "closed";

    /**
     * Retrieves a room by its ID.
     *
     * @param string $id The ID of the room.
     * @return array|null The room data as an array, or null if the room is not found.
     */
    public function getById(string $id): ?array;

    /**
     * Retrieves the action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data or null if not found.
     */
    public function getActionById(string $id): ?array;

    /**
     * Retrieves records from the database based on specified conditions, sorts them, and limits the result set.
     *
     * @param array $params The conditions to filter the records.
     * @param array $sort The sorting criteria for the records.
     * @param int $limit The maximum number of records to retrieve (default is 1).
     * @return array The retrieved records.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array;

    /**
     * Retrieves a single action room by its parent organization.
     *
     * @param string $idOrga The ID of the parent organization.
     * @return array|null The single action room if found, null otherwise.
     */
    public function getSingleActionRoomByOrgaParent(string $idOrga): ?array;

    /**
     * Check if a user can participate in the room.
     *
     * @param string $userId The ID of the user.
     * @param string|null $id The ID of the room (optional).
     * @param string|null $type The type of the room (optional).
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, string $id = null, string $type = null): bool;

    /**
     * Check if a user is a moderator in the room.
     *
     * @param string|null $userId The ID of the user to check.
     * @param string $key The key to access the room.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $key): bool;

    /**
     * Check if a user can administrate a room.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the room.
     * @return bool Returns true if the user can administrate the room, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool;

    /**
     * Get the access level for a given room based on the user's roles.
     *
     * @param array $room The room details.
     * @param array|null $myRoles The user's roles.
     * @return string The access level for the room.
     */
    public function getAccessByRole(array $room, ?array $myRoles): string;

    /**
     * Insert a new room into the database.
     *
     * @param array $parentRoom The parent room data.
     * @param string $type The type of the room.
     * @param mixed $copyOf The room to copy from (optional).
     * @return mixed The new room data.
     */
    public function insert(array $parentRoom, string $type, $copyOf = null);

    /**
     * Deletes an action room.
     *
     * @param string $id The ID of the room to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function deleteActionRoom(string $id, string $userId): array;

    /**
     * Deletes an action room element.
     *
     * @param string $elementId The ID of the element to delete.
     * @param string $elementType The type of the element to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @param bool $forced Whether the deletion should be forced.
     * @return array The result of the deletion operation.
     */
    public function deleteElementActionRooms(string $elementId, string $elementType, string $userId, bool $forced = false): array;

    /**
     * Closes the action.
     *
     * @param array $params The parameters for the action.
     * @return array The result of the action.
     */
    public function closeAction(array $params): array;

    /**
     * Assigns the current room to the user.
     *
     * @param array $params The parameters for the assignment.
     * @return array The updated room details.
     */
    public function assignMe(array $params): array;

    /**
     * Retrieves all rooms by type and ID.
     *
     * @param string $type The type of room.
     * @param string $id The ID of the room.
     * @param bool|null $archived Whether to include archived rooms. Defaults to null.
     * @return array An array of rooms.
     */
    public function getAllRoomsByTypeId(string $type, string $id, bool $archived = null): array;

    /**
     * Retrieves all rooms activity by type and ID.
     *
     * @param string $type The type of the room.
     * @param string $id The ID of the room.
     * @param bool|null $archived (optional) Whether to include archived rooms. Defaults to null.
     * @return array An array containing the rooms activity.
     */
    public function getAllRoomsActivityByTypeId(string $type, string $id, bool $archived = null): array;

    /**
     * Retrieve the action rooms for a specific element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @return array The array of action rooms.
     */
    public function getElementActionRooms(string $elementId, string $elementType): array;
}
