<?php

namespace PixelHumain\Models\Interfaces;

interface BookmarkInterface
{
    public const COLLECTION = "bookmarks";

    public const CONTROLLER = "bookmark";
}
