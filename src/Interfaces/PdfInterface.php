<?php

namespace PixelHumain\Models\Interfaces;

interface PdfInterface
{
    /**
     * Creates a PDF using the given parameters.
     *
     * @param array $params The parameters for creating the PDF.
     * @return void
     */
    public function createPdf(array $params): void;

    /**
     * Generates a PDF document for a dossier.
     *
     * @param array $params The parameters for generating the PDF.
     * @return string The generated PDF document.
     */
    public function cteDossier(array $params): string;
}
