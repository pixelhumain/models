<?php

namespace PixelHumain\Models\Interfaces;

interface ActionRoomInterface
{
    public const COLLECTION = "rooms";

    public const CONTROLLER = "rooms";

    public const TYPE_SURVEY = "survey"; //sondage à la Google Forms

    public const TYPE_DISCUSS = "discuss"; // systeme de discussioin voir avec dialoguea

    public const TYPE_FRAMAPAD = "framapad"; // systeme de discussioin voir avec dialoguea

    public const TYPE_BRAINSTORM = "proposals"; //systeme de rpopositions pour prendre des décision

    public const TYPE_VOTE = "vote"; //vote

    public const TYPE_ENTRY = "entry"; //vote

    public const TYPE_DISTRIBUTE = "distribute"; //vote par distribution sur des proposition

    public const STATE_ARCHIVED = "archived";

    public const TYPE_ACTIONS = "actions"; //things to do

    public const TYPE_ACTION = "action"; //things to do

    public const COLLECTION_ACTIONS = "actions";

    public const ACTIONS_PARENT = "rooms";

    //ACTION STATES
    public const ACTION_TODO = "todo";

    public const ACTION_INPROGRESS = "inprogress";

    public const ACTION_LATE = "late";

    public const ACTION_CLOSED = "closed";

    /**
     * Retrieves an ActionRoom by its ID.
     *
     * @param string $id The ID of the ActionRoom.
     * @return array|null The ActionRoom data as an array or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data if found, null otherwise.
     */
    public function getActionById(string $id): ?array;

    /**
     * Retrieves records from the ActionRoom table based on the provided parameters, sorting, and limit.
     *
     * @param array $params The parameters to filter the records.
     * @param array $sort The sorting criteria for the records.
     * @param int $limit The maximum number of records to retrieve. Default is 1.
     * @return array The retrieved records.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array;

    /**
     * Retrieves a single action room by its parent organization.
     *
     * @param string $idOrga The ID of the parent organization.
     * @return array|null The action room data as an array or null if not found.
     */
    public function getSingleActionRoomByOrgaParent(string $idOrga): ?array;

    /**
     * Check if a user can participate in an action room.
     *
     * @param string $userId The ID of the user.
     * @param string|null $id The ID of the action room (optional).
     * @param string|null $type The type of the action room (optional).
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, string $id = null, string $type = null): bool;

    /**
     * Check if a user is a moderator in a specific app.
     *
     * @param string|null $userId The user ID.
     * @param string $app The app name.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $app): bool;

    /**
     * Check if a user can administrate a specific action room.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the action room.
     * @return bool Returns true if the user can administrate the action room, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool;

    /**
     * Insert a new action room.
     *
     * @param array $parentRoom The parent room.
     * @param array $type The type of the action room.
     * @param mixed $copyOf The room to copy from (optional).
     * @return mixed The new action room data.
     */
    public function insert(array $parentRoom, array $type, $copyOf = null);

    /**
     * Deletes the action rooms associated with a specific element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user performing the action.
     * @return array An array containing the deleted action rooms.
     */
    public function deleteElementActionRooms(string $elementId, string $elementType, string $userId): array;

    /**
     * Closes the action.
     *
     * @param array $params The parameters for closing the action.
     * @return array The updated array after closing the action.
     */
    public function closeAction(array $params): array;

    /**
     * Assigns the current user to the action room.
     *
     * @param array $params The parameters for the assignment.
     * @return array The updated action room details.
     */
    public function assignMe(array $params): array;

    /**
     * Assigns people to the action room.
     *
     * @param array $params The parameters for assigning people.
     * @return array The updated array after assigning people.
     */
    public function assignPeople(array $params): array;

    /**
     * Récupère toutes les salles en fonction du type et de l'identifiant.
     *
     * @param string $type Le type de la salle.
     * @param string $id L'identifiant de la salle.
     * @param bool|null $archived (optionnel) Indique si les salles archivées doivent être incluses.
     * @return array Les salles correspondantes.
     */
    public function getAllRoomsByTypeId(string $type, string $id, bool $archived = null): array;

    /**
     * Récupère toutes les activités des salles par type et identifiant.
     *
     * @param string $type Le type de salle.
     * @param string $id L'identifiant de la salle.
     * @param bool|null $archived (optionnel) Indique si les activités archivées doivent être incluses.
     * @return array Les activités des salles correspondantes.
     */
    public function getAllRoomsActivityByTypeId(string $type, string $id, bool $archived = null): array;

    /**
     * Récupère les ActionRooms d'un élément spécifié.
     *
     * @param string $elementId L'identifiant de l'élément.
     * @param string $elementType Le type de l'élément.
     * @return array Les ActionRooms de l'élément.
     */
    public function getElementActionRooms(string $elementId, string $elementType): array;
}
