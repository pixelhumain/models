<?php

namespace PixelHumain\Models\Interfaces;

interface CO2StatInterface
{
    public const HASH_DOMAINS = [
        "co2-login", "co2-web", "co2-websearch", "co2-referencement",
        "co2-page", "co2-search", "co2-dda", "co2-live",
        "co2-dashboard", "co2-info", "co2-annonces", "co2-agenda", "co2-welcome",
    ];

    public const COLLECTION = 'co2stats';

    /**
     * Increases the number of loads for a given hash.
     *
     * @param string $hash The hash value.
     * @return array The stats.
     */
    public function incNbLoad(string $hash): array;

    /**
     * Retrieves the statistics by hash.
     *
     * @param string|null $week The week to filter the statistics (optional).
     * @param string|null $hash The hash to filter the statistics (optional).
     * @return array The array containing the statistics.
     */
    public function getStatsByHash(?string $week = null, ?string $hash = null): array;

    /**
     * Retrieves all custom statistics.
     *
     * @return array An array containing the custom statistics.
     */
    public function getStatsCustomAll(): array;
}
