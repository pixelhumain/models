<?php

namespace PixelHumain\Models\Interfaces;

interface ApplicationInterface
{
    public const COLLECTION = "applications";

    public const ICON = "fa-th";

    /**
     * Loads the database application configuration.
     *
     * @param string|null $key The key of the configuration to load. If null, all configurations will be loaded.
     * @return void
     */
    public function loadDBAppConfig(?string $key = null): void;

    /**
     * Retrieves the token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @return array|null The token for the specified API, or null if it doesn't exist.
     */
    public function getToken(string $apiName): ?array;

    /**
     * Saves a token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @param array $token The token to be saved.
     * @return array The updated token.
     */
    public function saveToken(string $apiName, array $token): array;
}
