<?php

namespace PixelHumain\Models\Interfaces;

interface CsvInterface
{
    /**
     * Retrieves the CSV data for the admin.
     *
     * @param array $post The post data.
     * @return array The CSV data for the admin.
     */
    public function getCsvAdmin(array $post): array;

    /**
     * Parse the fields of a CSV row.
     *
     * @param array $elt The CSV row to parse.
     * @return array The parsed fields.
     */
    public function parseFields(array $elt): array;

    /**
     * Parse the fields of an element based on the given roles.
     *
     * @param array $elt The element to parse.
     * @param array $roles The roles to use for parsing.
     * @return array The parsed fields.
     */
    public function parseFieldsRoles(array $elt, array $roles): array;
}
