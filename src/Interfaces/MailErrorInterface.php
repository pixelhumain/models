<?php

namespace PixelHumain\Models\Interfaces;

interface MailErrorInterface
{
    public const COLLECTION = "mailerror";

    public const EVENT_DROPPED_EMAIL = "dropped";

    public const EVENT_SPAM_COMPLAINTS = "complained";

    public const EVENT_BOUNCED_EMAIL = "bounced";

    /**
     * Perform an action on the event.
     *
     * @return void
     */
    public function actionOnEvent(): void;

    /**
     * Saves the MailError object.
     *
     * @return void
     */
    public function save(): void;

    /**
     * Returns an array of mail errors since a given timestamp.
     *
     * @param int $sinceTS The timestamp to retrieve mail errors since.
     * @return array An array of mail errors.
     */
    public function getMailErrorSince(int $sinceTS): array;
}
