<?php

namespace PixelHumain\Models\Interfaces;

interface MicroFormatsInterface
{
    public const COLLECTION = "microformats";

    /**
     * Retrieves records from the database based on the specified parameters.
     *
     * @param array $params The parameters used to filter the records.
     * @param array|null $fields The fields to be retrieved from the records. If null, all fields will be retrieved.
     * @return array The retrieved records.
     */
    public function getWhere(array $params, ?array $fields = null): array;
}
