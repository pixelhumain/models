<?php

namespace PixelHumain\Models\Interfaces;

interface GamificationInterface
{
    public const COLLECTION = "gamification";

    //platefrom actions
    public const POINTS_USER_LOGIN = 5;

    public const POINTS_USER_REGISTRATION = 15;

    //const POINTS_SEARCH = 1;
    public const POINTS_ADD_POST = 0.5;

    public const POINTS_ANSWER_POST = 0.1;

    public const POINTS_POST_LIKED = 0.1;

    //links
    public const POINTS_LINK_USER_2_USER = 1;

    public const POINTS_LINK_USER_2_ORGANIZATION = 1;

    public const POINTS_LINK_USER_2_EVENT = 0.5;

    public const POINTS_LINK_USER_2_PROJECT = 0.5;

    //creation
    public const POINTS_CREATE = 10;

    //person activity
    //points if profile is filled above 80%
    //presenting a MOAC

    //organization activity
    public const POINTS_ORG_ADD_MEMBER = 5;
    //points if profile is filled above 80%

    //event activity
    public const POINTS_EVENT_INVITE_ATTENDEES = 5;

    //project activity
    public const POINTS_PROJECT_ADDING_BULLET_POINTS = 5;

    public const POINTS_PROJECT_ADD_CONTRIBUTOR = 5;

    public const BADGE_SEED_LIMIT = 50;

    public const BADGE_GERM_LIMIT = 100;

    public const BADGE_PLANT_LIMIT = 200;

    public const BADGE_TREE_LIMIT = 300;

    public const BADGE_FOREST_LIMIT = 500;

    public const BADGE_COUNTRY_LIMIT = 5000;

    public const BADGE_PLANET_LIMIT = 20000;

    public const BADGE_SUN_LIMIT = 100000;

    public const BADGE_SYSTEM_LIMIT = 200000;

    public const BADGE_GALAXY_LIMIT = 300000;
    //points if profile is filled above 80%

    //actions (like/dislike/abuse)
    public const POINTS_VOTEUP = 0.1;

    public const POINTS_VOTEDOWN = 0.1;

    public const POINTS_REPORTABUSE = 0.2;

    //Moderate
    public const POINTS_MODERATE = 3;

    /**
     * Calculates the points for a given element type and user.
     *
     * @param string $eltype The element type.
     * @param string $userId The user ID.
     * @param string|null $filter The filter to apply (optional).
     * @return int The calculated points.
     */
    public function calcPoints(string $eltype, string $userId, ?string $filter = null): int;

    /**
     * Inserts a gamification user into the system.
     *
     * @param string $userId The ID of the user to insert.
     * @return void
     */
    public function insertGamificationUser(string $userId): void;

    /**
     * Calculates the points for a specific user.
     *
     * @param string $userId The ID of the user.
     * @return array The calculated points for the user.
     */
    public function calcPointsAction(string $userId): array;

    // TODO : $filter is not used
    /**
     * Consolidates the points for a given user.
     *
     * @param string $userId The ID of the user.
     * @param mixed $filter (Optional) The filter to apply when consolidating points.
     * @return array The consolidated points for the user.
     */
    public function consolidatePoints(string $userId, $filter = null): array;

    /**
     * Récupère tous les niveaux et le niveau actuel d'un utilisateur.
     *
     * @param int $total Le nombre total de niveaux.
     * @param string $userId L'identifiant de l'utilisateur.
     * @param string $path Le chemin du fichier.
     * @return array Les niveaux et le niveau actuel de l'utilisateur.
     */
    public function getAllLevelAndCurent(int $total, string $userId, string $path): array;

    /**
     * Assigns a badge to a user.
     *
     * @param string $userId The ID of the user.
     * @param int|null $total The total number of badges assigned to the user (optional).
     * @param string|null $iconPath The path to the badge icon (optional).
     * @return string The assigned badge.
     */
    public function badge(string $userId, ?int $total = null, ?string $iconPath = null): string;

    /**
     * Updates the gamification data.
     *
     * @param array $params The parameters for the update.
     * @return void
     */
    public function update(array $params): void;

    /**
     * Saves the given entry.
     *
     * @param array $entry The entry to be saved.
     * @return void
     */
    public function save(array $entry): void;

    /**
     * Updates a user.
     *
     * @param string $id The ID of the user to update.
     * @return void
     */
    public function updateUser(string $id): void;

    /**
     * Increments the user's gamification score based on the given action.
     *
     * @param string|null $id The ID of the user.
     * @param string|null $action The action performed by the user.
     * @return void
     */
    public function incrementUser(?string $id, ?string $action): void;
}
