<?php

namespace PixelHumain\Models\Interfaces;

interface RequestInterface
{
    public function getCookies();
}
