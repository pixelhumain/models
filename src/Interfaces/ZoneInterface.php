<?php

namespace PixelHumain\Models\Interfaces;

interface ZoneInterface
{
    public const COLLECTION = "zones";

    public const CONTROLLER = "zone";

    public const TRANSLATE = "translate";

    public const COLOR = "#E6304C";

    public const ICON = "fa-university";

    /**
     * Get a zone by its ID.
     *
     * @param string $id The ID of the zone.
     * @param array $fields The optional fields to include in the result.
     * @return array|null The zone data as an array, or null if not found.
     */
    public function getById(string $id, array $fields = []): ?array;

    /**
     * Retrieves a zone by its key.
     *
     * @param string $key The key of the zone.
     * @return array|null The zone data if found, null otherwise.
     */
    public function getByKey(string $key): ?array;

    /**
     * Get the zones by EPCI ID.
     *
     * @param string $id The EPCI ID.
     * @param array $fields The optional fields to retrieve.
     * @return array|null The array of zones or null if not found.
     */
    public function getByEpci(string $id, array $fields = []): ?array;

    /**
     * Retrieves the translation by ID and type.
     *
     * @param string $id The ID of the translation.
     * @param string $type The type of the translation.
     * @return array|null The translated data as an array or null if not found.
     */
    public function getTranslateById(string $id, string $type): ?array;

    /**
     * Retrieves the zone and its translation by ID.
     *
     * @param string $id The ID of the zone.
     * @return array|null The zone and its translation as an array, or null if not found.
     */
    public function getZoneAndTranslateById(string $id): ?array;

    /**
     * Get the records from the database based on the given parameters.
     *
     * @param array $params The parameters to filter the records.
     * @param array|null $fields The fields to retrieve from the records. If null, all fields will be retrieved.
     * @param int $limit The maximum number of records to retrieve. Default is 20.
     * @return array The retrieved records.
     */
    public function getWhere(array $params, array $fields = null, int $limit = 20): array;

    /**
     * Retrieves the details of a zone by its ID.
     *
     * @param string $id The ID of the zone.
     * @return array|null The details of the zone as an array, or null if the zone is not found.
     */
    public function getDetailById(string $id): ?array;

    /**
     * Retrieves the level ID by its ID.
     *
     * @param string $id The ID of the level.
     * @param array|null $zone The zone array (optional).
     * @param string|null $type The type of the zone (optional).
     * @return array The level ID.
     */
    public function getLevelIdById(string $id, array $zone = null, string $type = null): array;

    /**
     * Create a new level.
     *
     * @param string      $name         The name of the level.
     * @param string      $countryCode  The country code.
     * @param string      $level        The level.
     * @param string|null $level2       Optional second level.
     * @param string|null $level3       Optional third level.
     *
     * @return array The created level.
     */
    public function createLevel(string $name, string $countryCode, string $level, string $level2 = null, string $level3 = null): array;

    /**
     * Save the zone.
     *
     * @param array $zone The zone data.
     * @return array The saved zone data.
     */
    public function save(array $zone = []): array;

    /**
     * Crée une clé à partir d'un tableau de zone.
     *
     * @param array $zone Le tableau de zone.
     * @return string La clé créée.
     */
    public function createKey(array $zone): string;

    /**
     * Retrieves the country based on the country code.
     *
     * @param string|array $countryCode The country code.
     * @return array|null The country name or null if not found.
     */
    public function getCountryByCountryCode($countryCode): ?array;

    /**
     * Get the ID of a country by its country code.
     *
     * @param string|array $countryCode The country code.
     * @return string|null The ID of the country, or null if not found.
     */
    public function getIdCountryByCountryCode($countryCode): ?string;

    /**
     * Retrieves the level of a zone by its name and country code.
     *
     * @param string $name The name of the zone.
     * @param string $level The level of the zone.
     * @param string|array $countryCode The country code of the zone.
     * @return array|null The zone or null if not found.
     */
    public function getLevelByNameAndCountry(string $name, string $level, $countryCode): ?array;

    /**
     * Get the ID level by name and country code.
     *
     * @param string $name The name of the zone.
     * @param string $level The level of the zone.
     * @param string|null $countryCode The country code.
     * @return string|null The ID level if found, null otherwise.
     */
    public function getIdLevelByNameAndCountry(string $name, string $level, $countryCode): ?string;

    /**
     * Inserts a translated zone into the database.
     *
     * @param string $parentId The ID of the parent zone.
     * @param string $parentType The type of the parent zone.
     * @param string $countryCode The country code of the translated zone.
     * @param mixed $origin The origin of the translated zone.
     * @param string|null $osmID The OSM ID of the translated zone (optional).
     * @param string|null $wikidataID The Wikidata ID of the translated zone (optional).
     * @return array The inserted translated zone.
     */
    public function insertTranslate(string $parentId, string $parentType, string $countryCode, $origin, string $osmID = null, string $wikidataID = null): array;

    /**
     * Returns the name of the country for the given ID.
     *
     * @param string $id The ID of the country.
     * @return string The name of the country.
     */
    public function getNameCountry(string $id): string;

    /**
     * Returns the name origin for a given ID.
     *
     * @param string $id The ID of the zone.
     * @return string The name origin.
     */
    public function getNameOrigin(string $id): string;

    /**
     * Get the list of countries.
     *
     * @param bool|null $hasCity Whether to include only countries with cities.
     * @return array The list of countries.
     */
    public function getListCountry(bool $hasCity = null): array;

    /**
     * Custom sort function for comparing two values.
     *
     * @param mixed $a The first value to compare.
     * @param mixed $b The second value to compare.
     * @return int Returns a negative integer if $a is less than $b, a positive integer if $a is greater than $b, or 0 if they are equal.
     */
    public function custom_sort($a, $b): int;

    /**
     * Removes accents from a string.
     *
     * @param string $string The string to remove accents from.
     * @return string The string without accents.
     */
    public function stripAccents(string $string);

    /**
     * Retrieves the records from the database based on the provided parameters.
     *
     * @param array $params The parameters to filter the records.
     * @param array|null $fields The fields to include in the result. If null, all fields will be included.
     * @param int $limit The maximum number of records to retrieve. If 0, all records will be retrieved.
     * @return array The retrieved records.
     */
    public function getWhereTranlate(array $params, array $fields = null, int $limit = 0): array;

    /**
     * Retrieves the scope by its IDs.
     *
     * @param array $params The parameters for retrieving the scope.
     * @return array The scope retrieved by its IDs.
     */
    public function getScopeByIds(array $params): array;

    /**
     * Crée une portée à partir des données de la zone.
     *
     * @param array $zone Les données de la zone.
     * @param string $type Le type de la portée.
     * @param string|null $cp Le code postal de la portée (optionnel).
     * @return array La portée créée.
     */
    public function createScope(array $zone, string $type, string $cp = null): array;

    /**
     * Retrieves the level of a zone.
     *
     * @param array $zone The zone data.
     * @return string|null The level of the zone, or null if not found.
     */
    public function getLevel(array $zone): ?string;
}
