<?php

namespace PixelHumain\Models\Interfaces;

interface PreferenceInterface
{
    /**
     * Clears the preferences for a specific user based on the given element and element type.
     *
     * @param array $element The element to clear preferences for.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @return array The updated preferences after clearing.
     */
    public function clearByPreference(array $element, string $elementType, string $userId): array;

    /**
     * Initializes the preferences for a given type.
     *
     * @param string $type The type of preferences to initialize.
     * @return array The initialized preferences.
     */
    public function initPreferences(string $type): array;

    /**
     * Check if the edition is open based on the given preferences.
     *
     * @param array|null $preferences The preferences to check.
     * @return bool Returns true if the edition is open, false otherwise.
     */
    public function isOpenEdition(?array $preferences): bool;
}
