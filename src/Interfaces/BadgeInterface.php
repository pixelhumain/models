<?php

namespace PixelHumain\Models\Interfaces;

interface BadgeInterface
{
    public const COLLECTION = "badges";

    public const CONTEXT = "https://w3id.org/openbadges/v2";
}
