<?php

namespace PixelHumain\Models\Interfaces;

interface SlugInterface
{
    public const COLLECTION = 'slugs';

    /**
     * Check and create a slug for the given string.
     *
     * @param string $str The string to create a slug from.
     * @return string The generated slug.
     */
    public function checkAndCreateSlug(string $str): string;

    /**
     * Check if a slug is valid for a given type and ID.
     *
     * @param string $slug The slug to check.
     * @param string|null $type The type of the entity associated with the slug (optional).
     * @param string|null $id The ID of the entity associated with the slug (optional).
     * @return bool Returns true if the slug is valid for the given type and ID, false otherwise.
     */
    public function check(string $slug, ?string $type = null, ?string $id = null): bool;

    /**
     * Saves the slug for a specific type and ID.
     *
     * @param string $type The type of the entity.
     * @param string $id The ID of the entity.
     * @param string $slug The slug to be saved.
     * @return bool Returns true.
     */
    public function save(string $type, string $id, string $slug): bool;

    /**
     * Removes a slug by its parent ID and type.
     *
     * @param string $id The ID of the parent entity.
     * @param string $type The type of the parent entity.
     * @return void
     */
    public function removeByParentIdAndType(string $id, string $type): void;
}
