<?php

namespace PixelHumain\Models\Interfaces;

use DateTime;
use MongoDate;

interface DataValidatorInterface
{
    /**
     * Clears the user input by removing any unwanted or potentially harmful data.
     *
     * @param mixed $data The user input data to be cleared.
     * @return array The cleared user input data.
     */
    public function clearUserInput($data);

    /**
     * Validates an email address.
     *
     * @param string|null $toValidate The email address to validate.
     * @param mixed $object (optional) Currently unused and kept for compatibility reasons.
     * @param mixed $objectId (optional) Currently unused and kept for compatibility reasons.
     * @return string The validation result message.
     */
    public function email(?string $toValidate, $object = null, $objectId = null): string;

    /**
     * Retrieves the field name from the collection and performs validation on the field value.
     *
     * @param array $dataBinding The collection of data bindings.
     * @param string $fieldName The name of the field to retrieve and validate.
     * @param mixed $fieldValue The value of the field to validate.
     * @param mixed $object The object associated with the field (optional).
     * @param bool|null $import Indicates if the validation is for import (optional).
     * @param string|null $id The ID of the field (optional).
     * @return string The name of the field.
     */
    public function getCollectionFieldNameAndValidate(array $dataBinding, string $fieldName, $fieldValue, $object = null, ?bool $import = null, ?string $id = null): string;

    /**
     * Validates the address data.
     *
     * @param array $toValidate The address data to validate.
     * @return string The validation result.
     */
    public function addressValid(array $toValidate): string;

    /**
     * Validates the geo data.
     *
     * @param array $toValidate The data to be validated.
     * @return string The validation result.
     */
    public function geoValid(array $toValidate): string;

    /**
     * Validates the geo position data.
     *
     * @param array $toValidate The data to validate.
     * @return string The validation result.
     */
    public function geoPositionValid(array $toValidate): string;

    /**
     * Converts a string representation of a date and time into a DateTime object.
     *
     * @param string|MongoDate $myDate The string representation of the date and time value or a MongoDate object.
     * @param string $label The label for the date and time value.
     * @return DateTime|MongoDate The DateTime object or the MongoDate object.
     */
    public function getDateTimeFromString($myDate, string $label);
}
