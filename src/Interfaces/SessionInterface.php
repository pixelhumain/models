<?php

namespace PixelHumain\Models\Interfaces;

use ArrayAccess;

interface SessionInterface extends ArrayAccess
{
    /**
     * Retrieves the value of a session variable.
     *
     * @param string $name The key of the session variable.
     * @return mixed The value of the session variable.
     */
    public function __get($name);

    /**
     * Sets the value of a session variable.
     *
     * @param string $name The key of the session variable.
     * @param mixed $value The value to be set.
     * @return void
     */
    public function __set($name, $value);

    /**
     * Check if a session variable is set.
     *
     * @param string $name The name of the session variable.
     * @return bool Returns true if the session variable is set, false otherwise.
     */
    public function __isset($name);

    /**
     * Unsets a session variable by key.
     *
     * @param string $name The key of the session variable to unset.
     * @return void
     */
    public function __unset($name);

    /**
     * Retrieves the value of a session variable.
     *
     * @param string $name The key of the session variable.
     * @return mixed The value of the session variable.
     */
    public function get($name);

    /**
     * Sets the value of a session variable.
     *
     * @param string $name The key of the session variable.
     * @param mixed $value The value to be set.
     * @return void
     */
    public function set($name, $value);

    /**
     * Check if a session key exists.
     *
     * @param string $key The key to check.
     * @return bool True if the key exists, false otherwise.
     */
    public function has($key);

    /**
     * Removes a value from the session using the specified key.
     *
     * @param string $name The key of the value to remove.
     * @return void
     */
    public function remove($name);

    /**
     * Removes all data from the session.
     *
     * @return void
     */
    public function removeAll();
}
