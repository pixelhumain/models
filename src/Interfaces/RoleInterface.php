<?php

namespace PixelHumain\Models\Interfaces;

interface RoleInterface
{
    public const ADD_BETA_TESTER = "addBetaTester";

    public const REVOKE_BETA_TESTER = "revokeBetaTester";

    public const ADD_SUPER_ADMIN = "addSuperAdmin";

    public const REVOKE_SUPER_ADMIN = "revokeSuperAdmin";

    public const ADD_BANNED_USER = "addBannedUser";

    public const REVOKE_BANNED_USER = "revokeBannedUser";

    public const DEVELOPER = "developer";

    public const SUPERADMIN = "superAdmin";

    public const SUPERADMINCMS = "superAdminCms";

    public const SOURCEADMIN = "sourceAdmin";

    public const COEDITOR = "coEditor";

    /**
     * Check if a user can login.
     *
     * @param array $person The person object.
     * @param bool $isRegisterProcess Whether the login is part of the registration process.
     * @return array The result of the login check.
     */
    public function canUserLogin(array $person, bool $isRegisterProcess = false): array;

    /**
     * Check if the user has the super admin role.
     *
     * @param array|null $roles The roles of the user.
     * @return bool Returns true if the user has the super admin role, false otherwise.
     */
    public function isUserSuperAdmin(?array $roles): bool;

    /**
     * Check if the role is a source admin.
     *
     * @param array|null $roles The roles to check.
     * @return bool Returns true if the role is a source admin, false otherwise.
     */
    public function isSourceAdmin(?array $roles): bool;

    /**
     * Returns the default roles.
     *
     * @return array The default roles.
     */
    public function getDefaultRoles(): array;

    /**
     * Check user roles authorization.
     *
     * @param array $person The person object.
     * @return array The roles of the user.
     */
    public function checkUserRolesAuthorization($person): array;

    /**
     * Check if the user has the super admin CMS role.
     *
     * @param array|null $roles The roles of the user.
     * @return bool Returns true if the user has the super admin CMS role, false otherwise.
     */
    public function isUserSuperAdminCms(?array $roles): bool;

    /**
     * Check if a user has the specified roles.
     *
     * @param array $userRoles The roles of the user.
     * @param array $roles The roles to check against.
     * @return bool Returns true if the user has any of the specified roles, false otherwise.
     */
    public function isUser(array $userRoles = [], array $roles): bool;

    /**
     * Check if the given roles belong to a super admin.
     *
     * @param array|null $roles The roles to check.
     * @return bool Returns true if the roles belong to a super admin, false otherwise.
     */
    public function isSuperAdmin(?array $roles): bool;

    /**
     * Check if a user is activated.
     *
     * @param string $id The user ID.
     * @return bool Returns true if the user is activated, false otherwise.
     */
    public function isUserActivated(string $id): bool;

    /**
     * Update the role of a person.
     *
     * @param string $action The action to perform.
     * @param string $userId The ID of the user.
     * @return array The result of the update.
     */
    public function updatePersonRole(string $action, string $userId): array;

    /**
     * Retrieves the roles of a user by their ID.
     *
     * @param string $id The ID of the user.
     * @return array The roles of the user.
     */
    public function getRolesUserId(string $id): array;
}
