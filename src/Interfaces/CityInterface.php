<?php

namespace PixelHumain\Models\Interfaces;

interface CityInterface
{
    public const COLLECTION = "cities";

    public const CONTROLLER = "city";

    public const COLLECTION_DATA = "cityData";

    public const COLOR = "#E6304C";

    public const REGION = "region";

    public const DEPARTEMENT = "departement";

    public const CITY = "city";

    public const NEIGHBOUR_HOOD = "neighbourhood";

    public const CITOYENS = "citoyens";

    public const COLLECTION_IMPORTHISTORY = "importHistory";

    public const ICON = "fa-university";

    public const ZONES = "zones";
}
