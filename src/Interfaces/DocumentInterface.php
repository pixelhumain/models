<?php

namespace PixelHumain\Models\Interfaces;

interface DocumentInterface
{
    public const COLLECTION = "documents";

    public const IMG_BANNER = "banner";

    public const IMG_PROFIL = "profil";

    public const IMG_LOGO = "logo";

    public const IMG_SLIDER = "slider";

    public const IMG_MEDIA = "media";

    public const IMG_PROFIL_RESIZED = "profil-resized";

    public const IMG_PROFIL_MARKER = "profil-marker";

    public const CATEGORY_PLAQUETTE = "Plaquette";

    public const UPLOAD_DIR = "communecter";

    public const DOC_TYPE_IMAGE = "image";

    public const DOC_TYPE_FILE = "file";

    public const DOC_TYPE_CSV = "text/csv";

    public const GENERATED_IMAGES_FOLDER = "thumb";

    public const GENERATED_FILE_FOLDER = "file";

    public const GENERATED_MEDIUM_FOLDER = "medium";

    public const GENERATED_ALBUM_FOLDER = "album";

    public const GENERATED_BANNER_FOLDER = "banner";

    public const FILENAME_PROFIL_RESIZED = "profil-resized.png";

    public const FILENAME_PROFIL_MARKER = "profil-marker.png";

    public const FILENAME_PROFIL_BANNER = "banner.png";

    public const GENERATED_THUMB_PROFIL = "thumb";

    public const GENERATED_MARKER = "marker";

    public const RESTRICTED_FOLDER = "restricted";

    public const FILE_ENCRYPTION_BLOCKS = 10000;

    /**
     * Retrieves a document by its ID.
     *
     * @param string $id The ID of the document.
     * @return array|null The document data if found, null otherwise.
     */
    public function getById(string $id): ?array;

    /**
     * Get the documents that match the given parameters.
     *
     * @param array $params The parameters to filter the documents.
     * @return array The array of documents that match the given parameters.
     */
    public function getWhere(array $params): array;

    /**
     * Saves the document with the given parameters.
     *
     * @param array $params The parameters for saving the document.
     * @return array The saved document.
     */
    public function save(array $params): array;

    /**
     * Get the doctype for the given name.
     *
     * @param string $strname The name of the document.
     * @return string The doctype for the given name.
     */
    public function getDoctype(string $strname): string;

    /**
     * Move a document to a collection.
     *
     * @param string $id The ID of the document.
     * @param string|null $name The name of the collection (optional).
     * @return array The updated document.
     */
    public function moveDocumentToCollection(string $id, string $name = null): array;

    /**
     * Removes all documents of a specific type from a target.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $collection The name of the collection.
     * @param string $docType The type of the document (default: "image").
     * @return void
     */
    public function removeAllDocument(string $targetId, string $targetType, string $collection, string $docType = 'image'): void;

    /**
     * Removes all documents associated with a specific element.
     *
     * @param string $targetId The ID of the target element.
     * @param string $targetType The type of the target element.
     * @return array The list of removed documents.
     */
    public function removeAllForElement(string $targetId, string $targetType): array;

    /**
     * Lists documents by ID and type.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param array|string $contentKey The content key of the document (optional).
     * @param string|null $docType The document type (optional).
     * @param array $sort The sorting criteria for the documents (optional).
     * @return array An array of documents matching the given criteria.
     */
    public function listMyDocumentByIdAndType(string $id, string $type, $contentKey = null, string $docType = null, array $sort = []): array;

    /**
     * Returns the last thumbnail for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string $contentKey The content key of the document.
     * @param string|null $collection The collection of the document (optional).
     * @return string The URL of the last thumbnail.
     */
    public function getLastThumb(string $id, string $type, string $contentKey, string $collection = null): string;

    /**
     * Counts the number of documents that match the specified criteria.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string|null $contentKey The content key of the document (optional).
     * @param string|null $col The column of the document (optional).
     * @param string $docType The type of the document (default: "image").
     * @return int The number of documents that match the criteria.
     */
    public function countByWhere(string $id, string $type, string $contentKey = null, string $col = null, string $docType = 'image'): int;

    /**
     * Get the list of images from the given array of documents of a specific type.
     *
     * @param array $listDocumentsofType The array of documents of a specific type.
     * @return array The list of images.
     */
    public function getListOfImage(array $listDocumentsofType): array;

    /**
     * Check if the user is authorized to stock a document.
     *
     * @param string $id The ID of the document.
     * @param string|null $type The type of the document.
     * @param string|null $docType The document type.
     * @return bool Returns true if the user is authorized to stock the document, false otherwise.
     */
    public function authorizedToStock(string $id, ?string $type, ?string $docType): bool;

    /**
     * Removes a document by its ID.
     *
     * @param string $id The ID of the document to remove.
     * @param bool $canDelete Whether the document can be deleted.
     * @return array The result of the removal operation.
     */
    public function removeDocumentById(string $id, bool $canDelete = false): array;

    /**
     * Removes a document by folder.
     *
     * @param string $folder The folder of the document.
     * @return array An array containing the removed document.
     */
    public function removeDocumentByFolder(string $folder): array;

    /**
     * Removes a document communevent by its object ID.
     *
     * @param string $id The object ID of the document communevent.
     * @param string $userId The user ID.
     * @return array|null The removed document communevent, or null if not found.
     */
    public function removeDocumentCommuneventByObjId(string $id, string $userId): ?array;

    /**
     * Update a document.
     *
     * @param string $id The ID of the document to update.
     * @param array|null $update The data to update the document with.
     * @return bool Returns true if the document was successfully updated, false otherwise.
     */
    public function update(string $id, ?array $update): bool;

    /**
     * Retrieves images by key.
     *
     * @param string $itemId The ID of the item.
     * @param string $itemType The type of the item.
     * @param array $limit The limit of images to retrieve.
     * @return array The array of images.
     */
    public function getImagesByKey(string $itemId, string $itemType, array $limit): array;

    /**
     * Retrieves the last image URL associated with a specific key and subkey (optional) for an item.
     *
     * @param string $itemId The ID of the item.
     * @param string $itemType The type of the item.
     * @param string $key The key associated with the image.
     * @param string|null $subKey The subkey associated with the image (optional).
     * @return string The URL of the last image.
     */
    public function getLastImageByKey(string $itemId, string $itemType, string $key, string $subKey = null): string;

    /**
     * Returns the available categories for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @return array The available categories for the document.
     */
    public function getAvailableCategories(string $id, string $type): array;

    /**
     * Returns the URL of the document.
     *
     * @param array $document The document data.
     * @return string The URL of the document.
     */
    public function getDocumentUrl(array $document): string;

    /**
     * Returns the URL of the document folder.
     *
     * @param array $document The document data.
     * @return string The URL of the document folder.
     */
    public function getDocumentFolderUrl(array $document): string;

    /**
     * Returns the path of a document.
     *
     * @param array $document The document data.
     * @param bool $imgPath Whether to include the image path.
     * @param string $thumb The thumbnail size.
     * @return string The path of the document.
     */
    public function getDocumentPath(array $document, bool $imgPath = false, string $thumb = ''): string;

    /**
     * Returns the URL of the generated image for a document.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param string $generatedImageType The type of the generated image.
     * @param string|null $subType The subtype of the document (optional).
     * @return string|null The URL of the generated image, or null if it doesn't exist.
     */
    public function getGeneratedImageUrl(string $id, string $type, string $generatedImageType, string $subType = null): ?string;

    /**
     * Retrieve all images URL for a given ID, type, and entity.
     *
     * @param string $id The ID of the document.
     * @param string $type The type of the document.
     * @param array|null $entity The entity associated with the document (optional).
     * @return array An array of URLs for the images.
     */
    public function retrieveAllImagesUrl(string $id, string $type, array $entity = null): array;

    /**
     * Retrieves the contents of a URL and returns them as an array.
     *
     * @param string $url The URL to retrieve the contents from.
     * @return array The contents of the URL as an array.
     */
    public function urlGetContents(string $url): array;

    /**
     * Check if a URL exists.
     *
     * @param string $url The URL to check.
     * @return bool Returns true if the URL exists, false otherwise.
     */
    public function urlExists(string $url): bool;

    /**
     * Uploads a document from a URL.
     *
     * @param string $dir The directory where the document will be saved.
     * @param string $folder The folder where the document will be stored.
     * @param string $ownerId The ID of the document owner.
     * @param string $input The input data for the document.
     * @param bool $rename Whether to rename the document or not.
     * @param string $urlFile The URL of the document to be uploaded.
     * @param string $nameFile The name of the document.
     * @return array An array containing the result of the upload process.
     */
    public function uploadDocumentFromURL(string $dir, string $folder, string $ownerId, string $input, bool $rename = false, string $urlFile, string $nameFile): array;

    /**
     * Uploads a document.
     *
     * @param array $file The file to upload.
     * @param string $uploadDir The directory where the file will be uploaded.
     * @param mixed $input The input data.
     * @param bool $rename Whether to rename the file or not. Default is false.
     * @param string|null $nameUrl The URL of the file name. Default is null.
     * @param string|null $sizeUrl The URL of the file size. Default is null.
     * @param mixed|null $forcedUnloggued The forced unlogged value. Default is null.
     * @param bool $cryptage Whether to encrypt the file or not. Default is false.
     * @return array The result of the upload.
     */
    public function uploadDocument(array $file, string $uploadDir, $input, bool $rename = false, string $nameUrl = null, string $sizeUrl = null, $forcedUnloggued = null, bool $cryptage = false);

    /**
     * Encrypts a file.
     *
     * @param string $source The path to the source file.
     * @param string $dest The path to the destination file.
     * @return bool|string The result of the encryption.
     */
    public function encryptFile(string $source, string $dest);

    /**
     * Check the file requirements.
     *
     * @param array $file The file to check.
     * @param string $input The input string.
     * @param array $pathFolder The path folder.
     * @return array The result of the file requirements check.
     */
    public function checkFileRequirements(array $file, string $input, array $pathFolder): array;
}
