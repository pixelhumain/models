<?php

namespace PixelHumain\Models\Interfaces;

interface ActionsInterface
{
    public const COLLECTION = "actions";

    public const TYPE_ACTIONS = "actions"; //things to do

    public const TYPE_ACTION = "action"; //things to do

    public const ACTIONS_PARENT = "rooms";

    //ACTION STATES
    public const ACTION_TODO = "todo";

    public const ACTION_INPROGRESS = "inprogress";

    public const ACTION_LATE = "late";

    public const ACTION_CLOSED = "closed";

    /**
     * Retrieves an action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data if found, null otherwise.
     */
    public function getById(string $id): ?array;

    /**
     * Check if a user can administrate a specific item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the item.
     * @return bool Returns true if the user can administrate the item, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool;

    /**
     * Closes an action.
     *
     * @param array $params The parameters for closing the action.
     * @return array The updated array after closing the action.
     */
    public function closeAction(array $params): array;

    /**
     * Assigns the current user to the specified parameters.
     *
     * @param array $params The parameters to assign the current user to.
     * @return array The updated parameters.
     */
    public function assignMe(array $params): array;

    /**
     * Assigns the given parameters to the action.
     *
     * @param array $params The parameters to assign.
     * @return array The assigned parameters.
     */
    public function assign(array $params): array;

    /**
     * Deletes an action.
     *
     * @param string $id The ID of the action to delete.
     * @param string $userId The ID of the user performing the action.
     * @return array The result of the delete operation.
     */
    public function deleteAction(string $id, string $userId): array;

    /**
     * Deletes all actions of the room.
     *
     * @param string $actionRoomId The ID of the action room.
     * @param string $userId The ID of the user.
     * @return array The result of the deletion operation.
     */
    public function deleteAllActionsOfTheRoom(string $actionRoomId, string $userId): array;
}
