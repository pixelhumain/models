<?php

namespace PixelHumain\Models\Interfaces;

interface RiskInterface
{
    public const COLLECTION = "risks";

    public const ICON = "fa-warning";

    public const COLOR = "red";
}
