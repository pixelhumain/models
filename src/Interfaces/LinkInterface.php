<?php

namespace PixelHumain\Models\Interfaces;

interface LinkInterface
{
    public const person2person = "friends";

    public const person2organization = "memberOf";

    public const person2projects = "projects";

    public const person2events = "events";

    public const person2personfollows = "follows";

    public const person2personfollowers = "followers";

    public const event2subevent = "subEvents";

    public const event2person = "attendees";

    public const organization2element = "members";

    public const organization2person = "members people";

    public const organization2organization = "members org";

    public const organization2personOrga = "organizer";

    public const project2person = "contributors people";

    public const project2organization = "contributors org";

    public const project2element = "contributors";

    public const need2Item = "needs";

    public const element2forms = "answers";

    public const element2projects = "projects";

    //Link options
    public const TO_BE_VALIDATED = "toBeValidated";

    public const IS_ADMIN = "isAdmin";

    public const IS_ADMIN_PENDING = "isAdminPending";

    public const INVITED_BY_ID = "invitorId";

    public const INVITED_BY_NAME = "invitorName";

    public const IS_INVITING = "isInviting";

    public const IS_ADMIN_INVITING = "isAdminInviting";

    public const ALL_COMMUNITY = "allCommunity";

    public const ALL = "all";
}
