<?php

namespace PixelHumain\Models\Interfaces;

interface CostumInterface
{
    public const COLLECTION = "costum";

    public const CONTROLLER = "costum";

    public const MODULE = "costum";

    /**
     * Retrieves custom data based on the provided parameters.
     *
     * @param string|null $id The ID of the custom data.
     * @param string|null $type The type of the custom data.
     * @param string|null $slug The slug of the custom data.
     * @return mixed The custom data retrieved from the cache.
     */
    public function getCostum(?string $id = null, ?string $type = null, ?string $slug = null);
}
