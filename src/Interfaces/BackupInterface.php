<?php

namespace PixelHumain\Models\Interfaces;

interface BackupInterface
{
    public const COLLECTION = "backups";

    public const CONTROLLER = "backup";
}
