<?php

namespace PixelHumain\Models\Interfaces;

interface TagsInterface
{
    public const COLLECTION = "tags";

    /**
     * Filters and saves new tags.
     *
     * @param array|null $tags The array of tags to filter and save.
     * @return array The filtered and saved tags.
     */
    public function filterAndSaveNewTags(?array $tags): array;

    /**
     * Inserts an active tag into the database.
     *
     * @param string $tag The tag to be inserted.
     * @return bool Returns true if the tag was successfully inserted, false otherwise.
     */
    public function insertActiveTags(string $tag): bool;

    /**
     * Check and get a tag.
     *
     * @param string $tag The tag to check and get.
     * @return string The checked and retrieved tag.
     */
    public function checkAndGetTag(string $tag): string;
}
