<?php

namespace PixelHumain\Models\Interfaces;

use SimpleXMLElement;

interface NewsInterface
{
    public const COLLECTION = "news";

    public const CONTROLLER = "news";

    public const MODULE = "news";

    public const ICON = "fa-newspaper-o";

    public const COLOR = "#93C020";

    public const TYPE_PUBLIC = "public";

    public const TYPE_RESTRICTED = "restricted";

    public const TYPE_PRIVATE = "private";

    /**
     * Retrieves the configuration for the News class.
     *
     * @return array The configuration array.
     */
    public function getConfig();

    /**
     * Retrieves a news item by its ID.
     *
     * @param string $id The ID of the news item.
     * @param array|null $followsArrayIds An optional array of IDs representing the users who follow the news item.
     * @param bool $convertNews Specifies whether to convert the news item to an array.
     * @return array|null The news item as an array, or null if not found.
     */
    public function getById(string $id, array $followsArrayIds = null, bool $convertNews = true): ?array;

    /**
     * Get news by their IDs.
     *
     * @param array $ids The array of news IDs.
     * @param bool $convertNews Whether to convert the news objects.
     * @return array The array of news objects.
     */
    public function getByIds(array $ids, bool $convertNews = true): array;

    /**
     * Retrieves the author information for a given ID.
     *
     * @param string $id The ID of the author.
     * @return array|null The author information as an array, or null if not found.
     */
    public function getAuthor(string $id): ?array;

    /**
     * Retrieves news for a given object ID.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting criteria for the query. Default is ["created" => -1].
     * @param string|null $type The type of news to retrieve. Default is null.
     * @param int $limit The maximum number of news to retrieve. Default is 6.
     * @param array|null $followsArrayIds The array of IDs to filter the news by. Default is null.
     * @return array The array of news matching the given criteria.
     */
    public function getNewsForObjectId(array $param, array $sort = [
        'created' => -1,
    ], string $type = null, int $limit = 6, array $followsArrayIds = null): array;

    /**
     * Saves the news data.
     *
     * @param array $params The parameters for saving the news.
     * @return array The saved news data.
     */
    public function save(array $params): array;

    /**
     * Preps the data for the News model.
     *
     * @param array $params The parameters for the data preparation.
     * @return array The prepared data.
     */
    public function prepData(array $params): array;

    /**
     * Returns the formatted scope based on the given parameters.
     *
     * @param array $params The parameters to filter the scope.
     * @return array The formatted scope.
     */
    public function formatedScope(array $params): array;

    /**
     * Deletes a news item.
     *
     * @param string $id The ID of the news item to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @param bool $removeComments (optional) Whether to remove comments associated with the news item. Default is false.
     * @param bool $deleteProcess (optional) Whether to delete the associated process. Default is false.
     * @param bool $forced (optional) Whether to force the deletion. Default is false.
     * @return array An array containing the result of the deletion operation.
     */
    public function delete(string $id, string $userId, bool $removeComments = false, bool $deleteProcess = false, bool $forced = false): array;

    /**
     * Deletes news of an element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @param string $userId The ID of the user.
     * @param bool $removeComments (optional) Whether to remove comments associated with the news. Default is false.
     * @param bool $forced (optional) Whether to force the deletion. Default is false.
     * @return array An array containing the result of the deletion.
     */
    public function deleteNewsOfElement(string $elementId, string $elementType, string $userId, bool $removeComments = false, bool $forced = false): array;

    /**
     * Share the news.
     *
     * @param string $verb The action verb.
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string|null $comment The comment (optional).
     * @param array|null $activityValue The activity value (optional).
     * @return array The result of the share operation.
     */
    public function share(string $verb, string $targetId, string $targetType, string $comment = null, array $activityValue = null): array;

    /**
     * Removes news by image ID.
     *
     * @param string $imageId The ID of the image.
     * @return mixed The result of the operation.
     */
    public function removeNewsByImageId(string $imageId);

    /**
     * Update a field in the news object.
     *
     * @param string $newsId The ID of the news object.
     * @param string $name The name of the field to update.
     * @param mixed $value The new value for the field.
     * @param string|null $userId The ID of the user performing the update (optional).
     * @return array The updated news object.
     */
    public function updateField(string $newsId, string $name, $value, ?string $userId): array;

    /**
     * Update the comment mentions for a news item.
     *
     * @param array $mentionsComment The mentions in the comment.
     * @param string $id The ID of the news item.
     * @return bool Returns true if the comment mentions were successfully updated, false otherwise.
     */
    public function updateCommentMentions(array $mentionsComment, string $id): bool;

    /**
     * Update the news with the given parameters.
     *
     * @param array $params The parameters to update the news.
     * @return array The updated news.
     */
    public function update(array $params): array;

    /**
     * Sorts the news array based on the specified columns.
     *
     * @param array $array The array of news to be sorted.
     * @param array $cols The columns to sort the news by.
     * @return array The sorted news array.
     */
    public function sortNews(array $array, array $cols): array;

    /**
     * Get news to moderate.
     *
     * @param array|null $whereAdditional Additional conditions for the query.
     * @param int $limit The maximum number of news to retrieve.
     * @return array The array of news to moderate.
     */
    public function getNewsToModerate(array $whereAdditional = null, int $limit = 0): array;

    /**
     * Uploads a news image.
     *
     * @param string $urlImage The URL of the image to upload.
     * @param string $size The size of the image.
     * @param string $authorId The ID of the author.
     * @param bool $actionUpload Whether to perform the upload action or not. Default is true.
     * @return string The uploaded image URL.
     */
    public function uploadNewsImage(string $urlImage, string $size, string $authorId, bool $actionUpload = true): string;

    /**
     * Retrieves the structure of the RSS channel for a given element name.
     *
     * @param string $elementName The name of the element.
     * @return SimpleXMLElement The structure of the RSS channel.
     */
    public function getStrucChannelRss(string $elementName): SimpleXMLElement;

    /**
     * Returns the KML structure of the news.
     *
     * @return SimpleXMLElement The KML structure of the news.
     */
    public function getStrucKml(): SimpleXMLElement;

    /**
     * Check if a user can administrate a news item.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the news item.
     * @param bool $deleteProcess Whether it is a delete process or not. Default is false.
     * @return bool Returns true if the user can administrate the news item, false otherwise.
     */
    public function canAdministrate(string $userId, string $id, bool $deleteProcess = false): bool;

    /**
     * Ajoute des paramètres pour les nouvelles à partir d'ActivityPub.
     *
     * @param string $type Le type de l'objet ActivityPub.
     * @param string $id L'identifiant de l'objet ActivityPub.
     * @param array $params Les paramètres à ajouter.
     * @return array Les paramètres mis à jour.
     */
    public function addParamsForNewsFromActivitypub(string $type, string $id, array $params): array;
}
