<?php

namespace PixelHumain\Models\Interfaces;

interface SurveyInterface
{
    public const COLLECTION = "surveys";

    public const PARENT_COLLECTION = "actionRooms";

    public const CONTROLLER = "survey";

    public const MODULE = "survey";

    public const TYPE_SURVEY = 'survey';

    public const TYPE_ENTRY = 'entry';

    public const STATUS_CLEARED = "cleared";

    public const STATUS_REFUSED = "refused";

    public const ICON = "fa-connectdevelop";

    /**
     * Returns the data binding for the Survey model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array;

    /**
     * Retrieves a survey by its ID.
     *
     * @param string $id The ID of the survey.
     * @return array|null The survey data as an array or null if not found.
     */
    public function getById(string $id): ?array;

    /**
     * Check if a survey can be updated.
     *
     * @param string $id The ID of the survey.
     * @param string $fieldName The name of the field to update.
     * @param mixed $fieldValue The new value for the field.
     * @return array An array containing the result of the check.
     */
    public function canUpdateSurvey(string $id, string $fieldName, $fieldValue): array;

    /**
     * Modère une entrée du sondage.
     *
     * @param array $params Les paramètres de l'entrée à modérer.
     * @return array Les informations de l'entrée modérée.
     */
    public function moderateEntry(array $params): array;

    /**
     * Check if a user is a moderator for a survey.
     *
     * @param string|null $userId The ID of the user to check. Can be null.
     * @param string $key The key of the survey.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $key);

    /**
     * Deletes an entry from the survey.
     *
     * @param string $id The ID of the entry to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function deleteEntry(string $id, string $userId): array;

    /**
     * Deletes all surveys of the room.
     *
     * @param string $actionRoomId The ID of the action room.
     * @param string $userId The ID of the user.
     * @return array The result of the deletion operation.
     */
    public function deleteAllSurveyOfTheRoom(string $actionRoomId, string $userId): array;

    /**
     * Closes the entry of the survey.
     *
     * @param array $params The parameters for closing the entry.
     * @return array The updated survey data.
     */
    public function closeEntry(array $params): array;

    /**
     * Check if a user can administrate a survey.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the survey.
     * @return bool Returns true if the user can administrate the survey, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool;
}
