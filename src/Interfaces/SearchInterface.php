<?php

namespace PixelHumain\Models\Interfaces;

interface SearchInterface
{
    public const ICON = "fa-search";

    /**
     * Find records in a collection based on given criteria.
     *
     * @param string $collection The name of the collection to search in.
     * @param array $criterias An array of criteria to filter the records.
     * @param string $sortOnField The field to sort the records on. (optional)
     * @param int $nbResultMax The maximum number of results to return. (optional)
     * @return array An array of matching records.
     */
    public function findByCriterias(string $collection, array $criterias, string $sortOnField = "", int $nbResultMax = 10): array;

    /**
     * Converts accented characters in a string to their corresponding regular expression patterns.
     *
     * @param string $text The text to convert.
     * @return string The converted text with accented characters replaced by regular expression patterns.
     */
    public function accentToRegex(string $text): string;

    /**
     * Converts accented characters in a string to their corresponding regular expression pattern.
     *
     * @param string $text The input text.
     * @return string The converted text with accented characters replaced by their regex pattern.
     */
    public function accentToRegexSimply(string $text): string;

    /**
     * Auto-completes the network based on the given parameters.
     *
     * @param array $post The input parameters for the auto-complete.
     * @param mixed $filter The optional filter to apply to the auto-complete results.
     * @return void
     */
    public function networkAutoComplete(array $post, $filter = null): void;

    /**
     * Performs a global autocomplete search.
     *
     * @param array $post The search parameters.
     * @param string|null $filter The filter to apply to the search.
     * @param bool $api Determines if the search is performed via API.
     * @return array The search results.
     */
    public function globalAutoComplete(array $post, string $filter = null, $api = false): array;

    //*********************** Count search results********************************************************//
    // params countTYpe is an array defining collection searching in modules context
    // params query is array of condition general
    // params queryPersons is array of condition specific for people
    // params queryNews is array of condition specific for news
    // params queryEvents is array of condition specific for events
    /**
     * Counts the results by collection.
     *
     * @param array $countType The count types.
     * @param array $query The query.
     * @param array $queryPersons The query for persons.
     * @param array $queryNews The query for news.
     * @param array $queryEvents The query for events.
     * @param array $queryClassifieds The query for classifieds.
     * @param array $queryProposals The query for proposals.
     * @param array $queryPoi The query for points of interest.
     * @return array The count results.
     */
    public function countResultsByCollection(array $countType, array $query, array $queryPersons, array $queryNews, array $queryEvents, array $queryClassifieds, array $queryProposals, array $queryPoi): array;

    //*********************************  Search   ******************************************
    /**
     * Search for a string in an array of queries.
     *
     * @param string $search The string to search for.
     * @param array $query The array of queries to search in.
     * @return array The array of search results.
     */
    public function searchString(string $search, array $query): array;

    /**
     * Search for filters in the given query.
     *
     * @param array|null $filters The filters to search for.
     * @param array $query The query to search in.
     * @return array The filtered results.
     */
    public function searchFilters(?array $filters, array $query): array;

    //*********************************  Search  in news ******************************************
    /**
     * Search for news based on a search string and query parameters.
     *
     * @param string $search The search string.
     * @param array $query The query parameters.
     * @return array The search results.
     */
    public function searchNewsString(string $search, array $query): array;

    //*********************************  TAGS   ******************************************
    /**
     * Search for tags.
     *
     * @param array|string|null $searchTags The tags to search for.
     * @param string $verb The comparison operator for the search. Defaults to '$in'.
     * @return array The search results.
     */
    public function searchTags($searchTags, string $verb = '$in'): array;

    /**
     * Concatenates two query arrays using the specified verb.
     *
     * @param array|null $q1 The first query array.
     * @param array $q2 The second query array.
     * @param string $verb The verb used to concatenate the arrays.
     * @return array The concatenated query array.
     */
    public function concatQuery(?array $q1, array $q2, string $verb): array;

    /**
     * Search for a specific source key.
     *
     * @param string|array|null $sourceKey The source key to search for.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchSourceKey($sourceKey, array $query): array;

    //*********************************  Zones   ******************************************
    /**
     * Search for zones based on localities.
     *
     * @param array $localities The array of localities to search for.
     * @return array The array of zones matching the localities.
     */
    public function searchZones(array $localities): array;

    //****************************DEFINE LOCALITY QUERY   ***************************************
    //*********************************  ZONES   *************************************************
    //************************ LOCALITY QUERY FOR ELEMENT ****************************************
    // TODO : $geo regarder le type
    /**
     * Search for localities based on the given query and optional geographical coordinates.
     *
     * @param array|null $localities An array of localities to search in.
     * @param array $query The search query.
     * @param mixed|null $geo The geographical coordinates for the search.
     * @return array An array of search results.
     */
    public function searchLocality(?array $localities, array $query, $geo = null): array;

    /**
     * Search for a specific scope.
     *
     * @param array|null $localities The localities to search within.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchScope(?array $localities, array $query): array;

    //****************************DEFINE LOCALITY QUERY   ***************************************
    //*********************************  ZONES   *************************************************
    //************************ LOCALITY QUERY FOR ELEMENT ****************************************
    /**
     * Search for locality network.
     *
     * @param array|null $localities The localities to search in.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchLocalityNetwork(?array $localities, array $query): array;

    //***************************** LOCALITY QUERY FOR NEWS********************************************
    /**
     * Search for news in specific localities.
     *
     * @param array|null $localities The localities to search in.
     * @param array $query The search query.
     * @return array The search results.
     */
    public function searchLocalityNews(?array $localities, array $query): array;

    //*********************************  END DEFINE LOCALITY QUERY   ****************************************

    //*********************************  Specific queries   ****************************************
    // TODO : verfier comment $startDate, $endDate sont utilisés et quel type ils ont
    /**
     * Retrieves the query events based on the given parameters.
     *
     * @param array $queryEvent The query event array.
     * @param mixed $startDate The start date of the events.
     * @param mixed $endDate The end date of the events.
     * @return array The array of query events.
     */
    public function getQueryEvents(array $queryEvent, $startDate, $endDate): array;

    /**
     * Retrieves organizations based on the provided query and category.
     *
     * @param array $query The search query parameters.
     * @param string|null $category The category to filter the organizations by.
     * @return array The array of organizations matching the query and category.
     */
    public function getQueryOrganizations(array $query, ?string $category): array;

    /**
     * Get the query with a custom costum value.
     *
     * @param array $query The query array.
     * @param string|null $costum The custom costum value.
     * @return array The modified query array.
     */
    public function getQueryCostum(array $query, ?string $costum): array;

    /**
     * Retrieves the bookmark query based on the given parameters.
     *
     * @param array $query The search query.
     * @param string|null $category The category to filter the search results.
     * @return array The bookmark query.
     */
    public function getQueryBookmark(array $query, ?string $category): array;

    /**
     * Retrieves classifieds based on the provided query parameters.
     *
     * @param array $query The search query parameters.
     * @param string $section The section of the classifieds.
     * @param string $category The category of the classifieds.
     * @param string $subType The subtype of the classifieds.
     * @param int $priceMin The minimum price of the classifieds.
     * @param int $priceMax The maximum price of the classifieds.
     * @param string $devise The currency of the classifieds.
     * @return array The classifieds matching the query parameters.
     */
    public function getQueryClassifieds(array $query, string $section, string $category, string $subType, int $priceMin, int $priceMax, string $devise): array;

    /**
     * Retrieves the query points of interest.
     *
     * @param array $query The search query.
     * @param string|null $category The category of the points of interest.
     * @return array The array of query points of interest.
     */
    public function getQueryPoi(array $query, ?string $category): array;

    //*********************************  END Specific squeries   ****************************************
    //trie les éléments dans l'ordre alphabetique par name
    /**
     * Sorts an array of elements by name.
     *
     * @param array $a The first element to compare.
     * @param array $b The second element to compare.
     * @return bool Returns true if $a should be placed before $b, false otherwise.
     */
    public function mySortByName(array $a, array $b): bool;

    //trie les éléments dans l'ordre alphabetique par updated
    /**
     * Sorts the given arrays by their "updated" value.
     *
     * @param array $a The first array to compare.
     * @param array $b The second array to compare.
     * @return bool Returns true if $a is considered less than $b, false otherwise.
     */
    public function mySortByUpdated(array $a, array $b): bool;

    /**
     * Returns the type of localisation based on the given location string.
     *
     * @param string $locStr The location string.
     * @return string The type of localisation.
     */
    public function getTypeOfLocalisation(string $locStr): string;

    /**
     * Check if the given type matches the wanted type.
     *
     * @param string $type The type to check.
     * @param array|null $searchType The wanted type to compare against. Default is null.
     * @return bool Returns true if the given type matches the wanted type, false otherwise.
     */
    public function typeWanted(string $type, ?array $searchType = null): bool;

    /**
     * Check if the given parent object is valid for the scope.
     *
     * @param array|null $parentObj The parent object to check.
     * @return bool Returns true if the parent object is valid for the scope, false otherwise.
     */
    public function checkScopeParent(?array $parentObj): bool;

    /**
     * Check if the given parent object has the specified tags.
     *
     * @param array|null $parentObj The parent object to check.
     * @param array|null $tags The tags to check.
     * @return bool Returns true if the parent object has the tags, false otherwise.
     */
    public function checkTagsParent(?array $parentObj, ?array $tags): bool;

    //********************************* COSTUM  ******************************************/

    /**
     * Search for custom items based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for indexing.
     * @param int $indexMin The minimum index value.
     * @param mixed $searchCostum The custom search parameter.
     * @return array The search results as an array.
     */
    public function searchCostum(array $query, int $indexStep, int $indexMin, $searchCostum): array;

    //********************************* BOOKMARK  ******************************************/

    /**
     * Search for bookmarks based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step value for pagination.
     * @param int $indexMin The minimum index value for pagination.
     * @param string $type The type of bookmark to search for.
     * @param bool $searchBook Whether to search for books or not.
     * @return array The search results as an array.
     */
    public function searchBookmark(array $query, $indexStep, $indexMin, $type, $searchBook): array;

    //********************************* EVENTS COMMUNITY  ******************************************/

    /**
     * Search for a community based on the given query parameters.
     *
     * @param array $query The search query parameters.
     * @param array|null $community The community to search within (optional).
     * @return array The search results as an array.
     */
    public function searchCommunity(array $query, ?array $community): array;

    //*********************************  BADGES   ******************************************
    /**
     * Search for badges based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The array of badges matching the search query.
     */
    public function searchBadges(array $query, int $indexStep, int $indexMin): array;

    //*********************************  PERSONS   ******************************************
    /**
     * Search for persons based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param bool $prefLocality Whether to prioritize locality in the search.
     * @param array|null $community The community to filter the search results.
     * @return array The array of search results.
     */
    public function searchPersons(array $query, int $indexStep, int $indexMin, bool $prefLocality = false, ?array $community): array;

    //*********************************  ORGANIZATIONS   ******************************************
    /**
     * Search organizations based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The number of results to return per page.
     * @param int $indexMin The minimum index of the results to return.
     * @param string $searchTypeOrga The type of organization to search for.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchOrganizations(array $query, int $indexStep, int $indexMin, string $searchTypeOrga, array $sort = [
        "updated" => -1,
    ]): array;

    //*********************************  NEWS   ******************************************
    /**
     * Search for news based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchNews(array $query, int $indexStep, int $indexMin): array;

    /**
     * Search for answers based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for indexing.
     * @param int $indexMin The minimum index value.
     * @return array The array of search results.
     */
    public function searchAnswers(array $query, int $indexStep, int $indexMin): array;

    /**
     * Search events based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param mixed|null $searchOnAll Flag to indicate whether to search on all fields.
     * @param mixed|null $all Flag to indicate whether to search on all fields.
     * @param array $sort The sorting criteria for the search results.
     * @param mixed $startD The start date for the search.
     * @param string $startDateUTC The start date in UTC format.
     * @return array The search results as an array.
     */
    public function searchEvents(array $query, int $indexStep, int $indexMin, $searchOnAll = null, $all = null, $sort = [
        "startDate" => -1,
    ], $startD, string $startDateUTC): array;

    //*********************************  PROJECTS   ******************************************
    /**
     * Search for projects based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The number of results to return per page.
     * @param int $indexMin The minimum index of the results to return.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchProject(array $query, int $indexStep, int $indexMin, array $sort = [
        "updated" => -1,
    ]): array;

    //*********************************  CLASSIFIED   ******************************************

    /**
     * Recherche les annonces classées en fonction des critères spécifiés.
     *
     * @param array $query Les critères de recherche.
     * @param int $indexStep Le nombre d'annonces à récupérer par étape.
     * @param int $indexMin L'index minimum des annonces à récupérer.
     * @param mixed $priceMin Le prix minimum des annonces à récupérer.
     * @param mixed $priceMax Le prix maximum des annonces à récupérer.
     * @param string $devise La devise utilisée pour les prix.
     * @return array Les annonces classées correspondantes aux critères de recherche.
     */
    public function searchClassified(array $query, int $indexStep, int $indexMin, $priceMin, $priceMax, $devise);

    //*********************************  CLASSIFIED   ******************************************
    /**
     * Search for favorites of a specific type.
     *
     * @param string $type The type of favorites to search for.
     * @return array An array containing the search results.
     */
    public function searchFavorites(string $type): array;

    //*********************************  POI   ******************************************
    /**
     * Search for points of interest (POI) based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param array $sort The sorting criteria for the search results. Default is ["updated" => -1].
     * @return array The array of search results.
     */
    public function searchPoi(array $query, int $indexStep, int $indexMin, array $sort = [
        "updated" => -1,
    ]): array;

    //*********************************  PRODUCT   ******************************************
    /**
     * Search for a product based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchProduct(array $query, int $indexStep, int $indexMin): array;

    //*********************************  SERVICE   ******************************************
    /**
     * Search for a service based on the given query.
     *
     * @param array $query The search query.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @return array The search results.
     */
    public function searchService(array $query, int $indexStep, int $indexMin): array;

    //*********************************  CIRCUIT   ******************************************

    /**
    * Searches for a circuit based on the specified criteria.
    *
    * @param array $query The search criteria.
    * @param int $indexStep The starting index for pagination.
    * @param int $indexMin The minimum index of the results to return.
    * @return array The search results.
    */
    public function searchCircuit(array $query, int $indexStep, int $indexMin): array;

    //*********************************  Generic Search   ******************************************
    /**
     * Search for any item in a collection based on a query.
     *
     * @param string $collection The name of the collection to search in.
     * @param array $query The query parameters to filter the search results.
     * @param int $indexStep The step size for pagination of search results.
     * @param int $indexMin The minimum index for pagination of search results.
     * @return array The search results as an array.
     */
    public function searchAny(string $collection, array $query, int $indexStep, int $indexMin): array;

    //*********************************  DDA   ******************************************
    /**
     * Search for DDA (Data Driven Application) using the given query.
     *
     * @param array $query The search query.
     * @param int $indexMax The maximum number of results to return.
     * @return array The search results.
     */
    public function searchDDA(array $query, int $indexMax): array;

    //*********************************  VOTES / propositions   ******************************************

    /**
     * Search proposals based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param mixed $localities The localities to filter the search results.
     * @return array The array of search results.
     */
    public function searchProposals(array $query, int $indexStep, int $indexMin, $localities): array;

    /**
     * Search for votes based on the given query parameters.
     *
     * @param array $query The query parameters for the search.
     * @param int $indexStep The step size for pagination.
     * @param int $indexMin The minimum index for pagination.
     * @param string $searchType The type of search to perform.
     * @return array The array of search results.
     */
    public function searchVotes(array $query, int $indexStep, int $indexMin, string $searchType): array;

    //*********************************  CITIES   ******************************************
    /**
     * Search for cities based on the given parameters.
     *
     * @param string $search The search query.
     * @param string|null $locality The locality to filter the search results by.
     * @param string|null $country The country to filter the search results by.
     * @return array The array of search results.
     */
    public function searchCities(string $search, ?string $locality, ?string $country): array;

    /**
     * Removes empty words from the given search string.
     *
     * @param string $search The search string to remove empty words from.
     * @return string The modified search string without empty words.
     */
    public function removeEmptyWords(string $search): string;

    /***********************************  DEFINE LOCALITY QUERY   ***************************************/
    public function searchLocalityNetworkOld(array $query, array $post): array;

    /**
     * Search for addresses based on the given parameters.
     *
     * @param array $params The search parameters.
     *
     * @return array|null The array of addresses matching the search parameters, or null if no addresses are found.
     */
    public function address(array $params): ?array;
}
