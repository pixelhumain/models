<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\RoomInterface;
use PixelHumain\Models\Interfaces\SurveyInterface;
use PixelHumain\Models\Traits\ActionsTrait;
use PixelHumain\Models\Traits\ActionTrait;


use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\Interfaces\ActionsTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ActionTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SurveyTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\SurveyTrait;

class Room extends BaseModel implements RoomInterface, AuthorisationTraitInterface, CommentTraitInterface, SurveyTraitInterface, ActionsTraitInterface, ActionTraitInterface, PersonTraitInterface, LinkTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AuthorisationTrait;
    use CommentTrait;
    use SurveyTrait;
    use ActionsTrait;
    use ActionTrait;
    use PersonTrait;
    use LinkTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    public static array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "topic" => [
            "name" => "shortDescription",
        ],
        "description" => [
            "name" => "description",
            "rules" => ["required"],
        ],
        "tags" => [
            "name" => "tags",
        ],
        "roles" => [
            "name" => "roles",
        ],
        "urls" => [
            "name" => "urls",
        ],
        // Open / Closed
        "status" => [
            "name" => "status",
            "rules" => ["required"],
        ],
        "idUserAuthor" => [
            "name" => "idUserAuthor",
            "rules" => ["required"],
        ],
        "parentId" => [
            "name" => "parentId",
            "rules" => ["required"],
        ],
        "parentType" => [
            "name" => "parentType",
            "rules" => ["required"],
        ],
        "parentApp" => [
            "name" => "parentApp",
        ],
        "parentIdSurvey" => [
            "name" => "parentIdSurvey",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Retrieves a room by its ID.
     *
     * @param string $id The ID of the room.
     * @return array|null The room data as an array, or null if the room is not found.
     */
    public function getById(string $id): ?array
    {
        return $this->db->findOne(RoomInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Retrieves the action by its ID.
     *
     * @param string $id The ID of the action.
     * @return array|null The action data or null if not found.
     */
    public function getActionById(string $id): ?array
    {
        return $this->db->findOne(RoomInterface::COLLECTION_ACTIONS, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Retrieves records from the database based on specified conditions, sorts them, and limits the result set.
     *
     * @param array $params The conditions to filter the records.
     * @param array $sort The sorting criteria for the records.
     * @param int $limit The maximum number of records to retrieve (default is 1).
     * @return array The retrieved records.
     */
    public function getWhereSortLimit(array $params, array $sort, int $limit = 1): array
    {
        return $this->db->findAndSort(RoomInterface::COLLECTION, $params, $sort, $limit);
    }

    /**
     * Retrieves a single action room by its parent organization.
     *
     * @param string $idOrga The ID of the parent organization.
     * @return array|null The single action room if found, null otherwise.
     */
    public function getSingleActionRoomByOrgaParent(string $idOrga): ?array
    {
        return $this->db->findOne(
            RoomInterface::COLLECTION,
            [
                "parentId" => $idOrga,
            ]
        );
    }

    /**
     * Check if a user can participate in the room.
     *
     * @param string $userId The ID of the user.
     * @param string|null $id The ID of the room (optional).
     * @param string|null $type The type of the room (optional).
     * @return bool Returns true if the user can participate, false otherwise.
     */
    public function canParticipate(string $userId, ?string $id = null, ?string $type = null): bool
    {
        return ! empty($id) && ! empty($type) ? $this->getModelAuthorisation()->isElementMember($id, $type, $userId) : false;
    }

    /**
     * Check if a user is a moderator in the room.
     *
     * @param string|null $userId The ID of the user to check.
     * @param string $key The key to access the room.
     * @return bool Returns true if the user is a moderator, false otherwise.
     */
    public function isModerator(?string $userId, string $key): bool
    {
        $app = $this->db->findOne(PHType::TYPE_APPLICATIONS, [
            "key" => $key,
        ]);
        $res = false;
        if (! empty($app["moderator"]) && is_array($app["moderator"])) {
            $res = (isset($userId) && in_array($this->session->getUserId(), $app["moderator"])) ? true : false;
        }
        return $res;
    }

    /**
     * Check if a user can administrate a room.
     *
     * @param string $userId The ID of the user.
     * @param string $id The ID of the room.
     * @return bool Returns true if the user can administrate the room, false otherwise.
     */
    public function canAdministrate(string $userId, string $id): bool
    {
        $actionRoom = $this->getById($id);

        $parentId = $actionRoom["parentId"] ?? null;
        $parentType = $actionRoom["parentType"] ?? null;

        $isAdmin = false;
        if (($parentType == OrganizationInterface::COLLECTION || $parentType == ProjectInterface::COLLECTION || $parentType == EventInterface::COLLECTION) && $parentId != null && $parentType != null) {
            $isAdmin = $this->getModelAuthorisation()->canDeleteElement($parentId, $parentType, $userId);
        }
        return $isAdmin;
    }

    /**
     * Get the access level for a given room based on the user's roles.
     *
     * @param array $room The room details.
     * @param array|null $myRoles The user's roles.
     * @return string The access level for the room.
     */
    public function getAccessByRole(array $room, ?array $myRoles): string
    {
        $roomRoles = $room["roles"] ?? [];
        if (is_string($room["roles"])) {
            $roomRoles = explode(",", $room["roles"]);
        }

        $intersect = [];
        if (! empty($myRoles) && sizeof($myRoles) > 0) {
            foreach ($roomRoles as $key => $roomRole) {
                foreach ($myRoles as $key => $myRole) {
                    if ($roomRole == $myRole) {
                        $intersect[] = $myRole;
                    }
                }
            }
        }
        if (sizeof($intersect) > 0) {
            $accessRoom = "unlock";
        } elseif (! empty($roomRoles) && is_array($roomRoles) && $roomRoles[0] == "") {
            $accessRoom = "open";
        } else {
            $accessRoom = "lock";
        }

        return $accessRoom;
    }

    /**
     * Insert a new room into the database.
     *
     * @param array $parentRoom The parent room data.
     * @param string $type The type of the room.
     * @param mixed $copyOf The room to copy from (optional).
     * @return mixed The new room data.
     */
    public function insert(array $parentRoom, string $type, $copyOf = null)
    {
        $newInfos = [];
        $newInfos['email'] = $this->session->get('userEmail');
        $newInfos['name'] = $parentRoom['name'];
        $newInfos['type'] = $type;
        if (! empty($copyOf)) {
            $newInfos['copyOf'] = $copyOf;
        }
        $newInfos['parentType'] = $parentRoom['parentType'];
        $newInfos['parentId'] = $parentRoom['parentId'];
        if ((! empty($parentRoom['tags']) && is_countable($parentRoom['tags']) ? count($parentRoom['tags']) : 0) > 0) {
            $newInfos['tags'] = $parentRoom['tags'];
        }
        $newInfos['created'] = time();

        $newInfos = $this->db->insert(ActionRoomInterface::COLLECTION, $newInfos);
        return $newInfos;
    }

    /**
     * Deletes an action room.
     *
     * @param string $id The ID of the room to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @return array The result of the deletion operation.
     */
    public function deleteActionRoom(string $id, string $userId): array
    {
        $res = [
            "result" => false,
            "msg" => "Something went wrong : contact your admin !",
        ];

        $actionRoom = $this->getById($id);
        if (empty($actionRoom)) {
            return [
                "result" => false,
                "The action room does not exist",
            ];
        }

        if (! $this->canAdministrate($userId, $id)) {
            return [
                "result" => false,
                "msg" => "You must be admin of the parent of this room if you want delete it",
            ];
        }
        //Remove actionRoom of type discuss : remove all comments linked
        if (! empty($actionRoom["type"]) && $actionRoom["type"] == RoomInterface::TYPE_DISCUSS) {
            $resChildren = $this->getModelComment()->deleteAllContextComments($id, RoomInterface::COLLECTION, $userId);
            //Remove actionRoom of type vote : remove all survey linked
        } elseif (! empty($actionRoom["type"]) && $actionRoom["type"] == RoomInterface::TYPE_VOTE) {
            //Delete all surveys of this action room
            $resChildren = $this->getModelSurvey()->deleteAllSurveyOfTheRoom($id, $userId);
        } elseif (! empty($actionRoom["type"]) && $actionRoom["type"] == RoomInterface::TYPE_ACTIONS) {
            //Delete all actions of this action room
            $resChildren = $this->getModelActions()->deleteAllActionsOfTheRoom($id, $userId);
        } else {
            $resChildren = [
                "result" => false,
                "msg" => "The delete of this type of action room '" . (string) $actionRoom["type"] . "' is not yet implemented.",
            ];
        }

        if (isset($resChildren["result"]) && ! $resChildren["result"]) {
            return (array) $resChildren;
        }

        //Remove the action room
        if ($this->db->remove(RoomInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ])) {
            $res = [
                "result" => true,
                "msg" => "The action room has been deleted with success",
            ];
        }

        return $res;
    }

    /**
     * Deletes an action room element.
     *
     * @param string $elementId The ID of the element to delete.
     * @param string $elementType The type of the element to delete.
     * @param string $userId The ID of the user performing the deletion.
     * @param bool $forced Whether the deletion should be forced.
     * @return array The result of the deletion operation.
     */
    public function deleteElementActionRooms(string $elementId, string $elementType, string $userId, bool $forced = false): array
    {
        //Check if the $userId can delete the element
        $canDelete = $this->getModelAuthorisation()->canDeleteElement($elementId, $elementType, $userId);
        if (! $canDelete && $forced == false) {
            return [
                "result" => false,
                "msg" => "You do not have enough credential to delete this element rooms.",
            ];
        }

        //get all actions
        $actionRooms2delete = $this->getElementActionRooms($elementId, $elementType);
        $nbActionRoom = 0;
        foreach ($actionRooms2delete as $id => $anActionRoom) {
            $resDeleteActionRoom = $this->deleteActionRoom((string) $id, $userId);
            if ($resDeleteActionRoom["result"] == false) {
                error_log("Error during the process try to delete the action room " . $id . " : " . (string) $resDeleteActionRoom["msg"]);
                return $resDeleteActionRoom;
            }
            $nbActionRoom++;
        }

        return [
            "result" => true,
            "msg" => $nbActionRoom . " actionRoom of the element " . $elementId . " of type " . $elementType . " have been removed with succes.",
        ];
    }

    // TODO: $_POST
    /**
     * Closes the action.
     *
     * @param array $params The parameters for the action.
     * @return array The result of the action.
     */
    public function closeAction(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(RoomInterface::COLLECTION_ACTIONS, [
                "_id" => $this->db->MongoId((string) $params["id"]),
            ])) {
                if ($this->session->get('userEmail') == $action["email"]) {
                    //then remove the parent survey
                    $status = (@$action["status"] == RoomInterface::ACTION_CLOSED) ? RoomInterface::ACTION_INPROGRESS : RoomInterface::ACTION_CLOSED;
                    $this->db->update(
                        RoomInterface::COLLECTION_ACTIONS,
                        [
                            "_id" => $action["_id"],
                        ],
                        [
                            '$set' => [
                                "status" => $status,
                            ],
                        ]
                    );
                    $this->getModelAction()->updateParent($_POST['id'], RoomInterface::COLLECTION_ACTIONS);
                    $res["result"] = true;
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    /**
     * Assigns the current room to the user.
     *
     * @param array $params The parameters for the assignment.
     * @return array The updated room details.
     */
    public function assignMe(array $params): array
    {
        $res = [
            "result" => false,
        ];
        if ($this->session->getUserId()) {
            if ($action = $this->db->findOne(RoomInterface::COLLECTION_ACTIONS, [
                "_id" => $this->db->MongoId((string) $params["id"]),
            ])) {
                if ($this->getModelAuthorisation()->canParticipate($this->session->getUserId(), $action["parentType"], $action["parentId"])) {
                    $res = $this->getModelLink()->connect($params["id"], RoomInterface::COLLECTION_ACTIONS, $this->session->getUserId(), PersonInterface::COLLECTION, $this->session->getUserId(), "contributors", true);
                    $this->getModelAction()->updateParent($_POST['id'], RoomInterface::COLLECTION_ACTIONS);
                } else {
                    $res["msg"] = "restrictedAccess";
                }
            } else {
                $res["msg"] = "SurveydoesntExist";
            }
        } else {
            $res["msg"] = "mustBeLoggued";
        }
        return $res;
    }

    /**
     * Retrieves all rooms by type and ID.
     *
     * @param string $type The type of room.
     * @param string $id The ID of the room.
     * @param bool|null $archived Whether to include archived rooms. Defaults to null.
     * @return array An array of rooms.
     */
    public function getAllRoomsByTypeId(string $type, string $id, ?bool $archived = null): array
    {
        $where = [
            "created" => [
                '$exists' => 1,
            ],
        ];
        $where["status"] = ($archived) ? RoomInterface::STATE_ARCHIVED : [
            '$exists' => 0,
        ];

        if (! empty($type)) {
            $where["parentType"] = $type;
        }
        if (! empty($id)) {
            $where["parentId"] = $id;
        }

        if ($type == PersonInterface::COLLECTION) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonId($id, $archived);
        } elseif ($this->session->getUserId()) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonIdByType($this->session->getUserId(), $type, $id, $archived);
        } else {
            $rooms = $this->getWhereSortLimit($where, [
                "date" => 1,
            ], 0);
        }

        $actionHistory = [];
        if (isset($roomsActions) && isset($roomsActions["rooms"]) && isset($roomsActions["actions"])) {
            $rooms = $roomsActions["rooms"];
            $actionHistory = $roomsActions["actions"];
        }

        $discussions = [];
        $votes = [];
        $actions = [];
        foreach ($rooms as $e) {
            if (in_array($e["type"], [RoomInterface::TYPE_DISCUSS, RoomInterface::TYPE_FRAMAPAD])) {
                array_push($discussions, $e);
            } elseif ($e["type"] == RoomInterface::TYPE_VOTE) {
                array_push($votes, $e);
            } elseif ($e["type"] == RoomInterface::TYPE_ACTIONS) {
                array_push($actions, $e);
            }
        }
        $params = [
            "discussions" => $discussions,
            "votes" => $votes,
            "actions" => $actions,
            "history" => $actionHistory,
        ];
        return $params;
    }

    /**
     * Retrieves all rooms activity by type and ID.
     *
     * @param string $type The type of the room.
     * @param string $id The ID of the room.
     * @param bool|null $archived (optional) Whether to include archived rooms. Defaults to null.
     * @return array An array containing the rooms activity.
     */
    public function getAllRoomsActivityByTypeId(string $type, string $id, ?bool $archived = null): array
    {
        $where = [
            "created" => [
                '$exists' => 1,
            ],
        ];
        $where["status"] = ($archived) ? RoomInterface::STATE_ARCHIVED : [
            '$exists' => 0,
        ];

        if (! empty($type)) {
            $where["parentType"] = $type;
        }
        if (! empty($id)) {
            $where["parentId"] = $id;
        }

        if ($type == PersonInterface::COLLECTION) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonId($id, $archived);
        } elseif ($this->session->getUserId()) {
            $roomsActions = $this->getModelPerson()->getActionRoomsByPersonIdByType($this->session->getUserId(), $type, $id, $archived);
        } else {
            $rooms = $this->getWhereSortLimit($where, [
                "date" => 1,
            ], 0);
        }

        if (isset($roomsActions) && isset($roomsActions["rooms"]) && isset($roomsActions["actions"])) {
            $rooms = $roomsActions["rooms"];
        }

        //error_log("count rooms : ".count($rooms));

        $discussions = [];
        $proposals = [];
        $actions = [];
        foreach ($rooms as $e) {
            if (in_array($e["type"], [RoomInterface::TYPE_DISCUSS, RoomInterface::TYPE_FRAMAPAD])) {
                //ordonner par updated
                array_push($discussions, $e);
            } elseif ($e["type"] == RoomInterface::TYPE_VOTE) {
                //get all survey for this room by sorting
                $surveys = $this->db->findAndSort(SurveyInterface::COLLECTION, [
                    "survey" => (string) $e["_id"],
                    "updated" => [
                        '$exists' => 1,
                    ],
                ], [
                    "updated" => 1,
                ], 10);
                foreach ($surveys as $s) {
                    array_push($proposals, $s);
                }
            } elseif ($e["type"] == RoomInterface::TYPE_ACTIONS) {
                //get all survey for this room by sorting
                $actionElements = $this->db->findAndSort(RoomInterface::TYPE_ACTIONS, [
                    "room" => (string) $e["_id"],
                    "updated" => [
                        '$exists' => 1,
                    ],
                ], [
                    "updated" => 1,
                ], 10);
                foreach ($actionElements as $a) {
                    array_push($actions, $a);
                }
            }
        }

        function mySort($a, $b)
        {
            if (isset($a['updated']) && isset($b['updated'])) {
                return (strtolower(@$b['updated']) > strtolower(@$a['updated']));
            } else {
                return false;
            }
        }

        $list = [...$discussions, ...$proposals, ...$actions];
        usort($list, "mySort");

        return $list;
    }

    /**
     * Retrieve the action rooms for a specific element.
     *
     * @param string $elementId The ID of the element.
     * @param string $elementType The type of the element.
     * @return array The array of action rooms.
     */
    public function getElementActionRooms(string $elementId, string $elementType): array
    {
        $where = [
            '$and' => [[
                "parentType" => $elementType,
            ], [
                "parentId" => $elementId,
            ]],
        ];

        $actionRooms = $this->db->find(RoomInterface::COLLECTION, $where);

        return $actionRooms;
    }
}
