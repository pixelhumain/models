<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\MenuInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\FormTrait;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\FormTraitInterface;

// TODO : Yii::get('url')::base(true)
// TODO : Yii::get('module')->getModule

/**
 * Menu class
 * @deprecated
 */
class Menu extends BaseModel implements MenuInterface, AuthorisationTraitInterface, CostumTraitInterface, ElementTraitInterface, FormTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use ParamsTrait;
    use SessionTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use AuthorisationTrait;
    use CostumTrait;
    use ElementTrait;
    use FormTrait;

    private array $v = [];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateParamsProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    public function constructMenuBtn($btns, $btnClass, $connectedMode = null)
    {
        $menuButtonHtml = "";

        foreach ($btns as $k => $v) {
            if (self::showButton($v, null, $connectedMode)) {
                if ($k === 'chat') {
                    if ($this->params->get('rocketchatEnabled') && $this->params->get('rocketchatEnabled') == true && $this->params->get('rocketchatURL') && ! empty($this->params->get('rocketchatURL')) && $this->session->get('rocketUserId')) {
                        if (isset($v["construct"])) {
                            if (is_string($v["construct"])) {
                                $menuButtonHtml .= self::{$v["construct"]}($v, $btnClass, $k);
                            }
                        } else {
                            $menuButtonHtml .= self::commonButtonHtml($v, $btnClass, $k);
                        }
                    }
                } else {
                    if (isset($v["construct"])) {
                        if (is_string($v["construct"])) {
                            $menuButtonHtml .= self::{$v["construct"]}($v, $btnClass, $k);
                        }
                    } else {
                        $menuButtonHtml .= self::commonButtonHtml($v, $btnClass, $k);
                    }
                }
            }
        }
        return $menuButtonHtml;
    }

    public function showButton($value, $element = null, $connectedMode = null)
    {
        if (! empty($element)) {
            $show = true;
            if (isset($value["restricted"])) {
                if (isset($value["restricted"]["roles"])) {
                    $show = ! ($this->getModelAuthorisation()->hasRoles($value["restricted"]));
                }
                if (isset($value["restricted"]["edit"]) && empty($edit)) {
                    $show = false;
                }
                if (isset($value["restricted"]["members"])) {
                    $show = $this->getModelAuthorisation()->isElementMember((string) $element["_id"], $element["collection"], $this->session->getUserId());
                }
                if (isset($value["restricted"]["admins"]) && $value["restricted"]["admins"] === true) {
                    $show = $this->getModelAuthorisation()->isElementAdmin((string) $element["_id"], $element["collection"], $this->session->getUserId());
                }
                if (isset($value["restricted"]["costumAdmins"])) {
                    $show = $this->getModelAuthorisation()->isCostumAdmin();
                }
                if (isset($value["restricted"]["types"]) && ! in_array($element["collection"], $value["restricted"]["types"])) {
                    $show = false;
                }
                if (isset($value["restricted"]["category"]) && (! isset($element["category"]) || ! in_array($element["category"], $value["restricted"]["category"]))) {
                    $show = false;
                }
            }
            return $show;
        }
        if (isset($value["connected"])) {
            if ($this->getModelAuthorisation()->isInterfaceAdmin() && $connectedMode === false) {
                if ($value["connected"] === false) {
                    return true;
                } elseif ($value["connected"] === true) {
                    return false;
                }
            }
            if ($value["connected"] === true && empty($this->session->getUserId())) {
                return false;
            } elseif ($value["connected"] === false && ! empty($this->session->getUserId())) {
                return false;
            }
        }
        if (isset($value["restricted"]) && ! $this->getModelAuthorisation()->accessMenuButton($value)) {
            return false;
        }
        return true;
    }

    public function image($value, $btnClass, $keyClass)
    {
        $costum = $this->getModelCostum()->getCostum();
        $assetsPath = (isset($value["assets"])) ? Yii::get('module')->getModule($value["assets"])->getAssetsUrl() : "";
        $imgUrl = $assetsPath . @$value["url"];
        $imgUrlMin = (isset($value["urlMin"])) ? $assetsPath . $value["urlMin"] : $assetsPath . @$value["url"];

        $heightImg = (isset($value["height"])) ? "height='" . $value["height"] . "'" : "";
        $widthImg = (isset($value["width"])) ? "width='" . $value["width"] . "'" : "";
        $heightImgMin = (isset($value["heightMin"])) ? "height='" . $value["heightMin"] . "'" : "height='40'";
        $widthImgMin = (isset($value["widthMin"])) ? "width='" . $value["widthMin"] . "'" : "";

        $classImg = $value["class"] ?? "";
        $classImg .= " cosDyn-" . $keyClass;
        $href = $value["href"] ?? "#welcome"/*)*/;
        $aClass = $value["aClass"] ?? "btn btn-link pull-left no-padding lbh menu-btn-top";
        if (! empty($costum["type"]) && $costum["type"] == "aap") {
            $aClass = "aap-lists-btn lbh-menu-app";
        }
        $target = (isset($value["target"]) ? "target ='_blank'" : "");
        $str = '<a href="' . $href . '" class="' . $aClass . '" ' . $target . '>' .
                    "<img src='" . $imgUrl . "' class='image-menu hidden-xs " . $classImg . "' " . $heightImg . "  " . $widthImg . "/>" .
                    "<img src='" . $imgUrlMin . "' class='image-menu visible-xs " . $classImg . "' " . $heightImgMin . "  " . $widthImgMin . "/>" .
            '</a>';
        return $str;
    }

    public function commonButtonHtml($value, $btnClass, $keyClass, $extraHtml = "")
    {
        $trad = (isset($value["trad"]) && ! empty($value["trad"])) ? $value["trad"] : "common";
        $labelClass = (isset($value["labelClass"])) ? " " . $value["labelClass"] : "";
        $label = (isset($value["label"]) && ! empty($value["label"])) ? "<span class='" . $labelClass . "'>" . $this->language->t($trad, $value["label"]) . "</span>" : "";
        $icon = (isset($value["icon"])) ? "<i class='fa fa-" . $value["icon"] . "'></i>" : "";
        $img = (isset($value["img"]) && ! empty($img)) ? true : false;
        $btnClass .= (isset($value["class"])) ? " " . $value["class"] : "";
        $btnClass .= " cosDyn-" . $keyClass;
        $idButton = $value["id"] ?? "";
        $blank = (isset($value["blank"])) ? "target='_blank'" : "";
        $href = $value["href"] ?? "javascript:;";
        $spanTooltips = "";
        $badgesHtml = "";
        if (isset($value["spanTooltip"]) && ! empty($value["spanTooltip"])) {
            $spanTooltips = '<span class="tooltips-menu-btn">' . $this->language->t($trad, $value["spanTooltip"]) . '</span>';
        }
        if (isset($value["badge"])) {
            $badgeClass = $value["badge"]["class"] ?? "";
            $badgesHtml = '<span class="' . $badgeClass . ' badge animated bounceIn cosDyn-badge"></span>';
        }
        $dataModal = (isset($value["dataTarget"])) ? ' data-toggle="modal" data-target="' . $value["dataTarget"] . '" ' : "";

        if ($img) {
            $widthImgCommon = (isset($value["width"])) ? $value["width"] . "px" : "45px";
            $heightImgCommon = (isset($value["height"])) ? $value["height"] . "px" : "45px";

            return "<a href='" . $href . "' id='" . $idButton . "' " . $blank . " class='" . $btnClass . "' " . $dataModal . "> <img class='img-responsive' width='" . $widthImgCommon . "' height='" . $heightImgCommon . "' src='" . Yii::get('module')->getModule("costum")->assetsUrl . $value["img"] . "'> " . $badgesHtml . $label . $spanTooltips . $extraHtml . '</a>';
        } else {
            return "<a href='" . $href . "' id='" . $idButton . "' " . $blank . " class='commonButtonHtml " . $btnClass . "' " . $dataModal . ">" . $icon . " " . $badgesHtml . $label . $spanTooltips . $extraHtml . '</a>';
        }
    }

    public function elementButtonHtml($v, $btnClass, $extraHtml = "")
    {
        $dataAttr = "";
        $dataAttr .= (@$v["action"]) ? "data-action='" . $v["action"] . "' " : "";
        $dataAttr .= (@$v["view"]) ? "data-view='" . $v["view"] . "' " : "";
        $linkBtn = (@$v["href"]) ? $v["href"] : "javascript:;";
        if (isset($v["dataAttr"])) {
            $dataAttr .= (isset($v["dataAttr"]["dir"])) ? "data-dir='" . $v["dataAttr"]["dir"] . "' " : "";
            $dataAttr .= (isset($v["dataAttr"]["toggle"])) ? "data-toggle='" . $v["dataAttr"]["toggle"] . "' " : "";
            $dataAttr .= (isset($v["dataAttr"]["target"])) ? "data-target='" . $v["dataAttr"]["target"] . "' " : "";
        }
        $iconHtml = "";
        if (isset($v["img"])) {
            $iconHtml = '<span class="' . ($v["imgClass"] ?? "") . ' tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' . $this->language->t("common", @$v["label"]) . '">' .
                '<img src="' . Yii::get('module')->getModule("co2")->getAssetsUrl() . '/images/' . $v["img"] . '"></mg>' .
                '</span>';
        } elseif (isset($v["icon"])) {
            $iconHtml = '<span class="' . ($v["iconClass"] ?? "") . ' tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="' . $this->language->t("common", @$v["label"]) . '">' .
                '<i class="fa fa-' . $v["icon"] . '"></i>' .
            '</span>';
        }
        $idbtn = "";
        $idbtn .= (@$v["id"]) ? "id='" . $v["id"] . "' " : "";
        $str = '<a href="' . $linkBtn . '" ' . @$idbtn . ' class="elementButtonHtml ssmla btn btn-default btn-menu-tooltips ' . @$v["class"] . '" ' . @$dataAttr . ' data-toggle="tooltip" data-placement="bottom" title="">' .
                $iconHtml .
                '<span class="tooltips-top-menu-btn tooltips-visivility" style="display: none;left: auto!important;visibility: hidden;">' .
                    $this->language->t("common", @$v["label"]) .
                '</span>' .
                '<span class="' . @$v["labelClass"] . '">' . $this->language->t("common", @$v["label"]) . '</span>' .
            '</a>';
        return $str;
    }

    public function removeSlash($url)
    {
        // supprimer le slash au debut de l'url
        //ex: '/https://....'
        return ltrim($url, '/');
    }

    public function imageElt($v, $btnClass, $keyClass, $element)
    {
        $url = (isset($element["profilMediumImageUrl"]) && ! empty($element["profilMediumImageUrl"])) ? Yii::get('urlManager')->createUrl('/' . $element['profilMediumImageUrl']) : $element["defaultImg"];
        $btnClass .= (isset($v["class"])) ? " " . $v["class"] : "";
        $btnClass .= " cosDyn-" . $keyClass;
        return "<img src='" . self::removeSlash($url) . "' class='imageElt " . $btnClass . "'/>";
    }

    public function nameElt($v, $btnClass, $keyClass, $element)
    {
        $name = $element["name"] ?? "";
        $btnClass .= (isset($v["class"])) ? " " . $v["class"] : "";
        $colorElt = (isset($element["type"]) && $this->getModelElement()->getColorIcon($element["type"]) !== false) ? $this->getModelElement()->getColorIcon($element["type"]) : $this->getModelElement()->getColorIcon($element["collection"]);
        $btnClass .= " cosDyn-" . $keyClass;
        $str = '<div class="nameElt identity-min">' .
                '<div class="pastille-type-element bg-' . $colorElt . ' pull-left hidden"></div>' .
              ' <div class="' . $btnClass . ' pull-left no-padding no-margin">' .
                '<div class="text-left padding-left-15" id="second-name-element">' .
                    '<span id="nameHeader">' .
                        '<h5 class="elipsis">' . @$element["name"] . '</h5>' .
                    '</span>' .
                '</div>' .
              '</div>' .
          '</div>';
        return $str;
    }

    public function inviteInElement($v, $btnClass, $keyClass, $element)
    {
        $urlLink = "#element.invite.type." . $element["collection"] . ".id." . (string) $element["_id"];
        $inviteLink = "people";
        $inviteText = $this->language->t("common", "Invite people");
        if ($element["collection"] == OrganizationInterface::COLLECTION) {
            $inviteLink = "members";
            $inviteText = $this->language->t("common", 'Invite members');
        } elseif ($element["collection"] == ProjectInterface::COLLECTION) {
            $inviteLink = "contributors";
            $inviteText = $this->language->t("common", 'Invite contributors');
        }
        $iconBtn = (isset($v["icon"]) && ! empty($v["icon"])) ? '<i class="fa fa-' . $v["icon"] . '"></i>' : "";
        if (isset($v["label"])) {
            $inviteText = $v["label"];
        }
        $whereConnect = ($element["collection"] != PersonInterface::COLLECTION) ? 'to the ' . $this->getModelElement()->getControlerByCollection($element["collection"]) : "";
        $classHref = (isset($v["class"]) && ! empty($v["class"])) ? $v["class"] : "text-red";
        $classHref .= (isset($v["tooltip"]) && ! empty($v["tooltip"])) ? " tooltips" : "";
        $str = '<a  href="' . $urlLink . '" ' .
                'class="inviteInElement lbhp ' . $classHref . '" ' .
                'data-placement="bottom" ' .
                'data-original-title="' . $this->language->t("common", "Invite {what} {where}", [
                    "{what}" => $this->language->t("common", $inviteLink),
                    "{where}" => $this->language->t("common", $whereConnect),
                ]) . '">' .
                $iconBtn . '<span class="label-menu">' . $inviteText . '</span>' .
            '</a>';
        return $str;
    }

    public function dropdown($value, $btnClass, $keyClass)
    {
        $checkStatusBtn = null;
        $iconHtml = (! empty($value["icon"])) ? "<i class='fa fa-" . $value["icon"] . "'></i>" : "";
        $classDrop = $value["class"] ?? "";
        $classDrop .= " cosDyn-" . $keyClass;
        $buttonConstruct = $value["buttonConstruct"] ?? "li";
        $nbLi = is_countable($value["buttonList"]) ? count($value["buttonList"]) : 0;
        $statusBtn = [];
        $i = 0;
        foreach ($value["buttonList"] as $keyBtn => $vBtn) {
            array_push($statusBtn, self::showButton($vBtn));
        }
        if (! empty($statusBtn)) {
            $checkStatusBtn = array_reduce($statusBtn, fn ($key, $item) => $key && is_bool($item) && $item === false, true);
        }
        if (! $checkStatusBtn) {
            $str = "<div class='dropdown " . $classDrop . "'>" .
            "<a href='javascript:;' class='dropdown-toggle pull-left cosDyn-icon " . $btnClass . "' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" . $iconHtml;
            if (isset($value["spanTooltip"]) && ! empty($value["spanTooltip"])) {
                $str .= '<span class="tooltips-menu-btn">' . $this->language->t("common", $value["spanTooltip"]) . '</span>';
            }
            $str .= "</a>";
            $str .= "<div class='dropdown-menu no-padding'>" .
                    '<ul class="dropdown-menu arrow_box">';
            foreach ($value["buttonList"] as $k => $v) {
                $i++;
                if (self::showButton($v)) {
                    $str .= self::$buttonConstruct($v, $btnClass, "buttonList");
                } else {
                    $nbLi--;
                }
            }
            $str .= "</ul>" .
            "</div>" .
            "</div>";
            return $str;
        }
    }

    public function xsMenu($value, $btnClass, $keyClass, $element = null)
    {
        $iconHtml = (! empty($value["icon"])) ? "<i class='fa fa-" . $value["icon"] . " cosDyn-icon'></i>" : "";
        $dataIcon = (! empty($value["icon"])) ? "data-icon='fa-" . $value["icon"] . "'" : "";
        $classDrop = $value["class"] ?? "";
        $btnClass .= (isset($value["aClass"])) ? " " . $value["aClass"] : "";
        $btnClass .= " cosDyn-" . $keyClass;

        $label = (isset($value["label"])) ? "<span class='label-menu'>" . $value["label"] . "</span>" : "";
        $dataLabel = (! empty($value["label"])) ? "data-label='fa-" . $value["label"] . "'" : "";
        $targetMenu = $value["target-menu"] ?? "main-xs-menu";
        $buttonConstruct = $value["buttonConstruct"] ?? "commonButtonHtml";
        $nbLi = is_countable($value["buttonList"]) ? count($value["buttonList"]) : 0;
        $i = 0;

        $burgerBtn = "<a href='javascript:;' class='visible-xs open-xs-menu " . $btnClass . "' " . $dataLabel . " " . $dataIcon . " data-target=" . $targetMenu . ">" . $iconHtml . " " . $label;
        if (isset($value["spanTooltip"]) && ! empty($value["spanTooltip"])) {
            $burgerBtn .= '<span class="tooltips-menu-btn">' . $this->language->t("common", $value["spanTooltip"]) . '</span>';
        }
        $burgerBtn .= "</a>";
        $xsItem = '';
        foreach ($value["buttonList"] as $k => $v) {
            $i++;
            if (self::showButton($v, $element)) {
                if (isset($v["construct"])) {
                    $xsItem .= self::{$v["construct"]}($v, $btnClass, "xsMenu");
                } else {
                    $xsItem .= self::$buttonConstruct($v, $btnClass, "-buttonList");
                }
            }
        }

        $str = "<div class='xsMenu menu-xs-cplx pull-left'>";
        if ($xsItem !== "") {
            $str .= $burgerBtn;
        } else {
            $str .= "<a href='javascript:;' class='visible-xs open-xs-menu-collapse " . $btnClass . "' " . $dataIcon . " data-toggle='collapse' data-target='#menuTopCenter'>" . $iconHtml . " " . $label;
            if (isset($value["spanTooltip"]) && ! empty($value["spanTooltip"])) {
                $str .= '<span class="tooltips-menu-btn">' . $this->language->t("common", $value["spanTooltip"]) . '</span>';
            }
            $str .= "</a>";
        }
        $str .= "<div class='menu-xs-container shadow2 " . $targetMenu . "'>";
        $str .= $xsItem;
        $str .= "</div>";
        $str .= "</div>";
        return $str;
    }

    public function li($v, $liClass = "", $keyClass)
    {
        $label = $v["label"] ?? "";
        $href = $v["href"] ?? "javascript:;";
        $user = $this->session->get("user");
        if (strpos($href, "{userSlug}") !== false && isset($user) && isset($user["slug"])) {
            $href = str_replace("{userSlug}", "@" . $user["slug"], $href);
        }
        $blank = (isset($v["blank"])) ? "target='_blank'" : "";
        $class = $v["class"] ?? "";
        $class .= " cosDyn-" . $keyClass;
        $iconHtml = (isset($v["icon"])) ? '<i class="fa fa-' . $v["icon"] . '"></i>' : "";
        $str = "";
        $dataModal = (isset($v["dataTarget"])) ? ' data-toggle="modal" data-target="' . $v["dataTarget"] . '" ' : "";
        $str .= "<li class='" . $liClass . "'>" .
                    '<a href="' . $href . '" class="bg-white ' . $class . '" ' . $dataModal . ' ' . $blank . '>' .
                        $iconHtml . ' ' . $this->language->t("common", $label) .
                    '</a>' .
                '</li>';

        return $str;
    }

    private function sousMenuView($parent, $class, $key, $btnClass, $img, $icon, $iconClass)
    {
        $vd = [];
        $targetBlankItem = "";
        $targetBlankItem = "";
        $subdomaineName = "";
        $hrefItem = $key;
        $classBtnMenuItem = $class . ' lbh-menu-app';
        if (! empty($parent["urlExtern"])) {
            $hrefItem = $parent["urlExtern"];
            $targetBlankItem = ' target="_blanc" ';
            $classBtnMenuItem = $class . ' lbh-urlExtern';
        }
        if (isset($parent["lbhAnchor"]) && $parent["lbhAnchor"] == true) {
            $classBtnMenuItem = $class . ' lbh-anchor';
        }
        if (isset($vd["class"])) {
            $classBtnMenuItem .= " " . $parent["class"];
        }
        if (isset($parent["subdomainName"])) {
            $subdomaineName = $parent["subdomainName"];
        }
        $str = "<a href='" . $hrefItem . "' " . $targetBlankItem . " class='" . $classBtnMenuItem . " " . $btnClass . "'>";
        if (! empty($parent["img"]) && ! empty($img)) {
            $costum = $this->getModelCostum()->getCostum();
            if ($costum) {
                $str .= '<img src="' . Yii::get('module')->getModule("costum")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
            } else {
                $str .= '<img src="' . Yii::get('module')->getModule("co2")->assetsUrl . $parent["img"] . '" class="imgMenu hidden-xs">';
            }
        } elseif (! empty($icon) && ! empty($parent["icon"])) {
            $str .= '<i class="fa fa-' . $parent["icon"] . ' ' . $iconClass . '"></i>';
        }
        $str .= $subdomaineName;
        $str .= "</a>";
        if (isset($parent["buttonList"])) {
            $str .= "<div class='sous-menu-btn-navigation'>";
            if (! empty($parent["buttonList"])) {
                foreach ($parent["buttonList"] as $key => $value) {
                    $str .= self::sousMenuView($value, $class, $key, $btnClass, $img, $icon, $iconClass);
                }
            }
            $str .= "</div>";
        }
        return $str;
    }

    public function app($value, $btnClass, $keyClass)
    {
        $class = (! empty($value["class"])) ? $value["class"] : "menu-app hidden-xs";
        $showLabel = (! empty($value["label"])) ? true : false;
        $labelClass = $value["labelClass"] ?? "";
        $iconClass = $value["iconClass"] ?? "";
        $tooltips = $value["spanTooltip"] ?? false;
        $img = $value["img"] ?? true;
        $icon = $value["icon"] ?? true;
        $str = "<div class='cosDyn-" . $keyClass . "' style='position: relative;float: left;'>";

        foreach ($value["buttonList"] as $key => $v) {
            if ($this->getModelAuthorisation()->accessMenuButton($v) && ! empty($v)) {
                $labelStr = (isset($v["subdomainName"])) ? $this->language->t("common", $v["subdomainName"]) : "";
                $href = $key;
                $targetBlank = "";
                $classBtnMenu = $class . ' lbh-menu-app ';

                if (isset($v["buttonList"])) {
                    $classBtnMenu = $class . ' lbh-dropdown cosDyn-buttonList';
                    $costum = $this->getModelCostum()->getCostum();
                    $str .=
                    '<div class="dropdown hidden-xs ' . $btnClass . ' tools-dropdown">
                        <a class="dropdown-toggle ' . $classBtnMenu . ' " type="button" data-toggle="dropdown">';
                    if (! empty($v["img"]) && ! empty($img)) {
                        $costum = $this->getModelCostum()->getCostum();
                        if ($costum) {
                            $str .= '<img src="' . Yii::get('module')->getModule("costum")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
                        } else {
                            $str .= '<img src="' . Yii::get('module')->getModule("co2")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
                        }
                    } elseif (! empty($icon) && ! empty($v["icon"])) {
                        $str .= '<i class="fa fa-' . $v["icon"] . ' ' . $iconClass . '"></i>';
                    }
                    if ($showLabel && ! empty($labelStr)) {
                        $str .= '<span class="label-menu ' . $labelClass . '">' . $labelStr . '</span>';
                    }
                    $str .= '<span class="caret"></span>
                        </a>
                        <div class="dropdown-menu drop-down-sub-menu">';
                    foreach ($v["buttonList"] as $kd => $vd) {
                        if (self::showButton($vd)) {
                            $str .= '<div class="drop-down-sub-menu-content">';
                            $str .= self::sousMenuView($vd, $class, $kd, $btnClass, $img, $icon, $iconClass);
                            $str .= '</div>';
                        }
                    }
                    $str .= '</div>
                    </div>';
                } else {
                    if (! empty($v["urlExtern"])) {
                        $href = $v["urlExtern"];
                        $targetBlank = ' target="_blanc" ';
                        $classBtnMenu = $class . ' lbh-urlExtern';
                    }

                    if (isset($v["lbhAnchor"])) {
                        if ($v["lbhAnchor"] === true) {
                            $classBtnMenu = $class . ' lbh-anchor';
                        }
                    }

                    /* BEGIN: rajouter le hash dans un paramètre GET s'il s'agit de lbh-menu-app  */
                    if (str_contains($classBtnMenu, "lbh-menu-app")) {
                        $url = parse_url($href);
                        if (isset($url["fragment"])) {
                            $current_url = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            $href = parse_url($current_url, PHP_URL_SCHEME) . "://" .
                                    parse_url($current_url, PHP_URL_HOST) .
                                    parse_url($current_url, PHP_URL_PATH);

                            parse_str($_SERVER['QUERY_STRING'], $params);
                            $params["h"] = $url["fragment"];
                            $href .= "?" . http_build_query($params);

                            $href .= "#" . $url["fragment"];
                        }
                    }
                    /* END: rajouter le hash dans un paramètre GET s'il s'agit de lbh-menu-app  */

                    if (isset($v["class"])) {
                        $classBtnMenu .= " " . $v["class"];
                    }
                    $classBtnMenu .= ' cosDyn-buttonList';
                    $str .= '<a href="' . $href . '" ' . $targetBlank . ' class="' . $classBtnMenu . ' ' . $btnClass . '" >';
                    if (! empty($v["img"]) && ! empty($img)) {
                        // Remove default width of img menu => width="45px" height="45px"
                        $str .= '<img src="' . Yii::get('module')->getModule("costum")->assetsUrl . $v["img"] . '" class="imgMenu hidden-xs">';
                    } elseif (! empty($icon) && $icon == "true" && ! empty($v["icon"])) {
                        $str .= '<i class="fa fa-' . $v["icon"] . ' ' . $iconClass . '"></i>';
                    }
                    if ($showLabel && ! empty($labelStr)) {
                        $str .= '<span class="label-menu ' . $labelClass . '">' . $labelStr . '</span>';
                    }
                    if ($tooltips) {
                        $str .= '<span class="tooltips-menu-btn">' . $labelStr . '</span>';
                    }
                    $str .= "</a>";
                }
            }
        }
        $str .= "</div>";
        return $str;
    }

    public function createElement($value, $btnClass, $keyClass)
    {
        return self::commonButtonHtml($value, $btnClass, $keyClass, '<div class="toolbar-bottom-adds toolbar-bottom-fullwidth font-montserrat"></div>');
    }

    public function profilButton($value, $btnClass, $keyClass)
    {
        $tooltipsAccount = $this->language->t("common", "My page");
        $user = $this->session->get("user");

        if (isset($value["tooltips"])) {
            $tooltipsAccount = $value["tooltips"];
        }
        $nameLabel = null;
        if (isset($value["name"])) {
            $nameLabel = $user["name"] ?? $user["username"];
        }
        $strLabel = null;
        if (isset($value["label"])) {
            $strLabel = $value["label"];
        }

        $profilThumbImageUrl = (isset($user["profilImageUrl"]) && ! empty($user["profilImageUrl"])) ? Yii::get('url')::base(true) . $user["profilImageUrl"] : Yii::get('module')->getModule("co2")->getAssetsUrl() . '/images/thumb/default_citoyens.png';

        $dataUrl = "";
        $href = "#page.type.citoyens.id." . $this->session->getUserId();
        $classNav = "lbh";
        if (isset($value["dashboard"]) && ! empty($value["dashboard"])) {
            $classNav = "open-modal-dashboard";
            $href = "javascript:;";
            $viewModal = $value["view"] ?? "";
            $dataUrl = " data-view='" . $viewModal . "' ";
        }
        $classBtn = $value["class"] ?? "pull-right";
        $classBtn .= " cosDyn-" . $keyClass;
        $str = '<a  href="' . $href . '" ' .
                'class="menu-name-profil ' . $classNav . ' ' . $classBtn . ' shadow2 btn-menu-tooltips" ' . $dataUrl . ' data-toggle="dropdown">';
        if (! empty($nameLabel)) {
            $str .= '<small class="hidden-xs hidden-sm margin-left-10" id="menu-name-profil">' .
                $nameLabel .
            '</small>';
        }
        if (! empty($value["img"])) {
            $str .= '<img class="img-circle" id="menu-thumb-profil" width="40" height="40" src="' . $profilThumbImageUrl . '" alt="image">';
        }
        if (! empty($strLabel)) {
            $str .= '<span class="margin-left-5" id="menu-label-profil">' .
                $strLabel .
            '</span>';
        }
        if ($tooltipsAccount !== false) {
            $str .= '<span class="tooltips-menu-btn">' . $tooltipsAccount . '</span>';
        }
        $str .= '</a>';

        return $str;
    }

    public function languagesSelector($value, $btnClass, $keyClass)
    {
        $appConfig = $this->caheHelper->get("appConfig");
        $languages = $appConfig["languages"];
        $btnClass = "cosDyn-" . $keyClass;
        $str = '<ul class="nav navbar-nav ' . $btnClass . '">' .
                '<li class="dropdown">' .
                    '<a href="#" class="dropdown-toggle btn btn-default btn-language padding-5" style="padding-top: 7px !important" data-toggle="dropdown" role="button">' .
                        '<img src="' . Yii::get('url')::base(true) . '/images/flags/' . $this->language->get() . '.png" width="22"/> <span class="caret"></span>' .
                    '</a>' .
                    '<ul class="dropdown-menu arrow_box dropdown-languages-nouser" role="menu" style="">';
        foreach ($languages as $lang => $label) {
            $str .= '<li><a href="javascript:;" onclick="coInterface.setLanguage(\'' . $lang . '\')">' .
                    '<img src="' . Yii::get('url')::base(true) . '/images/flags/' . $lang . '.png" width="25"/>' . $this->language->t("common", $label) .
                '</a></li>';
        }
        $str .= '</ul>' .
                '</li>' .
            '</ul>';
        return $str;
    }

    public function searchBar($params, $btnClass, $keyClass)
    {
        $eventTargetInput = (! @$params["dropdownResult"]) ? "main-search-bar" : "second-search-bar";
        $eventTargetAddon = (! @$params["dropdownResult"]) ? "main-search-bar-addon" : "second-search-bar-addon";
        if (@$params["eventTargetInput"]) {
            $eventTargetInput;
        }
        if (@$params["eventTargetAddon"]) {
            $eventTargetAddon;
        }
        $classes = $params["class"] ?? "hidden-xs";
        $classes .= (isset($params["addClass"])) ? " " . $params["addClass"] : "";
        $classes .= " cosDyn-" . $keyClass;
        $placeholder = (@$params["placeholder"]) ? $this->language->t("common", $params["placeholder"]) : $this->language->t("common", "what are you looking for ?");
        $icon = (@$params["icon"]) ? $params["icon"] : "arrow-circle-right";
        $str = '<div class="searchBarInMenu ' . $classes . '">';

        if (! empty($params["pull-right"]) && ["pull-right"] == true) {
            $str .= '<span class="text-white input-group-addon pull-right ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
                       '<i class="fa fa-' . $icon . '"></i>' .
                   '</span>' .
                   '<input type="text" class="form-control pull-right text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">';
        } else {
            $str .= '<input type="text" class="form-control pull-left text-center ' . $eventTargetInput . '" id="' . $eventTargetInput . '" placeholder="' . $placeholder . '">' .
                    '<span class="text-white input-group-addon pull-left ' . $eventTargetAddon . '" id="' . $eventTargetAddon . '">' .
                        '<i class="fa fa-' . $icon . '"></i>' .
                    '</span>';
        }

        //if(@$params["dropdownResult"])
        $str .= '<div class="dropdown-result-global-search hidden-xs col-sm-6 col-md-5 col-lg-5 no-padding" style="display:none;"></div>';
        $str .= "</div>";
        return $str;
    }

    public function coTools($params, $btnClass, $keyClass)
    {
        $tools =
        '<div class="tools-dropdown dropdown cosDyn-' . $keyClass . '">
            <a href="javascript:;" class="dropdown-toggle menu-button  menu-btn-top" data-toggle="dropdown"><span class="fa fa-th"></span></a>
            <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li> <b>COTOOLS </b></li>
                <li class="divider"></li>
                <li class="col-xs-4"><a href="https://chat.communecter.org" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/chat.jpg" alt=""><p>Chat</p></a></li>
                <li class="col-xs-4"><a href="https://wekan.communecter.org" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/wekan.jpg" alt=""><p>Kaban</p></a></li>
                <li class="col-xs-4"><a href="https://conextcloud.communecter.org/" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/cloud.jpg" alt=""><p>Drive</p></a></li>
                <li  class="col-xs-4"><a href="https://peertube.communecter.org/" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/peertube.jpg" alt=""><p>Video</p></a></li>
                <li class="col-xs-4"><a href="https://codimd.communecter.org/" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/codimd.jpg" alt=""><p>Pad</p></a></li>
                <li  class="col-xs-4"><a href="https://meet.jit.si/co" target="_blank"><img src="' . Yii::get('module')->getModule("co2")->assetsUrl . '/images/jitsi.jpg" alt=""><p>Visio</p></a></li>';

        $tools .= '
                    <li class="ocecotools-title"> <b>OCECO TOOLS </b></li>
                    <li class="divider"></li>';
        foreach ($this->getModelForm()->ocecoTools() as $key => $value) {
            $tools .= '<li class="col-xs-4">
                            <a href="javascript:;" class="btn-ocecotools" data-label="' . $value["label"] . '" data-path="' . $value["path"] . '" data-id="' . $value["id"] . '">
                                <img src="' . $value["logo"] . '" alt="">
                                <p style="white-space: normal;">' . $value["label"] . '</p>
                            </a>
                        </li>';
        }
        $tools .= '</ul>
        </div>';

        return $tools;
    }
}
