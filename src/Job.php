<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\JobInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\Interfaces\OrganizationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TagsTraitInterface;
use PixelHumain\Models\Traits\OrganizationTrait;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\TagsTrait;

class Job extends BaseModel implements JobInterface, OrganizationTraitInterface, TagsTraitInterface, SIGTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use OrganizationTrait;
    use TagsTrait;
    use SIGTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Retrieves a job by its ID.
     *
     * @param string $id The ID of the job.
     * @return array|null The job data as an array or null if the job is not found.
     */
    public function getById(string $id): ?array
    {
        $job = $this->db->findOne(JobInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);

        //get the details of the hiring organization
        if (! empty($job["hiringOrganization"])) {
            $organization = $this->getModelOrganization()->getById($job["hiringOrganization"]);
            $job["hiringOrganization"] = $organization;
        }

        return $job;
    }

    /**
     * Inserts a job into the database.
     *
     * @param array $job The job data to be inserted.
     * @return array The inserted job data.
     */
    public function insertJob(array $job): array
    {
        foreach ($job as $jobFieldName => $jobFieldValue) {
            if (! self::checkFieldBeforeUpdate($jobFieldName, $jobFieldValue)) {
                throw new CTKException($this->language->t("job", "Can not insert the job : unknown field ") . $jobFieldName);
            }
        }
        //Manage tags : save any inexistant tag to DB
        if (isset($job["tags"])) {
            $job["tags"] = $this->getModelTags()->filterAndSaveNewTags($job["tags"]);
        }

        //Manage address
        if (isset($job["jobLocation.address"])) {
            if (! empty($job["jobLocation.address"]["postalCode"]) && ! empty($job["jobLocation.address"]["codeInsee"])) {
                $insee = $job["jobLocation.address"]["codeInsee"];
                $address = $this->getModelSIG()->getAdressSchemaLikeByCodeInsee($insee);
                $job["jobLocation.address"] = $address;
                $job["geo"] = $this->getModelSIG()->getGeoPositionByInseeCode($insee);
            }
        }
        //Insert the job
        $result = $this->db->updateWithOptions(
            JobInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId(),
            ],
            [
                '$set' => $job,
            ],
            [
                "upsert" => true,
            ]
        );
        $newJobId = $this->db->getIdFromUpsertResult($result);
        $job = $this->getById($newJobId);

        return [
            "result" => true,
            "msg" => $this->language->t("job", "Your job offer has been added with succes"),
            "id" => $newJobId,
            "job" => $job,
        ];
    }

    // TODO : $userId is not used
    /**
     * Update a job.
     *
     * @param string $jobId The ID of the job to update.
     * @param array $job The updated job data.
     * @param mixed $userId The ID of the user performing the update.
     * @return array The updated job data.
     */
    public function updateJob(string $jobId, array $job, $userId): array
    {
        foreach ($job as $jobFieldName => $jobFieldValue) {
            if (! self::checkFieldBeforeUpdate($jobFieldName, $jobFieldValue)) {
                throw new CTKException($this->language->t("job", "Can not insert the job : unknown field ") . $jobFieldName);
            }
            //address
            if ($jobFieldName == "jobLocation.address") {
                if (! empty($jobFieldValue["postalCode"]) && ! empty($jobFieldValue["codeInsee"])) {
                    $insee = $jobFieldValue["codeInsee"];
                    $address = $this->getModelSIG()->getAdressSchemaLikeByCodeInsee($insee);
                    $job["jobLocation"] = [
                        "address" => $address,
                    ];
                    $job["geo"] = getGeoPositionByInseeCode($insee);
                } else {
                    throw new CTKException($this->language->t("job", "Error updating the Organization : address is not well formated !"));
                }
                unset($job[$jobFieldName]);
            } else {
                $job[$jobFieldName] = $jobFieldValue;
            }
        }

        //Manage tags : save any inexistant tag to DB
        if (isset($job["tags"])) {
            $job["tags"] = $this->getModelTags()->filterAndSaveNewTags($job["tags"]);
        }

        //update the job
        $this->db->update(
            JobInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($jobId),
            ],
            [
                '$set' => $job,
            ]
        );

        return [
            "result" => true,
            "msg" => $this->language->t("job", "Your job offer has been updated with success"),
            "id" => $jobId,
        ];
    }

    // TODO : $userId is not used
    /**
     * Removes a job.
     *
     * @param string $jobId The ID of the job to remove.
     * @param mixed $userId The ID of the user.
     * @return array The updated job data.
     */
    public function removeJob(string $jobId, $userId): array
    {
        //update the job
        $this->db->remove(JobInterface::COLLECTION, [
            "_id" => $this->db->MongoId($jobId),
        ]);

        return [
            "result" => true,
            "msg" => $this->language->t("job", "Your job offer has been deleted with success"),
        ];
    }

    // TODO : $userId is not used
    /**
     * Update a specific field of a job.
     *
     * @param string $jobId The ID of the job.
     * @param string $jobFieldName The name of the field to update.
     * @param mixed $jobFieldValue The new value for the field.
     * @param string $userId The ID of the user performing the update.
     * @return array The updated job data.
     */
    public function updateJobField(string $jobId, string $jobFieldName, $jobFieldValue, $userId): array
    {
        $job = [
            $jobFieldName => $jobFieldValue,
        ];
        $res = $this->updateJob($jobId, $job, $userId);
        return $res;
    }

    // TODO : $jobFieldValue is not used
    /**
     * Check the field before updating the job.
     *
     * @param string $jobFieldName The name of the job field.
     * @param mixed $jobFieldValue The value of the job field.
     * @return bool True if the field is valid, false otherwise.
     */
    private static function checkFieldBeforeUpdate($jobFieldName, $jobFieldValue): bool
    {
        $res = false;
        $listFieldName = ["baseSalary", "benefits", "datePosted", "description", "educationRequirements", "employmentType", "experienceRequirements", "incentives", "industry", "jobLocation.description", "jobLocation.address", "occupationalCategory", "qualifications", "responsibilities", "salaryCurrency", "skills", "specialCommitments", "title", "workHours", "hiringOrganization", "startDate", "tags"];

        $res = in_array($jobFieldName, $listFieldName);

        //check for a composing fieldName
        //TODO SBAR - The choise could be to send json Data

        return $res;
    }

    /**
     * Get the list of jobs.
     *
     * @param string|null $organizationId The organization ID (optional).
     * @return array The list of jobs.
     */
    public function getJobsList(?string $organizationId = null): array
    {
        $res = [];
        //List all job offers or filter by organizationId
        if ($organizationId != null) {
            $where = [
                "hiringOrganization" => $organizationId,
            ];
        } else {
            $where = [];
        }
        $jobList = $this->db->findAndSort(JobInterface::COLLECTION, $where, [
            "datePosted" => -1,
        ]);

        //Get the organization hiring detail
        if ($jobList != null) {
            foreach ($jobList as $jobId => $job) {
                if (! empty($job["hiringOrganization"])) {
                    $organization = $this->getModelOrganization()->getById($job["hiringOrganization"]);
                    $job["hiringOrganization"] = $organization;
                    array_push($res, $job);
                }
            }
        }

        return $res;
    }
}
