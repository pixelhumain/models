<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\ZoneInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\SearchNewTrait;
use PixelHumain\Models\Traits\SIGTrait;

class Zone extends BaseModel implements ZoneInterface, SIGTraitInterface, SearchNewTraitInterface, CityTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SIGTrait;
    use SearchNewTrait;
    use CityTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Get a zone by its ID.
     *
     * @param string $id The ID of the zone.
     * @param array $fields The optional fields to include in the result.
     * @return array|null The zone data as an array, or null if not found.
     */
    public function getById(string $id, array $fields = []): ?array
    {
        $zone = $this->db->findOne(ZoneInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ], $fields);
        return $zone;
    }

    /**
     * Retrieves a zone by its key.
     *
     * @param string $key The key of the zone.
     * @return array|null The zone data if found, null otherwise.
     */
    public function getByKey(string $key): ?array
    {
        $zone = $this->db->findOne(ZoneInterface::COLLECTION, [
            "key" => $key,
        ]);
        return $zone;
    }

    /**
     * Get the zones by EPCI ID.
     *
     * @param string $id The EPCI ID.
     * @param array $fields The optional fields to retrieve.
     * @return array|null The array of zones or null if not found.
     */
    public function getByEpci(string $id, array $fields = []): ?array
    {
        $zone = $this->db->findOne(ZoneInterface::COLLECTION, [
            "epci" => $id,
        ], $fields);
        return $zone;
    }

    /**
     * Retrieves the translation by ID and type.
     *
     * @param string $id The ID of the translation.
     * @param string $type The type of the translation.
     * @return array|null The translated data as an array or null if not found.
     */
    public function getTranslateById(string $id, string $type): ?array
    {
        $translate = $this->db->findOne(ZoneInterface::TRANSLATE, [
            "parentId" => $id,
            "parentType" => $type,
        ]);
        return $translate;
    }

    /**
     * Retrieves the zone and its translation by ID.
     *
     * @param string $id The ID of the zone.
     * @return array|null The zone and its translation as an array, or null if not found.
     */
    public function getZoneAndTranslateById(string $id): ?array
    {
        $zone = $this->getById($id);
        $translate = $this->getTranslateById($id, ZoneInterface::COLLECTION);
        $zone["translates"] = $translate["translates"];
        return $zone;
    }

    /**
     * Get the records from the database based on the given parameters.
     *
     * @param array $params The parameters to filter the records.
     * @param array|null $fields The fields to retrieve from the records. If null, all fields will be retrieved.
     * @param int $limit The maximum number of records to retrieve. Default is 20.
     * @return array The retrieved records.
     */
    public function getWhere(array $params, ?array $fields = null, int $limit = 20): array
    {
        $zones = $this->db->findAndSort(ZoneInterface::COLLECTION, $params, [], $limit, $fields);
        return $zones;
    }

    /**
     * Retrieves the details of a zone by its ID.
     *
     * @param string $id The ID of the zone.
     * @return array|null The details of the zone as an array, or null if the zone is not found.
     */
    public function getDetailById(string $id): ?array
    {
        $where = [
            "_id" => $this->db->MongoId($id),
        ];
        $zone = $this->db->findOne(ZoneInterface::COLLECTION, $where);
        return $zone;
    }

    /**
     * Retrieves the level ID by its ID.
     *
     * @param string $id The ID of the level.
     * @param array|null $zone The zone array (optional).
     * @param string|null $type The type of the zone (optional).
     * @return array The level ID.
     */
    public function getLevelIdById(string $id, ?array $zone = null, ?string $type = null): array
    {
        if (empty($zone)) {
            $where = [
                "_id" => $this->db->MongoId($id),
            ];
            $fields = ["name", "level", "level1", "level1Name", "level2", "level2Name", "level3", "level3Name", "level4", "level4Name", "", "", "", "", "", "", ""];
            $zone = $this->db->findOne($type, $where, $fields);
        }

        if (! empty($zone["level1"])) {
            $res = [
                "level1" => $zone["level1"],
                "level1Name" => ((empty($zone["level1Name"]) && in_array("1", $zone["level"])) ? $zone["name"] : @$zone["level1Name"]),
            ];
        } else {
            $res = [];
        }

        if (! empty($zone["level2"])) {
            $res["level2"] = $zone["level2"];
            $res["level2Name"] = ((empty($zone["level2Name"]) && in_array("2", $zone["level"])) ? $zone["name"] : @$zone["level2Name"]);
        }
        if (! empty($zone["level3"])) {
            $res["level3"] = $zone["level3"];
            $res["level3Name"] = ((empty($zone["level3Name"]) && in_array("3", $zone["level"])) ? $zone["name"] : @$zone["level3Name"]);
        }
        if (! empty($zone["level4"])) {
            $res["level4"] = $zone["level4"];
            $res["level4Name"] = ((empty($zone["level4Name"]) && in_array("4", $zone["level"])) ? $zone["name"] : @$zone["level4Name"]);
        }

        return $res;
    }

    /**
     * Create a new level.
     *
     * @param string      $name         The name of the level.
     * @param string      $countryCode  The country code.
     * @param string      $level        The level.
     * @param string|null $level2       Optional second level.
     * @param string|null $level3       Optional third level.
     *
     * @return array The created level.
     */
    public function createLevel(string $name, string $countryCode, string $level, ?string $level2 = null, ?string $level3 = null): array
    {
        $zoneNominatim = [];
        $zone = [];

        $state = false;
        $county = false;

        if ($level == "2") {
            $state = true;
        } elseif ($level == "3") {
            if ($countryCode == "BE") {
                $county = true;
            } else {
                $state = true;
            }
        } elseif ($level == "4") {
            $county = true;
        }

        $zoneNominatim = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, null, $countryCode, null, true, true, $name, $state, $county), true);

        if (empty($zoneNominatim)) {
            $resNominatim = json_decode($this->getModelSIG()->getGeoByAddressNominatim(null, null, $name, $countryCode, null, true, true), true);
            if (! empty($resNominatim)) {
                foreach ($resNominatim as $key => $value) {
                    if (empty($value["address"]["city"])) {
                        $zoneNominatim = [$value];
                    }
                }
            }
        }

        if (! empty($zoneNominatim)) {
            $zone["name"] = $name;
            $zone["countryCode"] = $countryCode;
            $zone["level"] = [$level];
            if ($level != "1") {
                $zone["level1"] = $this->getIdCountryByCountryCode($countryCode);
                if ($level != "2" && ! empty($level2)) {
                    $zone["level2"] = $this->getIdLevelByNameAndCountry($level2, "2", $countryCode);
                }
                if ($level != "3" && ! empty($level3)) {
                    $zone["level3"] = $this->getIdLevelByNameAndCountry($level3, "3", $countryCode);
                }
            }

            $zone["geo"] = $this->getModelSIG()->getFormatGeo($zoneNominatim[0]["lat"], $zoneNominatim[0]["lon"]);
            $zone["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($zoneNominatim[0]["lat"], $zoneNominatim[0]["lon"]);

            if (! empty($zoneNominatim[0]["osm_id"])) {
                $zone["osmID"] = $zoneNominatim[0]["osm_id"];
            }
            if (! empty($zoneNominatim[0]["extratags"]["wikidata"])) {
                $zone["wikidataID"] = $zoneNominatim[0]["extratags"]["wikidata"];
            }
        }
        return $zone;
    }

    /**
     * Save the zone.
     *
     * @param array $zone The zone data.
     * @return array The saved zone data.
     */
    public function save(array $zone = []): array
    {
        $res = [
            "result" => false,
            "error" => "400",
            "msg" => "error",
        ];
        if (! empty($zone)) {
            $zone = $this->db->insert(ZoneInterface::COLLECTION, $zone);
            $this->insertTranslate(
                (string) $zone["_id"],
                ZoneInterface::COLLECTION,
                $zone["countryCode"],
                $zone["name"],
                (! empty($zone["osmID"]) ? $zone["osmID"] : null),
                (! empty($zone["wikidataID"]) ? $zone["wikidataID"] : null)
            );

            $res = [
                "result" => true,
                "msg" => "création Country",
                "zone" => $zone,
            ];
        }
        return $res;
    }

    /**
     * Crée une clé à partir d'un tableau de zone.
     *
     * @param array $zone Le tableau de zone.
     * @return string La clé créée.
     */
    public function createKey(array $zone): string
    {
        $key = $zone["countryCode"];
        if (in_array("1", $zone["level"])) {
            $key .= "@" . (string) $zone["_id"];
        } else {
            $key .= "@" . ((empty($zone["level1"])) ? "" : $zone["level1"]);
        }

        if (in_array("2", $zone["level"]) || in_array("3", $zone["level"]) || in_array("4", $zone["level"])) {
            if (in_array("2", $zone["level"])) {
                $key .= "@" . (string) $zone["_id"];
            } else {
                $key .= "@" . ((empty($zone["level2"])) ? "" : $zone["level2"]);
            }

            if (in_array("3", $zone["level"]) || in_array("4", $zone["level"])) {
                if (in_array("3", $zone["level"])) {
                    $key .= "@" . (string) $zone["_id"];
                } else {
                    $key .= "@" . ((empty($zone["level3"])) ? "" : $zone["level3"]);
                }

                if (in_array("4", $zone["level"])) {
                    $key .= "@" . (string) $zone["_id"];
                }
            }
        }
        return $key;
    }

    /**
     * Retrieves the country based on the country code.
     *
     * @param string|array $countryCode The country code.
     * @return array|null The country name or null if not found.
     */
    public function getCountryByCountryCode($countryCode): ?array
    {
        if (is_array($countryCode)) {
            $where = [
                "countryCode" => [
                    '$in' => $countryCode,
                ],
                "level" => "1",
            ];
        } else {
            $where = [
                "countryCode" => $countryCode,
                "level" => "1",
            ];
        }
        $country = $this->db->findOne(ZoneInterface::COLLECTION, $where);
        return $country;
    }

    /**
     * Get the ID of a country by its country code.
     *
     * @param string|array $countryCode The country code.
     * @return string|null The ID of the country, or null if not found.
     */
    public function getIdCountryByCountryCode($countryCode): ?string
    {
        $country = $this->getCountryByCountryCode($countryCode);
        return ((empty($country["_id"])) ? null : (string) $country["_id"]);
    }

    /**
     * Retrieves the level of a zone by its name and country code.
     *
     * @param string $name The name of the zone.
     * @param string $level The level of the zone.
     * @param string|array $countryCode The country code of the zone.
     * @return array|null The zone or null if not found.
     */
    public function getLevelByNameAndCountry(string $name, string $level, $countryCode): ?array
    {
        if (is_array($countryCode)) {
            $where = [
                "countryCode" => [
                    '$in' => $countryCode,
                ],
                "level" => $level,
                "name" => $this->db->MongoRegex("/^{$name}$/i"),
            ];
        } else {
            $where = [
                "countryCode" => $countryCode,
                "level" => $level,
                "name" => $this->db->MongoRegex("/^{$name}$/i"),
            ];
        }

        $zone = $this->db->findOne(ZoneInterface::COLLECTION, $where);
        return $zone;
    }

    /**
     * Get the ID level by name and country code.
     *
     * @param string $name The name of the zone.
     * @param string $level The level of the zone.
     * @param string|null $countryCode The country code.
     * @return string|null The ID level if found, null otherwise.
     */
    public function getIdLevelByNameAndCountry(string $name, string $level, $countryCode): ?string
    {
        $zone = $this->getLevelByNameAndCountry($name, $level, $countryCode);
        return ((empty($zone["_id"])) ? null : (string) $zone["_id"]);
    }

    // Todo : $idZone n'est pas utilisé
    // Todo : la fonction n'a pas l'aire d'être utilisé, je la commente pour le moment
    // public function getAreaAdministrative($countryCode, $level, $idZone = null, ?string $idInCountry = null, ?string $name = null)
    // {
    //     $where = [
    //         "countryCode" => $countryCode,
    //         "level" => $level,
    //     ];

    //     $zone = [];
    //     if (! empty($idInCountry) || ! empty($name)) {
    //         if (! empty($idInCountry)) {
    //             $where["idInCountry"] = $idInCountry;
    //         }

    //         if (! empty($name)) {
    //             $where["name"] = $name;
    //         }

    //         $zone = $this->db->findOne(self::COLLECTION, $where);
    //     }

    //     return $zone;
    // }

    /**
     * Inserts a translated zone into the database.
     *
     * @param string $parentId The ID of the parent zone.
     * @param string $parentType The type of the parent zone.
     * @param string $countryCode The country code of the translated zone.
     * @param mixed $origin The origin of the translated zone.
     * @param string|null $osmID The OSM ID of the translated zone (optional).
     * @param string|null $wikidataID The Wikidata ID of the translated zone (optional).
     * @return array The inserted translated zone.
     */
    public function insertTranslate(string $parentId, string $parentType, string $countryCode, $origin, ?string $osmID = null, ?string $wikidataID = null): array
    {
        $res = [
            "result" => false,
        ];
        $translate = [];
        $info = [];

        if ($parentType != ZoneInterface::COLLECTION && $parentType != CityInterface::COLLECTION) {
            if (! empty($osmID)) {
                $zoneNominatim = json_decode($this->getModelSIG()->getUrl("http://nominatim.openstreetmap.org/lookup?format=json&namedetails=1&osm_ids=R" . $osmID), true);

                if (! empty($zoneNominatim) && ! empty($zoneNominatim[0]["namedetails"])) {
                    foreach ($zoneNominatim[0]["namedetails"] as $keyName => $valueName) {
                        $arrayName = explode(":", $keyName);
                        if (! empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName) {
                            $translate[strtoupper($arrayName[1])] = $valueName;
                        }
                    }
                }
            }
        }

        if (! empty($wikidataID)) {
            $zoneWiki = json_decode($this->getModelSIG()->getUrl("https://www.wikidata.org/wiki/Special:EntityData/" . $wikidataID . ".json"), true);

            if (! empty($zoneWiki) && ! empty($zoneWiki["entities"][$wikidataID]["labels"])) {
                foreach ($zoneWiki["entities"][$wikidataID]["labels"] as $keyName => $valueName) {
                    if (strlen($keyName) == 2 && ! array_key_exists(strtoupper($keyName), $translate) && $origin != $valueName["value"]) {
                        $translate[strtoupper($keyName)] = $valueName["value"];
                    }
                }
            }
        }

        $info["countryCode"] = $countryCode;
        $info["parentId"] = $parentId;
        $info["parentType"] = $parentType;
        $info["translates"] = $translate;
        $info["origin"] = $origin;

        $info = $this->db->insert(ZoneInterface::TRANSLATE, $info);
        $this->db->update(
            $parentType,
            [
                "_id" => $this->db->MongoId($parentId),
            ],
            [
                '$set' => [
                    "translateId" => (string) $info["_id"],
                ],
            ]
        );
        $res = [
            "result" => true,
            "translate" => $info,
        ];

        return $res;
    }

    /**
     * Returns the name of the country for the given ID.
     *
     * @param string $id The ID of the country.
     * @return string The name of the country.
     */
    public function getNameCountry(string $id): string
    {
        $translates = $this->getTranslateById($id, ZoneInterface::COLLECTION);
        $userT = strtoupper($this->language->get());
        if (! empty($translates)) {
            $name = (! empty($translates["translates"][$userT]) ? $translates["translates"][$userT] : @$translates["origin"]);
        } else {
            $name = "";
        }

        return $name;
    }

    /**
     * Returns the name origin for a given ID.
     *
     * @param string $id The ID of the zone.
     * @return string The name origin.
     */
    public function getNameOrigin(string $id): string
    {
        $translates = $this->getTranslateById($id, ZoneInterface::COLLECTION);
        return $translates["origin"] ?? null;
    }

    /**
     * Get the list of countries.
     *
     * @param bool|null $hasCity Whether to include only countries with cities.
     * @return array The list of countries.
     */
    public function getListCountry(?bool $hasCity = null): array
    {
        $where = [
            "level" => "1",
        ];
        if ($hasCity) {
            $where["hasCity"] = [
                '$exists' => "1",
            ];
        }

        $fields = ["name", "level", "translateId", "countryCode", "hasCity", "geo"];

        $zones = $this->db->find(ZoneInterface::COLLECTION, $where, $fields);

        $translateIds = array_keys($zones);

        $trad = $this->getTranslations($translateIds);

        $res = $this->prepareZones($zones, $trad);

        usort($res, fn ($a, $b) => $this->custom_sort($a, $b));
        return $res;
    }

    /**
     * Retrieves translations for the given translate IDs.
     *
     * @param array $translateIds The array of translate IDs.
     * @return array The array of translations.
     */
    private function getTranslations(array $translateIds): array
    {
        return $this->db->find(
            ZoneInterface::TRANSLATE,
            [
                "parentId" => [
                    '$in' => $translateIds,
                ],
                "parentType" => ZoneInterface::COLLECTION,
            ],
            ["origin", "translates." . strtoupper($this->language->get())]
        );
    }

    /**
     * Prepare the zones.
     *
     * @param array $zones The zones to prepare.
     * @param array $trad The translations for the zones.
     * @return array The prepared zones.
     */
    private function prepareZones(array $zones, array $trad): array
    {
        $res = [];
        foreach ($zones as $key => $zone) {
            if (! empty($zone["translateId"])) {
                $name = $trad[$zone["translateId"]]["translates"][strtoupper($this->language->get())] ?? $trad[$zone["translateId"]]["origin"] ?? null;
                $newZone = [
                    "name" => $name,
                    "countryCode" => $zone["countryCode"],
                    "level" => $zone["level"],
                    "translateId" => $zone["translateId"],
                    "geo" => $zone["geo"] ?? null,
                ];
                $res[$key] = $newZone;
            }
        }
        return $res;
    }

    /**
     * Custom sort function for comparing two values.
     *
     * @param mixed $a The first value to compare.
     * @param mixed $b The second value to compare.
     * @return int Returns a negative integer if $a is less than $b, a positive integer if $a is greater than $b, or 0 if they are equal.
     */
    public function custom_sort($a, $b): int
    {
        $aName = strtolower($this->getModelSearchNew()->accentToRegexSimply($a["name"]));
        $bName = strtolower($this->getModelSearchNew()->accentToRegexSimply($b["name"]));
        return strcasecmp($aName, $bName);
    }

    /**
     * Removes accents from a string.
     *
     * @param string $string The string to remove accents from.
     * @return string The string without accents.
     */
    public function stripAccents(string $string)
    {
        return strtr(
            $string,
            utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'),
            'SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'
        );
    }

    /**
     * Retrieves the records from the database based on the provided parameters.
     *
     * @param array $params The parameters to filter the records.
     * @param array|null $fields The fields to include in the result. If null, all fields will be included.
     * @param int $limit The maximum number of records to retrieve. If 0, all records will be retrieved.
     * @return array The retrieved records.
     */
    public function getWhereTranlate(array $params, ?array $fields = null, int $limit = 0): array
    {
        $zones = $this->db->findAndSort(ZoneInterface::TRANSLATE, $params, [], $limit, $fields);
        return $zones;
    }

    /**
     * Retrieves the scope by its IDs.
     *
     * @param array $params The parameters for retrieving the scope.
     * @return array The scope retrieved by its IDs.
     */
    public function getScopeByIds(array $params): array
    {
        $res = [
            "scopes" => [],
        ];
        if (! empty($params["cities"])) {
            foreach ($params["cities"] as $key => $value) {
                $cp = null;
                if (strpos($value, "cp") != false) {
                    $exp = explode("cp", $value);
                    $cityId = $exp[0];
                    $cp = $exp[1];
                } else {
                    $cityId = $value;
                }
                $city = $this->getModelCity()->getById($cityId);
                $key = (string) $city["_id"] . CityInterface::COLLECTION . (empty($cp) ? "" : $cp);
                $res["scopes"][$key] = $this->createScope($city, CityInterface::COLLECTION, $cp);
            }
        }
        if (! empty($params["zones"])) {
            foreach ($params["zones"] as $key => $zoneId) {
                $zone = $this->getById($zoneId);
                $key = (string) $zone["_id"] . "level" . $this->getLevel($zone);
                $res["scopes"][$key] = $this->createScope($zone, ZoneInterface::COLLECTION, $this->getLevel($zone));
            }
        }
        if (! empty($params["epci"])) {
            foreach ($params["epci"] as $key => $epciId) {
                $zone = $this->getByEpci($epciId);
                $key = (string) $zone["_id"] . "level" . $this->getLevel($zone);
                $res["scopes"][$key] = $this->createScope($zone, ZoneInterface::COLLECTION, $this->getLevel($zone));
            }
        }
        if (! empty($params["cp"])) {
            foreach ($params["cp"] as $key => $value) {
                if (strpos($value, "level") != false) {
                    $cp = substr($value, 0, strlen($value) - 8);
                    $countryCode = substr($value, -8, 2);
                } else {
                    $cp = substr($value, 0, strlen($value) - 2);
                    $countryCode = substr($value, -2, 2);
                }

                $key = $cp . $countryCode;
                $zone = [
                    "id" => $key,
                    "name" => $cp,
                    "type" => "cp",
                    "countryCode" => $countryCode,
                    "active" => true,
                ];

                $res["scopes"][$key] = $this->createScope($zone, "cp");
            }
        }

        return $res;
    }

    /**
     * Crée une portée à partir des données de la zone.
     *
     * @param array $zone Les données de la zone.
     * @param string $type Le type de la portée.
     * @param string|null $cp Le code postal de la portée (optionnel).
     * @return array La portée créée.
     */
    public function createScope(array $zone, string $type, ?string $cp = null): array
    {
        $scope = [
            "id" => (! empty($zone["id"]) ? $zone["id"] : (string) $zone["_id"]),
            "name" => $zone["name"],
            "type" => $type,
            "countryCode" => (! empty($zone["countryCode"]) ? $zone["countryCode"] : $zone["country"]),
            "active" => true,
        ];

        if (isset($zone["geo"])) {
            $scope["latitude"] = $zone["geo"]["latitude"];
            $scope["longitude"] = $zone["geo"]["longitude"];
        }

        if ($type == CityInterface::COLLECTION) {
            if (! empty($cp)) {
                $scope["postalCode"] = $cp;
                $scope["allCP"] = false;
                foreach ($zone["postalCodes"] as $key => $value) {
                    if ($value["postalCode"] == $scope["postalCode"]) {
                        $scope["name"] = $value["name"] . " ( " . $scope["postalCode"] . " ) ";
                        break;
                    }
                }
            } else {
                $scope["allCP"] = true;
            }
        } elseif ($type == ZoneInterface::COLLECTION) {
            if ($cp == null) {
                $cp = $this->getLevel($zone);
            }
            $scope["type"] = "level" . $cp;
            $scope["level"] = $cp;
        }
        return $scope;
    }

    /**
     * Retrieves the level of a zone.
     *
     * @param array $zone The zone data.
     * @return string|null The level of the zone, or null if not found.
     */
    public function getLevel(array $zone): ?string
    {
        $level = null;
        if (in_array("1", $zone["level"])) {
            $level = "1";
        } elseif (in_array("2", $zone["level"])) {
            $level = "2";
        } elseif (in_array("3", $zone["level"])) {
            $level = "3";
        } elseif (in_array("4", $zone["level"])) {
            $level = "4";
        } elseif (in_array("5", $zone["level"])) {
            $level = "5";
        }

        return $level;
    }
}
