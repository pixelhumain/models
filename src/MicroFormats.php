<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\MicroFormatsInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

// TODO : est ce que cette class est encore utilisé ?

/**
 * Class MicroFormats
 *
 * This class represents the MicroFormats model and implements the MicroFormatsInterface.
 * It extends the BaseModel class.
 * @deprecated
 */
class MicroFormats extends BaseModel implements MicroFormatsInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Retrieves records from the database based on the specified parameters.
     *
     * @param array $params The parameters used to filter the records.
     * @param array|null $fields The fields to be retrieved from the records. If null, all fields will be retrieved.
     * @return array The retrieved records.
     */
    public function getWhere(array $params, ?array $fields = null): array
    {
        return $this->db->find(MicroFormatsInterface::COLLECTION, $params, $fields);
    }
}
