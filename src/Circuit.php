<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;

use PixelHumain\Models\Interfaces\CircuitInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\OrderItemTraitInterface;
use PixelHumain\Models\Traits\OrderItemTrait;

// TODO : class qui n'est pas utilisée, à supprimer ?

class Circuit extends BaseModel implements CircuitInterface, DataValidatorTraitInterface, OrderItemTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DataValidatorTrait;
    use OrderItemTrait;

    //TODO Translate
    public static array $orderTypes = [];

    //From Post/Form name to database field name
    public static array $dataBinding = [
        "name" => [
            "name" => "name",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "total" => [
            "name" => "total",
        ],
        "start" => [
            "name" => "start",
        ],
        "end" => [
            "name" => "end",
        ],
        "currency" => [
            "name" => "currency",
        ],
        "capacity" => [
            "name" => "capacity",
        ],
        "frequency" => [
            "name" => "frequency",
        ],
        "description" => [
            "name" => "description",
        ],
        "countQuantity" => [
            "name" => "countQuantity",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "services" => [
            "name" => "services",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Retrieves a circuit by its ID.
     *
     * @param string $id The ID of the circuit.
     * @return array|null The circuit data as an array or null if not found.
     */
    public function getById(string $id): ?array
    {
        $order = $this->db->findOneById(CircuitInterface::COLLECTION, $id);
        return $order;
    }

    /**
     * Get a list of circuits based on the given conditions.
     *
     * @param array $where The conditions to filter the circuits.
     * @return array The list of circuits that match the conditions.
     */
    public function getListBy(array $where): array
    {
        $circuits = $this->db->find(CircuitInterface::COLLECTION, $where);
        return $circuits;
    }

    /**
     * Insère un circuit dans la base de données.
     *
     * @param array $circuit Les données du circuit à insérer.
     * @param string $userId L'identifiant de l'utilisateur.
     * @return array Les données du circuit inséré.
     */
    public function insert(array $circuit, string $userId): array
    {
        // TODO : json_decode json_encode
        try {
            $valid = $this->getModelDataValidator()->validate(CircuitInterface::CONTROLLER, json_decode(json_encode($circuit), true), null);
        } catch (CTKException $e) {
            $valid = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        if ($valid["result"]) {
            $circuit["created"] = $this->db->MongoDate(time());
            settype($circuit["countQuantity"], "integer");
            settype($circuit["capacity"], "integer");
            settype($circuit["total"], "float");
            $circuit = $this->db->insert(CircuitInterface::COLLECTION, $circuit);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Your circuit is well registred"),
                "circuit" => $circuit,
            ];
        } else {
            return [
                "result" => false,
                "error" => "400",
                "msg" => $this->language->t("common", "Something went really bad : " . $valid['msg']),
            ];
        }
    }

    // TODO : ne semble pas utilisée, à supprimer ?
    // /**
    //  * Get a list of circuits by user.
    //  *
    //  * @param array $where The conditions to filter the circuits.
    //  * @return array The list of circuits.
    //  */
    // public function getListByUser(array $where): array
    // {
    //     $allOrders = $this->db->findAndSort(CircuitInterface::COLLECTION, $where, [
    //         "created" => -1,
    //     ]);
    //     return $allOrders;
    // }

    // TODO : ne semble pas utilisée, à supprimer ?
    // /**
    //  * Retrieves an order item by its ID.
    //  *
    //  * @param string $id The ID of the order item.
    //  * @return array The order item data.
    //  */
    // public function getOrderItemById(string $id): array
    // {
    //     $order = $this->getById($id);
    //     $orderItems = [];
    //     if (! empty($order["orderItems"])) {
    //         foreach ($order["orderItems"] as $data) {
    //             $orderItem = $this->getModelOrderItem()->getById((string) $data);
    //             if (! empty($orderItem)) {
    //                 $orderItems[(string) $orderItem["_id"]] = $orderItem;
    //             }
    //         }
    //     }
    //     return $orderItems;
    // }
}
