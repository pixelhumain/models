<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CronInterface;
use PixelHumain\Models\Interfaces\MailErrorInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\Interfaces\NotificationTraitInterface;
use PixelHumain\Models\Traits\NotificationTrait;

class MailError extends BaseModel implements MailErrorInterface, NotificationTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use NotificationTrait;

    //DB field
    public string $id = "";

    public string $event = "";

    public string $recipient = "";

    public string $reason = "";

    public string $personId = "";

    public string $description = "";

    public string $timestamp = "";

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    // TODO : Attention la il y a un constructeur au quel je dois passer $params["config"]["db"] injecter la gestion de la base
    /**
     * Constructor for the MailError class.
     *
     * @param array $params The parameters of the MailError.
     * @param string $from The sender of the email (optional, default value is "hook").
     */
    public function __construct(array $params, string $from = "hook")
    {
        if (empty($params["config"]) || empty($params["config"]["db"])) {
            throw new CTKException("Error in MailError : no db in config");
        }

        // Init the parent
        parent::__construct($params["config"]);

        $event = null;
        //Check the event
        $manageableEvent = [MailErrorInterface::EVENT_DROPPED_EMAIL, MailErrorInterface::EVENT_SPAM_COMPLAINTS, MailErrorInterface::EVENT_BOUNCED_EMAIL];
        $this->event = @$params["event"];

        if (empty($this->event) || ! in_array($this->event, $manageableEvent)) {
            throw new CTKException("Unknown Event in mail error : " . $event);
        }

        //Construct from mongo : get the id and transform the mongoDate
        if ($from == "mongo") {
            if (isset($params["id"])) {
                $this->id = $params["id"];
            }
            if (isset($params["_id"])) {
                $this->id = (string) $params["_id"];
            }
            $this->timestamp = isset($params["timestamp"]) ? $params["timestamp"]->sec : null;
            $this->reason = $params["reason"] ?? null;
            $this->description = $params["description"] ?? null;
        } elseif ($from == "hook") {
            $this->timestamp = $params["timestamp"] ?? null;
            if ($this->event == MailErrorInterface::EVENT_DROPPED_EMAIL) {
                $this->reason = $params["reason"] ?? null;
                $this->description = $params["description"] ?? null;
            } elseif ($this->event == MailErrorInterface::EVENT_BOUNCED_EMAIL) {
                $this->reason = $params["code"] ?? null;
                $this->description = $params["error"] ?? null;
            } elseif ($this->event == MailErrorInterface::EVENT_SPAM_COMPLAINTS) {
                $this->reason = "user complained";
                $this->description = "user click on spam complain";
            }
        }
        //recipient and account
        $this->recipient = $params["recipient"] ?? null;
        if (empty($this->recipient)) {
            throw new CTKException("No email specified");
        }
        $account = $this->db->findOne(PersonInterface::COLLECTION, [
            "email" => $this->recipient,
        ]);
        if (! $account) {
            throw new CTKException("unknown user with that email : " . $this->recipient);
        } else {
            $this->personId = (string) $account["_id"];
        }
    }

    /**
     * Perform an action on the event.
     *
     * @return void
     */
    public function actionOnEvent(): void
    {
        //Spam ou drop : suspension account
        if ($this->event == MailErrorInterface::EVENT_DROPPED_EMAIL || $this->event == MailErrorInterface::EVENT_SPAM_COMPLAINTS) {
            //Set invalid email flag on the person
            $this->db->update(PersonInterface::COLLECTION, [
                "_id" => $this->personId,
            ], [
                '$set' => [
                    "isNotValidEmail" => true,
                ],
            ]);
            //Hard bounce => TODO : try to delete the account
        } elseif ($this->event == MailErrorInterface::EVENT_BOUNCED_EMAIL) {
            $this->db->update(PersonInterface::COLLECTION, [
                "_id" => $this->personId,
            ], [
                '$set' => [
                    "isNotValidEmail" => true,
                    "hardbounced" => true,
                ],
            ]);
        }
        $this->save();

        //add a Notification to Super Admins
        $this->getModelNotification()->actionToAdmin(ActStrInterface::VERB_RETURN, [
            "type" => CronInterface::COLLECTION,
        ], [
            "id" => $this->id,
            "type" => MailErrorInterface::COLLECTION,
            "event" => $this->event,
        ], [
            "id" => $this->personId,
            "type" => PersonInterface::COLLECTION,
            "email" => $this->recipient,
        ]);
    }

    /**
     * Saves the MailError object.
     *
     * @return void
     */
    public function save(): void
    {
        $mailError = [
            "event" => $this->event,
            "recipient" => $this->recipient,
            "personId" => $this->personId,
            "reason" => $this->reason,
            "description" => $this->description,
            "timestamp" => $this->db->MongoDate($this->timestamp),
        ];
        $mailError = $this->db->insert(MailErrorInterface::COLLECTION, $mailError);
        if ($mailError) {
            $this->id = $mailError["_id"];
        } else {
            throw new CTKException("Error inserting a MailError ! ");
        }
    }

    /**
     * Returns an array of mail errors since a given timestamp.
     *
     * @param int $sinceTS The timestamp to retrieve mail errors since.
     * @return array An array of mail errors.
     */
    public function getMailErrorSince(int $sinceTS): array
    {
        $mailErrors = [];
        $dbMailError = $this->db->findAndSort(MailErrorInterface::COLLECTION, [
            "timestamp" => [
                '$gt' => $this->db->MongoDate($sinceTS),
            ],
        ], [
            "timestamp" => -1,
        ]);

        foreach ($dbMailError as $mailErrorId => $aMailError) {
            $aMailError["config"] = [
                "db" => $this->db,
            ];
            $mailError = new MailError($aMailError, "mongo");
            $mailErrors[$mailErrorId] = $mailError;
        }

        return $mailErrors;
    }
}
