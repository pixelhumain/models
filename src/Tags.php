<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\TagsInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;

class Tags extends BaseModel implements TagsInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Filters and saves new tags.
     *
     * @param array|null $tags The array of tags to filter and save.
     * @return array The filtered and saved tags.
     */
    public function filterAndSaveNewTags(?array $tags): array
    {
        // Retourne un tableau vide si $tags n'est pas un tableau ou est vide
        if (! is_array($tags) || empty($tags)) {
            return [];
        }

        // Filtrer les tags non vides
        $filteredTags = array_filter($tags, fn ($tag) => ! empty($tag));

        // Transformer chaque tag et insérer les tags actifs
        return array_map(function ($tag) {
            $newTags = $this->checkAndGetTag($tag);
            $this->insertActiveTags($newTags);
            return $newTags;
        }, $filteredTags);
    }

    /**
     * Inserts an active tag into the database.
     *
     * @param string $tag The tag to be inserted.
     * @return bool Returns true if the tag was successfully inserted, false otherwise.
     */
    public function insertActiveTags(string $tag): bool
    {
        $tagOne = $this->db->findOne(TagsInterface::COLLECTION, [
            "tag" => $tag,
        ], ['tag']);
        if (empty($tagOne)) {
            $data = [
                "tag" => $tag,
            ];
            $this->db->insert(TagsInterface::COLLECTION, $data);
        }
        return true;
    }

    /**
     * Check and get a tag.
     *
     * @param string $tag The tag to check and get.
     * @return string The checked and retrieved tag.
     */
    public function checkAndGetTag(string $tag): string
    {
        $carac = ["\r\n", "\n"];
        $newtags = str_replace($carac, " ", $tag);
        $newtags = str_replace("#", "", $newtags);
        $newtags = trim($newtags);
        return $newtags;
    }
}
