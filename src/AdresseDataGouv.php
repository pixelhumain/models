<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\AdresseDataGouvInterface;

use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\SIGTrait;

class AdresseDataGouv extends BaseModel implements AdresseDataGouvInterface, SIGTraitInterface, CityTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SIGTrait;
    use CityTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Retrieves an address using the DataGouv API.
     *
     * @param array $params The parameters for the address search.
     * @return mixed The address data retrieved from the DataGouv API.
     */
    public function getAddressByDataGouv(array $params)
    {
        try {
            $url = "https://api-adresse.data.gouv.fr/search/";
            if (! empty($params['text']) && is_string($params['text'])) {
                $url .= "?q=" . urlencode($params['text']);
                return json_decode((string) $this->getModelSIG()->getUrl($url), true);
            }
            return null;
        } catch (CTKException $e) {
            return null;
        }
    }

    /**
     * Parse an address using the given data and parameters.
     *
     * @param array $data The data to parse.
     * @param array $params The parameters to use for parsing.
     * @return array The parsed address.
     */
    public function parseAddress(array $data, array $params): array
    {
        $res = [];
        if (! empty($data) && ! empty($data["features"])) {
            foreach ($data["features"] as $key => $val) {
                $city = null;
                if (! empty($val["properties"])) {
                    if (! empty($val["properties"]["citycode"])) {
                        $city = $this->db->findOne(
                            CityInterface::COLLECTION,
                            [
                                "insee" => $val["properties"]["citycode"],
                                "country" => [
                                    '$in' => $params["countryCode"],
                                ],
                            ]
                        );
                    }

                    if (! empty($city)) {
                        $res[] = $this->getModelCity()->createLocality($city, $val["properties"]["name"], $val["geometry"]["coordinates"][1], $val["geometry"]["coordinates"][0], $val["properties"]["postcode"]);
                    }
                }
            }
        }
        return $res;
    }
}
