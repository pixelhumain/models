<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\OrderInterface;
use PixelHumain\Models\Interfaces\OrderItemInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CommentTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\Interfaces\CommentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;

class OrderItem extends BaseModel implements OrderItemInterface, CommentTraitInterface, DocumentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;
    use SessionTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CommentTrait;
    use DocumentTrait;

    //TODO Translate
    public static array $orderTypes = [];

    //From Post/Form name to database field name
    public static array $dataBinding = [
        "section" => [
            "name" => "section",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "name" => [
            "name" => "name",
            "rules" => ["required"],
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
        ],
        "geoPosition" => [
            "name" => "geoPosition",
        ],
        "description" => [
            "name" => "description",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "price" => [
            "name" => "price",
        ],
        "devise" => [
            "name" => "devise",
        ],
        "contactInfo" => [
            "name" => "contactInfo",
            "rules" => ["required"],
        ],
        "toBeValidated" => [
            "name" => "toBeValidated",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
        $this->validateSessionProperty();
    }

    /**
     * Retrieves an order item by its ID.
     *
     * @param string $id The ID of the order item.
     * @return array|null The order item data as an array, or null if not found.
     */
    public function getById(string $id): ?array
    {
        $orderItem = $this->db->findOneById(OrderItemInterface::COLLECTION, $id);

        if (empty($orderItem) || empty($orderItem["orderedItemType"]) || empty($orderItem["orderedItemId"])) {
            return null;
        }

        if (! empty($orderItem["comment"]) && is_string($orderItem["comment"])) {
            $orderItem["comment"] = $this->getModelComment()->getById($orderItem["comment"]);
        }
        $orderedItem = $this->db->findOneById($orderItem["orderedItemType"], $orderItem["orderedItemId"]);

        if (empty($orderedItem)) {
            return null;
        }

        $orderItem["name"] = $orderedItem["name"];
        $orderItem["description"] = $orderedItem["description"] ?? null;
        // TODO : use fonction from DocumentTrait
        $orderItem = array_merge($orderItem, $this->getModelDocument()->retrieveAllImagesUrl($orderItem["orderedItemId"], $orderItem["orderedItemType"]));
        return $orderItem;
    }

    /**
     * Retrieves order items by array of IDs.
     *
     * @param array $arrayId The array of IDs to retrieve order items for.
     * @param array $fields  Optional. The fields to include in the retrieved order items. Default is an empty array.
     *
     * @return array The retrieved order items.
     */
    public function getByArrayId(array $arrayId, array $fields = []): array
    {
        $orderItem = $this->db->find(OrderItemInterface::COLLECTION, [
            "_id" => [
                '$in' => $arrayId,
            ],
        ], $fields);
        return $orderItem;
    }

    /**
     * Get a list of OrderItems based on the given conditions.
     *
     * @param array $where The conditions to filter the OrderItems.
     * @return array The list of OrderItems that match the conditions.
     */
    public function getListBy(array $where): array
    {
        $products = $this->db->find(OrderItemInterface::COLLECTION, $where);
        return $products;
    }

    /**
     * Get a list of order items by user.
     *
     * @param array $where The conditions to filter the order items.
     * @return array The list of order items.
     */
    public function getListByUser(array $where): array
    {
        $allOrders = $this->db->findAndSort(OrderItemInterface::COLLECTION, $where, [
            "created" => -1,
        ]);
        foreach ($allOrders as $key => $value) {
            $orderedItem = $this->db->findOneById($value["orderedItemType"], $value["orderedItemId"]);
            if (@$value["comment"]) {
                $allOrders[$key]["comment"] = $this->getModelComment()->getById($value["comment"]);
            }

            $allOrders[$key]["name"] = $orderedItem["name"];
            $allOrders[$key]["description"] = @$orderedItem["description"];
            $allOrders[$key]["profilImageUrl"] = @$orderedItem["profilImageUrl"];
            $allOrders[$key]["profilThumbImageUrl"] = @$orderedItem["profilThumbImageUrl"];
            $allOrders[$key]["profilMediumImageUrl"] = @$orderedItem["profilMediumImageUrl"];
        }
        return $allOrders;
    }

    /**
     * Inserts a new order item into the database.
     *
     * @param string $orderedItemId The ID of the ordered item.
     * @param array $orderedItemData The data of the ordered item.
     * @param string $userId The ID of the user.
     * @return array The inserted order item data.
     */
    public function insert(string $orderedItemId, array $orderedItemData, string $userId): array
    {
        $orderedItemData["customerId"] = $userId;
        $orderedItemData["orderedItemId"] = $orderedItemId;
        $orderedItemData["created"] = $this->db->MongoDate(time());
        settype($orderedItemData["quantity"], "integer");
        settype($orderedItemData["price"], "float");
        if (@$orderedItemData["reservations"]) {
            $reservations = [];
            foreach ($orderedItemData["reservations"] as $key => $value) {
                $value["date"] = $this->db->MongoDate(strtotime($key));
                array_push($reservations, $value);
            }
            $orderedItemData["reservations"] = $reservations;
        }

        $orderedItemData = $this->db->insert(OrderItemInterface::COLLECTION, $orderedItemData);
        return [
            "res" => true,
            "msg" => $this->language->t("common", "Your orderItem is well reistred"),
            "id" => (string) $orderedItemData["_id"],
        ];
    }

    /**
     * Action to rate an order item.
     *
     * @param array $params The parameters for rating.
     * @param string $commentId The ID of the comment to be rated.
     * @return void
     */
    public function actionRating(array $params, string $commentId): void
    {
        $allRating = $this->getModelComment()->buildCommentsTree($params["contextId"], $params["contextType"], $this->session->getUSerId(), ["rating"]);
        $sum = 0;
        foreach ($allRating["comments"] as $key => $value) {
            $sum = $sum + $value["rating"];
        }
        if ($allRating["nbComment"] != 0) {
            $sum = $sum / $allRating["nbComment"];
        }
        $average = round($sum, 1);
        $this->db->update($params["contextType"], [
            "_id" => $this->db->MongoId($params["contextId"]),
        ], [
            '$set' => [
                "averageRating" => $average,
            ],
        ]);
        $this->db->update(OrderItemInterface::COLLECTION, [
            "_id" => $this->db->MongoId($params["orderId"]),
        ], [
            '$set' => [
                "comment" => $commentId,
            ],
        ]);
    }
}
