<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonInterface;

class TranslateGeoJson implements TranslateGeoJsonInterface
{
    /*Exemple type d'un fichier GeoJSON

        {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [ 102, 0.5 ]
                    },
                    "properties": {
                        "prop0": "value0"
                    }
                },
                {
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [55,0.5]
                    },
                    "properties": {
                        "prop0": "value0"
                    }
                }

            ]
        }
    */
    public static array $dataBinding_news = [
        "type" => "Feature",
        "geometry" => [
            "type" => "Point",
            "coordinates" => [
                "valueOf" => "scope.cities.0.geo",
            ],
        ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allOrganization = [
        "type" => "Feature",
        "geometry" => [
            "type" => "Point",
            "coordinates" => [
                "valueOf" => "geo",
            ],
        ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_organization = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allEvent = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_event = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allProject = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_project = [
        "type" => "Feature",
        "geometry" =>
                            [
                                "type" => "Point",
                                "coordinates" => [
                                    "valueOf" => "geo",
                                ],
                            ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allPerson = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_person = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    public static array $dataBinding_city = [
        "type" => "Feature",
        "geometry" =>
 [
     "type" => "Point",
     "coordinates" => [
         "valueOf" => "geo",
     ],
 ],
        "properties" => [
            "type" => "properties",
            [
                "email" => [
                    "valueOf" => "email",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "username" => [
                    "valueOf" => "username",
                ],
                "img" => [
                    "valueOf" => "profilImageUrl",
                    "type" => "url",
                    "prefix" => "/",
                ],
            ],
        ],
    ];

    /**
     * Retrieves the coordinates from a GeoJSON array.
     *
     * @param array $val The GeoJSON array.
     * @param array|null $bindPath The path to bind the coordinates.
     * @return array|null The extracted coordinates.
     */
    public function getGeojsonCoor(array $val, ?array $bindPath): ?array
    {
        if (isset($bindPath["type"]) && $bindPath["type"] == "Point") {
            if ((isset($val["coordinates"]["latitude"])) && (isset($val["coordinates"]["longitude"]))) {
                // Damien : 1 ligne
                $latitude = $val["coordinates"]["latitude"];
                $longitude = $val["coordinates"]["longitude"];

                $latitude = floatval($latitude);
                $longitude = floatval($longitude);

                $val["coordinates"] = [];
                array_push($val["coordinates"], $longitude);
                array_push($val["coordinates"], $latitude);
                // Fin Damien : 1 ligne
            } elseif (! isset($val["coordinates"]["latitude"], $val["coordinates"]["longitude"])) {
                unset($val);
                return null;
            }
        }
        return $val;
    }

    //TODO : $bindPath is not used
    /**
     * Retrieves the properties from a GeoJSON array.
     *
     * @param array $val The GeoJSON array.
     * @param array|null $bindPath The optional bind path array.
     * @return array The properties array.
     */
    public function getGeojsonProperties(array $val, ?array $bindPath = null): array
    {
        if (isset($val["0"]["name"])) {
            $val["name"] = $val["0"]["name"];
            //var_dump($val);
        }
        if (isset($val["0"]["username"])) {
            $val["username"] = $val["0"]["username"];
        }
        if (isset($val["0"]["email"])) {
            $val["email"] = $val["0"]["email"];
        }
        if (isset($val["0"]["img"])) {
            $val["img"] = $val["0"]["img"];
        }

        unset($val["0"]);

        unset($val["type"]);

        return $val;
    }
}
