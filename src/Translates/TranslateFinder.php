<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateFinderInterface;

class TranslateFinder implements TranslateFinderInterface
{
    /*

        ----------------- COMMUNECTER -----------------
    */

    // TODO : $type not used
    /**
     * Builds a translation based on the given data and type.
     *
     * @param array $data The data used to build the translation.
     * @param string $type The type of translation to build.
     * @return array The built translation.
     */
    public function build(array $data, string $type = null): array
    {
        $person = [];
        $orgas = [];
        $projects = [];
        $events = [];
        foreach ($data as $keyID => $valueData) {
            if (is_array($valueData)) {
                $information = [[
                    "label" => "my ID :" . $keyID,
                ], [
                    "label" => "email : " . (! empty($valueData["email"]) ? (string) $valueData["email"] : "N/A"),
                ], [
                    "label" => "username : " . (! empty($valueData["username"]) ? (string) $valueData["username"] : "N/A"),
                ], [
                    "label" => "country : " . (! empty($valueData["address"]["addressCountry"]) ? (string) $valueData["address"]["addressCountry"] : "N/A"),
                ], [
                    "label" => "city : " . (! empty($valueData["address"]["addressLocality"]) ? (string) $valueData["address"]["addressLocality"] : "N/A"),
                ], [
                    "label" => "postalCode : " . (! empty($valueData["address"]["postalCode"]) ? (string) $valueData["address"]["postalCode"] : "N/A"),
                ], [
                    "label" => "see in map : http://www.openstreetmap.org/?mlat=" . (! empty($valueData["geo"]["latitude"]) ? (string) $valueData["geo"]["latitude"] : "N/A") . "&mlon=" . (! empty($valueData["geo"]["longitude"]) ? (string) $valueData["geo"]["longitude"] : "N/A") . "&zoom=12",
                ]];

                if (! empty($valueData["shortDescription"])) {
                    $information[] = [
                        "label" => "shortDescription" . (string) $valueData["shortDescription"],
                        "size" => 3000,
                    ];
                }
                if (! empty($valueData["description"])) {
                    $information[] = [
                        "label" => "description" . (string) $valueData["description"],
                        "size" => 3000,
                    ];
                }

                $person = [
                    "label" => $valueData["name"] ?? "",
                    "icon" => "user ",
                    "children" => $information,
                ];

                if (! empty($valueData["links"]["memberOf"])) {
                    $elements = [];
                    foreach ($valueData["links"]["memberOf"] as $ix => $o) {
                        if (is_array($o)) {
                            $el = [
                                "label" => (string) $o["name"],
                                "url" => ! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : "",
                            ];
                            $elements[] = $el;
                        }
                    }
                    $orgas = [
                        "label" => "Organizations",
                        "icon" => "users ",
                        "children" => $elements,
                    ];
                }

                if (! empty($valueData["links"]["projects"])) {
                    $elements = [];
                    foreach ($valueData["links"]["projects"] as $ix => $o) {
                        if (is_array($o)) {
                            $el = [
                                "label" => (string) $o["name"],
                                "url" => ! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : "",
                            ];
                            $elements[] = $el;
                        }
                    }
                    $projects = [
                        "label" => "Projects",
                        "icon" => "lightbulb-o ",
                        "children" => $elements,
                    ];
                }

                if (! empty($valueData["links"]["events"])) {
                    $elements = [];
                    foreach ($valueData["links"]["events"] as $ix => $o) {
                        if (is_array($o)) {
                            $el = [
                                "label" => (string) $o["name"],
                                "url" => ! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : "",
                            ];
                            $elements[] = $el;
                        }
                    }
                    $events = [
                        "label" => "Events",
                        "icon" => "calendar",
                        "children" => $elements,
                    ];
                }

                /*	"# Points of interests"."\n".
                    "# Ressources : "."\n".
                    "# Places : "."\n".
                    "# Classifieds : "; */
            }
        }

        return [$person, $orgas, $projects, $events];
    }
}
