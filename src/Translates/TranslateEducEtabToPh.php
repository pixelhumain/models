<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateEducEtabToPhInterface;

class TranslateEducEtabToPh implements TranslateEducEtabToPhInterface
{
    public static array $mapping = [
        "fields.libelle" => "name",
        "fields.geolocalisation.0" => "geo.latitude",
        "fields.geolocalisation.1" => "geo.longitude",
        "fields.site_web" => "url",
    ];
}
