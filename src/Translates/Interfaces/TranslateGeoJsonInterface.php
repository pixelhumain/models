<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateGeoJsonInterface
{
    /**
     * Retrieves the coordinates from a GeoJSON array.
     *
     * @param array $val The GeoJSON array.
     * @param array|null $bindPath The path to bind the coordinates.
     * @return array|null The extracted coordinates.
     */
    public function getGeojsonCoor(array $val, ?array $bindPath): ?array;

    //TODO : $bindPath is not used
    /**
     * Retrieves the properties from a GeoJSON array.
     *
     * @param array $val The GeoJSON array.
     * @param array|null $bindPath The optional bind path array.
     * @return array The properties array.
     */
    public function getGeojsonProperties(array $val, ?array $bindPath = null): array;
}
