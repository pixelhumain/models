<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateKmlInterface
{
    // TODO : $bindPath is not used
    /**
     * Retrieves the KML coordinates.
     *
     * @param array $val The input array.
     * @param array|null $bindPath The optional bind path array.
     * @return array|null The KML coordinates array or null if not found.
     */
    public function getKmlCoor(array $val, ?array $bindPath = null): ?array;

    /**
     * Formats the given value based on its type.
     *
     * @param array $val The value to be formatted.
     * @param array|null $bindPath The optional bind path.
     * @return string The formatted value.
     */
    public function specFormatByType(array $val, ?array $bindPath = null): string;
}
