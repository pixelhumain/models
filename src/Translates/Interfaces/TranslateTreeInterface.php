<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateTreeInterface
{
    /**
     * Build a translated tree from the given data.
     *
     * @param array $data The data to build the tree from.
     * @param string $type The type of the tree.
     * @return array The translated tree.
     */
    public function build(array $data, string $type): array;
}
