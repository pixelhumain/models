<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateFinderInterface
{
    /**
     * Builds a translation based on the given data and type.
     *
     * @param array $data The data used to build the translation.
     * @param string $type The type of translation to build.
     * @return array The built translation.
     */
    public function build(array $data, string $type = null): array;
}
