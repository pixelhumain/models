<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateFtlInterface
{
    /**
     * Returns the opening hours as a string.
     *
     * @param array|null $hours The opening hours.
     * @return string The opening hours as a string.
     */
    public function openingHours(?array $hours = null): string;

    /**
     * This method is used to determine the type of place.
     *
     * @param array $val The input array containing the values.
     * @return array The array containing the type of place.
     */
    public function typePlace(array $val): array;
}
