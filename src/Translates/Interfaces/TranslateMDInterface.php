<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateMDInterface
{
    /**
     * TranslateMD class.
     *
     * This class provides a method to translate an array of data into a string representation based on the specified type.
     *
     * @param array $data The data to be translated.
     * @param string $type The type of translation to be performed.
     *
     * @return string|null The translated string representation of the data, or null if the translation failed.
     */
    public function MD(array $data, string $type): ?string;
}
