<?php

namespace PixelHumain\Models\Translates\Interfaces;

interface TranslateMediaWikiInterface
{
    /**
     * Binds data to the TranslateMediaWiki class.
     *
     * @param string $categorie The category of the data.
     * @param string $wikiName The name of the wiki.
     * @return array The array of data bindings.
     */
    public function dataBinding(string $categorie, string $wikiName): array;

    /**
     * Returns the base map for the given type.
     *
     * @param string $type The type of map.
     * @return array The base map.
     */
    public function baseMap(string $type): array;
}
