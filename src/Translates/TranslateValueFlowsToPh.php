<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsToPhInterface;

class TranslateValueFlowsToPh implements TranslateValueFlowsToPhInterface
{
    public static array $mapping_valueflows = [
        "fields.name" => "name",
        "fields.note" => "description",
        "fields.url" => "url",
    ];
}
