<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Translates\Interfaces\TranslateFtlInterface;

class TranslateFtl extends BaseModel implements TranslateFtlInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */



    /*

        ----------------- COMMUNECTER -----------------
    */

    public static array $dataBinding_allOrganization = [
        "id_communecter" => [
            "valueOf" => '_id.$id',
        ],
        "nom" => [
            "valueOf" => "name",
        ],
        "statut" => [
            "valueOf" => "state",
        ],
        "type_lieu" => [
            "valueOf" => "tags",
            "type" => "typePlace",
        ],
        "description_courte" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "logo" => [
            "valueOf" => "image",
            "type" => "url",
            "toArray" => true,
        ],
        "photo" => [
            "valueOf" => "image",
            "type" => "url",
            "toArray" => true,
        ],
        "video" => "A définir",
        "reseau_sociaux" => [
            "valueOf" => "socialNetwork",
        ],
        "site_internet" => [
            "valueOf" => 'url',
        ],
        "address" => [
            "valueOf" => [
                "streetAddress" => [
                    "valueOf" => "address.streetAddress",
                ],
                "code_insee" => [
                    "valueOf" => "address.codeInsee",
                ],
                "code_postal" => [
                    "valueOf" => "address.postalCode",
                ],
                "commune" => [
                    "valueOf" => "address.addressLocality",
                ],
                "pays" => [
                    "valueOf" => "address.addressCountry",
                ],
                "latitude" => [
                    "valueOf" => "geoPosition.coordinates.1",
                ],
                "longitude" => [
                    "valueOf" => "geoPosition.coordinates.0",
                ],
            ],
        ],
        "horaires_ouverture" => [
            "valueOf" => 'openingHours',
            "type" => "openingHours",
        ],
        "nature_juridique" => [
            "valueOf" => "manageModel",
        ],
        "contact_email" => [
            "valueOf" => 'email',
        ],
        "contact_telephone" => [
            "valueOf" => 'telephone.fixe.0',
        ],
        "reseaux" => [
            "valueOf" => 'network',
        ],
        "taille" => [
            "valueOf" => 'spaceSize',
        ],
    ];

    public static array $dataBinding_organization = [
        "id_communecter" => [
            "valueOf" => '_id.$id',
        ],
        "nom" => [
            "valueOf" => "name",
        ],
        "statut" => [
            "valueOf" => "state",
        ],
        "type_lieu" => [
            "valueOf" => "tags",
            "type" => "typePlace",
        ],
        "description_courte" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "logo" => "A définir",
        "photo" => [
            "valueOf" => "image",
            "type" => "url",
            "toArray" => true,
        ],
        "video" => "A définir",
        "reseau_sociaux" => "A définir",
        "site_internet" => [
            "valueOf" => 'url',
        ],
        "address" => [
            "valueOf" => [
                "streetAddress" => [
                    "valueOf" => "address.streetAddress",
                ],
                "code_insee" => [
                    "valueOf" => "address.codeInsee",
                ],
                "code_postal" => [
                    "valueOf" => "address.postalCode",
                ],
                "commune" => [
                    "valueOf" => "address.addressLocality",
                ],
                "pays" => [
                    "valueOf" => "address.addressCountry",
                ],
                "latitude" => [
                    "valueOf" => "geoPosition.coordinates.1",
                ],
                "longitude" => [
                    "valueOf" => "geoPosition.coordinates.0",
                ],
            ],
        ],
        "horaires_ouverture" => [
            "valueOf" => 'openingHours',
            "type" => "openingHours",
        ],
        "nature_juridique" => [
            "valueOf" => "manageModel",
        ],
        "contact_email" => [
            "valueOf" => 'email',
        ],
        "contact_telephone" => [
            "valueOf" => 'telephone.fixe.0',
        ],
        "reseaux" => [
            "valueOf" => 'network',
        ],
        "taille" => [
            "valueOf" => 'spaceSize',
        ],
    ];

    public static array $TypePlaceList = [
        "Coworking",
        "Ateliers artisanaux partagés",
        "Fablab / Atelier de Fabrication Numérique",
        "Tiers-lieu culturel",
        "Tiers-lieu agricole",
        "Cuisine partagée / Foodlab",
        "LivingLab / Laboratoire d'innovation sociale",
    ];

    public static array $dataBinding_organization_symply = [[
        "valueOf" => '_id.$id',
    ], [
        "valueOf" => "name",
    ], [
        "valueOf" => "geoPosition.coordinates.1",
    ], [
        "valueOf" => "geoPosition.coordinates.0",
    ], [10507, 10513, 10512]];

    public static array $dataBinding_network = [
        "@type" => "Organization",
        "typeSig" => "organizations",
        "typeElement" => "organization",
        "typeOrganization" => "NGO",
        "type" => "NGO",
        "name" => [
            "valueOf" => "name",
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geo",
            "valueOf" => [
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "description",
        ],
        "description" => [
            "valueOf" => "descriptionMore",
        ],
        "url" => [
            "valueOf" => 'website',
        ],
        "source" => [
            "valueOf" => 'id',
            "type" => "url",
            "prefix" => "http://presdecheznous.fr/annuaire#/fiche/pdcn/",
            "suffix" => "",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateI18nProperty();
    }

    /**
     * Returns the opening hours as a string.
     *
     * @param array|null $hours The opening hours.
     * @return string The opening hours as a string.
     */
    public function openingHours(?array $hours = null): string
    {
        $res = "";
        if (! empty($hours)) {
            $hours = array_values(array_filter($hours));
            $dayArr = [];
            $rangeDay = [];
            $dayAndRange = [];
            foreach ($hours as $ind => $dayHours) {
                if (is_array($dayHours) && ! empty($dayHours["dayOfWeek"]) && ! empty($dayHours["hours"]) && is_array($dayHours["hours"])) {
                    $day = $this->language->t("translate", (string) $dayHours["dayOfWeek"]);

                    $dayArr[$ind] = $day;

                    $rangeDay[$ind] = "";

                    foreach ($dayHours["hours"] as $i => $time) {
                        if (! empty($time["opens"]) && ! empty($time["closes"])) {
                            $rangeDay[$ind] = (! empty($rangeDay[$ind])) ? $rangeDay[$ind] . "|" . (string) $time["opens"] . "-" . (string) $time["closes"] : $rangeDay[$ind] . (string) $time["opens"] . "-" . (string) $time["closes"];
                        }
                    }

                    if ($ind > 0 && $rangeDay[$ind] == $rangeDay[$ind - 1]) {
                        $sameDay = "";
                        $i = $ind;
                        while ($i > 0 && $rangeDay[$i] == $rangeDay[$i - 1]) {
                            $sameDay = $dayArr[$i - 1] . "-" . $dayArr[$ind];
                            $i--;
                        }
                        $dayAndRange[$ind] = $sameDay . " " . $rangeDay[$ind];
                        $res = str_replace($dayAndRange[$ind - 1], $dayAndRange[$ind], $res);
                    } else {
                        $dayAndRange[$ind] = $dayArr[$ind] . " " . $rangeDay[$ind];
                        $res = $res . " " . $dayAndRange[$ind] . " ;";
                    }
                }
            }
        }
        return $res;
    }

    /**
     * This method is used to determine the type of place.
     *
     * @param array $val The input array containing the values.
     * @return array The array containing the type of place.
     */
    public function typePlace(array $val): array
    {
        $res = [];
        foreach ($val as $ind => $value) {
            if (in_array($value, self::$TypePlaceList)) {
                array_push($res, $value);
            }
        }

        return $res;
    }
}
