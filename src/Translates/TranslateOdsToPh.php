<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateOdsToPhInterface;

class TranslateOdsToPh implements TranslateOdsToPhInterface
{
    public static array $mapping_activity = [
        "fields.l1_declaree" => "name",
        "fields.categorie" => "type",
        "fields.siret" => "shortDescription",
        "fields.coordonnees.0" => "geo.latitude",
        "fields.coordonnees.1" => "geo.longitude",
        "fields.l4_declaree" => "address.streetAddress",
        "fields.libapen" => "tags.0",
    ];
}
