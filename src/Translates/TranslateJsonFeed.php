<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateJsonFeedInterface;

class TranslateJsonFeed implements TranslateJsonFeedInterface
{
    public static array $dataBinding_news = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#news.detail.id.",
            "suffix" => "",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "title" => [
            "valueOf" => "text",
        ],
        "content_html" => [
            "valueOf" => "text",
        ],
        "date_published" => [
            "valueOf" => "date",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
    ];

    public static array $dataBinding_allOrganization = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#page.type.organizations.id.",
            "suffix" => "",
        ],
        "title" => [
            "valueOf" => "name",
        ],
        "date_last_modified" => [
            "valueOf" => "modified",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
        "author" => [
            "valueOf" => "creator",
            "type" => "url",
            "prefix" => "/#page.type.citoyens.id.",
            "suffix" => ".viewer.",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "content" => [
            "shortDescription" => [
                "valueOf" => "shortDescription",
            ],
            "Description" => [
                "valueOf" => "Description",
            ],
        ],
    ];

    public static array $dataBinding_allEvent = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#page.type.organizations.id.",
            "suffix" => "",
        ],
        "title" => [
            "valueOf" => "name",
        ],
        "dates" => [
            "startDate" => [
                "valueOf" => "startDate",
                "type" => "date",
                "prefix" => "",
                "suffix" => "",
            ],
            "endDate" => [
                "valueOf" => "endDate",
                "type" => "date",
                "prefix" => "",
                "suffix" => "",
            ],
            "lastModifiedDate" => [
                "valueOf" => "modified",
                "type" => "date",
                "prefix" => "",
                "suffix" => "",
            ],
        ],
        "author" => [
            "valueOf" => "creator",
            "type" => "url",
            "prefix" => "/#page.type.citoyens.id.",
            "suffix" => ".viewer.",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "content" => [
            "shortDescription" => [
                "valueOf" => "shortDescription",
            ],
            "Description" => [
                "valueOf" => "Description",
            ],
        ],
    ];

    public static array $dataBinding_allProject = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#page.type.organizations.id.",
            "suffix" => "",
        ],
        "title" => [
            "valueOf" => "name",
        ],
        "lastModifiedDate" => [
            "valueOf" => "modified",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
        "author" => [
            "valueOf" => "creator",
            "type" => "url",
            "prefix" => "/#page.type.citoyens.id.",
            "suffix" => ".viewer.",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "content" => [
            "shortDescription" => [
                "valueOf" => "shortDescription",
            ],
            "Description" => [
                "valueOf" => "Description",
            ],
        ],
    ];

    public static array $dataBinding_allPerson = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#page.type.organizations.id.",
            "suffix" => "",
        ],
        "title" => [
            "valueOf" => "name",
        ],
        "lastModifiedDate" => [
            "valueOf" => "modified",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "content" => [
            "shortDescription" => [
                "valueOf" => "shortDescription",
            ],
            "Description" => [
                "valueOf" => "Description",
            ],
        ],
    ];

    public static array $dataBinding_city = [
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#page.type.organizations.id.",
            "suffix" => "",
        ],
        "title" => [
            "valueOf" => "alternateName",
        ],
        "date_last_modified" => [
            "valueOf" => "modified",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
        "author" => [
            "valueOf" => "creator",
            "type" => "url",
            "prefix" => "/#page.type.citoyens.id.",
            "suffix" => ".viewer.",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "content" => [
            "shortDescription" => [
                "valueOf" => "shortDescription",
            ],
            "Description" => [
                "valueOf" => "Description",
            ],
        ],
    ];
}
