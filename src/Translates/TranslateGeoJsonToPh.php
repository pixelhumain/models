<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonToPhInterface;

class TranslateGeoJsonToPh implements TranslateGeoJsonToPhInterface
{
    public static array $mapping_organisation = [
        "type_elt" => OrganizationInterface::COLLECTION,
        "properties.name" => "name",
        "geometry.coordinates.0" => "geo.longitude",
        "geometry.coordinates.1" => "geo.latitude",
    ];

    public static array $mapping_person = [
        "type_elt" => PersonInterface::COLLECTION,
        "properties.name" => "name",
        "geometry.coordinates.0" => "geo.longitude",
        "geometry.coordinates.1" => "geo.latitude",
    ];

    public static array $mapping_event = [
        "type_elt" => EventInterface::COLLECTION,
        "properties.name" => "name",
        "geometry.coordinates.0" => "geo.longitude",
        "geometry.coordinates.1" => "geo.latitude",
    ];

    public static array $mapping_project = [
        "type_elt" => ProjectInterface::COLLECTION,
        "properties.name" => "name",
        "geometry.coordinates.0" => "geo.longitude",
        "geometry.coordinates.1" => "geo.latitude",
    ];
}
