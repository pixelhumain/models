<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateEducEcoleToPhInterface;

class TranslateEducEcoleToPh implements TranslateEducEcoleToPhInterface
{
    public static array $mapping = [
        "fields.libelle" => "name",
        "fields.geolocalisation.0" => "geo.latitude",
        "fields.geolocalisation.1" => "geo.longitude",
        "fields.site_web" => "url",
        "fields.mail" => "email",
        "fields.groupe_disciplinaire" => "tags.0",
    ];
}
