<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateEducStructToPhInterface;

class TranslateEducStructToPh implements TranslateEducStructToPhInterface
{
    public static array $mapping = [
        "fields.libelle" => "name",
        "fields.geolocalisation.0" => "geo.latitude",
        "fields.geolocalisation.1" => "geo.longitude",
        "fields.etat" => "tags.0",
    ];
}
