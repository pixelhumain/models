<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Translates\Interfaces\TranslateGogoCartoInterface;

class TranslateGogoCarto implements TranslateGogoCartoInterface
{
    /*

        ----------------- COMMUNECTER -----------------
    */

    public static array $dataBinding_allOrganization = [
        "@type" => "Organization",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "typeSig" => OrganizationInterface::COLLECTION,
        "typeElement" => OrganizationInterface::CONTROLLER,
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/#organization.detail.id.",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/organization/get/id/",
                    "suffix" => "",
                ],
                "website" => [
                    "valueOf" => 'url',
                ],
            ],
        ],
    ];

    public static array $dataBinding_organization = [
        "id" => [
            "valueOf" => '_id.$id',
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "geo" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "latitude" => [
                    "valueOf" => "coordinates.1",
                ],
                "longitude" => [
                    "valueOf" => "coordinates.0",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "description" => [
            "valueOf" => "shortDescription",
        ],
        "descriptionMore" => [
            "valueOf" => "description",
        ],
        "website" => [
            "valueOf" => 'url',
        ],
        "categories" => [
            "valueOf" => 'tags',
        ],
        "openHours" => null,
        "openHoursMoreInfos" => "",
        "sourceKey" => "Communecter",
        "images" => [
            "valueOf" => "image",
            "type" => "url",
            "toArray" => true,
        ],
    ];

    public static array $dataBinding_organization_symply = [[
        "valueOf" => '_id.$id',
    ], [
        "valueOf" => "name",
    ], [
        "valueOf" => "geoPosition.coordinates.1",
    ], [
        "valueOf" => "geoPosition.coordinates.0",
    ], [10507, 10513, 10512]];

    public static array $dataBinding_network = [
        "@type" => "Organization",
        "typeSig" => "organizations",
        "typeElement" => "organization",
        "typeOrganization" => "NGO",
        "type" => "NGO",
        "name" => [
            "valueOf" => "name",
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geo",
            "valueOf" => [
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "description",
        ],
        "description" => [
            "valueOf" => "descriptionMore",
        ],
        "url" => [
            "valueOf" => 'website',
        ],
        "source" => [
            "valueOf" => 'id',
            "type" => "url",
            "prefix" => "http://presdecheznous.fr/annuaire#/fiche/pdcn/",
            "suffix" => "",
        ],
    ];
}
