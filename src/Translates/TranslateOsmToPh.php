<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateOsmToPhInterface;

class TranslateOsmToPh implements TranslateOsmToPhInterface
{
    public static array $mapping_element = [
        "tags.name" => "name",
        "type" => "type",
        "lat" => "geo.latitude",
        "lon" => "geo.longitude",
        "tags.amenity" => "tags.0",
        "id" => "shortDescription",
    ];
}
