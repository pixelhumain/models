<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Translates\Interfaces\TranslateCommunecterInterface;

class TranslateCommunecter implements TranslateCommunecterInterface
{
    /*
        ----------------- COMMUNECTER -----------------
    */

    public static array $dataBinding_allAnswer = [
        "value" => [
            "valueOf" => "value",
        ],
        "fieldLabel" => [
            "valueOf" => "label",
        ],
        "fieldType" => [
            "valueOf" => "type",
        ],
        "fieldId" => [
            "valueOf" => "fieldId",
        ],
        "fullCommunecterValue" => [
            "valueOf" => "fullValue",
        ],
    ];

    public static array $dataBinding_allPerson = [
        "@type" => "Person",
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/person/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
    ];

    public static array $dataBinding_time = [
        "@type" => "Date",
        "date" => [
            "valueOf" => [
                "year" => [
                    "valueOf" => 'year',
                ],
                "mon" => [
                    "valueOf" => 'mon',
                ],
                "mday" => [
                    "valueOf" => 'mday',
                ],
            ],
        ],
        "time" => [
            "valueOf" => [
                "hours" => [
                    "valueOf" => 'hours',
                ],
                "minutes" => [
                    "valueOf" => 'minutes',
                ],
                "secondes" => [
                    "valueOf" => 'secondes',
                ],
            ],
        ],
    ];

    public static array $dataBinding_person = [
        "@type" => "Person",
        "name" => [
            "valueOf" => "name",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/person/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "@type" => "Point",
                "coordinates" => [
                    "valueOf" => "coordinates",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "phone" => [
            "parentKey" => "telephone",
            "valueOf" => [
                "fixe" => [
                    "parentKey" => "fixe",
                    "valueOf" => "fixe",
                ],
                "mobile" => [
                    "parentKey" => "mobile",
                    "valueOf" => "mobile",
                ],
                "fax" => [
                    "parentKey" => "fax",
                    "valueOf" => "fax",
                ],
            ],
        ],
        "socialNetwork" => [
            "parentKey" => "socialNetwork",
            "valueOf" => [
                "github" => [
                    "valueOf" => "github",
                ],
                "twitter" => [
                    "valueOf" => "twitter",
                ],
                "facebook" => [
                    "valueOf" => "facebook",
                ],
                "googleplus" => [
                    "valueOf" => "googleplus",
                ],
                "linkedin" => [
                    "valueOf" => "linkedin",
                ],
                "skype" => [
                    "valueOf" => "skype",
                ],
            ],
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "links" => [
            "parentKey" => "links",
            "valueOf" => [
                "memberOf" => [
                    "object" => "memberOf",
                    "collection" => OrganizationInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Organization",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#organization.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/organization/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "projects" => [
                    "object" => "projects",
                    "collection" => ProjectInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Project",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#project.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/project/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "events" => [
                    "object" => "events",
                    "collection" => EventInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Event",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => 'slug',
                                    "type" => "url",
                                    "prefix" => "/#@",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/event/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    public static array $dataBinding_allOrganization = [
        "@type" => "Organization",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/organization/get/id/",
                    "suffix" => "",
                ],
                "website" => [
                    "valueOf" => 'url',
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "slug" => [
            "valueOf" => "slug",
        ],
    ];

    public static array $dataBinding_organization = [
        "@type" => "Organization",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/organization/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "@type" => "Point",
                "coordinates" => [
                    "valueOf" => "coordinates",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "phone" => [
            "parentKey" => "telephone",
            "valueOf" => [
                "fixe" => [
                    "parentKey" => "fixe",
                    "valueOf" => "fixe",
                ],
                "mobile" => [
                    "parentKey" => "mobile",
                    "valueOf" => "mobile",
                ],
                "fax" => [
                    "parentKey" => "fax",
                    "valueOf" => "fax",
                ],
            ],
        ],
        "socialNetwork" => [
            "parentKey" => "socialNetwork",
            "valueOf" => [
                "github" => [
                    "valueOf" => "github",
                ],
                "twitter" => [
                    "valueOf" => "twitter",
                ],
                "facebook" => [
                    "valueOf" => "facebook",
                ],
                "googleplus" => [
                    "valueOf" => "googleplus",
                ],
                "linkedin" => [
                    "valueOf" => "linkedin",
                ],
                "skype" => [
                    "valueOf" => "skype",
                ],
            ],
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "links" => [
            "parentKey" => "links",
            "valueOf" => [
                "followers" => [
                    "object" => "followers",
                    "collection" => PersonInterface::COLLECTION,
                    "valueOf" => [
                        "@type" => "Person",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#person.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/person/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "members" => [
                    "object" => "members",
                    "collection" => PersonInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Person",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#person.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/person/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "projects" => [
                    "object" => "projects",
                    "collection" => ProjectInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Project",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#project.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/project/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "events" => [
                    "object" => "events",
                    "collection" => EventInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Event",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#event.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/event/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    public static array $dataBinding_allEvent = [
        "@type" => "Event",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/event/get/id/",
                    "suffix" => "",
                ],
                "website" => [
                    "valueOf" => 'url',
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
    ];

    public static array $dataBinding_event = [
        "@type" => "Event",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/#event.detail.id.",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/event/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "@type" => "Point",
                "coordinates" => [
                    "valueOf" => "coordinates",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "allDay" => [
            "valueOf" => "allDay",
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "phone" => [
            "parentKey" => "telephone",
            "valueOf" => [
                "fixe" => [
                    "parentKey" => "fixe",
                    "valueOf" => "fixe",
                ],
                "mobile" => [
                    "parentKey" => "mobile",
                    "valueOf" => "mobile",
                ],
                "fax" => [
                    "parentKey" => "fax",
                    "valueOf" => "fax",
                ],
            ],
        ],
        "socialNetwork" => [
            "parentKey" => "socialNetwork",
            "valueOf" => [
                "github" => [
                    "valueOf" => "github",
                ],
                "twitter" => [
                    "valueOf" => "twitter",
                ],
                "facebook" => [
                    "valueOf" => "facebook",
                ],
                "googleplus" => [
                    "valueOf" => "googleplus",
                ],
                "linkedin" => [
                    "valueOf" => "linkedin",
                ],
                "skype" => [
                    "valueOf" => "skype",
                ],
            ],
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "links" => [
            "parentKey" => "links",
            "valueOf" => [
                "attendees" => [
                    "object" => "attendees",
                    "collection" => PersonInterface::COLLECTION,
                    "valueOf" => [
                        "@type" => "Person",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#person.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/person/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
                "needs" => [
                    "object" => "needs",
                    "collection" => EventInterface::COLLECTION,
                    "valueOf" => [
                        "type" => "Need",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#need.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/need/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    public static array $dataBinding_allProject = [
        "@type" => "Project",
        "name" => [
            "valueOf" => "name",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "maturity" => [
            "valueOf" => "maturity",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/#project.detail.id.",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/project/get/id/",
                    "suffix" => "",
                ],
                "pdf" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/co2/export/pdfelement/type/projects/id/",
                    "suffix" => "",
                ],
                "website" => [
                    "valueOf" => 'url',
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
    ];

    public static array $dataBinding_project = [
        "@type" => "Project",
        "name" => [
            "valueOf" => "name",
        ],
        "maturity" => [
            "valueOf" => "maturity",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/#project.detail.id.",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/project/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "@type" => "Point",
                "coordinates" => [
                    "valueOf" => "coordinates",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "allDay" => [
            "valueOf" => "allDay",
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "phone" => [
            "parentKey" => "telephone",
            "valueOf" => [
                "fixe" => [
                    "parentKey" => "fixe",
                    "valueOf" => "fixe",
                ],
                "mobile" => [
                    "parentKey" => "mobile",
                    "valueOf" => "mobile",
                ],
                "fax" => [
                    "parentKey" => "fax",
                    "valueOf" => "fax",
                ],
            ],
        ],
        "socialNetwork" => [
            "parentKey" => "socialNetwork",
            "valueOf" => [
                "github" => [
                    "valueOf" => "github",
                ],
                "twitter" => [
                    "valueOf" => "twitter",
                ],
                "facebook" => [
                    "valueOf" => "facebook",
                ],
                "googleplus" => [
                    "valueOf" => "googleplus",
                ],
                "linkedin" => [
                    "valueOf" => "linkedin",
                ],
                "skype" => [
                    "valueOf" => "skype",
                ],
            ],
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "links" => [
            "parentKey" => "links",
            "valueOf" => [
                "contributors" => [
                    "object" => "contributors",
                    "collection" => PersonInterface::COLLECTION,
                    "valueOf" => [
                        "@type" => "Person",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#person.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/person/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    public static array $dataBinding_need = [
        "@type" => "Project",
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "duration" => [
            "valueOf" => "duration",
        ],
        "quantity" => [
            "valueOf" => "quantity",
        ],
        "benefits" => [
            "valueOf" => "benefits",
        ],
        "url" => [
            "valueOf" => [
                "website" => [
                    "valueOf" => 'url',
                ],
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/#need.detail.id.",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/need/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "allDay" => [
            "valueOf" => "allDay",
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "phone" => [
            "parentKey" => "telephone",
            "valueOf" => [
                "fixe" => [
                    "parentKey" => "fixe",
                    "valueOf" => "fixe",
                ],
                "mobile" => [
                    "parentKey" => "mobile",
                    "valueOf" => "mobile",
                ],
                "fax" => [
                    "parentKey" => "fax",
                    "valueOf" => "fax",
                ],
            ],
        ],
        "socialNetwork" => [
            "parentKey" => "socialNetwork",
            "valueOf" => [
                "github" => [
                    "valueOf" => "github",
                ],
                "twitter" => [
                    "valueOf" => "twitter",
                ],
                "facebook" => [
                    "valueOf" => "facebook",
                ],
                "googleplus" => [
                    "valueOf" => "googleplus",
                ],
                "linkedin" => [
                    "valueOf" => "linkedin",
                ],
                "skype" => [
                    "valueOf" => "skype",
                ],
            ],
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "links" => [
            "parentKey" => "links",
            "valueOf" => [
                "contributors" => [
                    "object" => "contributors",
                    "collection" => PersonInterface::COLLECTION,
                    "valueOf" => [
                        "@type" => "Person",
                        "name" => [
                            "valueOf" => "name",
                        ],
                        "url" => [
                            "valueOf" => [
                                "communecter" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/#person.detail.id.",
                                    "suffix" => "",
                                ],
                                "api" => [
                                    "valueOf" => '_id.$id',
                                    "type" => "url",
                                    "prefix" => "/api/person/get/id/",
                                    "suffix" => "",
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    public static array $dataBinding_city = [
        "@type" => "City",
        "@id" => [
            "valueOf" => 'insee',
            "type" => "url",
            "prefix" => "/api/data/get/type/cities/insee/",
            "suffix" => "",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "alternateName" => [
            "valueOf" => "alternateName",
        ],
        "insee" => [
            "valueOf" => "insee",
        ],
        "level1" => [
            "valueOf" => "level1",
        ],
        "level1Name" => [
            "valueOf" => "level1Name",
        ],
        "level2" => [
            "valueOf" => "level2",
        ],
        "level2Name" => [
            "valueOf" => "level2Name",
        ],
        "level3" => [
            "valueOf" => "level3",
        ],
        "level3Name" => [
            "valueOf" => "level3Name",
        ],
        "level4" => [
            "valueOf" => "level4",
        ],
        "level4Name" => [
            "valueOf" => "level4Name",
        ],
        "country" => [
            "valueOf" => "country",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/#city.detail.insee.",
                    "suffix" => "",
                ],
                "wikidata" => [
                    "valueOf" => 'wikidataID',
                    "type" => "url",
                    "prefix" => "http://www.wikidata.org/entity/",
                    "suffix" => "",
                    "outsite" => true,
                ],
                "citoyens" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/api/person/get/insee/",
                    "suffix" => "",
                ],
                "organizations" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/api/organization/get/insee/",
                    "suffix" => "",
                ],
                "projects" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/api/project/get/insee/",
                    "suffix" => "",
                ],
                "events" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/api/event/get/insee/",
                    "suffix" => "",
                ],
            ],
        ],
        "postalCodes" => [
            "valueOf" => "postalCodes",
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "geoPosition" => [
            "parentKey" => "geoPosition",
            "valueOf" => [
                "@type" => "Point",
                "coordinates" => [
                    "valueOf" => "coordinates",
                ],
            ],
        ],
        "geoShape" => [
            "valueOf" => "geoShape",
        ],
    ];

    public static array $dataBinding_news = [
        "@type" => "News",
        "text" => [
            "valueOf" => "text",
        ],
        "date" => [
            "valueOf" => "startDate",
        ],
        "created" => [
            "valueOf" => "endDate",
        ],
        "scope" => [
            "parentKey" => "scope",
            "valueOf" => [
                "type" => [
                    "valueOf" => "type",
                ],
            ],
        ],
        "target" => [
            "communecter" => [
                "valueOf" => 'id',
                "type" => "url",
                "prefix" => "/#person.detail.id.",
                "suffix" => "",
            ],
            "api" => [
                "valueOf" => 'id',
                "type" => "url",
                "prefix" => "/api/person/get/id/",
                "suffix" => "",
            ],
        ],
        "author" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/#person.detail.id.",
            "suffix" => "",
        ],
    ];

    public static array $dataBinding_allActivityPub = [
        "@context" => "http://www.w3.org/ns/activitystreams",
        "type" => [
            "valueOf" => "type",
        ],
        "verb" => [
            "valueOf" => "verb",
        ],
        "autor" => [
            "parentKey" => 'author',
            "valueOf" => [
                "id" => [
                    "valueOf" => "id",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/activitypub/get/id/",
                    "suffix" => "",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allActivity = [
        "@context" => "http://www.w3.org/ns/activitystreams",
        "type" => [
            "valueOf" => "type",
        ],
        "verb" => [
            "valueOf" => "verb",
        ],
        "autor" => [
            "parentKey" => 'author',
            "valueOf" => [
                "id" => [
                    "valueOf" => "id",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
                "type" => [
                    "valueOf" => "type",
                ],
            ],
        ],
        "target" => [
            "valueOf" => "target",
        ],
        "scope" => [
            "parentKey" => "scope",
            "valueOf" => [
                "type" => [
                    "valueOf" => "type",
                ],
            ],
        ],
        "object" => [
            "parentKey" => 'object',
            "valueOf" => [
                "displayName" => [
                    "valueOf" => "displayName",
                ],
                "displayValue" => [
                    "valueOf" => "displayValue",
                ],
            ],
        ],
        "notify" => [
            "valueOf" => "notify",
        ],
        "sharedBy" => [
            "valueOf" => "sharedBy",
        ],
    ];

    public static array $dataBinding_proposal = [
        "@type" => "Proposal",
        "name" => [
            "valueOf" => "title",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "voteUp" => [
            "parentKey" => "votes",
            "valueOf" => "up",
        ],
        "votes" => [
            "valueOf" => "votes",
        ],
        "amendements" => [
            "valueOf" => "amendements",
        ],
        "comments" => [
            "valueOf" => "comments",
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "#dda?proposals.",
                    "suffix" => "",
                ],
            ],
        ],
    ];

    public static array $dataBinding_allPoi = [
        "@type" => "Poi",
        "id" => [
            "valueOf" => "id",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "typeCommunecter" => [
            "valueOf" => "type",
        ],
        "image" => [
            "valueOf" => "image",
            "type" => "url",
        ],
        "url" => [
            "valueOf" => [
                "communecter" => [
                    "valueOf" => 'slug',
                    "type" => "url",
                    "prefix" => "/#@",
                    "suffix" => "",
                ],
                "api" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/api/organization/get/id/",
                    "suffix" => "",
                ],
                "website" => [
                    "valueOf" => 'url',
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "codeInsee" => [
                    "valueOf" => "codeInsee",
                ],
                "addressRegion" => [
                    "valueOf" => "addressRegion",
                ],
                "addressCountry" => [
                    "valueOf" => "addressCountry",
                ],
            ],
        ],
        "geo" => [
            "parentKey" => "geo",
            "valueOf" => [
                "@type" => "GeoCoordinates",
                "latitude" => [
                    "valueOf" => "latitude",
                ],
                "longitude" => [
                    "valueOf" => "longitude",
                ],
            ],
        ],
        "shortDescription" => [
            "valueOf" => "shortDescription",
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "tags" => [
            "valueOf" => "tags",
        ],
        "slug" => [
            "valueOf" => "slug",
        ],
    ];
}
