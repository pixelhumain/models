<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateOrgancityToPhInterface;

class TranslateOrgancityToPh implements TranslateOrgancityToPhInterface
{
    public static array $mapping_organcity = [
        "related.site.name" => "name",
        "id" => "description",
        "related.site.description" => "shortDescription",
        "related.site.position.longitude" => "geo.longitude",
        "related.site.position.latitude" => "geo.latitude",
    ];
}
