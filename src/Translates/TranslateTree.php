<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateTreeInterface;

class TranslateTree implements TranslateTreeInterface
{
    /**
     * Build a translated tree from the given data.
     *
     * @param array $data The data to build the tree from.
     * @param string $type The type of the tree.
     * @return array The translated tree.
     */
    public function build(array $data, string $type): array
    {
        $res = [
            "name" => $type,
            "children" => [],
        ];
        $ct = 0;
        foreach ($data as $keyID => $valueData) {
            if (! empty($valueData) && is_array($valueData)) {
                $res["children"][$ct] = [
                    "name" => "I am a " . (isset($valueData["name"]) ? (string) $valueData["name"] : ""),
                    "children" => [],
                ];

                $res["children"][$ct]["children"][] = [
                    "name" => "exports",
                    "children" => [[
                        "name" => "markdown :: http://communecter.org/api/" . $type . "/get/id/" . $keyID . "/format/md",
                    ]],
                ];

                // TODO : $information n'est pas utilisé
                $information = [];
                $fields = ["email", "username", "address", "geo", "shortDescription", "description"];
                foreach ($fields as $field) {
                    if (isset($valueData[$field])) {
                        // Special handling for nested fields like 'address' and 'geo'
                        if ($field === "address" || $field === "geo") {
                            foreach ($valueData[$field] as $subfield => $subvalue) {
                                $information[] = [
                                    "name" => (string) $subfield . " : " . (string) $subvalue,
                                ];
                            }
                        } else {
                            $information[] = [
                                "name" => $field . " : " . (string) $valueData[$field],
                            ];
                        }
                    }
                }

                if (! empty($valueData["links"])) {
                    foreach (["memberOf", "projects", "events"] as $linkType) {
                        if (isset($valueData["links"][$linkType]) && is_array($valueData["links"][$linkType])) {
                            $elements = [];
                            foreach ($valueData["links"][$linkType] as $ix => $o) {
                                if (is_array($o)) {
                                    $el = [
                                        "name" => ! empty($o["name"]) ? (string) $o["name"] : "",
                                        "url" => ! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : "",
                                    ];
                                    $elements[] = $el;
                                }
                            }
                            $res["children"][$ct]["children"][] = [
                                "name" => ucfirst($linkType),
                                "children" => $elements,
                            ];
                        }
                    }
                }
                $ct++;
            }
        }
        return $res;
    }
}
