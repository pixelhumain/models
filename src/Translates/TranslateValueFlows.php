<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsInterface;

class TranslateValueFlows implements TranslateValueFlowsInterface
{
    public static array $dataBinding_agent = [
        "fields" => [
            "name" => [
                "valueOf" => "name",
            ],
            "url" => [
                "valueOf" => "url",
            ],
            "note" => [
                "valueOf" => "description",
            ],
        ],
    ];
}
