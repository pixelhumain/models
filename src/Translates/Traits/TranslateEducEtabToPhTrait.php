<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateEducEtabToPhInterface;

trait TranslateEducEtabToPhTrait
{
    protected ?TranslateEducEtabToPhInterface $translateEducEtabToPh = null;

    /**
     * Set the translateEducEtabToPh.
     *
     * @param TranslateEducEtabToPhInterface $translateEducEtabToPh The translateEducEtabToPh object to set.
     * @return void
     */
    public function setTranslateEducEtabToPh(TranslateEducEtabToPhInterface $translateEducEtabToPh): void
    {
        $this->translateEducEtabToPh = $translateEducEtabToPh;
    }

    /**
     * Gets the translateEducEtabToPh.
     *
     * @return TranslateEducEtabToPhInterface The translateEducEtabToPh.
     */
    public function getTranslateEducEtabToPh(): TranslateEducEtabToPhInterface
    {
        if ($this->translateEducEtabToPh === null) {
            throw new Exception(TranslateEducEtabToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateEducEtabToPh;
    }
}
