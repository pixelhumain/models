<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateGogoCartoInterface;

trait TranslateGogoCartoTrait
{
    protected ?TranslateGogoCartoInterface $translateGogoCarto = null;

    /**
     * Set the translateGogoCarto.
     *
     * @param TranslateGogoCartoInterface $translateGogoCarto The translateGogoCarto object to set.
     * @return void
     */
    public function setTranslateGogoCarto(TranslateGogoCartoInterface $translateGogoCarto): void
    {
        $this->translateGogoCarto = $translateGogoCarto;
    }

    /**
     * Gets the translateGogoCarto.
     *
     * @return TranslateGogoCartoInterface The translateGogoCarto.
     */
    public function getTranslateGogoCarto(): TranslateGogoCartoInterface
    {
        if ($this->translateGogoCarto === null) {
            throw new Exception(TranslateGogoCartoInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateGogoCarto;
    }
}
