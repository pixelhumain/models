<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateWikiToPhInterface;

trait TranslateWikiToPhTrait
{
    protected ?TranslateWikiToPhInterface $translateWikiToPh = null;

    /**
     * Set the translateWikiToPh.
     *
     * @param TranslateWikiToPhInterface $translateWikiToPh The translateWikiToPh object to set.
     * @return void
     */
    public function setTranslateWikiToPh(TranslateWikiToPhInterface $translateWikiToPh): void
    {
        $this->translateWikiToPh = $translateWikiToPh;
    }

    /**
     * Gets the translateWikiToPh.
     *
     * @return TranslateWikiToPhInterface The translateWikiToPh.
     */
    public function getTranslateWikiToPh(): TranslateWikiToPhInterface
    {
        if ($this->translateWikiToPh === null) {
            throw new Exception(TranslateWikiToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateWikiToPh;
    }
}
