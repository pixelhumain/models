<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateDatanovaToPhInterface;

trait TranslateDatanovaToPhTrait
{
    protected ?TranslateDatanovaToPhInterface $translateDatanovaToPh = null;

    /**
     * Set the translateDatanovaToPh.
     *
     * @param TranslateDatanovaToPhInterface $translateDatanovaToPh The translateDatanovaToPh object to set.
     * @return void
     */
    public function setTranslateDatanovaToPh(TranslateDatanovaToPhInterface $translateDatanovaToPh): void
    {
        $this->translateDatanovaToPh = $translateDatanovaToPh;
    }

    /**
     * Gets the translateDatanovaToPh.
     *
     * @return TranslateDatanovaToPhInterface The translateDatanovaToPh.
     */
    public function getTranslateDatanovaToPh(): TranslateDatanovaToPhInterface
    {
        if ($this->translateDatanovaToPh === null) {
            throw new Exception(TranslateDatanovaToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateDatanovaToPh;
    }
}
