<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateSchemaInterface;

trait TranslateSchemaTrait
{
    protected ?TranslateSchemaInterface $translateSchema = null;

    /**
     * Set the translateSchema.
     *
     * @param TranslateSchemaInterface $translateSchema The translateSchema object to set.
     * @return void
     */
    public function setTranslateSchema(TranslateSchemaInterface $translateSchema): void
    {
        $this->translateSchema = $translateSchema;
    }

    /**
     * Gets the translateSchema.
     *
     * @return TranslateSchemaInterface The translateSchema.
     */
    public function getTranslateSchema(): TranslateSchemaInterface
    {
        if ($this->translateSchema === null) {
            throw new Exception(TranslateSchemaInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateSchema;
    }
}
