<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateMDInterface;

interface TranslateMDTraitInterface
{
    /**
     * Set the translateMD.
     *
     * @param TranslateMDInterface $translateMD The translateMD object to set.
     * @return void
     */
    public function setTranslateMD(TranslateMDInterface $translateMD): void;

    /**
     * Gets the translateMD.
     *
     * @return TranslateMDInterface The translateMD.
     */
    public function getTranslateMD(): TranslateMDInterface;
}
