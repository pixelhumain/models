<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateTreeInterface;

interface TranslateTreeTraitInterface
{
    /**
     * Set the translateTree.
     *
     * @param TranslateTreeInterface $translateTree The translateTree object to set.
     * @return void
     */
    public function setTranslateTree(TranslateTreeInterface $translateTree): void;

    /**
     * Gets the translateTree.
     *
     * @return TranslateTreeInterface The translateTree.
     */
    public function getTranslateTree(): TranslateTreeInterface;
}
