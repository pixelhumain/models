<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonInterface;

interface TranslateGeoJsonTraitInterface
{
    /**
     * Set the translateGeoJson.
     *
     * @param TranslateGeoJsonInterface $translateGeoJson The translateGeoJson object to set.
     * @return void
     */
    public function setTranslateGeoJson(TranslateGeoJsonInterface $translateGeoJson): void;

    /**
     * Gets the translateGeoJson.
     *
     * @return TranslateGeoJsonInterface The translateGeoJson.
     */
    public function getTranslateGeoJson(): TranslateGeoJsonInterface;
}
