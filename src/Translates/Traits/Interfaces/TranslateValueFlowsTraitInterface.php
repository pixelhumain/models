<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsInterface;

interface TranslateValueFlowsTraitInterface
{
    /**
     * Set the translateValueFlows.
     *
     * @param TranslateValueFlowsInterface $translateValueFlows The translateValueFlows object to set.
     * @return void
     */
    public function setTranslateValueFlows(TranslateValueFlowsInterface $translateValueFlows): void;

    /**
     * Gets the translateValueFlows.
     *
     * @return TranslateValueFlowsInterface The translateValueFlows.
     */
    public function getTranslateValueFlows(): TranslateValueFlowsInterface;
}
