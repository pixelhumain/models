<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateEducEcoleToPhInterface;

interface TranslateEducEcoleToPhTraitInterface
{
    /**
     * Set the translateEducEcoleToPh.
     *
     * @param TranslateEducEcoleToPhInterface $translateEducEcoleToPh The translateEducEcoleToPh object to set.
     * @return void
     */
    public function setTranslateEducEcoleToPh(TranslateEducEcoleToPhInterface $translateEducEcoleToPh): void;

    /**
     * Gets the translateEducEcoleToPh.
     *
     * @return TranslateEducEcoleToPhInterface The translateEducEcoleToPh.
     */
    public function getTranslateEducEcoleToPh(): TranslateEducEcoleToPhInterface;
}
