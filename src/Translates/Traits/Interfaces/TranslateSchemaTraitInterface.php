<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateSchemaInterface;

interface TranslateSchemaTraitInterface
{
    /**
     * Set the translateSchema.
     *
     * @param TranslateSchemaInterface $translateSchema The translateSchema object to set.
     * @return void
     */
    public function setTranslateSchema(TranslateSchemaInterface $translateSchema): void;

    /**
     * Gets the translateSchema.
     *
     * @return TranslateSchemaInterface The translateSchema.
     */
    public function getTranslateSchema(): TranslateSchemaInterface;
}
