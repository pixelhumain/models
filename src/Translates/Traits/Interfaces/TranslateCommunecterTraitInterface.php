<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateCommunecterInterface;

interface TranslateCommunecterTraitInterface
{
    /**
     * Set the translateCommunecter.
     *
     * @param TranslateCommunecterInterface $translateCommunecter The translateCommunecter object to set.
     * @return void
     */
    public function setTranslateCommunecter(TranslateCommunecterInterface $translateCommunecter): void;

    /**
     * Gets the translateCommunecter.
     *
     * @return TranslateCommunecterInterface The translateCommunecter.
     */
    public function getTranslateCommunecter(): TranslateCommunecterInterface;
}
