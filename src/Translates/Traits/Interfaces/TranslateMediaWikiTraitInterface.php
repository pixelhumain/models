<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateMediaWikiInterface;

interface TranslateMediaWikiTraitInterface
{
    /**
     * Set the translateMediaWiki.
     *
     * @param TranslateMediaWikiInterface $translateMediaWiki The translateMediaWiki object to set.
     * @return void
     */
    public function setTranslateMediaWiki(TranslateMediaWikiInterface $translateMediaWiki): void;

    /**
     * Gets the translateMediaWiki.
     *
     * @return TranslateMediaWikiInterface The translateMediaWiki.
     */
    public function getTranslateMediaWiki(): TranslateMediaWikiInterface;
}
