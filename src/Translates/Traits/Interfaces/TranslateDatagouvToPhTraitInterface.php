<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateDatagouvToPhInterface;

interface TranslateDatagouvToPhTraitInterface
{
    /**
     * Set the translateDatagouvToPh.
     *
     * @param TranslateDatagouvToPhInterface $translateDatagouvToPh The translateDatagouvToPh object to set.
     * @return void
     */
    public function setTranslateDatagouvToPh(TranslateDatagouvToPhInterface $translateDatagouvToPh): void;

    /**
     * Gets the translateDatagouvToPh.
     *
     * @return TranslateDatagouvToPhInterface The translateDatagouvToPh.
     */
    public function getTranslateDatagouvToPh(): TranslateDatagouvToPhInterface;
}
