<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateJsonFeedInterface;

interface TranslateJsonFeedTraitInterface
{
    /**
     * Set the translateJsonFeed.
     *
     * @param TranslateJsonFeedInterface $translateJsonFeed The translateJsonFeed object to set.
     * @return void
     */
    public function setTranslateJsonFeed(TranslateJsonFeedInterface $translateJsonFeed): void;

    /**
     * Gets the translateJsonFeed.
     *
     * @return TranslateJsonFeedInterface The translateJsonFeed.
     */
    public function getTranslateJsonFeed(): TranslateJsonFeedInterface;
}
