<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateAgendaToPhInterface;

interface TranslateAgendaToPhTraitInterface
{
    /**
     * Set the translateAgendaToPh.
     *
     * @param TranslateAgendaToPhInterface $translateAgendaToPh The translateAgendaToPh object to set.
     * @return void
     */
    public function setTranslateAgendaToPh(TranslateAgendaToPhInterface $translateAgendaToPh): void;

    /**
     * Gets the translateAgendaToPh.
     *
     * @return TranslateAgendaToPhInterface The translateAgendaToPh.
     */
    public function getTranslateAgendaToPh(): TranslateAgendaToPhInterface;
}
