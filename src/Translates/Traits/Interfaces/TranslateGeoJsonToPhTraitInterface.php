<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonToPhInterface;

interface TranslateGeoJsonToPhTraitInterface
{
    /**
     * Set the translateGeoJsonToPh.
     *
     * @param TranslateGeoJsonToPhInterface $translateGeoJsonToPh The translateGeoJsonToPh object to set.
     * @return void
     */
    public function setTranslateGeoJsonToPh(TranslateGeoJsonToPhInterface $translateGeoJsonToPh): void;

    /**
     * Gets the translateGeoJsonToPh.
     *
     * @return TranslateGeoJsonToPhInterface The translateGeoJsonToPh.
     */
    public function getTranslateGeoJsonToPh(): TranslateGeoJsonToPhInterface;
}
