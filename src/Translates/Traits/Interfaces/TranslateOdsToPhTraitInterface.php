<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateOdsToPhInterface;

interface TranslateOdsToPhTraitInterface
{
    /**
     * Set the translateOdsToPh.
     *
     * @param TranslateOdsToPhInterface $translateOdsToPh The translateOdsToPh object to set.
     * @return void
     */
    public function setTranslateOdsToPh(TranslateOdsToPhInterface $translateOdsToPh): void;

    /**
     * Gets the translateOdsToPh.
     *
     * @return TranslateOdsToPhInterface The translateOdsToPh.
     */
    public function getTranslateOdsToPh(): TranslateOdsToPhInterface;
}
