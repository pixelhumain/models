<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateEducEtabToPhInterface;

interface TranslateEducEtabToPhTraitInterface
{
    /**
     * Set the translateEducEtabToPh.
     *
     * @param TranslateEducEtabToPhInterface $translateEducEtabToPh The translateEducEtabToPh object to set.
     * @return void
     */
    public function setTranslateEducEtabToPh(TranslateEducEtabToPhInterface $translateEducEtabToPh): void;

    /**
     * Gets the translateEducEtabToPh.
     *
     * @return TranslateEducEtabToPhInterface The translateEducEtabToPh.
     */
    public function getTranslateEducEtabToPh(): TranslateEducEtabToPhInterface;
}
