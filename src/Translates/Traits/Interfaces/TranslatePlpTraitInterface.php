<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslatePlpInterface;

interface TranslatePlpTraitInterface
{
    /**
     * Set the translatePlp.
     *
     * @param TranslatePlpInterface $translatePlp The translatePlp object to set.
     * @return void
     */
    public function setTranslatePlp(TranslatePlpInterface $translatePlp): void;

    /**
     * Gets the translatePlp.
     *
     * @return TranslatePlpInterface The translatePlp.
     */
    public function getTranslatePlp(): TranslatePlpInterface;
}
