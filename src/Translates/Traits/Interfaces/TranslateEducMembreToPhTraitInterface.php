<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateEducMembreToPhInterface;

interface TranslateEducMembreToPhTraitInterface
{
    /**
     * Set the translateEducMembreToPh.
     *
     * @param TranslateEducMembreToPhInterface $translateEducMembreToPh The translateEducMembreToPh object to set.
     * @return void
     */
    public function setTranslateEducMembreToPh(TranslateEducMembreToPhInterface $translateEducMembreToPh): void;

    /**
     * Gets the translateEducMembreToPh.
     *
     * @return TranslateEducMembreToPhInterface The translateEducMembreToPh.
     */
    public function getTranslateEducMembreToPh(): TranslateEducMembreToPhInterface;
}
