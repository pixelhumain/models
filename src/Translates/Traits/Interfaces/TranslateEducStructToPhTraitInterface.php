<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateEducStructToPhInterface;

interface TranslateEducStructToPhTraitInterface
{
    /**
     * Set the translateEducStructToPh.
     *
     * @param TranslateEducStructToPhInterface $translateEducStructToPh The translateEducStructToPh object to set.
     * @return void
     */
    public function setTranslateEducStructToPh(TranslateEducStructToPhInterface $translateEducStructToPh): void;

    /**
     * Gets the translateEducStructToPh.
     *
     * @return TranslateEducStructToPhInterface The translateEducStructToPh.
     */
    public function getTranslateEducStructToPh(): TranslateEducStructToPhInterface;
}
