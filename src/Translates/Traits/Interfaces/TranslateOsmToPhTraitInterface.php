<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateOsmToPhInterface;

interface TranslateOsmToPhTraitInterface
{
    /**
     * Set the translateOsmToPh.
     *
     * @param TranslateOsmToPhInterface $translateOsmToPh The translateOsmToPh object to set.
     * @return void
     */
    public function setTranslateOsmToPh(TranslateOsmToPhInterface $translateOsmToPh): void;

    /**
     * Gets the translateOsmToPh.
     *
     * @return TranslateOsmToPhInterface The translateOsmToPh.
     */
    public function getTranslateOsmToPh(): TranslateOsmToPhInterface;
}
