<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsToPhInterface;

interface TranslateValueFlowsToPhTraitInterface
{
    /**
     * Set the translateValueFlowsToPh.
     *
     * @param TranslateValueFlowsToPhInterface $translateValueFlowsToPh The translateValueFlowsToPh object to set.
     * @return void
     */
    public function setTranslateValueFlowsToPh(TranslateValueFlowsToPhInterface $translateValueFlowsToPh): void;

    /**
     * Gets the translateValueFlowsToPh.
     *
     * @return TranslateValueFlowsToPhInterface The translateValueFlowsToPh.
     */
    public function getTranslateValueFlowsToPh(): TranslateValueFlowsToPhInterface;
}
