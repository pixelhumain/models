<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateKmlInterface;

interface TranslateKmlTraitInterface
{
    /**
     * Set the translateKml.
     *
     * @param TranslateKmlInterface $translateKml The translateKml object to set.
     * @return void
     */
    public function setTranslateKml(TranslateKmlInterface $translateKml): void;

    /**
     * Gets the translateKml.
     *
     * @return TranslateKmlInterface The translateKml.
     */
    public function getTranslateKml(): TranslateKmlInterface;
}
