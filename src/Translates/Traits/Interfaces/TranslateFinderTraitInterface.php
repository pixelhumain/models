<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateFinderInterface;

interface TranslateFinderTraitInterface
{
    /**
     * Set the translateFinder.
     *
     * @param TranslateFinderInterface $translateFinder The translateFinder object to set.
     * @return void
     */
    public function setTranslateFinder(TranslateFinderInterface $translateFinder): void;

    /**
     * Gets the translateFinder.
     *
     * @return TranslateFinderInterface The translateFinder.
     */
    public function getTranslateFinder(): TranslateFinderInterface;
}
