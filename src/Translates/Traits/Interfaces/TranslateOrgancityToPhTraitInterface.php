<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateOrgancityToPhInterface;

interface TranslateOrgancityToPhTraitInterface
{
    /**
     * Set the translateOrgancityToPh.
     *
     * @param TranslateOrgancityToPhInterface $translateOrgancityToPh The translateOrgancityToPh object to set.
     * @return void
     */
    public function setTranslateOrgancityToPh(TranslateOrgancityToPhInterface $translateOrgancityToPh): void;

    /**
     * Gets the translateOrgancityToPh.
     *
     * @return TranslateOrgancityToPhInterface The translateOrgancityToPh.
     */
    public function getTranslateOrgancityToPh(): TranslateOrgancityToPhInterface;
}
