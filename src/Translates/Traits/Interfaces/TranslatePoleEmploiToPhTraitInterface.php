<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslatePoleEmploiToPhInterface;

interface TranslatePoleEmploiToPhTraitInterface
{
    /**
     * Set the translatePoleEmploiToPh.
     *
     * @param TranslatePoleEmploiToPhInterface $translatePoleEmploiToPh The translatePoleEmploiToPh object to set.
     * @return void
     */
    public function setTranslatePoleEmploiToPh(TranslatePoleEmploiToPhInterface $translatePoleEmploiToPh): void;

    /**
     * Gets the translatePoleEmploiToPh.
     *
     * @return TranslatePoleEmploiToPhInterface The translatePoleEmploiToPh.
     */
    public function getTranslatePoleEmploiToPh(): TranslatePoleEmploiToPhInterface;
}
