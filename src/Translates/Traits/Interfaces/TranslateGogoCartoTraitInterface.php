<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateGogoCartoInterface;

interface TranslateGogoCartoTraitInterface
{
    /**
     * Set the translateGogoCarto.
     *
     * @param TranslateGogoCartoInterface $translateGogoCarto The translateGogoCarto object to set.
     * @return void
     */
    public function setTranslateGogoCarto(TranslateGogoCartoInterface $translateGogoCarto): void;

    /**
     * Gets the translateGogoCarto.
     *
     * @return TranslateGogoCartoInterface The translateGogoCarto.
     */
    public function getTranslateGogoCarto(): TranslateGogoCartoInterface;
}
