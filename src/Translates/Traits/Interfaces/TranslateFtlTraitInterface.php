<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateFtlInterface;

interface TranslateFtlTraitInterface
{
    /**
     * Set the translateFtl.
     *
     * @param TranslateFtlInterface $translateFtl The translateFtl object to set.
     * @return void
     */
    public function setTranslateFtl(TranslateFtlInterface $translateFtl): void;

    /**
     * Gets the translateFtl.
     *
     * @return TranslateFtlInterface The translateFtl.
     */
    public function getTranslateFtl(): TranslateFtlInterface;
}
