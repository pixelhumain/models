<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateNetworkInterface;

interface TranslateNetworkTraitInterface
{
    /**
     * Set the translateNetwork.
     *
     * @param TranslateNetworkInterface $translateNetwork The translateNetwork object to set.
     * @return void
     */
    public function setTranslateNetwork(TranslateNetworkInterface $translateNetwork): void;

    /**
     * Gets the translateNetwork.
     *
     * @return TranslateNetworkInterface The translateNetwork.
     */
    public function getTranslateNetwork(): TranslateNetworkInterface;
}
