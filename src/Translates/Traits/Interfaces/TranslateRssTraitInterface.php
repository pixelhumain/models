<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateRssInterface;

interface TranslateRssTraitInterface
{
    /**
     * Set the translateRss.
     *
     * @param TranslateRssInterface $translateRss The translateRss object to set.
     * @return void
     */
    public function setTranslateRss(TranslateRssInterface $translateRss): void;

    /**
     * Gets the translateRss.
     *
     * @return TranslateRssInterface The translateRss.
     */
    public function getTranslateRss(): TranslateRssInterface;
}
