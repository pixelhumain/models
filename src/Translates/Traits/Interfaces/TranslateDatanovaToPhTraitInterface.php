<?php

namespace PixelHumain\Models\Translates\Traits\Interfaces;

use PixelHumain\Models\Translates\Interfaces\TranslateDatanovaToPhInterface;

interface TranslateDatanovaToPhTraitInterface
{
    /**
     * Set the translateDatanovaToPh.
     *
     * @param TranslateDatanovaToPhInterface $translateDatanovaToPh The translateDatanovaToPh object to set.
     * @return void
     */
    public function setTranslateDatanovaToPh(TranslateDatanovaToPhInterface $translateDatanovaToPh): void;

    /**
     * Gets the translateDatanovaToPh.
     *
     * @return TranslateDatanovaToPhInterface The translateDatanovaToPh.
     */
    public function getTranslateDatanovaToPh(): TranslateDatanovaToPhInterface;
}
