<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateAgendaToPhInterface;

trait TranslateAgendaToPhTrait
{
    protected ?TranslateAgendaToPhInterface $translateAgendaToPh = null;

    /**
     * Set the translateAgendaToPh.
     *
     * @param TranslateAgendaToPhInterface $translateAgendaToPh The translateAgendaToPh object to set.
     * @return void
     */
    public function setTranslateAgendaToPh(TranslateAgendaToPhInterface $translateAgendaToPh): void
    {
        $this->translateAgendaToPh = $translateAgendaToPh;
    }

    /**
     * Gets the translateAgendaToPh.
     *
     * @return TranslateAgendaToPhInterface The translateAgendaToPh.
     */
    public function getTranslateAgendaToPh(): TranslateAgendaToPhInterface
    {
        if ($this->translateAgendaToPh === null) {
            throw new Exception(TranslateAgendaToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateAgendaToPh;
    }
}
