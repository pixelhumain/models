<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonToPhInterface;

trait TranslateGeoJsonToPhTrait
{
    protected ?TranslateGeoJsonToPhInterface $translateGeoJsonToPh = null;

    /**
     * Set the translateGeoJsonToPh.
     *
     * @param TranslateGeoJsonToPhInterface $translateGeoJsonToPh The translateGeoJsonToPh object to set.
     * @return void
     */
    public function setTranslateGeoJsonToPh(TranslateGeoJsonToPhInterface $translateGeoJsonToPh): void
    {
        $this->translateGeoJsonToPh = $translateGeoJsonToPh;
    }

    /**
     * Gets the translateGeoJsonToPh.
     *
     * @return TranslateGeoJsonToPhInterface The translateGeoJsonToPh.
     */
    public function getTranslateGeoJsonToPh(): TranslateGeoJsonToPhInterface
    {
        if ($this->translateGeoJsonToPh === null) {
            throw new Exception(TranslateGeoJsonToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateGeoJsonToPh;
    }
}
