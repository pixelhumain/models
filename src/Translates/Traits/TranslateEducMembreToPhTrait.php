<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateEducMembreToPhInterface;

trait TranslateEducMembreToPhTrait
{
    protected ?TranslateEducMembreToPhInterface $translateEducMembreToPh = null;

    /**
     * Set the translateEducMembreToPh.
     *
     * @param TranslateEducMembreToPhInterface $translateEducMembreToPh The translateEducMembreToPh object to set.
     * @return void
     */
    public function setTranslateEducMembreToPh(TranslateEducMembreToPhInterface $translateEducMembreToPh): void
    {
        $this->translateEducMembreToPh = $translateEducMembreToPh;
    }

    /**
     * Gets the translateEducMembreToPh.
     *
     * @return TranslateEducMembreToPhInterface The translateEducMembreToPh.
     */
    public function getTranslateEducMembreToPh(): TranslateEducMembreToPhInterface
    {
        if ($this->translateEducMembreToPh === null) {
            throw new Exception(TranslateEducMembreToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateEducMembreToPh;
    }
}
