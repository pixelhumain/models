<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateKmlInterface;

trait TranslateKmlTrait
{
    protected ?TranslateKmlInterface $translateKml = null;

    /**
     * Set the translateKml.
     *
     * @param TranslateKmlInterface $translateKml The translateKml object to set.
     * @return void
     */
    public function setTranslateKml(TranslateKmlInterface $translateKml): void
    {
        $this->translateKml = $translateKml;
    }

    /**
     * Gets the translateKml.
     *
     * @return TranslateKmlInterface The translateKml.
     */
    public function getTranslateKml(): TranslateKmlInterface
    {
        if ($this->translateKml === null) {
            throw new Exception(TranslateKmlInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateKml;
    }
}
