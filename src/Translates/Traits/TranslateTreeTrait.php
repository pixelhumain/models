<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateTreeInterface;

trait TranslateTreeTrait
{
    protected ?TranslateTreeInterface $translateTree = null;

    /**
     * Set the translateTree.
     *
     * @param TranslateTreeInterface $translateTree The translateTree object to set.
     * @return void
     */
    public function setTranslateTree(TranslateTreeInterface $translateTree): void
    {
        $this->translateTree = $translateTree;
    }

    /**
     * Gets the translateTree.
     *
     * @return TranslateTreeInterface The translateTree.
     */
    public function getTranslateTree(): TranslateTreeInterface
    {
        if ($this->translateTree === null) {
            throw new Exception(TranslateTreeInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateTree;
    }
}
