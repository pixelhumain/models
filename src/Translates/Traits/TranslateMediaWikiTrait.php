<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateMediaWikiInterface;

trait TranslateMediaWikiTrait
{
    protected ?TranslateMediaWikiInterface $translateMediaWiki = null;

    /**
     * Set the translateMediaWiki.
     *
     * @param TranslateMediaWikiInterface $translateMediaWiki The translateMediaWiki object to set.
     * @return void
     */
    public function setTranslateMediaWiki(TranslateMediaWikiInterface $translateMediaWiki): void
    {
        $this->translateMediaWiki = $translateMediaWiki;
    }

    /**
     * Gets the translateMediaWiki.
     *
     * @return TranslateMediaWikiInterface The translateMediaWiki.
     */
    public function getTranslateMediaWiki(): TranslateMediaWikiInterface
    {
        if ($this->translateMediaWiki === null) {
            throw new Exception(TranslateMediaWikiInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateMediaWiki;
    }
}
