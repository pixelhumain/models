<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateNetworkInterface;

trait TranslateNetworkTrait
{
    protected ?TranslateNetworkInterface $translateNetwork = null;

    /**
     * Set the translateNetwork.
     *
     * @param TranslateNetworkInterface $translateNetwork The translateNetwork object to set.
     * @return void
     */
    public function setTranslateNetwork(TranslateNetworkInterface $translateNetwork): void
    {
        $this->translateNetwork = $translateNetwork;
    }

    /**
     * Gets the translateNetwork.
     *
     * @return TranslateNetworkInterface The translateNetwork.
     */
    public function getTranslateNetwork(): TranslateNetworkInterface
    {
        if ($this->translateNetwork === null) {
            throw new Exception(TranslateNetworkInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateNetwork;
    }
}
