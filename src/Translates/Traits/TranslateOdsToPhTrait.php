<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateOdsToPhInterface;

trait TranslateOdsToPhTrait
{
    protected ?TranslateOdsToPhInterface $translateOdsToPh = null;

    /**
     * Set the translateOdsToPh.
     *
     * @param TranslateOdsToPhInterface $translateOdsToPh The translateOdsToPh object to set.
     * @return void
     */
    public function setTranslateOdsToPh(TranslateOdsToPhInterface $translateOdsToPh): void
    {
        $this->translateOdsToPh = $translateOdsToPh;
    }

    /**
     * Gets the translateOdsToPh.
     *
     * @return TranslateOdsToPhInterface The translateOdsToPh.
     */
    public function getTranslateOdsToPh(): TranslateOdsToPhInterface
    {
        if ($this->translateOdsToPh === null) {
            throw new Exception(TranslateOdsToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateOdsToPh;
    }
}
