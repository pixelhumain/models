<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateMDInterface;

trait TranslateMDTrait
{
    protected ?TranslateMDInterface $translateMD = null;

    /**
     * Set the translateMD.
     *
     * @param TranslateMDInterface $translateMD The translateMD object to set.
     * @return void
     */
    public function setTranslateMD(TranslateMDInterface $translateMD): void
    {
        $this->translateMD = $translateMD;
    }

    /**
     * Gets the translateMD.
     *
     * @return TranslateMDInterface The translateMD.
     */
    public function getTranslateMD(): TranslateMDInterface
    {
        if ($this->translateMD === null) {
            throw new Exception(TranslateMDInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateMD;
    }
}
