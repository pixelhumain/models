<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateEducStructToPhInterface;

trait TranslateEducStructToPhTrait
{
    protected ?TranslateEducStructToPhInterface $translateEducStructToPh = null;

    /**
     * Set the translateEducStructToPh.
     *
     * @param TranslateEducStructToPhInterface $translateEducStructToPh The translateEducStructToPh object to set.
     * @return void
     */
    public function setTranslateEducStructToPh(TranslateEducStructToPhInterface $translateEducStructToPh): void
    {
        $this->translateEducStructToPh = $translateEducStructToPh;
    }

    /**
     * Gets the translateEducStructToPh.
     *
     * @return TranslateEducStructToPhInterface The translateEducStructToPh.
     */
    public function getTranslateEducStructToPh(): TranslateEducStructToPhInterface
    {
        if ($this->translateEducStructToPh === null) {
            throw new Exception(TranslateEducStructToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateEducStructToPh;
    }
}
