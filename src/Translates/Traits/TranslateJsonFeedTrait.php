<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateJsonFeedInterface;

trait TranslateJsonFeedTrait
{
    protected ?TranslateJsonFeedInterface $translateJsonFeed = null;

    /**
     * Set the translateJsonFeed.
     *
     * @param TranslateJsonFeedInterface $translateJsonFeed The translateJsonFeed object to set.
     * @return void
     */
    public function setTranslateJsonFeed(TranslateJsonFeedInterface $translateJsonFeed): void
    {
        $this->translateJsonFeed = $translateJsonFeed;
    }

    /**
     * Gets the translateJsonFeed.
     *
     * @return TranslateJsonFeedInterface The translateJsonFeed.
     */
    public function getTranslateJsonFeed(): TranslateJsonFeedInterface
    {
        if ($this->translateJsonFeed === null) {
            throw new Exception(TranslateJsonFeedInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateJsonFeed;
    }
}
