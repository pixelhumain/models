<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateEducEcoleToPhInterface;

trait TranslateEducEcoleToPhTrait
{
    protected ?TranslateEducEcoleToPhInterface $translateEducEcoleToPh = null;

    /**
     * Set the translateEducEcoleToPh.
     *
     * @param TranslateEducEcoleToPhInterface $translateEducEcoleToPh The translateEducEcoleToPh object to set.
     * @return void
     */
    public function setTranslateEducEcoleToPh(TranslateEducEcoleToPhInterface $translateEducEcoleToPh): void
    {
        $this->translateEducEcoleToPh = $translateEducEcoleToPh;
    }

    /**
     * Gets the translateEducEcoleToPh.
     *
     * @return TranslateEducEcoleToPhInterface The translateEducEcoleToPh.
     */
    public function getTranslateEducEcoleToPh(): TranslateEducEcoleToPhInterface
    {
        if ($this->translateEducEcoleToPh === null) {
            throw new Exception(TranslateEducEcoleToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateEducEcoleToPh;
    }
}
