<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateFtlInterface;

trait TranslateFtlTrait
{
    protected ?TranslateFtlInterface $translateFtl = null;

    /**
     * Set the translateFtl.
     *
     * @param TranslateFtlInterface $translateFtl The translateFtl object to set.
     * @return void
     */
    public function setTranslateFtl(TranslateFtlInterface $translateFtl): void
    {
        $this->translateFtl = $translateFtl;
    }

    /**
     * Gets the translateFtl.
     *
     * @return TranslateFtlInterface The translateFtl.
     */
    public function getTranslateFtl(): TranslateFtlInterface
    {
        if ($this->translateFtl === null) {
            throw new Exception(TranslateFtlInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateFtl;
    }
}
