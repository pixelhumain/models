<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateActivityStreamInterface;

trait TranslateActivityStreamTrait
{
    protected ?TranslateActivityStreamInterface $translateActivityStream = null;

    /**
     * Set the translateActivityStream.
     *
     * @param TranslateActivityStreamInterface $translateActivityStream The translateActivityStream object to set.
     * @return void
     */
    public function setTranslateActivityStream(TranslateActivityStreamInterface $translateActivityStream): void
    {
        $this->translateActivityStream = $translateActivityStream;
    }

    /**
     * Gets the translateActivityStream.
     *
     * @return TranslateActivityStreamInterface The translateActivityStream.
     */
    public function getTranslateActivityStream(): TranslateActivityStreamInterface
    {
        if ($this->translateActivityStream === null) {
            throw new Exception(TranslateActivityStreamInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateActivityStream;
    }
}
