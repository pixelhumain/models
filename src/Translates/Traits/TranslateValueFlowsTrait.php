<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsInterface;

trait TranslateValueFlowsTrait
{
    protected ?TranslateValueFlowsInterface $translateValueFlows = null;

    /**
     * Set the translateValueFlows.
     *
     * @param TranslateValueFlowsInterface $translateValueFlows The translateValueFlows object to set.
     * @return void
     */
    public function setTranslateValueFlows(TranslateValueFlowsInterface $translateValueFlows): void
    {
        $this->translateValueFlows = $translateValueFlows;
    }

    /**
     * Gets the translateValueFlows.
     *
     * @return TranslateValueFlowsInterface The translateValueFlows.
     */
    public function getTranslateValueFlows(): TranslateValueFlowsInterface
    {
        if ($this->translateValueFlows === null) {
            throw new Exception(TranslateValueFlowsInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateValueFlows;
    }
}
