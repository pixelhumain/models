<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateOsmToPhInterface;

trait TranslateOsmToPhTrait
{
    protected ?TranslateOsmToPhInterface $translateOsmToPh = null;

    /**
     * Set the translateOsmToPh.
     *
     * @param TranslateOsmToPhInterface $translateOsmToPh The translateOsmToPh object to set.
     * @return void
     */
    public function setTranslateOsmToPh(TranslateOsmToPhInterface $translateOsmToPh): void
    {
        $this->translateOsmToPh = $translateOsmToPh;
    }

    /**
     * Gets the translateOsmToPh.
     *
     * @return TranslateOsmToPhInterface The translateOsmToPh.
     */
    public function getTranslateOsmToPh(): TranslateOsmToPhInterface
    {
        if ($this->translateOsmToPh === null) {
            throw new Exception(TranslateOsmToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateOsmToPh;
    }
}
