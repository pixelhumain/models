<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateOrgancityToPhInterface;

trait TranslateOrgancityToPhTrait
{
    protected ?TranslateOrgancityToPhInterface $translateOrgancityToPh = null;

    /**
     * Set the translateOrgancityToPh.
     *
     * @param TranslateOrgancityToPhInterface $translateOrgancityToPh The translateOrgancityToPh object to set.
     * @return void
     */
    public function setTranslateOrgancityToPh(TranslateOrgancityToPhInterface $translateOrgancityToPh): void
    {
        $this->translateOrgancityToPh = $translateOrgancityToPh;
    }

    /**
     * Gets the translateOrgancityToPh.
     *
     * @return TranslateOrgancityToPhInterface The translateOrgancityToPh.
     */
    public function getTranslateOrgancityToPh(): TranslateOrgancityToPhInterface
    {
        if ($this->translateOrgancityToPh === null) {
            throw new Exception(TranslateOrgancityToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateOrgancityToPh;
    }
}
