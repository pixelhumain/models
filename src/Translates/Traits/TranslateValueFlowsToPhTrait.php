<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateValueFlowsToPhInterface;

trait TranslateValueFlowsToPhTrait
{
    protected ?TranslateValueFlowsToPhInterface $translateValueFlowsToPh = null;

    /**
     * Set the translateValueFlowsToPh.
     *
     * @param TranslateValueFlowsToPhInterface $translateValueFlowsToPh The translateValueFlowsToPh object to set.
     * @return void
     */
    public function setTranslateValueFlowsToPh(TranslateValueFlowsToPhInterface $translateValueFlowsToPh): void
    {
        $this->translateValueFlowsToPh = $translateValueFlowsToPh;
    }

    /**
     * Gets the translateValueFlowsToPh.
     *
     * @return TranslateValueFlowsToPhInterface The translateValueFlowsToPh.
     */
    public function getTranslateValueFlowsToPh(): TranslateValueFlowsToPhInterface
    {
        if ($this->translateValueFlowsToPh === null) {
            throw new Exception(TranslateValueFlowsToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateValueFlowsToPh;
    }
}
