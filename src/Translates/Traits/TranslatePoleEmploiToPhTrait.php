<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslatePoleEmploiToPhInterface;

trait TranslatePoleEmploiToPhTrait
{
    protected ?TranslatePoleEmploiToPhInterface $translatePoleEmploiToPh = null;

    /**
     * Set the translatePoleEmploiToPh.
     *
     * @param TranslatePoleEmploiToPhInterface $translatePoleEmploiToPh The translatePoleEmploiToPh object to set.
     * @return void
     */
    public function setTranslatePoleEmploiToPh(TranslatePoleEmploiToPhInterface $translatePoleEmploiToPh): void
    {
        $this->translatePoleEmploiToPh = $translatePoleEmploiToPh;
    }

    /**
     * Gets the translatePoleEmploiToPh.
     *
     * @return TranslatePoleEmploiToPhInterface The translatePoleEmploiToPh.
     */
    public function getTranslatePoleEmploiToPh(): TranslatePoleEmploiToPhInterface
    {
        if ($this->translatePoleEmploiToPh === null) {
            throw new Exception(TranslatePoleEmploiToPhInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translatePoleEmploiToPh;
    }
}
