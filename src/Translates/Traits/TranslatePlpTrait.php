<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslatePlpInterface;

trait TranslatePlpTrait
{
    protected ?TranslatePlpInterface $translatePlp = null;

    /**
     * Set the translatePlp.
     *
     * @param TranslatePlpInterface $translatePlp The translatePlp object to set.
     * @return void
     */
    public function setTranslatePlp(TranslatePlpInterface $translatePlp): void
    {
        $this->translatePlp = $translatePlp;
    }

    /**
     * Gets the translatePlp.
     *
     * @return TranslatePlpInterface The translatePlp.
     */
    public function getTranslatePlp(): TranslatePlpInterface
    {
        if ($this->translatePlp === null) {
            throw new Exception(TranslatePlpInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translatePlp;
    }
}
