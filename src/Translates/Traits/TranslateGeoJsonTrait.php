<?php

namespace PixelHumain\Models\Translates\Traits;

use Exception;
use PixelHumain\Models\Translates\Interfaces\TranslateGeoJsonInterface;

trait TranslateGeoJsonTrait
{
    protected ?TranslateGeoJsonInterface $translateGeoJson = null;

    /**
     * Set the translateGeoJson.
     *
     * @param TranslateGeoJsonInterface $translateGeoJson The translateGeoJson object to set.
     * @return void
     */
    public function setTranslateGeoJson(TranslateGeoJsonInterface $translateGeoJson): void
    {
        $this->translateGeoJson = $translateGeoJson;
    }

    /**
     * Gets the translateGeoJson.
     *
     * @return TranslateGeoJsonInterface The translateGeoJson.
     */
    public function getTranslateGeoJson(): TranslateGeoJsonInterface
    {
        if ($this->translateGeoJson === null) {
            throw new Exception(TranslateGeoJsonInterface::class . " is not set in model (" . self::class . "::" . __FUNCTION__ . "::" . __LINE__ . ")");
        }
        return $this->translateGeoJson;
    }
}
