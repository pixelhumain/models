<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslatePlpInterface;

class TranslatePlp implements TranslatePlpInterface
{
    /*

        ----------------- PLP -----------------

        https://github.com/hackers4peace/plp-test-data/blob/master/graph.jsonld
        */
    //http://127.0.0.1/ph/communecter/data/get/type/citoyens/id/520931e2f6b95c5cd3003d6c/format/plp
    public static array $dataBinding_person = [
        "@context" => "https://w3id.org/plp/v1",
        "@type" => "Person",
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/citoyens/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "image" => [
            "valueOf" => "img",
            "type" => "url",
            "prefix" => "/communecter/",
        ],
        "birthDate" => [
            "valueOf" => "bitrh",
        ],
        "currentLocation" => [
            "valueOf" => [
                "@type" => "Place",
                "name" => "CURRENT",
                "address" => [
                    "parentKey" => "address",
                    "valueOf" => [
                        "@type" => "PostalAddress",
                        "@id" => [
                            "valueOf" => 'codeInsee',
                            "type" => "url",
                            "prefix" => "/data/get/type/city/insee/",
                            "suffix" => "/format/schema",
                        ],
                        "addressLocality" => [
                            "valueOf" => "addressLocality",
                        ],
                        "addressRegion" => [
                            "valueOf" => "region",
                        ],
                        "postalCode" => [
                            "valueOf" => "postalCode",
                        ],
                        "streetAddress" => [
                            "valueOf" => "streetAddress",
                        ],
                    ],
                ],
            ],
        ],
        "homeLocation" => [
            "valueOf" => [
                "@type" => "Place",
                "name" => "HOME",
                "address" => [
                    "parentKey" => "address",
                    "valueOf" => [
                        "@type" => "PostalAddress",
                        "@id" => [
                            "valueOf" => 'codeInsee',
                            "type" => "url",
                            "prefix" => "/data/get/type/city/insee/",
                            "suffix" => "/format/schema",
                        ],
                        "addressLocality" => [
                            "valueOf" => "addressLocality",
                        ],
                        "addressRegion" => [
                            "valueOf" => "region",
                        ],
                        "postalCode" => [
                            "valueOf" => "postalCode",
                        ],
                        "streetAddress" => [
                            "valueOf" => "streetAddress",
                        ],
                    ],
                ],
            ],
        ],
        "description" => [
            "valueOf" => "description",
        ],
        "contactPoint" => [
            [
                "name" => "email",
                "@type" => "ContactPoint",
                "id" => [
                    "valueOf" => "email",
                    "prefix" => "mailto:",
                ],
            ],
            [
                "name" => "phoneNumber",
                "@type" => "ContactPoint",
                "id" => [
                    "valueOf" => "telephone",
                ],
            ],
            [
                "name" => "jabber",
                "@type" => "ContactPoint",
                "id" => [
                    "valueOf" => "jabber",
                ],
            ],
            [
                "name" => "irc",
                "@type" => "ContactPoint",
                "id" => [
                    "valueOf" => "jabber",
                ],
            ],
            [
                "name" => "jabber",
                "@type" => "ContactPoint",
                "id" => [
                    "valueOf" => "jabber",
                ],
            ],
        ],
        "sameAs" => [[
            "name" => "Website",
            "id" => [
                "valueOf" => "url",
            ],
        ], [
            "name" => "Github",
            "id" => [
                "valueOf" => "socialNetwork.github",
            ],
        ], [
            "name" => "Twitter",
            "id" => [
                "valueOf" => "socialNetwork.twitter",
            ],
        ], [
            "name" => "Facebook",
            "id" => [
                "valueOf" => "socialNetwork.facebook",
            ],
        ], [
            "name" => "Google+",
            "id" => [
                "valueOf" => "socialNetwork.googleplus",
            ],
        ], [
            "name" => "LinkedIn",
            "id" => [
                "valueOf" => "socialNetwork.linkedin",
            ],
        ], [
            "name" => "Skype",
            "id" => [
                "valueOf" => "socialNetwork.skype",
            ],
        ]],
        "cco:skill" => [
            "valueOf" => "positions",
        ],
        "foaf:currentProject" => [
            "object" => "links.projects",
            "collection" => "projects",
            "valueOf" => [
                "type" => "doap:Project",
                "id" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/data/get/type/projects/id/",
                    "suffix" => "/format/schema",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
        "member" => [
            "object" => "links.memberOf",
            "collection" => "organizations",
            "valueOf" => [
                "type" => "Organization",
                "id" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/data/get/type/organizations/id/",
                    "suffix" => "/format/schema",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
        "attendeeIn" => [
            "object" => "links.events",
            "collection" => "events",
            "valueOf" => [
                "type" => "Event",
                "id" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/data/get/type/events/id/",
                    "suffix" => "/format/schema",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
    ];
}
