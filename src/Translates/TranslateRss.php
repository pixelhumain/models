<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\BaseModel;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Translates\Interfaces\TranslateRssInterface;

//TODO : Yii::get('application')->controller->module->assetsUrl

class TranslateRss extends BaseModel implements TranslateRssInterface, ElementTraitInterface, DocumentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ElementTrait;
    use DocumentTrait;

    public static array $dataBinding_news = [
        "link" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            //url de la news visible quand on à un très grand texte comme news
            "prefix" => "/#news.detail.id.",
            "suffix" => "",
        ],
        "title" => [
            "type" => "title",
            "type_el" => [
                "valueOf" => "type",
            ],
            "object_news" => [
                "valueOf" => "object",
            ],
        ],
        "description" => [
            "type" => "description",
            "verb" => [
                "valueOf" => "verb",
            ],
            "object_news" => [
                "valueOf" => "object",
            ],
            "text" => [
                "valueOf" => "text",
            ],
            "author" => [
                "valueOf" => "author",
            ],
            "target" => [
                "valueOf" => "target",
            ],
        ],
        "pubDate" => [
            "valueOf" => "date",
            "type" => "date",
            "prefix" => "",
            "suffix" => "",
        ],
        "guid" => [
            "valueOf" => "_id",
        ],
        "enclosure" => [
            "type" => "image_rss",
            "id" => [
                "valueOf" => '_id.$id',
            ],
            "target_id" => [
                "valueOf" => "target.id",
            ],
            "target_type" => [
                "valueOf" => "target.type",
            ],
            "verb" => [
                "valueOf" => "verb",
            ],
            "object_type" => [
                "valueOf" => "object.objectType",
            ],
            "object_id" => [
                "valueOf" => "object.id",
            ],
            "type2" => [
                "valueOf" => "type",
            ],
            "image_id" => [
                "valueOf" => "media.images.0",
            ],
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
    }

    /**
     * Formats the given value based on its type.
     *
     * @param array $val The value to be formatted.
     * @param array|null $bindPath The path to bind the formatted value.
     * @return array|string The formatted value.
     */
    public function specFormatByType(array $val, ?array $bindPath)
    {
        $formattedValue = ""; // Variable pour stocker la chaîne formatée
        $author = [];

        if (isset($bindPath["type"]) && $bindPath["type"] == "description") {
            if (! empty($val["text"])) {
                $formattedValue = $val["text"];
            } else {
                if (is_array($val["object_news"]) && ! empty($val["object_news"]["objectType"]) && ! empty($val["object_news"]["id"])) {
                    $element = $this->getModelElement()->getSimpleByTypeAndId($val["object_news"]["objectType"], $val["object_news"]["id"]);

                    if (isset($val["target"]["type"]) && (isset($val["target"]["id"]))) {
                        $author = $this->getModelElement()->getSimpleByTypeAndId($val["target"]["type"], $val["target"]["id"]);
                    }

                    $verb = (string) $val["verb"];
                    $type = (string) $val["object_news"]["objectType"];
                    $val_nom = "Nom non défini"; // Initialisation par défaut

                    if (in_array($type, ["events", OrganizationInterface::COLLECTION, "projects"]) && is_array($element) && ! empty($element["name"])) {
                        $val_nom = (string) $element["name"];
                    }

                    $formattedValue = is_array($author) && ! empty($author["username"]) ? (string) $author["username"] : 'Quelqu\'un ou quelque chose ';
                    $formattedValue .= ' ' . ($verb == "create" ? "a crée un(e) nouvel(le) " : $verb);

                    switch ($type) {
                        case OrganizationInterface::COLLECTION:
                            $formattedValue .= ' Organisation';
                            break;
                        case "projects":
                            $formattedValue .= ' Projet';
                            break;
                        case "events":
                            $formattedValue .= ' Evenement';
                            break;
                    }

                    $formattedValue .= ' sous le nom de "' . $val_nom . '"';
                }
            }
        }

        if (isset($bindPath["type"]) && $bindPath["type"] == "title") {
            $type = $val["type_el"];
            $object_type = $val["object_news"]["objectType"] ?? null;

            if ($type == "news") {
                $formattedValue = "Rédaction d'un message";
            } elseif ($type == "activityStream") {
                $formattedValue = "Création";
                switch ($object_type) {
                    case OrganizationInterface::COLLECTION:
                        $formattedValue .= " d'une Organisation";
                        break;
                    case "projects":
                        $formattedValue .= " d'un Projet";
                        break;
                    case "events":
                        $formattedValue .= " d'un Evenement";
                        break;
                }
            }
        }

        return ! empty($formattedValue) ? (string) $formattedValue : $val; // Renvoie la chaîne formatée ou $val si non formaté
    }

    /**
     * Retrieves the RSS image.
     *
     * @param array $val The array containing the RSS data.
     * @param string|null $bindPath The optional bind path.
     * @return string The RSS image.
     */
    public function getRssImage(array $val, ?string $bindPath = null): string
    {
        $assetsUrl = Yii::get('application')->controller->module->assetsUrl;
        $baseUrl = "http://127.0.0.1";
        $defaultImagePath = $baseUrl . "/" . $assetsUrl . "/images/thumbnail-default.jpg";
        $imagePath = null;

        if (isset($val["verb"])) {
            $docCrea = $this->getModelDocument()->getListDocumentsByIdAndType($val["object_id"], $val["object_type"]);
            $imagePath = is_array($docCrea) && is_array($docCrea["profil"]) && ! empty($docCrea["profil"]["imagePath"]) ? (string) $docCrea["profil"]["imagePath"] : null;
        } elseif (isset($val["image_id"])) {
            $docs = $this->getModelDocument()->getListDocumentsByIdAndType($val["target_id"], $val["target_type"]);
            if (! empty($docs['slider']) && is_array($docs['slider'])) {
                foreach ($docs['slider'] as $doc) {
                    if (is_array($doc) && $val["image_id"] == $doc["image_id"]) {
                        $imagePath = $doc["imagePath"];
                        break;
                    }
                }
            }
        }

        return is_string($imagePath) ? $baseUrl . $imagePath : $defaultImagePath;
    }
}
