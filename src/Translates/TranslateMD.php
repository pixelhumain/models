<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateMDInterface;

class TranslateMD implements TranslateMDInterface
{
    /*

        ----------------- COMMUNECTER -----------------
    */

    /**
     * TranslateMD class.
     *
     * This class provides a method to translate an array of data into a string representation based on the specified type.
     *
     * @param array $data The data to be translated.
     * @param string $type The type of translation to be performed.
     *
     * @return string|null The translated string representation of the data, or null if the translation failed.
     */
    public function MD(array $data, string $type): ?string
    {
        $str = null;
        foreach ($data as $keyID => $valueData) {
            if (! empty($valueData) && is_array($valueData)) {
                $str = "[<i class='fa fa-link'></i> Markdown Source](http://communecter.org/api/" . $type . "/get/id/" . $keyID . "/format/md) \n\n\n";
                $str .= "<span class='text-red'>\n" .
                        "## I am a " . (! empty($valueData["name"]) ? (string) $valueData["name"] : "") . "\n<br/> \n"
                        . "</span>\n";

                if (! empty($valueData["shortDescription"])) {
                    $str .= "> " . (string) $valueData["shortDescription"] . "<br/><br/>\n";
                }
                if (! empty($valueData["description"])) {
                    $str .= "> " . (string) $valueData["description"] . "<br/><br/>\n\n";
                }

                $str .= "### <i class='fa fa-user'></i> Information : " . "\n" .

                        "- my ID :" . $keyID . "\n" .
                        "- email : " . (! empty($valueData["email"]) ? (string) $valueData["email"] : "") . "\n" .
                        "- username : " . (! empty($valueData["username"]) ? (string) $valueData["username"] : "") . "\n" .

                        //"### Where : "."\n".
                        "- country : " . (! empty($valueData["address"]["addressCountry"]) ? (string) $valueData["address"]["addressCountry"] : "") . "\n" .
                        "- city : " . (! empty($valueData["address"]["addressLocality"]) ? (string) $valueData["address"]["addressLocality"] : "") . "\n" .
                        //"- email : ".@$valueData["email"]."\n".
                        "- postalCode : " . (! empty($valueData["address"]["postalCode"]) ? (string) $valueData["address"]["postalCode"] : "") . "\n" .
                        "- [url]( http://www.openstreetmap.org/?mlat=" . (! empty($valueData["geo"]["latitude"]) ? (string) $valueData["geo"]["latitude"] : "") . "&mlon=" . (! empty($valueData["geo"]["longitude"]) ? (string) $valueData["geo"]["longitude"] : "") . "&zoom=12 )\n" .
                        "\n";

                if (! empty($valueData["links"]["memberOf"])) {
                    $str .= "### <i class='fa fa-group'></i> Organizations : \n";
                    foreach ($valueData["links"]["memberOf"] as $ix => $o) {
                        if (is_array($o)) {
                            $urlT = explode("#", (! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : ""));
                            if (! empty($urlT[1])) {
                                $el = "- <a href='#" . $urlT[1] . "' class='lbh'>" . (! empty($o["name"]) ? (string) $o["name"] : "") . "</a>";
                                if (! empty($o["isAdmin"]) && $o["isAdmin"] == "true") {
                                    $el .= "(Admin)";
                                }
                                $str .= $el . "\n";
                            }
                        }
                    }
                    $str .= "\n";
                }

                if (! empty($valueData["links"]["projects"])) {
                    $str .= "### <i class='fa fa-lightbulb-o'></i> Projects : \n";
                    foreach ($valueData["links"]["projects"] as $ix => $o) {
                        if (is_array($o)) {
                            $urlT = explode("#", (! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : ""));
                            if (! empty($urlT[1])) {
                                $el = "- <a href='#" . $urlT[1] . "' class='lbh'>" . (! empty($o["name"]) ? (string) $o["name"] : "") . "</a>";
                                if (! empty($o["isAdmin"])) {
                                    $el .= "(Admin)";
                                }
                                $str .= $el . "\n";
                            }
                        }
                    }
                    $str .= "\n";
                }

                if (! empty($valueData["links"]["events"])) {
                    $str .= "### <i class='fa fa-calendar'></i> Events : \n";
                    foreach ($valueData["links"]["events"] as $ix => $o) {
                        if (is_array($o)) {
                            $urlT = explode("#", (! empty($o["url"]["communecter"]) ? (string) $o["url"]["communecter"] : ""));
                            if (! empty($urlT[1])) {
                                $el = "- <a href='#" . $urlT[1] . "' class='lbh'>" . (! empty($o["name"]) ? (string) $o["name"] : "") . "</a>";
                                if (! empty($o["isAdmin"])) {
                                    $el .= "(Admin)";
                                }
                                $str .= $el . "\n";
                            }
                        }
                    }
                    $str .= "\n";
                }

                /*	"# Points of interests"."\n".
                    "# Ressources : "."\n".
                    "# Places : "."\n".
                    "# Classifieds : "; */
            }
        }
        return $str;
    }
}
