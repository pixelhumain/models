<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateWikiToPhInterface;

class TranslateWikiToPh implements TranslateWikiToPhInterface
{
    public static array $mapping_element = [
        "0" => "name",
        "1.longitude" => "geo.longitude",
        "1.latitude" => "geo.latitude",
        "2" => "url",
        "3" => "description",
        "4" => "type",
    ];
}
