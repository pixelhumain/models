<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateAgendaToPhInterface;

class TranslateAgendaToPh implements TranslateAgendaToPhInterface
{
    public function GetOpenAgendaMapping(): array
    {
        return [
            'slug' => 'slug',
            'title' => 'name',
        ];
    }

    public function GetICalendarMapping(): array
    {
        return [

        ];
    }

    public function GetGoogleCalendarMapping(): array
    {
        return [];
    }
}
