<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateMediaWikiInterface;

class TranslateMediaWiki implements TranslateMediaWikiInterface
{
    private string $type = '';

    /**
     * Binds data to the TranslateMediaWiki class.
     *
     * @param string $categorie The category of the data.
     * @param string $wikiName The name of the wiki.
     * @return array The array of data bindings.
     */
    public function dataBinding(string $categorie, string $wikiName): array
    {
        switch ($categorie) {
            case 'acteurs':
                $this->type = "Organization";
                break;
            case 'projets':
                $this->type = "Project";
                break;
            case 'utilisateurs':
                $this->type = "Person";
                break;
            case 'ressources':
                $this->type = "Classifieds";
                break;
            default:
                $this->type = "Classifieds";
                break;
        }
        $map = $this->{$wikiName}($this->type) ?? $this->baseMap($this->type);
        return (array) $map;
    }

    /**
     * Returns the base map for the given type.
     *
     * @param string $type The type of map.
     * @return array The base map.
     */
    public function baseMap(string $type): array
    {
        return [
            "@type" => $type,
            "name" => [
                "valueOf" => "title",
            ],
            "image" => [
                "valueOf" => "logo",
            ],
            "slug" => [
                "valueOf" => "inCo.0",
            ],
            "id" => [
                "valueOf" => "id",
            ],
            "parrent" => [
                "valueOf" => "name",
            ],
            "urls" => [
                "webSite" => [
                    "valueOf" => "Website.0",
                ],
                "wiki" => [
                    "valueOf" => "url",
                ],
            ],
        ];
    }

    /**
     * TranslateMediaWiki class.
     *
     * This class is responsible for translating MediaWiki content.
     *
     * @param string $type The type of translation.
     *
     * @return array The translated content.
     */
    private function fabmob(string $type): array
    {
        $map = $this->baseMap($type);
        $map += [
            "type" => [
                "valueOf" => "Type",
            ],
            "shortDescription" => [
                "valueOf" => "ShortDescription.0",
            ],
            "description" => [
                "valueOf" => "Description.0",
            ],
            "tags" => [
                "valueOf" => "Tags",
            ],
            "themes" => [
                "valueOf" => "Theme",
            ],
            "skill" => [
                "object" => "Skills",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
            "themeUt" => [
                "valueOf" => "Themes",
            ],
            "coInWiki" => [
                "valueOf" => "PageCo.0",
            ],
            "city" => [
                "valueOf" => "Ville",
            ],
            "country" => [
                "valueOf" => "Lieu projet",
            ],
            "stateProject" => [
                "valueOf" => "Develop.0",
            ],
            "geoPosition" => [
                "valueOf" => "Coordonnées géo.0",
            ],
            "acteur" => [
                "object" => "Acteur",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
            "communaute" => [
                "object" => "Communauté",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
            "defi" => [
                "object" => "Défi",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
            "ressource" => [
                "object" => "Ressource",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
            "utilisateur" => [
                "object" => "Utilisateur",
                "valueOf" => [
                    "name" => [
                        "valueOf" => "fulltext",
                    ],
                    "url" => [
                        "valueOf" => "fullurl",
                    ],
                ],
            ],
        ];
        return $map;
    }

    /**
     * Translates the given type using the Movilab translation service.
     *
     * @param string $type The type to be translated.
     * @return array The translated type.
     */
    private function movilab(string $type): array
    {
        $map = $this->baseMap($type);
        $map += [];
        return $map;
    }
}
