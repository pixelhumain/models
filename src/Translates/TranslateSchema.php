<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateSchemaInterface;

class TranslateSchema implements TranslateSchemaInterface
{
    public static array $dataBinding_allPerson = [
        "@context" => "http://schema.org",
        "@type" => "Person",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/citoyens/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_person = [
        "@context" => "http://schema.org",
        "@type" => "Person",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/citoyens/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "@id" => [
                    "valueOf" => 'codeInsee',
                    "type" => "url",
                    "prefix" => "/data/get/type/city/insee/",
                    "suffix" => "/format/schema",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressRegion" => [
                    "valueOf" => "region",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "image" => [
            "valueOf" => "img",
            "type" => "url",
            "prefix" => "/",
        ],
        "jobTitle" => [
            "valueOf" => "positions",
        ],
        "telephone" => [
            "valueOf" => "phoneNumber",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_allOrganization = [
        "@context" => "http://schema.org",
        "@type" => "Organization",
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/organizations/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_organization = [
        "@context" => "http://schema.org",
        "@type" => "Organization",
        "id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/organizations/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "@id" => [
                    "valueOf" => 'codeInsee',
                    "type" => "url",
                    "prefix" => "/data/get/type/city/insee/",
                    "suffix" => "/format/schema",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressRegion" => [
                    "valueOf" => "region",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "image" => [
            "valueOf" => "img",
            "type" => "url",
            "prefix" => "/communecter/",
        ],
        "telephone" => [
            "valueOf" => "phoneNumber",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_allProject = [
        "@context" => "http://schema.org",
        "@type" => "CreativeWork",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/projects/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_project = [
        "@context" => "http://schema.org",
        "@type" => "CreativeWork",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/projects/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "@id" => [
                    "valueOf" => 'codeInsee',
                    "type" => "url",
                    "prefix" => "/data/get/type/city/insee/",
                    "suffix" => "/format/schema",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressRegion" => [
                    "valueOf" => "region",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "image" => [
            "valueOf" => "img",
            "type" => "url",
            "prefix" => "/communecter/",
        ],
        "telephone" => [
            "valueOf" => "phoneNumber",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_allEvent = [
        "@context" => "http://schema.org",
        "@type" => "CreativeWork",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/projects/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "url" => [
            "valueOf" => "url",
        ],
    ];

    public static array $dataBinding_event = [
        "@context" => "http://schema.org",
        "@type" => "Event",
        "@id" => [
            "valueOf" => '_id.$id',
            "type" => "url",
            "prefix" => "/data/get/type/events/id/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "address" => [
            "parentKey" => "address",
            "valueOf" => [
                "@type" => "PostalAddress",
                "@id" => [
                    "valueOf" => 'codeInsee',
                    "type" => "url",
                    "prefix" => "/data/get/type/city/insee/",
                    "suffix" => "/format/schema",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressRegion" => [
                    "valueOf" => "region",
                ],
                "postalCode" => [
                    "valueOf" => "postalCode",
                ],
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
            ],
        ],
        "email" => [
            "valueOf" => "email",
        ],
        "image" => [
            "valueOf" => "img",
            "type" => "url",
            "prefix" => "/communecter/",
        ],
        "telephone" => [
            "valueOf" => "phoneNumber",
        ],
        "url" => [
            "valueOf" => "url",
        ],
        "startDate" => [
            "valueOf" => "startDate",
        ],
        "endDate" => [
            "valueOf" => "endDate",
        ],
        "eventStatus" => [
            "valueOf" => "eventStatus",
        ],
        "organizers" => [
            "object" => "links.organizer",
            "collection" => "organizations",
            "valueOf" => [
                "@type" => "Organization",
                "@id" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/data/get/type/organizations/id/",
                    "suffix" => "/format/schema",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
        "attendees" => [
            "object" => "links.attendees",
            "collection" => "citoyens",
            "valueOf" => [
                "@type" => "Person",
                "@id" => [
                    "valueOf" => '_id.$id',
                    "type" => "url",
                    "prefix" => "/data/get/type/citoyens/id/",
                    "suffix" => "/format/schema",
                ],
                "name" => [
                    "valueOf" => "name",
                ],
            ],
        ],
    ];

    //http://127.0.0.1/ph/communecter/data/get/type/cities/insee/97414/format/schema
    public static array $dataBinding_city = [
        "@context" => "http://schema.org",
        "@type" => "City",
        "@id" => [
            "valueOf" => 'insee',
            "type" => "url",
            "prefix" => "/data/get/type/cities/insee/",
            "suffix" => "/format/schema",
        ],
        "name" => [
            "valueOf" => "name",
        ],
        "alternateName" => [
            "valueOf" => "alternateName",
        ],
        "address" => [
            "valueOf" => [
                "@type" => "PostalAddress",
                "@id" => [
                    "valueOf" => 'insee',
                    "type" => "url",
                    "prefix" => "/data/get/type/city/insee/",
                    "suffix" => "/format/schema",
                ],
                "addressLocality" => [
                    "valueOf" => "addressLocality",
                ],
                "addressRegion" => [
                    "valueOf" => "region",
                ],
                "postalCode" => [
                    "valueOf" => "cp",
                ],
                "streetAddress" => [
                    "valueOf" => "streetAddress",
                ],
            ],
        ],
        "geo" => [
            "valueOf" => "geo",
        ],
    ];
}
