<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateDatanovaToPhInterface;

class TranslateDatanovaToPh implements TranslateDatanovaToPhInterface
{
    public static array $mapping_activity = [
        "fields.libelle_du_site" => "name",
        "recordid" => "type",
        "fields.identifiant_a" => "shortDescription",
        "fields.latlong.0" => "geo.latitude",
        "fields.latlong.1" => "geo.longitude",
        "fields.adresse" => "address.streetAddress",
    ];
}
