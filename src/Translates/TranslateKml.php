<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateKmlInterface;

class TranslateKml implements TranslateKmlInterface
{
    public static array $dataBinding_news = [
        "name" => [
            "valueOf" => '_id.$id',
        ],
        "description" =>
 [
     "type" => "description_kml",
     "text" => [
         "valueOf" => "text",
     ],
 ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "scope.cities.0.geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "scope.cities.0.geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_allOrganization = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_organization = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_allEvent = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_event = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_allProject = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_project = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_allPerson = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_person = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "img" => [
                "valueOf" => "profilImageUrl",
                "type" => "url",
                "prefix" => "/",
            ],
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    public static array $dataBinding_city = [
        "name" => [
            "valueOf" => 'name',
        ],
        "description" => [
            "valueOf" => "structure",
        ],
        "Point" => [
            "type" => "coor",
            "coordinates" =>
         [
             "latitude" => [
                 "valueOf" => "geo.latitude",
             ],
             "longitude" => [
                 "valueOf" => "geo.longitude",
             ],
         ],
        ],
    ];

    // TODO : $bindPath is not used
    /**
     * Retrieves the KML coordinates.
     *
     * @param array $val The input array.
     * @param array|null $bindPath The optional bind path array.
     * @return array|null The KML coordinates array or null if not found.
     */
    public function getKmlCoor(array $val, ?array $bindPath = null): ?array
    {
        if ((isset($val["coordinates"]["latitude"])) && (isset($val["coordinates"]["longitude"]))) {
            $latitude = (string) $val["coordinates"]["latitude"];
            $longitude = (string) $val["coordinates"]["longitude"];

            $coor = $longitude . ',' . $latitude;
            $val['coordinates'] = $coor;

            unset($val["type"]);
            return $val;
        }
        return null;
    }

    /**
     * Formats the given value based on its type.
     *
     * @param array $val The value to be formatted.
     * @param array|null $bindPath The optional bind path.
     * @return string The formatted value.
     */
    public function specFormatByType(array $val, ?array $bindPath = null): string
    {
        return ! empty($val["text"]) ? (string) $val["text"] : "Pas de description pour cette news";
    }
}
