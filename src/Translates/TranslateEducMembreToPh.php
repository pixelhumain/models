<?php

namespace PixelHumain\Models\Translates;

use PixelHumain\Models\Translates\Interfaces\TranslateEducMembreToPhInterface;

class TranslateEducMembreToPh implements TranslateEducMembreToPhInterface
{
    public static array $mapping = [
        "fields.nom" => "name",
        "fields.geolocalisation.0" => "geo.latitude",
        "fields.geolocalisation.1" => "geo.longitude",
        "fields.site_web" => "url",
        "fields.sexe" => "tags.0",
        "fields.etablissement" => "tas.1",
        "fields.secteur_disciplinaire" => "tags.2",
    ];
}
