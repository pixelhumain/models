<?php

namespace PixelHumain\Models;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;

use PixelHumain\Models\Interfaces\SIGInterface;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;

use PixelHumain\Models\Traits\BaseModel\ParamsTrait;

use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\ImportTrait;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ImportTraitInterface;

class SIG extends BaseModel implements SIGInterface, ElementTraitInterface, ImportTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    use ElementTrait;
    use ImportTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Retrieves the address schema that matches the given INSEE code.
     *
     * @param string $codeInsee The INSEE code.
     * @param string|null $postalCode The postal code (optional).
     * @param string|null $cityName The city name (optional).
     * @return array The array of address schemas.
     */
    public function getAdressSchemaLikeByCodeInsee($codeInsee, ?string $postalCode = null, ?string $cityName = null): array
    {
        $city = $this->getCityByCodeInsee($codeInsee);

        $address = [
            "@type" => "PostalAddress",
            "codeInsee" => $city['insee'] ?? "",
            "addressCountry" => $city['country'] ?? "",
            'localityId' => (string) $city["_id"],
            'level1' => $city["level1"],
            'level1Name' => $city["level1Name"] ?? "",
            'level2' => $city["level2"] ?? "",
            'level2Name' => $city["level2Name"] ?? "",
            'level3' => $city["level3"] ?? "",
            'level3Name' => $city["level3Name"] ?? "",
            'level4' => $city["level4"] ?? "",
            'level4Name' => $city["level4Name"] ?? "",
            'postalCode' => "",
            'addressLocality' => "",
        ];

        if ($postalCode !== null) {
            foreach ($city["postalCodes"] as $data) {
                if ($data["postalCode"] === $postalCode && ($cityName === null || $data["name"] === $cityName)) {
                    $address["postalCode"] = $data["postalCode"];
                    $address["addressLocality"] = $data["name"];
                    break; // Quitte la boucle une fois la correspondance trouvée
                }
            }
        }

        return $address;
    }

    /**
     * Retrieves the city information based on the INSEE code.
     *
     * @param string $codeInsee The INSEE code of the city.
     * @return array The city information.
     * @throws InvalidArgumentException If the INSEE code is empty.
     * @throws CTKException If the city cannot be found.
     */
    public function getCityByCodeInsee(string $codeInsee): array
    {
        if (empty($codeInsee)) {
            throw new InvalidArgumentException("The Insee Code is mandatory");
        }

        $city = $this->db->findOne(CityInterface::COLLECTION, [
            "insee" => $codeInsee,
        ]);
        if (empty($city)) {
            throw new CTKException("Impossible to find the city with the insee code : " . $codeInsee);
        }

        return $city;
    }

    /**
     * Retrieves the geographical position by INSEE code.
     *
     * @param string $inseeCode The INSEE code of the city.
     * @param string|null $postalCode Optional postal code to filter the location.
     * @param string|null $cityName Optional city name to filter the location.
     * @return array The geographical coordinates.
     * @throws CTKException If the geographical position cannot be found.
     */
    public function getGeoPositionByInseeCode(string $inseeCode, ?string $postalCode = null, ?string $cityName = null): array
    {
        $city = $this->getCityByCodeInsee($inseeCode);

        foreach ($city["postalCodes"] as $data) {
            if ($data["postalCode"] == $postalCode && ($cityName === null || $data["name"] == $cityName)) {
                return [
                    "@type" => "GeoCoordinates",
                    "latitude" => $data["geo"]["latitude"],
                    "longitude" => $data["geo"]["longitude"],
                ];
            }
        }

        throw new CTKException("Impossible to find a postalCode for insee: " . $inseeCode . " and cp: " . $postalCode);
    }

    /**
     * Updates the geoposition of an entity.
     *
     * @param string $entityType The type of the entity.
     * @param string $entityId The ID of the entity.
     * @param string|null $latitude The latitude of the geoposition.
     * @param string|null $longitude The longitude of the geoposition.
     * @param mixed|null $addressIndex The index of the address.
     * @return void
     */
    public function updateEntityGeoposition(string $entityType, string $entityId, ?string $latitude, ?string $longitude, $addressIndex = null): void
    {
        // Validation des entrées
        if (empty($latitude) || empty($longitude)) {
            throw new InvalidArgumentException("Latitude and longitude are required.");
        }

        // Création des structures de données géographiques
        $geo = [
            "@type" => "GeoCoordinates",
            "latitude" => $latitude,
            "longitude" => $longitude,
        ];
        $geoPosition = [
            "type" => "Point",
            "coordinates" => [floatval($longitude), floatval($latitude)],
        ];

        // Ajout de addressIndex s'il n'est pas vide
        if (! empty($addressIndex)) {
            $geo["addressesIndex"] = $addressIndex;
            $geoPosition["addressesIndex"] = $addressIndex;
        }

        // Mise à jour de l'entité
        $types = [PersonInterface::COLLECTION, EventInterface::COLLECTION, OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION];
        if (in_array($entityType, $types)) {
            $this->getModelElement()->updateField($entityType, $entityId, "geo", $geo);
            $this->getModelElement()->updateField($entityType, $entityId, "geoPosition", $geoPosition);
        } else {
            throw new InvalidArgumentException("Invalid entity type: " . $entityType);
        }

        // Vérification des données incomplètes et alerte
        if ($this->getModelImport()->isUncomplete($entityId, $entityType)) {
            $this->getModelImport()->checkWarning($entityId, $entityType, $this->session->getUserId());
        }
    }
}
