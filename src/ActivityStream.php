<?php

namespace PixelHumain\Models;

use Exception;
use InvalidArgumentException;
use PixelHumain\Models\Interfaces\ActionRoomInterface;
use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CacheHelperInterface;

use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;

use PixelHumain\Models\Traits\ActivityStreamReferenceTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;

use PixelHumain\Models\Traits\Interfaces\ActivityStreamReferenceTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;
use PixelHumain\Models\Traits\ZoneTrait;

class ActivityStream extends BaseModel implements ActivityStreamInterface, ActivityStreamReferenceTraitInterface, PersonTraitInterface, ZoneTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ActivityStreamReferenceTrait;
    use PersonTrait;
    use ZoneTrait;
    use CostumTrait;

    protected ?CacheHelperInterface $cacheHelper = null;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Adds an entry to the activity stream.
     *
     * @param array $param The parameters for the entry.
     * @return void
     */
    public function addEntry(array $param): void
    {
        if ($param["type"] == ActivityStreamInterface::COLLECTION) {
            $news = $param;
            $costum = $this->getModelCostum()->getCostum();
            if (isset($costum) && isset($costum["slug"])) {
                $news["source"] = [
                    "origin" => "costum",
                    "key" => $costum["slug"],
                    "keys" => [$costum["slug"]],
                ];
            }
            $news["collection"] = NewsInterface::COLLECTION;
            $news = $this->db->insert(ActionRoomInterface::COLLECTION, $news);
            // return $news;
        } else {
            $param = $this->db->insert(ActionRoomInterface::COLLECTION, $param);
            $this->activityStreamReference->add($param);
        }
    }

    /**
     * Retrieves the activity stream records based on the specified parameters.
     *
     * @param array $params The parameters used to filter the activity stream records.
     * @return array The filtered activity stream records.
     */
    public function getWhere(array $params): array
    {
        return $this->db->find(ActivityStreamInterface::COLLECTION, $params, null);
    }

    /**
     * Retrieves notifications from the activity stream.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotifications(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array
    {
        return $this->db->findAndSortAndLimitAndIndex(ActivityStreamInterface::COLLECTION, $param, $sort, $indexStep, $indexMin);
    }

    /**
     * Retrieves notifications based on the specified parameters.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotificationsByStep(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array
    {
        return $this->db->findAndSortAndLimitAndIndex(ActivityStreamInterface::COLLECTION, $param, $sort, $indexStep, $indexMin);
    }

    /**
     * Retrieves notifications from the activity stream based on a time limit.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param array $sort The sorting criteria for the notifications. Default is sorting by "updated" in descending order.
     * @return array The array of notifications that match the specified criteria.
     */
    public function getNotificationsByTimeLimit(array $param, array $sort = [
        "updated" => -1,
    ]): array
    {
        return $this->db->findAndSort(ActivityStreamInterface::COLLECTION, $param, $sort);
    }

    /**
     * Counts the number of unseen notifications for a given user, element type, and element ID.
     *
     * @param string $userId The ID of the user.
     * @param string|null $elementType The type of the element (optional).
     * @param string|null $elementId The ID of the element (optional).
     * @return int The number of unseen notifications.
     */
    public function countUnseenNotifications(string $userId, ?string $elementType, ?string $elementId): int
    {
        if ($elementType != PersonInterface::COLLECTION) {
            $params = [
                '$and' =>
                  [
                      [
                          "notify.id." . $userId . ".isUnseen" => [
                              '$exists' => true,
                          ],
                          "verb" => [
                              '$ne' => ActStrInterface::VERB_ASK,
                          ],
                          'type' => [
                              '$ne' => "oceco",
                          ],
                      ],
                      [
                          '$or' => [
                              [
                                  "target.type" => $elementType,
                                  "target.id" => $elementId,
                              ],
                              [
                                  "target.parent.type" => $elementType,
                                  "target.parent.id" => $elementId,
                              ],
                          ],
                      ],
                  ],
            ];
        } else {
            $params = [
                "notify.id." . $userId . ".isUnseen" => [
                    '$exists' => true,
                ],
            ];
        }
        return $this->db->count(ActivityStreamInterface::COLLECTION, $params);
    }

    /**
     * Retrieves the activity stream for a given object ID.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting options for the query. Default is descending by timestamp.
     * @return array The activity stream for the object ID.
     */
    public function getActivtyForObjectId(array $param, array $sort = [
        "timestamp" => -1,
    ]): array
    {
        return $this->db->findAndSort(ActivityStreamInterface::COLLECTION, $param, $sort, 5);
    }

    /**
     * Returns the activity history for a given ID and type.
     *
     * @param string $id The ID of the citizen.
     * @param string $type The type of activity.
     * @return array The activity history.
     */
    public function activityHistory(string $id, string $type): array
    {
        $where = [
            "target.id" => $id,
            "target.type" => $type,
            "type" => ActStrInterface::TYPE_ACTIVITY_HISTORY,
        ];
        $sort = [
            "created" => -1,
        ];
        return $this->db->findAndSort(ActivityStreamInterface::COLLECTION, $where, $sort);
    }

    /**
     * Removes an activity history entry.
     *
     * @param string $id The ID of the activity history entry to remove.
     * @param string $type The type of the activity history entry to remove.
     * @return mixed The result of the removal.
     */
    public function removeActivityHistory(string $id, string $type)
    {
        $where = [
            "target.id" => $id,
            "target.objectType" => $type,
            "type" => ActStrInterface::TYPE_ACTIVITY_HISTORY,
        ];
        return $this->db->remove(ActivityStreamInterface::COLLECTION, $where);
    }

    /**
     * Removes an element from the activity stream.
     *
     * @param string $id The ID of the element to remove.
     * @param string $type The type of the element to remove.
     * @return array The updated activity stream.
     */
    public function removeElementActivityStream(string $id, string $type): array
    {
        $res = [
            "result" => true,
            "msg" => "All the activity stream of the element have been removed.",
        ];
        $where = [
            '$or' => [
                [
                    '$and' => [
                        [
                            "target.id" => $id,
                        ],
                        [
                            "target.objectType" => $type,
                        ],
                    ],
                ],
                [
                    '$and' => [
                        [
                            "object.id" => $id,
                        ],
                        [
                            "object.objectType" => $type,
                        ],
                    ],
                ],
            ],
        ];
        $res["count"] = $this->db->count(ActivityStreamInterface::COLLECTION, $where);
        $this->db->remove(ActivityStreamInterface::COLLECTION, $where);

        return $res;
    }

    /**
     * Saves the activity history.
     *
     * @param string $verb The verb of the activity.
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string|null $activityName The name of the activity (optional).
     * @param string|null $activityValue The value of the activity (optional).
     * @return void
     */
    public function saveActivityHistory(string $verb, string $targetId, string $targetType, ?string $activityName = null, ?string $activityValue = null): void
    {
        $buildArray = [
            "type" => ActStrInterface::TYPE_ACTIVITY_HISTORY,
            "verb" => $verb,
            "target" => [
                "id" => $targetId,
                "type" => $targetType,
            ],
            "author" => [
                "id" => $this->session->getUserId(),
                "name" => $this->session->getUserName(),
            ],
        ];

        if ($activityName != null) {
            $buildArray["label"] = $activityName;
        }
        if ($activityValue != null) {
            $buildArray["value"] = $activityValue;
        }
        if ($activityName == "geo" || $activityName == "geoPosition") {
            $buildArray["value"] = "geoposition";
        }

        $params = $this->buildEntry($buildArray);
        $this->addEntry($params);
    }

    /**
     * Removes notifications for a specific ID.
     *
     * @param string $id The ID of the notification to be removed.
     * @return array An array containing the removed notifications.
     */
    public function removeNotifications(string $id): array
    {
        $notif = $this->db->findOne(ActivityStreamInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
        $res = [
            "result" => false,
            "msg" => "Something went wrong : Activty Stream Not Found",
            "id" => $id,
        ];
        if (isset($notif) && isset($notif["notify"]) && isset($notif["notify"]["id"])) {
            if ((is_countable($notif["notify"]["id"]) ? count($notif["notify"]["id"]) : 0) > 1) {
                $this->db->update(
                    ActivityStreamInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($id),
                    ],
                    [
                        '$unset' => [
                            "notify.id." . $this->session->getUserId() => true,
                        ],
                    ]
                );
            } else {
                $this->db->remove(
                    ActivityStreamInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($id),
                    ]
                );
            }

            $this->activityStreamReference->removeByNotificationId($id);

            try {
                $res = [
                    "result" => true,
                    "msg" => "Removed succesfully",
                ];
            } catch (Exception $e) {
                $res = [
                    "result" => false,
                    "msg" => "Something went wrong :" . $e->getMessage(),
                ];
            }
        }

        return $res;
    }

    /**
     * Update the notification by its ID.
     *
     * @param string $id The ID of the notification.
     * @param string $action The action to be performed on the notification.
     * @return array The updated notification.
     */
    public function updateNotificationById(string $id, string $action): array
    {
        try {
            $this->db->update(
                ActivityStreamInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                [
                    '$unset' => [
                        "notify.id." . $this->session->getUserId() . "." . $action => true,
                    ],
                ]
            );

            $this->activityStreamReference->updateStateByNotificationId($id, $action, false);

            $res = [
                "result" => true,
                "msg" => "Updated succesfully",
            ];
        } catch (Exception $e) {
            $res = [
                "result" => false,
                "msg" => "Something went wrong :" . $e->getMessage(),
            ];
        }

        return $res;
    }

    /**
     * Updates the notifications for a user based on the specified action.
     *
     * @param string $action The action to update the notifications for.
     * @return array The updated notifications.
     */
    public function updateNotificationsByUser(string $action): array
    {
        try {
            $this->db->updateWithOptions(
                ActivityStreamInterface::COLLECTION,
                [
                    "notify.id." . $this->session->getUserId() . "." . $action => [
                        '$exists' => true,
                    ],
                ],
                [
                    '$unset' => [
                        "notify.id." . $this->session->getUserId() . "." . $action => true,
                    ],
                ],
                [
                    "multiple" => true,
                ]
            );

            $this->activityStreamReference->updateStateByUser($action, false);

            $res = [
                "result" => true,
                "msg" => "Updated succesfully",
            ];
        } catch (Exception $e) {
            $res = [
                "result" => false,
                "msg" => "Something went wrong :" . $e->getMessage(),
            ];
        }

        return $res;
    }

    /**
     * Removes notifications for a specific user.
     *
     * @param string $userId The ID of the user.
     * @return array The removed notifications.
     */
    public function removeNotificationsByUser(string $userId): array
    {
        try {
            $this->db->updateWithOptions(
                ActivityStreamInterface::COLLECTION,
                [
                    "notify.id." . $userId => [
                        '$exists' => true,
                    ],
                ],
                [
                    '$unset' => [
                        "notify.id." . $userId => true,
                    ],
                ],
                [
                    "multiple" => true,
                ]
            );

            $this->activityStreamReference->removeByUserId($userId);
            $res = [
                "result" => true,
                "msg" => "Removed succesfully",
            ];
        } catch (Exception $e) {
            $res = [
                "result" => false,
                "msg" => "Something went wrong :" . $e->getMessage(),
            ];
        }

        return $res;
    }

    /**
     * Adds a notification to the activity stream.
     *
     * @param array $params The parameters for the notification.
     * @return array The updated activity stream.
     */
    public function addNotification(array $params): array
    {
        $objectType = "persons";
        if (@$params["objectType"]) {
            $objectType = $params["objectType"];
        }
        $notify = [
            "objectType" => $objectType,
            "id" => $params["persons"],
            "displayName" => $params["label"],
            "icon" => $params["icon"],
            "url" => $params["url"],
        ];
        if (@$params["labelAuthorObject"]) {
            $notify["labelAuthorObject"] = $params["labelAuthorObject"];
        }
        if (@$params["mentions"]) {
            $notify["mentions"] = $params["mentions"];
        }
        if (@$params["labelArray"]) {
            $notify["labelArray"] = $params["labelArray"];
        }
        if (@$params["type"]) {
            $notify["type"] = $params["type"];
        }
        if (isset($params["iconColor"])) {
            $notify["iconColor"] = $params["iconColor"];
        }

        return $notify;
    }

    // todo : elle est utilisé que dans une action RemoveMemberAction qui n'a pas l'air d'être utilisé
    /**
     * Removes an object from the activity stream.
     *
     * @param string $objectId The ID of the object to be removed.
     * @param string $type The type of the object.
     * @param string|null $targetId The ID of the target object (optional).
     * @param string|null $targetType The type of the target object (optional).
     * @param string|null $verb The verb associated with the action (optional).
     * @return mixed The result of the removal.
     */
    public function removeObject(string $objectId, string $type, ?string $targetId = null, ?string $targetType = null, ?string $verb = null)
    {
        $where = [
            "object.id" => $objectId,
            "object.objectType" => $type,
        ];
        if (@$targetId && $targetId != null) {
            $where["target.id"] = $targetId;
        }
        if (@$targetType && $targetType != null) {
            $where["target.objectType"] = $targetType;
        }
        if (@$verb && $verb != null) {
            $where["verb"] = $verb;
        }
        $res = $this->db->remove(ActivityStreamInterface::COLLECTION, $where);
        return $res;
    }

    /**
     * Builds an entry for the activity stream.
     *
     * @param array $params The parameters for building the entry.
     * @return array The built entry.
     */
    public function buildEntry(array $params): array
    {
        $localityId = null;
        $geo = null;
        $action = [
            "type" => $params["type"],
            "verb" => $params["verb"],
            "author" => $this->session->getUserId(),
            "updated" => $this->db->MongoDate(time()),
            "created" => $this->db->MongoDate(time()),
        ];

        if (@$params["object"]) {
            $action["object"] = $params["object"];
        }

        if (isset($params["target"])) {
            $action["target"] = [
                "type" => $params["target"]['type'],
                "id" => $params["target"]['id'],
            ];
            $action["sharedBy"] = [[
                "type" => $params["target"]['type'],
                "id" => $params["target"]['id'],
                "updated" => $this->db->MongoDate(time()),
            ]];
        }

        if (isset($params["ip"])) {
            $action["author"]["ip"] = $params["ip"];
        }

        if ($params["type"] == ActivityStreamInterface::COLLECTION && ! empty($params["address"])) {
            $action["scope"]["type"] = "public";
            $address = null;
            if (! empty($params["address"])) {
                $localityId = $params["address"]["localityId"];
                $address = $params["address"];
            }

            if (isset($params["geo"])) {
                $geo = $params["geo"];
            }

            if (! @$localityId) {
                $author = $this->person->getSimpleUserById($this->session->getUserId());

                if (@$author["address"] && @$author["address"]["localityId"]) {
                    $localityId = $author["address"]["localityId"];
                    $address = $author["address"];
                    if (! @$geo) {
                        $geo = $author["geo"];
                    }
                }
            }

            $scope = [
                "parentId" => $localityId,
                "parentType" => CityInterface::COLLECTION,
                "name" => $address["addressLocality"],
                "geo" => $geo,
            ];
            if (! (empty($address["postalCode"]))) {
                $scope["postalCode"] = $address["postalCode"];
            }

            $scope = array_merge($scope, $this->zone->getLevelIdById($localityId, $address, CityInterface::COLLECTION));

            $action["scope"]["localities"][] = $scope;
        } else {
            $action["scope"]["type"] = NewsInterface::TYPE_RESTRICTED;
        }

        if (isset($params["label"])) {
            $action["object"]["displayName"] = $params["label"];
        }

        if (isset($params["value"])) {
            $action["object"]["displayValue"] = ((isset($params["label"]) && $params["label"] = "address") ? $params["value"] : preg_replace('/<[^>]*>/', '', $params["value"]));
        }
        if (isset($params["author"])) {
            $action["author"] = $params["author"];
        }

        if (isset($params["tags"])) {
            $action["tags"] = $params["tags"];
        }

        return $action;
    }

    /**
     * Saves the activity stream without sending an email notification.
     *
     * @param string $mail The email address associated with the activity stream.
     * @return array An array containing the saved activity stream data.
     */
    public function saveNotSendMail(string $mail): array
    {
        $params = [
            "target" => [
                "email" => $mail,
            ],
            "date" => time(),
            "summary" => "You will no longer receive any emails from the platform",
            "type" => ActStrInterface::TYPE_ACTIVITY_HISTORY,
            "verb" => ActStrInterface::VERB_NOSENDING,
            "updated" => $this->db->MongoDate(time()),
            "created" => $this->db->MongoDate(time()),
        ];
        $this->addEntry($params);
        return $params;
    }
}
