<?php

namespace PixelHumain\Models;

use CTKException;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;

use PixelHumain\Models\Traits\BaseModel\DataHandlerTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;

use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;

use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\PersonTrait;

class Organization extends BaseModel implements OrganizationInterface, DataValidatorTraitInterface, LinkTraitInterface, DocumentTraitInterface, PersonTraitInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use I18NTrait;
    use DbTrait;

    use DataHandlerTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DataValidatorTrait;
    use LinkTrait;
    use DocumentTrait;
    use PersonTrait;
    use ElementTrait;

    public array $types = [
        //"NGO" => $this->language->t("common","NGO"),
        "NGO" => "NGO",
        "LocalBusiness" => "Local Business",
        "Group" => "Group",
        "GovernmentOrganization" => "Government Organization",
        "Cooperative" => "Cooperative",
    ];

    //From Post/Form name to database field name
    //TODO : remove name
    public static array $dataBinding = [
        "name" => [
            "name" => "name",
            "rules" => ["required", "organizationSameName"],
        ],
        "slug" => [
            "name" => "slug",
            "rules" => ["checkSlug"],
        ],
        "email" => [
            "name" => "email",
            "rules" => ["email"],
        ],
        "collection" => [
            "name" => "collection",
        ],
        "referantName" => [
            "name" => "referantName",
        ],
        "type" => [
            "name" => "type",
            "rules" => ["required", "typeOrganization"],
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "description" => [
            "name" => "description",
        ],
        "openingHours" => [
            "name" => "openingHours",
        ],
        "category" => [
            "name" => "category",
        ],
        "address" => [
            "name" => "address",
            "rules" => ["addressValid"],
        ],
        "sourceType" => [
            "name" => "source.type",
        ],
        "source" => [
            "name" => "source",
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "streetAddress" => [
            "name" => "address.streetAddress",
        ],
        "postalCode" => [
            "name" => "address.postalCode",
        ],
        "city" => [
            "name" => "address.codeInsee",
        ],
        "addressLocality" => [
            "name" => "address.addressLocality",
        ],
        "addressCountry" => [
            "name" => "address.addressCountry",
        ],
        "geo" => [
            "name" => "geo",
            "rules" => ["geoValid"],
        ],
        "geoPosition" => [
            "name" => "geoPosition",
            "rules" => ["geoPositionValid"],
        ],
        "tags" => [
            "name" => "tags",
        ],
        "typeIntervention" => [
            "name" => "typeIntervention",
        ],
        "typeOfPublic" => [
            "name" => "typeOfPublic",
        ],
        "url" => [
            "name" => "url",
        ],
        "telephone" => [
            "name" => "telephone",
        ],
        "mobile" => [
            "name" => "telephone.mobile",
        ],
        "fixe" => [
            "name" => "telephone.fixe",
        ],
        "fax" => [
            "name" => "telephone.fax",
        ],
        "modules" => [
            "name" => "modules",
        ],
        "preferences" => [
            "name" => "preferences",
        ],
        "video" => [
            "name" => "video",
        ],
        "state" => [
            "name" => "state",
        ],
        "warnings" => [
            "name" => "warnings",
        ],
        "urlFacebook" => [
            "name" => "urlFacebook",
        ],
        "urlTwitter" => [
            "name" => "urlTwitter",
        ],
        "urlWiki" => [
            "name" => "urlWiki",
        ],
        "isOpenData" => [
            "name" => "isOpenData",
        ],
        "badges" => [
            "name" => "badges",
        ],
        // TODO :  verifier quel source prendre je dirai que c'est le deuxieme dans le tableau d'utilisé
        "source" => [
            "name" => "source",
            "rules" => ["source"],
        ],
        "role" => [
            "name" => "role",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "locality" => [
            "name" => "address",
        ],
        "contacts" => [
            "name" => "contacts",
        ],
        "descriptionHTML" => [
            "name" => "descriptionHTML",
        ],
        "socialNetwork" => [
            "name" => "socialNetwork",
        ],
        "facebook" => [
            "name" => "socialNetwork.facebook",
        ],
        "twitter" => [
            "name" => "socialNetwork.twitter",
        ],
        "gpplus" => [
            "name" => "socialNetwork.googleplus",
        ],
        "github" => [
            "name" => "socialNetwork.github",
        ],
        "gitlab" => [
            "name" => "socialNetwork.gitlab",
        ],
        "instagram" => [
            "name" => "socialNetwork.instagram",
        ],
        "diaspora" => [
            "name" => "socialNetwork.diaspora",
        ],
        "mastodon" => [
            "name" => "socialNetwork.mastodon",
        ],
        "signal" => [
            "name" => "socialNetwork.signal",
        ],
        "telegram" => [
            "name" => "socialNetwork.telegram",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "onepageEdition" => [
            "name" => "onepageEdition",
        ],
        "urlImg" => [
            "name" => "urlImg",
        ],
        "scope" => [
            "name" => "scope",
        ],
        "actionPrincipal" => [
            "name" => "actionPrincipal",
        ],
        "admins" => [
            "name" => "admins",
        ],
        "categoryNA" => [
            "name" => "categoryNA",
        ],
        "typeStruct" => [
            "name" => "typeStruct",
        ],
        "activityPlace" => [
            "name" => "activityPlace",
        ],
        "extraInfo" => [
            "name" => "extraInfo",
        ],
        "timetables" => [
            "name" => "timetables",
        ],
        "certificationsAwards" => [
            "name" => "certificationsAwards",
        ],
        "thematic" => [
            "name" => "thematic",
        ],
        "template" => [
            "name" => "template",
        ],
        "jobFamily" => [
            "name" => "jobFamily",
        ],
        "typeFinancing" => [
            "name" => "typeFinancing",
        ],
        "modality" => [
            "name" => "modality",
        ],
        "legalStatus" => [
            "name" => "legalStatus",
        ],
        "responsable" => [
            "name" => "responsable",
        ],
        "link" => [
            "name" => "link",
        ],
        "objective" => [
            "name" => "objective",
        ],
        "linkFinancialDevice" => [
            "name" => "linkFinancialDevice",
        ],
        "financialPartners" => [
            "name" => "financialPartners",
        ],
        "maximumAmount" => [
            "name" => "maximumAmount",
        ],
        "publicCible" => [
            "name" => "publicCible",
        ],
        "strongPoints" => [
            "name" => "strongPoints",
        ],
        "weaknesses" => [
            "name" => "weaknesses",
        ],
        "conditionsEligibility" => [
            "name" => "conditionsEligibility",
        ],
        "objectiveOdd" => [
            "name" => "objectiveOdd",
        ],
        "toBeValidated" => [
            "name" => "toBeValidated",
        ],
        "serviceOffers" => [
            "name" => "serviceOffers",
        ],
        "areaOfIntervention" => [
            "name" => "areaOfIntervention",
        ],
        "namesStartupers" => [
            "name" => "namesStartupers",
        ],
        "yearEntryInpri" => [
            "name" => "yearEntryInpri",
        ],
        "status" => [
            "name" => "status",
        ],
        "statusActor" => [
            "name" => "statusActor",
        ],
        "otherLocality" => [
            "name" => "otherLocality",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    /**
     * Retrieves organizations based on the specified parameters.
     *
     * @param array $params The parameters used to filter the organizations.
     * @return array The array of organizations matching the specified parameters.
     */
    public function getWhere(array $params): array
    {
        return $this->db->find(OrganizationInterface::COLLECTION, $params);
    }

    /**
     * Returns the data binding for the Organization model.
     *
     * @return array The data binding array.
     */
    public function getDataBinding(): array
    {
        return self::$dataBinding;
    }

    /** TO REFACTOR FOR IMPORT TRANSLATION COMMON FOR ALL ENTRIES
    * _function_ @TranslateType is used in import process
    * Perhaps impose a standard or genaralized this process for correlation by importer
    * Ex : A file with 1000 entries where there used a specific nomenclature for instance here for (string)type
    * Importer user has to fullfil a list of correlation acording to our documentation API
    * _Array_ $correlateCsv=array(type=> array(
    *					self::TYPE_NGO => ["Association", "association"],
                        self::TYPE_BUSINESS => ["Entreprise", "CEQUON VEUT"],
                        ...
                    )
    *			);
    */

    /**
     * Translates the given type into a string representation.
     *
     * @param string $type The type to be translated.
     * @return string The translated type as a string.
     */
    public function translateType(string $type): string
    {
        if (trim($type) == "Association" || trim($type) == "association") {
            $type = OrganizationInterface::TYPE_NGO;
        } elseif (trim($type) == "Groupe Gouvernemental" || trim($type) == "Groupe gouvernemental" || trim($type) == "Organisation gouvernementale" || trim($type) == "Structure publique" || trim($type) == "Structure Publique" || trim($type) == "Service public") {
            $type = OrganizationInterface::TYPE_GOV;
        } elseif (trim($type) == "Entreprise") {
            $type = OrganizationInterface::TYPE_BUSINESS;
        } elseif (trim($type) == "Cooperative") {
            $type = OrganizationInterface::TYPE_COOP;
        } elseif (trim($type) == "Groupe" || trim($type) == "Groupe informel") {
            $type = OrganizationInterface::TYPE_GROUP;
        }
        return $type;
    }

    /**
     * Executes after saving an organization.
     *
     * @param array $organization The organization data.
     * @param string $creatorId The ID of the creator.
     * @param array|null $paramsImport Additional import parameters (optional).
     * @return array The modified organization data.
     */
    public function afterSave(array $organization, string $creatorId, ?array $paramsImport = null): array
    {
        $newOrganizationId = (string) $organization['_id'];
        if (@$paramsImport) {
            if (! empty($paramsImport["link"])) {
                $idLink = $paramsImport["link"]["idLink"];
                $typeLink = $paramsImport["link"]["typeLink"];
                if (@$paramsImport["link"]["role"] == "admin") {
                    $isAdmin = true;
                } else {
                    $isAdmin = false;
                }

                if ($typeLink == OrganizationInterface::COLLECTION) {
                    $this->getModelLink()->connect($idLink, $typeLink, $newOrganizationId, OrganizationInterface::COLLECTION, $creatorId, "members", false);
                    $this->getModelLink()->connect($newOrganizationId, OrganizationInterface::COLLECTION, $idLink, $typeLink, $creatorId, "memberOf", false);
                } elseif ($typeLink == PersonInterface::COLLECTION) {
                    $this->getModelLink()->connect($newOrganizationId, OrganizationInterface::COLLECTION, $idLink, PersonInterface::COLLECTION, $creatorId, "members", $isAdmin);
                    $this->getModelLink()->connect($idLink, $typeLink, $newOrganizationId, OrganizationInterface::COLLECTION, $creatorId, "memberOf", $isAdmin);
                }
            }

            if (! empty($paramsImport["img"])) {
                try {
                    $paramsImg = $paramsImport["img"];
                    $resUpload = $this->getModelDocument()->uploadDocumentFromURL(
                        $paramsImg["module"],
                        OrganizationInterface::COLLECTION,
                        $newOrganizationId,
                        "avatar",
                        false,
                        $paramsImg["url"],
                        $paramsImg["name"]
                    );

                    if (! empty($resUpload["result"]) && $resUpload["result"] == true) {
                        $params = [];
                        $params['id'] = $newOrganizationId;
                        $params['type'] = OrganizationInterface::COLLECTION;
                        $params['moduleId'] = $paramsImg["module"];
                        $params['folder'] = OrganizationInterface::COLLECTION . "/" . $newOrganizationId;
                        $params['name'] = $resUpload['name'];
                        $params['author'] = $this->session->getUserId();
                        $params['size'] = $resUpload["size"];
                        $params["contentKey"] = "profil";
                        $params["doctype"] = DocumentInterface::DOC_TYPE_IMAGE;
                        $resImgSave = $this->getModelDocument()->save($params);
                        if ($resImgSave["result"] == false) {
                            throw new CTKException("Impossible de sauvegarder l'image.");
                        }
                    } else {
                        throw new CTKException("Impossible uploader l'image.");
                    }
                } catch (CTKException $e) {
                    throw new CTKException($e);
                }
            }
        }
        if (! empty($organization["parentType"]) &&
            ! empty($organization["parentId"]) &&
            ! empty($organization["source"]) &&
            ! empty($organization["source"]["insertOrign"]) &&
            $organization["source"]["insertOrign"] == "network") {
            $child = [];
            $child[] = [
                "childId" => $newOrganizationId,
                "childType" => OrganizationInterface::COLLECTION,
                "childName" => $organization["name"],
                "roles" => [],
            ];
            $this->getModelLink()->multiconnect($child, $organization["parentId"], $organization["parentType"]);
        }

        $organization = $this->getById($newOrganizationId);
        return [
            "result" => true,
            "msg" => "Votre organisation est communectée.",
            "id" => $newOrganizationId,
            "organization" => $organization,
        ];
    }

    /**
     * Retrieves an organization by its ID.
     *
     * @param string $id The ID of the organization.
     * @return array The organization data.
     */
    public function getById(string $id): array
    {
        $organization = $this->getDataById($id);

        $organization = $organization ?: $this->getModelElement()->getGhost(OrganizationInterface::COLLECTION);

        return $organization;
    }

    /**
     * Retrieves an organization by an array of IDs.
     *
     * @param array $arrayId The array of organization IDs.
     * @param array $fields The optional array of fields to retrieve.
     * @param bool $simply Whether to simplify the result or not.
     * @return array The retrieved organization(s).
     */
    public function getByArrayId(array $arrayId, array $fields = [], bool $simply = false): array
    {
        return $this->getDataByArrayId($arrayId, $fields, $simply);
    }

    /**
     * Retrieves a simple organization by its ID.
     *
     * @param string $id The ID of the organization.
     * @param array|null $orga Optional organization data.
     * @return array The simple organization data.
     */
    public function getSimpleOrganizationById(string $id, ?array $orga = null): array
    {
        return $this->getSimpleDataById($id, $orga);
    }

    /**
     * Get simple data for a specific organization.
     *
     * @param int $id The ID of the organization.
     * @param array $data Additional data for the organization.
     * @return array The simple data for the organization.
     */
    protected function getDataSimple($id, array $data): array
    {
        $simpleOrganization = [
            "id" => $id,
            "category" => $data["category"] ?? null,
            "sourceType" => $data["source"]["type"] ?? null,
            "name" => $data["name"] ?? null,
            "type" => $data["type"] ?? null,
            "email" => $data["email"] ?? null,
            "url" => $data["url"] ?? null,
            "telephone" => $data["telephone"] ?? null,
            "pending" => $data["pending"] ?? null,
            "tags" => $data["tags"] ?? null,
            "geo" => $data["geo"] ?? null,
            "shortDescription" => $data["shortDescription"] ?? null,
            "description" => $data["description"] ?? null,
            "updated" => $data["updated"] ?? null,
            "addresses" => $data["addresses"] ?? null,
            "slug" => $data["slug"] ?? null,
            "scope" => $data["scope"] ?? null,
            "typeSig" => "organizations",
        ];

        $logo = $this->getModelDocument()->getLastImageByKey($id, OrganizationInterface::COLLECTION, DocumentInterface::IMG_LOGO);
        $simpleOrganization["logoImageUrl"] = $logo;

        $simpleOrganization["address"] = empty($data["address"]) ? [
            "addressLocality" => $this->language->t("common", "Unknown Locality"),
        ] : $data["address"];

        return $simpleOrganization;
    }

    /**
     * Returns the fields of the project.
     *
     * @return array The fields of the project.
     */
    protected function getDataFields(): array
    {
        return [
            "id" => 1,
            "category" => 1,
            "name" => 1,
            "type" => 1,
            "email" => 1,
            "url" => 1,
            "shortDescription" => 1,
            "description" => 1,
            "address" => 1,
            "pending" => 1,
            "tags" => 1,
            "links" => 1,
            "geo" => 1,
            "updated" => 1,
            "profilImageUrl" => 1,
            "profilThumbImageUrl" => 1,
            "profilMarkerImageUrl" => 1,
            "profilMediumImageUrl" => 1,
            "addresses" => 1,
            "telephone" => 1,
            "slug" => 1,
            "scope" => 1,
            "sourceType" => 1,
        ];
    }

    /**
     * Returns an array of link types.
     *
     * @return array The link types.
     */
    protected function getlinkTypes(): array
    {
        return ["members", "followers"];
    }

    /**
     * Format the enrich data.
     *
     * @param array $data The data to be formatted.
     * @return array The formatted data.
     */
    protected function formatEnrichData(array $data): array
    {
        return $data;
    }
}
