<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ActionInterface;
use PixelHumain\Models\Interfaces\ActionsInterface;
use PixelHumain\Models\Interfaces\AnswerInterface;
use PixelHumain\Models\Interfaces\CommentInterface;
use PixelHumain\Models\Interfaces\CronInterface;
use PixelHumain\Models\Interfaces\FormInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;

use PixelHumain\Models\Interfaces\RoomInterface;
use PixelHumain\Models\Traits\ActionRoomTrait;
use PixelHumain\Models\Traits\ApiTrait;

use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\FormTrait;
use PixelHumain\Models\Traits\ImportTrait;
use PixelHumain\Models\Traits\Interfaces\ActionRoomTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ApiTraitInterface;
use PixelHumain\Models\Traits\Interfaces\AuthorisationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\FormTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ImportTraitInterface;
use PixelHumain\Models\Traits\Interfaces\LinkTraitInterface;
use PixelHumain\Models\Traits\Interfaces\MailTraitInterface;

use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SlugTraitInterface;
use PixelHumain\Models\Traits\LinkTrait;
use PixelHumain\Models\Traits\MailTrait;
use PixelHumain\Models\Traits\SearchNewTrait;

use PixelHumain\Models\Traits\SlugTrait;

class Answer extends BaseModel implements AnswerInterface, CostumTraitInterface, SlugTraitInterface, DocumentTraitInterface, FormTraitInterface, AuthorisationTraitInterface, SearchNewTraitInterface, ElementTraitInterface, MailTraitInterface, LinkTraitInterface, ImportTraitInterface, ActionRoomTraitInterface, ApiTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use SlugTrait;
    use DocumentTrait;
    use FormTrait;
    use AuthorisationTrait;
    use SearchNewTrait;
    use ElementTrait;
    use MailTrait;
    use LinkTrait;
    use ImportTrait;
    use ActionRoomTrait;
    use ApiTrait;

    public array $mailConfig = [
        "validation" => [
            "tpl" => "answer.validation",
            "subject" => "La candidature {what} a été validée",
        ],
        "rejected" => [
            "tpl" => "answer.rejected",
            "subject" => "La candidature {what} a été rejetée",
        ],
        "default" => [
            "tpl" => "answer.default",
            "subject" => "",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Retrieves an answer by its ID.
     *
     * @param string $id The ID of the answer.
     * @param array $fields The optional fields to retrieve.
     * @return array|null The answer data as an associative array, or null if not found.
     */
    public function getById(string $id, array $fields = []): ?array
    {
        return $this->db->findOne(AnswerInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ], $fields);
    }

    // TODO : verifier le retour
    /**
     * Generates an answer based on the given form.
     *
     * @param array $form The form data.
     * @param bool $draft Indicates if the answer is a draft (optional, default: false).
     * @param array|null $context The context for generating the answer (optional).
     * @return array The generated answer.
     */
    public function generateAnswer(array $form, bool $draft = false, ?array $context = null): array
    {
        $costum = $this->getModelCostum()->getCostum();
        $ans = [
            "collection" => AnswerInterface::COLLECTION,
            "user" => $this->session->getUserId(),
            "links" => [
                "answered" => [$this->session->getUserId()],
                //proposed,particpated,decision,financed,financed,referenced,commented,worked,payed,tested,validated
                //each actor is connectable to his activitystream
            ],
            "created" => time(),
            "cterSlug" => $this->getModelSlug()->getSlugByIdAndCol(array_keys($form["parent"])[0], $form["parent"][array_keys($form["parent"])[0]]["type"]),
        ];

        $ans["context"] = $form["parent"];
        if (! empty($context)) {
            foreach ($context as $keyC => $valC) {
                if (empty($ans["context"][$keyC])) {
                    $ans["context"][$keyC] = $valC;
                }
            }
        }
        if (isset($form["type"]) && $form["type"] == "aap") {
            $ans["acceptation"] = "pending";
        }

        if ($draft === true) {
            $ans["draft"] = true;
        }

        $formList = null;
        if (! empty($form)) {
            $ans["form"] = (string) $form["_id"];
            if (! empty($form["subForms"])) {
                $formList = $form["subForms"];
                if (! is_array($formList)) {
                    $ans["formId"] = implode("|", $formList);
                } else {
                    $ans["formId"] = array_keys($formList);
                }

                $ans["formList"] = is_countable($formList) ? count($formList) : 0;
            }
        }

        if (! empty($costum) && ! empty($costum["slug"])) {
            $ans["source"] = $this->getModelCostum()->getSource($costum["slug"]);
        }

        if ($this->getModelCostum()->isSameFunction("generateAnswerBeforeSave")) {
            $paramBeforeSave = $this->getModelCostum()->sameFunction(
                "generateAnswerBeforeSave",
                [
                    "answer" => $ans,
                    "formList" => $formList,
                ]
            );
            $ans = $paramBeforeSave["answer"];
        }

        if (! empty($ans) && ! empty($ans["form"])) {
            $parentForm = $this->db->findOneById(FormInterface::COLLECTION, $ans["form"]);
            if (isset($parentForm["params"]["budgetdepense"]["prefilledDepense"])) {
                $depense = [];
                foreach ($parentForm["params"]["budgetdepense"]["prefilledDepense"] as $prefid => $pref) {
                    array_push($depense, [
                        "poste" => $pref,
                    ]);
                }
                $ans["answers"]["aapStep1"]["depense"] = $depense;
            }

            if (isset($parentForm["params"]["budgetbudget"]["prefilledDepense"])) {
                $budget = [];
                foreach ($parentForm["params"]["budgetbudget"]["prefilledDepense"] as $prefid => $pref) {
                    array_push($budget, [
                        "poste" => $pref,
                    ]);
                }
                $ans["answers"]["aapStep1"]["budget"] = $budget;
            }
        }

        $ans = $this->db->insert(AnswerInterface::COLLECTION, $ans);
        $answer = $this->db->findOne(AnswerInterface::COLLECTION, [
            "_id" => $this->db->MongoId($ans["_id"]),
        ]);
        return $answer;
    }

    // TODO : changer le retour pour l'erreur pour que ça renvois un array
    /**
     * Deletes an answer by its ID.
     *
     * @param string $id The ID of the answer to delete.
     * @return array The result of the deletion operation.
     */
    public function delete(string $id): array
    {
        try {
            $this->getModelDocument()->removeAllForElement($id, AnswerInterface::COLLECTION);
            $this->db->remove(
                AnswerInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($id),
                ]
            );
            return [
                "result" => true,
                "msg" => $this->language->t("common", "The answer has been well deleted"),
            ];
        } catch (CTKException $e) {
            return [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
    }

    // @RemoveLinks permet d'enlever les liens dans les autres collections liés à cette réponse
    // Cette fonction peut être surchargée dans les models des costums
    /**
     * Removes links from the given answer.
     *
     * @param array $answer The answer to remove links from.
     * @return bool Returns true if the links were successfully removed, false otherwise.
     */
    public function removeLinks(array $answer): bool
    {
        if ($this->getModelCostum()->isSameFunction("removeLinksAnswer", [
            "answer" => $answer,
        ])) {
            $this->getModelCostum()->sameFunction("removeLinksAnswer", [
                "answer" => $answer,
            ]);
        }
        return true;
    }

    /**
     * Check if the user can administer the answer.
     *
     * @param array $ans The answer data.
     * @param array|null $form The form data (optional).
     * @return bool Returns true if the user can administer the answer, false otherwise.
     */
    public function canAdmin(array $ans, ?array $form = null): bool
    {
        if (! empty($form) && $this->getModelForm()->canAdmin((string) $form["_id"], $form)) {
            return true;
        } elseif ($this->getModelCostum()->isSameFunction("canAdminAnswer", [
            "answer" => $ans,
        ])) {
            return $this->getModelCostum()->sameFunction("canAdminAnswer", [
                "answer" => $ans,
            ]);
        }

        return false;
    }

    // TODO :  pourquoi on utilise ctype_xdigit ? remplacer par is_string
    // TODO : $form not used
    // TODO : $userId not used
    // TODO : $parent not used
    /**
     * Check if the answer can be edited.
     *
     * @param string|array $ans The answer ID or data.
     * @param array|null $form The form data (optional).
     * @param string|null $userId The user ID (optional).
     * @param mixed $parent The parent object (optional).
     * @return bool Returns true if the answer can be edited, false otherwise.
     */
    public function canEdit($ans, ?array $form = null, ?string $userId = null, ?array $parent = null): bool
    {
        if ($this->getModelAuthorisation()->isInterfaceAdmin()) {
            return true;
        }
        if (is_string($ans)) {
            $ans = $this->getById($ans);
        }
        // Si auteur de la réponse
        if ($this->session->getUserId() && $ans["user"] == $this->session->getUserId()) {
            return true;
        }
        // Si administrateur du context via l'attr parentSlug (peut correspondre au parent du form ou au context dans lequel la réponse est rempli)
        if (isset($ans["parentSlug"])) {
            $parent = $this->getModelSlug()->getElementBySlug($ans["parentSlug"], ["name"]);
            if ($this->getModelAuthorisation()->isElementAdmin($parent["id"], $parent["type"], $this->session->getUserId())) {
                return true;
            }
        }
        //Si l'answer a un entrée context, on regarde dedans
        // [] Elle contient le parent du formulaire lié à la réponse
        // [] Et le contexte dans laquelle la réponse a été écrite (ex : élément projet, orga, etc)
        if (isset($ans["context"]) && ! empty($ans["context"])) {
            $res = false;
            foreach ($ans["context"] as $k => $v) {
                if ($this->getModelAuthorisation()->isElementAdmin($k, $v["type"], $this->session->getUserId())) {
                    $res = true;
                    break;
                }
            }
            if ($res) {
                return true;
            }
        }
        // TODO  check $this->getForm()->Admin : Pas commenté sur master
        // !!! Vérifier que bien utile en plus du parentSlug check et du context (condition ci-dessus) !!!!
        if (! empty($ans["form"]) && $this->getModelForm()->canAdmin($ans["form"])) {
            return true;
        }

        if ($this->getModelCostum()->isSameFunction("canEditAnswer", [
            "answer" => $ans,
        ])) {
            return $this->getModelCostum()->sameFunction("canEditAnswer", [
                "answer" => $ans,
            ]);
        }

        return false;
    }

    // TODO :  pourquoi on utilise ctype_xdigit ? remplacer par is_string
    // TODO : $form not used in canEdit
    // TODO : $userId not used in canEdit
    // TODO : $parent not usedin canEdit
    /**
     * Determines if a user can access the answer.
     *
     * @param string|array $answer The answer ID or data.
     * @param array|null $form The form associated with the answer (optional).
     * @param string|null $userId The ID of the user (optional).
     * @param array|null $parentForm The parent form of the answer (optional).
     * @return bool Returns true if the user can access the answer, false otherwise.
     */
    public function canAccess($answer, ?array $form = null, ?string $userId = null, ?array $parentForm = null): bool
    {
        if (is_string($answer)) {
            $answer = $this->getById($answer);
            $form = $this->getModelForm()->getByIdMongo($answer["form"]);
            $parent = $this->getModelSlug()->getElementBySlug($form["id"]);
        }
        if ($this->canEdit($answer, $form, $userId, $parentForm)) {
            return true;
        } elseif ($this->getModelCostum()->isSameFunction("canAccessAnswer", [
            "answer" => $answer,
        ])) {
            return $this->getModelCostum()->sameFunction("canAccessAnswer", [
                "answer" => $answer,
            ]);
        }
        return false;
    }

    // TODO :  $form est toujours utilisé à null
    /**
     * Get a list of answers based on optional parameters.
     *
     * @param string|null $form The form parameter.
     * @param string|null $costum The costum parameter.
     * @param string|null $userId The user ID parameter.
     * @param array|null $cond The condition parameter.
     * @return array The list of answers.
     */
    public function getListBy(?string $form = null, ?string $costum = null, ?string $userId = null, ?array $cond = null): array
    {
        $where = [];
        $lists = [];
        if (! empty($form)) {
            $where["form"] = $form;
        }
        if (! empty($costum)) {
            $where["source.keys"] = [
                '$in' => [$costum],
            ];
        }
        if (! empty($userId)) {
            $where["user"] = $userId;
        }
        if (! empty($cond)) {
            $where = [
                '$and' => [$where, $cond],
            ];
        }
        if (! empty($where)) {
            $lists = $this->db->find(AnswerInterface::COLLECTION, $where);
        }
        return $lists;
    }

    /**
     * Global autocomplete function.
     *
     * @param array $form The form data.
     * @param array $searchParams The search parameters.
     * @return array The autocomplete results.
     */
    public function globalAutocomplete(array $form, array $searchParams): array
    {
        $searchParams["indexMin"] ??= 0;
        $searchParams["indexStep"] ??= 100;
        $mappingData = $form["mapping"] ?? [
            "name" => "name",
            "address" => "address",
            "answers.aapStep1.titre" => "answers.aapStep1.titre",
        ];
        $query = [];
        if ($searchParams["searchType"][0] == "answers" && empty($searchParams["filters"]["sousOrganisation"])) {
            $query = [
                "form" => (string) @$form["_id"],
            ];
        }

        if (! empty($searchParams["filters"]["notSeen"]) && ! empty($searchParams["filters"]["form"])) {
            $views = $this->db->find("views", [
                "form" => $searchParams["filters"]["form"],
                "user" => [
                    '$ne' => $this->session->getUserId(),
                ],
            ]);
            $answersId = [];
            foreach ($views as $key => $value) {
                $answersId[] = $this->db->MongoId($value["parentId"]);
            }
            $query = $this->getModelSearchNew()->addQuery($query, [
                "_id" => [
                    '$nin' => $answersId,
                ],
            ]);
            unset($searchParams["filters"]["notSeen"]);
        }

        if (! empty($searchParams["filters"]["sousOrganisation"]) && ! empty($searchParams["filters"]["form"])) {
            $prms = explode("-", $searchParams["filters"]["sousOrganisation"][0]);
            $prms[] = $searchParams["filters"]["form"];

            $query = [
                "form" => [
                    '$in' => $prms,
                ],
            ];
            unset($searchParams["filters"]["sousOrganisation"]);
            if (! empty($searchParams["filters"]["allSousOrganisation"])) {
                unset($searchParams["filters"]["allSousOrganisation"]);
            }
            unset($searchParams["filters"]["form"]);
        }

        if (! empty($searchParams["filters"]["allSousOrganisation"])) {
            $prms = explode("-", $searchParams["filters"]["allSousOrganisation"][0]);
            $prms[] = $searchParams["filters"]["form"];

            $query = [
                "form" => [
                    '$in' => $prms,
                ],
            ];
            unset($searchParams["filters"]["allSousOrganisation"]);
            unset($searchParams["filters"]["form"]);
        }

        if (! empty($searchParams["userId"])) {
            $query = $this->getModelSearchNew()->addQuery($query, [
                "user" => $searchParams["userId"],
            ]);
        }

        if (! empty($searchParams["name"])) {
            if (! empty($searchParams["textPath"])) {
                $query = $this->getModelSearchNew()->searchText($searchParams["name"], $query, [
                    "textPath" => $searchParams["textPath"],
                ]);
            } else {
                $query = $this->getModelSearchNew()->searchText($searchParams["name"], $query);
            }
        }
        if (! empty($searchParams["filters"]["address"])) {
            $pathToAddress = $mappingData["address"]["path"] ?? $mappingData["address"];
            $searchRegExp = $this->getModelSearchNew()->accentToRegex(trim(urldecode($searchParams["filters"]['address'])));
            $query = $this->getModelSearchNew()->addQuery(
                $query,
                [
                    '$or' => [[
                        $pathToAddress . ".postalCode" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                    ], [
                        $pathToAddress . ".name" => $this->db->MongoRegex("/.*{$searchRegExp}.*/i"),
                    ]],
                ]
            );
            unset($searchParams["filters"]["address"]);
        }

        if (! empty($searchParams["searchTags"]) &&
            ((is_countable($searchParams["searchTags"]) ? count($searchParams["searchTags"]) : 0) > 1 || (is_countable($searchParams["searchTags"]) ? count($searchParams["searchTags"]) : 0) == 1 && $searchParams["searchTags"][0] != "")) {
            $operator = (! empty($options) && isset($options["tags"]) && isset($options["tags"]["verb"])) ? $options["tags"]["verb"] : '$in';
            if (! empty($searchParams["tagsPath"])) {
                $query = $this->getModelSearchNew()->searchTags($searchParams["searchTags"], $operator, $query, $searchParams["tagsPath"]);
                unset($searchParams["tagsPath"]);
            } else {
                $query = $this->getModelSearchNew()->searchTags($searchParams["searchTags"], $operator, $query);
            }
            unset($searchParams["searchTags"]);
        }

        if (! empty($searchParams['filters'])) {
            $query = $this->getModelSearchNew()->searchFilters($searchParams['filters'], $query);
        }

        if (! empty($searchParams['sortBy'])) {
            if ($this->getModelApi()->isAssociativeArray($searchParams['sortBy'])) {
                $sortBy = [];
                foreach ($searchParams["sortBy"] as $key => $value) {
                    $sortBy[$key] = (int) $value;
                }
                $searchParams["sortBy"] = $sortBy;
            } else {
                $sortBy = [];
                foreach ($searchParams["sortBy"] as $key => $value) {
                    $sortBy[$value] = 1;
                }
                $searchParams["sortBy"] = $sortBy;
            }
        } else {
            $searchParams['sortBy'] = [
                "updated" => -1,
            ];
        }

        if (! isset($searchParams['fields'])) {
            $searchParams['fields'] = [];
        }

        $res = [];

        $res["results"] = $this->db->findAndFieldsAndSortAndLimitAndIndex($searchParams["searchType"][0], $query, $searchParams["fields"], $searchParams['sortBy'], $searchParams["indexStep"], $searchParams["indexMin"]);
        if (isset($searchParams["count"])) {
            $res["count"][$searchParams["searchType"][0]] = $this->db->count($searchParams["searchType"][0], $query);
        }

        return $res;
    }

    /**
     * Returns an array of mapping values based on the given mapping and answer.
     *
     * @param array $mapping The mapping array.
     * @param array $ans The answer array.
     * @return array The array of mapping values.
     */
    public function getMappingValues(array $mapping, array $ans): array
    {
        foreach ($mapping as $k => $path) {
            if (is_array($path) && isset($path["path"])) {
                $path = $path["path"];
            }
            $mapping[$k] = $this->getValueByPath($path, $ans);
        }
        return $mapping;
    }

    // TODO : n'est utiliser que dans cette class changer la visibilité en private
    // TODO : verfifier le retour
    /**
     * Get the value from an array by accessing it using a given path.
     *
     * @param array|string $indexes The path to access the value in the array.
     * @param array $arrayToAccess The array to access the value from.
     * @return mixed The value from the array at the specified path.
     */
    public function getValueByPath($indexes, array $arrayToAccess)
    {
        if (is_string($indexes)) {
            $indexes = explode(".", $indexes);
        }
        if (isset($arrayToAccess[$indexes[0]])) {
            if ((is_countable($indexes) ? count($indexes) : 0) > 1) {
                return $this->getValueByPath(array_slice($indexes, 1), $arrayToAccess[$indexes[0]]);
            } else {
                return $arrayToAccess[$indexes[0]];
            }
        }
    }

    /**
     * Récupère les données des réponses.
     *
     * @param array $answerList La liste des réponses.
     * @param array $subForms Les sous-formulaires.
     * @param array $form Le formulaire.
     * @return array Les données des réponses.
     */
    public function getDataAnswers(array $answerList, array $subForms, array $form): array
    {
        $globalLinks = [];
        $gUids = [];
        if (! empty($answerList)) {
            foreach ($answerList as $key => $ans) {
                if (isset($form["mapping"])) {
                    $answerList[$key]["mappingValues"] = $this->getMappingValues($form["mapping"], $ans);
                }
                $answerList[$key]["countComment"] = $this->db->count(CommentInterface::COLLECTION, [
                    "contextId" => $key,
                    "contextType" => FormInterface::ANSWER_COLLECTION,
                ]);

                $localLinks = [];
                $uids = [];
                $todo = 0;
                $done = 0;
                $tasksPerson = [];
                if (! isset($ans["answers"])) {
                    $percent = 0;
                } else {
                    $totalInputs = 0;
                    $answeredInputs = 0;
                    foreach ($subForms as $fid => $f) {
                        $totalInputs += is_countable(@$f["inputs"]) ? count(@$f["inputs"]) : 0;
                        if (isset($ans["answers"][$fid])) {
                            $answeredInputs += is_countable($ans["answers"][$fid]) ? count($ans["answers"][$fid]) : 0;
                        }

                        //todo lists are on depense for the moment
                        //todo genereaclly not with a fixed input ID
                        if (isset($ans["answers"][$fid]["depense"])) {
                            foreach ($ans["answers"][$fid]["depense"]  as $ix => $dep) {
                                if (isset($dep["todo"])) {
                                    foreach ($dep["todo"] as $ixx => $t) {
                                        if (! isset($t["done"]) || $t["done"] == "0") {
                                            $todo++;
                                            $whos = (is_array($t["who"])) ? $t["who"] : explode(",", $t["who"]);
                                            foreach ($whos as $whoix => $who) {
                                                if (! isset($tasksPerson[$who])) {
                                                    $tasksPerson[$who] = [];
                                                }
                                                $tasksPerson[$who][] = $t["what"];
                                            }
                                        } else {
                                            $done++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (isset($ans["links"])) {
                        foreach ($ans["links"] as $type => $ls) {
                            if (! isset($localLinks[$type])) {
                                $localLinks[$type] = [];
                            }

                            if (! isset($globalLinks[$type])) {
                                $globalLinks[$type] = [];
                            }

                            foreach ($ls as $uid => $time) {
                                if (is_string($uid) && strlen($uid) == 24 && ctype_xdigit($uid)) {
                                    if (! in_array($uid, $localLinks[$type])) {
                                        $localLinks[$type][] = $uid;
                                    }
                                    if (! in_array($uid, $uids)) {
                                        $uids[] = $this->db->MongoId($uid);
                                    }
                                    if (! in_array($uid, $globalLinks[$type])) {
                                        $globalLinks[$type][] = $uid;
                                    }
                                    if (! in_array($uid, $gUids)) {
                                        $gUids[] = $this->db->MongoId($uid);
                                    }
                                }
                            }
                        }
                    }
                    $percent = floor($answeredInputs * 100 / $totalInputs);
                }
                $answerList[$key]["percent"] = $percent;
                $answerList[$key]["uids"] = $uids;
                $answerList[$key]["todo"] = $todo;
                $answerList[$key]["done"] = $done;
                $answerList[$key]["tasksPerson"] = $tasksPerson;
                $answerList[$key]["canEdit"] = $this->canEdit($ans, $form, $this->session->getUserId());
            }
        }
        return $answerList;
    }

    // TODO :  fonction non utilisé
    // public function csv($elements, $idElt, $valElt, $forms)
    // {
    //     $elements[$idElt]["id"] = $idElt;
    //     $elements[$idElt]["user"] = $valElt["user"];
    //     $elements[$idElt]["created"] = date("d/m/Y", $valElt["created"]);
    //     if (! empty($valElt["answers"])) {
    //         foreach ($forms as $keyF => $valF) {
    //             if (! empty($valF["id"]) && ! empty($valElt["answers"][$valF["id"]])) {
    //                 foreach ($valElt["answers"][$valF["id"]] as $keyAns => $valAns) {
    //                     if (! empty($valF["inputs"]) && ! empty($valF["inputs"][$keyAns]) && ! empty($valF["inputs"][$keyAns]["label"])) {
    //                         $elements[$idElt][$valF["inputs"][$keyAns]["label"]] = $valAns;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     return $elements;
    // }

    // TODO : fonction utiliser que dans cette class changer la visibilité en private
    // TODO : getAnswerCommunityToNotify n'existe pas
    /**
     * Returns the community to notify based on the answer and entry.
     *
     * @param array $answer The answer array.
     * @param array|null $entry The entry array (optional).
     * @return array The community to notify.
     */
    private function getCommunityToNotify(array $answer, ?array $entry = null): array
    {
        $arrayMail = [];
        if (! empty($answer["links"])) {
            foreach ($answer["links"] as $k => $v) {
                if ($k == "answered" && (empty($entry) || in_array("answered", $entry))) {
                    foreach ($v as $id) {
                        $people = $this->getModelElement()->getElementById($id, PersonInterface::COLLECTION, null, ["email", "name", "slug"]);
                        array_push($arrayMail, $people);
                    }
                }
            }
            if ($this->getModelCostum()->isSameFunction("getAnswerCommunityToNotify")) {
                // CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
                array_merge($arrayMail, $this->getModelCostum()->sameFunction("getAnswerCommunityToNotify", [
                    "answer" => $answer,
                ]));
            }
        }
        return $arrayMail;
    }

    /**
     * Process the mail.
     *
     * @param string $id The ID of the mail.
     * @param string $tpl The template to use for the mail.
     * @param string|null $step The step of the mail process.
     * @param array $infos Additional information for the mail.
     * @return void
     */
    public function mailProcess(string $id, string $tpl, ?string $step, array $infos): void
    {
        $costum = $this->getModelCostum()->getCostum();
        $answer = $this->getById($id);
        $mailsCommunity = [];
        if (isset($infos["emails"]) && ! empty($infos["emails"])) {
            foreach ($infos["emails"] as $v) {
                array_push($mailsCommunity, $this->getModelElement()->getElementByWhere(PersonInterface::COLLECTION, [
                    "email" => $v,
                ], ["email", "name", "slug"]));
            }
        }
        if (isset($infos["community"]) && ! empty($infos["community"])) {
            $mailsCommunity = [];
            foreach ($infos["community"] as $k => $v) {
                if ($v["type"] == PersonInterface::COLLECTION) {
                    array_push($mailsCommunity, $this->getModelElement()->getElementById($k, PersonInterface::COLLECTION, null, ["email", "name", "slug"]));
                } elseif (in_array($v["type"], [OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION])) {
                    $community = $this->getModelElement()->getCommunityByTypeAndId($v["type"], $k, PersonInterface::COLLECTION, "isAdmin");
                    foreach ($community as $e => $val) {
                        array_push($mailsCommunity, $this->getModelElement()->getElementById($e, PersonInterface::COLLECTION, null, ["email", "name", "slug"]));
                    }
                }
            }
        }
        if (empty($mailsCommunity)) {
            $mailsCommunity = $this->getCommunityToNotify($answer);
        }
        $answer["mappingValues"] = [];
        $form = $this->getModelForm()->getByIdMongo($answer["form"]);
        if (isset($form["mapping"])) {
            $answer["mappingValues"] = $this->getMappingValues($form["mapping"], $answer);
        }
        $nameAnsw = $answer["mappingValues"]["name"] ?? "";
        $objMail = "[" . $this->getModelMail()->getAppName() . "] " . $this->language->t("mail", self::$mailConfig[$tpl]["subject"], [
            "{what}" => $nameAnsw,
        ]);
        if (isset($infos["tplObject"])) {
            $objMail = "[" . $this->getModelMail()->getAppName() . "] " . $infos["tplObject"];
        }
        foreach ($mailsCommunity as $c) {
            if (isset($c["email"])) {
                $params = [
                    "type" => CronInterface::TYPE_MAIL,
                    "tpl" => self::$mailConfig[$tpl]["tpl"],
                    "subject" => $objMail,
                    "from" => $this->params->get('adminEmail'),
                    "to" => $c["email"],
                    "tplParams" => [
                        "user" => $c,
                        "title" => $this->getModelMail()->getAppName(),
                        "answer" => $answer,
                        "form" => $form,
                        "msg" => @$infos,
                    ],
                ];
                if (isset($infos["msg"])) {
                    $params["tplParams"]["msg"] = $infos["msg"];
                }
                if (isset($infos["html"])) {
                    $params["tplParams"]["html"] = $infos["html"];
                }

                $params = $this->getModelMail()->getCustomMail($params);
                $this->getModelMail()->schedule($params);
            }
        }
        if ($this->getModelCostum()->isSameFunction("AnswerMailProcess")) {
            // CARREFUL $costumUserArray need to be return as result : return $params["costumUserArray"]
            $this->getModelCostum()->sameFunction(
                "AnswerMailProcess",
                [
                    "tpl" => $tpl,
                    "step" => $step,
                    "infos" => $infos,
                    "form" => $form,
                    "answer" => $answer,
                ]
            );
        }
    }

    /**
     * Validates the given parameters.
     *
     * @param array $params The parameters to validate.
     * @return array The validated parameters.
     */
    public function validate(array $params): array
    {
        $costum = $this->getModelCostum()->getCostum();
        $set = [];
        $res = [
            'result' => false,
            "msg" => "Erreur",
        ];
        if (! empty($params["answerId"])) {
            $answer = $this->db->findOneById(AnswerInterface::COLLECTION, $params["answerId"]);
        }

        if (! empty($answer)) {
            if (! empty($params["input"]["validated"]) && ($params["input"]["validated"] === true || $params["input"]["validated"] === "true")) {
                $set["validated"] = true;
            }

            if (! empty($params["input"]["generateElement"])) {
                $mapping = [];
                $import = [
                    'file' => [json_encode([$answer])],
                    'nameFile' => "test",
                    'typeFile' => 'json',
                    "warnings" => "false",
                ];

                foreach ($params["input"]["generateElement"] as $type => $map) {
                    $mapping = [];
                    foreach ($map as $k => $v) {
                        $mapping[] = [
                            'idHeadCSV' => $v["from"],
                            'valueAttributeElt' => $v["to"],
                        ];
                    }
                    $import['infoCreateData'] = $mapping;
                    $import['typeElement'] = $type;

                    $resImport = $this->getModelImport()->previewData($import, true, true, true);
                    // Todo c'est bizzare dans une boucle pas de Rest::json dans les modeles je commente surement un oubli
                    // return Rest::json($resImport); exit;
                    foreach ($resImport["elementsObj"] as $keyElt => $elt) {
                        $elt = $this->getModelImport()->checkElement($elt, $type);
                        if (! empty($elt["msgError"])) {
                            unset($elt["msgError"]);
                        }
                        $elt["collection"] = $type;
                        $elt["key"] = $type;
                        if ($this->getModelCostum()->isSameFunction("generateElementBeforeSave")) {
                            $elt = $this->getModelCostum()->sameFunction("generateElementBeforeSave", $elt);
                        }
                        $save = $this->getModelElement()->save($elt);

                        $this->getModelLink()->connect($save["id"], $type, $params["answerId"], AnswerInterface::COLLECTION, $this->session->getUserId(), "answers", false, false, false, false);
                        $this->getModelLink()->connect($params["answerId"], AnswerInterface::COLLECTION, $save["id"], $type, $this->session->getUserId(), $type, false, false, false, false);
                    }
                    $res = [
                        'result' => true,
                        "msg" => "Valider",
                    ];
                }
            }

            if ($this->getModelCostum()->isSameFunction("answerValidate")) {
                $paramBeforeValidate = $this->getModelCostum()->sameFunction("answerValidate", $params);
            }

            if (! empty($set)) {
                $this->db->update(
                    AnswerInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId((string) $params["answerId"]),
                    ],
                    [
                        '$set' => $set,
                    ]
                );

                $res = [
                    'result' => true,
                    "msg" => "Valider",
                ];
            }

            if ($res["result"] === true) {
                // TODO : pas de renderPartial dans le modele le passer dans l'action
                $res["html"] = $params["controller"]->renderPartial('survey.views.tpls.forms.finish', $res, true);
            }
        }

        return $res;
    }

    /**
     * Check if a value is true.
     *
     * @param string|bool|int $val The value to check.
     * @param bool $return_null Whether to return null if the value is not true. Default is false.
     * @return bool Returns true if the value is true, false otherwise.
     */
    public function is_true($val, bool $return_null = false): bool
    {
        $boolval = (is_string($val) ? filter_var($val, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) : (bool) $val);
        return ($boolval === null && ! $return_null ? false : (bool) $boolval);
    }

    // TODO : le nom de la signature n'a pas la même syntaxe que les autres fonctions
    /**
     * Generates a project from an answer.
     *
     * @param string $name The name of the project.
     * @param string $descr The description of the project.
     * @param array $links The links related to the project (optional).
     * @param array $financer The financers of the project (optional).
     * @param array $parent The parent project (required).
     * @param array $action The actions related to the project (optional).
     * @param string $answerId The ID of the answer.
     * @param array|null $images The images related to the project (optional).
     *
     * @return array The generated project.
     */
    public function GenerateProjectFromAnswer(string $name, string $descr, array $links = [], array $financer = [], array $parent, array $action = [], string $answerId, ?array $images = null): array
    {
        $room = [];
        $params = [];
        $answer = $this->db->findOneById(FormInterface::ANSWER_COLLECTION, $answerId);

        if (! empty($answer["project"]["id"])) {
            $project = $this->db->findOneById(ProjectInterface::COLLECTION, (string) $answer["project"]["id"]);
            if (! empty($answer["project"]["room"])) {
                $room = $this->db->findOneById(RoomInterface::COLLECTION, $answer["project"]["room"]);
            } else {
                $room["_id"] = "";
            }
            $p_id = "";
            $p_type = "";
            foreach ($parent as $keyp => $valuep) {
                if (isset($valuep["type"])) {
                    $p_id = $keyp;
                    $p_type = $valuep["type"];
                }
            }
            if ($room["_id"] == "") {
                $room = [
                    "collection" => RoomInterface::COLLECTION,
                    "name" => $name,
                    "description" => $descr,
                    "parentId" => (string) $p_id,
                    "parentType" => $p_type,
                    "status" => "open",
                    "created" => time(),
                    "creator" => $this->session->getUserId(),
                ];

                $room = $this->db->insert(RoomInterface::COLLECTION, $room);
            }
        } else {
            $project = [
                "collection" => ProjectInterface::COLLECTION,
                "name" => $name,
                "shortDescription" => $descr,
                "created" => time(),
                "creator" => $this->session->getUserId(),
            ];
            $p_id = "";
            $p_type = "";
            foreach ($parent as $keyp => $valuep) {
                if (isset($valuep["type"])) {
                    $p_id = $keyp;
                    $p_type = $valuep["type"];
                }
            }

            $room = [
                "collection" => RoomInterface::COLLECTION,
                "name" => $name,
                "description" => $descr,
                "parentId" => (string) $p_id,
                "parentType" => $p_type,
                "status" => "open",
                "created" => time(),
                "creator" => $this->session->getUserId(),
            ];

            $project["parent"] = $parent;
            if (! empty($images)) {
                if (isset($images["profilImageUrl"])) {
                    $project["profilImageUrl"] = $images["profilImageUrl"];
                }
                if (isset($images["profilMarkerImageUrl"])) {
                    $project["profilMarkerImageUrl"] = $images["profilMarkerImageUrl"];
                }
                if (isset($images["profilMediumImageUrl"])) {
                    $project["profilMediumImageUrl"] = $images["profilMediumImageUrl"];
                }
                if (isset($images["profilThumbImageUrl"])) {
                    $project["profilThumbImageUrl"] = $images["profilThumbImageUrl"];
                }
            }

            $project = $this->db->insert(ProjectInterface::COLLECTION, $project);

            $slugpro = $this->getModelSlug()->checkAndCreateSlug($project["name"], ProjectInterface::COLLECTION, (string) $project["_id"]);

            $this->getModelSlug()->save(ProjectInterface::COLLECTION, (string) $project["_id"], $slugpro);
            $params["slug"] = $slugpro;
            $this->getModelElement()->updateField(ProjectInterface::COLLECTION, (string) $project["_id"], "slug", $slugpro);

            $room = $this->db->insert(RoomInterface::COLLECTION, $room);
        }

        $child = [];
        $child["childType"] = ProjectInterface::COLLECTION;
        $child["childId"] = $project["_id"];
        $this->getModelLink()->connectParentToChild($p_id, $p_type, $child, false, $this->session->getUserId());

        foreach ($links as $keylink => $valuelink) {
            $child = [];
            $child["childType"] = PersonInterface::COLLECTION;
            $child["childId"] = $valuelink;
            $this->getModelLink()->connectParentToChild($project["_id"], ProjectInterface::COLLECTION, $child, false, $this->session->getUserId(), "");
        }
        foreach ($financer as $keyf => $valuef) {
            $child = [];
            $child["childType"] = OrganizationInterface::COLLECTION;
            $child["childId"] = $valuef;
            $this->getModelLink()->connectParentToChild($project["_id"], ProjectInterface::COLLECTION, $child, false, $this->session->getUserId(), ["Financeur"]);
        }

        //add action
        if (! empty($answer["project"]["id"])) {
            $oldactions = $this->db->find(ActionsInterface::COLLECTION, [
                "parentId" => (string) $answer["project"]["id"],
                "parentType" => ProjectInterface::COLLECTION,
            ]);

            foreach ($oldactions as $okey => $ovalue) {
                foreach ($action as $nkey => $nvalue) {
                    /* if ($nvalue["nom"] == $ovalue["name"]) {
                        array_splice($action,$nkey,1);
                    } */
                    if (isset($nvalue["actionid"])) {
                        array_splice($action, $nkey, 1);
                    }
                }
            }
        }
        foreach ($action as $akey => $avalue) {
            $action = [
                "name" => @$avalue["nom"],
                "credits" => @$avalue["credits"],
                "parentId" => (string) $project["_id"],
                "parentType" => ProjectInterface::COLLECTION,
                "answerId" => (string) $answer["_id"],
                "status" => "todo",
                "idUserAuthor" => $this->session->getUserId(),
                "idParentRoom" => (string) $room["_id"],
                "options" => [
                    "creditAddPorteur" => false,
                    "creditSharePorteur" => false,
                    "possibleStartActionBeforeStartDate" => false,
                ],
                "max" => 1,
                "min" => 1,
                "noStartDate" => true,
            ];

            $action = $this->db->insert(ActionInterface::COLLECTION, $action);

            if (isset($avalue["depenseKey"]) && isset($action["_id"])) {
                $this->db->update(
                    AnswerInterface::COLLECTION,
                    [
                        "_id" => $this->db->MongoId($answerId),
                    ],
                    [
                        '$set' => [
                            'answers.aapStep1.depense.' . $avalue["depenseKey"] . '.actionid' => (string) $action["_id"],
                        ],
                    ]
                );
            }
            $paramsaction = [
                "id" => $action["_id"],
            ];

            $paramsaction["child"] = [
                "id" => $avalue["user"],
                "type" => PersonInterface::COLLECTION,
            ];

            $this->getModelActionRoom()->assignPeople($paramsaction);
        }

        return [
            "projet" => (string) $project["_id"],
            "room" => (string) $room["_id"],
        ];
    }

    // TODO : $key verifier le type
    // TODO : le nom de la signature n'a pas la même syntaxe que les autres fonctions
    // TODO : renvois du html ça ne devrait pas être dans le modele
    /**
     * Calcule le résultat d'un vote majoritaire.
     *
     * @param array $parentForm Le tableau contenant les données du formulaire parent.
     * @param string $formId L'identifiant du formulaire.
     * @param mixed $key La clé utilisée pour récupérer les données du formulaire.
     * @param array $paramsData Les données supplémentaires utilisées dans le calcul.
     * @return string Le résultat du vote majoritaire.
     */
    public function ResultVoteMajoritaire(array $parentForm, string $formId, $key, array $paramsData = []): string
    {
        $allFormAnswers = $this->db->find(FormInterface::ANSWER_COLLECTION, [
            "form" => (string) $parentForm['_id'],
            "updated" => [
                '$exists' => 1,
            ],
        ]);
        $reslutView = [];
        $nbVote = 0;
        foreach ($allFormAnswers as $elem) {
            if (isset($elem['answers'][$formId][$key])) {
                if (is_countable($elem['answers'][$formId][$key]) ? count($elem['answers'][$formId][$key]) : 0) {
                    foreach ($elem['answers'][$formId][$key] as $index => $item) {
                        if (isset($reslutView[$item['propos']])) {
                            $keyFound = array_search($item['niveau'], array_column(json_decode(json_encode($reslutView[$item['propos']]), true), 'niveau'));
                            if ($keyFound > -1) {
                                $reslutView[$item['propos']][$keyFound]['count']++;
                            } else {
                                array_push($reslutView[$item['propos']], [
                                    'niveau' => $item['niveau'],
                                    'count' => 1,
                                ]);
                            }
                        } else {
                            $reslutView[$item['propos']][0] = [
                                'niveau' => $item['niveau'],
                                'count' => 1,
                            ];
                        }
                    }
                }
            }
            $nbVote++;
        }

        $htmlResult = '
		<div class="col-xs-12">';

        if (count($reslutView)) {
            $htmlResult .= '
				<div class="col-xs-12 p-0 card" style="width: 100%; min-width: 550px">
					<div class="card-header">
						<h4>Profil de mérites</h4>
					</div>
					<div class="card-body">
						<div class="">
							<table class="profiles table-merite">';
            foreach ($paramsData['list'] as $index => $item) {
                if ($index == 0) {
                    $htmlResult .= '
											<thead>
                                            	<tr>
                                                	<th></th>';
                    foreach ($paramsData['mentions'] as $i => $mentItem) {
                        $htmlResult .= '
																		<th>
																			<small class="tab-mieuxvoter n' . $i . ' badge">' . $mentItem . '</small>
																		</th>';
                    }
                    $htmlResult .= '</tr>
											</thead>
											<tbody>';
                }
                $htmlResult .= '<tr>
												<th>' . $item . '</th>';
                if (isset($reslutView[$item])) {
                    foreach ($paramsData['mentions'] as $idMent => $mentItem) {
                        $indexFound = array_search($idMent, array_column(json_decode(json_encode($reslutView[$item]), true), 'niveau'));
                        if ($indexFound > -1) {
                            $htmlResult .= '
																		<td>
                                                                            ' . round(($reslutView[$item][$indexFound]['count'] * 100) / $nbVote, 2, PHP_ROUND_HALF_EVEN) . '
                                                                            %
                                                                        </td>';
                        } else {
                            $htmlResult .= '<td>0%</td>';
                        }
                    }
                }
                $htmlResult .= '</tr>';
            }
            $htmlResult .= '</tbody>
							</table>
						</div>
					</div>     
						<span class="ml-1 text-nowrap">Nombre de votes: <span class="text-bold">' . $nbVote . '</span></span>
					</div>';
        }
        $htmlResult .= '
		</div>';

        return $htmlResult;
    }

    // TODO : le nom de la signature n'a pas la même syntaxe que les autres fonctions
    // TODO : renvois du html ça ne devrait pas être dans le modele
    public function ResultCoDate(array $parentForm, string $coDateKunik, string $inputKey, array $inputValue, array $paramsData = [], bool $isAdmin = false): string
    {
        $allFormAnswers = $this->db->find(FormInterface::ANSWER_COLLECTION, [
            "form" => (string) $parentForm['_id'],
            "updated" => [
                '$exists' => 1,
            ],
        ], ['answers', 'user', 'updated']);
        $reslutView = [];
        $nbVote = 0;
        $userConcerned = null;
        $bodyUsersAnswers = '';
        $tabViewAllResult = '<table class="codate table-merite" style="margin-bottom:0; width: unset;"><thead><tr><th rowspan="2"></th>';
        $trSubBlock = '';
        $bodyBlock = '';
        $iterationCount = 0;
        $footerBlock = '';
        $maxChoose = 0;
        $ownAnswerBlock = '';
        $valideVoters = 0;
        $label = $inputValue['label'];
        if (count($allFormAnswers) > 0) {
            foreach ($allFormAnswers as $iter => $elem) {
                if ($iterationCount == 0) {
                    foreach ($paramsData['alldates'] as $indexDateParam => $itemDateParam) {
                        $tabViewAllResult .= '
							<th colspan="' . (is_countable($itemDateParam) ? count($itemDateParam) : 0) . '">' . $indexDateParam . '</th>
						';
                        foreach ($itemDateParam as $itemIter => $timeValue) {
                            $colUnikId = uniqid();
                            $createEventBtn = $isAdmin ? '<a href="javascript:;" id="' . $colUnikId . '" class="createEvent" data-id="' . $this->db->MongoId() . '" data-formid="' . (string) $parentForm['_id'] . '" data-date="' . $indexDateParam . '" data-time="' . $timeValue . '" data-titleQuestion="' . $label . '" data-eventduration="' . $paramsData["eventduration"] . '"><i class="fa fa-calendar"><small> Create event</small></i></a>' : '';
                            $trSubBlock .= '
							<th>' . $timeValue . '<br>' . $createEventBtn . '</th>
						';
                        }
                    }
                }
                if (isset($elem['updated'])) {
                    $valideVoters++;
                    $userConcerned = $this->db->findOneById(Citoyen::COLLECTION, $elem['user'], ['name', 'username', "roles"]);
                    if (isset($userConcerned['name'])) {
                        if (isset($userConcerned["roles"])) {
                            if (isset($userConcerned["roles"]["tobeactivated"])) {
                                isset($elem["name"]) ? $bodyBlock .= '<tr><th class="bg-name">' . $elem['name'] . '</th>' : $bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
                            } else {
                                $bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
                            }
                        } else {
                            $bodyBlock .= '<tr><th class="bg-name">' . $userConcerned['name'] . '</th>';
                        }
                    } else {
                        $bodyBlock .= '<tr><th class="bg-name">' . 'Not logged' . '</th>';
                    }

                    foreach ($paramsData['alldates'] as $indexKey => $item) {
                        foreach ($item as $itemIter => $timeValue) {
                            $colUnikId = uniqid();

                            if ($iterationCount == 0) {
                                $reslutView[$indexKey][$timeValue] = 0;
                            }
                            $valChecked = '';
                            if (isset($elem['answers'][$inputValue['subFormId']][$inputKey])) {
                                if (isset($elem['answers'][$inputValue['subFormId']][$inputKey][$indexKey])) {
                                    if (array_search($timeValue, $elem['answers'][$inputValue['subFormId']][$inputKey][$indexKey]) > -1) {
                                        $valChecked = "checked";
                                        if (isset($reslutView[$indexKey][$timeValue])) {
                                            $reslutView[$indexKey][$timeValue]++;
                                        } else {
                                            $reslutView[$indexKey][$timeValue] = 1;
                                        }
                                        if ($reslutView[$indexKey][$timeValue] > $maxChoose) {
                                            $maxChoose = $reslutView[$indexKey][$timeValue];
                                        }
                                    }
                                }
                            }
                            $bodyBlock .= '
							<td>
							<input type="checkbox" id="cbr_' . $colUnikId . '" name="codateR' . $coDateKunik . '" class="animate-check" disabled ' . $valChecked . ' />
								<label for="cbr_' . $colUnikId . '" class="check-box"></label> 
							</td>
						';
                        }
                    }
                    $bodyBlock .= '
					</tr>';
                }
                if (count($allFormAnswers) - 1 == $iterationCount) {
                    foreach ($paramsData['alldates'] as $indexKey => $item) {
                        foreach ($item as $itemIter => $timeValue) {
                            $classColMax = '';
                            $maxIcon = '';
                            if (isset($reslutView[$indexKey])) {
                                if (isset($reslutView[$indexKey][$timeValue])) {
                                    if ($reslutView[$indexKey][$timeValue] == $maxChoose && $maxChoose != 0) {
                                        $classColMax = 'max-votant';
                                        $maxIcon = '<i class="fa fa-star"></i>';
                                        // $maxChoose = 99999999999;
                                    }
                                    $footerBlock .= '
										<td class="' . $classColMax . '" >' . $maxIcon . ' ' . $reslutView[$indexKey][$timeValue] . '</td>
									';
                                } else {
                                    $footerBlock .= '
										<td class="fix-width ' . $classColMax . '" > 0</td>
									';
                                }
                            } else {
                                $footerBlock .= '
									<td class="fix-width ' . $classColMax . '" > 0</td>
								';
                            }
                        }
                    }
                }
                $iterationCount++;
            }
        }
        return $tabViewAllResult .= '
			</tr>
			<tr class="fs">
				' . $trSubBlock . '
			</tr>
			</thead>
			<tbody class="fix-width">
				' . $bodyBlock . '
			</tbody>
			<tfoot class="fix-width">
				<tr>
					<th class="bg-name">' . $this->language->t("common", "Total voters ") . $valideVoters . '</th>
					' . $footerBlock . '
				</tr>
			</tfoot>
			</table>
		';
    }

    // TODO : le nom de la signature n'a pas la même syntaxe que les autres fonctions
    // TODO : $controller renvois le controller ça devrait pas être fait dans le modele
    // TODO : renderPartial ne devrait pas être dans le modele
    // TODO : le retour devrait être $allFormAnswers array
    // FIXME : Devrait être ResultScore(array $parentForm): array et modifier dans la vue ou l'action le renvois des données à utilisé avce le renderPartial
    public function ResultScore($controller, array $parentForm, string $show)
    {
        $allFormAnswers = $this->db->find(FormInterface::ANSWER_COLLECTION, [
            "form" => (string) $parentForm['_id'],
            "updated" => [
                '$exists' => 1,
            ],
        ], ['answers', 'user', 'updated']);
        return $controller->renderPartial("costum.views.custom.appelAProjet.callForProjects.scoreTable", [
            "allAnswers" => $allFormAnswers,
            "show" => $show,
        ]);
    }
    // /**
    //  * Calculates the result score based on the parent form.
    //  *
    //  * @param array $parentForm The parent form data.
    //  * @return array The result score.
    //  */
    // public function ResultScore(array $parentForm): array
    // {
    //     $allFormAnswers = $this->db->find(FormInterface::ANSWER_COLLECTION, [
    //         "form" => (string) $parentForm['_id'],
    //         "updated" => [
    //             '$exists' => 1,
    //         ],
    //     ], ['answers', 'user', 'updated']);
    //     return $allFormAnswers;
    // }
}
