<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\LogInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;

class Log extends BaseModel implements LogInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Saves a log entry.
     *
     * @param array $log The log entry to be saved.
     * @return void
     */
    public function save(array $log): void
    {
        if (isset($log['_id'])) {
            $id = $log['_id'];
            unset($log['_id']);
            $this->db->update(LogInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ], [
                '$set' => $log,
            ]);
        } else {
            $this->db->insert(LogInterface::COLLECTION, $log);
        }
    }
}
