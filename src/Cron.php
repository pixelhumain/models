<?php

namespace PixelHumain\Models;

use Exception;
use PixelHumain\Models\Interfaces\CronInterface;


use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\MailTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\MailTrait;
use PixelHumain\Models\Traits\SearchNewTrait;

class Cron extends BaseModel implements CronInterface, CostumTraitInterface, MailTraitInterface, SearchNewTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use MailTrait;
    use SearchNewTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateParamsProperty();
    }

    /**
     * Saves the cron data.
     *
     * @param array $params The cron parameters.
     * @param bool|null $update Whether to update an existing cron.
     * @return void
     */
    public function save(array $params, ?bool $update = null): void
    {
        $userId = null;
        $costum = $this->getModelCostum()->getCostum();
        if ($this->session->getUserId()) {
            $userId = $this->session->getUserId();
        } elseif (is_array($params['tplParams']) && ! empty($params['tplParams']) && ! empty($params['tplParams']["user"])) {
            $userId = (string) $params['tplParams']["user"];
        }

        $status = (($update == true) ? CronInterface::STATUS_UPDATE : CronInterface::STATUS_PENDING);

        $new = [
            "userId" => $userId,
            "status" => $status,
            "type" => $params['type'],
        ];

        if (isset($params["status"]) && $params["status"] === CronInterface::STATUS_FUTURE && isset($params["sendOn"])) {
            $new["status"] = $params["status"];
            $new["sendOn"] = $params["sendOn"];
        }

        if (isset($params['execTS'])) {
            $new['execTS'] = $params['execTS'];
        }

        if ($params["type"] == CronInterface::TYPE_BADGE) {
            $new["badgeId"] = $params["badgeId"];
            $new["awardId"] = $params["awardId"];
            $new["awardCollection"] = $params["awardCollection"];
            $new = $this->db->insert(CronInterface::COLLECTION, $new);
            return;
        }

        if (isset($params['expeditor'])) {
            $new['expeditor'] = $params['expeditor'];
        }

        if (isset($params['attach'])) {
            $new['attach'] = $params['attach'];
        }
        $tplBanned = null;
        if (is_array($costum) && ! empty($costum) && ! empty($costum["slug"])) {
            $new['source'] = [
                "key" => (string) $costum["slug"],
                "insertOrign" => "costum",
            ];
            if (isset($costum["mailsConfig"]) && isset($costum["mailsConfig"]["bannedTpl"])) {
                $tplBanned = $costum["mailsConfig"]["bannedTpl"];
            }
        }
        if ($params['type'] == CronInterface::TYPE_MAIL) {
            $new = array_merge($new, $this->addMailParams($params));
        }

        if (empty($tplBanned) || (is_array($tplBanned) && ! in_array($params["tpl"], $tplBanned))) {
            if (! empty($new["to"])) {
                $entity = $this->db->findOne(PersonInterface::COLLECTION, [
                    "email" => $new["to"],
                ], ["preferences"]);

                if (! empty($entity)) {
                    $mailActivated = ! (
                        isset($entity["preferences"]["mails"]) &&
                        ($entity["preferences"]["mails"] == "desactivated")
                    );

                    $preferenceSourceExist = ! empty($entity["preferences"]["sendMail"]["source"]) &&
                                            is_array($entity["preferences"]["sendMail"]["source"]);

                    $allowToReceiveMailFromSourceKey = $preferenceSourceExist &&
                                                        isset($params["source"]["key"]) &&
                                                        in_array($params["source"]["key"], $entity["preferences"]["sendMail"]["source"]);

                    $allowToReceiveMailFromCommunecter = $preferenceSourceExist &&
                                                        in_array("communecter", $entity["preferences"]["sendMail"]["source"]);

                    $sendMail = (
                        $mailActivated &&
                        $preferenceSourceExist && (
                            $allowToReceiveMailFromSourceKey ||
                            $allowToReceiveMailFromCommunecter
                        )
                    );

                    if ($sendMail === true || in_array($params["tpl"], ["invitation", "validation", "validationWithNewPwd", "inviteYouTo", "askToBecome", "relaunchInvitation", "passwordRetreive", "basic", "custom", "contactForm", "newsletter"])) {
                        $new = $this->db->insert(CronInterface::COLLECTION, $new);
                    }
                } else {
                    $new = $this->db->insert(CronInterface::COLLECTION, $new);
                }
            }
        }
    }

    /**
     * Ajoute les paramètres de messagerie.
     *
     * @param array $params Les paramètres à ajouter.
     * @return array Les paramètres mis à jour.
     */
    private function addMailParams(array $params): array
    {
        $mailParams = [
            "tpl" => $params['tpl'],
            "subject" => $params['subject'],
            "from" => $params['from'],
            "to" => $params['to'],
            "tplParams" => $params['tplParams'],
        ];
        if (isset($params["language"])) {
            $mailParams["language"] = $params["language"];
        }
        return $mailParams;
    }

    //TODO return result
    /**
     * Process the mail.
     *
     * @param array $params The parameters for processing the mail.
     * @return mixed The result of the mail processing.
     */
    public function processMail(array $params)
    {
        $forceMail = $this->params->get('forceMailSend');
        try {
            return $this->getModelMail()->send([
                "tpl" => $params['tpl'],
                "subject" => $params['subject'],
                "from" => $params['from'],
                "to" => $params['to'],
                "tplParams" => $params['tplParams'],
            ], $forceMail);
        } catch (Exception $e) {
            return [
                "result" => false,
                "msg" => "Problem sending Email : " . $e->getMessage(),
            ];
        }
    }

    /**
     * Process an entry with the given parameters.
     *
     * @param array $params The parameters for processing the entry.
     * @return void
     */
    public function processEntry(array $params): void
    {
        $res = null;
        if ($params["type"] == CronInterface::TYPE_MAIL) {
            $this->db->update(
                CronInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $params["_id"]),
                ],
                [
                    '$set' => [
                        "status" => CronInterface::STATUS_PROCESSED,
                        "executedTS" => $this->db->MongoDate(),
                    ],
                ]
            );
            $res = $this->processMail($params);

            if (! empty($params['attach']) && ! empty($params['attach']["id"])) {
                $this->db->remove(CronInterface::ASK_COLLECTION, [
                    "_id" => $this->db->MongoId((string) $params['attach']["id"]),
                ]);
            }
        } elseif ($params["type"] == CronInterface::TYPE_BADGE) {
            $this->db->update((string) $params["awardCollection"], [
                "_id" => $this->db->MongoId((string) $params["awardId"]),
            ], [
                "\$set" => [
                    "badges." . (string) $params["badgeId"] . ".revoke" => "true",
                    "badges." . (string) $params["badgeId"] . ".revokeReason" => "Time expired",
                ],
            ]);
            $this->db->remove(CronInterface::COLLECTION, [
                "_id" => $this->db->MongoId((string) $params["_id"]),
            ]);
            return;
        }

        if (! is_array($res) && $res) {
            $this->db->remove(CronInterface::COLLECTION, [
                "_id" => $this->db->MongoId((string) $params["_id"]),
            ]);
        } else {
            //something went wrong with the process
            $msg = (is_array($res) && isset($res["msg"])) ? $res["msg"] : "";
            $this->db->update(
                CronInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $params["_id"]),
                ],
                [
                    '$set' => [
                        "status" => CronInterface::STATUS_FAIL,
                        "executedTS" => $this->db->MongoDate(),
                        "errorMsg" => $msg,
                    ],
                ]
            );

            //TODO : add notification to system admin
            //explaining the fail
        }
    }

    /**
     * Process the cron job.
     *
     * @param int $count The number of times to process the cron job (default: 5).
     * @return void
     */
    public function processCron(int $count = 5): void
    {
        $regex = $this->getModelSearchNew()->accentToRegex("fake.");

        $tpl = ["invitation", "passwordRetreive", "inviteYouTo", "askToBecome", "validation", "askdata", "removeData"];

        $where = [
            '$and' => [[
                "status" => CronInterface::STATUS_PENDING,
            ], [
                "to" => [
                    '$ne' => null,
                ],
            ], [
                "to" => [
                    '$not' => $this->db->MongoRegex("/" . $regex . "/i"),
                ],
            ], [
                "tpl" => [
                    '$ne' => "priorisationCTE",
                ],
            ], [
                "tpl" => [
                    '$in' => $tpl,
                ],
            ]],
        ];
        $jobs = $this->db->findAndSort(CronInterface::COLLECTION, $where, [
            'execDate' => 1,
        ], CronInterface::EXEC_COUNT);

        $reste = CronInterface::EXEC_COUNT - (is_countable($jobs) ? count($jobs) : 0);

        if ($reste > 0) {
            $valID = [];
            foreach ($jobs as $key => $value) {
                $valID[] = $this->db->MongoId((string) $key);
            }

            $where2 = [
                '$or' => [[
                    '$and' => [[
                        "status" => CronInterface::STATUS_PENDING,
                    ], [
                        "to" => [
                            '$ne' => null,
                        ],
                    ], [
                        "to" => [
                            '$not' => $this->db->MongoRegex("/" . $regex . "/i"),
                        ],
                    ], [
                        "tpl" => [
                            '$ne' => "priorisationCTE",
                        ],
                    ], [
                        "tpl" => [
                            '$ne' => "actionCTE",
                        ],
                    ], [
                        "tpl" => [
                            '$ne' => "referenceEmailInElement",
                        ],
                    ], [
                        "_id" => [
                            '$nin' => $valID,
                        ],
                    ]],
                ]],
            ];
            $others = $this->db->findAndSort(CronInterface::COLLECTION, $where2, [
                'execDate' => 1,
            ], $reste);
            $jobs = array_merge($jobs, $others);
        }

        //OPEN BADGES
        $whereBadge = [
            '$and' => [
                [
                    'type' => CronInterface::TYPE_BADGE,
                ],
                [
                    'sendOn' => [
                        '$lte' => time(),
                    ],
                ],
            ],
        ];
        $jobsBadge = $this->db->findAndSort(CronInterface::COLLECTION, $whereBadge, [
            'execDate' => 1,
        ], 20);
        $jobs = array_merge($jobs, $jobsBadge);

        foreach ($jobs as $key => $value) {
            //TODO : cumulé plusieur message au meme email
            try {
                $this->processEntry((array) $value);
            } catch (Exception $e) {
                error_log("processCron : " . $e);
            }
        }

        //process future to pending
        $this->processFutureToPending();
    }

    /**
     * Process the update to pending.
     *
     * @return void
     */
    public function processUpdateToPending(): void
    {
        $where = [
            "status" => CronInterface::STATUS_UPDATE,
        ];
        $mails = $this->db->find(CronInterface::COLLECTION, $where);

        foreach ($mails as $key => $value) {
            $set = [
                "status" => CronInterface::STATUS_PENDING,
            ];
            $res = $this->db->update(
                CronInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($key),
                ],
                [
                    '$set' => $set,
                ]
            );
        }
    }

    /**
     * Retrieves the cron data based on the given conditions.
     *
     * @param array $where The conditions to filter the cron data.
     * @return array The cron data that matches the given conditions.
     */
    public function getCron(array $where = []): array
    {
        $cron = $this->db->find(CronInterface::COLLECTION, $where);

        return $cron;
    }

    /**
     * Process future tasks to pending status.
     *
     * @return void
     */
    public function processFutureToPending(): void
    {
        $mails = $this->db->find(CronInterface::COLLECTION, [
            'status' => CronInterface::STATUS_FUTURE,
            'sendOn' => [
                '$lte' => date("Y-m-d"),
            ],
            'type' => [
                '$ne' => CronInterface::TYPE_BADGE,
            ],
        ]);

        foreach ($mails as $key => $value) {
            $set = [
                "status" => CronInterface::STATUS_PENDING,
            ];
            $this->db->update(CronInterface::COLLECTION, [
                "_id" => $this->db->MongoId((string) $key),
            ], [
                '$set' => $set,
            ]);
        }
    }
}
