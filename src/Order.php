<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\OrderInterface;
use PixelHumain\Models\Interfaces\ProductInterface;
use PixelHumain\Models\Interfaces\ServiceInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;
use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;
use PixelHumain\Models\Traits\Interfaces\MailTraitInterface;
use PixelHumain\Models\Traits\Interfaces\OrderItemTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ProductTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ServiceTraitInterface;
use PixelHumain\Models\Traits\MailTrait;
use PixelHumain\Models\Traits\OrderItemTrait;
use PixelHumain\Models\Traits\ProductTrait;
use PixelHumain\Models\Traits\ServiceTrait;

// TODO : est ce que la class est utilisée ?

class Order extends BaseModel implements OrderInterface, DataValidatorTraitInterface, OrderItemTraitInterface, MailTraitInterface, ServiceTraitInterface, ProductTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DataValidatorTrait;
    use OrderItemTrait;
    use MailTrait;
    use ServiceTrait;
    use ProductTrait;

    //TODO Translate
    public static array $orderTypes = [];

    //From Post/Form name to database field name
    public static array $dataBinding = [
        "section" => [
            "name" => "section",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "orderItems" => [
            "name" => "orderItems",
        ],
        "circuit" => [
            "name" => "circuit",
        ],
        "bookingFor" => [
            "name" => "bookingFor",
        ],
        "countOrderItem" => [
            "name" => "countOrderItem",
        ],
        "totalPrice" => [
            "name" => "totalPrice",
        ],
        "currency" => [
            "name" => "currency",
        ],
        "name" => [
            "name" => "name",
        ],
        "description" => [
            "name" => "description",
        ],
        "addresses" => [
            "name" => "addresses",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "price" => [
            "name" => "price",
        ],
        "devise" => [
            "name" => "devise",
        ],
        "contactInfo" => [
            "name" => "contactInfo",
            "rules" => ["required"],
        ],
        "toBeValidated" => [
            "name" => "toBeValidated",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
    }

    /**
     * Retrieves an order by its ID.
     *
     * @param string $id The ID of the order.
     * @return array|null The order data as an array, or null if the order is not found.
     */
    public function getById(string $id): ?array
    {
        $order = $this->db->findOneById(OrderInterface::COLLECTION, $id);
        return $order;
    }

    /**
     * Get a list of orders based on the given conditions.
     *
     * @param array $where The conditions to filter the orders.
     * @return array The list of orders that match the conditions.
     */
    public function getListBy(array $where): array
    {
        $orders = $this->db->find(OrderInterface::COLLECTION, $where);
        return $orders;
    }

    /**
     * Insert a new order into the database.
     *
     * @param array $order The order data.
     * @param string $userId The ID of the user placing the order.
     * @return array The inserted order data.
     */
    public function insert(array $order, string $userId): array
    {
        try {
            $valid = $this->getModelDataValidator()->validate(OrderInterface::CONTROLLER, json_decode(json_encode($order), true), null);
        } catch (CTKException $e) {
            $valid = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        if ($valid["result"]) {
            $order["customerId"] = $userId;
            $order["orderDate"] = $this->db->MongoDate(time());
            $order["created"] = $this->db->MongoDate(time());
            $orderItems = [];
            settype($order["countOrderItem"], "integer");
            settype($order["totalPrice"], "float");
            foreach ($order["orderItems"] as $key => $value) {
                $res = $this->getModelOrderItem()->insert($key, $value, $userId);
                array_push($orderItems, $res["id"]);
            }
            $order["orderItems"] = $orderItems;
            $order = $this->db->insert(OrderInterface::COLLECTION, $order);
            $this->getModelMail()->notifAdminNewReservation($order);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Your payment and reservations are well registred"),
                "order" => $order,
            ];
        } else {
            return [
                "result" => false,
                "error" => "400",
                "msg" => $this->language->t("common", "Something went really bad : " . $valid['msg']),
            ];
        }
    }

    /**
     * Get a list of orders by user.
     *
     * @param array $where The conditions to filter the orders.
     * @return array The list of orders.
     */
    public function getListByUser(array $where): array
    {
        $allOrders = $this->db->findAndSort(OrderInterface::COLLECTION, $where, [
            "created" => -1,
        ]);
        return $allOrders;
    }

    /**
     * Retrieves an order item by its ID.
     *
     * @param string $id The ID of the order item.
     * @return array The order item data.
     */
    public function getOrderItemById(string $id): array
    {
        $order = $this->getById($id);
        $orderItems = [];
        foreach ($order["orderItems"] as $data) {
            $orderItem = $this->getModelOrderItem()->getById($data);
            $orderItems[(string) $orderItem["_id"]] = $orderItem;
        }
        return $orderItems;
    }

    /**
     * Retrieves the order item for invoice by user ID.
     *
     * @param string $id The user ID.
     * @return array The order item for invoice.
     */
    public function getOrderItemForInvoiceByIdUser(string $id): array
    {
        $newOrder = [];
        $order = $this->getListBy([
            "customerId" => $id,
        ]);
        foreach ($order as $key => $value) {
            $arrayId = [];
            foreach ($value["orderItems"] as $keyOrder => $valueOrder) {
                $arrayId[] = $this->db->MongoId($valueOrder);
            }

            $orderItem = $this->getModelOrderItem()->getByArrayId($arrayId, []);
            $newOrderItem = [];
            foreach ($orderItem as $keyItem => $valueItem) {
                if ($valueItem["orderedItemType"] == ServiceInterface::COLLECTION) {
                    $elt = $this->getModelService()->getById($valueItem["orderedItemId"]);
                } elseif ($valueItem["orderedItemType"] == ProductInterface::COLLECTION) {
                    $elt = $this->getModelProduct()->getById($valueItem["orderedItemId"]);
                }
                $newOrderItem[$keyItem] = [
                    "description" => $elt["name"],
                    "quantity" => $valueItem["quantity"],
                    "price" => $elt["price"],
                    "totalPrice" => $valueItem["price"],
                ];
            }

            $value["orderItems"] = $newOrderItem;

            $newOrder[$key] = $value;
        }

        return $newOrder;
    }
}
