<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\PdfInterface;
use PixelHumain\Models\Services\Interfaces\PdfServiceInterface;
use PixelHumain\Models\Traits\BaseModel\PdfTrait;

// TODO : TCPDF class
// TODO : Yii::get('fs')
// TODO : constantes PDF_*

/**
 * Pdf class.
 *
 * This class is used to generate PDF files.
 */
class Pdf extends BaseModel implements PdfInterface, PdfServiceInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use PdfTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validatePdfProperty();
    }

    /**
     * Creates a PDF using the given parameters.
     *
     * @param array $params The parameters for creating the PDF.
     * @return void
     */
    public function createPdf(array $params): void
    {
        $this->createPdf($params);
    }

    // TODO : est ce que c'est encore utilisé ?
    /**
     * Generates a PDF document for a dossier.
     *
     * @param array $params The parameters for generating the PDF.
     * @return string The generated PDF document.
     */
    public function cteDossier(array $params): string
    {
        $html = "<h1>Dossier</h1>";
        $html = (string) $params["html"];
        return $html;
    }

    // TODO : $Parsedown
    // TODO : j'ai commenté la fonction affichage() car $Parsedown n'est pas défini
    // public function affichage($elt, $str)
    // {
    //     $exept = ['_id', 'name'];

    //     foreach ($elt as $key => $value) {
    //         if (! in_array($key, $exept)) {
    //             $str .= '<h4 class="padding-20 blue" style="">' . $key . '</h4>';
    //             if (! empty($value)) {
    //                 if (is_string($value) === true) {
    //                     $str .= '<span>' . $Parsedown->text($value) . '</span>';
    //                 } elseif (is_object($value)) {
    //                     foreach ($value as $keyV => $valV) {
    //                         if (is_string($valV) === true) {
    //                             $str .= '<span>' . $Parsedown->text($valV) . '</span>';
    //                         }
    //                     }
    //                 }
    //             } else {
    //                 $str .= "<i> Pas renseigner </i>";
    //             }
    //             $str .= '<br/>';
    //         }
    //     }

    //     return $str;
    // }
}
