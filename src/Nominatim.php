<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\NominatimInterface;
use PixelHumain\Models\Interfaces\ZoneInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\Interfaces\CityTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ZoneTraitInterface;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\ZoneTrait;

class Nominatim extends BaseModel implements NominatimInterface, SIGTraitInterface, CityTraitInterface, ZoneTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use ParamsTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use SIGTrait;
    use CityTrait;
    use ZoneTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateParamsProperty();
        $this->validateI18nProperty();
    }

    public function getAddressByNominatim($params)
    {
        try {
            $url = "https://nominatim.openstreetmap.org/search?format=json&addressdetails=1";
            if (! empty($params['text'])) {
                $url .= "&q=" . urlencode($params['text']);
                if (! empty($params['countryCode']) && is_string($params['countryCode'])) {
                    $url .= "&country=" . $params['countryCode'];
                } elseif (! empty($params['countryCode']) && is_array($params['countryCode'])) {
                    $stringCC = implode(",", $params['countryCode']);
                    $url .= "&countrycodes=" . $stringCC;
                }

                if (! empty($params['state'])) {
                    $url .= "&state=" . $params['state'];
                }

                if (! empty($params['namedetails']) && $params['namedetails'] === true) {
                    $url .= "&namedetails=1";
                }

                if (! empty($params['extratags']) && $params['extratags'] === true) {
                    $url .= "&extratags=1";
                }

                if (! empty($params['geoShape']) && $params['geoShape'] === true) {
                    $url .= "&polygon_geojson=1";
                }

                $url .= "&email=" . $this->params->get('contactEmail');
                return json_decode($this->getModelSIG()->getUrl($url), true);
            }
            return null;
        } catch (CTKException $e) {
            return null;
        }
    }

    public function getElementByIdNominatim($params)
    {
        try {
            if (! empty($params['type']) && ! empty($params['id'])) {
                $url = "https://nominatim.openstreetmap.org/details.php?format=json&addressdetails=1";
                $url .= "&osmtype=" . $params['type'] . "&osmid=" . $params['id'];

                if (! empty($params['geoShape']) && $params['geoShape'] === true) {
                    $url .= "&polygon_geojson=1";
                }

                $url .= "&email=" . $this->params->get('contactEmail');
                return json_decode($this->getModelSIG()->getUrl($url), true);
            }
            return null;
        } catch (CTKException $e) {
            return null;
        }
    }

    public function getCityByLatLon($lat, $lon)
    {
        try {
            return self::getZoneByLatLon($lat, $lon, "10", true);
        } catch (CTKException $e) {
            return null;
        }
    }

    public function getZoneByLatLon($lat, $lon, $zoom, $geoShape = false)
    {
        try {
            $url = "https://nominatim.openstreetmap.org/reverse?format=json";
            $url .= "&lat=" . urlencode($lat);
            $url .= "&lon=" . urlencode($lon);
            if (! empty($geoShape) && $geoShape === true) {
                $url .= "&polygon_geojson=1";
            }
            $url .= "&extratags=1";
            $url .= "&namedetails=1";
            $url .= "&zoom=" . urlencode($zoom);
            $url .= "&email=" . $this->params->get('contactEmail');

            return json_decode($this->getModelSIG()->getUrl($url), true);
        } catch (CTKException $e) {
            return null;
        }
    }

    public function parseAddress($data, $params)
    {
        $res = [];
        if (! empty($data)) {
            $countryCode = (! empty($params["countryCode"]) ? $params["countryCode"] : null);
            foreach ($data as $key => $val) {
                $city = null;
                $postalCode = (! empty($val["address"]) && ! empty($val["address"]["postcode"]) ? $val["address"]["postcode"] : null);

                $street = "";
                if (! empty($val["address"]["house_number"])) {
                    $street .= $val["address"]["house_number"] . " ";
                }
                if (! empty($val["address"]["road"])) {
                    $street .= $val["address"]["road"];
                }

                $city = $this->getModelCity()->getCityByLatLng($val["lat"], $val["lon"], $postalCode, $countryCode);

                if (! empty($city)) {
                    $res[] = $this->getModelCity()->createLocality($city, $street, $val["lat"], $val["lon"], $postalCode);
                } else {
                    $level1 = $this->getModelZone()->getCountryByCountryCode($countryCode);
                    $address = [
                        '@type' => 'PostalAddress',
                        'addressCountry' => $countryCode,
                        'level1' => (string) $level1["_id"],
                        'level1Name' => $level1["name"],
                        "streetAddress" => $street,
                    ];

                    if (! empty($postalCode)) {
                        $address["postalCode"] = $postalCode;
                    }
                    if (! empty($val["address"]["city"])) {
                        $address["addressLocality"] = $val["address"]["city"];
                    }

                    $address["name"] = $this->getModelCity()->createNameAdress($address);

                    $geo = $this->getModelSIG()->getFormatGeo($val["lat"], $val["lon"]);
                    $geoPosition = $this->getModelSIG()->getFormatGeoPosition($val["lat"], $val["lon"]);

                    $res[] = [
                        "address" => $address,
                        "geo" => $geo,
                        "geoPosition" => $geoPosition,
                        "new" => true,
                        "lat" => $val["lat"],
                        "lon" => $val["lon"],
                        "api" => $val,
                        "countryCode" => $countryCode,
                    ];
                }
            }
        }
        return $res;
    }

    public function createCity($params, $userId, $cityNominatim = null)
    {
        $zoneNominatim = [];
        if (empty($cityNominatim)) {
            $cityNominatim = self::getCityByLatLon($params["lat"], $params["lon"]);
        }

        if (! empty($cityNominatim)) {
            $name = null;
            $fieldName = (! empty($cityNominatim["namedetails"]) ? "namedetails" : "names");

            if (! empty($cityNominatim[$fieldName])) {
                if (! empty($cityNominatim[$fieldName]["name:" . strtolower($this->language->get())])) {
                    $name = $cityNominatim[$fieldName]["name:" . strtolower($this->language->get())];
                } elseif (! empty($cityNominatim[$fieldName]["name"])) {
                    $name = $cityNominatim[$fieldName]["name"];
                }
            }

            $name = (! empty($name) ? $name : $cityNominatim["address"]["city"]);

            $wikidata = (empty($cityNominatim["extratags"]["wikidata"]) ? null : $cityNominatim["extratags"]["wikidata"]);
            $newCity = [
                "name" => $name,
                "alternateName" => mb_strtoupper($name),
                "country" => $params['countryCode'],
                "geo" => $this->getModelSIG()->getFormatGeo($cityNominatim["lat"], $cityNominatim["lon"]),
                "geoPosition" => $this->getModelSIG()->getFormatGeoPosition($cityNominatim["lat"], $cityNominatim["lon"]),
                "osmID" => $cityNominatim["osm_id"],
            ];
            $newCity["modified"] = $this->db->MongoDate(time());
            $newCity["updated"] = time();
            $newCity["creator"] = $userId;
            $newCity["created"] = time();
            $newCity["new"] = true;

            if (! empty($cityNominatim["geojson"])) {
                $newCity["geoShape"] = $cityNominatim["geojson"];
            } elseif (! empty($cityNominatim["geometry"])) {
                $newCity["geoShape"] = $cityNominatim["geometry"];
            }

            if (! empty($wikidata)) {
                $newCity = $this->getModelCity()->getCitiesWithWikiData($wikidata, $newCity);
            }

            if (empty($newCity["insee"])) {
                $newCity["insee"] = $cityNominatim["osm_id"] . "*" . $params['countryCode'];
            }

            if (empty($newCity["postalCodes"])) {
                $newCity["postalCodes"] = [];
            }

            $postalCodes = [];
            if (! empty($newCity["postalCodes"])) {
                foreach ($newCity["postalCodes"] as $keyCP => $cp) {
                    $newCP = [];
                    $newCP["postalCode"] = $cp["postalCode"];
                    $newCP["name"] = $cp["name"];
                    $newCP["geo"] = $cp["geo"];
                    $newCP["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($cp["geo"]["latitude"], $cp["geo"]["longitude"]);
                    $postalCodes[] = $newCP;
                }
            }
            $newCity["postalCodes"] = $postalCodes;

            $level1 = $this->getModelZone()->getCountryByCountryCode($newCity["country"]);
            $newCity["level1"] = (string) $level1["_id"];
            $newCity["level1Name"] = $this->getModelZone()->getNameOrigin($newCity["level1"]);

            $levelParents = [
                "level1" => $newCity["level1"],
                "level1Name" => $newCity["level1Name"],
            ];

            if (! empty($cityNominatim["address"]["state"])) {
                $level2 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["state"], "2", $newCity["country"]);
                if (! empty($level2) && ! empty($level2["_id"])) {
                    $newCity["level2"] = (string) $level2["_id"];
                    $newCity["level2Name"] = $this->getModelZone()->getNameOrigin($newCity["level2"]);
                    $levelParents["level2"] = $newCity["level2"];
                    $levelParents["level2Name"] = $newCity["level2Name"];
                } else {
                    $level3 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["state"], "3", $newCity["country"]);
                    if (empty($level3)) {
                        $level3 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "3", $levelParents);
                    }

                    if (! empty($level3["_id"])) {
                        $newCity["level3"] = (string) $level3["_id"];
                        $newCity["level3Name"] = $this->getModelZone()->getNameOrigin($newCity["level3"]);
                        $levelParents["level3"] = $newCity["level3"];
                        $levelParents["level3Name"] = $newCity["level3Name"];
                    }
                }
            }

            if (! empty($cityNominatim["address"]["county"])) {
                $level4 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["county"], "4", $newCity["country"]);
                if (empty($level4)) {
                    $level4 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "4", $levelParents);
                }

                if (! empty($level4["_id"])) {
                    $newCity["level4"] = (string) $level4["_id"];
                    $newCity["level4Name"] = $this->getModelZone()->getNameOrigin($newCity["level4"]);
                    $levelParents["level4"] = $newCity["level4"];
                    $levelParents["level4Name"] = $newCity["level4Name"];
                }
            }

            $translate = [];
            $info = [];
            $origin = $newCity["name"];
            if (! empty($zoneNominatim[$fieldName])) {
                foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                    $arrayName = explode(":", $keyName);
                    if (! empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName) {
                        $translate[strtoupper($arrayName[1])] = $valueName;
                    }
                }
            }
            //Yii::get('mongodb')->selectCollection(City::COLLECTION)->insert($newCity);
            $newCity = $this->db->insert(CityInterface::COLLECTION, $newCity);
            $info["countryCode"] = $params['countryCode'];
            $info["parentId"] = (string) $newCity["_id"];
            $info["parentType"] = CityInterface::COLLECTION;
            $info["translates"] = $translate;
            $info["origin"] = $origin;
            //Yii::get('mongodb')->selectCollection(Zone::TRANSLATE)->insert($info);
            $info = $this->db->insert(ZoneInterface::TRANSLATE, $info);
            $this->db->update(
                CityInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $newCity["_id"]),
                ],
                [
                    '$set' => [
                        "translateId" => (string) $info["_id"],
                    ],
                ]
            );

            $params["address"] = $this->getModelCity()->createAddressByCity($newCity, $params['address']['streetAddress'], (! empty($params['address']['postalCode']) ? $params['address']['postalCode'] : null));

            $res = [
                "address" => $params["address"],
                "geo" => $params["geo"],
                "geoPosition" => $params["geoPosition"],
            ];
            return $res;
        }
    }

    public function createLevel($lat, $lon, $countryCode, $level, $levelParents, $zoneNominatim = null)
    {
        $zone = [];

        if (empty($zoneNominatim)) {
            $zoneNominatim = self::getZoneByLatLon($lat, $lon, NominatimInterface::ZOOM_BY_LEVEL[$level]);
        }

        if (! empty($zoneNominatim)) {
            $fieldName = (! empty($zoneNominatim["namedetails"]) ? "namedetails" : "names");

            if (! empty($zoneNominatim[$fieldName]) &&
                ! empty($zoneNominatim[$fieldName]["name"])) {
                $zone["name"] = $zoneNominatim[$fieldName]["name"];
            } else {
                $zone["name"] = $zoneNominatim["address"][NominatimInterface::NAME_BY_LEVEL[$level]];
            }

            $zone["countryCode"] = $countryCode;
            $zone["level"] = [$level];
            if (! empty($levelParents)) {
                if (! empty($levelParents["level1"])) {
                    $zone["level1"] = $levelParents["level1"];
                    $zone["level1Name"] = $levelParents["level1Name"];
                }
                if (! empty($levelParents["level2"])) {
                    $zone["level2"] = $levelParents["level2"];
                    $zone["level2Name"] = $levelParents["level2Name"];
                }
                if (! empty($levelParents["level3"])) {
                    $zone["level3"] = $levelParents["level3"];
                    $zone["level3Name"] = $levelParents["level3Name"];
                }
            }

            $zone["geo"] = $this->getModelSIG()->getFormatGeo($zoneNominatim["lat"], $zoneNominatim["lon"]);
            $zone["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($zoneNominatim["lat"], $zoneNominatim["lon"]);

            if (! empty($zoneNominatim["osm_id"])) {
                $zone["osmID"] = $zoneNominatim["osm_id"];
            }
            if (! empty($zoneNominatim["extratags"]["wikidata"])) {
                $zone["wikidataID"] = $zoneNominatim["extratags"]["wikidata"];
            }

            $translate = [];
            $info = [];
            $origin = $zone["name"];
            if (! empty($zoneNominatim[$fieldName])) {
                foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                    $arrayName = explode(":", $keyName);
                    if (! empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName) {
                        $translate[strtoupper($arrayName[1])] = $valueName;
                    }
                }
            }

            $zone = $this->db->insert(ZoneInterface::COLLECTION, $zone);
            $info["countryCode"] = $countryCode;
            $info["parentId"] = (string) $zone["_id"];
            $info["parentType"] = ZoneInterface::COLLECTION;
            $info["translates"] = $translate;
            $info["origin"] = $origin;
            $zone["translate"] = $info;

            $info = $this->db->insert(ZoneInterface::TRANSLATE, $info);
            $this->db->update(
                ZoneInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $zone["_id"]),
                ],
                [
                    '$set' => [
                        "translateId" => (string) $info["_id"],
                    ],
                ]
            );
        }

        return $zone;
    }

    public function createCityByNominatim($params, $userId, $cityNominatim = null)
    {
        $zoneNominatim = [];
        if (empty($cityNominatim)) {
            $cityNominatim = self::getCityByLatLon($params["lat"], $params["lon"]);
        }

        if (! empty($cityNominatim)) {
            $name = null;
            $fieldName = (! empty($cityNominatim["namedetails"]) ? "namedetails" : "names");

            if (! empty($cityNominatim[$fieldName])) {
                if (! empty($cityNominatim[$fieldName]["name:" . strtolower($this->language->get())])) {
                    $name = $cityNominatim[$fieldName]["name:" . strtolower($this->language->get())];
                } elseif (! empty($cityNominatim[$fieldName]["name"])) {
                    $name = $cityNominatim[$fieldName]["name"];
                }
            }

            $name = (! empty($name) ? $name : $cityNominatim["address"]["city"]);

            $wikidata = (empty($cityNominatim["extratags"]["wikidata"]) ? null : $cityNominatim["extratags"]["wikidata"]);
            $newCity = [
                "name" => $name,
                "alternateName" => mb_strtoupper($name),
                "country" => $params['countryCode'],
                "geo" => $this->getModelSIG()->getFormatGeo($cityNominatim["lat"], $cityNominatim["lon"]),
                "geoPosition" => $this->getModelSIG()->getFormatGeoPosition($cityNominatim["lat"], $cityNominatim["lon"]),
                "osmID" => $cityNominatim["osm_id"],
            ];
            $newCity["modified"] = $this->db->MongoDate(time());
            $newCity["updated"] = time();
            $newCity["creator"] = $userId;
            $newCity["created"] = time();
            $newCity["new"] = true;

            if (! empty($cityNominatim["geojson"])) {
                $newCity["geoShape"] = $cityNominatim["geojson"];
            } elseif (! empty($cityNominatim["geometry"])) {
                $newCity["geoShape"] = $cityNominatim["geometry"];
            }

            if (! empty($wikidata)) {
                $newCity = $this->getModelCity()->getCitiesWithWikiData($wikidata, $newCity);
            }

            if (empty($newCity["insee"])) {
                $newCity["insee"] = $cityNominatim["osm_id"] . "*" . $params['countryCode'];
            }

            if (empty($newCity["postalCodes"])) {
                $newCity["postalCodes"] = [];
            }

            $postalCodes = [];
            if (! empty($newCity["postalCodes"])) {
                foreach ($newCity["postalCodes"] as $keyCP => $cp) {
                    $newCP = [];
                    $newCP["postalCode"] = $cp["postalCode"];
                    $newCP["name"] = $cp["name"];
                    $newCP["geo"] = $cp["geo"];
                    $newCP["geoPosition"] = $this->getModelSIG()->getFormatGeoPosition($cp["geo"]["latitude"], $cp["geo"]["longitude"]);
                    $postalCodes[] = $newCP;
                }
            }
            $newCity["postalCodes"] = $postalCodes;

            $level1 = $this->getModelZone()->getCountryByCountryCode($newCity["country"]);
            $newCity["level1"] = (string) $level1["_id"];
            $newCity["level1Name"] = $this->getModelZone()->getNameOrigin($newCity["level1"]);

            $levelParents = [
                "level1" => $newCity["level1"],
                "level1Name" => $newCity["level1Name"],
            ];

            if (! empty($cityNominatim["address"]["state"])) {
                $level2 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["state"], "2", $newCity["country"]);
                if (! empty($level2) && ! empty($level2["_id"])) {
                    $newCity["level2"] = (string) $level2["_id"];
                    $newCity["level2Name"] = $this->getModelZone()->getNameOrigin($newCity["level2"]);
                    $levelParents["level2"] = $newCity["level2"];
                    $levelParents["level2Name"] = $newCity["level2Name"];
                } else {
                    $level3 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["state"], "3", $newCity["country"]);
                    if (empty($level3)) {
                        $level3 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "3", $levelParents);
                    }

                    if (! empty($level3["_id"])) {
                        $newCity["level3"] = (string) $level3["_id"];
                        $newCity["level3Name"] = $this->getModelZone()->getNameOrigin($newCity["level3"]);
                        $levelParents["level3"] = $newCity["level3"];
                        $levelParents["level3Name"] = $newCity["level3Name"];
                    }
                }
            }

            if (! empty($cityNominatim["address"]["county"])) {
                $level4 = $this->getModelZone()->getLevelByNameAndCountry($cityNominatim["address"]["county"], "4", $newCity["country"]);
                if (empty($level4)) {
                    $level4 = self::createLevel($cityNominatim["lat"], $cityNominatim["lon"], $params['countryCode'], "4", $levelParents);
                }

                if (! empty($level4["_id"])) {
                    $newCity["level4"] = (string) $level4["_id"];
                    $newCity["level4Name"] = $this->getModelZone()->getNameOrigin($newCity["level4"]);
                    $levelParents["level4"] = $newCity["level4"];
                    $levelParents["level4Name"] = $newCity["level4Name"];
                }
            }

            $translate = [];
            $info = [];
            $origin = $newCity["name"];
            if (! empty($zoneNominatim[$fieldName])) {
                foreach ($zoneNominatim[$fieldName] as $keyName => $valueName) {
                    $arrayName = explode(":", $keyName);
                    if (! empty($arrayName[1]) && $arrayName[0] == "name" && strlen($arrayName[1]) == 2 && $origin != $valueName) {
                        $translate[strtoupper($arrayName[1])] = $valueName;
                    }
                }
            }

            $newCity = $this->db->insert(CityInterface::COLLECTION, $newCity);
            $info["countryCode"] = $params['countryCode'];
            $info["parentId"] = (string) $newCity["_id"];
            $info["parentType"] = CityInterface::COLLECTION;
            $info["translates"] = $translate;
            $info["origin"] = $origin;

            $info = $this->db->insert(ZoneInterface::TRANSLATE, $info);
            $this->db->update(
                CityInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId((string) $newCity["_id"]),
                ],
                [
                    '$set' => [
                        "translateId" => (string) $info["_id"],
                    ],
                ]
            );

            return $newCity;
        }
    }
}


/*

Test Nominatim

Reverse :
    BE :  https://nominatim.openstreetmap.org/reverse?format=json&lat=43.29441800&lon=5.35999000&extratags=1&zoom=10

    IT : https://nominatim.openstreetmap.org/reverse?format=json&lat=41.9076293&lon=12.4977726&polygon_geojson=1&extratags=1&namedetails=1&zoom=10

        https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&q=Via+Flavia%2C+98&countrycodes=IT

        https://nominatim.openstreetmap.org/search?q=14+Rue+Albert+Briand&countrycodes=PM

*/
