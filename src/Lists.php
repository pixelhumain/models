<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Adapter\InflectorHelperAdapter;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\ListsInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class Lists extends BaseModel implements ListsInterface
{
    use DbTrait;

    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    public static array $dataBinding = [
        "name" => [
            "name" => "name",
        ],
        "form" => [
            "form" => "form",
        ],
        "list" => [
            "list" => "list",
        ],
        "type" => [
            "name" => "type",
        ],
        "subtype" => [
            "name" => "subtype",
        ],
        "collection" => [
            "name" => "collection",
        ],
        "description" => [
            "name" => "description",
        ],
        "parent" => [
            "name" => "parent",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "media" => [
            "name" => "media",
        ],
        "urls" => [
            "name" => "urls",
        ],
        "medias" => [
            "name" => "medias",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "structags" => [
            "name" => "structags",
        ],
        "shortDescription" => [
            "name" => "shortDescription",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "source" => [
            "name" => "source",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
    ];

    /**
     * Ajoute une nouvelle entrée à la liste spécifiée.
     *
     * @param string $listName Le nom de la liste.
     * @param string $entryKey La clé de l'entrée.
     * @param array $extra Les données supplémentaires de l'entrée (facultatif).
     * @return void
     */
    public function newEntry(string $listName, string $entryKey, array $extra = []): void
    {
        //check if usage exist otherwise create it
        $slug = InflectorHelperAdapter::slugify($entryKey);
        $exist = $this->db->findOne(ListsInterface::COLLECTION, [
            "name" => $listName,
            "list." . $slug => [
                '$exists' => 1,
            ],
        ]);

        if (! $exist) {
            $params = array_merge([
                "name" => $entryKey,
            ], $extra);
            $this->db->update(
                ListsInterface::COLLECTION,
                [
                    "name" => $listName,
                ],
                [
                    '$set' => [
                        "list." . $slug => $params,
                    ],
                ]
            );
        }
    }

    /**
     * Retrieves the specified lists.
     *
     * @param array $listNames The names of the lists to retrieve.
     * @return array The retrieved lists.
     */
    public function get(array $listNames): array
    {
        $lists = $this->db->find(ListsInterface::COLLECTION, [
            "name" => [
                '$in' => $listNames,
            ],
        ], ["name", "list"]);
        $res = [];
        foreach ($lists as $key => $value) {
            $res[$value["name"]] = $value["list"];
        }
        return $res;
    }

    /**
     * Retrieves a list by its name.
     *
     * @param string $name The name of the list.
     * @return array The list matching the given name.
     */
    public function getListByName(string $name): array
    {
        $res = [];
        //The tags are found in the list collection, key tags
        $list = $this->db->findOne(ListsInterface::COLLECTION, [
            "name" => $name,
        ], ['list']);

        if (! empty($list['list'])) {
            $res = $list['list'];
        } else {
            throw new CTKException("Impossible to find the list name " . $name);
        }

        return $res;
    }

    /**
     * Retrieves the labels for a given list name and IDs.
     *
     * @param array $listName The name of the list.
     * @param array $ids The IDs of the items.
     * @return array The labels corresponding to the given list name and IDs.
     */
    public function getLabels(array $listName, array $ids): array
    {
        $listValue = $this->get($listName);
        $res = [];
        foreach ($ids as $id) {
            if (! empty($listValue[(string) $id])) {
                array_push($res, $listValue[(string) $id]);
            }
        }
        return $res;
    }
}
