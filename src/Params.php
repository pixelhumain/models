<?php

namespace PixelHumain\Models;

use ArrayAccess;
use Exception;
use PixelHumain\Models\Interfaces\ParamsInterface;

/**
 * Represents a container for storing and accessing parameters.
 */
class Params implements ParamsInterface, ArrayAccess
{
    protected $params;

    /**
     * Constructs a new Params object with the specified parameters.
     *
     * @param array $params The parameters to be stored.
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Magic method to get the value of a property.
     *
     * @param string $key The name of the property to get.
     * @return mixed The value of the property.
     */
    public function __get(string $key)
    {
        return $this->params[$key];
    }

    /**
     * Set the value of a parameter.
     *
     * @param string $key The key of the parameter.
     * @param mixed $value The value of the parameter.
     * @return void
     */
    public function __set(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * Checks if a property is set.
     *
     * @param string $key The name of the property to check.
     * @return bool Returns true if the property is set, false otherwise.
     */
    public function __isset(string $key): bool
    {
        return isset($this->params[$key]);
    }

    /**
     * Unsets a specific property of the Params object.
     *
     * @param string $key The name of the property to unset.
     * @return void
     */
    public function __unset(string $key): void
    {
        unset($this->params[$key]);
    }

    /**
     * Get the value of a parameter by its key.
     *
     * @param string $key The key of the parameter.
     * @return mixed The value of the parameter.
     */
    public function get(string $key)
    {
        return $this->params[$key];
    }

    /**
     * Set the value of a parameter by its key.
     *
     * @param string $key The key of the parameter.
     * @param mixed $value The value to set.
     * @return void
     */
    public function set(string $key, $value): void
    {
        $this->params[$key] = $value;
    }

    /**
     * Check if a parameter exists by its key.
     *
     * @param string $key The key of the parameter.
     * @return bool True if the parameter exists, false otherwise.
     */
    public function has(string $key): bool
    {
        return isset($this->params[$key]);
    }

    public function remove(string $key): void
    {
        throw new Exception("Cannot modify the configuration.");
    }

    /**
     * Checks if a parameter with the specified offset exists.
     *
     * @param mixed $offset The offset to check.
     * @return bool True if the parameter exists, false otherwise.
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset): bool
    {
        return isset($this->params[$offset]);
    }

    /**
     * Retrieves the value of the parameter with the specified offset.
     *
     * @param mixed $offset The offset of the parameter to retrieve.
     * @return mixed|null The value of the parameter if it exists, null otherwise.
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->params[$offset] ?? null;
    }

    /**
     * Sets the value of the parameter with the specified offset.
     *
     * @param mixed $offset The offset of the parameter to set.
     * @param mixed $value The value to set.
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        $this->params[$offset] = $value;
    }

    /**
     * Throws an exception to prevent deleting the configuration.
     *
     * @param mixed $offset The offset of the parameter to unset.
     * @throws Exception Always throws an exception.
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        throw new Exception("Cannot modify the configuration.");
    }
}
