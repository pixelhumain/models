<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\PreferenceInterface;

use PixelHumain\Models\Traits\ActivityStreamTrait;
use PixelHumain\Models\Traits\AuthorisationTrait;
use PixelHumain\Models\Traits\BadgeTrait;

use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\CityTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\LinkTrait;

class Preference extends BaseModel implements PreferenceInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CityTrait;
    use LinkTrait;
    use AuthorisationTrait;
    use BadgeTrait;
    use ElementTrait;
    use ActivityStreamTrait;
    use CostumTrait;

    /**
     * @var string[]
     */
    public static $listPref = ["email", "locality", "phone", "directory", "birthDate"];

    /**
    * Configure the model with the given configuration.
    *
    * @param array $config The configuration array.
    * @return void
    */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Retrieves preferences by type ID and type.
     *
     * @param string $id The ID of the preference.
     * @param string $type The type of the preference.
     * @return array The preferences matching the given ID and type.
     */
    public function getPreferencesByTypeId(string $id, string $type): array
    {
        if ($type == CityInterface::COLLECTION) {
            $city = $this->getModelCity()->getByUnikey($id);
            $id = $city["_id"];
        }
        $entity = $this->db->findOneById($type, $id, [
            "preferences" => 1,
        ]);
        $preferences = empty($entity["preferences"]) ? [] : $entity["preferences"];
        return $preferences;
    }

    public function updatePreferences($id, $type, $preferenceName = null, $preferenceValue = null, $preferenceSubName = null)
    {
        $action = '$set';
        $update = [];
        if (! @$preferenceName || empty($preferenceName)) {
            $action = '$unset';
            $update = [
                "preferences.seeExplanations" => "",
            ];
        } else {
            if ($preferenceValue === "true" || $preferenceValue == "default") {
                $action = '$unset';
                $preferenceValue = "";
            }
            $update = [
                "preferences." . $preferenceName => $preferenceValue,
            ];
            if (! empty($preferenceSubName)) {
                $update = [
                    "preferences." . $preferenceName . "." . $preferenceSubName => $preferenceValue,
                ];
            }
        }

        $this->db->update(
            $type,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                $action => $update,
            ]
        );
        $res = [
            "result" => true,
            "msg" => $this->language->t("common", "Your request is well updated"),
        ];
        return $res;
    }

    public function updateSettings($userId, $params)
    {
        $settings = [
            "name" => $params["settings"],
            "value" => $params["value"],
        ];
        $parentId = $params["id"];
        $parentType = $params["type"];
        $childId = (@$params["childId"]) ? $params["childId"] : $this->session->getUserId();
        $childType = (@$params["childType"]) ? $params["childType"] : PersonInterface::COLLECTION;
        $parentConnectAs = LINK::$linksTypes[$parentType][$childType];
        $childConnectAs = LINK::$linksTypes[$childType][$parentType];
        if ($settings["value"] == "default") {
            //Add notification - email label in parent link
            $this->getModelLink()->disconnect($parentId, $parentType, $childId, $childType, $this->session->getUserId(), $parentConnectAs, null, $settings);
            //Add notification - email label in child link
            $this->getModelLink()->disconnect($childId, $childType, $parentId, $parentType, $this->session->getUserId(), $childConnectAs, null, $settings);
        } else {
            //Add notification - email label in parent link
            $this->getModelLink()->connect($parentId, $parentType, $childId, $childType, $this->session->getUserId(), $parentConnectAs, null, null, null, null, null, $settings);
            //Add notification - email label in child link
            $this->getModelLink()->connect($childId, $childType, $parentId, $parentType, $this->session->getUserId(), $childConnectAs, null, null, null, null, null, $settings);
        }
        return [
            "result" => true,
            "msg" => $this->language->t("common", "Your request is well updated"),
        ];
    }

    public function beforeUpdateElement($elt, $params)
    {
        if (isset($params["preferences"]) && isset($elt["preferences"])) {
            if (isset($elt["preferences"]["private"]) && ! isset($params["preferences"]["private"])) {
                $params["preferences"]["private"] = $elt["preferences"]["private"];
            }
            if (isset($elt["preferences"]["isOpenData"]) && ! isset($params["preferences"]["isOpenData"])) {
                $params["preferences"]["isOpenData"] = $elt["preferences"]["isOpenData"];
            }
            if (isset($elt["preferences"]["isOpenEdition"]) && ! isset($params["preferences"]["isOpenEdition"])) {
                $params["preferences"]["isOpenEdition"] = $elt["preferences"]["isOpenEdition"];
            }
        }
        return $params;
    }

    public function updateConfidentiality($id, $type, $param)
    {
        $preferences = [];
        $id = $param["idEntity"];
        $context = $this->element->getElementSimpleById($id, $type, null, ["preferences", "hasRC", "slug", "name"]);

        $setType = $param["type"];
        $setValue = $param["value"];
        $res = [];

        $publicFields = [];
        $privateFields = [];

        if (@$context["preferences"]["publicFields"] && ! empty($context["preferences"]["publicFields"])) {
            $publicFields = $context["preferences"]["publicFields"];
            foreach ($publicFields as $key => $value) {
                if ($setType === $value) {
                    array_splice($publicFields, $key, 1);
                }
            }
        }

        if (@$context["preferences"]["privateFields"] && ! empty($context["preferences"]["privateFields"])) {
            $privateFields = $context["preferences"]["privateFields"];
            foreach ($privateFields as $key => $value) {
                if ($setType === $value) {
                    array_splice($privateFields, $key, 1);
                }
            }
        }

        if ($setValue == "public") {
            array_push($publicFields, $setType);
        }
        if ($setValue == "private") {
            array_push($privateFields, $setType);
        }
        if ($setValue == "true") {
            $setValue = true;
        } elseif ($setValue == "false") {
            $setValue = false;
        }
        if (! empty($privateFields)) {
            $preferences["privateFields"] = $privateFields;
        }
        if (! empty($publicFields)) {
            $preferences["publicFields"] = $publicFields;
        }

        if ($setType == "isOpenData") {
            $preferences["isOpenData"] = $setValue;
        } else {
            $preferences["isOpenData"] = ((empty($context["preferences"]["isOpenData"])) ? false : true);
        }

        if ($setType == "badge") {
            $preferences["badge"] = $setValue;
        } else {
            $preferences["badge"] = ((empty($context["preferences"]["badge"])) ? false : true);
        }

        if ($setType == "sendMail") {
            $preferences["sendMail"] = $setValue;
        } else {
            $preferences["sendMail"] = ((empty($context["preferences"]["sendMail"])) ? false : [
                'source' => [],
            ]);
        }

        if ($setType == "crowdfunding") {
            $preferences["crowdfunding"] = $setValue;
        }

        if ($setType == "feedback") {
            $preferences["feedback"] = $setValue;
        }

        if ($setType == "activitypub") {
            $preferences["activitypub"] = $setValue;
        } else {
            $preferences["activitypub"] = ((empty($context["preferences"]["activitypub"])) ? false : true);
        }

        if ($setType == "private") {
            $preferences["private"] = $setValue;

            if (@$context["hasRC"]) {
                $roomType = (@$setValue === true) ? "group" : "channel";
                $name = @$context["slug"] ? $context["slug"] : $context["name"];
                $this->settingChat($name, $roomType, $type, $id);
            }
        } else {
            if (! empty($context["preferences"]["private"])) {
                $preferences["private"] = true;
                if (@$context["hasRC"]) {
                    $roomType = (@$preferences["private"] === true) ? "group" : "channel";
                    $name = @$context["slug"] ? $context["slug"] : $context["name"];
                    $this->settingChat($name, $roomType, $type, $id);
                }
            }
        }
        if ($setType == "isOpenEdition") {
            $preferences["isOpenEdition"] = $setValue;
        } else {
            if ($type != PersonInterface::COLLECTION) {
                $preferences["isOpenEdition"] = (empty($context["preferences"]["isOpenEdition"]) ? false : $context["preferences"]["isOpenEdition"]);
            }
        }

        if (self::isOpenData($preferences)) {
            $this->getModelBadge()->addAndUpdateBadges("opendata", $id, $type);
        } else {
            $this->getModelBadge()->delete("opendata", $id, $type);
        }

        $result = $this->db->update(
            $type,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    "preferences" => $preferences,
                ],
            ]
        );

        $this->getModelActivityStream()->saveActivityHistory(ActStrInterface::VERB_UPDATE, $id, $type, $setType, $this->valueActivityStream($setValue));
        $res = [
            "result" => true,
            "msg" => $this->language->t("common", "Confidentiality param well updated"),
        ];
        return $res;
    }

    public function isOpenData($preferences)
    {
        $isOpenData = false;
        if (! empty($preferences["isOpenData"]) &&
            ($preferences["isOpenData"] === true || $preferences["isOpenData"] === "true")) {
            $isOpenData = true;
        }
        return $isOpenData;
    }

    public function isPublicElement($preferences)
    {
        $costum = $this->getModelCostum()->getCostum();
        if (isset($costum["preferences"]) && isset($costum["preferences"]["allSeePrivate"])) {
            return true;
        }
        $isPublic = true;
        if (@$preferences["private"]) {
            $isPublic = false;
        } elseif (isset($preferences["toBeValidated"])) {
            if (! empty($costum) && isset($preferences["toBeValidated"][$costum["contextSlug"]])) {
                $isPublic = false;
            }
        }

        return $isPublic;
    }

    public function isActivitypubActivate($preferences)
    {
        return (
            isset($preferences["activitypub"]) &&
            (
                $preferences["activitypub"] === true ||
                $preferences["activitypub"] == "true"
            )
        );
    }

    public function valueActivityStream($setValue)
    {
        $value = "";
        if ($setValue == true) {
            $value = $this->language->t("common", "True");
        } elseif ($setValue == false) {
            $value = $this->language->t("common", "False");
        } else {
            $value = $setValue;
        }

        return $value;
    }

    public function showPreference($element, $elementType, $namePref, $userId)
    {
        $result = false;

        $eltId = ((! empty($element["_id"])) ? (string) $element["_id"] : (string) $element["id"]);

        if (empty($element["preferences"])) {
            $element["preferences"] = $this->getPreferencesByTypeId($eltId, $elementType);
        }
        //mask
        if ($elementType == PersonInterface::COLLECTION && $eltId == $userId) {
            $result = true;
        }//public
        elseif ($result == false && $this->isPublic($element, $namePref)) {
            $result = true;
        }//private
        elseif ($result == false &&
                @$element["preferences"]["privateFields"] &&
                in_array($namePref, $element["preferences"]["privateFields"]) &&
                ($eltId == $userId || $this->getModelLink()->isLinked($eltId, $elementType, $userId, @$element["links"]))) {
            $result = true;
        }
        return $result;
    }

    public function isPublic($element, $namePref)
    {
        $result = false;
        if (@$element["preferences"]["publicFields"] && in_array($namePref, $element["preferences"]["publicFields"])) {
            $result = true;
        }
        return $result;
    }

    public function settingChat($name, $roomType, $type, $id)
    {
        $isAdmin = $this->getModelAuthorisation()->canEditItem($this->session->getUserId(), $type, $id);
        if ($roomType == "channel") {
            $path = "/channel/" . $name;
            $group = $this->getRocketChat()->createGroup($name, $roomType, $this->session->getUserUsername(), null, $isAdmin);
        } else {
            $path = "/group/" . $name;
            $group = null;
            if ($isAdmin ||
                $this->getModelLink()->isLinked($id, $type, $this->session->getUserId())) {
                $group = $this->getRocketChat()->createGroup($name, null, $this->session->getUserUsername(), null, $isAdmin);
            }
        }

        if ($group != null && (@$group->create->channel->_id || @$group->create->group->_id)) {
            $result = $this->db->update(
                $type,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                [
                    '$set' => [
                        "hasRC" => true,
                    ],
                    '$addToSet' => [
                        "tools.chat.int" => [
                            "name" => $name,
                            "url" => $path,
                        ],
                    ],
                ]
            );
        }

        if ($group != null && @$group->settype->success) {
            $result = $this->db->update(
                $type,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                [
                    '$set' => [
                        "hasRC" => true,
                    ],
                ]
            );
            $result = $this->db->update(
                $type,
                [
                    "_id" => $this->db->MongoId($id),
                    "tools.chat.int.name" => $name,
                ],
                [
                    '$set' => [
                        "tools.chat.int.$.url" => $path,
                    ],
                ]
            );
        }
    }

    public function getAppConfig($appKey, $type = "", $id = "", $moreConfig = [])
    {
        $id = (empty($id) && ! empty($this->session->getUserId())) ? $this->session->getUserId() : $id;
        $params = [];
        if (! empty($id)) {
            $prefUser = $this->getPreferencesByTypeId($id, $type);
            if (! empty($prefUser[$appKey])) {
                $params = $prefUser[$appKey];
            }
            if (! empty($moreConfig)) {
                array_push($params, $moreConfig);
            }
            $ord = 1;
            foreach ($params as $k => $v) {
                if (empty($v["order"])) {
                    $params[$k]["order"] = $ord;
                } else {
                    foreach ($params as $i => $val) {
                        if ($k != $i && isset($val["order"]) && $val["order"] >= $v["order"]) {
                            $params[$i]["order"]++;
                        }
                    }
                }
                $ord++;
            }
        }

        return $params;
    }

    /**
     * Check if the edition is open based on the given preferences.
     *
     * @param array|null $preferences The preferences to check.
     * @return bool Returns true if the edition is open, false otherwise.
     */
    public function isOpenEdition(?array $preferences): bool
    {
        $isOpenData = false;
        if (isset($preferences["isOpenEdition"]) &&
            ($preferences["isOpenEdition"] === true || $preferences["isOpenEdition"] === "true")) {
            $isOpenData = true;
        }
        return $isOpenData;
    }

    /**
     * Initializes the preferences for a given type.
     *
     * @param string $type The type of preferences to initialize.
     * @return array The initialized preferences.
     */
    public function initPreferences(string $type): array
    {
        $preferences = [];
        $preferences["isOpenData"] = true;
        $preferences["sendMail"] = false;
        if ($type == PersonInterface::COLLECTION) {
            $preferences["isOpenData"] = false;
            $preferences["publicFields"] = ["locality", "directory"];
            $preferences["privateFields"] = ["birthDate", "email", "phone"];
        } else {
            $preferences["isOpenEdition"] = true;
        }
        return $preferences;
    }

    /**
     * Clear the given element by preference.
     *
     * @param array $element The element to clear.
     * @param string $elementType The type of the element.
     * @param string $userId The user ID.
     * @return array The cleared element.
     */
    public function clearByPreference(array $element, string $elementType, string $userId): array
    {
        $eltId = ((! empty($element["_id"])) ? (string) $element["_id"] : (string) $element["id"]);
        if (! empty($eltId)) {
            if (empty($element["preferences"])) {
                $element["preferences"] = $this->getPreferencesByTypeId($eltId, $elementType);
            }

            foreach (self::$listPref as $key => $namePref) {
                if (! $this->showPreference($element, $elementType, $namePref, $userId)) {
                    if ($namePref == "locality") {
                        unset($element["address"]);
                        unset($element["geo"]);
                        unset($element["geoPosition"]);
                    } elseif ($namePref == "phone") {
                        unset($element["telephone"]);
                    } elseif ($namePref == "directory") {
                        unset($element["links"]);
                    } else {
                        unset($element[$namePref]);
                    }
                }
            }
        }

        return $element;
    }
}
