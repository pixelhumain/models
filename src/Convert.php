<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ConvertInterface;
use PixelHumain\Models\Interfaces\EventInterface;
use PixelHumain\Models\Traits\ApplicationTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\ImportTrait;
use PixelHumain\Models\Traits\Interfaces\ApplicationTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ImportTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SIGTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TranslateTraitInterface;
use PixelHumain\Models\Traits\SIGTrait;
use PixelHumain\Models\Traits\TranslateTrait;
use PixelHumain\Models\Translates\TranslateDatagouvToPh;
use PixelHumain\Models\Translates\TranslateDatanovaToPh;
use PixelHumain\Models\Translates\TranslateEducEcoleToPh;
use PixelHumain\Models\Translates\TranslateEducEtabToPh;
use PixelHumain\Models\Translates\TranslateEducMembreToPh;
use PixelHumain\Models\Translates\TranslateEducStructToPh;
use PixelHumain\Models\Translates\TranslateFtl;
use PixelHumain\Models\Translates\TranslateGogoCarto;
use PixelHumain\Models\Translates\TranslateMediaWiki;
use PixelHumain\Models\Translates\TranslateOdsToPh;
use PixelHumain\Models\Translates\TranslateOrgancityToPh;
use PixelHumain\Models\Translates\TranslateOsmToPh;
use PixelHumain\Models\Translates\TranslatePoleEmploiToPh;
use PixelHumain\Models\Translates\TranslateValueFlowsToPh;
use PixelHumain\Models\Translates\TranslateWikiToPh;

class Convert extends BaseModel implements ConvertInterface, ImportTraitInterface, ApplicationTraitInterface, SIGTraitInterface, TranslateTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use ParamsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use ImportTrait;
    use ApplicationTrait;
    use SIGTrait;
    use TranslateTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateParamsProperty();
    }

    /**
     * Retrieves the primary parameter from the given map.
     *
     * @param array $map The map containing the parameters.
     * @return array The primary parameter.
     */
    public function getPrimaryParam(array $map): array
    {
        $param = [];
        $param['typeElement'] = EventInterface::COLLECTION;

        foreach ($map as $key => $value) {
            $p = [
                "valueAttributeElt" => $value,
                "idHeadCSV" => $key,
            ];
            $param['infoCreateData'][] = $p;
        }

        $param['typeFile'] = 'json';
        $param['warnings'] = false;

        return $param;
    }

    /**
     * Returns the correct URL for ODS and Datanova.
     *
     * @return string The correct URL for ODS and Datanova.
     */
    public function getCorrectUrlForOdsAndDatanova(): string
    {
        // TODO : ne pas utiliser $_GET directement
        $url_final = "";
        $url_ods_head = "";
        $url_ods_params = "";

        foreach ($_GET as $key => $value) {
            if ($key == "url") {
                $url_ods_head = $value;
            } else {
                if (is_array($value)) {
                    foreach ($value as $key2 => $value2) {
                        $url_ods_params .= "&" . $key . "=" . $value2;
                    }
                } else {
                    $url_ods_params .= "&" . $key . "=" . $value;
                }
            }
            $url_final = $url_ods_head . $url_ods_params;
        }

        $pos = strpos($url_ods_head, "?");

        $url_ods_head_final = substr($url_ods_head, 0, $pos) . "?";
        $url_param_dataset = substr($url_ods_head, ($pos + 1));

        $url_ods_params = $url_param_dataset . $url_ods_params;

        $url_ods_params = str_replace("@", "%40", $url_ods_params);

        $url_complete = $url_ods_head_final . $url_ods_params;

        return $url_complete;
    }

    /**
     * Converts an ODS file to PH format.
     *
     * @param string $url The URL of the ODS file.
     * @return array An array containing the converted data.
     */
    public function convertOdsToPh(string $url): array
    {
        $map = TranslateOdsToPh::$mapping_activity;

        $url_complete = $this->getCorrectUrlForOdsAndDatanova();

        $url_complete = str_replace("_", ".", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_ods';
        $param['nameFile'] = 'convert_ods';
        $param['pathObject'] = 'records';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the EducMembre object to a PixelHumain object.
     *
     * @param string $url The URL of the object.
     * @return array The converted object as an array.
     */
    public function convertEducMembreToPh(string $url): array
    {
        $map = TranslateEducMembreToPh::$mapping;

        $url_complete = $this->getCorrectUrlForOdsAndDatanova();
        $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_educ_membre';
        $param['nameFile'] = 'convert_educ_membre';
        $param['pathObject'] = 'records';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the given EducEcole URL to a PH URL.
     *
     * @param string $url The EducEcole URL to convert.
     * @return array The converted PH URL.
     */
    public function convertEducEcoleToPh(string $url): array
    {
        $map = TranslateEducEcoleToPh::$mapping;

        $url_complete = $this->getCorrectUrlForOdsAndDatanova();
        $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_educ_ecole';
        $param['nameFile'] = 'convert_educ_ecole';
        $param['pathObject'] = 'records';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the EducEtab data to PixelHumain format.
     *
     * @param string $url The URL of the EducEtab data.
     * @return array The converted data in PixelHumain format.
     */
    public function convertEducEtabToPh(string $url): array
    {
        $map = TranslateEducEtabToPh::$mapping;
        $url_complete = $this->getCorrectUrlForOdsAndDatanova();
        $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_educ_etab';
        $param['nameFile'] = 'convert_educ_etab';
        $param['pathObject'] = 'records';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the educational structure to the PH format.
     *
     * @param string $url The URL of the educational structure.
     * @return array The converted educational structure in the PH format.
     */
    public function convertEducStructToPh(string $url): array
    {
        $map = TranslateEducStructToPh::$mapping;

        $url_complete = $this->getCorrectUrlForOdsAndDatanova();
        $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_educ_struct';
        $param['nameFile'] = 'convert_educ_struct';
        $param['pathObject'] = 'records';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts Datanova data to PH format.
     *
     * @param string $url The URL of the Datanova data.
     * @return array The converted data in PH format.
     */
    public function convertDatanovaToPh(string $url): array
    {
        $map = TranslateDatanovaToPh::$mapping_activity;

        $url_complete = $this->getCorrectUrlForOdsAndDatanova();

        $url_complete = str_replace("geofilter_polygon", "geofilter.polygon", $url_complete);

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_datanova';
        $param['nameFile'] = 'convert_datanova';
        $param['pathObject'] = 'records';
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url_complete);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        $return = curl_exec($ch);
        curl_close($ch);

        if (isset($url)) {
            $param['file'][0] = $return;
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts an OSM file to PH format.
     *
     * @param string $url The URL of the OSM file.
     * @return array The converted data in PH format.
     */
    public function convertOsmToPh(string $url): array
    {
        $map = TranslateOsmToPh::$mapping_element;

        $param = $this->getPrimaryParam($map);

        $param['pathObject'] = 'elements';
        $param['nameFile'] = 'convert_osm';
        $param['key'] = 'convert_osm';

        $pos = strpos($url, "=");

        $url_head = substr($url, 0, ($pos + 1));
        $url_param = substr($url, ($pos + 1));

        $url_osm = $url_head . urlencode($url_param);

        if (isset($url_osm)) {
            $param['file'][0] = file_get_contents($url_osm);
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the data from Datagouv to Pixelhumain format.
     *
     * @param string $url The URL of the data to be converted.
     * @return array The converted data in Pixelhumain format.
     */
    public function convertDatagouvToPh(string $url): array
    {
        $all_data = [];

        $map = TranslateDatagouvToPh::$mapping_datasets;
        $param = $this->getPrimaryParam($map);

        $param['typeFile'] = 'json';
        $param['key'] = 'convert_datagouv';
        $param['nameFile'] = 'convert_datagouv';
        $list_dataset = json_decode(file_get_contents($url), true);

        foreach ($list_dataset as $key => $value) {
            $url_orga = "https://www.data.gouv.fr/api/1/datasets/" . $value['id'] . "/";
            $data_orga = json_decode((file_get_contents($url_orga)), true);

            array_push($all_data, $data_orga);
        }

        $param['file'][0] = json_encode($all_data);

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts a Wiki page to a PH page.
     *
     * @param string $url The URL of the Wiki page.
     * @param string|null $text_filter The text filter to apply during the conversion.
     * @return array The converted PH page.
     */
    public function convertWikiToPh(string $url, ?string $text_filter): array
    {
        $all_data = [];

        $map = TranslateWikiToPh::$mapping_element;

        $param = $this->getPrimaryParam($map);

        $param['key'] = 'convert_wiki';
        $param['nameFile'] = 'convert_wiki';

        $wikidata_page_city = json_decode(file_get_contents($url), true);

        $pos_wikidataID = strrpos($url, "Q");

        $wikidataID = substr($url, $pos_wikidataID);
        $wikidataID = substr($wikidataID, 0, strpos($wikidataID, "."));

        $label_dbpedia = $wikidata_page_city["entities"][$wikidataID]["sitelinks"]["frwiki"]["title"];

        $wikidata_article = json_decode(file_get_contents("https://query.wikidata.org/sparql?format=json&query=SELECT%20DISTINCT%20%3Fitem%20%3FitemLabel%20%3FitemDescription%20%3Fcoor%20%3Frange%20WHERE%20{%0A%20%3Fitem%20wdt%3AP131%20wd%3A" . $wikidataID . ".%0A%20%3Fitem%20%3Frange%20wd%3A" . $wikidataID . ".%0A%20%3Fitem%20wdt%3AP625%20%3Fcoor.%0A%20SERVICE%20wikibase%3Alabel%20{%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22.%20}%0A}"), true);

        $key_wikidata_item = 0;

        foreach ($wikidata_article['results']['bindings'] as $key => $value) {
            if ($text_filter !== null) {
                if (stristr($value['itemLabel']['value'], (string) $text_filter) == true) {
                    $all_data[$key_wikidata_item] = [];

                    if (@$value['coor']) {
                        $coor = $this->getLatLongWikidataItem($value);
                        array_push($all_data[$key_wikidata_item], $value['itemLabel']['value'], $coor, $value['item']['value'], @$value['itemDescription']['value'], @$value['itemDescription']['value']);
                    }
                    $key_wikidata_item++;
                }
            } else {
                $all_data[$key_wikidata_item] = [];

                if (@$value['coor']) {
                    $coor = $this->getLatLongWikidataItem($value);
                    array_push($all_data[$key_wikidata_item], $value['itemLabel']['value'], $coor, $value['item']['value'], @$value['itemDescription']['value'], @$value['itemDescription']['value']);
                }

                $key_wikidata_item++;
            }
        }

        $param['file'][0] = json_encode($all_data);

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Convert class.
     *
     * This class provides methods for converting data.
     */
    public function poleEmploi(string $url): array
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire");

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=PAR_communecter_9cfae83c352184eff02df647f08661355f3be7028c7ea4eda731bf8718efbfff&client_secret=62a4a6aa2d82fa201eca1ebb3df639882d2ed7cd75284486aaed3a436df67e55&scope=application_PAR_communecter_9cfae83c352184eff02df647f08661355f3be7028c7ea4eda731bf8718efbfff api_infotravailv1");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $token = curl_exec($curl);

        curl_close($curl);

        $token_final = json_decode($token, true);

        $curl2 = curl_init();

        $pos = strpos($url, "=");

        $url_head = substr($url, 0, ($pos + 1));
        $url_param = substr($url, ($pos + 1));

        $url = $url_head . urlencode($url_param);

        curl_setopt($curl2, CURLOPT_URL, $url);
        curl_setopt($curl2, CURLOPT_HTTPHEADER, ["Authorization: Bearer " . $token_final["access_token"]]);

        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
        $offres = curl_exec($curl2);

        curl_close($curl2);

        $offres_final = json_decode($offres, true);
        return $offres_final;
    }

    /**
     * Retrieves the token for the specified API.
     *
     * @param string $apiName The name of the API.
     * @return string|null The token for the API, or null if not found.
     */
    public function getToken(string $apiName): ?string
    {
        $res = $this->getModelApplication()->getToken($apiName);
        if (empty($res) || ((int) $res["expireToken"] - time()) <= 180) {
            if ($apiName == "poleEmploi") {
                $res["token"] = $this->poleEmploiToken();
            }
        }

        return (empty($res["token"]) ? null : (string) $res["token"]);
    }

    // public function poleEmploiToken()
    // {
    //     $poleEmploi = $this->params->get("poleEmploi");
    //     // TODO : verifier $poleEmploi est bien un tableau et contient les clés client_id, client_secret, scope
    //     $curl = curl_init();
    //     curl_setopt($curl, CURLOPT_URL, "https://entreprise.pole-emploi.fr/connexion/oauth2/access_token?realm=%2Fpartenaire");
    //     curl_setopt($curl, CURLOPT_POST, true);
    //     curl_setopt($curl, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id=" . $poleEmploi["client_id"] . "&client_secret=" . $poleEmploi["client_secret"] . "&scope=" . $poleEmploi["scope"]);
    //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    //     $token = curl_exec($curl);
    //     $token_final = json_decode($token, true);
    //     $this->getModelApplication()->saveToken("poleEmploi", $token_final);
    //     curl_close($curl);
    //     return $token_final;
    // }

    // public function poleEmploi2($url, $params)
    // {
    //     $token_final = $this->getToken("poleEmploi");

    //     $res = [
    //         "httpCode" => "0",
    //         "result" => [],
    //         "token" => $token_final,
    //     ];

    //     if (! empty($token_final) && ! empty($token_final["access_token"])) {
    //         $curl2 = curl_init();
    //         curl_setopt($curl2, CURLOPT_URL, $url);
    //         curl_setopt(
    //             $curl2,
    //             CURLOPT_HTTPHEADER,
    //             ['Authorization: Bearer ' . $token_final["access_token"], 'Content-type: application/json']
    //         );
    //         curl_setopt($curl2, CURLOPT_POST, true);
    //         $dataCurl = json_encode($params);
    //         curl_setopt($curl2, CURLOPT_POSTFIELDS, $dataCurl);
    //         curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
    //         $offres = curl_exec($curl2);
    //         $res = [
    //             "httpCode" => curl_getinfo($curl2, CURLINFO_HTTP_CODE),
    //             "result" => $offres,
    //             "token" => $token_final,
    //         ];

    //         curl_close($curl2);
    //     }
    //     return $res;
    // }

    // public function convertPoleEmploiToPh($url, $params = [], $activity_letters = null)
    // {
    //     $result = [];
    //     if (empty($params)) {
    //         $params = [
    //             'technicalParameters' => [
    //                 'page' => 1,
    //                 'per_page' => 20,
    //                 'sort' => 1,
    //             ],
    //         ];
    //     }

    //     $resPoleEmploie = $this->poleEmploi2($url, $params);

    //     if ($resPoleEmploie["httpCode"] == "200") {
    //         $offres_final = json_decode($resPoleEmploie["result"], true);
    //         $map = TranslatePoleEmploiToPh::$mapping_offres;

    //         $param = $this->getPrimaryParam($map);

    //         $param['pathObject'] = '';
    //         $param['key'] = 'convert_poleemploi';
    //         $param['nameFile'] = 'convert_poleemploi';

    //         $offres_array = [];
    //         $offres_array['records'] = [];

    //         if (isset($url) && ! empty($offres_final["results"])) {
    //             $param['file'][0] = json_encode($offres_final["results"]);
    //         }

    //         $result = $this->getModelImport()->previewData($param, true);
    //         $result["count"] = $offres_final["technicalParameters"]["totalNumber"];

    //         if ($result['result'] !== false) {
    //             $result['elements'] = json_decode($result['elements']);
    //         }
    //     } else {
    //         $result['result'] = false;
    //         $result['msg'] = "Il y a un soucis avec l'api du site Pole Emploi. Veuillez rééssayer dans quelque instant";
    //         $result['return'] = $resPoleEmploie;
    //         $result['data'] = $resPoleEmploie["result"];
    //     }

    //     return $result;
    // }

    /**
     * ConvertValueFlowsToPh method converts value flows to PH.
     *
     * @param string $url The URL to convert.
     * @return array The converted value flows.
     */
    public function ConvertValueFlowsToPh(string $url): array
    {
        $map = TranslateValueFlowsToPh::$mapping_valueflows;

        $param = $this->getPrimaryParam($map);

        $param['nameFile'] = 'convert_valueflows';
        $param['key'] = 'convert_valueflows';

        if (isset($url)) {
            $param['file'][0] = file_get_contents($url);
        }

        $result = $this->getModelImport()->previewData($param);

        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }

        return $res;
    }

    /**
     * Converts the Organcity URL to PH format.
     *
     * @param string $url The Organcity URL to convert.
     * @return array The converted URL in PH format.
     */
    public function ConvertOrgancityToPh(string $url): array
    {
        $map = TranslateOrgancityToPh::$mapping_organcity;
        $param = $this->getPrimaryParam($map);

        $param['nameFile'] = 'convert_organcity';
        $param['key'] = 'convert_organcity';

        if (isset($url)) {
            $param['file'][0] = file_get_contents($url);
        }

        $result = $this->getModelImport()->previewData($param);
        if ($result['result'] !== false) {
            $res = json_decode($result['elements']);
        } else {
            $res = [];
        }
        return $res;
    }

    /**
     * Retrieves the latitude and longitude of a Wikidata item.
     *
     * @param string $wikidata_item The Wikidata item to retrieve the latitude and longitude from.
     * @return array An array containing the latitude and longitude values.
     */
    public function getLatLongWikidataItem($wikidata_item): array
    {
        $coor = [];
        if (@$wikidata_item['coor']) {
            $coor = explode(" ", $wikidata_item['coor']['value']);
            $coor["longitude"] = substr($coor[0], 6);
            $coor["latitude"] = rtrim($coor[1], ')');

            unset($coor[0]);
            unset($coor[1]);
        }

        return $coor;
    }

    /**
     * Converts a GogoCarto URL to an array.
     *
     * @param string $url The GogoCarto URL to convert.
     * @param mixed $text_filter The text filter to apply.
     * @return array The converted array.
     */
    public function convertGogoCarto(string $url, $text_filter): array
    {
        $data = [];
        $res = [];

        if (! empty($url)) {
            $data = $this->getModelSIG()->getUrl($url);
            $data = json_decode($data, true);
        }

        $map = TranslateGogoCarto::$dataBinding_network;

        $res = $this->getModelTranslate()->convert($data["data"], $map);

        $result = [
            'entities' => $res,
        ];

        return $result;
    }

    /**
     * Converts the given FTL file to an array.
     *
     * @param string $url The URL of the FTL file.
     * @param mixed $text_filter The text filter to apply during the conversion.
     * @return array The converted FTL file as an array.
     */
    public function convertFtl(string $url, $text_filter): array
    {
        $data = [];
        $res = [];

        if (! empty($url)) {
            $data = $this->getModelSIG()->getUrl($url);
            $data = json_decode($data, true);
        }

        $map = TranslateFtl::$dataBinding_allOrganization;

        $res = $this->getModelTranslate()->convert($data["data"], $map);

        $result = [
            'entities' => $res,
        ];

        return $result;
    }

    /**
     * Converts WikiMedia data to PH format.
     *
     * @param array $data The WikiMedia data to convert.
     * @param string $categorie The category of the data.
     * @param string $wikiName The name of the Wiki.
     * @return array The converted data in PH format.
     */
    public function convertWikiMediaToPh(array $data, string $categorie, string $wikiName): array
    {
        $res = [];

        $init = new TranslateMediaWiki();
        $map = $init->dataBinding($categorie, $wikiName);

        $res += $this->getModelTranslate()->convert($data, $map);

        if (isset($data['data']['Type']) && is_array($data['data']['Type'])) {
            $type = '';
            foreach ($res['data']['type'] as $k => $v) {
                $type = $type . " " . $v;
            }
            $res['data']['type'] = $type;
        } else {
            if ($res['data']['@type'] == "Organization") {
                if (isset($res['data']['type'])) {
                    foreach ($res['data']['type'] as $v) {
                        if ($v == "Association") {
                            $res['data']['type'] = "NGO";
                            break;
                        } elseif ($v == "Groupe public") {
                            $res['data']['type'] = "GovernmentOrganization";
                            break;
                        } elseif ($v == "Startup" || $v == "Collectivité" || $v == "Indépendant") {
                            $res['data']['type'] = "LocalBusiness";
                            break;
                        }
                    }
                } else {
                    $res['data']['type'] = "Group";
                }
            } elseif ($res['data']['@type'] == "Project") {
                $res['data'] += [
                    'type' => "projet",
                ];
            } elseif ($res['data']['@type'] == "Classifieds") {
                $res['data'] += [
                    'type' => "ressource",
                ];
            } elseif ($res['data']['@type'] == "Person") {
                $res['data'] += [
                    'type' => "citoyen",
                ];
            }
        }
        $res['data'] += $data['data'];
        return $res;
    }

    /**
     * Converts OpenAgenda data to Ph format.
     *
     * @param array $parameters The parameters for the conversion.
     * @return array The converted data in Ph format.
     */
    public function ConvertOpenAgendaToPh(array $parameters = []): array
    {
        // récupération des liens
        $simple_fields = [
            'key',
            'limit',
            'after',
            'size',
            'search',
            'official',
            'network',
        ];
        $array_fields = [
            'fields',
            'slug',
            'uid',
        ];
        $url = 'https://api.openagenda.com/v2/me/agendas?';
        $url_constructor = [];

        // vérification des paramètres simples
        foreach ($simple_fields as $simple_field) {
            if (! empty($parameters[$simple_field])) {
                $url_constructor[] = $simple_field . '=' . $parameters[$simple_field];
            }
        }

        // vérirification des paramètres avec des tableaux
        foreach ($array_fields as $array_field) {
            if (! empty($parameters[$array_field])) {
                foreach ($parameters[$array_field] as $simple_field) {
                    $url_constructor[] = $array_field . "[]=" . $simple_field;
                }
            }
        }

        // assemblage des liens
        $url .= implode('&', $url_constructor);

        // recueillir les données distantes
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output, true);
    }

    /**
     * ConvertOpenAgendaEventToPh converts an OpenAgenda event to a PixelHumain event.
     *
     * @param string $agenda The agenda to convert.
     * @param string $key The key to use for the conversion.
     * @return string The converted PixelHumain event.
     */
    public function ConvertOpenAgendaEventToPh(string $agenda, string $key): string
    {
        $url = "https://api.openagenda.com/v2/agendas/$agenda/events?key=$key";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);

        // mapping
        return $output;
    }

    /**
     * Converts an iCalendar string to a Ph string.
     *
     * @param string $http The iCalendar string to convert.
     * @return string The converted Ph string.
     */
    public function ConvertICalendarToPh(string $http): string
    {
        $curl = curl_init($http);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);

        // mapping
        return $output;
    }
}
