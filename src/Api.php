<?php

namespace PixelHumain\Models;

use DateTime;
use PixelHumain\Models\Interfaces\ApiInterface;
use PixelHumain\Models\Interfaces\CityInterface;
use PixelHumain\Models\Interfaces\CommentInterface;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\OrganizationInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Interfaces\ProjectInterface;
use PixelHumain\Models\Interfaces\ProposalInterface;

use PixelHumain\Models\Interfaces\TranslateInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\BaseModel\UrlHelperTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\Interfaces\SearchNewTraitInterface;
use PixelHumain\Models\Traits\Interfaces\TranslateTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;

use PixelHumain\Models\Traits\SearchNewTrait;
use PixelHumain\Models\Traits\TranslateTrait;
use PixelHumain\Models\Translates\TranslateCommunecter;
use PixelHumain\Models\Translates\TranslateFinder;
use PixelHumain\Models\Translates\TranslateMD;

use PixelHumain\Models\Translates\TranslateTree;

class Api extends BaseModel implements ApiInterface, DocumentTraitInterface, SearchNewTraitInterface, CostumTraitInterface, ElementTraitInterface, PersonTraitInterface, TranslateTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use SessionTrait;
    use UrlHelperTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DocumentTrait;
    use SearchNewTrait;
    use CostumTrait;
    use ElementTrait;
    use PersonTrait;
    use TranslateTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateUrlHelperProperty();
    }

    /**
     * Retrieves a new format link.
     *
     * @param array $link The link to be formatted.
     * @return array The formatted link.
     */
    public function getNewFormatLink(array $link): array
    {
        $fieldsLink = ["name"];
        $allData = [];
        foreach ($link as $typeLinks => $valueLinks) {
            foreach ($valueLinks as $keyLink => $valueLink) {
                $paramsLink = [];
                $dataLink = $this->db->findOne($valueLink["type"], [
                    "_id" => $this->db->MongoId($keyLink),
                ], $fieldsLink);
                if (! empty($dataLink)) {
                    $newFormatData["name"] = $dataLink["name"];
                    $newFormatData["url"] = "/data/get/type/" . $valueLink["type"] . "/id/" . $keyLink;

                    if (! empty($valueLink["isAdmin"])) {
                        $newFormatData["isAdmin"] = $valueLink["isAdmin"];
                    }

                    $allData[$typeLinks][] = $newFormatData;
                }
            }
        }
        return $allData;
    }

    /**
     * Retrieves the URL of an image based on the provided data and type.
     *
     * @param array $data The data used to retrieve the image URL.
     * @param string $type The type of the image.
     * @return array The URL of the image.
     */
    public function getUrlImage(array $data, string $type): array
    {
        foreach ($data as $keyEntities => $valueEntities) {
            $doc = $this->getModelDocument()->getLastImageByKey($keyEntities, $type, DocumentInterface::IMG_PROFIL);
            $data[$keyEntities]["image"] = $doc;
        }
        return $data;
    }

    /**
     * Retrieves data based on search criteria.
     *
     * @param string $search The search term.
     * @param string|null $type The type of data to retrieve.
     * @param string|null $tags The tags associated with the data.
     * @param int|null $index The starting index of the data.
     * @param int|null $limit The maximum number of data to retrieve.
     * @return array The retrieved data.
     */
    public function getDataBySearch(string $search, ?string $type, ?string $tags, ?int $index, ?int $limit): array
    {
        $paramsSearch = [
            'name' => $search,
            'searchType' => (! empty($type) ? explode(",", $type) : [PersonInterface::COLLECTION, OrganizationInterface::COLLECTION, ProjectInterface::COLLECTION]),
            'searchTag' => (! empty($tags) ? explode(",", $tags) : []),
            'indexMin' => (! empty($index) ? $index : "0"),
            'indexStep' => (! empty($limit) ? $index + $limit : "30"),
        ];

        $res = $this->getModelSearchNew()->globalAutoComplete($paramsSearch, null, true);
        return $res;
    }

    // TODO : $subtype n'est pas utilisé la ou la fonction est utilisé
    public function getData(array $bindMap, ?string $format = null, string $type, ?string $id = null, int $limit = 50, int $index = 0, ?string $tags = null, ?string $multiTags = null, ?string $key = null, ?string $insee = null, ?string $geoShape = null, ?string $idElement = null, ?string $typeElement = null, ?string $namecity = null, ?array $p = null, ?string $subtype = null)
    {
        $meta = [];
        $result = [];

        $params = [];
        if (@$id) {
            $params["_id"] = $this->db->MongoId($id);
        }

        if (@$idElement) {
            $params["target.id"] = $idElement;
        }

        if (@$typeElement) {
            $params["target.type"] = $typeElement;
        }

        if (@$insee) {
            if ($type == CityInterface::COLLECTION) {
                $params["insee"] = $insee;
            } else {
                $params["address.codeInsee"] = $insee;
            }
        }

        if (@$namecity) {
            $namecityArray = explode(",", $namecity);

            $allQueryLocality = [];
            if (count($namecityArray) == 1) {
                $params["address.addressLocality"] = $this->db->MongoRegex("/" . $namecityArray[0] . "/i");
            } else {
                foreach ($namecityArray as $keyLocality => $locality) {
                    $queryLocality = $this->db->MongoRegex("/" . $locality . "/i");

                    if (! empty($queryLocality)) {
                        $allQueryLocality[] = $queryLocality;
                    }
                    unset($queryLocality);
                }
                $params["address.addressLocality"] = [
                    '$in' => $allQueryLocality,
                ];
            }
        }

        if (! empty($p)) {
            foreach ($p as $keyP => $valP) {
                if ($keyP == "level3") {
                    $params["address.level3"] = $valP;
                } elseif ($keyP == "level4") {
                    $params["address.level4"] = $valP;
                } elseif ($keyP == "level1") {
                    $params["address.level1"] = $valP;
                } else {
                    $params[$keyP] = $valP;
                }
            }
        }

        if (! empty($subtype)) {
            $params["type"] = $subtype;
        }

        if (@$tags) {
            $tagsArray = explode(",", $tags);

            $verbTag = (($multiTags == "true") ? '$eq' : '$in');
            $queryTags = $this->getModelSearchNew()->searchTags($tagsArray, $verbTag);

            if (! empty($queryTags)) {
                $params["tags"] = $queryTags["tags"];
            }
        }

        if ($type == NewsInterface::COLLECTION) {
            $params["scope.type"] = "public";
        }

        if ($format != "csv") {
            if ($limit > 500) {
                $limit = 500;
            } elseif ($limit < 1) {
                $limit = 50;
            }
        } else {
            $limit = 0;
        }

        if ($index < 0) {
            $index = 0;
        }

        if ($format == TranslateInterface::FORMAT_GOGO) {
            $params["geoPosition"] = [
                '$exists' => 1,
            ];
        }

        $costumSession = $this->session->get('costum');
        if (! empty($costumSession)) {
            $params["source.keys"] = [
                '$in' => [$costumSession["request"]["sourceKey"]],
            ];
        }

        if (@$key) {
            if (empty($params)) {
                $params = [
                    '$or' => [[
                        "source.key" => $key,
                    ], [
                        "source.keys" => [
                            '$in' => [$key],
                        ],
                    ], [
                        "reference.costum" => [
                            '$in' => [$key],
                        ],
                    ]],
                ];
            } else {
                $params = [
                    '$and' => [
                        $params, [
                            '$or' => [[
                                "source.key" => $key,
                            ], [
                                "source.keys" => [
                                    '$in' => [$key],
                                ],
                            ], [
                                "reference.costum" => [
                                    '$in' => [$key],
                                ],
                            ]],
                        ]],
                ];
            }
        }

        $data = $this->db->findAndLimitAndIndex($type, $params, $limit, $index);
        $data = self::getUrlImage($data, $type);

        if ($type == CityInterface::COLLECTION) {
            if (@$geoShape != "1") {
                foreach ($data as $key => $value) {
                    unset($value["geoShape"]);
                    $data[$key] = $value;
                }
            }
        }

        if ($type == ProposalInterface::COLLECTION) {
            foreach ($data as $key => $value) {
                if (! empty($value["votes"])) {
                    $value["votes"]["up"] = ((! empty($value["votes"]["up"])) ? count($value["votes"]["up"]) : 0);
                    $value["votes"]["white"] = ((! empty($value["votes"]["white"])) ? count($value["votes"]["white"]) : 0);
                    $value["votes"]["down"] = ((! empty($value["votes"]["down"])) ? count($value["votes"]["down"]) : 0);

                    $value["votes"]["uncomplet"] = ((! empty($value["votes"]["uncomplet"])) ? count($value["votes"]["uncomplet"]) : 0);
                }
                $a = [];
                if (! empty($value["amendements"])) {
                    foreach ($value["amendements"] as $keyA => $valA) {
                        unset($valA["idUserAuthor"]);
                        if (! empty($valA["votes"])) {
                            $valA["votes"]["up"] = ((! empty($valA["votes"]["up"])) ? count($valA["votes"]["up"]) : 0);
                            $valA["votes"]["white"] = ((! empty($valA["votes"]["white"])) ? count($valA["votes"]["white"]) : 0);
                            $valA["votes"]["down"] = ((! empty($valA["votes"]["down"])) ? count($valA["votes"]["down"]) : 0);
                        }

                        if (! empty($valA["textAdd"])) {
                            $valA["textAdd"] = str_replace(";", ".", $valA["textAdd"]);
                            $valA["textAdd"] = str_replace('"', "'", $valA["textAdd"]);
                        }

                        $a[] = $valA;
                    }

                    $value["amendements"] = $a;
                }

                $whereContext = [
                    "contextId" => $key,
                    "contextType" => ProposalInterface::COLLECTION,
                ];
                $sort = [
                    "voteUpCount" => -1,
                ];
                $comments = $this->db->findAndSort(CommentInterface::COLLECTION, $whereContext, $sort);

                $coms = [];
                foreach ($comments as $keyCC => $valCC) {
                    $valCC["text"] = str_replace(";", ".", $valCC["text"]);
                    $valCC["text"] = str_replace('"', "'", $valCC["text"]);

                    $coms[] = [
                        "text" => $valCC["text"],
                        "argval" => $valCC["argval"],
                        "voteCount" => ((! empty($valCC["voteCount"])) ? $valCC["voteCount"] : []),
                    ];
                }
                $value["comments"] = $coms;

                $data[$key] = $value;
            }
        }

        if (PersonInterface::COLLECTION == $type) {
            $costum = $this->getModelCostum()->getCostum();
            foreach ($data as $key => $value) {
                $costumSession = $this->session->get('costum');
                if (! empty($costumSession) && ! empty($costumSession[$costum["slug"]]) && ! empty($costumSession[$costum["slug"]]['isCostumAdmin'])) {
                    $person = $this->db->findOneById(PersonInterface::COLLECTION, $key);
                } else {
                    $person = $this->getModelPerson()->getSimpleUserById($key, $value);
                }
                $data[$key] = $person;
            }
        }

        if (ProjectInterface::COLLECTION == $type) {
            foreach ($data as $key => $value) {
                if (! empty($value["startDate"])) {
                    $value["startDate"] = date(DateTime::ISO8601, $value["startDate"]->sec);
                }
                if (! empty($value["endDate"])) {
                    $value["endDate"] = date(DateTime::ISO8601, $value["endDate"]->sec);
                }
                $data[$key] = $value;
            }
        }

        foreach ($data as $key => $value) {
            if (empty($value["id"])) {
                $value["id"] = (string) $value["_id"];
            }
            $data[$key] = $value;
        }

        if (($format == TranslateInterface::FORMAT_RSS) || ($format == TranslateInterface::FORMAT_KML) || ($format == TranslateInterface::FORMAT_KML)) {
            foreach ($data as $key => $value) {
                $data2[] = $value;
            }

            if (isset($data2)) {
                $data = $data2;
            }
        }

        if (empty($id)) {
            $meta["limit"] = is_countable($data) ? count($data) : 0;
            $server = ((isset($_SERVER['HTTPS']) and (! empty($_SERVER['HTTPS'])) and strtolower($_SERVER['HTTPS']) != 'off') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
            $meta["next"] = $server . $this->urlHelper->createUrl("/api/" . $this->getModelElement()->getControlerByCollection($type) . "/get/limit/" . $limit . "/index/" . ($index + $limit));

            if (@$format) {
                $meta["next"] .= "/format/" . $format;
            }

            if (! empty($tags)) {
                $meta["next"] .= "/tags/" . $tags;
            }

            if (! empty($namecity)) {
                $meta["next"] .= "/namecity/" . $namecity;
            }

            if ($index != 0) {
                $newIndex = $index - $limit;
                if ($newIndex < 0) {
                    $newIndex = 0;
                }
                $meta["previous"] = $server . $this->urlHelper->createUrl("/api/" . $this->getModelElement()->getControlerByCollection($type) . "/get/limit/" . $limit . "/index/" . $newIndex);
            }
        } else {
            $meta["limit"] = 1;
        }

        if (($format == TranslateInterface::FORMAT_RSS) || ($format == TranslateInterface::FORMAT_KML)) {
            $result = ((! empty($data) && ! empty($bindMap)) ? $this->getModelTranslate()->convert($data, $bindMap) : $data);
        } elseif ($format == TranslateInterface::FORMAT_GEOJSON) {
            $result["type"] = "FeatureCollection";
            $result["features_temp"] = ((! empty($data) && ! empty($bindMap)) ? $this->getModelTranslate()->convert_geojson($data, $bindMap) : $data);
            $result["features"] = [];
            $result["features"] = $result["features_temp"];
            unset($result["features_temp"]);
        } elseif ($format == TranslateInterface::FORMAT_JSONFEED) {
            $meta = [];
            $meta["version"] = "https://jsonfeed.org/version/1";
            $meta["title"] = "Communecter's JSON Feed";
            $meta["description"] = "This is the JSON Feed of the Communecter site";
            $meta["home_page_url"] = "www.communecter.org";

            $result["meta"] = $meta;
            $result["items"] = ((! empty($data) && ! empty($bindMap)) ? $this->getModelTranslate()->convert($data, $bindMap) : $data);
        } elseif ($format == TranslateInterface::FORMAT_GOGO) {
            $meta = [];
            $newData = [];
            foreach ($data as $key => $value) {
                $newData[] = $value;
            }
            //var_dump($meta);
            $meta["data"] = ((! empty($newData) && ! empty($bindMap)) ? $this->getModelTranslate()->convert($newData, $bindMap) : $newData);
            $meta["fullRepresentation"] = true;
            $meta["allElementsSends"] = true;
            $result = $meta;
        } elseif ($format == TranslateInterface::FORMAT_MD) {
            $data = $this->getModelTranslate()->convert($data, TranslateCommunecter::$dataBinding_person);
            $result = (new TranslateMD())->MD($data, $bindMap);
        } elseif ($format == TranslateInterface::FORMAT_TREE) {
            $data = $this->getModelTranslate()->convert($data, TranslateCommunecter::$dataBinding_person);
            $result = (new TranslateTree())->build($data, $bindMap);
        } elseif ($format == TranslateInterface::FORMAT_FINDER) {
            $data = $this->getModelTranslate()->convert($data, TranslateCommunecter::$dataBinding_person);
            $result = (new TranslateFinder())->build($data, $bindMap);
        } else {
            $result["meta"] = $meta;
            $result["entities"] = ((! empty($data) && ! empty($bindMap)) ? $this->getModelTranslate()->convert($data, $bindMap) : $data);
        }
        return $result;
    }

    /**
    * Returns a string with accent to REGEX expression to find any combinations
    * in accent insentive way
    *
    * @param string $text The text.
    * @return string The REGEX text.
    */
    public function accentToRegex($text)
    {
        $from = str_split(utf8_decode('ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ'));
        $to = str_split(strtolower('SOZsozYYuAAAAAAACEEEEEIIIIIDNOOOOOOUUUUYsaaaaaaaceeeeeiiiiionoooooouuuuyy'));
        $text = utf8_decode($text);
        $regex = [];

        foreach ($to as $key => $value) {
            if (isset($regex[$value])) {
                $regex[$value] .= $from[$key];
            } else {
                $regex[$value] = $value;
            }
        }

        foreach ($regex as $rg_key => $rg) {
            $text = preg_replace("/[$rg]/", "_{$rg_key}_", $text);
        }

        foreach ($regex as $rg_key => $rg) {
            $text = preg_replace("/_{$rg_key}_/", "[$rg]", $text);
        }
        return utf8_encode($text);
    }

    // TODO : la fonction n'est pas utilisé
    /**
     * Makes a rating based on the given rate and best value.
     *
     * @param mixed $rate The rate value.
     * @param int $bestvalue The best value for rating (default is 5).
     * @return string The rating string.
     */
    public function makeRating($rate, int $bestvalue = 5): string
    {
        // extraction of the integer part of the score (whether decimal or not)
        $intrate = intval($rate);
        // calculation of the possible decimal part in %.
        $decrate = (floatval($rate) - $intrate) * 100;
        //  insertion of microformats and microdatas
        $ratingBox = '                     <!-- item AggregateRating -->' . PHP_EOL;
        $ratingBox .= '             <p class="ratingBox" itemprop="aggregateRating" itemscope itemtype="http://schema.xyz/AggregateRating">' . PHP_EOL;
        $ratingBox .= '             <span title="' . $rate . ' / ' . $bestvalue . '">' . PHP_EOL;

        for ($i = 0; $i < $bestvalue; ++$i) {
            $ratingBox .= '             <svg height="16" width="16">' . PHP_EOL;
            if ($i < $intrate) {
                $ratingBox .= '               <polygon points="8,0 10.5,5 16,6 12,10 13,16 8,13 3,16 4,10 0,6 5.5,5" fill="#93C020" stroke="DarkKhaki" stroke-width=".5" />' . PHP_EOL;
            } elseif ($i == $intrate && $decrate > 0) {
                $ratingBox .= '               <defs>' . PHP_EOL;
                $ratingBox .= '                  <linearGradient id="starGradient">' . PHP_EOL;
                $ratingBox .= '                    <stop offset="' . $decrate . '%" stop-color="#93C020"/>' . PHP_EOL;
                $ratingBox .= '                    <stop offset="' . $decrate . '%" stop-color="LightGrey"/>' . PHP_EOL;
                $ratingBox .= '                  </linearGradient>' . PHP_EOL;
                $ratingBox .= '               </defs>' . PHP_EOL;
                $ratingBox .= '               <polygon points="8,0 10.5,5 16,6 12,10 13,16 8,13 3,16 4,10 0,6 5.5,5" fill="url(#starGradient)" stroke="DarkKhaki" stroke-width=".5" />' . PHP_EOL;
            } else {
                $ratingBox .= '               <polygon points="8,0 10.5,5 16,6 12,10 13,16 8,13 3,16 4,10 0,6 5.5,5"  fill="LightGrey" stroke="DimGray" stroke-width=".5" />' . PHP_EOL;
            }
            $ratingBox .= '               </svg>' . PHP_EOL;
        }
        $ratingBox .= '             </span>' . PHP_EOL;
        // insert the note in plain text but hidden, with microformat and microdata - delete style="display:none;" to display it
        $ratingBox .= '             <span style="display:none;"><span itemprop="ratingValue" class="rating" title="' . $rate . '">' . $rate . '</span>';
        $ratingBox .= '<span > / </span><span itemprop="bestRating">' . $bestvalue . '</span></span>' . PHP_EOL;
        $ratingBox .= '             </p>' . PHP_EOL;
        $ratingBox .= '                     <!-- end of item -->' . PHP_EOL;
        return $ratingBox;
    }

    /**
     * Check if a given value is a valid MongoDB ObjectId.
     *
     * @param mixed $id The value to check.
     * @return bool True if the value is a valid MongoDB ObjectId, false otherwise.
     */
    public function isValidMongoId($id)
    {
        return is_string($id) && strlen($id) == 24 && ctype_xdigit($id);
    }

    /**
     * Checks if a string starts with a specific substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle The substring to search for.
     * @param bool $case (optional) Whether to perform a case-sensitive search. Default is true.
     * @return int|bool True if the string starts with the specified substring, false otherwise.
     */
    public function stringStartsWith(string $haystack, string $needle, bool $case = true)
    {
        if ($case) {
            return strpos($haystack, $needle, 0) === 0;
        }
        return stripos($haystack, $needle, 0) === 0;
    }

    /**
     * Checks if a string ends with a specific substring.
     *
     * @param string $haystack The string to search in.
     * @param string $needle The substring to search for.
     * @param bool $case (optional) Whether to perform a case-sensitive search. Default is true.
     * @return int|bool True if the string ends with the specified substring, false otherwise.
     */
    public function stringEndsWith(string $haystack, string $needle, bool $case = true)
    {
        $expectedPosition = strlen($haystack) - strlen($needle);
        if ($case) {
            return strrpos($haystack, $needle, 0) === $expectedPosition;
        }
        return strripos($haystack, $needle, 0) === $expectedPosition;
    }

    /**
     * Check if something is an associative array
     *
     * Note: this method only checks the key of the first value in the array.
     *
     * @param mixed $param The variable to check
     * @return bool true if the variable is an associative array
     */
    public function isAssociativeArray($param)
    {
        if (is_array($param)) {
            $keys = array_keys($param);
            if ($keys[0] === 0) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Check the status of a date within a given range.
     *
     * @param string $targetDate The date to check the status of.
     * @param string $startDate The start date of the range.
     * @param string $endDate The end date of the range.
     * @return string The status of the target date within the range.
     */
    public function checkDateStatus(string $targetDate, string $startDate, string $endDate): string
    {
        $targetTimestamp = strtotime($targetDate);
        $startTimestamp = strtotime($startDate);
        $endTimestamp = strtotime($endDate);

        if (! empty($startDate) && $targetTimestamp < $startTimestamp) {
            return "late";
        } elseif (
            (empty($startDate) || $targetTimestamp >= $startTimestamp) &&
            (empty($endDate) || $targetTimestamp <= $endTimestamp)
        ) {
            return "include";
        } else {
            return "past";
        }
    }

    // TODO : le nom de la signature n'est pas ecris en camelCase
    /**
     * Checks if a value is a valid timestamp.
     *
     * @param mixed $value The value to check.
     * @return bool Returns true if the value is a valid timestamp, false otherwise.
     */
    public function is_timestamp($value): bool
    {
        if (ctype_digit((string) $value)) {
            $timestamp = (int) $value;
            return ($timestamp <= PHP_INT_MAX) && ($timestamp >= ~PHP_INT_MAX);
        }
        return false;
    }

    // TODO : le nom de la signature n'est pas ecris en camelCase
    /**
     * Converts a timestamp to the format "d/m/Y H:i".
     *
     * @param string $default The default format to use if the timestamp is empty.
     * @param string $timestamp The timestamp to convert.
     * @return string The converted timestamp in the format "d/m/Y H:i".
     */
    public function convert_to_dd_mm_YYYY_H_i(string $default = 'd/m/Y H:i', string $timestamp): string
    {
        return date($default, strtotime($timestamp));
    }

    // TODO : le nom de la signature n'est pas ecris en camelCase
    /**
     * Converts a timestamp to a formatted date and time string.
     *
     * @param string $target The desired format for the date and time string. Default is 'd/m/Y H:i'.
     * @param int $timestamp The timestamp to convert.
     * @return string The formatted date and time string.
     */
    public function convert_timestamp_to_dd_mm_YYYY_H_i(string $target = 'd/m/Y H:i', int $timestamp): string
    {
        return date($target, $timestamp);
    }

    /**
     * Retrieves the value from an array based on the given path.
     *
     * @param array $array The array to search in.
     * @param string $path The path to the desired value, using dot notation.
     * @return array|null The value found at the given path, or null if not found.
     */
    public function getArrayValueByPath(array $array, string $path): ?array
    {
        $segments = explode('.', $path);

        foreach ($segments as $segment) {
            if (is_array($array) && isset($array[$segment])) {
                $array = $array[$segment];
            } else {
                return null; // Return null if the path is not valid
            }
        }

        return (array) $array;
    }

    // TODO : utilisation de reference mais il y a un retour c'est bizarre
    // TODO : le nom de la signature n'est pas ecris en camelCase
    /**
     * Get the value from an array using a path.
     *
     * This method retrieves the value from an array using a path. The path is a string that represents the keys
     * separated by a delimiter (default is dot). If the path is not found in the array, null is returned.
     *
     * @param array &$array The array to search in.
     * @param string|array $path The path to the value.
     * @param string $glue The delimiter used in the path (default is dot).
     * @return array|null The value found in the array or null if not found.
     */
    public function path_get_value(array &$array, $path, string $glue = '.'): ?array
    {
        // Si $path est une chaîne, utilisez explode pour la convertir en tableau de clés
        $keys = is_array($path) ? $path : explode($glue, $path);

        // Référence utilisée pour parcourir le tableau
        $ref = &$array;

        foreach ($keys as $key) {
            // Vérifiez si $ref est un tableau et contient la clé
            if (is_array($ref) && array_key_exists($key, $ref)) {
                // Mettez à jour la référence pour pointer sur l'élément enfant
                $ref = &$ref[$key];
            } else {
                // Si le chemin n'est pas valide, retournez null
                return null;
            }
        }

        // Retournez la valeur référencée, convertie en tableau (si possible)
        return is_array($ref) ? $ref : null;
    }
}
