<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\FolderInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\FsTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\CostumTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\CostumTraitInterface;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;

// TODO : Yii::get('fs')

class Folder extends BaseModel implements FolderInterface, CostumTraitInterface, DocumentTraitInterface, ElementTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;
    use ParamsTrait;
    use FsTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use CostumTrait;
    use DocumentTrait;
    use ElementTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
        $this->validateFsProperty();
    }

    /**
     * Retrieves a folder by its ID.
     *
     * @param string $id The ID of the folder.
     * @return array|null The folder data if found, null otherwise.
     */
    public function getById(string $id): ?array
    {
        return $this->db->findOne(FolderInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
    }

    /**
     * Create a new folder.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $name The name of the folder.
     * @param string|null $docType The type of the document.
     * @param string|null $parentId The ID of the parent folder.
     * @return array The created folder.
     */
    public function create(string $contextId, string $contextType, string $name, string $docType = null, string $parentId = null): array
    {
        $newComment = [];
        $costum = $this->getModelCostum()->getCostum();
        $params = [
            "contextId" => $contextId,
            "contextType" => $contextType,
            "name" => $name,
            "collection" => FolderInterface::COLLECTION,
            "created" => time(),
            "updated" => time(),
        ];
        if (@$parentId) {
            $params["parentId"] = $parentId;
        }
        if (@$docType) {
            $params["docType"] = $docType;
        }
        if (isset($costum) && isset($costum["slug"])) {
            // TODO : $newComment n'est jamais utilisé, est ce que c'est normalement dans params que ça devrait être ?
            $newComment["source"] = [
                "insertOrigin" => "costum",
                "key" => $costum["slug"],
                "keys" => [$costum["slug"]],
            ];
        }

        $params = $this->db->insert(FolderInterface::COLLECTION, $params);

        $this->createFolder($params);
        return [
            "result" => true,
            "folder" => $params,
            "msg" => $this->language->t("common", "Folder {what} created with success", [
                "{what}" => $name,
            ]),
        ];
    }

    // TODO : la fonction est utilisée que dans la classe changer la visibilité en private
    /**
     * Creates a folder.
     *
     * @param array $folder The folder data.
     * @return bool Returns true if the folder is created successfully, false otherwise.
     */
    private function createFolder(array $folder): bool
    {
        $folderPath = $this->getFolderPath($folder);
        $upload_dir = $this->params->get('uploadDir') . "communecter/";
        $folderPath = str_replace($upload_dir, "", $folderPath);
        $folderPathExp = explode("/", $folderPath);
        foreach ($folderPathExp as $v) {
            $upload_dir .= $v . '/';
            if (! $this->fs->has($upload_dir)) {
                $this->fs->createDirectory($upload_dir);
            }
        }
        return true;
    }

    /**
     * Update a folder.
     *
     * @param string $id The ID of the folder.
     * @param string $name The new name of the folder.
     * @param bool $del Whether to delete the folder or not.
     * @return array The updated folder data.
     */
    public function update(string $id, string $name, bool $del = false): array
    {
        $this->db->update(
            FolderInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($id),
            ],
            [
                '$set' => [
                    "name" => $name,
                ],
            ]
        );
        $fold = [
            'name' => $name,
            "id" => $id,
        ];
        return [
            "result" => true,
            "name" => $name,
            "folder" => $fold,
            "msg" => $this->language->t("common", "Collection {what} renamed with success", [
                "{what}" => $name,
            ]),
        ];
    }

    /**
     * Retrieves an array of subfolders by their ID.
     *
     * @param string $id The ID of the folder.
     * @return array An array of subfolders.
     */
    public function getSubfoldersById(string $id): array
    {
        return $this->db->find(FolderInterface::COLLECTION, [
            "parentId" => $id,
        ]);
    }

    /**
     * Retrieves an array of subfolders based on the given context ID, context type, and document type.
     *
     * @param string $contextId The ID of the context.
     * @param string $contextType The type of the context.
     * @param string $docType The type of the document.
     * @return array An array of subfolders.
     */
    public function getSubfoldersByContext(string $contextId, string $contextType, string $docType): array
    {
        return $this->db->find(FolderInterface::COLLECTION, [
            "contextId" => $contextId,
            "contextType" => $contextType,
            "docType" => $docType,
            "parentId" => [
                '$exists' => false,
            ],
        ]);
    }

    /**
     * Deletes a folder.
     *
     * @param string $id The ID of the folder to delete.
     * @param bool $removeDir Whether to remove the directory associated with the folder.
     * @return array An array containing the result of the deletion operation.
     */
    public function delete(string $id, bool $removeDir = true): array
    {
        $folder = $this->getById($id);
        if (@$folder["docType"]) {
            //process of deleted document id db and children

            // Remove all document in Db link to this folder
            $this->removeDocumentByFolder($id);

            //Find sub folders and give the same process of deleted
            $subFolders = $this->getSubfoldersById($id);
            if (! empty($subFolders)) {
                foreach ($subFolders as $key => $data) {
                    $this->delete($key, null);
                }
            }
        }
        // Deleted file in upload (only one time no on subfolders)
        if (! empty($removeDir)) {
            $folderPath = $this->getFolderPath($folder);
            if ($folderPath && $this->fs->has($folderPath)) {
                $this->fs->delete($folderPath);
            }
        }
        // remove the current folder in DB
        $this->db->remove(FolderInterface::COLLECTION, [
            "_id" => $this->db->MongoId($id),
        ]);
        return [
            "result" => true,
            "folder" => [
                "id" => $id,
            ],
            "msg" => $this->language->t("common", "Folder {what} and all its elements deleted with success", [
                "{what}" => $folder["name"],
            ]),
        ];
    }

    // TODO : la fonction est utilisée que dans la classe changer la visibilité en private
    /**
     * Removes a document by folder.
     *
     * @param string $folder The folder to remove the document from.
     * @return array The updated array after removing the document.
     */
    private function removeDocumentByFolder(string $folder): array
    {
        //TODO SBAR - Generate new thumbs if the image is the current image
        $docs = $this->getModelDocument()->getWhere([
            "folderId" => $folder,
        ]);
        if (@$docs) {
            //delete all entries in DB
            foreach ($docs as $key => $doc) {
                $this->db->remove(DocumentInterface::COLLECTION, [
                    "_id" => $key,
                ]);
            }
            $res = [
                'result' => true,
                "msg" => $this->language->t("document", "no Documents associated"),
            ];
        } else {
            $res = [
                'result' => true,
                "msg" => $this->language->t("document", "no Documents associated"),
            ];
        }

        return $res;
    }

    /**
     * Retrieves the parent folders of a folder by its ID.
     *
     * @param string $id The ID of the folder.
     * @return array|null An array of parent folders or null if no parent folders are found.
     */
    public function getParentsFoldersById(string $id): ?array
    {
        $parentFolder = $this->getById($id);
        $parents = [$parentFolder];
        if (@$parentFolder["parentId"]) {
            $parents[$parentFolder["parentId"]] = $this->getById($parentFolder["parentId"]);
        }

        return $parents;
    }

    /**
     * Returns the path of the parent folders for a given folder ID.
     *
     * @param string $id The ID of the folder.
     * @return string The path of the parent folders.
     */
    public function getParentFoldersPath(string $id): string
    {
        $parentFolder = $this->getById($id);
        $result = "";
        if (@$parentFolder["parentId"]) {
            $result .= $this->getParentFoldersPath($parentFolder["parentId"]);
        }

        return $result . $id . "/";
    }

    /**
     * Returns the folder path for a given folder.
     *
     * @param array $folder The folder data.
     * @param bool $thumb Whether to include the thumbnail path or not. Default is false.
     * @return string|false The folder path.
     */
    public function getFolderPath(array $folder, bool $thumb = false)
    {
        $path = $this->params->get('uploadDir') . "communecter" . "/";
        if (@$folder["contextType"] && @$folder["contextId"] && @$folder["docType"]) {
            $docTypePath = ($folder["docType"] == "image") ? "album" : $folder["docType"];
            $path .= $folder["contextType"] . "/" . $folder["contextId"] . "/" . $docTypePath . "/";
            if (@$folder["parentId"]) {
                $path .= $this->getParentFoldersPath($folder["parentId"]);
            }
            $path .= (string) $folder["_id"] . "/";
            if ($thumb) {
                $path .= DocumentInterface::GENERATED_IMAGES_FOLDER . "/";
            }
        } else {
            $path = false;
        }
        return $path;
    }

    /**
     * Move the specified documents to a new folder.
     *
     * @param array $ids The IDs of the documents to move.
     * @param string|null $idFolder The ID of the destination folder. If null, the documents will be moved to the root folder.
     * @param string $type The type of the documents to move. Defaults to DocumentInterface::COLLECTION.
     * @return array The updated documents.
     */
    public function moveToFolder(array $ids, ?string $idFolder = null, string $type = DocumentInterface::COLLECTION): array
    {
        if (! empty($idFolder)) {
            $folder = $this->getById($idFolder);
            $folderName = $folder["name"];
            $newFolderPath = $this->getFolderPath($folder);
            $action = '$set';
        } else {
            $folderName = $this->language->t("common", "the root");
            $action = '$unset';
        }

        foreach ($ids as $id) {
            if ($type == DocumentInterface::COLLECTION) {
                $movedEl = $this->getModelDocument()->getById($id);
                $tmp_path = $this->getModelDocument()->getDocumentPath($movedEl);
                if (! @$newFolderPath) {
                    $newFolderPath = $this->params->get('uploadDir') . $movedEl["moduleId"] . "/" . $movedEl["folder"] . "/";
                }
                $new_path = $newFolderPath . $movedEl["name"];

                //thumb move
                if ($movedEl["doctype"] == DocumentInterface::DOC_TYPE_IMAGE) {
                    $tmp_documentPath = $this->getModelDocument()->getDocumentPath($movedEl, false, DocumentInterface::GENERATED_IMAGES_FOLDER . "/");
                    $new_documentPath = $newFolderPath . DocumentInterface::GENERATED_IMAGES_FOLDER . "/" . $movedEl["name"];
                    if (! $this->fs->has($newFolderPath . DocumentInterface::GENERATED_IMAGES_FOLDER . "/")) {
                        $this->fs->createDirectory($newFolderPath . DocumentInterface::GENERATED_IMAGES_FOLDER . "/");
                    }

                    // si existe déjà le fichier, on le supprime
                    if ($this->fs->has($new_documentPath)) {
                        $this->fs->delete($new_documentPath);
                    }
                    if ($this->fs->has($tmp_documentPath)) {
                        $this->fs->copy($tmp_documentPath, $new_documentPath);
                    }
                }

                // si existe déjà le fichier, on le supprime
                if ($this->fs->has($new_path)) {
                    $this->fs->delete($new_path);
                }
                if ($this->fs->has($tmp_path)) {
                    $this->fs->copy($tmp_path, $new_path);
                }

                $labelUpdate = "folderId";
            } else {
                $movedEl = $this->getById($id);
                $tmp_path = $this->getFolderPath($movedEl);
                if (! @$newFolderPath) {
                    $newFolderPath = $this->params->get('uploadDir') . "communecter/";
                    if (@$movedEl["contextType"] && @$movedEl["contextId"] && @$movedEl["docType"]) {
                        $docTypePath = ($movedEl["docType"] == "image") ? "album" : $movedEl["docType"];
                        $newFolderPath .= $movedEl["contextType"] . "/" . $movedEl["contextId"] . "/" . $docTypePath . "/";
                    }
                }
                $new_path = $newFolderPath . (string) $movedEl["_id"];

                if ($this->fs->has($tmp_path)) {
                    $this->fs->move($tmp_path, $new_path);
                }

                $labelUpdate = "parentId";
            }

            $this->db->update(
                $type,
                [
                    "_id" => $this->db->MongoId($id),
                ],
                [
                    $action => [
                        $labelUpdate => $idFolder,
                    ],
                ]
            );
            if ($idFolder != "") {
                $movedEl[$labelUpdate] = $idFolder;
            } else {
                $movedEl[$labelUpdate] = (@$movedEl["docType"] == "image" || @$movedEl["doctype"] == "image") ? "album" : "file";
            }
        }
        return [
            "result" => true,
            "movedEl" => $movedEl,
            "msg" => $this->language->t("common", "Documents added with success to {what}", [
                "{what}" => $folderName,
            ]),
            "movedIn" => $idFolder,
        ];
    }

    // TODO : fonction not used
    // public function createDocument($targetId, $targetType, $name, $colType = "collections", $docType = null, $subDir = [])
    // {
    //     $createdIn = null;
    //     $target = $this->getModelElement()->getByTypeAndId($targetType, $targetId);
    //     if (! empty($target)) {
    //         $pathToCreate = $colType . ".";
    //         if ($docType != null) {
    //             $pathToCreate .= $docType . ".";
    //         }
    //         if (@$subDir && ! empty($subDir)) {
    //             foreach ($subDir as $dir) {
    //                 $pathToCreate .= $dir . ".";
    //                 $createdIn = $dir;
    //             }
    //         }

    //         $pathToCreate .= $name;
    //         $this->db->update(
    //             $targetType,
    //             [
    //                 "_id" => $this->db->MongoId($targetId),
    //             ],
    //             [
    //                 '$set' => [
    //                     $pathToCreate => [
    //                         "updated" => $this->db->MongoDate(time()),
    //                     ],
    //                 ],
    //             ]
    //         );

    //         return [
    //             "result" => true,
    //             "msg" => $this->language->t("common", "Collection {what} created with success", [
    //                 "{what}" => $name,
    //             ]),
    //             "createdIn" => @$createdIn,
    //         ];
    //     } else {
    //         return [
    //             "result" => false,
    //             "msg" => $this->language->t("common", "Something went wrong"),
    //         ];
    //     }
    // }

    /**
     * Counts the number of subfolders for a given folder ID.
     *
     * @param string $id The ID of the folder.
     * @return int The number of subfolders.
     */
    public function countSubfolders(string $id): int
    {
        return $this->db->count(FolderInterface::COLLECTION, [
            "parentId" => $id,
        ]);
    }

    /**
     * Counts the number of subfolders by context.
     *
     * @param string $id The ID of the folder.
     * @param string $type The type of the folder.
     * @param string $docType The document type.
     * @return int The number of subfolders.
     */
    public function countSubfoldersByContext(string $id, string $type, string $docType): int
    {
        return $this->db->count(FolderInterface::COLLECTION, [
            "contextId" => $id,
            "contextType" => $type,
            "docType" => $docType,
            "parentId" => [
                '$exists' => false,
            ],
        ]);
    }
}
