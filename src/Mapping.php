<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\MappingInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;

class Mapping extends BaseModel implements MappingInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
    }

    /**
     * Insert a new mapping into the database.
     *
     * @param array $mapping The mapping data to be inserted.
     * @param string $creatorId The ID of the creator.
     * @return array The inserted mapping data.
     */
    public function insert(array $mapping, string $creatorId): array
    {
        $newMapping = $mapping;
        $newMapping["userId"] = $creatorId;
        $newMapping["key"] = $mapping["name"];

        $newMapping = $this->db->insert(MappingInterface::COLLECTION, $newMapping);

        return [
            "result" => true,
            "msg" => "Votre mapping à bien été sauvegardé.",
            "newMapping" => $newMapping,
        ];
    }

    // TODO : $creatorId is not used
    /**
     * Deletes a record from the database.
     *
     * @param array $post The post data.
     * @param string|null $creatorId The ID of the creator.
     * @return array The updated array after deletion.
     */
    public function delete(array $post, ?string $creatorId): array
    {
        $idMapping = $this->db->MongoId($post['idMapping']);

        $where = [
            '_id' => $idMapping,
        ];

        $this->db->remove(MappingInterface::COLLECTION, $where);

        return [
            "result" => true,
            "msg" => "Votre mapping à bien été supprimé.",
        ];
    }

    // TODO : $creatorId is not used
    /**
     * Update the mapping with the given post data and creator ID.
     *
     * @param array $post The post data to update the mapping.
     * @param string|null $creatorId The ID of the creator. Can be null.
     * @return array The updated mapping.
     */
    public function update(array $post, ?string $creatorId): array
    {
        $idMapping = $this->db->MongoId($post['idMapping']);

        $where = [
            '_id' => $idMapping,
        ];
        $newField = $post['mapping']['fields'];

        $newField = $this->replaceDot($newField);

        var_dump($newField);

        $this->db->update(MappingInterface::COLLECTION, $where, [
            '$set' => [
                'fields' => $newField,
            ],
        ]);

        return [
            "result" => true,
            "msg" => "Votre mapping à bien été modifié.",
            "newField" => $newField,
        ];
    }

    /**
     * Replaces dots in the keys of the given array with the provided new field.
     *
     * @param array $newField The new field to replace the dots with.
     * @return array The modified array with dots replaced.
     */
    public function replaceDot(array $newField): array
    {
        foreach ($newField as $key => $value) {
            if (strpos($key, '.')) {
                $newKey = $key;
                $newKey = str_replace(".", "_dot_", $newKey);
                $newField[$newKey] = $value;
                unset($newField[$key]);
            }
        }

        return $newField;
    }

    /**
     * Replaces the keys in the array with dot notation.
     *
     * @param array $newField The array with the new keys.
     * @return array The modified array with dot notation keys.
     */
    public function replaceByRealDot(array $newField): array
    {
        foreach ($newField as $key => $value) {
            if (strpos($key, '_dot_')) {
                $newKey = $key;
                $newKey = str_replace("_dot_", ".", $newKey);

                $newField[$newKey] = $value;
                unset($newField[$key]);
            }
        }

        return $newField;
    }
}
