<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\ActivityStreamInterface;
use PixelHumain\Models\Interfaces\ActivityStreamReferenceInterface;
use PixelHumain\Models\Interfaces\ActStrInterface;
use PixelHumain\Models\Interfaces\NewsInterface;
use PixelHumain\Models\Interfaces\PersonInterface;


use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\ParamsTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;

class ActivityStreamReference extends BaseModel implements ActivityStreamReferenceInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use SessionTrait;
    use ParamsTrait;
    use I18NTrait;
    use DbTrait;


    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
        $this->validateParamsProperty();
    }

    /**
     * Adds an activity stream reference.
     *
     * @param array|null $activityStream The activity stream reference to add.
     * @return void
     */
    public function add(?array $activityStream): void
    {
        if (isset($activityStream["type"]) && isset($activityStream["updated"]) && isset($activityStream["notify"]["id"])) {
            $refs = [];
            foreach ($activityStream["notify"]["id"] as $userId => $value) {
                $refs[] = [
                    "notificationId" => $activityStream["_id"],
                    "type" => $activityStream["type"],
                    "userId" => $userId,
                    "isUnread" => $value["isUnread"] ?? false,
                    "isUnseen" => $value["isUnseen"] ?? false,
                    "updated" => $activityStream["updated"],
                    "verb" => $activityStream["verb"],
                    "targetId" => $activityStream["target"]["id"] ?? null,
                    "targetType" => $activityStream["target"]["type"] ?? null,
                    "targetParentId" => $activityStream["target"]["parent"]["id"] ?? null,
                    "targetParentType" => $activityStream["target"]["parent"]["type"] ?? null,
                    "notifyObjectType" => $activityStream["notify"]["objectType"] ?? null,
                    "objectId" => $activityStream["object"]["id"] ?? null,
                    "objectType" => $activityStream["object"]["type"] ?? null,
                    "author" => (isset($activityStream["author"]) && is_array($activityStream["author"])) ? key($activityStream["author"]) : null,
                ];
            }

            $this->db->batchInsert(ActivityStreamReferenceInterface::COLLECTION, $refs);
        }
    }

    /**
     * Update the state of the activity stream reference by user.
     *
     * @param string $stateName The name of the state.
     * @param mixed $stateValue The value of the state.
     * @return void
     */
    public function updateStateByUser(string $stateName, $stateValue): void
    {
        if (array_search($stateName, ActivityStreamReferenceInterface::NOTIFICATION_STATES) > -1) {
            $userId = $this->session->getUserId();
            $refs = $this->db->updateWithOptions(
                ActivityStreamReferenceInterface::COLLECTION,
                [
                    "userId" => $userId,
                ],
                [
                    '$set' => [
                        $stateName => $stateValue,
                    ],
                ],
                [
                    "multiple" => true,
                ]
            );
        }
    }

    /**
     * Updates the state of the activity stream reference by notification ID.
     *
     * @param string $notificationId The ID of the notification.
     * @param string $stateName The name of the state to update.
     * @param mixed $stateValue The new value of the state.
     * @return void
     */
    public function updateStateByNotificationId(string $notificationId, string $stateName, $stateValue): void
    {
        if (array_search($stateName, ActivityStreamReferenceInterface::NOTIFICATION_STATES) > -1) {
            $userId = $this->session->getUserId();
            $refs = $this->db->updateWithOptions(
                ActivityStreamReferenceInterface::COLLECTION,
                [
                    "notificationId" => $this->db->MongoId($notificationId),
                    "userId" => $userId,
                ],
                [
                    '$set' => [
                        $stateName => $stateValue,
                    ],
                ],
                [
                    "multiple" => true,
                ]
            );
        }
    }

    /**
     * Removes activity stream references by user ID.
     *
     * @param string $userId The ID of the user.
     * @return void
     */
    public function removeByUserId(string $userId): void
    {
        $this->db->remove(ActivityStreamReferenceInterface::COLLECTION, [
            "userId" => $userId,
        ]);
    }

    /**
     * Removes an activity stream reference by notification ID.
     *
     * @param string $notificationId The ID of the notification.
     * @return void
     */
    public function removeByNotificationId(string $notificationId): void
    {
        $this->db->remove(ActivityStreamReferenceInterface::COLLECTION, [
            "userId" => $this->session->getUserId(),
            "notificationId" => $this->db->MongoId($notificationId),
        ]);
    }

    /**
     * Initializes the state of the activity stream reference.
     *
     * @param string $notificationId The ID of the notification.
     * @return void
     */
    public function initializeState(string $notificationId): void
    {
        $this->db->update(
            ActivityStreamReferenceInterface::COLLECTION,
            [
                "notificationId" => $this->db->MongoId($notificationId),
            ],
            [
                '$set' => [
                    "isUnseen" => true,
                    "isUnread" => true,
                ],
            ]
        );
    }

    /**
     * Retrieves a notification by constructing an array of parameters.
     *
     * @param array $construct The array of parameters used to construct the notification.
     * @return array|null The notification matching the given parameters, or null if not found.
     */
    public function getNotificationByConstruct(array $construct): ?array
    {
        $where = [
            "verb" => $construct["verb"],
            "targetId" => $construct["target"]["id"],
            "targetType" => $construct["target"]["type"],
            "updated" => [
                '$gte' => $this->db->MongoDate(strtotime('-7 days', time())),
            ],
        ];
        if ($construct["labelUpNotifyTarget"] == "object") {
            $where["author"] = $this->session->getUserId();
        }
        if ($construct["labelUpNotifyTarget"] == "object" && $construct["verb"] == ActStrInterface::VERB_ACCEPT) {
            $where["objectId"] = [
                '$ne' => null,
            ];
        }

        if ($construct["levelType"]) {
            $where["notifyObjectType"] = $construct["levelType"];
        } elseif ($construct["verb"] == ActStrInterface::VERB_POST && ! @$construct["target"]["targetIsAuthor"] && ! @$construct["target"]["userWall"]) {
            $where["notifyObjectType"] = NewsInterface::COLLECTION;
        }

        if ($construct["object"] && ! empty($construct["object"]) && in_array($construct["verb"], [ActStrInterface::VERB_COMMENT, ActStrInterface::VERB_REACT])) {
            $where["objectId"] = $construct["object"]["id"];
            $where["objectType"] = $construct["object"]["type"];
        }

        $ref = $this->db->findOne(ActivityStreamReferenceInterface::COLLECTION, $where, ["notificationId"]);

        return $ref ? $this->db->findOneById(ActivityStreamInterface::COLLECTION, $ref["notificationId"]) : null;
    }

    /**
     * Retrieves notifications based on a time limit.
     *
     * @param array $param The parameters for the query.
     * @param array $sort The sorting criteria for the query. Default is ["updated" => -1].
     * @return array The array of notifications.
     */
    public function getNotificationsByTimeLimit(array $param, array $sort = [
        "updated" => -1,
    ]): array
    {
        $refs = $this->db->findAndSort(ActivityStreamReferenceInterface::COLLECTION, $param, $sort);
        $notificationIds = [];
        foreach ($refs as $ref) {
            $notificationIds[] = $this->db->MongoId($ref["notificationId"]);
        }

        return $this->db->findAndSort(
            ActivityStreamInterface::COLLECTION,
            [
                "_id" => [
                    '$in' => $notificationIds,
                ],
            ],
            $sort
        );
    }

    /**
     * Retrieves notifications based on the given parameters.
     *
     * @param array $param The parameters for filtering the notifications.
     * @param int $indexMin The minimum index of the notifications to retrieve.
     * @param array $sort The sorting criteria for the notifications.
     * @param int $indexStep The number of notifications to retrieve per step.
     * @return array The array of notifications.
     */
    public function getNotificationsByStep(array $param, int $indexMin = 0, array $sort = [
        "updated" => -1,
    ], int $indexStep = 15): array
    {
        $refs = $this->db->findAndSortAndLimitAndIndex(ActivityStreamReferenceInterface::COLLECTION, $param, $sort, $indexStep, $indexMin);
        $notificationIds = [];
        foreach ($refs as $ref) {
            $notificationIds[] = $this->db->MongoId($ref["notificationId"]);
        }

        return $this->db->findAndSort(
            ActivityStreamInterface::COLLECTION,
            [
                "_id" => [
                    '$in' => $notificationIds,
                ],
            ],
            $sort
        );
    }

    /**
     * Counts the number of unseen notifications for a given user, element type, and element ID.
     *
     * @param string $userId The ID of the user.
     * @param string|null $elementType The type of the element (optional).
     * @param string|null $elementId The ID of the element (optional).
     * @return int The number of unseen notifications.
     */
    public function countUnseenNotifications(string $userId, ?string $elementType, ?string $elementId)
    {
        if ($elementType != PersonInterface::COLLECTION) {
            $params = [
                '$and' => [
                    [
                        'userId' => $userId,
                        'isUnseen' => true,
                        'verb' => [
                            '$ne' => ActStrInterface::VERB_ASK,
                        ],
                        'type' => [
                            '$ne' => "oceco",
                        ],
                    ],
                    [
                        '$or' => [
                            [
                                "targetType" => $elementType,
                                "targetId" => $elementId,
                            ],
                            [
                                "targetParentType" => $elementType,
                                "targetParentId" => $elementId,
                            ],
                        ],
                    ],
                ],
            ];
        } else {
            $params = [
                'userId' => $userId,
                'isUnseen' => true,
            ];
        }

        return $this->db->count(ActivityStreamReferenceInterface::COLLECTION, $params);
    }
}
