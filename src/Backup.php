<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Exception\CTKException;
use PixelHumain\Models\Interfaces\BackupInterface;
use PixelHumain\Models\Interfaces\CircuitInterface;

use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DataValidatorTrait;

use PixelHumain\Models\Traits\Interfaces\DataValidatorTraitInterface;

class Backup extends BaseModel implements BackupInterface, DataValidatorTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use I18NTrait;
    use SessionTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use DataValidatorTrait;

    //TODO Translate
    public static array $orderTypes = [];

    //From Post/Form name to database field name
    public static array $dataBinding = [
        "id" => [
            "name" => "id",
        ],
        "type" => [
            "name" => "type",
        ],
        "name" => [
            "name" => "name",
        ],
        "object" => [
            "name" => "object",
        ],
        "description" => [
            "name" => "description",
        ],
        "parentId" => [
            "name" => "parentId",
        ],
        "parentType" => [
            "name" => "parentType",
        ],
        "tags" => [
            "name" => "tags",
        ],
        "modified" => [
            "name" => "modified",
        ],
        "updated" => [
            "name" => "updated",
        ],
        "creator" => [
            "name" => "creator",
        ],
        "created" => [
            "name" => "created",
        ],
        "totalPrice" => [
            "name" => "totalPrice",
        ],
        "currency" => [
            "name" => "currency",
        ],
    ];

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateI18nProperty();
        $this->validateSessionProperty();
    }

    /**
     * Retrieves a backup by its ID.
     *
     * @param string $id The ID of the backup.
     * @return array|null The backup data as an array, or null if not found.
     */
    public function getById(string $id): ?array
    {
        $backup = $this->db->findOneById(BackupInterface::COLLECTION, $id);
        return $backup;
    }

    /**
     * Get a list of backups based on the given conditions.
     *
     * @param array $where The conditions to filter the backups.
     * @return array The list of backups that match the conditions.
     */
    public function getListBy(array $where): array
    {
        $backups = $this->db->findAndSort(BackupInterface::COLLECTION, $where, [
            "updated" => -1,
        ]);
        return $backups;
    }

    /**
     * Insert a backup record into the database.
     *
     * @param array $backup The backup data to be inserted.
     * @return array The inserted backup data.
     */
    public function insert(array $backup): array
    {
        try {
            $valid = $this->getModelDataValidator()->validate(BackupInterface::CONTROLLER, json_decode(json_encode($backup), true), null);
        } catch (CTKException $e) {
            $valid = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        if ($valid["result"]) {
            $backup["created"] = $this->db->MongoDate(time());
            $backup["updated"] = $this->db->MongoDate(time());
            if (! @$backup["parentId"]) {
                $backup["parentId"] = $this->session->getUserId();
                $backup["parentType"] = PersonInterface::COLLECTION;
            }

            $backup = $this->db->insert(BackupInterface::COLLECTION, $backup);
            if ($backup["type"] == CircuitInterface::COLLECTION) {
                $msg = "Your circuit is well registered";
            } else {
                $msg = "Your cart is well registered";
            }
            return [
                "result" => true,
                "msg" => $this->language->t("common", $msg),
                "backup" => $backup,
            ];
        } else {
            return [
                "result" => false,
                "error" => "400",
                "msg" => $this->language->t("common", "Something went really bad : " . $valid['msg']),
            ];
        }
    }

    /**
     * Update the backup with the given parameters.
     *
     * @param array $params The parameters for the update.
     * @return array The updated backup.
     */
    public function update(array $params): array
    {
        try {
            $valid = $this->getModelDataValidator()->validate(BackupInterface::CONTROLLER, json_decode(json_encode($params), true), null);
        } catch (CTKException $e) {
            $valid = [
                "result" => false,
                "msg" => $e->getMessage(),
            ];
        }
        if ($valid["result"]) {
            $set = [
                "updated" => $this->db->MongoDate(time()),
                "object" => $params["object"],
            ];
            if (@$params["totalPrice"]) {
                $set["totalPrice"] = $params["totalPrice"];
            }
            $id = $params["id"];
            $this->db->update(BackupInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ], [
                '$set' => $set,
            ]);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Your backup has been succesfuly updated"),
            ];
        } else {
            return [
                "result" => false,
                "error" => "400",
                "msg" => $this->language->t("common", "Something went really bad: " . $valid['msg']),
            ];
        }
    }

    /**
     * Deletes a backup by its ID.
     *
     * @param string $id The ID of the backup to delete.
     * @return array The result of the deletion operation.
     */
    public function delete(string $id): array
    {
        $backup = $this->getById($id);
        if (! empty($backup["parentId"]) && $this->session->getUserId() && $this->session->getUserId() == $backup["parentId"]) {
            $this->db->remove(BackupInterface::COLLECTION, [
                "_id" => $this->db->MongoId($id),
            ]);
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Your backup has been deleted with success"),
            ];
        } else {
            return [
                "result" => false,
                "error" => "400",
                "msg" => $this->language->t("common", "Something went really bad"),
            ];
        }
    }
}
