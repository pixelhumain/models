<?php

namespace PixelHumain\Models;

use PixelHumain\Models\Interfaces\CollectionInterface;
use PixelHumain\Models\Interfaces\DocumentInterface;
use PixelHumain\Models\Interfaces\PersonInterface;
use PixelHumain\Models\Traits\BaseModel\DbTrait;
use PixelHumain\Models\Traits\BaseModel\I18NTrait;
use PixelHumain\Models\Traits\BaseModel\SessionTrait;
use PixelHumain\Models\Traits\DocumentTrait;
use PixelHumain\Models\Traits\ElementTrait;
use PixelHumain\Models\Traits\Interfaces\DocumentTraitInterface;
use PixelHumain\Models\Traits\Interfaces\ElementTraitInterface;
use PixelHumain\Models\Traits\Interfaces\PersonTraitInterface;
use PixelHumain\Models\Traits\PersonTrait;
use stdClass;

class Collection extends BaseModel implements CollectionInterface, PersonTraitInterface, ElementTraitInterface, DocumentTraitInterface
{
    /**
     * Les traits définis ci-dessous correspondent à des fonctionnalités
     * qui peuvent être utilisés par la classe.
     */
    use DbTrait;
    use SessionTrait;
    use I18NTrait;

    /**
     * Les traits définis ci-dessous correspondent à des modèles
     * qui peuvent être utilisés par la classe.
     */
    use PersonTrait;
    use ElementTrait;
    use DocumentTrait;

    /**
     * Configure the model with the given configuration.
     *
     * @param array $config The configuration array.
     * @return void
     */
    protected function configure(array $config): void
    {
        foreach ($config as $property => $value) {
            if (property_exists($this, (string) $property)) {
                $this->$property = $value;
            }
        }

        $this->validateProperties();
    }

    /**
     * Validates the properties of the model.
     */
    protected function validateProperties(): void
    {
        $this->validateDbProperty();
        $this->validateSessionProperty();
        $this->validateI18nProperty();
    }

    /**
     * Create a new collection with the given name.
     *
     * @param string $name The name of the collection.
     * @return array The created collection.
     */
    public function create(string $name): array
    {
        // TODO : check if $this->session->getUserId() exists
        // TODO : pourquoi on fait new stdClass() ?
        $person = $this->getModelPerson()->getById($this->session->getUserId());
        $action = '$set';
        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($this->session->getUserId()),
            ],
            [
                $action => [
                    "collections." . $name => new stdClass(),
                ],
            ]
        );

        return [
            "result" => true,
            "msg" => $this->language->t("common", "Collection {what} created with success", [
                "{what}" => $name,
            ]),
        ];
    }

    /**
     * Update a collection item.
     *
     * @param string $name The name of the item to update.
     * @param string $newName The new name for the item.
     * @param bool $del Whether to delete the item after updating.
     * @return array The updated collection.
     */
    public function update(string $name, string $newName, bool $del = false): array
    {
        // TODO : check if $this->session->getUserId() exists
        $person = $this->getModelPerson()->getById($this->session->getUserId());

        if (! empty($person["collections"][$name])) {
            $actions = [];
            $action = "deleted";
            if (! $del) {
                $actions['$set'] = [
                    "collections." . $newName => $person["collections"][$name],
                ];
                $action = "updated";
            }

            $actions['$unset'] = [
                "collections." . $name => true,
            ];
            $this->db->update(
                PersonInterface::COLLECTION,
                [
                    "_id" => $this->db->MongoId($this->session->getUserId()),
                ],
                $actions
            );
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Collection {what} " . $action . " with success", [
                    "{what}" => $name,
                ]),
            ];
        } else {
            return [
                "result" => false,
                "collection" => "collections." . $name,
                "msg" => "Collection $name doesn't exist",
            ];
        }
    }

    /**
     * Adds an item to the collection.
     *
     * @param string $targetId The ID of the target item.
     * @param string $targetType The type of the target item.
     * @param string $collection The name of the collection (default: "favorites").
     * @return array The updated collection.
     */
    public function add(string $targetId, string $targetType, string $collection = "favorites"): array
    {
        // TODO : check if $this->session->getUserId() exists
        $person = $this->getModelPerson()->getById($this->session->getUserId());
        $target = $this->getModelElement()->checkIdAndType($targetId, $targetType);
        $collections = [
            "collections." . $collection . "." . $targetType . "." . $targetId => $this->db->MongoDate(time()),
            "updated" => time(),
        ];

        $action = '$set';
        $inc = 1;
        $verb = "added";
        $linkVerb = $this->language->t("common", "to") . " " . $collection;
        if ($collection == "favorites") {
            $linkVerb = $this->language->t("common", "to favorites");
        }

        if (@$person["collections"][$collection][$targetType][$targetId]) {
            $action = '$unset';
            $inc = -1;
            $verb = "removed";
            $linkVerb = $this->language->t("common", "from") . " " . $collection;
            if ($collection == "favorites") {
                $linkVerb = $this->language->t("common", "from favorites");
            }

            $collections = [
                "collections." . $collection . "." . $targetType . "." . $targetId => 1,
            ];
        }

        $this->db->update(
            PersonInterface::COLLECTION,
            [
                "_id" => $this->db->MongoId($this->session->getUserId()),
            ],
            [
                $action => $collections,
            ]
        );

        $this->db->update(
            $targetType,
            [
                "_id" => $this->db->MongoId($targetId),
            ],
            [
                '$inc' => [
                    "collectionCount" => $inc,
                ],
            ]
        );

        return [
            "result" => true,
            "list" => $action,
            "msg" => $this->language->t("common", "{what} " . $verb . " {where} with success", [
                "{what}" => $target["name"],
                "{where}" => $linkVerb,
            ]),
        ];
    }

    /**
     * Retrieves a collection of items.
     *
     * @param string|null $userId The user ID. Defaults to null.
     * @param string|null $type The type of items. Defaults to null.
     * @param string $collection The name of the collection. Defaults to "favorites".
     * @return array The collection of items.
     */
    public function get(?string $userId = null, ?string $type = null, string $collection = "favorites"): array
    {
        if (! $userId) {
            $userId = $this->session->getUserId();
        }
        // TODO : check if $this->session->getUserId() exists
        $person = $this->getModelPerson()->getById($userId);
        $list = [];
        $count = 0;
        if ($person && ! empty($person["collections"][$collection]) && is_array($person["collections"][$collection])) {
            foreach ($person["collections"][$collection] as $favtype => $value) {
                $ids = [];
                if (! $type || $type == $favtype) {
                    foreach ($value as $id => $date) {
                        array_push($ids, $this->db->MongoId((string) $id));
                    }
                    if (count($ids) > 0) {
                        $count += count($ids);
                        $list[$favtype] = $this->db->find((string) $favtype, [
                            "_id" => [
                                '$in' => $ids,
                            ],
                        ]);
                    }
                }
            }
        }

        return [
            "result" => true,
            "count" => $count,
            "list" => $list,
        ];
    }

    /**
     * Creates a new document in the collection.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $name The name of the document.
     * @param string $colType The type of the collection (default: "collections").
     * @param string|null $docType The type of the document (nullable).
     * @param array $subDir The subdirectories of the document.
     * @return array The created document.
     */
    public function createDocument(string $targetId, string $targetType, string $name, string $colType = "collections", ?string $docType = null, array $subDir = []): array
    {
        $createdIn = null;
        $target = $this->getModelElement()->getByTypeAndId($targetType, $targetId);
        if (! empty($target)) {
            $pathToCreate = $colType . ".";
            if ($docType != null) {
                $pathToCreate .= $docType . ".";
            }
            if (@$subDir && ! empty($subDir)) {
                foreach ($subDir as $dir) {
                    $pathToCreate .= $dir . ".";
                    $createdIn = $dir;
                }
            }

            $pathToCreate .= $name;
            $this->db->update(
                $targetType,
                [
                    "_id" => $this->db->MongoId($targetId),
                ],
                [
                    '$set' => [
                        $pathToCreate => [
                            "updated" => $this->db->MongoDate(time()),
                        ],
                    ],
                ]
            );

            return [
                "result" => true,
                "msg" => $this->language->t("common", "Collection {what} created with success", [
                    "{what}" => $name,
                ]),
                "createdIn" => @$createdIn,
            ];
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "Something went wrong"),
            ];
        }
    }

    //////////////////////////////BOUBOULLLEEEEE - MANAGE PORTFOLIO//////////////////////////////////
    /**
     * Updates the name of a document in a collection.
     *
     * @param string $targetId The ID of the target document.
     * @param string $targetType The type of the target document.
     * @param string $name The current name of the document.
     * @param string $newName The new name for the document.
     * @param string $colType The type of the collection. Default value is "collections".
     * @param string|null $docType The type of the document. Can be null.
     * @return array An array containing the updated collection.
     */
    public function updateCollectionNameDocument(string $targetId, string $targetType, string $name, string $newName, string $colType = "collections", ?string $docType = null): array
    {
        $params = [];
        $target = $this->getModelElement()->getElementSimpleById($params["target"]["id"], $params["target"]["type"], null, [$colType]);
        $targetCollection = $target[$colType];
        $pathToUp = $colType . ".";
        if ($docType != null && @$targetCollection[$docType]) {
            $targetCollection = $targetCollection[$docType];
            $pathToUp .= $docType . ".";
        }
        if (@$targetCollection[$name]) {
            $actions = [];
            $actions['$set'] = [
                $pathToUp . $newName => [
                    "updated" => $this->db->MongoDate(time()),
                ],
            ];
            $actions['$unset'] = [
                $pathToUp . $name => true,
            ];
            $findListDocument = $this->getModelDocument()->updateCollectionDocument($targetId, $targetType, $name, $newName, $docType);
            $this->db->update(
                $targetType,
                [
                    "_id" => $this->db->MongoId($targetId),
                ],
                $actions
            );
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Collection {what} updated with success", [
                    "{what}" => $name,
                ]),
                "newName" => $newName,
            ];
        } else {
            return [
                "result" => false,
                "collection" => "collections." . $name,
                "msg" => $this->language->t("common", "Collection {what} doesn't exist", [
                    "{what}" => $name,
                ]),
            ];
        }
    }

    // TODO : $subtype is not used
    /**
     * Deletes a document from the collection.
     *
     * @param string $targetId The ID of the target.
     * @param string $targetType The type of the target.
     * @param string $name The name of the document.
     * @param string $colType The type of the collection. Default value is "collections".
     * @param string|null $docType The type of the document. Nullable.
     * @param string|null $subtype The subtype of the document. Nullable.
     * @return array The result of the deletion operation.
     */
    public function deleteDocument(string $targetId, string $targetType, string $name, string $colType = "collections", ?string $docType = null, ?string $subtype = null): array
    {
        $params = [];
        $target = $this->getModelElement()->getElementSimpleById($params["target"]["id"], $params["target"]["type"], null, [$colType]);
        $targetCollection = $target[$colType];
        $pathToUp = $colType . ".";
        if ($docType != null && @$targetCollection[$docType]) {
            $targetCollection = $targetCollection[$docType];
            $pathToUp .= $docType . ".";
        }
        if (@$targetCollection[$name]) {
            $actions = [];
            $actions['$unset'] = [
                $pathToUp . $name => true,
            ];
            $this->db->update(
                $targetType,
                [
                    "_id" => $this->db->MongoId($targetId),
                ],
                $actions
            );
            if ($docType == DocumentInterface::COLLECTION) {
                $this->getModelDocument()->removeAllDocument($params["target"]["id"], $params["target"]["type"], $name, $docType);
            }
            return [
                "result" => true,
                "msg" => $this->language->t("common", "Collection {what} deleted with success", [
                    "{what}" => $name,
                ]),
            ];
        } else {
            return [
                "result" => false,
                "collection" => "collections." . $name,
                "msg" => $this->language->t("common", "Collection {what} doesn't exist", [
                    "{what}" => $name,
                ]),
            ];
        }
    }

    /**
     * Deletes an item from a collection.
     *
     * @param string $targetId The ID of the target item.
     * @param string $targetType The type of the target item.
     * @param string $fileId The ID of the file to be deleted.
     * @param string $colType The type of the collection (default: "collections").
     * @param string|null $docType The type of the document (optional).
     * @param array $nameCol The name of the collection (optional).
     * @return array The updated collection.
     */
    public function deleteFromCollection(string $targetId, string $targetType, string $fileId, string $colType = "collections", ?string $docType = null, array $nameCol = []): array
    {
        $params = [];
        $name = null;
        $target = $this->getModelElement()->getElementSimpleById($params["target"]["id"], $params["target"]["type"], null, [$colType]);
        $targetCollection = $target[$colType];
        $pathToUp = $colType . ".";
        if ($docType != null && @$targetCollection[$docType]) {
            $targetCollection = $targetCollection[$docType];
            $pathToUp .= $docType . ".";
        }
        if ($nameCol != null && @$targetCollection[$nameCol]) {
            $targetCollection = $targetCollection[$docType];
            $pathToUp .= $nameCol . ".";
        }
        if (@$targetCollection[$fileId]) {
            $actions = [];
            $actions['$unset'] = [
                $pathToUp . $name => true,
            ];
            $this->db->update(
                $targetType,
                [
                    "_id" => $this->db->MongoId($targetId),
                ],
                $actions
            );
            return [
                "result" => true,
                "msg" => $this->language->t("common", $docType . " deleted with success", [
                    "{what}" => $name,
                ]),
            ];
        } else {
            return [
                "result" => false,
                "msg" => $this->language->t("common", "Something went wrong"),
            ];
        }
    }

    /**
     * Adds a document to the collection.
     *
     * @param array $ids The IDs of the documents to add.
     * @param string|null $nameCol The name of the collection. Defaults to null.
     * @return array The updated collection.
     */
    public function addDocumentToColection(array $ids, ?string $nameCol = null): array
    {
        foreach ($ids as $id) {
            $this->getModelDocument()->moveDocumentToCollection($id, $nameCol);
        }
        return [
            "result" => true,
            "msg" => $this->language->t("common", "Documents added with success to {what}", [
                "{what}" => $nameCol,
            ]),
            "movedIn" => $nameCol,
        ];
    }
}
